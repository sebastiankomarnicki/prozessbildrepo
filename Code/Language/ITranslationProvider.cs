﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Promess.Language
{
    /// <summary>
    /// Interface for providing translations of items (mostly text)
    /// </summary>
    public interface ITranslationProvider
    {
        bool TrySetLanguage(CultureInfo language);

        String GetLanguageDisplayName(CultureInfo language);

        CultureInfo CurrentLanguage { get; }

        LanguageData LanguageData { get; }

        IEnumerable<CultureInfo> Languages { get; }
    }
}
