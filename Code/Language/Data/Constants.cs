﻿namespace Promess.Language.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
	class Constants
	{

		/// <summary>
		/// The default interface language if the current system language isn't in the list
		/// of supported languages.
		/// </summary>
		internal const string FallbackDefaultLanguageCode = "de";
        internal const bool FallbackDefaultIsRtl = false;

		//Application Defaults

		//Configuration XML names/paths
        internal const string LanguagSettingsPathFormatString = "{0}.LanguageSettings.xml";
        internal const string LanguageDictionaryByCodePathFormatString = "{0}.Language{1}.xml";

		//Language definition XML names/XPaths
		internal const string LanguageCodeAttr = "code";
		internal const string TextEntryXPath = "//UIText/Entry";
		internal const string TextEntryKeyAttr = "key";

		internal const string IsRtlXPath = "//IsRtl";
		internal const string IsNotRtlValue = "0";

        internal const string DefaultValuesXPath = "//DefaultValues";
        internal const string SupportedLanguageCodesXPath = "//SupportedLanguages";
        internal const string LanguageNamesXPath = "//LanguageNames";
        internal const string LanguageXPath = "Language";


    }
}
