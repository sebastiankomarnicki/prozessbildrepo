﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Promess.Language
{
    /// <summary>
    /// Dummy translation provider for when a provider was not yet initialized from data
    /// </summary>
    internal class DummyTranslationProvider:ITranslationProvider
    {
        public const bool DefaultIsRightToLeft = true;
        private LanguageData _dummyData = new LanguageData(new Dictionary<string, string>(), DefaultIsRightToLeft);

        public CultureInfo CurrentLanguage
        {
            get { return CultureInfo.InvariantCulture; }
        }

        public IEnumerable<CultureInfo> Languages
        {
            get
            {
                return Enumerable.Empty<CultureInfo>();
            }
        }

        public LanguageData LanguageData
        {
            get
            {
                return _dummyData;
            }
        }

        public bool TrySetLanguage(CultureInfo language)
        {
            return false;
        }

        public string GetLanguageDisplayName(CultureInfo language)
        {
            return null;
        }
    }
}
