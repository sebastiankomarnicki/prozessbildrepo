﻿using System;
using System.Collections.Generic;
using System.Waf.Foundation;

namespace Promess.Language
{
    /// <summary>
    /// Encapsulates data read from a language file. Only for looking up text.
    /// </summary>
    public class LanguageData:Model
    {
        private Dictionary<String, String> _uiText;
        private bool _isRightToLeft;

        public LanguageData(Dictionary<String, String>languageEntries, bool isRightToLeft)
        {
            this._uiText = languageEntries;
            this._isRightToLeft = isRightToLeft;
        }

        public bool IsRightToLeft
        {
            get
            {
                return _isRightToLeft;
            }
        }

        /// <summary>
        /// Provides the translation of the key. Retrieved value will always be a <see cref="String"/>.
        /// </summary>
        /// <param name="key">Lookup key</param>
        /// <returns>Value for the key, otherwise !key!.</returns>
        public object Translate(string key)
        {
            if (_uiText.ContainsKey(key))
                return _uiText[key];

            return String.Format("!{0}!", key);
        }
    }
}
