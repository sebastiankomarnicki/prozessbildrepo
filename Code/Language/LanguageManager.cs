﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Waf.Foundation;

namespace Promess.Language
{
    /// <summary>
    /// Manager to provide information about supported languages, switch between languages and retrieve translations.
    /// </summary>
    public class LanguageManager : Model
	{
		private static LanguageManager _current;

		private ITranslationProvider _currentTranslationProvider;

		private LanguageManager()
		{
            PropertyChangedEventManager.AddHandler(this, LanguageChanged, nameof(LanguageManager.CurrentLanguage));
        }

        private void LanguageChanged(object sender, PropertyChangedEventArgs e)
        {
            if (Thread.CurrentThread.CurrentUICulture != CurrentLanguage)
                Thread.CurrentThread.CurrentUICulture = CurrentLanguage;
        }

        public IEnumerable<CultureInfo> Languages
        {
            get
            {
                    return TranslationProvider.Languages;
            }
        }


        /// <summary>
        /// Gets the current instance of this singleton.
        /// </summary>
        /// <value>The current instance.</value>
        public static LanguageManager Current
        {
            get
            {
                if (_current == null)
                    _current = new LanguageManager();

                return _current;
            }
        }


		/// <summary>
		/// Gets or sets the language definition used by the entire interface.
		/// </summary>
		/// <value>The language definition.</value>
        public ITranslationProvider TranslationProvider
        {
            get
            {
                if (_currentTranslationProvider == null)
                {
                    return new DummyTranslationProvider(); ;
                }
                return _currentTranslationProvider;
            }
            set
            {
                if (SetProperty(ref _currentTranslationProvider, value))
                {
                    RaisePropertiesChanged();
                }
            }
        }

        public bool TryChangeLanguage(CultureInfo newLanguage)
        {
            if (TranslationProvider.TrySetLanguage(newLanguage))
            {
                RaisePropertiesChanged();
                return true;
            }
            return false;
        }

        public CultureInfo CurrentLanguage
        {
            get { return TranslationProvider.CurrentLanguage; }
        }

        public LanguageData CurrentLanguageData
        {
            get { return TranslationProvider.LanguageData; }
        }

		public bool IsRightToLeft
		{
			get
			{
				return CurrentLanguageData.IsRightToLeft;
			}
		}

        private void RaisePropertiesChanged()
        {
            RaisePropertyChanged(nameof(CurrentLanguage));
            RaisePropertyChanged(nameof(CurrentLanguageData));
            RaisePropertyChanged(nameof(IsRightToLeft));
        }
    }
}
