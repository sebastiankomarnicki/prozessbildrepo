﻿using Promess.Language.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Waf.Foundation;
using System.Xml;

namespace Promess.Language
{
    /// <summary>
    /// Read language data from an xml document
    /// </summary>
    public class XmlTranslationProvider : Model, ITranslationProvider
    {
        private  Dictionary<CultureInfo, String> _languageDisplayNames;
        private  Dictionary<String, CultureInfo> _supportedLanguageCodes;
        private  CultureInfo _defaultLanguage;
        private  bool _defaultIsRightToLeft;
        private String _baseResourcePath;
        private Assembly _backingAssembly;

        private CultureInfo _currentLanguage;

        //wrap the following fields inside an object?
        private LanguageData _currentLanguageData;

        public XmlTranslationProvider(String baseResourcePath, Assembly assembly, CultureInfo startupLanguage = null)
        {
            this._baseResourcePath = baseResourcePath;
            this._backingAssembly = assembly;

            _languageDisplayNames = new Dictionary<CultureInfo, String>();
            _supportedLanguageCodes = new Dictionary<String, CultureInfo>();

            LoadDefaultValues();

            SetInitialLanguageToOrDefault(startupLanguage);

            LoadCurrentLanguage();
        }

        public IEnumerable<CultureInfo> Languages
        {
            get
            {
                return _languageDisplayNames.Keys.AsEnumerable();
            }
        }

        public CultureInfo CurrentLanguage
        {
            get
            {
                return _currentLanguage;
            }
            private set
            {
                SetProperty(ref _currentLanguage, value);
            }
        }

        public LanguageData LanguageData
        {
            get { return _currentLanguageData;  }
            private set
            {
                SetProperty(ref _currentLanguageData, value);
            }
        }

        public bool TrySetLanguage(CultureInfo language)
        {
            if (language==null || language.Equals(CurrentLanguage) || !_supportedLanguageCodes.Values.Contains(language))
            {
                return false;
            }
            CurrentLanguage = language;
            LoadCurrentLanguage();
            return true;
        }

        #region load data
        /// <summary>
        /// Load the current language data from the resources
        /// </summary>
        private void LoadCurrentLanguage()
        {
            String languageCode = _supportedLanguageCodes.First(kvp => kvp.Value.Equals(CurrentLanguage)).Key.ToUpper();
            String resourcePath = String.Format(Constants.LanguageDictionaryByCodePathFormatString, this._baseResourcePath, languageCode);
            Stream stream = this._backingAssembly.GetManifestResourceStream(resourcePath);

            XmlDocument languageDocument = new XmlDocument();
            languageDocument.Load(stream);

            XmlElement languageData = languageDocument.DocumentElement;

            XmlNodeList mappings = languageData.SelectNodes(Constants.TextEntryXPath);

            //Add key-value pairs for each localised text entry.
            Dictionary<String, String> uiText = new Dictionary<string, string>();
            foreach (XmlNode currentMapping in mappings)
            {
                XmlAttribute key = currentMapping.Attributes[Constants.TextEntryKeyAttr];

                if (key != null && !String.IsNullOrEmpty(key.Value))
                {
                    Debug.Assert(!uiText.ContainsKey(key.Value), String.Format("Key '{0}' already present in dictionary.", key.Value));
                    uiText.Add(key.Value, currentMapping.InnerText);
                }
            }

            bool isRightToLeft;
            XmlNode rtlNode = languageData.SelectSingleNode(Constants.IsRtlXPath);
            if (rtlNode == null)
            {
                isRightToLeft = _defaultIsRightToLeft;
            } else
            {
                isRightToLeft = rtlNode.InnerText != Constants.IsNotRtlValue;
            }
            LanguageData = new LanguageData(uiText, isRightToLeft);
        }

        /// <summary>
        /// Set the initial startup language
        /// </summary>
        /// <param name="startupLanguage">Try to set the given language as startup language, use loaded default language if null or unavailable</param>
        private void SetInitialLanguageToOrDefault(CultureInfo startupLanguage)
        {
            if (startupLanguage == null || !_supportedLanguageCodes.Values.Contains(startupLanguage))
            {
                CurrentLanguage = _defaultLanguage;
            }else
            {
                CurrentLanguage = startupLanguage;
            }
        }

        /// <summary>
        /// Load all default values for the language settings
        /// </summary>
        private void LoadDefaultValues()
        {
            string resourcePath = String.Format(Constants.LanguagSettingsPathFormatString, this._baseResourcePath);
            Stream stream = this._backingAssembly.GetManifestResourceStream(resourcePath);

            XmlDocument languageSettings = new XmlDocument();
            languageSettings.Load(stream);
            RegisterSupportedLanguages(languageSettings.DocumentElement);
            LoadLanguageNames(languageSettings.DocumentElement);
            LoadDefaultValues(languageSettings.DocumentElement);
        }

        /// <summary>
        /// Loads the language ISO codes
        /// </summary>
        /// <param name="languageSettingsContent">Content of the languageSetings file, path to ISO codes needs to be selected</param>
        internal void RegisterSupportedLanguages(XmlElement languageSettingsContent)
        {
            XmlNode supportedLanguages = languageSettingsContent.SelectSingleNode(Constants.SupportedLanguageCodesXPath);
            Dictionary<String, String> toRegisterLanguages = GetAllSubentries(supportedLanguages, Constants.LanguageCodeAttr);
            _supportedLanguageCodes.Clear();
            foreach (string languageCode in toRegisterLanguages.Keys)
            {
                CultureInfo language = CultureInfo.GetCultures(CultureTypes.AllCultures).FirstOrDefault(c => c.Name == languageCode);
                _supportedLanguageCodes.Add(languageCode, language);
            }
            Debug.Assert(_supportedLanguageCodes.Count > 0, "There are no supported languages!");
        }


        /// <summary>
        /// Loads the language names for display and selection.
        /// </summary>
        /// <param name="languageSettingsContent">Content of the languageSetings file, path to localized language names must be selected.</param>
        /// <remarks>All languages are to be registered by this stage.</remarks>
        internal void LoadLanguageNames(XmlElement languageSettingsContent)
        {
            XmlNode languageCodeNameMappings = languageSettingsContent.SelectSingleNode(Constants.LanguageNamesXPath);
            Dictionary<String, String> languageNames = GetAllSubentries(languageCodeNameMappings, Constants.LanguageCodeAttr);

            _languageDisplayNames.Clear();

            CultureInfo retrievedCultureInfo = null;

            foreach (KeyValuePair<String, String> currentLangEntry in languageNames)
            {
                if (_supportedLanguageCodes.TryGetValue(currentLangEntry.Key, out retrievedCultureInfo))
                {
                    _languageDisplayNames.Add(retrievedCultureInfo, currentLangEntry.Value);
                }
            }
            Debug.Assert(_supportedLanguageCodes.Count == _languageDisplayNames.Count, String.Format("There were only {0} code to language name mapping, but {1} supported languages", _languageDisplayNames.Count, _supportedLanguageCodes.Count));
        }

        /// <summary>
        /// Loads the default (fallback) values of misc. settings, including the default language.
        /// </summary>
        /// <param name="languageSettings">Content of the languageSetings file, path to default values must be selected.</param>
        /// <remarks>All languages are to be registered by this stage and language name mappings loaded.</remarks>
        private void LoadDefaultValues(XmlElement languageSettings)
        {
            XmlNode defaultNode = languageSettings.SelectSingleNode(Constants.DefaultValuesXPath);
            XmlNode defaultLanguageNode = defaultNode.SelectSingleNode(Constants.LanguageXPath);
            String languageCode = Constants.FallbackDefaultLanguageCode;
            if (defaultLanguageNode != null)
            {
                XmlAttribute languageCodeAttributes = defaultLanguageNode.Attributes[Constants.LanguageCodeAttr];
                if (languageCode != null && !String.IsNullOrEmpty(languageCodeAttributes.Value))
                {
                    languageCode = languageCodeAttributes.Value;
                }
            }
            Debug.Assert(_supportedLanguageCodes.ContainsKey(languageCode), String.Format("The default language code {0} must be part of the supported language codes.",languageCode));
            _defaultLanguage = _supportedLanguageCodes[languageCode];

            XmlNode rtlNode = defaultNode.SelectSingleNode(Constants.IsRtlXPath);
            if (rtlNode != null) {
                _defaultIsRightToLeft = !rtlNode.InnerText.Equals(Constants.IsNotRtlValue);
            } else
            {
                _defaultIsRightToLeft = Constants.FallbackDefaultIsRtl;
            }
        }

        #region helper functions
        /// <summary>
        /// Extracts the attribute values and their inner text
        /// </summary>
        /// <param name="titles">The data</param>
        /// <param name="attribute">The attributes to extract</param>
        /// <returns></returns>
        internal static Dictionary<String, String> GetAllSubentries(XmlNode nodeWithChildren, String attribute)
        {
            Debug.Assert(!(nodeWithChildren == null));
            Debug.Assert(!String.IsNullOrEmpty(attribute));

            Dictionary<String, String> entries = new Dictionary<String, String>();

            foreach (XmlNode childNode in nodeWithChildren)
            {
                XmlAttribute extractedAttribute = childNode.Attributes[attribute];
                if (extractedAttribute != null && !String.IsNullOrWhiteSpace(extractedAttribute.Value))
                {
                    entries.Add(extractedAttribute.Value, childNode.InnerText);
                }
            }
            return entries;
        }

        public String GetLanguageDisplayName(CultureInfo language)
        {
            String displayName = null;
            if (language != null)
            {
                _languageDisplayNames.TryGetValue(language, out displayName);
            }
            return displayName;
        }
        #endregion
        #endregion
    }
}
