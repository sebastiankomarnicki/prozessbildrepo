﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Promess.Pbld.Adorners
{
    /// <summary>
    /// original code: https://www.codeproject.com/Articles/404827/Rubberband-Behavior-for-WPF-ListBox
    /// Ordner to select contained elements using a rubber band
    /// </summary>
    public class RubberBandAdorner : Adorner
    {
        ListBox _selector;
        private Point _startpoint;
        private Point _currentpoint;
        private Brush _brush;
        private bool _flag;
        private ScrollViewer _viewer;
        private ScrollBar _scrollbar;

        public RubberBandAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
            IsHitTestVisible = false;
            adornedElement.MouseMove += new MouseEventHandler(adornedElement_PreviewMouseMove);
            adornedElement.MouseLeftButtonDown += new MouseButtonEventHandler(adornedElement_PreviewMouseLeftButtonDown);
            adornedElement.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(adornedElement_PreviewMouseLeftButtonUp);
            _brush = new SolidColorBrush(SystemColors.HighlightColor);
            _brush.Opacity = 0.3;
        }

        void adornedElement_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DisposeRubberBand();
            this._selector?.Focus();
        }

        void adornedElement_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this._selector = AdornedElement as ListBox;
            this._selector.Focus();
            if (this._selector.SelectedItems != null && (this._selector.SelectionMode == SelectionMode.Extended || this._selector.SelectionMode == SelectionMode.Multiple))
            {
                this._selector.SelectedItems.Clear();
            }
            this._startpoint = Mouse.GetPosition(this.AdornedElement);
            Mouse.Capture(this._selector);
            this._flag = true;
        }

        public static childItem FindVisualChild<childItem>(DependencyObject obj)
   where childItem : DependencyObject
        {
            // Search immediate children first (breadth-first)
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child != null && child is childItem)
                    return (childItem)child;

                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);

                    if (childOfChild != null)
                        return childOfChild;
                }
            }

            return null;
        }

        void adornedElement_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && _flag)
            {
                _currentpoint = Mouse.GetPosition(AdornedElement);

                Selector _selector = AdornedElement as Selector;
                if (_viewer == null)
                {
                    _viewer = FindVisualChild<ScrollViewer>(_selector);
                }

                if (_scrollbar == null)
                {
                    _scrollbar = FindVisualChild<ScrollBar>(_viewer);
                }

                if (_selector.Items.Count > 0)
                {
                    if (_currentpoint.Y > ((FrameworkElement)AdornedElement).ActualHeight && _viewer.VerticalOffset < _selector.ActualHeight && _scrollbar.Visibility == System.Windows.Visibility.Visible)
                    {
                        _startpoint.Y -= 50;
                    }
                    else if (_currentpoint.Y < 0 && _viewer.VerticalOffset > 0 && _scrollbar.Visibility == System.Windows.Visibility.Visible)
                    {
                        _startpoint.Y += 50;
                    }
                }

                InvalidateVisual();

                foreach (var obj in _selector.Items)
                {
                    ListBoxItem item = _selector.ItemContainerGenerator.ContainerFromItem(obj) as ListBoxItem;
                    if (item != null)
                    {
                        Point point = item.TransformToAncestor(AdornedElement).Transform(new Point(0, 0));
                        Rect bandrect = new Rect(_startpoint, _currentpoint);
                        Rect elementrect = new Rect(point.X, point.Y, item.ActualWidth, item.ActualHeight);
                        if (bandrect.Contains(elementrect))
                        {
                            item.IsSelected = true;
                        }
                        else
                        {
                            item.IsSelected = false;
                        }
                    }
                }
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            Rect rect = new Rect(_startpoint, _currentpoint);
            drawingContext.DrawGeometry(_brush, new Pen(SystemColors.HighlightBrush, 1), new RectangleGeometry(rect));
            base.OnRender(drawingContext);
        }

        private void DisposeRubberBand()
        {
            _currentpoint = new Point(0, 0);
            _startpoint = new Point(0, 0);
            AdornedElement.ReleaseMouseCapture();
            InvalidateVisual();
            _flag = false;
        }
    }
}
