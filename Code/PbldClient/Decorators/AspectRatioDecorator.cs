﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Promess.Pbld.Decorators
{
    public class AspectRatioDecorator:Decorator
    {
        public static readonly DependencyProperty AspectRatioProperty =
            DependencyProperty.Register(
                nameof(AspectRatio),
                typeof(double),
                typeof(AspectRatioDecorator),
                new FrameworkPropertyMetadata(1d, FrameworkPropertyMetadataOptions.AffectsMeasure),
                ValidateAspectRatio
                );

        public double AspectRatio
        {
            get { return (double)GetValue(AspectRatioProperty); }
            set { SetValue(AspectRatioProperty, value); }
        }

        private bool ApplyAspectRatio
        {
            get { return AspectRatio > 0; }
        }

        private static bool ValidateAspectRatio(object maybeAspectRatio)
        {
            //should binding to number type be allower? (int, short..)
            if (!(maybeAspectRatio is double))
                return false;

            return ValidateAspectRatio((double)maybeAspectRatio);
        }

        private static bool ValidateAspectRatio(double aspectRatio)
        {
            return !Double.IsPositiveInfinity(aspectRatio) && !Double.IsNaN(aspectRatio);
        }



        protected override Size MeasureOverride(Size availableSize)
        {
            if (Child is null)
                return Size.Empty;
            if (!ApplyAspectRatio)
                return base.MeasureOverride(availableSize);

            //this is most likely wrong when child is a panel
            availableSize = ApplyRatioToSize(availableSize, AspectRatio);
            if (Double.IsInfinity(availableSize.Width) || Double.IsInfinity(availableSize.Width)){
                Child.Measure(availableSize);
                return ApplyRatioToSize(Child.DesiredSize, AspectRatio);
            }
            return availableSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            if (Child is null || !ApplyAspectRatio)
                return base.ArrangeOverride(finalSize);

            var ratioSize = ApplyRatioToSize(finalSize, AspectRatio);
            double deltaWidth = finalSize.Width - ratioSize.Width;
            double deltaHeight = finalSize.Height - ratioSize.Height;

            double top = 0;
            double left = 0;

            if (!Double.IsNaN(deltaWidth) && !Double.IsInfinity(deltaWidth))
                left = deltaWidth / 2;
            if (!Double.IsNaN(deltaHeight) && !Double.IsInfinity(deltaHeight))
                top = deltaHeight / 2;

            var arrangeRect = new Rect(new Point(left, top), ratioSize);
            Child.Arrange(arrangeRect);
            return finalSize;
        }

        internal static Size ApplyRatioToSize(Size size, double aspectRatio)
        {
            double toUseHeight = Math.Min(size.Width/aspectRatio, size.Height);
            double toUseWidth = toUseHeight*aspectRatio;
            return new Size(toUseWidth, toUseHeight);
        }
    }
}
