﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Windows;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    [Export(typeof(IAboutView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class AboutWindow : Window, IAboutView
    {
        public AboutWindow()
        {
            InitializeComponent();
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
