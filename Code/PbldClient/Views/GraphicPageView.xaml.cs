﻿using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.IViews;
using Promess.Pbld.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for GraphicPageView.xaml
    /// </summary>
    [Export(typeof(IGraphicPageView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class GraphicPageView : UserControl, IGraphicPageView
    {
        public GraphicPageView()
        {
            InitializeComponent();
        }

        private void EmbeddedImage_ToolTipOpening(object sender, ToolTipEventArgs e)
        {
            Image image = (Image)sender;
            //ToolTip tooltip = new ToolTip { Content = "My Tooltip" };
            var point = Mouse.GetPosition(image);
            image.ToolTip = $"{point.X}, {point.Y}";
            //tooltip.IsOpen = true;
            //tooltip.IsOpen = false;
        }

        private void EmbeddedImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                return;
            Image image = (Image)sender;
            Size size = new Size(image.ActualWidth, image.ActualHeight);
            Point position = e.GetPosition(image);
            var vm = (GraphicPageViewModel)DataContext;
            var item = (ImageGPEditorItem)image.DataContext;
            var args = Tuple.Create(size, item, position);
            var result = vm.GetQdasImageItemDetailsForPosition(size, item, position);
            if (String.IsNullOrEmpty(result))
                return;
            
            Popup myPopup = new Popup();
            myPopup.PlacementTarget = image;           
            myPopup.Placement = PlacementMode.Mouse;
            myPopup.AllowsTransparency = true;
            TextBlock popupText = new TextBlock();
            popupText.Text = result;             
            popupText.TextWrapping = TextWrapping.Wrap;
            popupText.Background = SystemColors.InfoBrush;
            myPopup.Child = popupText;
            image.ToolTip = myPopup;
            myPopup.IsOpen = true;
            myPopup.StaysOpen = false;
        }
    }
}
