﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for MetaEditGraphicsView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IEditGraphicProjectView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class EditGraphicProjectView : UserControl, IEditGraphicProjectView
    {
        public EditGraphicProjectView()
        {
            InitializeComponent();
        }
    }
}
