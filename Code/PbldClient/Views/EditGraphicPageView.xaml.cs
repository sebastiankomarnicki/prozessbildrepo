﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for EditGraphicPageView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IEditGraphicPageView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class EditGraphicPageView : UserControl, IEditGraphicPageView
    {
        public EditGraphicPageView()
        {
            InitializeComponent();
        }
    }
}
