﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for SchemaItemPropertiesView.xaml
    /// </summary>
    [Export(typeof(ISchemaItemPropertiesView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SchemaItemPropertiesView : UserControl, ISchemaItemPropertiesView
    {
        public SchemaItemPropertiesView()
        {
            InitializeComponent();
        }
    }
}
