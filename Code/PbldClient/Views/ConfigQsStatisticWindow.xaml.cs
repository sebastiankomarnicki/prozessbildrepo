﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for ConfigQsStatisticView.xaml
    /// </summary>
    ///
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IConfigQsStatisticView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class ConfigQsStatisticWindow : Window, IConfigQsStatisticView
    {
        protected static DependencyProperty InitialMinHeightProperty = DependencyProperty.Register(
            "InitialMinHeight", typeof(double), typeof(ConfigQsStatisticWindow)
            );

        protected static DependencyProperty InitialMinWidthProperty = DependencyProperty.Register(
            "InitialMinWidth", typeof(double), typeof(ConfigQsStatisticWindow)
            );

        public ConfigQsStatisticWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Window_Loaded);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitialMinHeight = this.ActualHeight;
            InitialMinWidth = this.ActualWidth;
        }

        protected double InitialMinHeight
        {
            get { return (Double)GetValue(InitialMinHeightProperty); }
            set { SetValue(InitialMinHeightProperty, value); }
        }

        protected double InitialMinWidth
        {
            get { return (Double)GetValue(InitialMinWidthProperty); }
            set { SetValue(InitialMinWidthProperty, value); }
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
