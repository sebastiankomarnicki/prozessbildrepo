﻿using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for PartOverviewView.xaml
    /// </summary>
    [Export(typeof(IPartOverviewView)), PartCreationPolicy(CreationPolicy.Shared)]
    public partial class PartOverviewView : UserControl, IPartOverviewView
    {
        public PartOverviewView()
        {
            InitializeComponent();
        }
    }
}
