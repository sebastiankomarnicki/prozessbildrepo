﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for PanelEditorView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IEditPanelView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class EditPanelView : UserControl, IEditPanelView
    {
        public EditPanelView()
        {
            InitializeComponent();
        }
    }
}
