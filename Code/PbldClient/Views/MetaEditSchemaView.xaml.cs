﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for MetaEditSchemaView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IMetaEditSchemaView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class MetaEditSchemaView : UserControl, IMetaEditSchemaView
    {
        public MetaEditSchemaView()
        {
            InitializeComponent();
        }
    }
}
