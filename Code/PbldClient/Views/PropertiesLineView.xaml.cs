﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for LinePropertiesView.xaml
    /// </summary>
    [Export(typeof(IPropertiesLineView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class PropertiesLineView : UserControl, IPropertiesLineView
    {
        public PropertiesLineView()
        {
            InitializeComponent();
        }
    }
}
