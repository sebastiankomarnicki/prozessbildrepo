﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Windows;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for AddLabelWindow.xaml
    /// </summary>
    [Export(typeof(ISelectQdasNumberView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SelectQdasNumberWindow : Window, ISelectQdasNumberView
    {
        public SelectQdasNumberWindow()
        {
            InitializeComponent();
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
