﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for MetaQsStatRequestView.xaml
    /// </summary>
    [Export(typeof(IMetaQsStatRequestView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class MetaQsStatRequestView : UserControl, IMetaQsStatRequestView
    {
        public MetaQsStatRequestView()
        {
            InitializeComponent();
        }
    }
}
