﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Windows;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for SetAreaWindow.xaml
    /// </summary>
    [Export(typeof(ISetAreaView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SetAreaWindow : Window, ISetAreaView
    {
        protected static DependencyProperty InitialMinHeightProperty = DependencyProperty.Register(
            "InitialMinHeight", typeof(double), typeof(SetAreaWindow)
            );

        protected static DependencyProperty InitialMinWidthProperty = DependencyProperty.Register(
            "InitialMinWidth", typeof(double), typeof(SetAreaWindow)
            );

        public SetAreaWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Window_Loaded);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitialMinHeight = this.ActualHeight;
            InitialMinWidth = this.ActualWidth;
        }

        protected double InitialMinHeight
        {
            get { return (Double)GetValue(InitialMinHeightProperty); }
            set { SetValue(InitialMinHeightProperty, value); }
        }

        protected double InitialMinWidth
        {
            get { return (Double)GetValue(InitialMinWidthProperty); }
            set { SetValue(InitialMinWidthProperty, value); }
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
