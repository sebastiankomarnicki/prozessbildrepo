﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for ExportAreaWindow.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IExportAreaView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class ExportAreaWindow : Window, IExportAreaView
    {
        protected static DependencyProperty InitialMinHeightProperty = DependencyProperty.Register(
            "InitialMinHeight", typeof(double), typeof(ExportAreaWindow)
            );

        protected static DependencyProperty InitialMinWidthProperty = DependencyProperty.Register(
            "InitialMinWidth", typeof(double), typeof(ExportAreaWindow)
            );

        public ExportAreaWindow()
        {
            this.Loaded += new RoutedEventHandler(Window_Loaded);
            InitializeComponent();           
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitialMinHeight = this.ActualHeight;
            InitialMinWidth = this.ActualWidth;
        }

        protected double InitialMinHeight
        {
            get { return (Double)GetValue(InitialMinHeightProperty); }
            set { SetValue(InitialMinHeightProperty, value); }
        }

        protected double InitialMinWidth
        {
            get { return (Double)GetValue(InitialMinWidthProperty); }
            set { SetValue(InitialMinWidthProperty, value); }
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
