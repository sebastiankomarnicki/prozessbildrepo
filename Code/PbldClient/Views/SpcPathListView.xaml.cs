﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for SpcPathListView.xaml
    /// </summary>
    [Export(typeof(ISpcPathListView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SpcPathListView : UserControl, ISpcPathListView
    {

        public SpcPathListView()
        {
            InitializeComponent();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {            
            if (spcList.SelectedItem is object)
            {
                spcList.ScrollIntoView(spcList.SelectedItem);
            }
        }
    }
}
