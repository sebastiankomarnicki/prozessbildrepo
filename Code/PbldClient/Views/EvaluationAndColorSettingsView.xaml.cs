﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for EvaluationAndColorSettingsView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IEvaluationAndColorSettingsView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class EvaluationAndColorSettingsView : UserControl, IEvaluationAndColorSettingsView
    {
        public EvaluationAndColorSettingsView()
        {
            InitializeComponent();
        }

    }
}
