﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for PropertiesFontAndColorView.xaml
    /// </summary>
    [Export(typeof(IPropertiesFontAndColorView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class PropertiesFontAndColorView : UserControl, IPropertiesFontAndColorView
    {
        public PropertiesFontAndColorView()
        {
            InitializeComponent();
        }
    }
}
