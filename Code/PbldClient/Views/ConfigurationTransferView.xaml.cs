﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for ConfigurationTransferView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(ITransferFollowerView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class ConfigurationTransferView : UserControl, ITransferFollowerView
    {
        public ConfigurationTransferView()
        {
            InitializeComponent();
        }
    }
}
