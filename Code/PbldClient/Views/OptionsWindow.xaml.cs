﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for OptionsWindow.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IOptionsView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class OptionsWindow : Window, IOptionsView
    {

        public OptionsWindow()
        {
            InitializeComponent();
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
