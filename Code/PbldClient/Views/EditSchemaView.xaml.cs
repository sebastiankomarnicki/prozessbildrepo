﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for EditSchemaView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IEditSchemaView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class EditSchemaView : UserControl, IEditSchemaView
    {
        public EditSchemaView()
        {
            InitializeComponent();
        }
    }
}
