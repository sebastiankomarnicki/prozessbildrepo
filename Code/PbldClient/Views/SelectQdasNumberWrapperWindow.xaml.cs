﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Windows;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for EditQdasNumberWrapperWindow.xaml
    /// </summary>
    [Export(typeof(ISelectQdasNumberWrapperView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SelectQdasNumberWrapperWindow : Window, ISelectQdasNumberWrapperView
    {
        public SelectQdasNumberWrapperWindow()
        {
            InitializeComponent();
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
