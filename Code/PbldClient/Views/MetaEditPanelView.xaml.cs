﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for MetaEditPanelView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IMetaEditPanelView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class MetaEditPanelView : UserControl, IMetaEditPanelView
    {
        public MetaEditPanelView()
        {
            InitializeComponent();
        }
    }
}
