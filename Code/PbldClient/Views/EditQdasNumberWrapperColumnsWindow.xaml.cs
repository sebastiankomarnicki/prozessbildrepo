﻿using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for EditQdasNumberWrapperColumnsWindow.xaml
    /// </summary>
    [Export(typeof(IEditQdasNumberWrapperColumnsView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class EditQdasNumberWrapperColumnsWindow : Window, IEditQdasNumberWrapperColumnsView
    {
        protected static DependencyProperty InitialMinHeightProperty = DependencyProperty.Register(
            "InitialMinHeight", typeof(double), typeof(EditQdasNumberWrapperColumnsWindow)
            );

        protected static DependencyProperty InitialMinWidthProperty = DependencyProperty.Register(
            "InitialMinWidth", typeof(double), typeof(EditQdasNumberWrapperColumnsWindow)
            );

        public EditQdasNumberWrapperColumnsWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Window_Loaded);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitialMinHeight = this.ActualHeight;
            InitialMinWidth = this.ActualWidth;
        }

        protected double InitialMinHeight
        {
            get { return (Double)GetValue(InitialMinHeightProperty); }
            set { SetValue(InitialMinHeightProperty, value); }
        }

        protected double InitialMinWidth
        {
            get { return (Double)GetValue(InitialMinWidthProperty); }
            set { SetValue(InitialMinWidthProperty, value); }
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
