﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for LinePropertiesView.xaml
    /// </summary>
    [Export(typeof(ILinePropertiesView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class LinePropertiesView : UserControl, ILinePropertiesView
    {
        public LinePropertiesView()
        {
            InitializeComponent();
        }
    }
}
