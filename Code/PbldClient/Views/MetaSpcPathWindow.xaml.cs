﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for MetaSpcPathWindow.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IMetaSpcPathView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class MetaSpcPathWindow : Window, IMetaSpcPathView
    {

        public MetaSpcPathWindow()
        {
            InitializeComponent();
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
