﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Windows;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for TextEvaluationTypeSelectionWindow.xaml
    /// </summary>
    [Export(typeof(ITextEvaluationTypeSelectionView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class TextEvaluationTypeSelectionWindow : Window, ITextEvaluationTypeSelectionView
    {
        protected static DependencyProperty InitialMinHeightProperty = DependencyProperty.Register(
            "InitialMinHeight", typeof(double), typeof(TextEvaluationTypeSelectionWindow)
            );

        protected static DependencyProperty InitialMinWidthProperty = DependencyProperty.Register(
            "InitialMinWidth", typeof(double), typeof(TextEvaluationTypeSelectionWindow)
            );

        public TextEvaluationTypeSelectionWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Window_Loaded);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitialMinHeight = this.ActualHeight;
            InitialMinWidth = this.ActualWidth;
        }

        protected double InitialMinHeight
        {
            get { return (Double)GetValue(InitialMinHeightProperty); }
            set { SetValue(InitialMinHeightProperty, value); }
        }

        protected double InitialMinWidth
        {
            get { return (Double)GetValue(InitialMinWidthProperty); }
            set { SetValue(InitialMinWidthProperty, value); }
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
