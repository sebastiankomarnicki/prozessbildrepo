﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for PropertiesRepresentationTextView.xaml
    /// </summary>
    [Export(typeof(IPropertiesRepresentationTextView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class PropertiesRepresentationTextView : UserControl, IPropertiesRepresentationTextView
    {
        public PropertiesRepresentationTextView()
        {
            InitializeComponent();
        }
    }
}
