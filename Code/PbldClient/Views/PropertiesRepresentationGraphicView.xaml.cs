﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for PropertiesRepresentationGraphicView.xaml
    /// </summary>
    [Export(typeof(IPropertiesRepresentationGraphicView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class PropertiesRepresentationGraphicView : UserControl, IPropertiesRepresentationGraphicView
    {
        public PropertiesRepresentationGraphicView()
        {
            InitializeComponent();
        }
    }
}
