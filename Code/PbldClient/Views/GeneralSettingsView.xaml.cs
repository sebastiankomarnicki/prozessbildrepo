﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for GeneralSettingsView.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(IGeneralSettingsView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class GeneralSettingsView : UserControl, IGeneralSettingsView
    {
        public GeneralSettingsView()
        {
            InitializeComponent();
        }
    }
}
