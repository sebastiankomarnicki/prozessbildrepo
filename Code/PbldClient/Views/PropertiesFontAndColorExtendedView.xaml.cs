﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for PropertiesFontAndColorExtendedView.xaml
    /// </summary>
    [Export(typeof(IPropertiesFontAndColorExtendedView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class PropertiesFontAndColorExtendedView : UserControl, IPropertiesFontAndColorExtendedView
    {
        public PropertiesFontAndColorExtendedView()
        {
            InitializeComponent();
        }
    }
}
