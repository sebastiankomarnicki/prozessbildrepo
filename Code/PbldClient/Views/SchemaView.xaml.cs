﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Windows.Controls;

namespace Promess.Pbld.Views
{
    /// <summary>
    /// Interaction logic for SchemaView.xaml
    /// </summary>
    [Export(typeof(ISchemaView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class SchemaView : UserControl, ISchemaView
    {
        public SchemaView()
        {
            InitializeComponent();
        }
    }
}
