﻿namespace Promess.Pbld.Behaviors
{
    /// <summary>
    /// Helper interface for selection state
    /// </summary>
    public interface ISupportIsSelected
    {
        /// <summary>
        /// Data to track the selection state of an item
        /// </summary>
        bool IsSelected { get; set; }
    }
}
