﻿using Promess.Common.Util;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;
using System.Windows.Media;

namespace Promess.Pbld.Behaviors
{
    /// <summary>
    /// Draggable item behavior
    /// </summary>
    public class DragOnCanvasBehavior : Behavior<ListBox>
    {

        /// <summary>
        /// Stores the mouse starting position
        /// </summary>
        private Point _lastMousePosition = new Point(0, 0);

        private FrameworkElement _draggedItem;

        public static readonly DependencyProperty InitMoveCommandProperty =
            DependencyProperty.Register(nameof(InitMoveCommand),
                typeof(ICommand),
                typeof(DragOnCanvasBehavior),
                null
                );

        public static readonly DependencyProperty MoveCommandProperty =
            DependencyProperty.Register(nameof(MoveCommand),
                typeof(DelegateCommand<Vector>),
                typeof(DragOnCanvasBehavior),
                null
                );

        public static readonly DependencyProperty ClearSelectionCommandProperty =
            DependencyProperty.Register(nameof(ClearSelectionCommand),
                typeof(DelegateCommand),
                typeof(DragOnCanvasBehavior),
                null
                );


        /// <summary>
        /// Gets or sets the draggable item
        /// </summary>
        public UIElement ParentItem { get; set; }

        /// <summary>
        /// Command to execute when movement begins
        /// </summary>
        public ICommand InitMoveCommand
        {
            get { return (ICommand)GetValue(InitMoveCommandProperty); }
            set { SetValue(InitMoveCommandProperty, value); }
        }

        /// <summary>
        /// Command to try to move selected item(s) by the given <c>Vector</c>
        /// </summary>
        public DelegateCommand<Vector> MoveCommand {
            get { return (DelegateCommand<Vector>)GetValue(MoveCommandProperty); }
            set { SetValue(MoveCommandProperty, value); }
        }

        /// <summary>
        /// Clear selected items
        /// </summary>
        public DelegateCommand ClearSelectionCommand
        {
            get { return (DelegateCommand)GetValue(ClearSelectionCommandProperty); }
            set { SetValue(ClearSelectionCommandProperty, value); }
        }

        /// <summary>
        /// Instantiates adorner controls and attaches mouse event
        /// </summary>
        protected override void OnAttached()
        {
            UIElement element = (UIElement)this.AssociatedObject;

            element.PreviewMouseLeftButtonDown += this.Element_PreviewMouseLeftButtonDown;
        }

        /// <summary>
        /// Detaches mouse event
        /// </summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();
            UIElement element = (UIElement)this.AssociatedObject;
            element.PreviewMouseLeftButtonDown -= this.Element_PreviewMouseLeftButtonDown;
        }

        /// <summary>
        /// Handles the mouse left button down event
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event</param>
        private void Element_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem possiblyFE = FindListBoxItem(e.OriginalSource);
            if (possiblyFE?.DataContext is ISupportIsSelected dragged && dragged is object)
            {
                if ((Keyboard.Modifiers & ModifierKeys.Control) != 0)
                {
                    dragged.IsSelected = !dragged.IsSelected;
                }
                else if (!dragged.IsSelected)
                {
                    ClearSelectionCommand.Execute(null);
                    dragged.IsSelected = true;
                }
                if (dragged.IsSelected)
                    OnMouseDownPreparationForStartDrag(possiblyFE, e);
                e.Handled = true;
            }
        }

        private ListBoxItem FindListBoxItem(object originalSource)
        {
            ListBoxItem result;
            FrameworkElement backup;
            FrameworkElement tmp = originalSource as FrameworkElement;
            while(tmp != null)
            {
                result = tmp as ListBoxItem;
                if (result != null)
                    return result;
                backup = tmp;
                tmp = VisualTreeHelper.GetParent(tmp) as FrameworkElement;
                if (tmp == null)
                    tmp = LogicalTreeHelper.GetParent(backup) as FrameworkElement;
            }
            return null;
        }

        /// <summary>
        /// Handles the mouse left button up event
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event</param>
        private void Element_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.OnStopDrag();
            this.AssociatedObject.PreviewMouseLeftButtonUp -= Element_PreviewMouseLeftButtonUp;
            this.AssociatedObject.MouseLeave -= Element_MouseLeave;
            e.Handled = true;
        }

        private void Element_MouseLeave(object sender, MouseEventArgs e)
        {
            this.OnStopDrag();
            this.AssociatedObject.PreviewMouseLeftButtonUp -= Element_PreviewMouseLeftButtonUp;
            this.AssociatedObject.MouseLeave -= Element_MouseLeave;
            e.Handled = true;
        }

        /// <summary>
        /// Handles the mouse move event when minimal mouse move distance is not yet reached
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event</param>
        private void Element_PreviewMouseMoveDragDistance(object sender, MouseEventArgs e)
        {
            Point mouseNewPos = e.GetPosition((UIElement)this.AssociatedObject);
            Vector movement = mouseNewPos - this._lastMousePosition;
            this._draggedItem?.CaptureMouse();
            if (movement.Length >= 5)
            {
                OnStartDrag();
            }
            e.Handled = true;
        }

        /// <summary>
        /// Handles the mouse move event when dragging is executed
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event</param>
        private void Element_PreviewMouseMoveDuringDrag(object sender, MouseEventArgs e)
        {
            this.OnDragging(e);
            e.Handled = true;
        }

        /// <summary>
        /// Drags the item with the mouse
        /// </summary>
        private void OnDragging(MouseEventArgs e)
        {
            if (!_draggedItem?.IsMouseCaptured??true)
            {
                return;
            }

            // calculate element movement
            Point mouseNewPos = e.GetPosition((UIElement)this.AssociatedObject);
            Vector movement = mouseNewPos - this._lastMousePosition;
            // make sure the mouse has moved since last time
            if (movement.Length >= 1)
            {
                // save current mouse position
                this._lastMousePosition = mouseNewPos;
                MoveCommand.Execute(movement);
            }
            e.Handled = true;
        }

        private void OnMouseDownPreparationForStartDrag(FrameworkElement fe, MouseButtonEventArgs e)
        {
            var element = (UIElement)this.AssociatedObject;
            element.Focus();
            this._lastMousePosition = e.GetPosition(this.AssociatedObject);
            this._draggedItem = fe;
            WeakEventManager<FrameworkElement, MouseEventArgs>.AddHandler(this._draggedItem, nameof(FrameworkElement.PreviewMouseMove), Element_PreviewMouseMoveDragDistance);
            this.AssociatedObject.PreviewMouseLeftButtonUp += this.Element_PreviewMouseLeftButtonUp;
            this.AssociatedObject.MouseLeave += Element_MouseLeave;
            //do not capture mouse here or double click cannot be evaluated
        }

        /// <summary>
        /// Starts the item drag
        /// </summary>
        private void OnStartDrag()
        {
            //var element = (UIElement)this.AssociatedObject;
            //element.Focus();
            InitMoveCommand?.Execute(null);
            if (this._draggedItem is object)
            {
                WeakEventManager<FrameworkElement, MouseEventArgs>.RemoveHandler(this._draggedItem, nameof(FrameworkElement.PreviewMouseMove), Element_PreviewMouseMoveDragDistance);
                WeakEventManager<FrameworkElement, MouseEventArgs>.AddHandler(this._draggedItem, nameof(FrameworkElement.PreviewMouseMove), Element_PreviewMouseMoveDuringDrag);
            }
        }

        /// <summary>
        /// Stops the item drag
        /// </summary>
        private void OnStopDrag()
        {
            if (_draggedItem is object)
            {
                WeakEventManager<FrameworkElement, MouseEventArgs>.RemoveHandler(this._draggedItem, nameof(FrameworkElement.PreviewMouseMove), Element_PreviewMouseMoveDragDistance);
                WeakEventManager<FrameworkElement, MouseEventArgs>.RemoveHandler(this._draggedItem, nameof(FrameworkElement.PreviewMouseMove), Element_PreviewMouseMoveDuringDrag);
                this._draggedItem.ReleaseMouseCapture();
                this._draggedItem = null;
            }
        }
    }
}
