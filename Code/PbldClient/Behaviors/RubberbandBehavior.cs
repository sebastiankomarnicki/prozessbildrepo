﻿using Promess.Pbld.Adorners;
using System.Windows.Controls;
using System.Windows.Documents;
using Microsoft.Xaml.Behaviors;

namespace Promess.Pbld.Behaviors
{
    /// <summary>
    /// original code: https://www.codeproject.com/Articles/404827/Rubberband-Behavior-for-WPF-ListBox
    /// Behavior to attach a <see cref="RubberBandAdorner"/> amd facilitate rubber band selection
    /// </summary>
    public class RubberBandBehavior : Behavior<ListBox>
    {
        protected override void OnAttached()
        {
            AssociatedObject.Loaded += new System.Windows.RoutedEventHandler(AssociatedObject_Loaded);
            base.OnAttached();
        }
        void AssociatedObject_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            RubberBandAdorner band = new RubberBandAdorner(AssociatedObject);
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(AssociatedObject);
            adornerLayer.Add(band);
        }
        protected override void OnDetaching()
        {
            AssociatedObject.Loaded -= new System.Windows.RoutedEventHandler(AssociatedObject_Loaded);
            base.OnDetaching();
        }
    }
}
