﻿using System.Waf.Applications;

namespace Promess.Pbld.DesignData
{
    /// <summary>
    /// General mock for a view providing only a datacontext
    /// </summary>
    public class MockView : IView
    {
        public object DataContext { get; set; }
    }
}
