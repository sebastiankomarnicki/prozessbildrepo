﻿using System;
using Promess.Pbld.IViews;
using Promess.Pbld.ViewModels;

namespace Promess.Pbld.DesignData
{
    /// <summary>
    /// Sample view model for a <see cref="ITextInputView"/> for design time visualization
    /// </summary>
    public class SampleTextInputViewModel : TextInputViewModel
    {
        public SampleTextInputViewModel() : base(new MockTextInputView())
        {
            Title = "SampleTitle";
            Header = "SampleHeader";
            Input = "SampleInput";
        }

        internal class MockTextInputView : MockView, ITextInputView
        {
            public void Close()
            {
                throw new NotImplementedException();
            }

            public void ShowDialog(object owner)
            {
                throw new NotImplementedException();
            }
        }
    }
}
