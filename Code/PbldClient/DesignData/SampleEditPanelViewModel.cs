﻿using Promess.Pbld.IViews;
using Promess.Pbld.Data.Editor;
using System.Windows.Media;
using Promess.Pbld.ViewModels;
using Promess.Pbld.Services;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.Data.GraphicPage;

namespace Promess.Pbld.DesignData
{
    /// <summary>
    /// Sample view model for a <see cref="IEditPanelView"/> for design time visualization
    /// </summary>
    public class SampleEditPanelViewModel : EditPanelViewModel
    {
        public SampleEditPanelViewModel() : base(new MockEditPanelView(),new MockConfigurationService(), null, null, null, null,null, null,null)
        {
            PanelItemModel sampleData = new PanelItemModel("SamplePanelData");
            ImageEditorItem imageItem;
            sampleData.Items.Add(new TextEditorItem()
            {
                PercentageX = 0.35,
                PercentageY = 0.35,
                PercentageWidth = 0.07,
                PercentageHeight = 0.07,
                Text = "bfgggtfzzgla",
                BackColor = Colors.AliceBlue,
                FontColor = Colors.Salmon,
                Border = BorderType.Raised,
                ScaleFont = true
            });
            sampleData.Items.Add(new ButtonEditorItem() { PercentageX = 0.49, PercentageY = 0.49, PercentageWidth = 0.07, PercentageHeight = 0.07, Text = "retrtrttretertrtgggggggggggggggggggggggggggggggla" });
            imageItem = new ImageEditorItem() { PercentageX = 0.7, PercentageY = 0.56, PercentageWidth = 0.07, PercentageHeight = 0.07};
            //string imagePath = "pack://application:,,,/Common;component/Resources/Icons/PromessLogoWBg.png";
            //imageItem.SetImage(imagePath, ImageHelper.GetImageFromPathAsByteArray(imagePath));
            //sampleData.Items.Add(imageItem);
            imageItem = new ImageEditorItem() { PercentageX = 0.56, PercentageY = 0.56, PercentageWidth = 0.07, PercentageHeight = 0.07, EvaluationCode=3000};
            sampleData.Items.Add(imageItem);
            Initialize(sampleData);
            ViewHeight = 700;
            ViewWidth = 800;
            SetXYDimensions();
        }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Type can be moved to separate file")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Type name does not correspond to file name")]
    internal class MockEditPanelView : MockView, IEditPanelView
    {
    }

    internal class MockConfigurationService : IConfigurationService
    {
        public string CurrentAreaName => "MockArea";
        public GraphicProjectWrapper GraphicProjectWrapper => null;

        public PresetGraphicSettings GetPresetGraphicSettingsCopy()
        {
            return new PresetGraphicSettings();
        }
    }
}
