﻿using System;
using System.Collections.Generic;
using Promess.Pbld.IViews;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.ViewModels;
using Promess.QsStat.Data;
using Promess.QsStat.Services;

namespace Promess.Pbld.DesignData
{
    /// <summary>
    /// Sample view model for a <see cref="ITextPropertiesView"/> for design time visualization
    /// </summary>
    public class SampleTextboxPropertiesViewModel : TextEditorItemViewModel
    {
        public SampleTextboxPropertiesViewModel() : base(new MockTextBoxPropertiesView(), null, null, null, null)
        {
            var item = new TextEditorItem();
            item.HorizontalAlignment = HorizontalAlignment.Center;
            item.QdasNumber = new QdasNumber(TextEvaluationType.Part, 1001);
            item.TextType = TextType.Qdas;
            this.Setup(null, item);
        }

        private class MockTextBoxPropertiesView : MockView, ITextPropertiesView
        {
        }

        private class MockQsStatService : IQsStatService
        {
            private IReadOnlyDictionary<int, string> _kFields;

            public MockQsStatService()
            {
                var kFields = new Dictionary<int, string>();
                kFields.Add(1001, "Text von 1001");
                this._kFields = kFields;
            }

            public QsStatState QsStatState { get { return QsStatState.Started; } }

            public IReadOnlyDictionary<int, string> PartKFields
            {
                get
                {
                    return _kFields;
                }
            }

            public IReadOnlyDictionary<int, string> StatResult
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public IReadOnlyDictionary<int, string> Graphics
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public IReadOnlyDictionary<int, string> CharacteristicKFields
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public QsStatVersion QCOMVersion => throw new NotImplementedException();
        }
    }
}
