﻿using Promess.Common.Settings;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.Services;
using System.ComponentModel.Composition;
using System.Waf.Applications.Services;
using System.Configuration;
using System.Windows.Media;

namespace Promess.Pbld.Utility
{
    /// <summary>
    /// Customization of a <see cref="ModifiedConfigurationManager"/> adjusted for process picture
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.Shared)]
    public class ModifiedConfigurationManagerPbld:ModifiedConfigurationManager
    {
        #region properties
        private IMessageService _messageService;
        private PathsAndResourceService _pathResourceService;
        #endregion

        [ImportingConstructor]
        public ModifiedConfigurationManagerPbld(IMessageService messageService, PathsAndResourceService pathsAndResourceService)
        {
            this._messageService = messageService;
            this._pathResourceService = pathsAndResourceService;
        }

        #region init
        public void Initialize()
        {
            this.Configuration = GetConfigurationFromPath(_pathResourceService.ConfigPath);
            if (PopulateEvaluationColorSettingsIfNecessary())
                Save(ConfigurationSaveMode.Modified);
        }

        /// <summary>
        /// Create default <see cref="EvaluationAndColorSettings"/> if they are not in the configuration
        /// </summary>
        /// <returns>Whether the settings were created</returns>
        private bool PopulateEvaluationColorSettingsIfNecessary()
        {
            bool created = false;
            var evaluationColorSettings = GetSectionAndAddIfMissing<EvaluationAndColorSettings>(Constants.EvaluationAndColor, out created);
            if (created) {

                evaluationColorSettings.ProcessOk = new EvaluationAndColorElement() {
                    BackgroundColor = Colors.LightGreen,
                    TreeColor = Colors.LightGreen,
                    BoldInTree = true,
                    IconElement = new IconElement(true, "face03_green.png")
                };
                evaluationColorSettings.ProcessEndangered = new EvaluationAndColorElement() {
                    BackgroundColor = Colors.Yellow,
                    TreeColor = Colors.Yellow,
                    BoldInTree = true,
                    IconElement = new IconElement(true, "face01.png")  
                };
                evaluationColorSettings.ProcessOoC = new EvaluationAndColorElement() {
                    BackgroundColor = Colors.Red,
                    TreeColor = Colors.Red,
                    BoldInTree = true,
                    IconElement = new IconElement(true, "face01_red.png")
                };
                evaluationColorSettings.StabilityViolation = new EvaluationAndColorElement() {
                    BackgroundColor = Colors.Violet,
                    TreeColor = Colors.Violet,
                    IconElement = new IconElement(true, "face01_red.png")
                };
                evaluationColorSettings.ActionLimitViolationXq = new EvaluationAndColorElement()
                {
                    BackgroundColor = Colors.Salmon,
                    TreeColor = Colors.Salmon,
                    FontColor = Colors.DarkOliveGreen,
                    IconElement = new IconElement(true, "face01_red.png")
                };
                evaluationColorSettings.ActionLimitViolationS = new EvaluationAndColorElement()
                {
                    BackgroundColor = Colors.Salmon,
                    TreeColor = Colors.Salmon,
                    FontColor = Colors.CornflowerBlue,
                    IconElement = new IconElement(true, "face01_red.png")
                };
                evaluationColorSettings.DeficientPart = new EvaluationAndColorElement()
                {
                    BackgroundColor = Colors.Red,
                    TreeColor = Colors.Red,
                    BoldInTree = true,
                    IconElement = new IconElement(true, "face01_red.png")
                };
                evaluationColorSettings.NoOrTooFewValues = new EvaluationAndColorElement() {
                    BackgroundColor = Colors.Gray,
                    TreeColor = Colors.SlateBlue,
                    IsRequired = true,
                    IconElement = new IconElement(true, "face01_grey.png")
                };
            } else if(!evaluationColorSettings.NoOrTooFewValues.IsRequired || !evaluationColorSettings.NoOrTooFewValues.IsEnabled)
            {
                evaluationColorSettings.NoOrTooFewValues.IsRequired = true;
                evaluationColorSettings.NoOrTooFewValues.IsEnabled = true;
                created = true;
            }
            return created;
        }
        #endregion
    }
}
