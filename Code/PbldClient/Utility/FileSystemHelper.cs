﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;

namespace Promess.Pbld.Utility
{
    internal class FileSystemHelper
    {
        /// <summary>
        /// Tries to get all leaf directories with <param name="root"/> as root directory.
        /// </summary>
        /// <param name="root">The root directory</param>
        /// <returns>true and list of leaf directories on success, otherwise false and an empty collection</returns>
        /// <exception cref="Exception">Problem occurred when accessing the directory</exception>
        public static (bool success, List<string> leafs) GetLeafDirectories(string root)
        {
            try
            {
                return (true, Directory.EnumerateDirectories(root, "*.*", SearchOption.AllDirectories)
                .Where(dir => IsLeafDirectory(dir))
                .ToList());
            }
            catch
            {
                return (false, new List<string>());
            }
        }

        /// <summary>
        /// Check if the given directory has no subdirectories
        /// </summary>
        /// <param name="directory">Directory to check</param>
        /// <returns></returns>
        /// <exception cref="Exception">Problem occurred when accessing the directory</exception>
        public static bool IsLeafDirectory(string directory)
        {
            return !Directory.EnumerateDirectories(directory, "*.*", SearchOption.TopDirectoryOnly).Any();
        }
    }
}
