﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Media.Imaging;

namespace Promess.Pbld.Utility
{
    public class ImageHelper
    {
        /// <summary>
        /// Tries to load the image under path as bitmap. Throws exception on error.
        /// </summary>
        /// <param name="path">path to an image</param>
        /// <returns>The image</returns>
        public static BitmapImage GetImageFromPath(string path)
        {
            var uri = new Uri(path);
            var bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = uri;
            bitmap.CacheOption = BitmapCacheOption.OnDemand;
            bitmap.EndInit();
            return bitmap;
        }

        public static byte[] GetImageFromPathAsByteArray(string path)
        {
            var image = Image.FromFile(path);
            using (var ms = new MemoryStream())
            {
                image.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                return ms.ToArray();
            }
        }
    }
}
