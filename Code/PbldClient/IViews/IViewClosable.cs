﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promess.Pbld.IViews
{
    public interface IViewClosable
    {
        void ShowDialog(object owner);

        void Close();
    }
}
