﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;

namespace Promess.Pbld.IViews
{
    public interface IMainView:IView
    {
        event CancelEventHandler Closing;

        event EventHandler Closed;

        void Show();
        void Close();
    }
}
