﻿using Promess.Pbld.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Foundation;
using System.Windows;
using System.Windows.Input;

namespace Promess.Pbld.Services
{
    [Export(typeof(IMainService)), Export]
    public class MainService: Model, IMainService
    {
        private object _mainView;
        private WindowState _windowState;

        private string _currentUserName;
        private bool _isAuthenticated;
        private ICommand _showLoginViewCommand;
        private ICommand _showCreateNewUserViewCommand;
        private ICommand _logoutCommand;

        /// <summary>
        /// Main view of the application as parent view when opening dialogs and no other parents are available.
        /// Notifies on Change
        /// </summary>
        public object MainView
        {
            get { return _mainView; }
            internal set { SetProperty(ref _mainView, value); }
        }

        /// <summary>
        /// Window state of the application, notifies on change
        /// </summary>
        public WindowState WindowState
        {
            get {return _windowState; }
            set { SetProperty(ref _windowState, value); }
        }

        /// <inheritdoc/>
        public OperatingMode OperatingMode
        {
            get;internal set;
        }

        /// <inheritdoc/>
        public String CurrentUserName
        {
            get { return _currentUserName; }
            set { SetProperty(ref _currentUserName, value); }
        }

        /// <inheritdoc/>
        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
            set { SetProperty(ref _isAuthenticated, value); }
        }

        /// <inheritdoc/>
        public ICommand ShowLoginViewCommand
        {
            get { return _showLoginViewCommand; }
            set { SetProperty(ref _showLoginViewCommand, value); }
        }

        /// <inheritdoc/>
        public ICommand ShowCreateNewUserViewCommand
        {
            get { return _showCreateNewUserViewCommand; }
            set { SetProperty(ref _showCreateNewUserViewCommand, value); }
        }

        /// <inheritdoc/>
        public ICommand LogoutCommand
        {
            get { return _logoutCommand; }
            set { SetProperty(ref _logoutCommand, value); }
        }

        public void Restart()
        {
            Application.Current.Exit += delegate (object sender, ExitEventArgs e)
            { 
                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location, String.Join(" ", Environment.GetCommandLineArgs().Skip(1)));
            };
            Application.Current.Shutdown();
        }
    }
}
