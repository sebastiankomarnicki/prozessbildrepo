﻿using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.Data.Settings;
using System;

namespace Promess.Pbld.Services
{
    public interface IConfigurationService
    {
        /// <summary>
        /// Name of the currently selected area
        /// </summary>
        String CurrentAreaName { get; }
        
        /// <summary>
        /// Get currently loaded graphic project or a dummy if no project is loaded
        /// </summary>
        GraphicProjectWrapper GraphicProjectWrapper { get; }

        /// <summary>
        /// Get a copy of the current presets for graphical elements
        /// </summary>
        /// <returns>The presets</returns>
        PresetGraphicSettings GetPresetGraphicSettingsCopy();
    }
}
