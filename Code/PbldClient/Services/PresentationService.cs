﻿using Promess.Language;
using System.ComponentModel.Composition;
using System.Globalization;

namespace Promess.Pbld.Services
{
    /// <summary>
    /// Service influencing the representation of the application. Currently only setting up the language data retrieval.
    /// </summary>
    [Export(typeof(IPresentationService))]
    public class PresentationService:IPresentationService
    {
        [ImportingConstructor]
        public PresentationService()
        {
        }

        public void Initialize()
        {
            var resourceAssembly = typeof(PresentationService).Assembly;
            var xmlTranslationProvider = new XmlTranslationProvider("Promess.Pbld.LanguageData", resourceAssembly, CultureInfo.CurrentCulture);
            LanguageManager.Current.TranslationProvider = xmlTranslationProvider;
        }
    }
}
