﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Promess.Pbld.Data.Settings;

namespace Promess.Pbld.Services
{
    /// <summary>
    /// Service to provide generall access to resource and user icons. User icons not yet implemented.
    /// </summary>
    [Export(typeof(IconLookupService)), PartCreationPolicy(CreationPolicy.Shared)]
    public class IconLookupService
    {
        #region fields
        private PathsAndResourceService _pathResources;

        private Dictionary<string, ImageSource> _resourceIcons;
        private Dictionary<string, ImageSource> _userIcons;
        #endregion

        [ImportingConstructor]
        public IconLookupService(PathsAndResourceService pathResources):base()
        {
            this._pathResources = pathResources;

            this._resourceIcons = new Dictionary<string, ImageSource>();
            this._userIcons = new Dictionary<string, ImageSource>(); 
        }

        internal Tuple<bool, ImageSource> TryGetImageSource(IconElement elem)
        {
            ImageSource result = null;
            if (elem.IsResourceIcon)
            {
                if (_resourceIcons.TryGetValue(elem.IconName, out result))
                {
                    return new Tuple<bool, ImageSource>(true, result);
                }
                else
                {
                    return new Tuple<bool, ImageSource>(false, null);
                }
            }
            else
            {
                if (_userIcons.TryGetValue(elem.IconName, out result))
                {
                    return new Tuple<bool, ImageSource>(true, result);
                }
                else
                {
                    return new Tuple<bool, ImageSource>(false, null);
                }
            }
        }

        #region initialization
        public void Initialize()
        {
            var assembly = Assembly.GetExecutingAssembly();
            string[] embeddedfiles = { "face01.png", "face01_grey.png", "face01_light_red.png", "face01_red.png",
                "face02.png", "face03.png", "face03_green.png", "face04.png", "face05.png"
            };
            foreach (var embeddedFile in embeddedfiles)
            {
                _resourceIcons[embeddedFile] = new BitmapImage(new Uri($"pack://application:,,,/Pbld;component/Resources/Icons/{embeddedFile}"));
            }
        }
        #endregion

        public List<IconElement> GetDefinedImageElements()
        {
            var result = new List<IconElement>();
            foreach (var elem in _resourceIcons)
            {
                result.Add(new IconElement(true, elem.Key));
            }
            foreach (var elem in _userIcons)
            {
                result.Add(new IconElement(false, elem.Key));
            }
            return result;
        }
    }
}
