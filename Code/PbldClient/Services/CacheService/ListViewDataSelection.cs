﻿using Promess.Pbld.Data;
using Promess.Pbld.ViewModels;
using System;
using System.Diagnostics;

namespace Promess.Pbld.Services.CacheService
{
    /// <summary>
    /// Wrapper for saving a selection for <see cref="ListViewModel"/>
    /// </summary>
    internal class ListViewDataSelection
    {
        internal ListViewDataSelection()
        {
            ResetSelection();
        }

        internal Guid? SelectedSpcPathGuid
        {
            get; private set;          
        }

        internal Guid? SelectedDataSetGuid
        {
            get; private set;
        }

        internal int? SelectedCharacteristicIndex
        {
            get; private set;
        }

        internal string SelectedReportDefFile
        {
            get; private set;
        }

        internal int? SelectedPage
        {
            get; private set;
        }

        internal void ResetSelection()
        {
            this.SelectedSpcPathGuid = null;
            this.SelectedDataSetGuid = null;
            this.SelectedCharacteristicIndex = null;
            this.SelectedReportDefFile = null;
            this.SelectedPage = null;
        }

        internal void Select(SpcPath spcPath)
        {
            this.SelectedSpcPathGuid = spcPath.Guid;
            this.SelectedDataSetGuid = null;
            this.SelectedCharacteristicIndex = null;
            this.SelectedReportDefFile = null;
            this.SelectedPage = null;
        }

        internal void Select(SpcPath spcPath, Guid dataSetId)
        {
            this.SelectedSpcPathGuid = spcPath.Guid;
            this.SelectedDataSetGuid = dataSetId;
            this.SelectedCharacteristicIndex = null;
            this.SelectedReportDefFile = null;
            this.SelectedPage = null;
        }

        internal void Select(SpcPath spcPath, Guid dataSetId, int characteristicIndex, string reportDefFile, int pageNumber)
        {
            this.SelectedSpcPathGuid = spcPath.Guid;
            this.SelectedDataSetGuid = dataSetId;
            this.SelectedCharacteristicIndex = characteristicIndex;
            this.SelectedReportDefFile = reportDefFile;
            this.SelectedPage = pageNumber;
        }

        /// <summary>
        /// Update selected report and page, only valid if a characteristic is currently selected
        /// </summary>
        /// <param name="reportDefFile">.def file of the selected report</param>
        /// <param name="pageNumber">Selected page of the report</param>
        internal void UpdateCharacteristicSelection(string reportDefFile, int pageNumber)
        {
            Debug.Assert(this.SelectedSpcPathGuid.HasValue && this.SelectedDataSetGuid.HasValue && this.SelectedCharacteristicIndex.HasValue, "Characteristic must be selected to save data for the selection.");
            this.SelectedReportDefFile = reportDefFile;
            this.SelectedPage = pageNumber;
        }
    }
}
