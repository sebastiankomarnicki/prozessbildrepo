﻿using System;

namespace Promess.Pbld.Services.CacheService
{
    /// <summary>
    /// Wrapper for selecting a specific data set by id, and characteristic by index
    /// </summary>
    internal class DataSelection
    {
        private Guid _dataSetId;
        private int _characteristicIndex;

        public DataSelection(Guid dataSetId, int characteristicIndex)
        {
            this._dataSetId = dataSetId;
            this._characteristicIndex = characteristicIndex;
        }

        public Guid DataSetId
        {
            get { return _dataSetId; }
        }

        public int CharacteristicIndex
        {
            get { return _characteristicIndex; } 
        }

    }
}
