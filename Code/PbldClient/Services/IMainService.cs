﻿using Promess.Pbld.Data;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Promess.Pbld.Services
{
    /// <summary>
    /// Interface providing general information about the application and for accessing injected funtionality.
    /// </summary>
    public interface IMainService:INotifyPropertyChanged
    {
        /// <summary>
        /// Main view of the application as parent view when opening dialogs and no other parents are available
        /// </summary>
        object MainView { get; }

        /// <summary>
        /// Window state of the application
        /// </summary>
        WindowState WindowState { get; }

        /// <summary>
        /// Operating mode of the application for en-/disabling the corresponding views
        /// </summary>
        OperatingMode OperatingMode { get; }

        void Restart();

        #region authentication
        /// <summary>
        /// Name of the current user, when logged in
        /// </summary>
        String CurrentUserName { get; set; }

        /// <summary>
        /// Is some user authenticated?
        /// </summary>
        bool IsAuthenticated { get; set; }

        /// <summary>
        /// Command for logging in
        /// </summary>
        ICommand ShowLoginViewCommand { get; set; }

        /// <summary>
        /// Command for creating a new user when logged in
        /// </summary>
        ICommand ShowCreateNewUserViewCommand { get; set; }

        /// <summary>
        /// Command for logging out
        /// </summary>
        ICommand LogoutCommand { get; set; }
       #endregion
    }
}
