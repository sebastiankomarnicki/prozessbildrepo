﻿using Promess.Pbld.Data;
using Promess.Pbld.Services.CacheService;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promess.Pbld.Services
{
    /// <summary>
    /// Service for caching data selections. Currently only required for the list view. 
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.Shared)]
    public class SelectedDataCacheService
    {
        private ListViewDataSelection _listViewDataSelection;

        [ImportingConstructor]
        public SelectedDataCacheService() {
            this._listViewDataSelection = new ListViewDataSelection();
        }

        internal ListViewDataSelection ListViewDataSelection
        {
            get { return this._listViewDataSelection; }
        }
    }
}
