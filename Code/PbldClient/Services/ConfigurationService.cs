﻿using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.Data.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO.Compression;
using System.Linq;
using System.Waf.Applications.Services;
using System.Waf.Foundation;

namespace Promess.Pbld.Services
{
    /// <summary>
    /// Service for providing the current overall configuration.
    /// </summary>
    [Export(typeof(IConfigurationService)), Export, PartCreationPolicy(CreationPolicy.Shared)]
    public class ConfigurationService:Model, IConfigurationService
    {
        #region fields
        private readonly PathsAndResourceService _pathResources;
        private readonly IMessageService _messageService;

        private Dictionary<string, string> _panelNameFilenameLookup;
        private Dictionary<string, PanelItemModel> _namePanelItemModelLookup;
        private Areas _areas;
        private GraphicProjectWrapper _graphicProjectWrapper;
        private ListViewSettings _listViewSettings;
        private EvaluationAndColorSettings _evaluationAndColorSettings;
        private GeneralSettings _generalSettings;
        private TransferFollowerSettings _transferFollowerSettings;
        private QsStatSettings _qsStatSettings;
        private LanguageSettings _languageSettings;
        private PresetGraphicSettings _presetGraphicSettings;
        #endregion

        #region events
        public event EventHandler ListViewSettingsChanged;
        public event EventHandler AreasChanged;
        public event EventHandler PanelsChanged;
        #endregion

        [ImportingConstructor]
        public ConfigurationService(PathsAndResourceService pathResources, IMessageService messageService) : base()
        {
            this._pathResources = pathResources;
            this._messageService = messageService;
        }

        #region initialization
        /// <summary>
        /// Initialization of the service. Tries to load not provided settings and processes provided setting for i.e. the language.
        /// Loading may present dialogs to the user.
        /// </summary>
        /// <param name="generalSettings">General settings</param>
        /// <param name="evalColorSettings">Evaluation and color settings</param>
        /// <param name="qsStatSettings">Settings for qsStat</param>
        /// <param name="graphicPageSettings">Settings for graphic pages</param>
        /// <param name="transferFollowerSettings">Settings for the leader/follower mode</param>
        /// <param name="languageSettings">Language</param>
        /// <returns>success of loading additional settings and processing of provided settings</returns>
        public bool Initialize(GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings,
            QsStatSettings qsStatSettings, GraphicPageSettings graphicPageSettings, TransferFollowerSettings transferFollowerSettings,
            LanguageSettings languageSettings)
        {
            this.GeneralSettings = generalSettings;
            this.EvaluationAndColorSettings = evalColorSettings;
            this.QsStatSettings = qsStatSettings;
            this.TransferFollowerSettings = transferFollowerSettings;
            this.LanguageSettings = languageSettings;

            //TODO better to throw the exceptions and handle outside?
            var initFuncs = new Func<bool>[] { () => LoadLanguage(languageSettings), LoadDefinedPanels, LoadAreas,
                LoadListViewSettings,()=> LoadGraphicProject(graphicPageSettings), LoadPresetGraphics };
            foreach (var func in initFuncs)
            {
                if (!func())
                    return false;
            }
            return true;
        }

        private bool LoadLanguage(LanguageSettings languageSettings)
        {
            //this is always fine, even if the language is not available. otherwise change.
            LanguageManager.Current.TryChangeLanguage(languageSettings.Culture);
            return true;
        }

        private bool LoadGraphicProject(GraphicPageSettings graphicPageSettings)
        {
            if (String.IsNullOrWhiteSpace(graphicPageSettings.ProjectPath))
            {
                this._graphicProjectWrapper = new GraphicProjectWrapper(graphicPageSettings.ProjectPath, null);
                return true;
            }
            try
            {
                var maybeNullGraphicProject = SerializeHelper.DeserializeFromFile<GraphicProject>(graphicPageSettings.ProjectPath, true);
                this._graphicProjectWrapper = new GraphicProjectWrapper(graphicPageSettings.ProjectPath, maybeNullGraphicProject);
                return true;
            } catch
            {
                string messagePart = (string)LanguageManager.Current.CurrentLanguageData.Translate("QGraphicProjectInvalid");
                if (_messageService.ShowYesNoQuestion(String.Format(messagePart, graphicPageSettings.ProjectPath)))
                {
                    this._graphicProjectWrapper = new GraphicProjectWrapper(graphicPageSettings.ProjectPath, null);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private bool LoadAreas()
        {
            try
            {
                //provide information about invalid area?
                this._areas = SerializeHelper.DeserializeFromFile<Areas>(_pathResources.AreasPath) ?? new Areas();
                if (this._areas.AvailableAreas.Any(area => !area.Validate()))
                {
                    if (_messageService.ShowYesNoQuestion((string)LanguageManager.Current.CurrentLanguageData.Translate("QDefinedAreasConfigInvalid")))
                    {
                        this._areas = new Areas();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                if (_messageService.ShowYesNoQuestion((string)LanguageManager.Current.CurrentLanguageData.Translate("QDefinedAreasConfigInvalid")))
                {
                    this._areas = new Areas();
                    return true;
                }else
                {
                    return false;
                }
            }
        }

        private bool LoadPresetGraphics()
        {
            try
            {
                this._presetGraphicSettings = SerializeHelper.DeserializeFromFile<PresetGraphicSettings>(this._pathResources.PresetGraphicsSettingsPath) ?? new PresetGraphicSettings();
                return true;
            }
            catch
            {
                if (_messageService.ShowYesNoQuestion((string)LanguageManager.Current.CurrentLanguageData.Translate("QPresetGraphicInvalid")))
                {
                    this._presetGraphicSettings = new PresetGraphicSettings();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private bool LoadListViewSettings()
        {
            try
            {
                this._listViewSettings = SerializeHelper.DeserializeFromFile<ListViewSettings>(_pathResources.ListViewSettingsPath) ?? new ListViewSettings();
                return true;
            }
            catch
            {
                if (_messageService.ShowYesNoQuestion((string)LanguageManager.Current.CurrentLanguageData.Translate("QListViewSettingsConfigInvalid")))
                {
                    this._listViewSettings = new ListViewSettings();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        internal bool LoadDefinedPanels()
        {
            _namePanelItemModelLookup = new Dictionary<string, PanelItemModel>();
            _panelNameFilenameLookup = new Dictionary<string, string>();
            try
            {
                //need to use Update because otherwise stream.Length is not implemented
                using (var archive = ZipFile.Open(_pathResources.Panels,ZipArchiveMode.Update))
                {
                    PanelItemModel currentPanel;
                    foreach (var entry in archive.Entries)
                    {
                        using (var stream = entry.Open())
                        {
                            currentPanel = SerializeHelper.DeserializeFromStream<PanelItemModel>(stream);
                            _namePanelItemModelLookup[currentPanel.Name]=currentPanel;
                            _panelNameFilenameLookup[currentPanel.Name] = entry.Name;
                        }
                    }
                    PanelsChanged?.Invoke(this, EventArgs.Empty);
                    return true;
                }
            }
            catch (Exception) {
                //FIXME use some logging service for all exceptions thrown here? or maybe use IMessageService and always display the error when a switch is set?
                return false;
            }
        }
        #endregion

        /// <inheritdoc/>
        public string CurrentAreaName
        {
            get { return _areas?.SelectedArea?.Name; }
        }

        /// <summary>
        /// Retrieves a copy of the currently selected <see cref="Area"/>
        /// </summary>
        /// <returns>Selection, <c>null</c> if nothing selected</returns>
        public Area GetCurrentAreaCopyOrNull()
        {
            Area currentArea = _areas?.SelectedArea;
            if (currentArea == null)
            {
                return null;
            }else
            {
                return SerializeHelper.CreateDataContractCopy(currentArea);
            }
        }

        public GeneralSettings GeneralSettings
        {
            get { return _generalSettings; }
            internal set { SetProperty(ref _generalSettings, value); }
        }

        public TransferFollowerSettings TransferFollowerSettings
        {
            get { return _transferFollowerSettings; }
            internal set { SetProperty(ref _transferFollowerSettings, value); }
        }

        public GraphicProjectWrapper GraphicProjectWrapper
        {
            get { return _graphicProjectWrapper; }
            set { SetProperty(ref _graphicProjectWrapper, value); }
        }

        public EvaluationAndColorSettings EvaluationAndColorSettings
        {
            get { return _evaluationAndColorSettings; }
            internal set { SetProperty(ref _evaluationAndColorSettings, value); }
        }

        public QsStatSettings QsStatSettings
        {
            get { return _qsStatSettings; }
            internal set { SetProperty(ref _qsStatSettings, value); }
        }

        public LanguageSettings LanguageSettings
        {
            get { return _languageSettings; }
            internal set { SetProperty(ref _languageSettings, value); }
        }

        /// <summary>
        /// Retrieves copies of defined panels
        /// </summary>
        /// <returns>Tuple of retrieval success and panel copies</returns>
        public Tuple<bool,List<PanelItemModel>> GetPanelsCopy()
        {
            List<PanelItemModel> copies = new List<PanelItemModel>();
            try
            {
                foreach (var elem in _namePanelItemModelLookup.Values)
                {
                    copies.Add(SerializeHelper.CreateDataContractCopy(elem));
                }
                return new Tuple<bool, List<PanelItemModel>>(true, copies);
            }
            catch (Exception)
            {
                return new Tuple<bool, List<PanelItemModel>>(false, null);
            }
        }

        /// <summary>
        /// Get saved panels
        /// </summary>
        /// <returns>Saved panels</returns>
        internal IReadOnlyList<PanelItemModel> GetPanels()
        {
            return _namePanelItemModelLookup.Values.ToList();
        }

        /// <summary>
        /// Retrieve a panel copy by name
        /// </summary>
        /// <param name="panelName">Name of the panel to retrieve</param>
        /// <returns><c>true</c> and panel copy on success, otherwise <c>false</c> and undefined.</returns>
        public Tuple<bool, PanelItemModel> GetSavedPanelCopy(string panelName)
        {
            if (_namePanelItemModelLookup.TryGetValue(panelName, out PanelItemModel retrieved))
            {
                try
                {
                    var copy = SerializeHelper.CreateDataContractCopy(retrieved);
                    return new Tuple<bool, PanelItemModel>(true, copy);
                }
                catch (Exception)
                {
                    return new Tuple<bool, PanelItemModel>(false, null);
                }
            }else
            {
                return new Tuple<bool, PanelItemModel>(false, null);
            }
        }

        /// <summary>
        /// <see cref="GetSavedPanelCopy(string)"/>, just with a <see cref="PanelItemModel"/> for retrieval
        /// </summary>
        public Tuple<bool, PanelItemModel> GetSavedPanelCopy(PanelItemModel toRetrieve)
        {
            return GetSavedPanelCopy(toRetrieve.Name);
        }

        public ListViewSettings GetListViewSettingsCopy()
        {
            return SerializeHelper.CreateDataContractCopy(_listViewSettings);
        }

        /// <summary>
        /// Replace saved <see cref="ListViewSettings"/> and raise <see cref="ListViewSettingsChanged"/> on change to inform subscribers
        /// </summary>
        /// <param name="newListViewSettings">New settings</param>
        /// <returns>Success of the replacement</returns>
        public bool TrySetListViewSettings(ListViewSettings newListViewSettings)
        {
            try
            {
                this._listViewSettings = SerializeHelper.CreateDataContractCopy(newListViewSettings);
                SerializeHelper.SerializeToFile(_pathResources.ListViewSettingsPath, this._listViewSettings);
                this.ListViewSettingsChanged?.Invoke(this, EventArgs.Empty);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Areas GetAreasCopy()
        {
            return SerializeHelper.CreateDataContractCopy(_areas);
        }

        /// <summary>
        /// Replace saved <see cref="Areas"/> and raise <see cref="AreasChanged"/> on change to inform subscribers
        /// </summary>
        /// <param name="newAreas">New areas</param>
        /// <returns>Success of the replacement</returns>
        public bool TrySetAreas(Areas newAreas)
        {
            try
            {
                this._areas = SerializeHelper.CreateDataContractCopy(newAreas);
                SerializeHelper.SerializeToFile(_pathResources.AreasPath, _areas);              
                this.AreasChanged?.Invoke(this, EventArgs.Empty);
                RaisePropertyChanged(nameof(CurrentAreaName));
                return true;
            }catch
            {
                return false;
            }
        }

        public PresetGraphicSettings GetPresetGraphicSettingsCopy()
        {
            return SerializeHelper.CreateDataContractCopy(this._presetGraphicSettings);
        }

        /// <summary>
        /// Replace saved <see cref="PresetGraphicSettings"/>. No update event.
        /// </summary>
        /// <param name="newPresets">New presets</param>
        /// <returns>Success of the replacement</returns>
        public bool TrySetPresetGraphicSettings(PresetGraphicSettings newPresets)
        {
            try
            {
                this._presetGraphicSettings = SerializeHelper.CreateDataContractCopy(newPresets);
                SerializeHelper.SerializeToFile(_pathResources.PresetGraphicsSettingsPath, this._presetGraphicSettings);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Creates a new panel in the archive and returns a deep copy for use if the
        /// name is not already in use. Raise <see cref="PanelsChanged"/> to inform subscribers of change.
        /// </summary>
        /// <param name="name">The name of the new panel</param>
        /// <param name="toCopyFrom">Panel with items that should be deep copied to the new panel</param>
        /// <returns>Tuple with value false and undefined if the name is already present,
        /// otherwise true and deep copy of the added element.</returns>
        public Tuple<bool, PanelItemModel> CreateNewAndSave(string name, PanelItemModel toCopyFrom=null)
        {
            if (IsDuplicateName(name))
                return new Tuple<bool, PanelItemModel>(false, null);
            var newModel = new PanelItemModel(name);
            try
            {
                if (toCopyFrom != null)
                    toCopyFrom.DeepCopyItemsInto(newModel);
                //creating the serialization before saving so no state change occurs in case this fails for whatever reason
                var toReturnModel = SerializeHelper.CreateDataContractCopy(newModel);
                SaveNewPanel(newModel);
                newModel.ResetDirty();
                PanelsChanged?.Invoke(this, EventArgs.Empty);
                return new Tuple<bool, PanelItemModel>(true, toReturnModel);
            }
            catch(Exception)
            {
                return new Tuple<bool, PanelItemModel>(false, null);
            }
        }

        private bool IsDuplicateName(string name)
        {
           return _namePanelItemModelLookup.Keys.Any(x => String.Equals(x, name, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Check if the given <see cref="PanelItemModel"/> is used by any <see cref="Area"/>
        /// </summary>
        /// <param name="panel">Panel to check</param>
        /// <returns>In use state</returns>
        public bool PanelInUse(PanelItemModel panel)
        {
            return _areas.AvailableAreas.Any((area) => IsInUse(area, panel));
        }

        private bool IsInUse(Area area, PanelItemModel data)
        {
            return area.ReadonlyPaths.Any(path => String.Equals(path.PanelConfigurationName, data.Name, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Tries to remove a <see cref="PanelItemModel"/>. Item must not be in use. Raise <see cref="PanelsChanged"/> to inform subscribers of change.
        /// </summary>
        /// <param name="panel">Item to remove</param>
        /// <returns>Success of removal</returns>
        public bool TryDeletePanel(PanelItemModel panel)
        {
            if (!_namePanelItemModelLookup.ContainsKey(panel.Name) || PanelInUse(panel))
                return false;
            try
            {
                using (var archive = ZipFile.Open(_pathResources.Panels, ZipArchiveMode.Update))
                {
                    string entryName = _panelNameFilenameLookup[panel.Name];
                    archive.GetEntry(entryName).Delete();
                }
                _namePanelItemModelLookup.Remove(panel.Name);
                _panelNameFilenameLookup.Remove(panel.Name);
                PanelsChanged?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Tries to save the given item. Raise <see cref="PanelsChanged"/> to inform subscribers of change.
        /// </summary>
        /// <param name="panel">Item to save</param>
        /// <returns>Success of operation</returns>
        public bool TrySavePanel(PanelItemModel panel)
        {
            if (!_namePanelItemModelLookup.ContainsKey(panel.Name))
                return false;
            try { 
                var toAdd = SerializeHelper.CreateDataContractCopy(panel);
                using (var archive = ZipFile.Open(_pathResources.Panels, ZipArchiveMode.Update))
                {
                    string entryName = _panelNameFilenameLookup[panel.Name];
                    archive.GetEntry(entryName).Delete();
                    var entry = archive.CreateEntry(entryName);
                    using (var stream = entry.Open())
                    {
                        SerializeHelper.SerializeToStream(stream, toAdd);
                    }
                }
                panel.ResetDirty();
                _namePanelItemModelLookup[panel.Name] = toAdd;
                PanelsChanged?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private void SaveNewPanel(PanelItemModel toAdd)
        {
            string filename;
            do
            {
                filename = $"{Guid.NewGuid().ToString()}.xml";
            } while (_panelNameFilenameLookup.ContainsValue(filename));
            using (var archive = ZipFile.Open(_pathResources.Panels, ZipArchiveMode.Update))
            {
                var entry = archive.CreateEntry(filename);

                using (var stream = entry.Open())
                {
                    SerializeHelper.SerializeToStream(stream, toAdd);
                }
            }
            toAdd.ResetDirty();
            _namePanelItemModelLookup[toAdd.Name] = toAdd;
            _panelNameFilenameLookup[toAdd.Name] = filename; 
        }
    }
}
