﻿using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.Utility;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications.Services;
using System.Windows;

namespace Promess.Pbld.Services
{
    /// <summary>
    /// Service for processing new settings when program is run in mode <see cref="OperatingMode.follower"/>
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.Shared)]
    public class FollowerService:IDisposable
    {
        private const int CHECK_DELAY = 60000;
        private const int ERROR_DELAY = 100;
        private const int INVALID_SETTINGS_DELAY = int.MaxValue;

        private readonly IMainService _mainService;
        private readonly IMessageService _messageService;
        private readonly ModifiedConfigurationManagerPbld _configurationManager;
        private readonly QsStatService _qsStatService;
        private readonly PathsAndResourceService _pathsAndResourceService;
        private readonly ConfigurationService _configurationService;
    
        private SemaphoreSlim _semaphore;
        private TaskCompletionSource<bool> _completeWait;
        private CancellationTokenSource _endServiceCts;
        private string _watchPath;
        private TransferFollowerSettings _tfSettings;
        private FileSystemWatcher _watcher;

        private CancellationTokenSource _pathCts;

        [ImportingConstructor]
        public FollowerService(IMainService mainService, IMessageService messageService, 
            QsStatService qsStatService, PathsAndResourceService pathsAndResourceService, ConfigurationService configurationService,
            ModifiedConfigurationManagerPbld configurationManager)
        {
            this._mainService = mainService;
            this._messageService = messageService;
            this._qsStatService = qsStatService;
            this._pathsAndResourceService = pathsAndResourceService;
            this._configurationService = configurationService;
            this._configurationManager = configurationManager;
            this._watcher = new FileSystemWatcher() { Filter = "PbldExport.zip" };
            this._watcher.Created += this.ImportFileCreatedEventHandler;
            //watcher.Renamed does not capture root folder movement when NotifyFilters.DirectoryName is set
            this._watcher.NotifyFilter = NotifyFilters.FileName;
            this._semaphore = new SemaphoreSlim(1);
            this._endServiceCts = new CancellationTokenSource();
            this._pathCts = new CancellationTokenSource();
            this._watchPath = String.Empty;
            this._completeWait = new TaskCompletionSource<bool>();
        }

        /// <summary>
        /// Start service and initialize with current setting. Register for setting changes.
        /// </summary>
        public void Start()
        {
            //this wait will block ui, do not put long running code inside if possible
            InitializeForFollowerSettingsAsync().Wait();
            PropertyChangedEventManager.AddHandler(this._configurationService, TransferFollowerSettingsChangedHandler, nameof(ConfigurationService.TransferFollowerSettings));
            Task.Factory.StartNew(async ()=> await RunAsync(this._endServiceCts.Token),TaskCreationOptions.LongRunning);
        }

        private async Task InitializeForFollowerSettingsAsync()
        {
            await this._semaphore.WaitAsync(this._endServiceCts.Token);
            this._completeWait.TrySetResult(true);
            this._pathCts?.Cancel();
            this._pathCts = new CancellationTokenSource();
            this._watcher.EnableRaisingEvents = false;
            this._tfSettings = this._configurationService.TransferFollowerSettings;
            try
            {
                if (!String.IsNullOrWhiteSpace(this._tfSettings.TransferPath))
                {
                    string watchPath = this._tfSettings.TransferPath;
                    if (this._tfSettings.AddUserNameToTransferPath)
                        watchPath = Path.Combine(watchPath, Environment.UserName);
                    Path.GetFullPath(watchPath);//just to check if path is valid, will throw an exception when invalid
                    this._watchPath = watchPath;
                    try
                    {
                        //Watcher only as support. Root folder move is not recognized as well as temporary access loss to folder which would require restarting
                        this._watcher.EnableRaisingEvents = false;
                        this._watcher.Path = watchPath;
                        this._watcher.EnableRaisingEvents = true;
                    }
                    catch { }
                }
            }
            catch {
                this._watchPath = String.Empty;
            }
            this._semaphore.Release();
        }

        private async void TransferFollowerSettingsChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            await InitializeForFollowerSettingsAsync().ConfigureAwait(false);
        }

        private void ImportFileCreatedEventHandler(object sender, FileSystemEventArgs e)
        {
            ContinueRunLoop();
        }

        private void ContinueRunLoop()
        {
            this._completeWait.TrySetResult(true);
        }

        /// <summary>
        /// Periodically check for new settings to import and import when required.
        /// </summary>
        /// <param name="ct">Cancellation for the service</param>
        /// <returns>Long running task of this check</returns>
        public async Task RunAsync(CancellationToken ct)
        {
            string path;
            CancellationToken pathCt;
            int delay;
            TaskCompletionSource<bool> tcs;
            try
            {
                while (!ct.IsCancellationRequested)
                {
                    await this._semaphore.WaitAsync(ct).ConfigureAwait(false);
                    path = this._watchPath;
                    pathCt = this._pathCts.Token;
                    var tfSettings = this._tfSettings;
                    //note: if setting the result uses the semaphore checking Task.IsCompleted can be added to not always create new tcs
                    tcs = new TaskCompletionSource<bool>();
                    this._completeWait = tcs;
                    this._semaphore.Release();
                    try
                    {
                        if (String.IsNullOrWhiteSpace(path))
                        {
                            delay = INVALID_SETTINGS_DELAY;
                        }
                        else
                        {
                            delay = CHECK_DELAY;
                            string file = Path.Combine(path, "PbldExport.zip");
                            if (File.Exists(file))
                            {
                                if (DispatchHelper.Invoke(() => this._messageService.ShowYesNoQuestion(this._mainService.MainView, (string)LanguageManager.Current.CurrentLanguageData.Translate("QImportSettings"))))
                                {
                                    await ImportFileAsync(file, tfSettings, pathCt).ConfigureAwait(false);
                                    return;
                                }
                                File.Delete(file);
                            }                            
                        }                       
                    }
                    catch(OperationCanceledException)
                    {
                        continue;
                    }
                    catch
                    {
                        delay = ERROR_DELAY;
                    }
                    try
                    {
                        await Task.WhenAny(Task.Delay(TimeSpan.FromMilliseconds(delay), pathCt), tcs.Task).ConfigureAwait(false);
                    }
                    catch { }                        
                }
            }
            catch (OperationCanceledException) { }
        }

        /// <summary>
        /// Read and import a file with new settings
        /// </summary>
        /// <param name="file">File with the new settings</param>
        /// <param name="tfSettings">Settings for importing</param>
        /// <param name="ct">Cancellation</param>
        /// <returns><see cref="Task"/> for the operation</returns>
        private async Task ImportFileAsync(string file, TransferFollowerSettings tfSettings, CancellationToken ct)
        {
            await this._semaphore.WaitAsync(ct);
            ZipArchive toImport = null;
            try
            {
                ZipArchiveEntry tmpEntry;
                int index;
                ExportSettings exportSettings;
                Area area;
                ZipArchiveEntry panelEntry;
                List<ZipArchiveEntry> graphicPages;
                ZipArchiveEntry backgroundEntry = null;
                GeneralSettings generalSettings = null;
                EvaluationAndColorSettings evaluationAndColorSettings = null;
                ListViewSettings listViewSettings = null;
                QsStatSettings qsStatSettings = null;
                              
                while (File.Exists(file))
                {
                    ct.ThrowIfCancellationRequested();
                    try
                    {
                        toImport = ZipFile.OpenRead(file);
                        break;
                    }
                    catch (IOException)
                    {
                        await Task.Delay(ERROR_DELAY);
                    }
                }
                if (toImport == null)
                    return;

                var remainingEntries = toImport.Entries.ToList();

                //meta settings and panel must exists, exception should be thrown on invalid
                index = remainingEntries.FindIndex(entry => entry.Name.Equals("ExportSettings.xml"));
                tmpEntry = remainingEntries.ElementAt(index);
                remainingEntries.RemoveAt(index);
                using (var stream = tmpEntry.Open())
                {
                    exportSettings = SerializeHelper.DeserializeFromStream<ExportSettings>(stream);
                }

                index = remainingEntries.FindIndex(entry => entry.Name.Equals(exportSettings.AreaFile));
                tmpEntry = remainingEntries.ElementAt(index);
                remainingEntries.RemoveAt(index);
                using (var stream = tmpEntry.Open())
                {
                    area = SerializeHelper.DeserializeFromStream<Area>(stream);
                }

                index = remainingEntries.FindIndex(entry => entry.Name.Equals(exportSettings.PanelsFile));
                panelEntry = remainingEntries.ElementAt(index);
                remainingEntries.RemoveAt(index);

                if (!String.IsNullOrWhiteSpace(exportSettings.BackgroundImageFile))
                {
                    index = remainingEntries.FindIndex(entry => entry.Name.Equals(exportSettings.BackgroundImageFile));
                    backgroundEntry = remainingEntries.ElementAt(index);
                    remainingEntries.RemoveAt(index);
                }

                if (!String.IsNullOrWhiteSpace(exportSettings.EvalAndColorSettingsFile))
                {
                    index = remainingEntries.FindIndex(entry => entry.Name.Equals(exportSettings.EvalAndColorSettingsFile));
                    tmpEntry = remainingEntries.ElementAt(index);
                    using (var stream = tmpEntry.Open())
                    {
                        evaluationAndColorSettings = SerializeHelper.DeserializeFromStream<EvaluationAndColorSettings>(stream);
                    }
                }

                if (!String.IsNullOrWhiteSpace(exportSettings.GeneralSettingsFile))
                {
                    index = remainingEntries.FindIndex(entry => entry.Name.Equals(exportSettings.GeneralSettingsFile));
                    tmpEntry = remainingEntries.ElementAt(index);
                    using (var stream = tmpEntry.Open())
                    {
                        generalSettings = SerializeHelper.DeserializeFromStream<GeneralSettings>(stream);
                    }
                }

                if (!String.IsNullOrWhiteSpace(exportSettings.ListViewSettingsFile))
                {
                    index = remainingEntries.FindIndex(entry => entry.Name.Equals(exportSettings.ListViewSettingsFile));
                    tmpEntry = remainingEntries.ElementAt(index);
                    using (var stream = tmpEntry.Open())
                    {
                        listViewSettings = SerializeHelper.DeserializeFromStream<ListViewSettings>(stream);
                    }
                }

                if (!String.IsNullOrWhiteSpace(exportSettings.QsStatSettingsFile))
                {
                    index = remainingEntries.FindIndex(entry => entry.Name.Equals(exportSettings.QsStatSettingsFile));
                    tmpEntry = remainingEntries.ElementAt(index);
                    using (var stream = tmpEntry.Open())
                    {
                        qsStatSettings = SerializeHelper.DeserializeFromStream<QsStatSettings>(stream);
                    }
                }

                graphicPages = remainingEntries.FindAll(entry => exportSettings.GraphicPageFiles.Contains(entry.Name));
                //do not cancel after applying permanent changes unless rollback is implemented
                ct.ThrowIfCancellationRequested();

                string graphicBasePath = String.IsNullOrWhiteSpace(tfSettings.GraphicProjectTransferPath) ?
                    this._pathsAndResourceService.GraphicProjectPath : tfSettings.GraphicProjectTransferPath;
                string destination=String.Empty;
                //copy pages, ask if replace
                string currentGpFn = this._configurationService.GraphicProjectWrapper.ProjectPath;
                bool requiresReload = false;
                var failureMessages = new List<string>();

                foreach (var graphicEntry in graphicPages)
                {
                    destination = Path.Combine(graphicBasePath, graphicEntry.Name);
                    if (!File.Exists(destination) || !tfSettings.OverwriteGraphicPages)
                    {
                        if (!DispatchHelper.Invoke(()=>this._messageService.ShowYesNoQuestion(this._mainService.MainView,
                            String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("QReplaceGraphicProject"), destination))))
                            continue;
                        if (string.Equals(destination, currentGpFn, StringComparison.OrdinalIgnoreCase))
                            requiresReload = true;
                        try
                        {
                            graphicEntry.ExtractToFile(destination, true);
                        }
                        catch
                        {
                            failureMessages.Add(String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("ESaveGraphicPage"), destination));
                        }                      
                    }
                }
                
                GraphicProject replacedGraphicProject = null;
                try
                {
                    if (graphicPages.Count == 1)
                    {
                        currentGpFn = Path.Combine(graphicBasePath, graphicPages[0].Name);
                        replacedGraphicProject = SerializeHelper.DeserializeFromFile<GraphicProject>(currentGpFn);
                    }
                    else if (requiresReload)
                    {
                        replacedGraphicProject = SerializeHelper.DeserializeFromFile<GraphicProject>(currentGpFn);
                    }
                }
                catch
                {
                    failureMessages.Add(String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("ELoadGraphicPage"), currentGpFn));
                }
                //copy panel
                panelEntry.ExtractToFile(this._pathsAndResourceService.Panels, true);
                
                if (replacedGraphicProject != null)
                {
                    var pageSettings = this._configurationManager.GetSectionAndAddIfMissing<GraphicPageSettings>(Constants.GraphicProject);
                    pageSettings.ProjectPath = currentGpFn;
                    this._configurationManager.Save(ConfigurationSaveMode.Modified);
                }

                if (qsStatSettings != null)
                {
                    var currentQsStatSettings = this._configurationManager.GetSectionAndAddIfMissing<QsStatSettings>(Constants.QsStat);
                    qsStatSettings.CopyDataOnly(currentQsStatSettings);
                    this._configurationManager.Save(ConfigurationSaveMode.Modified);
                }
                //do all the updates in the main thread
                DispatchHelper.Invoke(() => DoUpdates(area, backgroundEntry, generalSettings, evaluationAndColorSettings, listViewSettings, failureMessages));     
            }
            catch { }
            finally
            {
                toImport?.Dispose();
                try
                {
                    File.Delete(file);
                }
                catch { }
                this._semaphore.Release();
            }            
        }

        /// <summary>
        /// Update settings with retrieved data. Display failure messages, if any.
        /// </summary>
        /// <param name="area">Area</param>
        /// <param name="backgroundEntry">Schema background image</param>
        /// <param name="generalSettings">General settings</param>
        /// <param name="evaluationAndColorSettings">Evaluation and color setting</param>
        /// <param name="listViewSettings">Settings for the list view</param>
        /// <param name="failureMessages">Failes messages before this call</param>
        private void DoUpdates(Area area, ZipArchiveEntry backgroundEntry, GeneralSettings generalSettings, EvaluationAndColorSettings evaluationAndColorSettings, ListViewSettings listViewSettings, List<string> failureMessages)
        {
            if (backgroundEntry != null)
            {
                string destination = Path.Combine(this._pathsAndResourceService.PicturePath, backgroundEntry.Name);
                backgroundEntry.ExtractToFile(destination, true);
                area.SchemaFilePath = destination;
            }

            if (listViewSettings != null)
            {
                if (!this._configurationService.TrySetListViewSettings(listViewSettings))
                {
                    failureMessages.Add((string)LanguageManager.Current.CurrentLanguageData.Translate("EListViewSettingSaveFailed"));
                }
            }

            Areas areas = new Areas();
            areas.TryAddArea(area);
            areas.SelectArea(area);
            if (!this._configurationService.TrySetAreas(areas))
            {
                failureMessages.Add((string)LanguageManager.Current.CurrentLanguageData.Translate("ESetAreasFailed"));
            }
            if (failureMessages.Any())
            {
                //FIXME put these messages in some other container
                this._messageService.ShowError(this._mainService.MainView, String.Join("\n", failureMessages));
            }
            this._mainService.Restart();
        }


        #region IDisposable
        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this._disposed)
                return;

            if (disposing)
            {
                this._endServiceCts?.Cancel();
                this._endServiceCts.Dispose();
                this._endServiceCts = null;

                if (this._watcher!=null)
                    this._watcher.EnableRaisingEvents = false;

                this._pathCts?.Cancel();
                this._pathCts?.Dispose();
                this._pathCts = null;

                this._semaphore.Wait();
                this._semaphore.Dispose();
                this._semaphore = null;

                this._watcher?.Dispose();
                this._watcher = null;
            }

            this._disposed = true;
        }

        #endregion
    }
}
