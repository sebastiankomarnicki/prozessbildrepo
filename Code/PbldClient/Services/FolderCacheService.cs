﻿using System.ComponentModel.Composition;

namespace Promess.Pbld.Services
{
    /// <summary>
    /// Cache for last selected folders by the user
    /// </summary>
    [Export(typeof(IFolderCacheService)), Export, PartCreationPolicy(CreationPolicy.Shared)]
    public class FolderCacheService : IFolderCacheService
    {
        public string SpcPath { get; set; }
        public string TransferPath { get; set; }
        public string GraphicProjectTransferPath { get; set; }
        public string ExportFolder { get; set; }
    }
}
