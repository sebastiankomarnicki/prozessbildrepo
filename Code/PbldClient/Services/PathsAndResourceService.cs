﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promess.Pbld.Services
{
    [Export, PartCreationPolicy(CreationPolicy.Shared)]
    public class PathsAndResourceService
    {
        #region fields
        private const string PANELFILENAME = "Panels.zip";
        private const string GRAPHICPROJECTSAMPLEFILENAME = "GraphicProjectSample.gprj";

        private string _relativePathName;

        private string _panels;
        private string _areas;
        private string _listViewSettings;
        private string _users;
        private string _graphicPresets;
        #endregion

        #region initialization
        public void Initialize()
        {
            InitializeFoldersFromRegistry();
            GenerateMissingFolders();
            PersistTemplateFiles();
        }

        private void PersistTemplateFiles()
        {
            string sampleFolder = Path.Combine(AppContext.BaseDirectory, "SampleData");
            try
            {
                if (!File.Exists(this.Panels))
                    File.Copy(Path.Combine(sampleFolder, PANELFILENAME), Panels);
                if (!File.Exists(SampleGraphicProjectPath))
                    File.Copy(Path.Combine(sampleFolder, GRAPHICPROJECTSAMPLEFILENAME), SampleGraphicProjectPath);
            }
            catch { }
        }

        private void InitializeFoldersFromRegistry()
        {
            
            using (RegistryKey rkBase = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32))
            {        
                using (RegistryKey promessBase = rkBase.OpenSubKey(@"Software\Promess GmbH", false))
                {
                    _relativePathName = GetRelativePathName(promessBase);
                    if (promessBase == null)
                    {
                        SetOldStructurePaths();
                    }
                    else
                    {
                        string basePath = promessBase.GetValue("SpcSystemDataPath", "") as string;
                        if (string.IsNullOrEmpty(basePath))
                            SetOldStructurePaths();
                        else
                            SetNewStructurePaths(basePath, promessBase);
                    }
                }
            }           
        }

        private string GetRelativePathName(RegistryKey promessBase)
        {
            const string defaultName = "Pbld";
            using (RegistryKey relativePaths = promessBase?.OpenSubKey(@"RelativePaths", false))
            {
                if (relativePaths == null)
                    return defaultName;

                string tmp = relativePaths.GetValue("Pbld Module", "") as string;
                if (String.IsNullOrEmpty(tmp))
                    return defaultName;
                else
                    return tmp;
            }
        }

        private void SetOldStructurePaths()
        {
            string tmp = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            BasePath = Path.Combine(tmp, "Promess GmbH", _relativePathName);
            ConfigPath = Path.Combine(BasePath, "Config");
            TempPath = Path.Combine(BasePath, "Temp");
            //userworkspath only really exists in SPCData, same as base folder in appdata structure
            UserWorksPath = BasePath;
            GraphicProjectPath = Path.Combine(BasePath, "GraphicProject");
            PicturePath = Path.Combine(BasePath, "Pictures");
        }

        private void SetNewStructurePaths(string basePath, RegistryKey promessKey)
        {          
            if (!basePath.EndsWith(Path.DirectorySeparatorChar + "") && !basePath.EndsWith(Path.AltDirectorySeparatorChar + ""))
            {
                basePath += Path.DirectorySeparatorChar;
            }
            BasePath = basePath;
            ConfigPath = Path.Combine(BasePath, "Configurations", this._relativePathName);
            TempPath = Path.Combine(BasePath, "TmpFiles", this._relativePathName);
            UserWorksPath = Path.Combine(BasePath, "UserWorks", this._relativePathName);
            GraphicProjectPath = Path.Combine(UserWorksPath, "GraphicProject");
            PicturePath = Path.Combine(UserWorksPath, "Pictures");
        }

        public void GenerateMissingFolders()
        {
            string[] paths = { BasePath, ConfigPath, UserWorksPath, PicturePath, TempPath, GraphicProjectPath };
            foreach (var path in paths)
            {
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
            }
        }
        #endregion

        #region properties
        public string BasePath { get; private set; }

        public string UserWorksPath { get; private set; }

        public string ConfigPath { get; private set; }

        public string PicturePath { get; private set; }

        public string TempPath { get; private set; }

        public string GraphicProjectPath { get; private set; }

        public string Panels
        {
            get
            {
                if (_panels == null)
                    _panels = Path.Combine(ConfigPath, PANELFILENAME);
                return _panels;
            }
        }

        public string AreasPath
        {
            get
            {
                if (_areas == null)
                    _areas = Path.Combine(ConfigPath, "Areas.xml");
                return _areas;
            }
        }

        public string ListViewSettingsPath
        {
            get
            {
                if (_listViewSettings == null)
                    _listViewSettings = Path.Combine(ConfigPath, "ListViewSettings.xml");
                return _listViewSettings;
            }
        }

        public string PresetGraphicsSettingsPath
        {
            get
            {
                if (_graphicPresets == null)
                    _graphicPresets = Path.Combine(ConfigPath, "PresetGraphicsSettings.xml");
                return _graphicPresets;
            }
        }

        public string Users
        {
            get
            {
                if (_users == null)
                {
                    _users = Path.Combine(ConfigPath, "Users.xml");
                }
                return _users;
            }
        }

        internal string SampleGraphicProjectPath
        {
            get { return Path.Combine(GraphicProjectPath, GRAPHICPROJECTSAMPLEFILENAME); }
        }
        #endregion
    }
}
