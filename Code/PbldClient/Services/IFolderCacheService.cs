﻿namespace Promess.Pbld.Services
{
    /// <summary>
    /// Cache for last selected folders by the user
    /// </summary>
    public interface IFolderCacheService
    {
        string SpcPath { get; set; }
        string TransferPath { get; set; }
        string GraphicProjectTransferPath { get; set; }
        string ExportFolder { get; set; }
    }
}
