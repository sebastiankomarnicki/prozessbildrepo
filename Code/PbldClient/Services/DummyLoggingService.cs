﻿using Promess.Common.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promess.Pbld.Services
{
    /// <summary>
    /// Dummy for a logging service. Replace with real logging once required. As of 2021.02.10 only used by authentication.
    /// Most likely will need async methods once implemented.
    /// </summary>
    [Export(typeof(ILoggingService)), Export]
    public class DummyLoggingService : ILoggingService
    {
        public void ClearLogEntries()
        {
            //noop
        }

        public void Error(string message, Exception e)
        {
            //noop
        }

        public void Info(string message)
        {
            //noop
        }

        public void Warning(string message)
        {
            //noop
        }
    }
}
