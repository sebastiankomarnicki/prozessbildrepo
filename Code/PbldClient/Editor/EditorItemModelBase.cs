﻿using System.Waf.Applications;
using System.Waf.Foundation;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Behaviors;

namespace Promess.Pbld.Editor
{
    /// <summary>
    /// Base for all editor models. Defines shift, parent element for dimensions and grid as well as wrapper for the actual item.
    /// </summary>
    public abstract class EditorItemModelBase:Model, ISupportIsSelected
    {
        /// <summary>
        /// Smallest shift for which values are considered equal
        /// </summary>
        public const double EPSILON_SHIFT = 0.000001;

        private bool _isSelected;
        internal IDimensionsWithGrid _parent;
        private EditorItemBase _item;

        public EditorItemModelBase(IDimensionsWithGrid parent, EditorItemBase item)
        {
            this._isSelected = false;
            this._parent = parent;
            this._item = item;
        }

        /// <summary>
        /// Selection state of this item, notifies about changes
        /// </summary>
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        /// <summary>
        /// Wrapped item, notifies about changes
        /// </summary>
        public virtual EditorItemBase Item
        {
            get { return _item; }
            protected set
            {
                SetProperty(ref _item, value);
            }
        }

        /// <summary>
        /// Command for displaying the settings of the wrapped item
        /// </summary>
        public abstract DelegateCommand ShowSettingsCommand { get; }

        /// <summary>
        /// Checks if the <paramref name="actualShift"/> and <paramref name="desiredShift"/> have the same direction, and that for their absolute values the latter is smaller or that both are within <see cref="EPSILON_SHIFT"/>.
        /// </summary>
        /// <param name="actualShift">The shift that will be performed.</param>
        /// <param name="desiredShift">The desired shift.</param>
        /// <returns>True if <paramref name="actualShift"/> is valid, otherwise false.</returns>
        protected bool IsActualShiftValidGivenDesired(double actualShift, double desiredShift)
        {
            if (actualShift >= 0 && desiredShift >= 0)
                return actualShift - desiredShift <= EPSILON_SHIFT;
            else if (actualShift < 0 && desiredShift < 0)
                return desiredShift - actualShift <= EPSILON_SHIFT;
            return false;
        }
    }
}
