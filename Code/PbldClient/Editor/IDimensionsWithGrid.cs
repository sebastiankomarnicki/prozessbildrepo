﻿namespace Promess.Pbld.Editor
{
    /// <summary>
    /// Extends <see cref="IDimensions"/> with grid functionality.
    /// </summary>
    public interface IDimensionsWithGrid:IDimensions
    {
        GridSnap GridSnap { get; }
        GridPointDensity? GridPointDensity { get; }
    }
}
