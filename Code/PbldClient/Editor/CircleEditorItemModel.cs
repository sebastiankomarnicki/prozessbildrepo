﻿using System;
using Promess.Pbld.ViewModels;
using Promess.Pbld.Data.Editor;
using Promess.Common.Util;
using System.Windows;
using System.Waf.Applications;

namespace Promess.Pbld.Editor
{
    /// <summary>
    /// Editor model for a circle to allow manipulation. Is only really a circle when aspect ratio is locked
    /// </summary>
    public class CircleEditorItemModel : EditorItemModelBase
    {
        #region fields
        private DelegateCommand<Vector> _moveN;
        private DelegateCommand<Vector> _moveE;
        private DelegateCommand<Vector> _moveS;
        private DelegateCommand<Vector> _moveW;
        private DelegateCommand _showSettingsCommand;
        #endregion

        public CircleEditorItemModel(EditPanelViewModel parent, CircleEditorItem item, Func<CircleEditorItem, CircleEditorItem> editItem) : base(parent, item)
        {
            _moveN = new DelegateCommand<Vector>(x => TryMoveN(x));
            _moveS = new DelegateCommand<Vector>(x => TryMoveS(x));
            _moveE = new DelegateCommand<Vector>(x => TryMoveE(x));
            _moveW = new DelegateCommand<Vector>(x => TryMoveW(x));
            _showSettingsCommand = new DelegateCommand(() =>
            {
                Item = editItem((CircleEditorItem)Item);
            });
        }

        /// <summary>
        /// Resize by moving the upper part of the enclosing rectangle
        /// </summary>
        public DelegateCommand<Vector> MoveNCommand
        {
            get { return _moveN; }
        }

        /// <summary>
        /// Resize by moving the lower part of the enclosing rectangle
        /// </summary>
        public DelegateCommand<Vector> MoveSCommand
        {
            get { return _moveS; }
        }

        /// <summary>
        /// Resize by moving the right part of the enclosing rectangle
        /// </summary>
        public DelegateCommand<Vector> MoveECommand
        {
            get { return _moveE; }
        }

        /// <summary>
        /// Resize by moving the left part of the enclosing rectangle
        /// </summary>
        public DelegateCommand<Vector> MoveWCommand
        {
            get { return _moveW; }
        }

        /// <inheritdoc/>
        public override DelegateCommand ShowSettingsCommand
        {
            get
            {
                return _showSettingsCommand;
            }
        }

        private void TryMoveW(Vector toMove)
        {
            CircleEditorItem toManipulate = (CircleEditorItem) Item;
            double shift;
            double desiredShift = toMove.X;
            if (desiredShift == 0)
            {
                return;
            }
            else if (desiredShift > 0)
            {
                shift = Math.Min(toManipulate.Diameter - _parent.MinXYDimension, desiredShift);
            }
            else
            {
                shift = -Math.Min(toManipulate.X, _parent.ViewHeight - toManipulate.Y - toManipulate.Diameter);
                if (shift < desiredShift)
                    shift = desiredShift;
            }
            toManipulate.X += shift;
            toManipulate.Diameter -= shift;
        }

        private void TryMoveE(Vector toMove)
        {
            CircleEditorItem toManipulate = (CircleEditorItem)Item;
            double shift;
            double desiredShift = toMove.X;
            if (desiredShift == 0)
            {
                return;
            }
            else if (desiredShift > 0)
            {
                shift = Math.Min(_parent.ViewWidth - toManipulate.X - toManipulate.Diameter, _parent.ViewHeight - toManipulate.Y - toManipulate.Diameter);
                if (shift > desiredShift)
                    shift = desiredShift;
            }
            else
            {
                shift = Math.Max(-toManipulate.Diameter + _parent.MinXYDimension, desiredShift);
            }
            toManipulate.Diameter += shift;
        }

        private void TryMoveS(Vector toMove)
        {
            CircleEditorItem toManipulate = (CircleEditorItem)Item;
            double desiredShift = toMove.Y;
            double shift;
            if (desiredShift == 0)
            {
                return;
            }
            else if (desiredShift > 0)
            {
                shift = Math.Min(_parent.ViewWidth - toManipulate.X - toManipulate.Diameter, _parent.ViewHeight - toManipulate.Y - toManipulate.Diameter);
                if (shift > desiredShift)
                    shift = desiredShift;
            }
            else
            {
                shift = Math.Max(-toManipulate.Diameter + _parent.MinXYDimension, desiredShift);
            }
            toManipulate.Diameter += shift;
        }

        private void TryMoveN(Vector toMove)
        {
            CircleEditorItem toManipulate = (CircleEditorItem)Item;
            double shift;
            double desiredShift = toMove.Y;
            if (desiredShift == 0)
            {
                return;
            }
            else if (desiredShift > 0)
            {
                shift = Math.Min(toManipulate.Diameter - _parent.MinXYDimension, desiredShift);
            }
            else
            {
                shift = -Math.Min(toManipulate.Y, _parent.ViewWidth - toManipulate.X - toManipulate.Diameter);
                if (shift < desiredShift)
                    shift = desiredShift;
            }
            toManipulate.Y += shift;
            toManipulate.Diameter -= shift;
        }

    }
}
