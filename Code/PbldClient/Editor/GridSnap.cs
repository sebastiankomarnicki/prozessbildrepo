﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Linq;
using System.Waf.Foundation;

namespace Promess.Pbld.Editor
{
    /// <summary>
    /// Helper class to facilitate grid snap logic. Provides snap coordinates and snap to functionality.
    /// </summary>
    public class GridSnap: Model
    {
        /// <summary>
        /// Minimum x-dimension
        /// </summary>
        public const int MINX = 4;
        /// <summary>
        /// Minimum y-dimension
        /// </summary>
        public const int MINY = 2;
        /// <summary>
        /// Minimal number of non border snap points
        /// </summary>
        public const int MININNERSNAPPOINTS = 2;

        private Size _dimensions;
        private IReadOnlyList<double> _xSnapPoints;
        private IReadOnlyList<double> _ySnapPoints;
        private double _stepX;
        private double _stepY;
        private int _innerSnapPoints;

        public GridSnap()
        {
            this._xSnapPoints = new List<double>();
            this._ySnapPoints = new List<double>();
            this._dimensions = new Size(0, 0);
        }

        /// <summary>
        /// Initialize grid for given density and element size
        /// </summary>
        /// <param name="gridPointDensity">Abstract density</param>
        /// <param name="dimensions">Dimensions of element with snap points</param>
        /// <returns><c>true</c> if a grid could be created with the given parameters, otherwise <c>false</c></returns>
        public bool Setup(GridPointDensity gridPointDensity, Size dimensions)
        {
            this._innerSnapPoints = DensityToSnapPoints(gridPointDensity);
            this._dimensions = dimensions;
            return RecalculateSlices();
        }

        internal bool RecalculateSnapPoints(GridPointDensity gridPointDensity)
        {
            this._innerSnapPoints = DensityToSnapPoints(gridPointDensity);
            return RecalculateSlices();
        }

        internal bool RecalculateSnapPoints(Size dimensions)
        {
            this._dimensions = dimensions;
            return RecalculateSlices();
        }

        /// <summary>
        /// Translates the density to a concrete number of inner snap points.
        /// Warning: If the doubling scheme is ever changed the general snap logic must be examined again, because the snap points will not overlap anymore.
        /// </summary>
        /// <param name="gridPointDensity">Density to convert</param>
        /// <returns>Density as number of inner snap points </returns>
        private int DensityToSnapPoints(GridPointDensity gridPointDensity)
        {
            switch (gridPointDensity)
            {
                case GridPointDensity.Sparse:
                    return 19;
                case GridPointDensity.Medium:
                    return 38;
                case GridPointDensity.Dense:
                    return 76;
                default:
                    throw new NotImplementedException($"{nameof(GridPointDensity)} with value {gridPointDensity} not implemented.");
            }
        }

        private bool RecalculateSlices()
        {
            double stepX = this._dimensions.Width / this._innerSnapPoints;
            double stepY = this._dimensions.Height / this._innerSnapPoints;
            Debug.WriteLine($"Grid step sizes: {stepX}, {stepY}");
            if (stepY < MINY || stepX < MINX || this._innerSnapPoints < MININNERSNAPPOINTS)
            {
                this.XSnapPoints = new List<double>();
                this.XSnapPoints = new List<double>();
                this._stepX = 1;
                this._stepY = 1;
                return false;
            }
            var xSnapPoints = new List<double>();
            var ySnapPoints = new List<double>();

            for (int i = 0; i < this._innerSnapPoints; i++)
            {
                xSnapPoints.Add(i * stepX);
                ySnapPoints.Add(i * stepY);
            }
            xSnapPoints.Add(this._dimensions.Width);
            ySnapPoints.Add(this._dimensions.Height);
            this.XSnapPoints = xSnapPoints;
            this.YSnapPoints = ySnapPoints;
            this._stepX = stepX;
            this._stepY = stepY;
            return true;
        }

        /// <summary>
        /// Snap given point to closest snap point
        /// </summary>
        /// <param name="input">Point to snap</param>
        /// <returns>Closes snap point</returns>
        public Point Snap(Point input)
        {
            double newX = SnapX(input.X);
            double newY = Snap(input.Y, Math.Round, this._ySnapPoints, this._stepY);
            return new Point(newX, newY);
        }

        /// <summary>
        /// Snap x position to closest x snap position
        /// </summary>
        /// <param name="inX">x position</param>
        /// <returns>Closest x snap position</returns>
        public double SnapX(double inX)
        {
            return Snap(inX, Math.Round, this._xSnapPoints, this._stepX);
        }

        /// <summary>
        /// Snap y position to closest y snap position
        /// </summary>
        /// <param name="inY">y position</param>
        /// <returns>Closest y snap position</returns>
        public double SnapY(double inY)
        {
            return Snap(inY, Math.Round, this._ySnapPoints, this._stepY);
        }

        /// <summary>
        /// Floor x position to x snap position
        /// </summary>
        /// <param name="inX">x position</param>
        /// <returns>Floored x snap position</returns>
        public double SnapXLower(double inX)
        {
            return Snap(inX, Math.Floor, this._xSnapPoints, this._stepX);
        }

        /// <summary>
        /// Floor y position to y snap position
        /// </summary>
        /// <param name="inY">y position</param>
        /// <returns>Floored y snap position</returns>
        public double SnapYLower(double inY)
        {
            return Snap(inY, Math.Floor, this._ySnapPoints, this._stepY);
        }

        /// <summary>
        /// Ceil x position to x snap position
        /// </summary>
        /// <param name="inX">x position</param>
        /// <returns>Ceiled x snap position</returns>
        public double SnapXHigher(double inX)
        {
            return Snap(inX, Math.Ceiling, this._xSnapPoints, this._stepX);
        }

        /// <summary>
        /// Ceil y position to y snap position
        /// </summary>
        /// <param name="inY">y position</param>
        /// <returns>Ceiled y snap position</returns>
        internal double SnapYHigher(double inY)
        {
            return Snap(inY, Math.Ceiling, this._ySnapPoints, this._stepY);
        }

        private double Snap(double value, Func<double, double> indexFunc, IReadOnlyList<double> snapValues, double stepSize)
        {
            if (!snapValues.Any())
                return value;
            if (value < snapValues.First())
                return snapValues.First();
            if (value > snapValues.Last())
                return snapValues.Last();
            int index = (int)indexFunc(value / stepSize);
            return index >= snapValues.Count ? snapValues.Last() : snapValues[index];
        }

        public double StepX { get { return _stepX; } }
        public double StepY { get { return _stepY; } }

        public IReadOnlyList<double> XSnapPoints
        {
            get { return _xSnapPoints; }
            private set { SetProperty(ref _xSnapPoints, value); }
        }

        public IReadOnlyList<double> YSnapPoints
        {
            get { return _ySnapPoints; }
            private set { SetProperty(ref _ySnapPoints, value); }
        }

        /// <summary>
        /// Convert a shift to a shift conforming to the grid.
        /// The new shifts directions are at most as large as the inputs. 
        /// </summary>
        /// <param name="shift">Original shift</param>
        /// <returns>Grid conforming shift</returns>
        public Vector ConvertShiftToSnapShift(Vector shift)
        {
            double shiftX, shiftY;
            shiftX = Math.Floor(shift.X / this._stepX) * this._stepX;
            shiftY = Math.Floor(shift.Y / this._stepY) * this._stepY;
            return new Vector(shiftX, shiftY);
        }
    }
}
