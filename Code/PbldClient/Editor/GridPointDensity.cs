﻿namespace Promess.Pbld.Editor
{
    /// <summary>
    /// Abstraction for the density of a grid
    /// </summary>
    public enum GridPointDensity
    {
        Sparse,
        Medium,
        Dense
    }
}
