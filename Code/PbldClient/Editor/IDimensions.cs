﻿namespace Promess.Pbld.Editor
{
    /// <summary>
    /// Interface to provide information about the current size of an element and its minimal xy-dimension
    /// </summary>
    public interface IDimensions
    {
        /// <summary>
        /// Minimal size in x and y direction
        /// </summary>
        double MinXYDimension { get; }
        /// <summary>
        /// Height of the view/container
        /// </summary>
        double ViewHeight { get; }
        /// <summary>
        /// Width of the view/container
        /// </summary>
        double ViewWidth { get; }
    }
}
