﻿using System;
using Promess.Pbld.Data.Editor;
using System.Windows;
using Promess.Common.Util;
using System.Waf.Applications;

namespace Promess.Pbld.Editor
{
    /// <summary>
    /// Editor model for a line with custom movement logic.
    /// </summary>
    public class LineEditorItemModel : EditorItemModelBase
    {
        #region fields
        private DelegateCommand<Vector> _moveP1Command;
        private DelegateCommand<Vector> _moveP2Command;
        private DelegateCommand _showSettingsCommand;
        #endregion

        public LineEditorItemModel(IDimensionsWithGrid parent, LineEditorItem item, Func<LineEditorItem, LineEditorItem> editItem) : base(parent, item)
        {
            NormalizeAndSet(item, item.X1, item.Y1, item.X2, item.Y2);
            _moveP1Command = new DelegateCommand<Vector>(x => TryMove(x, true));
            _moveP2Command = new DelegateCommand<Vector>(x => TryMove(x, false));
            _showSettingsCommand = new DelegateCommand(() => {
                Item = editItem((LineEditorItem)Item);
            });
        }

        /// <summary>
        /// Command for moving the first point
        /// </summary>
        public DelegateCommand<Vector> MoveP1Command
        {
            get { return _moveP1Command; }
        }

        /// <summary>
        /// Command for moving the second point
        /// </summary>
        public DelegateCommand<Vector> MoveP2Command
        {
            get { return _moveP2Command; }
        }

        /// <inheritdoc/>
        public override DelegateCommand ShowSettingsCommand
        {
            get
            {
                return _showSettingsCommand;
            }
        }

        private void TryMove(Vector desiredMovement, bool moveP1)
        {
            double otherX, otherY, moveX, moveY;
            double actualShiftX, actualShiftY;
            double tmpValueX;
            double tmpValueY;
            LineEditorItem myItem = (LineEditorItem)Item;
            if (moveP1)
            {
                otherX = myItem.X2;
                otherY = myItem.Y2;
                moveX = myItem.X1;
                moveY = myItem.Y1;
            }else
            {
                otherX = myItem.X1;
                otherY = myItem.Y1;
                moveX = myItem.X2;
                moveY = myItem.Y2;
            }

            actualShiftX = CalculatePossibleShift(_parent.ViewWidth, myItem.X + moveX, desiredMovement.X);
            actualShiftY = CalculatePossibleShift(_parent.ViewHeight, myItem.Y + moveY, desiredMovement.Y);
            tmpValueX = moveX + actualShiftX;
            tmpValueY = moveY + actualShiftY;
            if (this._parent.GridPointDensity.HasValue)
            {
                var snap = this._parent.GridSnap.Snap(new Point(tmpValueX+myItem.X, tmpValueY+myItem.Y));
                double snapXShift = snap.X - myItem.X - moveX;
                double snapYShift = snap.Y - myItem.Y - moveY;
                if (IsActualShiftValidGivenDesired(actualShiftX, snapXShift))
                {
                    actualShiftX = snapXShift;
                    tmpValueX = moveX + actualShiftX;
                }
                else
                {
                    tmpValueX = moveX;
                    actualShiftX = 0;
                }
                if (IsActualShiftValidGivenDesired(actualShiftY, snapYShift))
                {
                    actualShiftY = snapYShift;
                    tmpValueY = moveY + actualShiftY;                    
                }
                else
                {
                    tmpValueY = moveY;
                    actualShiftX = 0;
                }
            }

            if (Math.Abs(tmpValueX - otherX) < _parent.MinXYDimension &&
                Math.Abs(tmpValueY - otherY) < _parent.MinXYDimension)
            {
                //adjustment depends on which was previously too small
                if (Math.Abs(moveX-otherX) > _parent.MinXYDimension)
                    moveX = actualShiftX >= 0 ? otherX - _parent.MinXYDimension : otherX + _parent.MinXYDimension;
                if (Math.Abs(moveY - otherY) > _parent.MinXYDimension)
                    moveY = actualShiftY >= 0 ? otherY - _parent.MinXYDimension : otherY + _parent.MinXYDimension;
            }
            else
            {
                moveX = tmpValueX;
                moveY = tmpValueY;
            }
            if (moveP1)
            {
                NormalizeAndSet(myItem, moveX, moveY, otherX, otherY);
            }else
            {
                NormalizeAndSet(myItem, otherX, otherY, moveX, moveY);
            }
        }

        private void NormalizeAndSet(LineEditorItem myItem, double x1, double y1, double x2, double y2)
        {
            double x = myItem.X;
            double y = myItem.Y;
            double adjustX = x1<=x2? x1:x2;
            double adjustY = y1<=y2? y1:y2;
            x += adjustX;
            x1 -= adjustX;
            x2 -= adjustX;
            y += adjustY;
            y1 -= adjustY;
            y2 -= adjustY;
            myItem.X = x;
            myItem.Y = y;
            myItem.X1 = x1;
            myItem.Y1 = y1;
            myItem.X2 = x2;
            myItem.Y2 = y2;
        }

        private double CalculatePossibleShift(double maxValue, double toMove, double movement)
        {
            double shiftValue;
            if (movement > 0)
            {
                shiftValue = Math.Min(maxValue - toMove, movement);
            }else
            {
                shiftValue = Math.Max(-toMove, movement);
            }
            return shiftValue;
        }
    }
}
