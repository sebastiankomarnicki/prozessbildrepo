﻿using System;
using Promess.Pbld.Data.Editor;
using Promess.Common.Util;
using System.Windows;
using System.Waf.Applications;

namespace Promess.Pbld.Editor
{
    /// <summary>
    /// Editor model for <see cref="RectangleEditorItem"/>s with custom movement logic for recantgular elements.
    /// </summary>
    /// <typeparam name="T"><see cref="RectangleEditorItem"/> or derived type which is wrapped for movement</typeparam>
    public class RectangleEditorItemModel<T> : EditorItemModelBase
        where T: RectangleEditorItem
    {
        #region fields
        private DelegateCommand<Vector> _moveNW;
        private DelegateCommand<Vector> _moveNE;
        private DelegateCommand<Vector> _moveSW;
        private DelegateCommand<Vector> _moveSE;
        private DelegateCommand _showSettingsCommand;
        #endregion

        public RectangleEditorItemModel(IDimensionsWithGrid parent, T item, Func<T,T> editItem) : base(parent, item)  
        {
            _moveNW = new DelegateCommand<Vector>(x => TryMove(x, true, false));
            _moveNE = new DelegateCommand<Vector>(x => TryMove(x, true, true));
            _moveSW = new DelegateCommand<Vector>(x => TryMove(x, false, false));
            _moveSE = new DelegateCommand<Vector>(x => TryMove(x, false, true));
            _showSettingsCommand = new DelegateCommand(()=> {
                Item = editItem((T)Item);
            });
        }

        /// <inheritdoc/>
        public override DelegateCommand ShowSettingsCommand
        {
            get { return _showSettingsCommand; }
        }

        /// <summary>
        /// Resize by moving NW corner
        /// </summary>
        public DelegateCommand<Vector> MoveNWCommand
        {
            get { return _moveNW; }
        }

        /// <summary>
        /// Resize by moving SW corner
        /// </summary>
        public DelegateCommand<Vector> MoveSWCommand
        {
            get { return _moveSW; }
        }

        /// <summary>
        /// Resize by moving NE corner
        /// </summary>
        public DelegateCommand<Vector> MoveNECommand
        {
            get { return _moveNE; }
        }

        /// <summary>
        /// Resize by moving SE corner
        /// </summary>
        public DelegateCommand<Vector> MoveSECommand
        {
            get { return _moveSE; }
        }

        /// <summary>
        /// Try to move one of the corners of this recangle by <paramref name="delta"/> with respect to the bounds, minimal size and grid defined by the container. 
        /// </summary>
        /// <param name="delta">Delta of the x and y shift.</param>
        /// <param name="top">True if top corner, otherwise false.</param>
        /// <param name="left">True if left corner, otherwise false.</param>
        private void TryMove(Vector delta, bool top, bool left)
        {
            RectangleEditorItem toManipulate = (RectangleEditorItem)Item;
            double actualShiftY, actualShiftX;
            double yDestination, xDestination;
            if (top)
            {
                if (this._parent.GridPointDensity.HasValue)
                {
                    yDestination = this._parent.GridSnap.SnapY(toManipulate.Y + delta.Y);
                    if (yDestination + this._parent.MinXYDimension > toManipulate.Y + toManipulate.Height)
                        yDestination = this._parent.GridSnap.SnapYLower(toManipulate.Y + toManipulate.Height - this._parent.MinXYDimension);
                }
                else
                {
                    yDestination = Math.Max(0, Math.Min(toManipulate.Y + delta.Y, toManipulate.Y + toManipulate.Height - this._parent.MinXYDimension));
                }
                actualShiftY = yDestination - toManipulate.Y;
                if (actualShiftY != 0 && IsActualShiftValidGivenDesired(actualShiftY, delta.Y) && toManipulate.Height - actualShiftY >= this._parent.MinXYDimension)
                {
                    toManipulate.Y += actualShiftY;
                    toManipulate.Height -= actualShiftY;
                }
            }
            else
            {
                if (this._parent.GridPointDensity.HasValue)
                {
                    yDestination = this._parent.GridSnap.SnapY(toManipulate.Y + toManipulate.Height + delta.Y);
                    if (yDestination - toManipulate.Y < this._parent.MinXYDimension)
                        yDestination = this._parent.GridSnap.SnapYHigher(toManipulate.Y + this._parent.MinXYDimension);
                }
                else
                {
                    yDestination = Math.Max(toManipulate.Y + this._parent.MinXYDimension, Math.Min(toManipulate.Y + toManipulate.Height + delta.Y, this._parent.ViewHeight));
                }
                actualShiftY = yDestination - toManipulate.Y - toManipulate.Height;
                if (actualShiftY != 0 && IsActualShiftValidGivenDesired(actualShiftY, delta.Y) && toManipulate.Height + actualShiftY >= this._parent.MinXYDimension)
                {
                    toManipulate.Height += actualShiftY;
                }
            }
            if (left)
            {
                if (this._parent.GridPointDensity.HasValue)
                {
                    xDestination = this._parent.GridSnap.SnapX(toManipulate.X + delta.X);
                    if (xDestination + this._parent.MinXYDimension > toManipulate.X + toManipulate.Width)
                        xDestination = this._parent.GridSnap.SnapXLower(toManipulate.X + toManipulate.Width - this._parent.MinXYDimension);
                }
                else
                {
                    xDestination = Math.Max(0, Math.Min(toManipulate.X + delta.X, toManipulate.X + toManipulate.Width - this._parent.MinXYDimension));
                }
                actualShiftX = xDestination - toManipulate.X;
                if (actualShiftX != 0 && IsActualShiftValidGivenDesired(actualShiftX, delta.X) && toManipulate.Width - actualShiftX >= this._parent.MinXYDimension)
                {
                    toManipulate.X += actualShiftX;
                    toManipulate.Width -= actualShiftX;
                }
            }
            else
            {
                if (this._parent.GridPointDensity.HasValue)
                {
                    xDestination = this._parent.GridSnap.SnapX(toManipulate.X + toManipulate.Width + delta.X);
                    if (xDestination - toManipulate.X < this._parent.MinXYDimension)
                        xDestination = this._parent.GridSnap.SnapXHigher(toManipulate.X + this._parent.MinXYDimension);
                }
                else
                {
                    xDestination = Math.Max(toManipulate.X + this._parent.MinXYDimension, Math.Min(toManipulate.X + toManipulate.Width + delta.X, this._parent.ViewWidth));
                }
                actualShiftX = xDestination - toManipulate.X - toManipulate.Width;
                if (actualShiftX != 0 && IsActualShiftValidGivenDesired(actualShiftX, delta.X) && toManipulate.Width + actualShiftX >= this._parent.MinXYDimension)
                {
                    toManipulate.Width += actualShiftX;
                }
            }
        }

        /// <summary>
        /// Calculates to possible shift given the lowest and highest possible values as well as the desired shift.
        /// </summary>
        /// <param name="lowestValue">Lowest possible value for the shift.</param>
        /// <param name="highestValue">Highest possible value for the shift.</param>
        /// <param name="desiredShift">Desired shift</param>
        /// <returns>The possible shift</returns>
        private double CalculatePossibleShift(double lowestValue, double highestValue, double desiredShift)
        {
            if (desiredShift >= 0)
            {
                return Math.Min(highestValue, desiredShift);
            }
            else
            {
                return Math.Max(lowestValue, desiredShift);
            }
        }
    }
}
