﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Threading;
using System.Diagnostics;
using Promess.Pbld.Services;
using Promess.Pbld.Utility;
using Promess.Pbld.Data.Settings;
using Promess.Common.Util;
using Promess.Pbld.Data;
using System.IO;

namespace Pbld
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private AggregateCatalog catalog;
        private CompositionContainer container;
        private IEnumerable<IModuleController> moduleControllers;

        /// <summary>
        /// Entry point for the application. Initializes MEF, operating mode, different services. Also initializes the module controllers and runs them.
        /// </summary>
        /// <param name="e">Startup event args, used to determine the operating mode</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            //FIXME make startup and shutdown code async (just add async) and distinguish between sync and async services and controllers 
            base.OnStartup(e);
            DispatcherUnhandledException += AppDispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += AppDomainUnhandledException;

            catalog = new AggregateCatalog();
            // Add the WpfApplicationFramework assembly to the catalog
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(ViewModel).Assembly));
            // Add pbld assemblies. This will need to be changed if the structure is changed of the elements
            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            catalog.Catalogs.Add(new AssemblyCatalog("Common.dll"));
            catalog.Catalogs.Add(new AssemblyCatalog("QsStat.dll"));
            catalog.Catalogs.Add(new AssemblyCatalog("Authentication.dll"));
            catalog.Catalogs.Add(new AssemblyCatalog("Authentication.Presentation.dll"));
            container = new CompositionContainer(catalog, CompositionOptions.DisableSilentRejection);
            CompositionBatch batch = new CompositionBatch();
            batch.AddExportedValue(container);
            container.Compose(batch);

            //Warning: This must be set first, because the controllers rely on the source being set. should be implemented as a service? Implement EnvironmentService (@see WAF), to get the path
            var pathAndResources = container.GetExportedValue<PathsAndResourceService>();
            try
            {
                pathAndResources.Initialize();
            }
            catch
            {
                MessageBox.Show("Die Dateistruktur konnte nicht initiailisiert werden, das Programm wird beendet.\n" +
                    "The file structure could not be initialized, the program is shutting down.",
                    "Shutdown", MessageBoxButton.OK, MessageBoxImage.Information);
                Shutdown();
                return;
            }

            //switch to more complex argument parsing if there is more later. maybe needs to move to module controller
            var mainService = container.GetExportedValue<MainService>();
            mainService.OperatingMode = GetOperatingMode(e);

            // Initialize all presentation services. If presentation ever needs settings this needs to be split up. Initial language uses fallback, not from settings
            var presentationServices = container.GetExportedValues<IPresentationService>();
            foreach (var presentationService in presentationServices) { presentationService.Initialize(); }

            var configurationManager = container.GetExportedValue<ModifiedConfigurationManagerPbld>();
            configurationManager.Initialize();

            var tmpGeneral = configurationManager.GetSectionAndAddIfMissing<GeneralSettings>(Constants.General);
            var tmpEvalColor = configurationManager.GetSectionAndAddIfMissing<EvaluationAndColorSettings>(Constants.EvaluationAndColor);
            var tmpQsStat = configurationManager.GetSectionAndAddIfMissing<QsStatSettings>(Constants.QsStat);
            var tmpGraphicProject = configurationManager.GetSectionAndAddIfMissing<GraphicPageSettings>(Constants.GraphicProject, out bool createdGraphicProjectSettings);
            if (createdGraphicProjectSettings && File.Exists(pathAndResources.SampleGraphicProjectPath))
            {
                tmpGraphicProject.ProjectPath = pathAndResources.SampleGraphicProjectPath;
                configurationManager.Save(ConfigurationSaveMode.Modified);
            }               
            var tmpTransfer = configurationManager.GetSectionAndAddIfMissing<TransferFollowerSettings>(Constants.TransferFollower);
            var tmpLanguageSettings = configurationManager.GetSectionAndAddIfMissing<LanguageSettings>(Constants.Language);
            
            var configurationService = container.GetExportedValue<ConfigurationService>();
            
            var success = configurationService.Initialize(SerializeHelper.CreateDataContractCopy(tmpGeneral),
                SerializeHelper.CreateDataContractCopy(tmpEvalColor), SerializeHelper.CreateDataContractCopy(tmpQsStat),
                SerializeHelper.CreateDataContractCopy(tmpGraphicProject), SerializeHelper.CreateDataContractCopy(tmpTransfer),
                SerializeHelper.CreateDataContractCopy(tmpLanguageSettings));
            if (!success)
                Shutdown();     

            moduleControllers = container.GetExportedValues<IModuleController>();
            foreach (IModuleController moduleController in moduleControllers) { moduleController.Initialize(); }
            foreach (IModuleController moduleController in moduleControllers) { moduleController.Run(); }
        }

        private OperatingMode GetOperatingMode(StartupEventArgs e)
        {
            OperatingMode mode = OperatingMode.normal;
            foreach(var value in e.Args)
            {
                if (!value.StartsWith(@"/"))
                    continue;
                string item = value.Substring(1);
                if ("leader".Equals(item, StringComparison.OrdinalIgnoreCase) || "master".Equals(item, StringComparison.OrdinalIgnoreCase))
                {
                    mode = OperatingMode.leader;
                    break;
                }else if ("follower".Equals(item, StringComparison.OrdinalIgnoreCase) || "slave".Equals(item, StringComparison.OrdinalIgnoreCase))
                {
                    mode = OperatingMode.follower;
                    break;
                }
            }
            return mode;
        }

        /// <summary>
        /// Shut down the application by shutting down all module controllers. Disposes MEF elements.
        /// </summary>
        /// <param name="e">Ignored</param>
        protected override void OnExit(ExitEventArgs e)
        {
            if (moduleControllers != null)
            {
                foreach (IModuleController moduleController in moduleControllers.Reverse())
                {
                    moduleController.Shutdown();
                }
            }
            container?.Dispose();
            catalog?.Dispose();
            base.OnExit(e);
        }

        private void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            HandleException(e.Exception, false);
        }

        private static void AppDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            HandleException(e.ExceptionObject as Exception, e.IsTerminating);
        }

        private static void HandleException(Exception e, bool isTerminating)
        {
            if (e == null) { return; }

            Trace.TraceError(e.ToString());

            if (!isTerminating)
            {
                MessageBox.Show(string.Format("An unhandled exception occured:\n{0}", e.ToString())
                    , ApplicationInfo.ProductName, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
