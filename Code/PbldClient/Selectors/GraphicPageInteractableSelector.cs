﻿using Promess.Pbld.Data.GraphicPage;
using System.Windows;
using System.Windows.Controls;

namespace Promess.Pbld.Selectors
{
    /// <summary>
    /// Selector to provide a different <see cref="DataTemplate"/> for qsStat image items which allows interaction and otherwise uses the default templates.
    /// </summary>
    public class GraphicPageInteractableSelector:DataTemplateSelector
    {
        public DataTemplate ImageTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is ImageGPEditorItem && ImageTemplate is object)
            {
                ImageGPEditorItem graphItem = item as ImageGPEditorItem;
                if (graphItem.ImageSourceType == Data.Editor.ImageSourceType.Qdas && graphItem.EvaluationCode.HasValue)
                {
                    if (graphItem.EvaluationCode.Value==3100 || graphItem.EvaluationCode.Value == 6102)
                    {
                        return ImageTemplate;
                    }
                }
            }
            return base.SelectTemplate(item, container);
        }
    }
}
