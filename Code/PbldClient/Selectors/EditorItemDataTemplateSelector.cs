﻿using Promess.Pbld.Data.Editor;
using System.Windows;
using System.Windows.Controls;

namespace Promess.Pbld.Selectors
{
    /// <summary>
    /// Provide different <see cref="DataTemplate"/>s for <see cref="LineEditorItem"/> and <see cref="RectangleEditorItem"/>
    /// </summary>
    public class EditorItemDataTemplateSelector:DataTemplateSelector
    {
        public DataTemplate LineTemplate
        {
            get;set;
        }

        public DataTemplate RectangleTemplate
        {
            get; set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate selected = null;
            EditorItemBase value = item as EditorItemBase;
            if (value is object)
            {
                switch (value)
                {
                    case LineEditorItem line:
                        selected = LineTemplate;
                        break;
                    case RectangleEditorItem rect:
                        selected = RectangleTemplate;
                        break;
                }
            }
            if (selected is object)
                return selected;
            return base.SelectTemplate(item, container);
        }
    }
}
