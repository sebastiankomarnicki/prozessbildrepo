﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Promess.Pbld.Selectors
{
    /// <summary>
    /// Selector to provide different <see cref="DataTemplate"/>s for <see cref="EditorItemModelBase"/> subclasses which require different templates for resizing.
    /// </summary>
    public class ListBoxItemStyleSelector : StyleSelector
    {
        private Style _lineResize;
        private Style _middleResizeStyle;
        private Style _cornerResizeStyle;

        public Style LineResizeStyle
        {
            get
            {
                return _lineResize;
            }

            set
            {
                _lineResize = value;
            }
        }

        public Style MiddleResizeStyle
        {
            get
            {
                return _middleResizeStyle;
            }

            set
            {
                _middleResizeStyle = value;
            }
        }

        public Style CornerResizeStyle
        {
            get
            {
                return _cornerResizeStyle;
            }

            set
            {
                _cornerResizeStyle = value;
            }
        }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            Type theType = item.GetType();
            if (theType == typeof(LineEditorItemModel))
            {
                return LineResizeStyle;
            }
            else if (theType == typeof(CircleEditorItemModel))
            {
                return MiddleResizeStyle;
            }
            else if (theType.IsGenericType && theType.GetGenericTypeDefinition() == typeof(RectangleEditorItemModel<>))
            {
                return CornerResizeStyle;
            }
            return base.SelectStyle(item, container);
        }
    }
}
