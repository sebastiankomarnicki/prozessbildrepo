﻿using Promess.Pbld.Data.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Promess.Pbld.Selectors
{
    /// <summary>
    /// Template selector for different values of <see cref="BorderType"/>.
    /// The intention is to provide different <see cref="DataTemplate"/>s to change the border look.
    /// </summary>
    public class BorderTemplateSelector:DataTemplateSelector
    {
        private DataTemplate _normal;
        private DataTemplate _raised;
        private DataTemplate _lowered;
        private DataTemplate _borderless;

        public DataTemplate NormalTemplate
        {
            get { return _normal; }
            set
            {
                _normal = value;
            }
        }

        public DataTemplate RaisedTemplate
        {
            get { return _raised; }
            set
            {
                _raised = value;
            }
        }

        public DataTemplate LoweredTemplate
        {
            get { return _lowered; }
            set
            {
                _lowered = value;
            }
        }

        public DataTemplate BorderlessTemplate
        {
            get { return _borderless; }
            set
            {
                _borderless = value;
            }
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DataTemplate selected=null;
            BorderType? value = item as BorderType?;
            if (value.HasValue)
            {
                switch (value.Value)
                {
                    case BorderType.Flat:
                        selected = _normal;
                        break;
                    case BorderType.Lowered:
                        selected = _lowered;
                        break;
                    case BorderType.Raised:
                        selected = _raised;
                        break;
                    case BorderType.Borderless:
                        selected = _borderless;
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            if (selected != null)
                return selected;
            else
                return base.SelectTemplate(item, container);
        }
    }
}
