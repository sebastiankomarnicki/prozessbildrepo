﻿using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.GraphicPage;
using System.ComponentModel;
using Promess.Pbld.Editor;
using Promess.Common.Util;
using Promess.Pbld.Services;
using Promess.Pbld.Data.Settings;
using System.Waf.Applications.Services;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class EditGraphicPageViewModel : BaseEditorViewModelExtended<IEditGraphicPageView>
    {
        #region fields
        private readonly ExportFactory<PropertiesViewModel<ITextGPPropertiesView>> _propertiesTextVMFactory;
        private readonly ExportFactory<PropertiesViewModel<IImageGPPropertiesView>> _propertiesImageVMFactory;
        private readonly ExportFactory<PropertiesViewModel<ILinePropertiesView>> _propertiesLineVMFactory;
        private readonly ExportFactory<TextGPEditorItemViewModel> _textEditorItemVMFactory;
        private readonly ExportFactory<ImageGPEditorItemViewModel> _imageEditorItemVMFactory;
        private readonly ExportFactory<LineEditorViewModel> _lineEditorVMFactory;

        private DelegateCommand _addImageCommand;
        private DelegateCommand _addTextBoxCommand;
        private DelegateCommand _addLineCommand;

        private GraphicItemModel _data;
        private PresetGraphicSettings _presetGraphicSettings;
        #endregion

        [ImportingConstructor]
        public EditGraphicPageViewModel(IEditGraphicPageView view, ConfigurationService configurationService, IMessageService messageService,
            ExportFactory<PropertiesViewModel<ITextGPPropertiesView>> propertiesTextVMFactory, ExportFactory<PropertiesViewModel<IImageGPPropertiesView>> propertiesImageVMFactory,
            ExportFactory<PropertiesViewModel<ILinePropertiesView>> propertiesLineVMFactory,
            ExportFactory<TextGPEditorItemViewModel> textEditorVMFactory, ExportFactory<ImageGPEditorItemViewModel> imageEditorVMFactory,
            ExportFactory<LineEditorViewModel> lineEditorVMFactory
            ) : base(view, messageService)
        {
            this._propertiesTextVMFactory = propertiesTextVMFactory;
            this._propertiesImageVMFactory = propertiesImageVMFactory;
            this._propertiesLineVMFactory = propertiesLineVMFactory;
            this._textEditorItemVMFactory = textEditorVMFactory;
            this._imageEditorItemVMFactory = imageEditorVMFactory;
            this._lineEditorVMFactory = lineEditorVMFactory;

            this._presetGraphicSettings = configurationService.GetPresetGraphicSettingsCopy();

            this._addImageCommand = new DelegateCommand(AddImageCommandHandler, () => Data != null);
            this._addTextBoxCommand = new DelegateCommand(AddTextBoxCommandHandler, () => Data != null);
            this._addLineCommand = new DelegateCommand(AddLineCommandHandler, () => Data != null);
            this.SelectAspectRatioCommand = new DelegateCommand<AspectRatio>(SelectAspectRatioCommandHandler, _ => Data != null);
        }

        internal void Initialize(GraphicItemModel data)
        {
            var elems = new List<EditorItemModelBase>();
            if (data != null)
            {
                foreach (var elem in data?.Items)
                    elems.Add(GenerateItemModel(elem));
            }
            base.Initialize(elems);
            this.Data = data;
            RaisePropertyChanged(nameof(CurrentDataName));
            UpdateCommandsCanExecute();
        }

        #region Properties
        public string CurrentDataName
        {
            get { return Data?.Name; }
        }

        public GraphicItemModel Data
        {
            get { return _data; }
            private set { SetProperty(ref _data, value); }
        }

        internal object ParentView { get; set; }
        #endregion

        #region commands
        public DelegateCommand AddImageCommand
        {
            get { return _addImageCommand; }
        }
        public DelegateCommand AddTextBoxCommand
        {
            get { return _addTextBoxCommand; }
        }
        public DelegateCommand AddLineCommand
        {
            get { return _addLineCommand; }
        }

        public DelegateCommand<AspectRatio> SelectAspectRatioCommand { get; private set; }

        private void UpdateCommandsCanExecute()
        {
            AddImageCommand.RaiseCanExecuteChanged();
            AddTextBoxCommand.RaiseCanExecuteChanged();
            AddLineCommand.RaiseCanExecuteChanged();
            SelectAspectRatioCommand.RaiseCanExecuteChanged();
        }
        #endregion

        private void AddItem(EditorItemBase elem)
        {
            var wrapped = GenerateItemModel(elem);
            AddItem(wrapped);
            ClearSelectedItems();
            wrapped.IsSelected = true;
        }

        protected override void AddItem(EditorItemModelBase model)
        {
            base.AddItem(model);
            Data.Items.Add(model.Item);
        }

        protected override void RemoveItem(EditorItemModelBase model)
        {
            base.RemoveItem(model);
            Data.Items.Remove(model.Item);
        }

        public override void ResetData()
        {
            base.ResetData();
            this.Data = null;
            UpdateCommandsCanExecute();
            RaisePropertyChanged(nameof(CurrentDataName));
        }

        protected override void RecalculateActualSizes(EditorItemBase elem)
        {
            bool oldIsDirty = elem.IsDirty;
            elem.X = elem.PercentageX * ViewWidth;
            elem.Y = elem.PercentageY * ViewHeight;
            if (elem is LineEditorItem line)
            {
                line.X1 = line.PercentageX1 * ViewWidth;
                line.X2 = line.PercentageX2 * ViewWidth;
                line.Y1 = line.PercentageY1 * ViewHeight;
                line.Y2 = line.PercentageY2 * ViewHeight;
            }
            else if (elem is RectangleEditorItem rectangle)
            {
                rectangle.Width = rectangle.PercentageWidth * ViewWidth;
                rectangle.Height = rectangle.PercentageHeight * ViewHeight;
            }
            elem.IsDirty = oldIsDirty;
        }

        protected override void SavePercentage(EditorItemBase elem)
        {
            elem.PercentageX = elem.X / LastRecalculateSize.Width;
            elem.PercentageY = elem.Y / LastRecalculateSize.Height;
            switch (elem)
            {
                case LineEditorItem line:
                    line.PercentageX1 = line.X1 / LastRecalculateSize.Width;
                    line.PercentageX2 = line.X2 / LastRecalculateSize.Width;
                    line.PercentageY1 = line.Y1 / LastRecalculateSize.Height;
                    line.PercentageY2 = line.Y2 / LastRecalculateSize.Height;
                    break;
                case RectangleEditorItem rectangle:
                    rectangle.PercentageWidth = rectangle.Width / LastRecalculateSize.Width;
                    rectangle.PercentageHeight = rectangle.Height / LastRecalculateSize.Height;
                    break;
                default:
                    throw new NotImplementedException($"{elem.GetType()} is not supported in {nameof(SavePercentage)}.");
            }
        }

        protected override EditorItemModelBase GenerateItemModel(EditorItemBase elem)
        {
            EditorItemModelBase model;
            if (elem is LineEditorItem line)
            {
                model = new LineEditorItemModel(this, line, EditLineEditorItemSettings);
            }
            else if (elem is TextGPEditorItem textGPItem)
            {
                model = new RectangleEditorItemModel<TextGPEditorItem>(this, textGPItem, EditTextEditorItemSettings);
            }
            else if (elem is ImageGPEditorItem imageItem)
            {
                model = new RectangleEditorItemModel<ImageGPEditorItem>(this, imageItem, EditImageEditorItemSettings);
            }
            else
            {
                throw new NotImplementedException($"The container implementation for {elem.GetType()} is missing.");
            }
            elem.IsDirty = false;
            PropertyChangedEventManager.AddHandler(model, IsSelectedChanged, nameof(EditorItemModelBase.IsSelected));
            return model;
        }

        protected LineEditorItem EditLineEditorItemSettings(LineEditorItem item)
        {
            var propVM = this._propertiesLineVMFactory.CreateExport().Value;
            var vm = this._lineEditorVMFactory.CreateExport().Value;
            var itemCopy = SerializeHelper.CreateDataContractCopy(item);
            CopyOriginalSizes(item, itemCopy);
            vm.Setup(propVM.View, itemCopy);
            propVM.Setup(vm);
            //add validation if ever required
            return ProcessPropertiesViewModel(propVM, item, itemCopy);
        }

        protected ImageGPEditorItem EditImageEditorItemSettings(ImageGPEditorItem item)
        {
            var propVM = _propertiesImageVMFactory.CreateExport().Value;
            var vm = _imageEditorItemVMFactory.CreateExport().Value;
            var itemCopy = SerializeHelper.CreateDataContractCopy(item);
            CopyOriginalSizes(item, itemCopy);
            vm.Setup(propVM.View, itemCopy);
            propVM.Setup(vm);
            void validUpdateHandler(object s, PropertyChangedEventArgs e) { propVM.IsValid = !vm.Data.HasErrors; }
            propVM.SetValidUpdateHandler(validUpdateHandler);
            PropertyChangedEventManager.AddHandler(vm.Data, validUpdateHandler, nameof(vm.Data.HasErrors));
            return ProcessPropertiesViewModel(propVM, item, itemCopy);
        }

        protected TextGPEditorItem EditTextEditorItemSettings(TextGPEditorItem item)
        {
            var propVM = _propertiesTextVMFactory.CreateExport().Value;
            var textVM = _textEditorItemVMFactory.CreateExport().Value;
            var itemCopy = SerializeHelper.CreateDataContractCopy(item);
            CopyOriginalSizes(item, itemCopy);
            textVM.Setup(propVM.View, itemCopy);
            propVM.Setup(textVM);
            void validUpdateHandler(object s, PropertyChangedEventArgs e) { propVM.IsValid = !textVM.Data.HasErrors; }
            propVM.SetValidUpdateHandler(validUpdateHandler);
            PropertyChangedEventManager.AddHandler(textVM.Data, validUpdateHandler, nameof(textVM.Data.HasErrors));
            return ProcessPropertiesViewModel(propVM, item, itemCopy);
        }

        private void CopyOriginalSizes(RectangleEditorItem source, RectangleEditorItem destination)
        {
            base.CopyOriginalSizes(source, destination);
            destination.Width = source.Width;
            destination.Height = source.Height;
        }
        private void CopyOriginalSizes(LineEditorItem source, LineEditorItem destination)
        {
            base.CopyOriginalSizes(source, destination);
            destination.X1 = source.X1;
            destination.Y1 = source.Y1;
            destination.X2 = source.X2;
            destination.Y2 = source.Y2;
        }

        private void InitRectangleBase(RectangleEditorItem item)
        {
            item.X = this._rnd.Next(0, 50);
            item.Y = this._rnd.Next(0, 50);
            item.Width = 50;
            item.Height = 50;
            item.ZIndex = ContainedElements.Count;
            item.IsDirty = true;
        }

        private void InitLineBase(LineEditorItem item)
        {
            item.X = this._rnd.Next(0, 50);
            item.Y = this._rnd.Next(0, 50);
            item.X1 = item.X;
            item.Y1 = item.Y;
            item.X2 = item.X1 + 50;
            item.Y2 = item.Y1;
            item.ZIndex = ContainedElements.Count;
            item.IsDirty = true;
        }

        private T ProcessPropertiesViewModel<TView,T>(PropertiesViewModel<TView> propVM, T original, T copy)
            where TView: IView
            where T: EditorItemBase
        {
            if (propVM.ShowDialog(ParentView))
            {
                var index = Data.Items.IndexOf(original);
                Data.Items[index] = copy;
                return copy;
            }
            else
            {
                return original;
            }
        }

        #region command handler
        private void AddLineCommandHandler()
        {
            var item = new LineEditorItem();
            this._presetGraphicSettings.PresetLine.OverwritePresets(item);
            AddItem(item);
            InitLineBase(item);
        }

        private void AddImageCommandHandler()
        {
            var item = new ImageGPEditorItem();
            this._presetGraphicSettings.PresetGraphic.OverwritePresets(item);
            InitRectangleBase(item);
            AddItem(item);                    
        }

        private void AddTextBoxCommandHandler()
        {
            var item = new TextGPEditorItem();
            this._presetGraphicSettings.PresetText.OverwritePresets(item);
            InitRectangleBase(item);
            AddItem(item);         
        }

        private void SelectAspectRatioCommandHandler(AspectRatio aspectRatio)
        {
            this.Data.AspectRatio = aspectRatio;
        }
        #endregion
    }
}
