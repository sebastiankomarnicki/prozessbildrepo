﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class ButtonEditorItemViewModel : ViewModel<IButtonPropertiesView>
    {
        #region fields
        private readonly ExportFactory<PropertiesFontAndColorViewModel> _propertiesFontColorVMFactory;

        private object _fontAndColorView;
        private ButtonEditorItem _data;
        private IReadOnlyCollection<int> _textSizes;
        #endregion

        [ImportingConstructor]
        public ButtonEditorItemViewModel(IButtonPropertiesView view,
            ExportFactory<PropertiesFontAndColorViewModel> propertiesFontColorVMFactory
            ) : base(view)
        {
            this._propertiesFontColorVMFactory = propertiesFontColorVMFactory;
            this._textSizes = new List<int>() { 10, 12, 14, 18, 22, 26 };
        }

        public void Setup(object parentView, ButtonEditorItem data)
        {
            this.ParentView = parentView;
            this.Data = data;
            var fontColorVM = this._propertiesFontColorVMFactory.CreateExport().Value;
            fontColorVM.Setup(this.Data);
            this.FontAndColorView = fontColorVM.View;
        }

        public object ParentView { get; private set; }

        public object FontAndColorView {
            get { return _fontAndColorView; }
            private set { SetProperty(ref _fontAndColorView, value); }
        }

        public ButtonEditorItem Data
        {
            get { return _data; }
            private set
            {
                SetProperty(ref _data, value);
            }
        }

        public IReadOnlyCollection<int> TextSizes
        {
            get { return _textSizes; }
        }
    }
}
