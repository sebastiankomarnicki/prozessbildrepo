﻿using Promess.Common.Services;
using Promess.Common.Util.Async;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.Pbld.Utility;
using Promess.Pbld.ViewModels.SpcPathHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class SpcPathViewModel : ViewModel<ISpcPathView>, ISpcPathResult
    {
        private bool _isValid;
        private bool _checkHostNameOrAdressRunning;
        private string _status;

        private readonly IMessageService _messageService;
        private readonly IFileDialogServiceExtended _fileDialogService;
        private readonly IFolderCacheService _folderCacheService;
        private readonly PathsAndResourceService _pathsAndResourceService;

        private SpcPath _data;
        private IReadOnlyList<PanelItemModel> _availablePanels;

        private Func<short, bool> _isSpcNumberDuplicate;
        private Func<SpcPath, string, bool> _tryAddMultipleFolders;

        private AsyncCommand _checkHostNameOrAddress;
        private DelegateCommand _selectFolderCmd;

        [ImportingConstructor]
        public SpcPathViewModel(ISpcPathView view, 
            IMessageService messageService, IFolderCacheService folderCacheService, PathsAndResourceService pathsAndResourceService,
            ConfigurationService configurationService,IFileDialogServiceExtended fileDialogService) : base(view)
        {
            this._messageService = messageService;
            this._folderCacheService = folderCacheService;
            this._pathsAndResourceService = pathsAndResourceService;
            this._availablePanels = configurationService.GetPanels();
            this._fileDialogService = fileDialogService;
            this._status = null;

            PropertyChangedEventManager.AddHandler(this, CheckHostNameOrAdressRunningChangedHandler, nameof(CheckHostNameOrAdressRunning));
        }

        public void Initialize(SpcPath data, Func<short, bool> isSpcNumberDuplicate, Func<SpcPath, string, bool> tryAddMultipleFolders)
        {
            this._data = data;
            this._isSpcNumberDuplicate = isSpcNumberDuplicate;
            this._tryAddMultipleFolders = tryAddMultipleFolders;
            this._selectFolderCmd = new DelegateCommand(SelectFolderHandler,()=>!CheckHostNameOrAdressRunning);
            this._checkHostNameOrAddress = new AsyncCommand(CheckHostNameOrAdressCommandHandlerAsync);
            PropertyChangedEventManager.AddHandler(this._data, HandleSpcNumberChangedDuplicationCheck, nameof(SpcPath.SpcNumber));
        }

        private void HandleSpcNumberChangedDuplicationCheck(object sender, PropertyChangedEventArgs e)
        {
            SetSpcNumberDuplicationState(this._isSpcNumberDuplicate(this._data.SpcNumber));
        }

        internal void SetSpcNumberDuplicationState(bool isDuplicate)
        {
            if (isDuplicate)
            {
                Status = (string)LanguageManager.Current.CurrentLanguageData.Translate("ESpcNumberDuplicate");
            }
            else
            {
                Status = null;
            }
        }

        private async Task CheckHostNameOrAdressCommandHandlerAsync(CancellationToken cts)
        {
            CheckHostNameOrAdressRunning = true;
            bool reply;
            if (String.IsNullOrWhiteSpace(SpcPath.ComputerName)){
                reply = false;
            }else
            {
                reply = await PingAsync(SpcPath.ComputerName);
            }
            if (!cts.IsCancellationRequested)
            {
                if (reply)
                {
                    _messageService.ShowMessage(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("FoundHostNameOrAddress"));
                }
                else
                {
                    _messageService.ShowWarning(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("WCouldNotFindHostNameOrAddress"));
                }
            }
            CheckHostNameOrAdressRunning = false;
        }

        private void SelectFolderHandler()
        {
            string defaultFolder = GetDefaultFolder();
            var selectedFolder = _fileDialogService.ShowOpenFolderDialog(View, defaultFolder);
            if (String.IsNullOrWhiteSpace(selectedFolder))
                return;
            _folderCacheService.SpcPath = selectedFolder;
            if (FileSystemHelper.IsLeafDirectory(selectedFolder)||
                !_messageService.ShowYesNoQuestion(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("QImportLeafFolders")))
            {
                _data.DataPath = selectedFolder;
            }
            else
            {
                this._tryAddMultipleFolders(this._data, selectedFolder);
            }
        }

        private string GetDefaultFolder()
        {
            if (Directory.Exists(_data.DataPath))
                return _data.DataPath;
            if (Directory.Exists(_folderCacheService.SpcPath))
                return _folderCacheService.SpcPath;
            return _pathsAndResourceService.UserWorksPath;
        }

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                SetProperty(ref _isValid, value);
            }
        }

        public string Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }

        public IReadOnlyList<PanelItemModel> AvailablePanels { get { return _availablePanels; } }

        public SpcPath SpcPath { get { return _data; } }

        public ICommand SelectFolderCommand { get { return _selectFolderCmd; } }
        public ICommand CheckHostNameOrAddressCommand { get { return _checkHostNameOrAddress; } }

        private bool CheckHostNameOrAdressRunning
        {
            get { return _checkHostNameOrAdressRunning; }
            set { SetProperty(ref _checkHostNameOrAdressRunning, value); }
        }

        private async Task<bool> PingAsync(string hostNameOrAddress)
        {
            try
            {
                using (var myPing = new Ping())
                {
                    PingReply reply = await myPing.SendPingAsync(hostNameOrAddress,200);
                    return reply.Status == IPStatus.Success;
                }
            }catch(PingException)
            {
                return false;
            }
        }

        bool ISpcPathResult.IsValid()
        {
            return !this._isSpcNumberDuplicate(this._data.SpcNumber) && IsValid;
        }

        public void CancelOutstandingOperations()
        {
            this._checkHostNameOrAddress.CancelCommand.Execute(null);
        }

        #region event handlers
        private void CheckHostNameOrAdressRunningChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            this._selectFolderCmd?.RaiseCanExecuteChanged();
        }

        public void RemoveEventHandlers()
        {
            PropertyChangedEventManager.RemoveHandler(this, CheckHostNameOrAdressRunningChangedHandler, nameof(CheckHostNameOrAdressRunning));
            if (this._data is object)
                PropertyChangedEventManager.RemoveHandler(this._data, HandleSpcNumberChangedDuplicationCheck, nameof(SpcPath.SpcNumber));
        }
        #endregion
    }
}
