﻿using Promess.Common.Util;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using Promess.QsStat.Data;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Data;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.Shared)]
    public class SpcPathOverviewViewModel : ViewModel<ISpcPathOverviewView>
    {
        #region fields
        private readonly QsStatService _qsStatService;

        private List<QdasNumberWrapper> _partColumnConfig;
        private CollectionView _areaView;
        private bool _partFilterApplied;
        private ObservableCollection<Tuple<SpcPath, QsStatPartResultDTO>> _areaData;
        #endregion

        [ImportingConstructor]
        public SpcPathOverviewViewModel(ISpcPathOverviewView view,
            QsStatService qsStatService) : base(view)
        {
            this._qsStatService = qsStatService;
        }

        internal void Initialize(object parentView, ICollection<QdasNumberWrapper> initialColumns)
        {
            this.ParentView = parentView;
            this._partColumnConfig = new List<QdasNumberWrapper>(initialColumns);
            PropertyChangedEventManager.AddHandler(this._qsStatService, SpcPathResultsChangedHandler, nameof(QsStatService.SpcPathResults));
            PropertyChangedEventManager.AddHandler(this._qsStatService, PartKFieldsChangedHandler, nameof(QsStatService.PartKFields));
            GenerateDataView();
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshDataCollectionHandler);
        }

        internal void UpdatePartColumnConfig(List<QdasNumberWrapper> newConfig)
        {
            this._partColumnConfig = newConfig;         
            RaisePropertyChanged(nameof(PartColumnConfig));
        }

        #region properties
        public object ParentView { get; private set; }

        public CollectionView AreaView
        {
            get { return _areaView; }
            private set { SetProperty(ref _areaView, value); }
        }

        public IReadOnlyCollection<QdasNumberWrapper> PartColumnConfig
        {
            get { return _partColumnConfig; }
        }

        public bool PartFilterApplied
        {
            get { return _partFilterApplied; }
            private set { SetProperty(ref _partFilterApplied, value); }
        }

        public IReadOnlyDictionary<int, string> PartKFields
        {
            get { return _qsStatService.PartKFields; }
        }

        #endregion

        internal void ShowAllSpc()
        {
            AreaView.Filter = null;
            PartFilterApplied = false;
        }

        internal void ShowSpc(SpcPath spcPath)
        {
            PartFilterApplied = true;
            AreaView.Filter = ((item) => FilterHelper(item, spcPath.SpcNumber));
        }

        private void GenerateDataView()
        {
            var dataList = new List<Tuple<SpcPath, QsStatPartResultDTO>>();
            var groupDescription = new PropertyGroupDescription("Item1");
            foreach (var spcPathResult in _qsStatService.SpcPathResults.OrderBy(kvp => kvp.Key.SpcNumber))
            {
                groupDescription.GroupNames.Add(spcPathResult.Key);
                foreach (var partResult in spcPathResult.Value.EvaluatedData.Values)
                {
                    dataList.Add(new Tuple<SpcPath, QsStatPartResultDTO>(spcPathResult.Key, partResult));
                }
            }
            this._areaData = new ObservableCollection<Tuple<SpcPath, QsStatPartResultDTO>>(dataList);
            var view = (CollectionView)CollectionViewSource.GetDefaultView(this._areaData);
            view.GroupDescriptions.Add(groupDescription);
            //TODO maybe sorting, though I did not get group sorting to work
            AreaView = view;
        }

        #region event handlers
        private void SpcPathResultsChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            DispatchHelper.BeginInvoke(SpcPathResultsChangedHandlerImpl);
        }

        private void SpcPathResultsChangedHandlerImpl()
        {
            GenerateDataView();
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshDataCollectionHandler);
        }

        private void PartKFieldsChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(PartKFields));
        }

        private void RefreshDataCollectionHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add)
                return;

            var replacedData = e.NewItems[0] as KeyValuePair<SpcPath, SpcPathEvaluationDTO>?;
            if (!replacedData.HasValue)
                throw new ArgumentOutOfRangeException($"{nameof(NotifyCollectionChangedEventArgs)} is missing the replaced element");
            DispatchHelper.BeginInvoke(()=> RefreshData(replacedData.Value.Key, replacedData.Value.Value));
        }

        private void RefreshData(SpcPath spcPath, SpcPathEvaluationDTO newData)
        {
            foreach (var elem in this._areaData.Where(tuple => tuple.Item1.Equals(spcPath)).ToList())
                this._areaData.Remove(elem);
            foreach (var elem in newData.EvaluatedData.Values)
                this._areaData.Add(new Tuple<SpcPath, QsStatPartResultDTO>(spcPath, elem));
        }
        #endregion

        #region helpers
        private bool FilterHelper(object toCheck, int spcNumber)
        {
            var item = toCheck as Tuple<SpcPath, QsStatPartResultDTO>;
            if (item == null)
                return false;
            return item.Item1.SpcNumber == spcNumber;
        }
        #endregion
    }
}
