﻿using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.IViews;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Data;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export,PartCreationPolicy(CreationPolicy.NonShared)]
    public class AreaSpcPathsViewModel : ViewModel<IAreaSpcPathsView>
    {
        #region fields
        private bool _isEnabled;
        private object _parentView;
        private Area _area;

        private IMessageService _messageService;

        private ExportFactory<MetaSpcPathViewModel> _metaSpcPathVMFactory;

        private DelegateCommand _editSpcPathCmd;
        private DelegateCommand _deleteSpcPathCmd;
        private DelegateCommand _addSpcPathCmd;

        private SpcPath _selectedSpcPath;
        private ICollectionView _sortedSpcPathsView;
        #endregion

        [ImportingConstructor]
        public AreaSpcPathsViewModel(IAreaSpcPathsView view, IMessageService messageService, ExportFactory<MetaSpcPathViewModel> metaSpcPathVMFactory) : base(view)
        {
            this._messageService = messageService;
            this._metaSpcPathVMFactory = metaSpcPathVMFactory;
            this._isEnabled = false;
        }

        public void Initialize(object parentView, Area area)
        {
            this._parentView = parentView;
            this._area = area;
            //TODO would it be better to allow multiselect here now?
            this._editSpcPathCmd = new DelegateCommand(EditSpcPathHandler, () => SelectedSpcPath != null);
            this._deleteSpcPathCmd = new DelegateCommand(DeleteSpcPathHandler, () => SelectedSpcPath != null);
            this._addSpcPathCmd = new DelegateCommand(AddSpcPathHandler, () => IsEnabled && _area.CanAddPath());
            PropertyChangedEventManager.AddHandler(this, SelectedSpcPathChangedHandler, nameof(SelectedSpcPath));
        }

        public void SetArea(Area area)
        {
            this.SelectedArea = area;
            IsEnabled = this.SelectedArea != null;
            SelectedSpcPath = null;
            SetSpcPathsView();
            this._addSpcPathCmd.RaiseCanExecuteChanged();      
        }

        private void SetSpcPathsView()
        {
            IList items = this.SelectedArea?.ReadonlyPaths;
            items = items?? new List<SpcPath>();
            var collectionView = new ListCollectionView(items);
            collectionView.SortDescriptions.Add(new SortDescription(nameof(SpcPath.SpcNumber), ListSortDirection.Ascending));
            collectionView.IsLiveSorting = true;
            this.SortedSpcPathsView = collectionView;
        }

        private void AddSpcPathHandler()
        {
            short? maybeSpcNumber = _area.GetNextFreeSpcNumber();
            if (!maybeSpcNumber.HasValue)
            {
                _messageService.ShowWarning(_parentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("WSpcNumbersExhausted"));
                return;
            }

            var vm = _metaSpcPathVMFactory.CreateExport().Value;

            if (!vm.Setup(maybeSpcNumber.Value, _area.ReadonlyPaths.Count,_area.DoesSpcNumberAlreadyExist))
            {
                _messageService.ShowWarning(_parentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("WCreateSpcMaybePanelsMissing"));
                return;
            }
            EventHandler<PropertyChangedEventArgs> closeHandler = new EventHandler<PropertyChangedEventArgs>((s, e) => vm.Close());
            PropertyChangedEventManager.AddHandler(this, closeHandler, nameof(SelectedArea));
            if (vm.ShowDialog(_parentView))
            {
                _area.AddSpcPaths(vm.GetSpcPaths());
                _addSpcPathCmd.RaiseCanExecuteChanged();
            }
            PropertyChangedEventManager.RemoveHandler(this, closeHandler, nameof(SelectedArea));
        }

        private void DeleteSpcPathHandler()
        {
            _area.RemoveSpcPath(SelectedSpcPath);
            SelectedSpcPath = null;
            _addSpcPathCmd.RaiseCanExecuteChanged();
        }

        private void EditSpcPathHandler()
        {
            SpcPath spcCopy = SerializeHelper.CreateDataContractCopy(SelectedSpcPath);
            var vm = _metaSpcPathVMFactory.CreateExport().Value;
            short nextSpcNumber = _area.GetNextFreeSpcNumber() ?? SpcPath.MAXSPCNUMBER;
            vm.Setup(nextSpcNumber, _area.ReadonlyPaths.Count, spcCopy,number=>number!=SelectedSpcPath.SpcNumber&&_area.DoesSpcNumberAlreadyExist(number));
            EventHandler<PropertyChangedEventArgs> closeHandler = new EventHandler<PropertyChangedEventArgs>((s, e) => vm.Close());
            PropertyChangedEventManager.AddHandler(this, closeHandler, nameof(SelectedArea));
            if (vm.ShowDialog(_parentView))
            {
                var spcPaths = vm.GetSpcPaths().ToList();
                int copyIndex = spcPaths.IndexOf(spcCopy);
                if (copyIndex >= 0)
                {
                    _area.ReplaceSpcPath(SelectedSpcPath, spcCopy);
                    spcPaths.RemoveAt(copyIndex);
                    SelectedSpcPath = spcCopy;
                }
                else
                {
                    _area.RemoveSpcPath(SelectedSpcPath);
                }             
                _area.AddSpcPaths(spcPaths);
                _addSpcPathCmd.RaiseCanExecuteChanged();
            }
            PropertyChangedEventManager.RemoveHandler(this, closeHandler, nameof(SelectedArea));
        }

        private void SelectedSpcPathChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            this._editSpcPathCmd.RaiseCanExecuteChanged();
            this._deleteSpcPathCmd.RaiseCanExecuteChanged();
        }

        #region properties
        public bool IsEnabled
        {
            get { return _isEnabled; }
            private set
            {
                SetProperty(ref _isEnabled, value);
            }
        }

        public Area SelectedArea
        {
            get { return _area; }
            private set { SetProperty(ref _area, value); }
        }
        public ICommand EditSpcPathCommand { get { return _editSpcPathCmd; } }
        public ICommand DeleteSpcPathCommand { get { return _deleteSpcPathCmd; } }
        public ICommand AddSpcPathCommand { get { return _addSpcPathCmd; } }
        public SpcPath SelectedSpcPath
        {
            get { return _selectedSpcPath; }
            set { SetProperty(ref _selectedSpcPath, value); }
        }
        public ICollectionView SortedSpcPathsView
        {
            get { return _sortedSpcPathsView; }
            private set { SetProperty(ref _sortedSpcPathsView, value); }
        }
        #endregion
    }
}
