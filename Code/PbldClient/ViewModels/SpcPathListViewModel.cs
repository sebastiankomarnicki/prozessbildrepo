﻿using Microsoft.VisualStudio.Language.Intellisense;
using Promess.Pbld.Data;
using Promess.Pbld.IViews;
using Promess.Pbld.ViewModels.SpcPathHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class SpcPathListViewModel: ViewModel<ISpcPathListView>, ISpcPathResult
    {
        #region fields
        private readonly ExportFactory<SpcPathViewModel> _spcPathVMFactory;

        private MetaSpcPathViewModel _metaVM;
        //FIXME change to dictionary of SpcPath, SpcPathViewModel
        private List<SpcPathViewModel> _spcPathViewModels;
        private Dictionary<SpcPath, short> _spcPathToLastSpcNumber;
        private Dictionary<short, List<SpcPath>> _spcNumberToSpcPaths;
        private BulkObservableCollection<object> _spcPathViews;
        private int _selectedIndex;
        private Action _tryTransitionToSpcPathVM;
        #endregion

        [ImportingConstructor]
        public SpcPathListViewModel(ISpcPathListView view,
            ExportFactory<SpcPathViewModel> spcPathVMFactory) : base(view)
        {
            this._spcPathViewModels = new List<SpcPathViewModel>();
            this._spcPathToLastSpcNumber = new Dictionary<SpcPath, short>();
            this._spcNumberToSpcPaths = new Dictionary<short, List<SpcPath>>();
            this._spcPathVMFactory = spcPathVMFactory;
            this.AddSpcCommand = new DelegateCommand(AddSpcCommandHandler);
            this.RemoveSpcCommand = new DelegateCommand(RemoveSpcCommandHandler, () => SelectedIndex >= 0 && this._spcPathViewModels.Count>1);
        }

        public void Setup(MetaSpcPathViewModel metaVM, IReadOnlyCollection<SpcPath> initialSpcPaths, Action tryTransitionToSpcPathVM)
        {
            this._metaVM = metaVM;
            this._spcPathViewModels = GenerateSpcPathVMs(initialSpcPaths);
            this._tryTransitionToSpcPathVM = tryTransitionToSpcPathVM;
            this._spcPathViews = new BulkObservableCollection<object>();
            this._spcPathViews.AddRange(this._spcPathViewModels.Select(item => item.View));
            this.SpcPathViews = new ReadOnlyObservableCollection<object>(this._spcPathViews);
            foreach (var vm in this._spcPathViewModels)
            {
                AddVMToDuplicationCheckingAndCheck(vm);
            }
        }

        private void HandleSpcNumberChanged(object sender, PropertyChangedEventArgs e)
        {
            
            var spcPath = (SpcPath)sender;
            var oldNumber = this._spcPathToLastSpcNumber[spcPath];
            RemoveSpcFromNumberTrackingAndCheckDuplication(spcPath, oldNumber);
            SpcPathViewModel vm = this._spcPathViewModels.First(item => item.SpcPath.Equals(spcPath)); ;
            AddVMAndCheckDuplication(vm);
        }

        private void RemoveSpcFromNumberTrackingAndCheckDuplication(SpcPath spcPath, short spcNumber)
        {
            var spcNumberDuplicates = this._spcNumberToSpcPaths[spcNumber];
            spcNumberDuplicates.Remove(spcPath);
            if (spcNumberDuplicates.Count == 0)
            {
                this._spcNumberToSpcPaths.Remove(spcNumber);
            }
            else if (spcNumberDuplicates.Count == 1)
            {
                var toRefresh = spcNumberDuplicates.First();
                var vm = this._spcPathViewModels.First(item => item.SpcPath.Equals(toRefresh));
                vm.SetSpcNumberDuplicationState(this._metaVM.IsSpcNumberDuplicate(toRefresh.SpcNumber));
            }
        }

        private List<SpcPathViewModel> GenerateSpcPathVMs(IReadOnlyCollection<SpcPath> spcPaths)
        {
            var vms = new List<SpcPathViewModel>();
            foreach (var spcPath in spcPaths)
            {
                vms.Add(GenerateSpcPathVM(spcPath));
            }
            return vms;
        }

        private bool TryAddMultipleSpcPaths(SpcPath toModify, string root)
        {
            (var added, var generated) = this._metaVM.TryAddMultipleSpcPaths(toModify, root);
            if (!added)
                return false;
            var vms = GenerateSpcPathVMs(generated);
            foreach (var vm in vms)
                AddVMToDuplicationCheckingAndCheck(vm);
            this._spcPathViewModels.AddRange(vms);
            this._spcPathViews.AddRange(vms.Select(vm => vm.View));
            this.SelectedIndex = this._spcPathViews.Count - 1;
            return true;
        }

        private SpcPathViewModel GenerateSpcPathVM(SpcPath spcPath)
        {
            var vm = this._spcPathVMFactory.CreateExport().Value;
            vm.Initialize(spcPath, this._metaVM.IsSpcNumberDuplicate, this.TryAddMultipleSpcPaths);
            return vm;
        }

        public bool IsValid()
        {
            if (this._spcPathViewModels.Any(vm => !vm.IsValid))
                return false;
            if (this._spcNumberToSpcPaths.Count != this._spcPathToLastSpcNumber.Count)
                return false;
            return !this._spcNumberToSpcPaths.Keys.Any(number => this._metaVM.IsSpcNumberDuplicate(number));
        }

        public void CancelOutstandingOperations()
        {
            foreach (var vm in this._spcPathViewModels)
                vm.CancelOutstandingOperations();
        }

        #region properties
        public ReadOnlyObservableCollection<object> SpcPathViews { get; private set; }
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set { if (SetProperty(ref _selectedIndex, value)) {
                    RemoveSpcCommand.RaiseCanExecuteChanged();
                } }
        }
        public DelegateCommand RemoveSpcCommand { get; private set; }
        public ICommand AddSpcCommand { get; private set; }
        #endregion
        #region command handlers
        private void RemoveSpcCommandHandler()
        {
            int removeIndex = SelectedIndex;
            var removeSpc = this._spcPathViewModels[SelectedIndex].SpcPath;
            if (!this._metaVM.RemoveSpc(removeSpc))
                return;
            if (this._spcPathViewModels.Count == 2)
            {
                _tryTransitionToSpcPathVM();
                return;
            }
            this._spcPathToLastSpcNumber.Remove(removeSpc);
            RemoveSpcFromNumberTrackingAndCheckDuplication(removeSpc, removeSpc.SpcNumber);
            PropertyChangedEventManager.RemoveHandler(removeSpc, HandleSpcNumberChanged, nameof(SpcPath.SpcNumber));
            this._spcPathViewModels.RemoveAt(removeIndex);
            this._spcPathViews.RemoveAt(removeIndex);
            this.RemoveSpcCommand.RaiseCanExecuteChanged();           
        }

        private void AddSpcCommandHandler()
        {
            SpcPath maybeSelected = null;
            if (SelectedIndex >= 0)
                maybeSelected = this._spcPathViewModels[SelectedIndex].SpcPath;
            (var success, var newSpcPath) = this._metaVM.TryGenerateAndAddSpcPath(maybeSelected);
            if (!success)
                return;

            var generatedVM = GenerateSpcPathVM(newSpcPath);
            AddVMToDuplicationCheckingAndCheck(generatedVM);

            this._spcPathViewModels.Add(generatedVM);
            this._spcPathViews.Add(generatedVM.View);
            this.SelectedIndex = this._spcPathViews.Count - 1;
        }

        private void AddVMToDuplicationCheckingAndCheck(SpcPathViewModel toAdd)
        {
            AddVMAndCheckDuplication(toAdd);
            PropertyChangedEventManager.AddHandler(toAdd.SpcPath, HandleSpcNumberChanged, nameof(SpcPath.SpcNumber));
        }

        private void AddVMAndCheckDuplication(SpcPathViewModel toAdd)
        {
            this._spcPathToLastSpcNumber[toAdd.SpcPath] = toAdd.SpcPath.SpcNumber;
            List<SpcPath> spcList = null;
            if (!this._spcNumberToSpcPaths.TryGetValue(toAdd.SpcPath.SpcNumber, out spcList))
            {
                spcList = new List<SpcPath>();
                this._spcNumberToSpcPaths[toAdd.SpcPath.SpcNumber] = spcList;
            }
            else if (spcList.Count == 1)
            {
                var oldSpcPathVM = this._spcPathViewModels.First(item => item.SpcPath.Equals(spcList.First()));
                oldSpcPathVM.SetSpcNumberDuplicationState(true);
            }
            spcList.Add(toAdd.SpcPath);
            toAdd.SetSpcNumberDuplicationState(spcList.Count >= 2 || this._metaVM.IsSpcNumberDuplicate(toAdd.SpcPath.SpcNumber));          
        }
        #endregion

        #region event handlers
        public void RemoveEventHandlers()
        {
            foreach (var vm in this._spcPathViewModels)
            {
                vm.RemoveEventHandlers();
                PropertyChangedEventManager.RemoveHandler(vm.SpcPath, HandleSpcNumberChanged, nameof(SpcPath.SpcNumber));
            }
        }
        #endregion
    }
}
