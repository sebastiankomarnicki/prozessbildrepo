﻿using Promess.Common.Services;
using Promess.Language;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using Promess.Pbld.Utility;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class ImageEditorItemViewModel : ViewModel<IImagePropertiesView>
    {
        #region fields
        private readonly IMessageService _messageService;
        private IQsStatService _qsStatService;
        private IFileDialogServiceExtended _fileDialogService;
        private ExportFactory<PropertiesRepresentationGraphicViewModel> _borderVMFactory;

        private object _borderView;
        private ImageEditorItem _data;
        private ICommand _selectImageCommand;
        #endregion

        [ImportingConstructor]
        public ImageEditorItemViewModel(IImagePropertiesView view, IMessageService messageService, IQsStatService qsStatService,
            IFileDialogServiceExtended fileDialogService, ExportFactory<PropertiesRepresentationGraphicViewModel> borderVMFactory) : base(view)
        {
            this._messageService = messageService;
            this._qsStatService = qsStatService;
            this._fileDialogService = fileDialogService;
            this._borderVMFactory = borderVMFactory;
            this._selectImageCommand = new DelegateCommand(SelectImageCommandLogic);
        }

        internal void Setup(object parentView, ImageEditorItem item)
        {
            this.ParentView = parentView;
            Data = item;
            var borderVm = this._borderVMFactory.CreateExport().Value;
            borderVm.Setup(item);
            this.BorderView = borderVm.View;
        }

        #region  properties
        public object ParentView { get; private set; }

        public object BorderView
        {
            get { return _borderView; }
            private set { SetProperty(ref _borderView, value); }
        }

        public IReadOnlyDictionary<int, string> QdasEvaluations
        {
            get { return _qsStatService.Graphics; }
        }

        public ImageEditorItem Data
        {
            get { return _data; }
            private set
            {
                SetProperty(ref _data, value);
            }
        }

        public ICommand SelectImageCommand
        {
            get { return _selectImageCommand; }
        }
        #endregion

        #region helpers
        private void SelectImageCommandLogic()
        {
            FileType ft = new FileType("jpg, png, bmp",new string[]{ "*.jpg", "*.png", "*.bmp" });
            var filename = _fileDialogService.ShowOpenFileDialog(ParentView??View, ft, Data.ImagePath, true).FirstOrDefault();

            if (!String.IsNullOrWhiteSpace(filename))
            {
                var trimmedPath = filename.Trim();
                try
                {
                    byte[] image = ImageHelper.GetImageFromPathAsByteArray(trimmedPath);
                    Data.ImageSourceType = ImageSourceType.File;
                    Data.ImagePath = trimmedPath;
                    Data.Image = image;
                }
                catch
                {
                    string message = (string)LanguageManager.Current.CurrentLanguageData.Translate("ELoadingFile");
                    this._messageService.ShowError(this.View, message);
                }
            }
        }
        #endregion
    }
}
