﻿using Promess.Pbld.Data.Settings.PresetEditorItems;
using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class PropertiesRepresentationGraphicViewModel: ViewModel<IPropertiesRepresentationGraphicView>
    {
        [ImportingConstructor]
        public PropertiesRepresentationGraphicViewModel(IPropertiesRepresentationGraphicView view) : base(view) { }

        public void Setup(IPresetRepresentationGraphic data)
        {
            this.Data = data;
        }

        public IPresetRepresentationGraphic Data { get; private set; }
    }
}
