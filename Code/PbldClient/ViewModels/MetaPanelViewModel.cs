﻿using Promess.Pbld.IViews;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using Promess.Pbld.Data;
using Promess.QsStat.Data;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Services;
using System.Collections.Specialized;
using Promess.Common.Util;
using System.Windows;
using Promess.Common.Util.Async;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class MetaPanelViewModel : ViewModel<IMetaPanelView>
    {
        #region fields
        private readonly QsStatService _qsStatService;
        private readonly ConfigurationService _configurationService;
        private readonly ExportFactory<PanelViewModel> _panelVMFactory;
        private readonly Dictionary<SpcPath, PanelViewModel> _spcPathPanelVMMapping;

        private IReadOnlyCollection<PanelViewModel> _roPanelViewModels;
        private IReadOnlyCollection<PanelViewModel> _sizeReferenceModels;
        #endregion

        [ImportingConstructor]
        public MetaPanelViewModel(IMetaPanelView view, QsStatService qsStatService,
            ConfigurationService configService,
            ExportFactory<PanelViewModel> panelVMFactory) : base(view)
        {
            this._qsStatService = qsStatService;
            this._configurationService = configService;
            this._panelVMFactory = panelVMFactory;
            this._spcPathPanelVMMapping = new Dictionary<SpcPath, PanelViewModel>();
        }

        public void Initialize(object parentView, DelegateCommand<SpcPath> showListViewCommand)
        {
            this.ParentView = parentView;
            this.ShowListViewCommand = showListViewCommand;
            PropertyChangedEventManager.AddHandler(_qsStatService, SpcPathResultsChangedHandler, nameof(QsStatService.SpcPathResults));
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshVMHandler);
            InitializeVMs();
            this._sizeReferenceModels = SetupSizeChange();
        }

        private IReadOnlyCollection<PanelViewModel> SetupSizeChange()
        {
            var alreadyProcessed = new HashSet<string>();
            var sizeChangeModels = new List<PanelViewModel>();
            foreach (var model in PanelViewModels)
            {
                if (alreadyProcessed.Contains(model.PanelItemModel.Name))
                    continue;
                alreadyProcessed.Add(model.PanelItemModel.Name);
                sizeChangeModels.Add(model);
                SetupSizeChange(model);
            }
            return sizeChangeModels.AsReadOnly();
        }

        private void SetupSizeChange(PanelViewModel model)
        {
            WeakEventManager<PanelViewModel, EventArgs>.AddHandler(model, nameof(PanelViewModel.Loaded), ViewLoadedHandler);
            WeakEventManager<PanelViewModel, SizeChangedEventArgs>.AddHandler(model, nameof(PanelViewModel.SizeChanged), ViewSizeChangedHandler);
        }

        private void InitializeVMs()
        {
            GeneratePanelViewModels(_qsStatService.SpcPathFolderDatasets);
            foreach (var spcPathResult in _qsStatService.SpcPathResults)
            {
                PanelViewModel tmp = null;
                if (_spcPathPanelVMMapping.TryGetValue(spcPathResult.Key, out tmp))
                {
                    tmp.UpdateData(spcPathResult.Value);
                }
            }
        }

        private void RefreshVMHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add)
                return;
            var replacedData = e.NewItems[0] as KeyValuePair<SpcPath, SpcPathEvaluationDTO>?;
            if (!replacedData.HasValue)
                throw new ArgumentOutOfRangeException($"{nameof(NotifyCollectionChangedEventArgs)} is missing the replaced element");
            DispatchHelper.BeginInvoke(()=>RefreshVMHandlerImpl(replacedData.Value));
        }

        private void RefreshVMHandlerImpl(KeyValuePair<SpcPath, SpcPathEvaluationDTO> replacedData)
        {
            PanelViewModel vm;
            if (this._spcPathPanelVMMapping.TryGetValue(replacedData.Key, out vm))
            {
                vm.UpdateData(replacedData.Value);
            }
        }

        #region properties
        public object ParentView { get; private set; }

        public IReadOnlyCollection<PanelViewModel> PanelViewModels {
            get { return _roPanelViewModels; }
            private set { SetProperty(ref _roPanelViewModels, value); }
        }

        public DelegateCommand<SpcPath> ShowListViewCommand
        {
            get; private set;
        }
        #endregion

        #region helper functions
        private void GeneratePanelViewModels(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders)
        {
            List<PanelViewModel> panelVMs = new List<PanelViewModel>();
            foreach (var spcPathFolder in spcPathFolders)
            {
                SpcPath path = spcPathFolder.Key;
                var retrieved = _configurationService.GetSavedPanelCopy(path.PanelConfigurationName);
                if (!retrieved.Item1)
                {
                    //this should only ever happen on an update cycle, if ever. in which case another update will be run
                    continue;
                }
                var panelItemModel = retrieved.Item2;

                var tmpPanelViewModel = _panelVMFactory.CreateExport().Value;
                tmpPanelViewModel.ParentView = ParentView;
                var nextPartCommand = new AsyncCommand(async () => { tmpPanelViewModel.RequestedNewData = true; await this._qsStatService.RequestNextPart(path); }, () => !tmpPanelViewModel.RequestedNewData && tmpPanelViewModel.HasNextPart);
                var previousPartCommand = new AsyncCommand(async () => { tmpPanelViewModel.RequestedNewData = true; await _qsStatService.RequestPreviousPart(path); }, () => !tmpPanelViewModel.RequestedNewData && tmpPanelViewModel.HasPreviousPart);
                var nextCharacteristicCommand = new AsyncCommand(async () => { tmpPanelViewModel.RequestedNewData = true; await _qsStatService.RequestNextCharacteristic(path); }, () => !tmpPanelViewModel.RequestedNewData && tmpPanelViewModel.HasNextCharacteristic);
                var previousCharacteristicCommand = new AsyncCommand(async () => { tmpPanelViewModel.RequestedNewData = true; await _qsStatService.RequestPreviousCharacteristic(path); }, () => !tmpPanelViewModel.RequestedNewData && tmpPanelViewModel.HasPreviousCharacteristic);
                var refreshCommand = new AsyncCommand(async () => { await this._qsStatService.RequestRefresh(path); });
                tmpPanelViewModel.Initialize(panelItemModel, path, refreshCommand, nextPartCommand, previousPartCommand, nextCharacteristicCommand, previousCharacteristicCommand);
                panelVMs.Add(tmpPanelViewModel);
                this._spcPathPanelVMMapping[path] = tmpPanelViewModel;
            }
            PanelViewModels = panelVMs;
        }
        #endregion

        #region event handlers
        private void SpcPathResultsChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            DispatchHelper.BeginInvoke(SpcPathResultsChangedHandlerImpl);
        }

        private void SpcPathResultsChangedHandlerImpl()
        {
            foreach (var model in this._sizeReferenceModels)
            {
                WeakEventManager<PanelViewModel, EventArgs>.RemoveHandler(model, nameof(PanelViewModel.Loaded), ViewLoadedHandler);
                WeakEventManager<PanelViewModel, SizeChangedEventArgs>.RemoveHandler(model, nameof(PanelViewModel.SizeChanged), ViewSizeChangedHandler);
            }

            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshVMHandler);
            InitializeVMs();
            this._sizeReferenceModels = SetupSizeChange();

            foreach (var model in this._sizeReferenceModels)
            {
                this._qsStatService.SetPanelHeightAndWidth(model.PanelName, model.ViewHeight, model.ViewWidth);
            }
        }

        private void ViewLoadedHandler(object sender, EventArgs args)
        {
            var model = (PanelViewModel)sender;
            this._qsStatService.SetPanelHeightAndWidth(model.PanelName, model.ViewHeight, model.ViewWidth);
        }

        private void ViewSizeChangedHandler(object sender, SizeChangedEventArgs sizeArgs)
        {
            var model = (PanelViewModel)sender;
            this._qsStatService.SetPanelHeightAndWidth(model.PanelName, model.ViewHeight, model.ViewWidth);
        }
        #endregion
    }
}
