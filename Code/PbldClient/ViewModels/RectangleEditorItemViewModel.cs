﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class RectangleEditorItemViewModel : ViewModel<IRectanglePropertiesView>
    {
        #region fields
        private RectangleEditorItem _data;
        #endregion

        [ImportingConstructor]
        public RectangleEditorItemViewModel(IRectanglePropertiesView view) : base(view)
        {
        }

        #region  properties
        public object ParentView { get; set; }

        public RectangleEditorItem Data
        {
            get { return _data; }
            set
            {
                SetProperty(ref _data, value);
            }
        }
        #endregion
    }
}
