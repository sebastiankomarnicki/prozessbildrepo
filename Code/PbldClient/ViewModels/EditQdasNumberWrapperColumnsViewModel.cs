﻿using Promess.Language;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class EditQdasNumberWrapperColumnsViewModel : ViewModel<IEditQdasNumberWrapperColumnsView>
    {
        #region fields
        private bool _dialogResult;
        private bool _isValid;

        private readonly ExportFactory<SelectQdasNumberWrapperViewModel> _selectQdasNumberWrapperViewModelFactory;

        private ObservableCollection<Tuple<QdasNumberWrapper, string>> _selectedQdasNumbersWithTranslation;
        private Dictionary<TextEvaluationType, List<Tuple<QdasNumber, string>>> _freeQdasNumbers;
        private int _selectedIndex;

        private DelegateCommand _addCommand;
        private DelegateCommand _removeCommand;
        private DelegateCommand _moveUpCommand;
        private DelegateCommand _moveDownCommand;
        private DelegateCommand _okCommand;
        #endregion

        [ImportingConstructor]
        public EditQdasNumberWrapperColumnsViewModel(IEditQdasNumberWrapperColumnsView view, ExportFactory<SelectQdasNumberWrapperViewModel> selectQdasNumberWrapperViewModelFactory) : base(view)
        {
            this._selectQdasNumberWrapperViewModelFactory = selectQdasNumberWrapperViewModelFactory;
            this._dialogResult = false;
            this._selectedQdasNumbersWithTranslation = new ObservableCollection<Tuple<QdasNumberWrapper, string>>();
            this._addCommand = new DelegateCommand(AddQdasNumberCommandHandler);
            this._removeCommand = new DelegateCommand(RemoveQdasNumberCommandHandler, () => SelectedIndex > -1 && SelectedQdasNumbersWithTranslation.Count >= 2);
            this._moveUpCommand = new DelegateCommand(MoveQdasNumberUpCommandHandler, CanMoveQdasNumberUp);
            this._moveDownCommand = new DelegateCommand(MoveQdasNumberDownCommandHandler, CanMoveQdasNumberDown);
            this._okCommand = new DelegateCommand(OkCommandHandler, ()=> IsValid);
        }

        public void Initialize(string title, List<Tuple<QdasNumberWrapper, string>> selectedQdasNumbers, Dictionary<TextEvaluationType, List<Tuple<QdasNumber, string>>> freeQdasNumbers)
        {
            this.Title = title;
            this._selectedQdasNumbersWithTranslation = new ObservableCollection<Tuple<QdasNumberWrapper, string>>(selectedQdasNumbers);
            SelectedQdasNumbersWithTranslation = new ReadOnlyObservableCollection<Tuple<QdasNumberWrapper, string>>(_selectedQdasNumbersWithTranslation);
            this._freeQdasNumbers = freeQdasNumbers;
            PropertyChangedEventManager.AddHandler(this, SelectedIndexChangedHandler, nameof(SelectedIndex));
        }

        internal bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        internal void Cancel()
        {
            _dialogResult = false;
            ViewCore.Close();
        }

        #region properties
        public bool IsValid {
            get { return _isValid; }
            set {
                if (SetProperty(ref _isValid, value))
                    this._okCommand.RaiseCanExecuteChanged();
            }
        }

        public ICommand OkCommand
        {
            get { return _okCommand; }
        }

        public DelegateCommand AddCommand
        {
            get { return _addCommand; }
        }

        public DelegateCommand RemoveCommand
        {
            get { return _removeCommand; }
        }

        public DelegateCommand MoveUpCommand
        {
            get { return _moveUpCommand; }
        }
        public DelegateCommand MoveDownCommand
        {
            get { return _moveDownCommand; }
        }

        public ReadOnlyObservableCollection<Tuple<QdasNumberWrapper, string>> SelectedQdasNumbersWithTranslation
        {
            get; private set;
        }

        public string Title { get; private set; }

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                SetProperty(ref _selectedIndex, value);
            }
        }
        #endregion

        #region commadn handlers

        private void OkCommandHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }

        private void MoveQdasNumberDownCommandHandler()
        {
            this._selectedQdasNumbersWithTranslation.Move(SelectedIndex, SelectedIndex + 1);
        }

        private bool CanMoveQdasNumberDown()
        {
            if (SelectedIndex == -1 || SelectedQdasNumbersWithTranslation.Count < 2)
                return false;
            return SelectedIndex < SelectedQdasNumbersWithTranslation.Count - 1;
        }

        private bool CanMoveQdasNumberUp()
        {
            if (SelectedIndex == -1 || SelectedQdasNumbersWithTranslation.Count < 2)
                return false;
            return SelectedIndex > 0;
        }

        private void MoveQdasNumberUpCommandHandler()
        {
            this._selectedQdasNumbersWithTranslation.Move(SelectedIndex, SelectedIndex - 1);
        }

        private void RemoveQdasNumberCommandHandler()
        {
            var toRemove = _selectedQdasNumbersWithTranslation.ElementAt(SelectedIndex);
            _selectedQdasNumbersWithTranslation.RemoveAt(SelectedIndex);
            _freeQdasNumbers[toRemove.Item1.QdasNumber.EvaluationType].Add(new Tuple<QdasNumber, string>(toRemove.Item1.QdasNumber, toRemove.Item2));
            MoveDownCommand.RaiseCanExecuteChanged();
            MoveUpCommand.RaiseCanExecuteChanged();
            RemoveCommand.RaiseCanExecuteChanged();
        }

        private void AddQdasNumberCommandHandler()
        {
            var vm = _selectQdasNumberWrapperViewModelFactory.CreateExport().Value;
            Dictionary<TextEvaluationType, IReadOnlyCollection<Tuple<QdasNumber,string>>> toChooseFrom = new Dictionary<TextEvaluationType, IReadOnlyCollection<Tuple<QdasNumber, string>>>();
            foreach(var kvp in _freeQdasNumbers)
            {
                if (kvp.Value.Count > 0)
                    toChooseFrom[kvp.Key] = kvp.Value;
            }
            string title = (string)LanguageManager.Current.CurrentLanguageData.Translate("SelectQdasNumber");
            vm.Initialize(title, null,toChooseFrom);
            if (vm.ShowDialog(this.View))
            {
                var selected = _freeQdasNumbers[vm.SelectedItem.EvaluationType].First(item => item.Item1.EvaluationNumber == vm.SelectedItem.EvaluationNumber);
                _freeQdasNumbers[vm.SelectedItem.EvaluationType].Remove(selected);
                //custom name is currently "", maybe trim when this changes
                var newWrapped = new QdasNumberWrapper(vm.CustomName, selected.Item1);
                _selectedQdasNumbersWithTranslation.Add(new Tuple<QdasNumberWrapper, string>(newWrapped, selected.Item2));
            }
        }
        #endregion

        #region helpers
        private void SelectedIndexChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            RemoveCommand.RaiseCanExecuteChanged();
            MoveDownCommand.RaiseCanExecuteChanged();
            MoveUpCommand.RaiseCanExecuteChanged();
        }
        #endregion
    }
}
