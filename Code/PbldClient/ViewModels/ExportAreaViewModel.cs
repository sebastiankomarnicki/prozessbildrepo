﻿using Promess.Common.Services;
using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class ExportAreaViewModel:ViewModel<IExportAreaView>
    {
        private readonly IMessageService _messageService;
        private readonly IFileDialogServiceExtended _fileDialogService;
        private readonly IFolderCacheService _folderCacheService;
        private readonly PathsAndResourceService _pathsAndResourceService;
        private readonly ConfigurationService _configurationService;

        private Area _exportArea;

        private string _selectedGraphicPage;
        private string _exportFolder;
        private string _backgroundImage;
        private HashSet<string> _exportGraphicProjectFileNames;

        private DelegateCommand _removeGraphicPageCommand;

        [ImportingConstructor]
        public ExportAreaViewModel(IExportAreaView view,
            IMessageService messageService, IFileDialogServiceExtended fileDialogService, IFolderCacheService folderCacheService,
            PathsAndResourceService pathsAndResourceService, ConfigurationService configurationService) : base(view)
        {
            this._messageService = messageService;
            this._fileDialogService = fileDialogService;
            this._folderCacheService = folderCacheService;
            this._pathsAndResourceService = pathsAndResourceService;
            this._configurationService = configurationService;
            this.SelectExportFolderCommand = new DelegateCommand(SelectExportFolderCommandHandler);
            this.SelectBackgroundImageCommand = new DelegateCommand(SelectBackgroundImageCommandHandler);
            this.AddGraphicPagesCommand = new DelegateCommand(AddGraphicPagesCommandHandler);
            this._removeGraphicPageCommand = new DelegateCommand(RemoveGraphicPageCommandHandler, ()=>SelectedGraphicPage!=null);
            this.ExportCommand = new DelegateCommand(ExportCommandHandler);
            this.ToExportGraphicPages = new ObservableCollection<string>();
            this._exportGraphicProjectFileNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        }

        internal void Initialize(Area area)
        {
            this._exportArea = area;
            this._exportFolder = String.Empty;
            this._backgroundImage = area.SchemaFilePath;
            PropertyChangedEventManager.AddHandler(this, SelectedGraphicPageChangedHandler, nameof(SelectedGraphicPage));
        }

        #region properties
        public string SelectedGraphicPage {
            get { return _selectedGraphicPage; }
            set { SetProperty(ref _selectedGraphicPage, value); }
        }
        public string ExportFolder
        {
            get { return _exportFolder; }
            set { SetProperty(ref _exportFolder, value); }
        }

        public string BackgroundImage
        {
            get { return _backgroundImage; }
            set { SetProperty(ref _backgroundImage, value); }
        }

        public bool ExportOptions
        {
            get;set;
        }

        public bool ExportListView
        {
            get;set;
        }

        public bool ExportQsSettings
        {
            get;set;
        }

        public ObservableCollection<string> ToExportGraphicPages { get; private set; }
        public ICommand SelectExportFolderCommand { get; private set; }
        public ICommand SelectBackgroundImageCommand { get; private set; }
        public ICommand AddGraphicPagesCommand { get; private set; }
        public ICommand RemoveGraphicPageCommand
        {
            get { return _removeGraphicPageCommand; }
        }
        public ICommand ExportCommand { get; private set; }
        #endregion

        #region command handlers
        private void RemoveGraphicPageCommandHandler()
        {
            //mind removal order, data bound selected element will be reset when removed from bound list
            _exportGraphicProjectFileNames.Remove(Path.GetFileName(_selectedGraphicPage));
            ToExportGraphicPages.Remove(_selectedGraphicPage);         
        }

        private void AddGraphicPagesCommandHandler()
        {
            FileType ft = new FileType("gprj", "*.gprj");
            var files = _fileDialogService.ShowOpenFilesDialog(View, ft, this._pathsAndResourceService.GraphicProjectPath);
            if (!files.Any())
                return;
            bool someNotAdded = false;
            string projectName;
            foreach(var file in files)
            {
                projectName = Path.GetFileName(file);
                if (this._exportGraphicProjectFileNames.Add(projectName))
                {
                    //BulkObservableCollection ist part of the visual studio sdk
                    ToExportGraphicPages.Add(file);                   
                }
                else
                {
                    someNotAdded = true;
                }                
            }
            if (someNotAdded)
                this._messageService.ShowMessage(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("EAddFileDuplicates"));
        }

        private void SelectExportFolderCommandHandler()
        {
            string defaultFolder = GetDefaultExportFolder();
            var selectedFolder = _fileDialogService.ShowOpenFolderDialog(View, ExportFolder);
            if (!String.IsNullOrWhiteSpace(selectedFolder))
            {
                ExportFolder = selectedFolder;
                _folderCacheService.ExportFolder = selectedFolder;
            }                
        }

        private string GetDefaultExportFolder()
        {
            if (Directory.Exists(ExportFolder))
            {
                return ExportFolder;
            }
            return _folderCacheService.ExportFolder;
        }

        private void SelectBackgroundImageCommandHandler()
        {
            FileType ft = new FileType("jpg, png, bmp", new string[] { "*.jpg", "*.png", "*.bmp" });
            var filename = _fileDialogService.ShowOpenFileDialog(View, ft, BackgroundImage, true).FirstOrDefault();

            if (!String.IsNullOrWhiteSpace(filename))
            {
                BackgroundImage = filename;
            }
        }

        private void ExportCommandHandler()
        {
            string tmpName;
            string zipLocation = Path.Combine(_pathsAndResourceService.TempPath, "tempExport.zip");
            try
            {
                if (!Directory.Exists(ExportFolder))
                    Directory.CreateDirectory(ExportFolder);
                File.Delete(zipLocation);
                using (var archive = ZipFile.Open(zipLocation, ZipArchiveMode.Create))
                {
                    var exportSettings = new ExportSettings();
                    ZipArchiveEntry entry;

                    //add area
                    tmpName = "Area.xml";
                    entry = archive.CreateEntry(tmpName);
                    using (var stream = entry.Open())
                    {
                        SerializeHelper.SerializeToStream(stream, _exportArea);
                    }
                    exportSettings.AreaFile = tmpName;
                    //add panels, for now add all
                    tmpName = "Panels.zip";
                    archive.CreateEntryFromFile(_pathsAndResourceService.Panels, tmpName);
                    exportSettings.PanelsFile = tmpName;
                    exportSettings.GraphicPageFiles = new List<String>();
                    //add graphic pages
                    foreach (var file in ToExportGraphicPages)
                    {
                        tmpName = Path.GetFileName(file);
                        archive.CreateEntryFromFile(file, tmpName);
                        exportSettings.GraphicPageFiles.Add(tmpName);
                    }
                    if (!String.IsNullOrWhiteSpace(BackgroundImage))
                    {
                        tmpName = Path.GetFileName(BackgroundImage);
                        archive.CreateEntryFromFile(BackgroundImage, tmpName);
                        exportSettings.BackgroundImageFile = tmpName;
                    }
                        
                    //add options
                    if (ExportOptions)
                    {
                        tmpName = "GeneralSettings.xml";
                        entry = archive.CreateEntry(tmpName);
                        using (var stream = entry.Open())
                        {
                            SerializeHelper.SerializeToStream(stream, _configurationService.GeneralSettings);
                        }
                        exportSettings.GeneralSettingsFile = tmpName;

                        tmpName = "EvalAndColorSettings.xml";
                        entry = archive.CreateEntry(tmpName);
                        using (var stream = entry.Open())
                        {
                            SerializeHelper.SerializeToStream(stream, _configurationService.EvaluationAndColorSettings);
                        }
                        exportSettings.EvalAndColorSettingsFile = tmpName;
                    }
                    //add list view setting
                    if (ExportListView)
                    {
                        tmpName = "ListViewSettings.xml";
                        entry = archive.CreateEntry(tmpName);
                        using (var stream = entry.Open())
                        {
                            SerializeHelper.SerializeToStream(stream, _configurationService.GetListViewSettingsCopy());
                        }
                        exportSettings.ListViewSettingsFile = tmpName;
                    }
                    if (ExportQsSettings)
                    {
                        tmpName = "QsStatSettings.xml";
                        entry = archive.CreateEntry(tmpName);
                        using (var stream = entry.Open())
                        {
                            SerializeHelper.SerializeToStream(stream, _configurationService.QsStatSettings);
                        }
                        exportSettings.QsStatSettingsFile = tmpName;
                    }
                    entry = archive.CreateEntry("ExportSettings.xml");
                    using (var stream = entry.Open())
                    {
                        SerializeHelper.SerializeToStream(stream, exportSettings);
                    }
                }
                string destination = Path.Combine(ExportFolder, "PbldExport.zip");
                if (File.Exists(destination))
                    File.Delete(destination);
                File.Move(zipLocation, destination);
                this._messageService.ShowMessage(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("ExportSuccess"));
            }
            catch
            {
                this._messageService.ShowError(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("ExportFailed"));
            }
            finally
            {
                try
                {
                    if (File.Exists(zipLocation))
                        File.Delete(zipLocation);
                }
                catch { }
            }
        }
        #endregion

        #region event handlers
        private void SelectedGraphicPageChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            this._removeGraphicPageCommand.RaiseCanExecuteChanged();
        }
        #endregion

        public void ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
        }
    }
}
