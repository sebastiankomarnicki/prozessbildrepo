﻿using Promess.Pbld.Data.Settings.PresetEditorItems;
using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class PropertiesLineViewModel:ViewModel<IPropertiesLineView>
    {
        [ImportingConstructor]
        public PropertiesLineViewModel(IPropertiesLineView view) : base(view) { }

        public void Setup(IPresetLine data)
        {
            this.Data = data;
        }

        public IPresetLine Data { get; private set; }
    }
}
