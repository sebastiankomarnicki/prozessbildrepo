﻿using Promess.Pbld.Data;
using Promess.Pbld.Data.Schema;
using Promess.Pbld.Editor;
using System;

namespace Promess.Pbld.ViewModels.SchemaViewModelHelpers
{
    public class EditorSchemaMetaItem:RectangleEditorItemModel<SchemaItem>
    {
        public EditorSchemaMetaItem(IDimensionsWithGrid parent, SpcPath spcPath, SchemaItem item, Func<SchemaItem, SchemaItem> editItem):base(parent, item, editItem)
        {
            this.SpcPath = spcPath;
        }

        /// <summary>
        /// Spc path associated with this object
        /// </summary>
        public SpcPath SpcPath
        {
            get; private set;
        }
    }
}
