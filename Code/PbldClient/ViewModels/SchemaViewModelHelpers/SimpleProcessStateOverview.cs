﻿namespace Promess.Pbld.ViewModels.SchemaViewModelHelpers
{
    public class SimpleProcessStateOverview
    {
        public SimpleProcessStateOverview(int okCount, int totalEvaluated, int skipped)
        {
            this.OkCount = okCount;
            this.TotalEvaluated = totalEvaluated;
            this.Skipped = skipped;
        }

        public int OkCount { get; private set; }
        public int TotalEvaluated { get; private set; }
        public int Skipped { get; private set; }
    }
}
