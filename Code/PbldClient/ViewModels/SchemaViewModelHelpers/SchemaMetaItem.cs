﻿using Promess.Pbld.Data;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Schema;
using System.Waf.Foundation;

namespace Promess.Pbld.ViewModels.SchemaViewModelHelpers
{
    public class SchemaMetaItem:Model
    {
        private ProcessState _processState;
        private SimpleProcessStateOverview _simpleProcessStateOverview;

        public SchemaMetaItem(SpcPath spcPath, SchemaItem schemaItem, SimpleProcessStateOverview psOverview, ProcessState processState = ProcessState.TooFewValues)
        {
            this.SpcPath = spcPath;
            this.SchemaItem = schemaItem;
            this._processState = processState;
            this._simpleProcessStateOverview = psOverview;
        }

        public SpcPath SpcPath { get; internal set; }

        public SchemaItem SchemaItem { get; private set; }

        public ProcessState ProcessState
        {
            get { return _processState; }
            internal set { SetProperty(ref _processState, value); }
        }

        public SimpleProcessStateOverview SimpleProcessStateOverview
        {
            get { return _simpleProcessStateOverview; }
            internal set { SetProperty(ref _simpleProcessStateOverview, value); }
        }
    }
}
