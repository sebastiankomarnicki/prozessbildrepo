﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class PropertiesViewModel<TView> : ViewModel<IPropertiesView>
        where TView:IView
    {
        private bool _dialogResult;
        private DelegateCommand _okCommand;
        private ViewModel<TView> _vm;
        private bool _isValid;
        private EventHandler<PropertyChangedEventArgs> _validUpdateHandler;

        [ImportingConstructor]
        public PropertiesViewModel(IPropertiesView view) : base(view)
        {
            _dialogResult = false;
            _isValid = true;
            _okCommand = new DelegateCommand(OkHandler, ()=>_isValid);
        }

        internal void Setup(ViewModel<TView> vm)
        {
            this._vm = vm;
            RaisePropertyChanged(nameof(WrappedView));
        } 

        public object WrappedView { get { return _vm?.View; } }

        public ICommand OkCommand
        {
            get { return _okCommand; }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set { if (SetProperty(ref _isValid, value)) {
                    _okCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private void OkHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        internal void SetValidUpdateHandler(EventHandler<PropertyChangedEventArgs> validUpdateHandler)
        {
            this._validUpdateHandler = validUpdateHandler;
        }
    }
}
