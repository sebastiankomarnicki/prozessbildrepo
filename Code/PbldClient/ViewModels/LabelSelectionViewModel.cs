﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class LabelSelectionViewModel : ViewModel<ILabelSelectionView>
    {
        private bool _dialogResult;

        private QsStatService _qsStatService;
        private TextEvaluationType _shownType;
        private ExportFactory<SelectQdasNumberViewModel> _selectQdasNumberVMFactory;

        private IReadOnlyDictionary<int, string> _evaluationTypeEntries;

        private string _groupBoxHeader;
        private string _selectedSeparator;
        private ObservableCollection<Tuple<QdasNumber, string>> _selectedLabels;
        private int _selectedIndex;

        private DelegateCommand _addLabelCommand;
        private DelegateCommand _removeLabelCommand;
        private DelegateCommand _moveLabelUpCommand;
        private DelegateCommand _moveLabelDownCommand;
        private ICommand _okCommand;

        [ImportingConstructor]
        public LabelSelectionViewModel(ILabelSelectionView view, QsStatService qsStatService,
            ExportFactory<SelectQdasNumberViewModel> selectQdasNumberVMFactory) : base(view)
        {
            this._qsStatService = qsStatService;
            this._selectQdasNumberVMFactory = selectQdasNumberVMFactory;
            this._dialogResult = false;
            this.AvailableSeparators = GetAvailableSeparators();
            this._addLabelCommand = new DelegateCommand(AddLabelCommandHandler);
            this._removeLabelCommand = new DelegateCommand(RemoveLabelCommandHandler, ()=>SelectedIndex>-1 && SelectedLabels.Count>=2);
            this._moveLabelUpCommand = new DelegateCommand(MoveLabelUpCommandHandler, CanMoveLabelUp);
            this._moveLabelDownCommand = new DelegateCommand(MoveLabelDownCommandHandler, CanMoveLabelDown);
            this._okCommand = new DelegateCommand(OkCommandHandler);
        }

        public void Initialize(String groupBoxHeader, List<QdasNumber> selectedLabels, TextEvaluationType selectableEvaluationType, string selectedSeparator)
        {
            this.GroupBoxHeader = groupBoxHeader;
            this._shownType = selectableEvaluationType;
            switch (selectableEvaluationType)
            {
                case TextEvaluationType.Part:
                    this._evaluationTypeEntries = this._qsStatService.PartKFields;
                    break;
                case TextEvaluationType.Characteristic:
                    this._evaluationTypeEntries = this._qsStatService.CharacteristicKFields; 
                    break;
                default:
                    throw new NotImplementedException();
            }
            var selectedLabelAndDescription = new List<Tuple<QdasNumber, string>>();
            foreach (var entry in selectedLabels)
            {
                if (!entry.EvaluationType.Equals(selectableEvaluationType))
                    selectedLabelAndDescription.Add(new Tuple<QdasNumber, string>(entry, ""));
                else
                {
                    string description;
                    if (this._evaluationTypeEntries.TryGetValue(entry.EvaluationNumber, out description))
                    {
                        selectedLabelAndDescription.Add(new Tuple<QdasNumber, string>(entry, description));
                    }
                    else
                    {
                        selectedLabelAndDescription.Add(new Tuple<QdasNumber, string>(entry, ""));
                    }
                }
            }
            this.SelectedLabels = new ObservableCollection<Tuple<QdasNumber, string>>( selectedLabelAndDescription);
            this.SelectedSeparator = selectedSeparator;
        }

        public string GroupBoxHeader
        {
            get { return _groupBoxHeader; }
            private set
            {
                SetProperty(ref _groupBoxHeader, value);
            }
        }

        public string SelectedSeparator
        {
            get { return _selectedSeparator; }
            set { SetProperty(ref _selectedSeparator, value); }
        }

        public List<Tuple<string, string>> AvailableSeparators
        {
            get;
        }

        public ObservableCollection<Tuple<QdasNumber, string>> SelectedLabels
        {
            get { return _selectedLabels; }
            private set { SetProperty(ref _selectedLabels, value); }
        }

        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                if (SetProperty(ref _selectedIndex, value))
                {
                    RemoveLabelCommand.RaiseCanExecuteChanged();
                    MoveLabelDownCommand.RaiseCanExecuteChanged();
                    MoveLabelUpCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand OkCommand
        {
            get { return _okCommand; }
        }

        public DelegateCommand AddLabelCommand
        {
            get { return _addLabelCommand; }
        }

        public DelegateCommand RemoveLabelCommand
        {
            get { return _removeLabelCommand; }
        }

        public DelegateCommand MoveLabelUpCommand
        {
            get { return _moveLabelUpCommand; }
        }
        public DelegateCommand MoveLabelDownCommand
        {
            get { return _moveLabelDownCommand; }
        }

        internal bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        internal void Cancel()
        {
            this._dialogResult = false;
            ViewCore.Close();
        }

        #region command handlers
        private bool CanMoveLabelDown()
        {
            if (SelectedIndex ==-1 || SelectedLabels.Count<2)
                return false;
            return SelectedIndex < SelectedLabels.Count - 1;
        }

        private void MoveLabelDownCommandHandler()
        {
            SelectedLabels.Move(SelectedIndex, SelectedIndex + 1);
        }

        private bool CanMoveLabelUp()
        {
            if (SelectedIndex == -1 || SelectedLabels.Count < 2)
                return false;
            return SelectedIndex > 0;
        }

        private void MoveLabelUpCommandHandler()
        {
            SelectedLabels.Move(SelectedIndex, SelectedIndex - 1);
        }

        private void RemoveLabelCommandHandler()
        {
            SelectedLabels.RemoveAt(SelectedIndex);
            MoveLabelDownCommand.RaiseCanExecuteChanged();
            MoveLabelUpCommand.RaiseCanExecuteChanged();
            RemoveLabelCommand.RaiseCanExecuteChanged();
        }

        private void AddLabelCommandHandler()
        {
            var usedNumbers = _selectedLabels.Select(tuple => tuple.Item1.EvaluationNumber);
            var freeNumbers = _evaluationTypeEntries.Keys.Except(usedNumbers);
            var toDisplay = new List<Tuple<QdasNumber, string>>();
            foreach (var number in freeNumbers.OrderBy(x => x))
            {
                toDisplay.Add(new Tuple<QdasNumber, string>(new QdasNumber(_shownType, number), _evaluationTypeEntries[number]));
            }
            var vm = _selectQdasNumberVMFactory.CreateExport().Value;
            vm.Initialize(toDisplay);
            if (vm.ShowDialog(this.View))
            {
                SelectedLabels.Add(new Tuple<QdasNumber, string>(vm.SelectedQdasNumber, _evaluationTypeEntries[vm.SelectedQdasNumber.EvaluationNumber]));
                MoveLabelDownCommand.RaiseCanExecuteChanged();
                MoveLabelUpCommand.RaiseCanExecuteChanged();
                RemoveLabelCommand.RaiseCanExecuteChanged();
            }
        }

        private void OkCommandHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }
        #endregion

        #region helpers
        private List<Tuple<string, string>> GetAvailableSeparators()
        {
            var result = new List<Tuple<string, string>>();
            result.Add(new Tuple<string, string>("_", "_"));
            result.Add(new Tuple<string, string>("/", "/"));
            result.Add(new Tuple<string, string>(" / ", "{Space}/{Space}"));
            result.Add(new Tuple<string, string>(" \\ ", "{Space}\\{Space}"));
            result.Add(new Tuple<string, string>("_", "_"));
            return result;
        }
        #endregion
    }
}
