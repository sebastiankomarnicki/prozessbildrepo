﻿using Promess.Common.Services;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class TransferFollowerViewModel:ViewModel<ITransferFollowerView>
    {
        private readonly IFileDialogServiceExtended _fileDialogService;
        private readonly IFolderCacheService _folderCacheService;
        private readonly PathsAndResourceService _pathsAndResourceService;

        private TransferFollowerSettings _transferFollowerSettings;

        [ImportingConstructor]
        public TransferFollowerViewModel(ITransferFollowerView view, 
            IFileDialogServiceExtended fileDialogService, IFolderCacheService folderCacheService,
            PathsAndResourceService pathsAndResourceService) : base(view)
        {
            this._fileDialogService = fileDialogService;
            this._folderCacheService = folderCacheService;
            this._pathsAndResourceService = pathsAndResourceService;
        }

        internal void Initialize(TransferFollowerSettings settings)
        {
            this.Settings = settings;
            this.SelectTransferFolderCommand = new DelegateCommand(SelectTransferFolderCommandHandler);
            this.SelectTransferFolderGraphicProjectsCommand = new DelegateCommand(SelectTransferFolderGraphicProjectsCommandHandler);
        }

        private void SelectTransferFolderCommandHandler()
        {
            string defaultPath = GetDefaultTransferPath();
            var selectedFolder = _fileDialogService.ShowOpenFolderDialog(View, defaultPath);
            if (!String.IsNullOrWhiteSpace(selectedFolder))
            {
                Settings.TransferPath = selectedFolder;
                _folderCacheService.TransferPath = selectedFolder;
                RaisePropertyChanged(nameof(Settings));
            }             
        }

        private string GetDefaultTransferPath()
        {
            if (Directory.Exists(Settings.TransferPath))
                return Settings.TransferPath;
            return this._folderCacheService.TransferPath;
        }

        private void SelectTransferFolderGraphicProjectsCommandHandler()
        {
            string defaultPath = GetDefaultGraphicProjectTransferPath();
            var selectedFolder = _fileDialogService.ShowOpenFolderDialog(View, defaultPath);
            if (!String.IsNullOrWhiteSpace(selectedFolder))
            {
                Settings.GraphicProjectTransferPath = selectedFolder;
                _folderCacheService.GraphicProjectTransferPath = selectedFolder;
                RaisePropertyChanged(nameof(Settings));
            }               
        }

        private string GetDefaultGraphicProjectTransferPath()
        {
            if (Directory.Exists(Settings.GraphicProjectTransferPath))
                return Settings.GraphicProjectTransferPath;
            if (!String.IsNullOrWhiteSpace(_folderCacheService.GraphicProjectTransferPath))
                return _folderCacheService.GraphicProjectTransferPath;
            return _pathsAndResourceService.GraphicProjectPath;
        }

        public TransferFollowerSettings Settings
        {
            get { return _transferFollowerSettings; }
            private set { SetProperty(ref _transferFollowerSettings, value); }
        }

        public DelegateCommand SelectTransferFolderCommand { get; private set; }
        public DelegateCommand SelectTransferFolderGraphicProjectsCommand { get; private set; }
    }
}
