﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class TextInputViewModel : ViewModel<ITextInputView>
    {
        #region fields
        private string _status;
        private string _title;
        private string _header;
        private string _input;
        private DelegateCommand _tryAcceptInputCommand;
        #endregion

        [ImportingConstructor]
        public TextInputViewModel(ITextInputView view) : base(view)
        {
            DialogResult = false;
            Title = "";
            Header = "";
            Input = "";
            Status = "";
        }

        public void Initialize(string title, string header, Func<string, Tuple<bool, string>> acceptFunc, Func<string, bool> checkValidFunc = null)
        {
            Title = title;
            Header = header;
            Action tryAccept = () =>
            {
                var res = acceptFunc(Input);
                if (res.Item1)
                {
                    DialogResult = true;
                    ViewCore.Close();
                }
                else
                    Status = res.Item2;
            };
            if (checkValidFunc == null)
            {
                TryAcceptInputCommand = new DelegateCommand(tryAccept);
            }else
            {
                TryAcceptInputCommand=new DelegateCommand(tryAccept, () => checkValidFunc(Input));
                PropertyChangedEventManager.AddHandler(this, InputPropertyChanged, nameof(Input));
            }
        }

        private void InputPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _tryAcceptInputCommand.RaiseCanExecuteChanged();
        }

        public void ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
        }

        internal void Close()
        {
            ViewCore.Close();
        }

        #region Properties
        public String Title
        {
            get { return _title; }
            protected set
            {
                SetProperty(ref _title, value);
            }
        }
        public String Header
        {
            get { return _header; }
            protected set
            {
                SetProperty(ref _header, value);
            }
        }

        public String Input
        {
            get { return _input; }
            set
            {
                SetProperty(ref _input, value);
            }
        }

        public bool DialogResult
        {
            get; private set;
        }
        public DelegateCommand TryAcceptInputCommand {
            get { return _tryAcceptInputCommand; }
            private set {
                SetProperty(ref _tryAcceptInputCommand, value);
            }
        }
        public String Status
        {
            get { return _status; }
            private set
            {
                SetProperty(ref _status, value);
            }
        }
        #endregion
    }
}
