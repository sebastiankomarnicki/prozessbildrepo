﻿using Promess.Common.Services;
using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Data;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class EditGraphicProjectViewModel : ViewModel<IEditGraphicProjectView>
    {
        #region fields
        private readonly IMessageService _messageService;
        private readonly IFileDialogServiceExtended _fileDialogservice;
        private readonly PathsAndResourceService _pathsAndResourceService;

        private readonly ExportFactory<AddItemViewModel> _addItemViewModelFactory;
        private readonly ExportFactory<TextInputViewModel> _textInputViewModel;
        private readonly EditGraphicPageViewModel _editGraphicPageViewModel;

        private ICollectionView _sortedGraphicPageModelsView;
        private GraphicProject _currentGraphicProject;
        private string _currentGraphicProjectPath;

        private ICommand _openGraphicProjectCommand;
        private DelegateCommand _saveGraphicProjectCommand;
        private DelegateCommand _saveAsGraphicProjectCommand;

        private DelegateCommand _createNewGraphicPageCommand;
        private DelegateCommand _deleteGraphicPageCommand;
        private DelegateCommand<GraphicItemModel> _openGraphicPageCommand;
        #endregion

        [ImportingConstructor]
        public EditGraphicProjectViewModel(IEditGraphicProjectView view,
            IMessageService messageService, IFileDialogServiceExtended fileDialogService, PathsAndResourceService pathsAndResourceService,
            ExportFactory<AddItemViewModel> addItemViewModelFactory, ExportFactory<TextInputViewModel> textInputViewModel,
            EditGraphicPageViewModel editGraphicPageViewModel
            ) : base(view)
        {
            this._messageService = messageService;
            this._fileDialogservice = fileDialogService;
            this._pathsAndResourceService = pathsAndResourceService;

            this._addItemViewModelFactory = addItemViewModelFactory;
            this._textInputViewModel = textInputViewModel;
            this._editGraphicPageViewModel = editGraphicPageViewModel;

            this._currentGraphicProject = null;
            this._currentGraphicProjectPath = "";

            OpenGraphicProjectCommand = new DelegateCommand(OpenGraphicProjectCommandHandler);
            SaveGraphicProjectCommand = new DelegateCommand(SaveGraphicProjectCommandHandler, ()=>_currentGraphicProject!=null);
            SaveAsGraphicProjectCommand = new DelegateCommand(SaveAsGraphicProjectCommandHandler, () => _currentGraphicProject != null);

            CreateNewGraphicPageCommand = new DelegateCommand(TryCreateNewGraphicPageCommandHandler, () => _currentGraphicProject != null);
            DeleteGraphicPageCommand = new DelegateCommand(DeleteGraphicPageCommandHandler, () => EditVM.Data != null);
            OpenGraphicPageCommand = new DelegateCommand<GraphicItemModel>(OpenGraphicPageCommandHandler, (model) => model != null && _currentGraphicProject != null);
        }

        public void Initialize(object parentView, string projectPath, GraphicProject currentProject)
        {
            this.ParentView = parentView;
            this.EditVM.ParentView = parentView;
            this.CurrentProjectPath = projectPath;
            this._currentGraphicProject = currentProject;
            InitializeForCurrentProject();
        }

        private void InitializeForCurrentProject()
        {
            EditVM.Initialize(null);
            ICollectionView collectionView;
            if (this._currentGraphicProject != null)
            {
                collectionView = CollectionViewSource.GetDefaultView(this._currentGraphicProject.GraphicPages);
                collectionView.SortDescriptions.Add(new SortDescription(nameof(GraphicItemModel.Name), ListSortDirection.Ascending));
            } else
            {
                collectionView = null;
            }
            this._sortedGraphicPageModelsView = collectionView;
            CreateNewGraphicPageCommand.RaiseCanExecuteChanged();
            OpenGraphicPageCommand.RaiseCanExecuteChanged();
            DeleteGraphicPageCommand.RaiseCanExecuteChanged();
            SaveGraphicProjectCommand.RaiseCanExecuteChanged();
            SaveAsGraphicProjectCommand.RaiseCanExecuteChanged();
        }

        #region properties
        public object ParentView
        {
            get; private set;
        }

        public EditGraphicPageViewModel EditVM
        {
            get { return _editGraphicPageViewModel; }
        }

        public string CurrentProjectPath
        {
            get { return _currentGraphicProjectPath; }
            private set { SetProperty(ref _currentGraphicProjectPath, value); }
        }

        public ICollectionView SortedGraphicPageModelsView
        {
            get { return _sortedGraphicPageModelsView; }
        }
        #endregion

        #region commands
        public DelegateCommand CreateNewGraphicPageCommand
        {
            get { return _createNewGraphicPageCommand; }
            private set
            {
                SetProperty(ref _createNewGraphicPageCommand, value);
            }
        }

        public DelegateCommand<GraphicItemModel> OpenGraphicPageCommand
        {
            get { return _openGraphicPageCommand; }
            private set
            {
                SetProperty(ref _openGraphicPageCommand, value);
            }
        }

        public DelegateCommand DeleteGraphicPageCommand
        {
            get { return _deleteGraphicPageCommand; }
            private set
            {
                SetProperty(ref _deleteGraphicPageCommand, value);
            }
        }

        public ICommand OpenGraphicProjectCommand
        {
            get { return _openGraphicProjectCommand; }
            private set
            {
                SetProperty(ref _openGraphicProjectCommand, value);
            }
        }

        public DelegateCommand SaveGraphicProjectCommand
        {
            get { return _saveGraphicProjectCommand; }
            private set
            {
                SetProperty(ref _saveGraphicProjectCommand, value);
            }
        }

        public DelegateCommand SaveAsGraphicProjectCommand
        {
            get { return _saveAsGraphicProjectCommand; }
            private set
            {
                SetProperty(ref _saveAsGraphicProjectCommand, value);
            }
        }

        public DelegateCommand CloseForParentCommand { get; internal set; }
        #endregion

        #region command handlers
        private void OpenGraphicPageCommandHandler(GraphicItemModel toOpen)
        {
            if (!_currentGraphicProject.GraphicPages.Contains(toOpen))
                return;

            if (EditVM.Data != null)
                EditVM.SavePercentages();
            EditVM.Initialize(toOpen);
            DeleteGraphicPageCommand.RaiseCanExecuteChanged();
        }

        private void DeleteGraphicPageCommandHandler()
        {
            if (!this._messageService.ShowYesNoQuestion(ParentView,
                String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("QGraphicPageDelete"), EditVM.Data.Name)))
            {
                return;
            }
            this._currentGraphicProject.RemoveGraphicPage(EditVM.Data);
            EditVM.ResetData();
            DeleteGraphicPageCommand.RaiseCanExecuteChanged();
        }

        private void TryCreateNewGraphicPageCommandHandler()
        {
            if (EditVM.Data != null)
                EditVM.SavePercentages();

            var vm = _addItemViewModelFactory.CreateExport().Value;
            vm.Initialize(TryCreateNewGraphicPage);
            vm.ShowDialog(ParentView);
        }

        private void SaveAsGraphicProjectCommandHandler()
        {
            FileType ft = new FileType("gprj", "*.gprj");
            string directory = String.IsNullOrWhiteSpace(CurrentProjectPath) ? _pathsAndResourceService.GraphicProjectPath : CurrentProjectPath;
            var result = _fileDialogservice.ShowSaveFileDialog(ParentView, ft, directory);
            if (!result.IsValid)
                return;
            var filename = result.FileName.Trim();
            EditVM.SavePercentages();
            try
            {
                SerializeHelper.SerializeToFile(filename, this._currentGraphicProject);
                this._currentGraphicProject.ResetDirty();
                this.CurrentProjectPath = filename;
            }
            catch
            {
                string message = (string)LanguageManager.Current.CurrentLanguageData.Translate("ESaveProject");
                this._messageService.ShowError(ParentView, message);
            }
        }

        private void SaveGraphicProjectCommandHandler()
        {
            if (!this._currentGraphicProject.GetIsDirty())
                return;
            try
            {
                EditVM.SavePercentages();
                SerializeHelper.SerializeToFile(this.CurrentProjectPath, this._currentGraphicProject);
                this._currentGraphicProject.ResetDirty();
            }
            catch
            {
                string message = (string)LanguageManager.Current.CurrentLanguageData.Translate("ESaveProject");
                this._messageService.ShowError(ParentView, message);
            }
        }

        private void OpenGraphicProjectCommandHandler()
        {
            ProcessProjectIsDirtyAndReset();
            FileType ft = new FileType("gprj", "*.gprj");
            string directory = String.IsNullOrWhiteSpace(CurrentProjectPath) ? _pathsAndResourceService.GraphicProjectPath : CurrentProjectPath;
            var result = _fileDialogservice.ShowOpenFileDialog(ParentView, ft, directory, false);
            if (!result.Any())
                return;

            var filename = result.First().Trim();
            GraphicProject loaded;
            try
            {
                if (File.Exists(filename))
                {
                    loaded = SerializeHelper.DeserializeFromFile<GraphicProject>(filename);
                }else
                {
                    loaded = new GraphicProject();
                    SerializeHelper.SerializeToFile(filename, loaded);
                }
            }catch
            {
                string msg = (string)LanguageManager.Current.CurrentLanguageData.Translate("ELoadProject");
                this._messageService.ShowError(ParentView, msg);
                return;
            }
            this._currentGraphicProject = loaded;
            CurrentProjectPath = filename;
            InitializeForCurrentProject();
        }
        #endregion

        #region helpers
        private bool TryCreateNewGraphicPage(string newName)
        {
            if (!CheckNameValid(newName))
                return false;
            var newPage = new GraphicItemModel(newName);
            if (!this._currentGraphicProject.TryAddGraphicPage(newPage))
                return false;
            EditVM.Initialize(newPage);
            DeleteGraphicPageCommand.RaiseCanExecuteChanged();
            return true;
        }

        private bool CheckNameValid(string proposedName)
        {
            if (String.IsNullOrWhiteSpace(proposedName))
                return false;
            if (proposedName.Length > 30)
                return false;
            string trimmed = proposedName.Trim();
            if (_currentGraphicProject.DoesPageNameAlreadyExist(trimmed))
            {
                return false;
            }
            return true;
        }

        private void ProcessProjectIsDirtyAndReset()
        {
            if (_currentGraphicProject?.GetIsDirty() ?? false)
            {
                bool answer = _messageService.ShowYesNoQuestion(ParentView,
                    (string)LanguageManager.Current.CurrentLanguageData.Translate("QProjectUnsavedChanges"));
                if (answer)
                {
                    SaveGraphicProjectCommandHandler();
                }
            }
        }

        public void Close()
        {
            ProcessProjectIsDirtyAndReset();
        }
        #endregion
    }
}
