﻿using Promess.Common.Settings;
using Promess.Language;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.Pbld.Utility;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class OptionsViewModel : ViewModel<IOptionsView>
    {
        #region fields
        private bool _dialogResult;

        private readonly IMessageService _messageService;
        private readonly IMainService _mainService;
        private readonly ConfigurationService _configurationService;

        private readonly GeneralSettingsViewModel _generalSettingsVM;
        private readonly EvaluationAndColorSettingsViewModel _evalColorSettingsVM;
        private readonly TransferFollowerViewModel _transferFollowerVM;
        private readonly ModifiedConfigurationManagerPbld _modifiedConfigManager;

        private AcceptableSettings<GeneralSettings> _acceptableGeneralSetting;
        private AcceptableSettings<EvaluationAndColorSettings> _acceptableEvalAndColorSettings;
        private AcceptableSettings<TransferFollowerSettings> _acceptableTransferFollowerSettings;

        private DelegateCommand _okCommand;
        #endregion

        [ImportingConstructor]
        public OptionsViewModel(IOptionsView view,
            IMessageService messageService, IMainService mainService,
            ConfigurationService configurationService,
            ModifiedConfigurationManagerPbld modifiedConfigManager,
            GeneralSettingsViewModel generalSettingsVM, EvaluationAndColorSettingsViewModel evalColorSettingsVM,
            TransferFollowerViewModel transferFollowerVM) : base(view)
        {
            this._dialogResult = false;
            this._messageService = messageService;
            this._mainService = mainService;
            this._configurationService = configurationService;
            this._generalSettingsVM = generalSettingsVM;
            this._evalColorSettingsVM = evalColorSettingsVM;
            this._transferFollowerVM = transferFollowerVM;
            this._modifiedConfigManager = modifiedConfigManager;
        }

        protected override void OnInitialize()
        {
            _acceptableGeneralSetting = _modifiedConfigManager.GetSectionAndAddIfMissingAsAcceptableSettings<GeneralSettings>(Constants.General);
            _acceptableEvalAndColorSettings = _modifiedConfigManager.GetSectionAndAddIfMissingAsAcceptableSettings<EvaluationAndColorSettings>(Constants.EvaluationAndColor);
            _acceptableTransferFollowerSettings = _modifiedConfigManager.GetSectionAndAddIfMissingAsAcceptableSettings<TransferFollowerSettings>(Constants.TransferFollower);
            _generalSettingsVM.Initialize(_acceptableGeneralSetting.Settings);
            _evalColorSettingsVM.Initialize(_acceptableEvalAndColorSettings.Settings);
            
            _okCommand = new DelegateCommand(OkCommandHandler);
            GeneralSettingsView = _generalSettingsVM.View;
            EvaluationAndColorsView = _evalColorSettingsVM.View;
            if (_mainService.OperatingMode == Data.OperatingMode.follower)
            {                
                _transferFollowerVM.Initialize(_acceptableTransferFollowerSettings.Settings);
                TransferFollowerView = _transferFollowerVM.View;
                ShowTransferFollowerView = true;
            }
            else
            {
                //maybe also use dummy settings?
                TransferFollowerView = null;
                ShowTransferFollowerView = false;
            }
            PropertyChangedEventManager.AddHandler(this._configurationService, HandleGeneralSettingsChanged, nameof(ConfigurationService.GeneralSettings));
            PropertyChangedEventManager.AddHandler(this._configurationService, HandleEvalColorSettingsChangedHandler, nameof(ConfigurationService.EvaluationAndColorSettings));
        }

        public object GeneralSettingsView { get; private set; }
        public object EvaluationAndColorsView { get; private set; }
        public object TransferFollowerView { get; private set; }
        public bool ShowTransferFollowerView { get; private set; }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        private void OkCommandHandler()
        {
            if (!_generalSettingsVM.IsValid)
            {
                _messageService.ShowError(this.View,
                    (string) LanguageManager.Current.CurrentLanguageData.Translate("SettingHasErrors"));
                return;
            }
            PropertyChangedEventManager.RemoveHandler(this._configurationService, HandleGeneralSettingsChanged, nameof(ConfigurationService.GeneralSettings));
            PropertyChangedEventManager.RemoveHandler(this._configurationService, HandleEvalColorSettingsChangedHandler, nameof(ConfigurationService.EvaluationAndColorSettings));
            _acceptableGeneralSetting.AcceptChanges();
            _acceptableEvalAndColorSettings.AcceptChanges();
            if (_mainService.OperatingMode == Data.OperatingMode.follower)
                _acceptableTransferFollowerSettings.AcceptChanges();
            _dialogResult = true;
            ViewCore.Close();
        }

        public GeneralSettings GeneralSettings { get { return _acceptableGeneralSetting.Settings; } }
        public EvaluationAndColorSettings EvalAndColorSettings { get { return _acceptableEvalAndColorSettings.Settings; } }
        public TransferFollowerSettings TransferFollowerSettings { get { return _acceptableTransferFollowerSettings.Settings; } }

        public ICommand OkCommand
        {
            get { return _okCommand; }
        }

        #region event handlers
        private void HandleEvalColorSettingsChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            _acceptableEvalAndColorSettings = _modifiedConfigManager.GetSectionAndAddIfMissingAsAcceptableSettings<EvaluationAndColorSettings>(Constants.EvaluationAndColor);
            _evalColorSettingsVM.UpdateSettings(_acceptableEvalAndColorSettings.Settings);
        }

        private void HandleGeneralSettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            _acceptableGeneralSetting = _modifiedConfigManager.GetSectionAndAddIfMissingAsAcceptableSettings<GeneralSettings>(Constants.General);
            _generalSettingsVM.Settings = _acceptableGeneralSetting.Settings;
        }
        #endregion
    }
}
