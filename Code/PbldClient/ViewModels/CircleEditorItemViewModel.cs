﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class CircleEditorItemViewModel:ViewModel<ICirclePropertiesView>
    {
        #region fields
        private CircleEditorItem _data;
        #endregion

        [ImportingConstructor]
        public CircleEditorItemViewModel(ICirclePropertiesView view) : base(view)
        {
        }

        public object ParentView { get; set; }

        public CircleEditorItem Data
        {
            get { return _data; }
            set
            {
                SetProperty(ref _data, value);
            }
        }
    }
}
