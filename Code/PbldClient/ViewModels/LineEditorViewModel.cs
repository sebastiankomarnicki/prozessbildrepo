﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class LineEditorViewModel:ViewModel<ILinePropertiesView>
    {
        #region fields
        private readonly ExportFactory<PropertiesLineViewModel> _propertiesLineVMFactory;

        private object _linePropertiesView;
        private LineEditorItem _data;
        #endregion

        [ImportingConstructor]
        public LineEditorViewModel(ILinePropertiesView view,
            ExportFactory<PropertiesLineViewModel> propertiesLineVMFactory) : base(view)
        {
            this._propertiesLineVMFactory = propertiesLineVMFactory;
        }

        internal void Setup(object parentView, LineEditorItem item)
        {
            this.ParentView = parentView;
            this.Data = item;
            var lineVm = this._propertiesLineVMFactory.CreateExport().Value;
            lineVm.Setup(item);
            this.LinePropertiesView = lineVm.View;
            RaisePropertyChanged(nameof(LinePropertiesView));
        }

        #region properties
        public object ParentView { get; private set; }
        public object LinePropertiesView {
            get { return _linePropertiesView; }
            private set { SetProperty(ref _linePropertiesView, value); }
        }
        public LineEditorItem Data
        {
            get { return _data; }
            private set
            {
                SetProperty(ref _data, value);
            }
        }
        #endregion
    }
}
