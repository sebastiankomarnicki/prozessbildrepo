﻿using Promess.Pbld.Data;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class SetAreaViewModel : ViewModel<ISetAreaView>
    {
        private readonly ConfigurationService _configurationService;

        private bool _dialogResult;

        private Areas _areas;
        private Area _selectedArea;
        private ICollectionView _sortedAreasView;

        private ICommand _okCommand;

        [ImportingConstructor]
        public SetAreaViewModel(ISetAreaView view, ConfigurationService configurationService) : base(view)
        {
            this._configurationService = configurationService;
            this._dialogResult = false;
        }

        public void Setup()
        {
            this._areas = this._configurationService.GetAreasCopy();
            SetSortedAreasView();
            this._selectedArea = this._areas.SelectedArea;
            this._okCommand = new DelegateCommand(OkCommandHelper);
            PropertyChangedEventManager.AddHandler(this, SelectedAreaChangedHandler, nameof(SelectedArea));
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.AreasChanged), AreasChangedHandler);
        }

        #region properties
        /// <summary>
        /// View of <see cref="Area"/> objects.
        /// </summary>
        public ICollectionView SortedAreasView {
            get { return _sortedAreasView; }
            private set { SetProperty(ref _sortedAreasView, value); }
        }

        public Area SelectedArea
        {
            get { return _selectedArea; }
            set { SetProperty(ref _selectedArea, value); }
        }

        public ICommand OkCommand { get { return _okCommand; } }
        #endregion

        internal Areas GetAreas()
        {
            return _areas;
        }

        private void OkCommandHelper()
        {
            _dialogResult = true;
            ViewCore.Close();
        }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            PropertyChangedEventManager.RemoveHandler(this, SelectedAreaChangedHandler, nameof(SelectedArea));
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.AreasChanged), AreasChangedHandler);
            return _dialogResult;
        }

        private void SelectAreaCommandHelper(Area toSelect)
        {
            this._areas.SelectArea(toSelect);
        }

        private void SelectedAreaChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            this._areas.SelectArea(SelectedArea);
        }

        private void AreasChangedHandler(object sender, EventArgs e)
        {
            this._areas = this._configurationService.GetAreasCopy();
            this.SelectedArea = this._areas.SelectedArea;
        }

        private void SetSortedAreasView()
        {
            var collectionView = CollectionViewSource.GetDefaultView(this._areas.AvailableAreas);
            collectionView.SortDescriptions.Add(new SortDescription(nameof(Area.Name), ListSortDirection.Ascending));
            this.SortedAreasView = collectionView;
        }
    }
}
