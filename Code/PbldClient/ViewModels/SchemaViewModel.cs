﻿using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.Pbld.ViewModels.SchemaViewModelHelpers;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Schema;
using Promess.QsStat.Data;
using Promess.QsStat.Services;
using Promess.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class SchemaViewModel : ViewModel<ISchemaView>
    {
        private const double DEFAULTWIDTHPERCENTAGE = 0.04;
        private const double DEFAULTHEIGHTPERCENTAGE = 0.02;
        #region fields
        private readonly QsStatService _qsStatService;
        private readonly IMainService _mainService;

        private Area _currentArea;
        private IReadOnlyCollection<SchemaMetaItem> _schemaMetaItems;
        private Dictionary<Guid, SchemaMetaItem> _metaItemsLookup;
        #endregion

        [ImportingConstructor]
        public SchemaViewModel(ISchemaView view, IMainService mainService,
           ConfigurationService configurationService, IconLookupService iconLookupService, QsStatService qsStatService
            ) : base(view)
        {
            this._mainService = mainService;
            this._qsStatService = qsStatService;
            this.ConfigurationService = configurationService;
            this.IconLookupService = iconLookupService;            
        }

        internal void Initialize(ICommand showEditView, Area currentArea, DelegateCommand<SpcPath> showListViewCommand)
        {
            Debug.Assert(currentArea != null);
            this.CurrentArea = currentArea;
            this.ShowEditSchemaViewCommand = showEditView;
            this.RefreshCommand = new DelegateCommand<SchemaMetaItem>(item=>RefreshCommandHandler(item));
            this.ShowListViewCommand = showListViewCommand;
            PropertyChangedEventManager.AddHandler(this._mainService, HandleWindowStateChanged, nameof(IMainService.WindowState));
            PropertyChangedEventManager.AddHandler(this._qsStatService, HandleSpcPathResultsChanged, nameof(QsStatService.SpcPathResults));
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshSchemaItemsHandler);
            InitializeSchemaItems();
        }

        public ConfigurationService ConfigurationService
        {
            get;private set;
        }

        public IconLookupService IconLookupService
        {
            get; private set;
        }

        public Area CurrentArea
        {
            get { return _currentArea; }
            internal set { SetProperty(ref _currentArea, value); }
        }

        public IReadOnlyCollection<SchemaMetaItem> DisplayItems
        {
            get { return _schemaMetaItems; }
            internal set { SetProperty(ref _schemaMetaItems, value); }
        }

        public ICommand ShowEditSchemaViewCommand
        {
            get; private set;
        }

        public DelegateCommand<SchemaMetaItem> RefreshCommand
        {
            get;private set;
        }

        public DelegateCommand<SpcPath> ShowListViewCommand
        {
            get; private set;
        }

        public WindowState WindowState
        {
            get { return _mainService.WindowState; }
        }

        private void InitializeSchemaItems()
        {
            Random rnd = new Random();
            var metaItems = new List<SchemaMetaItem>();
            var metaItemsLookup = new Dictionary<Guid, SchemaMetaItem>();
            var spcResults = this._qsStatService.SpcPathResults.ToList();

            foreach (var spcPath in CurrentArea.ReadonlyPaths)
            {
                SchemaItem schemaItem = null;
                SchemaMetaItem metaItem;
                if (!_currentArea.SpcGuidSchemaItem.TryGetValue(spcPath.Guid, out schemaItem))
                {
                    schemaItem = new SchemaItem() { PercentageX = rnd.Next(100) * 0.002, PercentageY = rnd.Next(100) * 0.002, PercentageWidth = DEFAULTWIDTHPERCENTAGE, PercentageHeight = DEFAULTHEIGHTPERCENTAGE };
                    _currentArea.SpcGuidSchemaItem[spcPath.Guid] = schemaItem;
                }
                ProcessState ps = ProcessState.TooFewValues;
                SimpleProcessStateOverview pso = null;
                SpcPath referenceSpc = null;
                for (int i = spcResults.Count - 1; i >= 0; i--)
                {
                    var kvp = spcResults[i];
                    if (kvp.Key.Guid.Equals(spcPath.Guid))
                    {
                        referenceSpc = kvp.Key;
                        ps = kvp.Value.ProcessState;
                        pso = GetProcessStateOverview(kvp.Value);
                        spcResults.RemoveAt(i);
                        break;
                    }
                }
                pso = pso ?? new SimpleProcessStateOverview(0, 0, 0);
                referenceSpc = referenceSpc ?? spcPath;
                metaItem = new SchemaMetaItem(referenceSpc, schemaItem, pso, ps);
                metaItems.Add(metaItem);
                metaItemsLookup[spcPath.Guid] = metaItem;
            }
            DisplayItems = metaItems;
            this._metaItemsLookup = metaItemsLookup;
        }

        #region event handlers
        private void HandleSpcPathResultsChanged(object sender, PropertyChangedEventArgs args)
        {
            //somehow check if old event exists and needs to be deregistered?
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshSchemaItemsHandler);         
        }

        private void HandleWindowStateChanged(object sender, PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(WindowState));
        }

        private void RefreshSchemaItemsHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add)
                return;
            var replacedData = e.NewItems[0] as KeyValuePair<SpcPath, SpcPathEvaluationDTO>?;
            if (!replacedData.HasValue)
                throw new ArgumentOutOfRangeException($"{nameof(NotifyCollectionChangedEventArgs)} is missing the replaced element");
            DispatchHelper.BeginInvoke(()=>RefreshSchemaItemsHandlerImpl(replacedData.Value));
        }

        private void RefreshSchemaItemsHandlerImpl(KeyValuePair<SpcPath, SpcPathEvaluationDTO> replacedData)
        {
            if (!this._metaItemsLookup.TryGetValue(replacedData.Key.Guid, out SchemaMetaItem metaItem))
                return;
            metaItem.SpcPath = replacedData.Key;
            metaItem.SimpleProcessStateOverview = GetProcessStateOverview(replacedData.Value);
            metaItem.ProcessState = replacedData.Value.ProcessState;
        }
        #endregion

        #region command handlers
        private void RefreshCommandHandler(SchemaMetaItem item)
        {
            this._qsStatService.RequestRefresh(item.SpcPath);
        }
        #endregion

        #region helpers
        private SimpleProcessStateOverview GetProcessStateOverview(SpcPathEvaluationDTO evaluation)
        {
            int okCount = 0;
            int elseCount = 0;
            int disabledCount = 0;
            foreach (var partData in evaluation.EvaluatedData.Values)
            {
                foreach (var charaKvp in partData.CharacteristicResults)
                {
                    var charaMetaData = charaKvp.Value.CharacteristicMetaData;
                    if (!charaMetaData.Enabled)
                        disabledCount += 1;
                    else
                    {
                        if (charaMetaData.State == ProcessState.Ok)
                            okCount += 1;
                        else
                            elseCount += 1;
                    }
                }
            }
            return new SimpleProcessStateOverview(okCount, okCount + elseCount, disabledCount);
        }
        #endregion
    }
}
