﻿using Promess.Pbld.Data.Settings;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class PresetMetaViewModel:ViewModel<IPresetMetaView>
    {
        #region fields
        private readonly ConfigurationService _configurationService;
        private readonly ExportFactory<PropertiesFontAndColorViewModel> _propertiesFontColorVMFactory;
        private readonly ExportFactory<PropertiesRepresentationGraphicViewModel> _propertiesBorderVMFactory;
        private readonly ExportFactory<PropertiesFontAndColorExtendedViewModel> _propertiesFontColorExtVMFactory;
        private readonly ExportFactory<PropertiesRepresentationTextViewModel> _propertiesBorderExtVMFactory;
        private readonly ExportFactory<PropertiesLineViewModel> _propertiesLineVMFactory;

        private PresetGraphicSettings _presets;

        private bool _dialogResult;
        #endregion

        [ImportingConstructor]
        public PresetMetaViewModel(IPresetMetaView view, ConfigurationService configurationService,
            ExportFactory<PropertiesFontAndColorViewModel> propertiesFontColorVMFactory,
            ExportFactory<PropertiesRepresentationGraphicViewModel> borderVMFactory,
            ExportFactory<PropertiesFontAndColorExtendedViewModel> propertiesFontColorExtVMFactory,
            ExportFactory<PropertiesRepresentationTextViewModel> propertiesBorderExtVMFactory,
            ExportFactory<PropertiesLineViewModel> propertiesLineVMFactory
            ) : base(view)
        {
            this._configurationService = configurationService;
            this._propertiesFontColorVMFactory = propertiesFontColorVMFactory;
            this._propertiesBorderVMFactory = borderVMFactory;
            this._propertiesFontColorExtVMFactory = propertiesFontColorExtVMFactory;
            this._propertiesBorderExtVMFactory = propertiesBorderExtVMFactory;
            this._propertiesLineVMFactory = propertiesLineVMFactory;
            this._dialogResult = false;
            this.OkCommand = new DelegateCommand(OkCommandHandler);
        }

        public void Setup()
        {
            this._presets = this._configurationService.GetPresetGraphicSettingsCopy();
            var buttonVM = this._propertiesFontColorVMFactory.CreateExport().Value;
            buttonVM.Setup(this._presets.PresetButton);
            ButtonPropertiesView = buttonVM.View;

            var textBorderVM = this._propertiesBorderExtVMFactory.CreateExport().Value;
            textBorderVM.Setup(this._presets.PresetText);
            var textFontColorExtVM = this._propertiesFontColorExtVMFactory.CreateExport().Value;
            textFontColorExtVM.Setup(this._presets.PresetText);           
            TextPropertiesViews = new object[] { textBorderVM.View, textFontColorExtVM.View };

            var imageVM = this._propertiesBorderVMFactory.CreateExport().Value;
            imageVM.Setup(this._presets.PresetGraphic);
            GraphicPropertiesView = imageVM.View;

            var lineVM = this._propertiesLineVMFactory.CreateExport().Value;
            lineVM.Setup(this._presets.PresetLine);
            LinePropertiesView = lineVM.View;
        }

        #region properties
        public ICollection<object> TextPropertiesViews { get; private set; }
        public object GraphicPropertiesView { get; private set; }
        public object ButtonPropertiesView { get; private set; }
        public object LinePropertiesView { get; private set; }
        public ICommand OkCommand
        {
            get;private set;
        }
        public PresetGraphicSettings PresetsGraphic { get { return this._presets; } }
        #endregion

        private void OkCommandHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }
    }
}
