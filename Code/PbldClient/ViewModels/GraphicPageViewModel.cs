﻿using Promess.Common.QDas;
using Promess.Common.Util;
using Promess.Common.Util.Async;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.IViews;
using Promess.QsStat.Data;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class GraphicPageViewModel : ViewModel<IGraphicPageView>
    {
        #region fields
        private readonly QsStatService _qsStatService;

        private IQsDedicatedCalculation _calculation;
        private IReadOnlyDictionary<SpcPath, FolderDatasets> _spcPathsAndDatasets;
        private IReadOnlyCollection<EditorItemBase> _readonlyElements;

        private double _viewWidth;
        private double _viewHeight;
        private SpcPath _currentSpc;
        private IReadOnlyList<SpcPath> _orderedSpcs;
        private IReadOnlyList<DataSet> _datasetsForCurrentSpc;
        private Guid _currentDatasetGuid;
        private int _currentCharacteristicNumber;
        private IReadOnlyList<int> _enabledCharacteristics;
        private Size _displaySize;
        private CancellationTokenSource _spcDataCts;
        private CancellationTokenSource _characteristicCts;

        private ICommand _sizeChangedCommand;

        private bool _hasNextDataset;
        private bool _hasPreviousDataset;
        private bool _hasNextCharacteristic;
        private bool _hasPreviousCharacteristic;

        private QsStatRetrievalItemsMultipleCharacteristics _retrievalItems;

        private QsDedicatedLoaded _loadedDataset;
        private int? _loadedCharacteristic;
        private EventHandler<PropertyChangedEventArgs> _currentDatasetsChangedHandler;
        #endregion

        [ImportingConstructor]
        public GraphicPageViewModel(IGraphicPageView view, QsStatService qsStatService) : base(view)
        {
            this._qsStatService = qsStatService;

            this._hasNextDataset = false;
            this._hasPreviousDataset = false;
            this._hasNextCharacteristic = false;
            this._hasPreviousDataset = false;
            this._currentSpc = null;
            this._currentDatasetGuid = Guid.Empty;
            this._currentCharacteristicNumber = -1;
            this._enabledCharacteristics = new List<int>();
            this._displaySize = new Size(0, 0);
            this._spcDataCts = new CancellationTokenSource();
            this._characteristicCts = new CancellationTokenSource();
            this._loadedDataset = QsDedicatedCalculation.NODATA;
            this._loadedCharacteristic = null;
        }

        public void Initialize(object parentView, ObservableCollection<EditorItemBase> items, AspectRatio aspectRatio)
        {
            this.ParentView = parentView;
            this.ContainedElements = new ReadOnlyCollection<EditorItemBase>(items);
            this.AspectRatio = aspectRatio;
            this._retrievalItems = GetRetrievalItems(ContainedElements);
            LoadSpcCommand = new AsyncCommand<SpcPath>(ChangeSpcCommandAsync);
            LoadedCommand = new AsyncCommand(LoadedCommandHandlerAsync);
            RefreshDatasetCommand = new AsyncCommand(RefreshDataSetCommandHandlerAsync, ()=>DataSets.Any());
            SizeChangedCommand = new DelegateCommand<SizeChangedEventArgs>((s) =>{ });
            FirstDatasetCommand = new DelegateCommand(FirstDatasetCommandHandler, () => DataSets.Any() && !CurrentDataSetGuid.Equals(DataSets[0].Guid));
            PreviousDatasetCommand = new DelegateCommand(PreviousDatasetCommandHandler, () => HasPreviousDataset);
            NextDatasetCommand = new DelegateCommand(NextDatasetCommandHandler, () => HasNextDataset);
            LastDatasetCommand = new DelegateCommand(LastDatasetCommandHandler, ()=> DataSets.Any() && !CurrentDataSetGuid.Equals(DataSets[DataSets.Count-1].Guid));
            FirstCharacteristicCommand = new DelegateCommand(FirstCharacteristicCommandHandler, () => AvailableCharacteristicNumbers.Any() && CurrentCharacteristicNumber != AvailableCharacteristicNumbers.First());
            PreviousCharacteristicCommand = new DelegateCommand(PreviousCharacteristicCommandHandler, () => HasPreviousCharacteristic);
            NextCharacteristicCommand = new DelegateCommand(NextCharacteristicCommandHandler, () => HasNextCharacteristic);
            LastCharacteristicCommand = new DelegateCommand(LastCharacteristicCommandHandler, () => AvailableCharacteristicNumbers.Any() && CurrentCharacteristicNumber != AvailableCharacteristicNumbers.Last());
            var dedicatedCalculation = this._qsStatService.DedicatedCalculation;
            PropertyChangedEventManager.AddHandler(this._qsStatService, DedicatedCalculationChangedHandler, nameof(QsStatService.DedicatedCalculation));
            PropertyChangedEventManager.AddHandler(this, SelectedCharacteristicChangedHandler, nameof(this.CurrentCharacteristicNumber));
            InitializeDedicatedCalculation(dedicatedCalculation);
            InitializeElements();   
        }

        private void InitializeDedicatedCalculation(IQsDedicatedCalculation dedicatedCalculation)
        {
            this._calculation = dedicatedCalculation;           
            PropertyChangedEventManager.AddHandler(this._calculation, CalculationDataLoadedEventHandler, nameof(IQsDedicatedCalculation.LoadedDataset));
            PropertyChangedEventManager.AddHandler(this._calculation, SpcPathFoldersChangedEventHandler, nameof(IQsDedicatedCalculation.SpcPathFolderDatasets));
            this._spcPathsAndDatasets = this._calculation.SpcPathFolderDatasets;
            (this._loadedDataset, this._loadedCharacteristic) = this._calculation.GetCurrentDataSelection();
        }


        #region event handlers
        private EventHandler<PropertyChangedEventArgs> GenerateSpcPathDatasetsChangedEventHandler(SpcPath targetSpcPath)
        {
            Action updateDatasets = (() =>
            {
                if (targetSpcPath == this._loadedDataset.SpcPath && this._spcPathsAndDatasets.TryGetValue(targetSpcPath, out var folderDs))
                {
                    if (folderDs.DataSets.Any(ds=>Guid.Equals(ds.Guid, this._loadedDataset.DatasetGuid)))
                    {
                        SetDatasets(folderDs.DataSets);
                    }
                    else
                    {
                        InitializeElements();
                    }                
                }
            });
            return new EventHandler<PropertyChangedEventArgs>((s, e) =>
            {
                if (Application.Current.CheckAccess())
                {
                    updateDatasets();
                }
                else
                {
                    Application.Current.Dispatcher.BeginInvoke(updateDatasets);
                }
            });

            
        }

        private void DedicatedCalculationChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            this._spcDataCts.Cancel();
            InitializeDedicatedCalculation(this._qsStatService.DedicatedCalculation);
            DispatchHelper.BeginInvoke(this.InitializeElements);
        }

        private void CalculationDataLoadedEventHandler(object sender, PropertyChangedEventArgs args)
        {
            if (Application.Current.CheckAccess())
            {
                CheckAndProcessDataLoaded();
            }
            else
            {

                Application.Current.Dispatcher.BeginInvoke(new Action(this.CheckAndProcessDataLoaded));
            }
        }

        private void SpcPathFoldersChangedEventHandler(object sender, PropertyChangedEventArgs args)
        {
            this._spcDataCts.Cancel();
            this._spcPathsAndDatasets = this._calculation.SpcPathFolderDatasets;
            if (Application.Current.CheckAccess())
            {             
                InitializeElements();
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(new Action(this.InitializeElements));
            }
        }

        #endregion

        private void CheckAndProcessDataLoaded()
        {           
            this._loadedDataset = this._calculation.LoadedDataset;
            var spcPath = this._loadedDataset.SpcPath;
            if (!Object.Equals(CurrentSpc, spcPath))
            {
                InitializeElements();
                return;
            }
            if (!this._loadedDataset.DatasetGuid.Equals(CurrentDataSetGuid)&&!Guid.Equals(CurrentDataSetGuid, Guid.Empty))
            {
                //this is an old result, if the datasets list was updated this is handled by another handler
                return;
            }
            this._characteristicCts.Cancel();
            this._characteristicCts = new CancellationTokenSource();
            var lastCharacteristic = this._currentCharacteristicNumber;
            this._currentCharacteristicNumber = -1;
            SetAvailableCharacteristicNumbers(this._loadedDataset.EnabledCharacteristics);
            if (AvailableCharacteristicNumbers.Any())
            {
                //kickoff loading characteristic by eventhandler
                CurrentCharacteristicNumber = AvailableCharacteristicNumbers.Contains(lastCharacteristic) ? lastCharacteristic: AvailableCharacteristicNumbers.First();                              
            }
        }

        private void SelectedCharacteristicChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            if (Application.Current.CheckAccess())
            {
                //just kickoff loading
                _ = LoadDataForCurrentCharacteristicAsync();
            }
            else
            {
                Application.Current.Dispatcher.InvokeAsync(this.LoadDataForCurrentCharacteristicAsync);
            }
        }

        private async Task LoadDataForCurrentCharacteristicAsync()
        {
            this._characteristicCts.Cancel();
            this._characteristicCts = new CancellationTokenSource();
            QsStatPartResultDTO result;
            using (var linkedTokenSource = CancellationTokenSource.CreateLinkedTokenSource(this._characteristicCts.Token, this._spcDataCts.Token))
            {
                result = await this._calculation.RequestAllData(this._loadedDataset, this._retrievalItems, CurrentCharacteristicNumber, this._displaySize, linkedTokenSource.Token);
                linkedTokenSource.Token.ThrowIfCancellationRequested();
            }
            UpdateElements(ContainedElements, result);
        }

        private void InitializeElements()
        {
            this._spcDataCts.Cancel();
            this._characteristicCts.Cancel();
            this._spcDataCts = new CancellationTokenSource();
            CurrentSpc = this._loadedDataset.SpcPath;       
            OrderedSpcs = this._spcPathsAndDatasets.Keys.OrderBy(item => item.SpcNumber).ToList().AsReadOnly();
            IReadOnlyList<DataSet> availableDatasets;
            if (this._spcPathsAndDatasets.TryGetValue(CurrentSpc, out FolderDatasets folderDs)){
                this._currentDatasetsChangedHandler = GenerateSpcPathDatasetsChangedEventHandler(CurrentSpc);
                PropertyChangedEventManager.AddHandler(folderDs, this._currentDatasetsChangedHandler, nameof(FolderDatasets.DataSets));
                availableDatasets = folderDs.DataSets;
            } else
            {
                this._currentDatasetsChangedHandler = null;
                availableDatasets = new List<DataSet>();
                this._loadedCharacteristic = null;
            }
            this._currentDatasetGuid = this._loadedDataset.DatasetGuid;
            SetDatasets(availableDatasets);
            this._currentCharacteristicNumber = -1;
            SetAvailableCharacteristicNumbers(this._loadedDataset.EnabledCharacteristics);
            if (Guid.Equals(CurrentSpc.Guid, Guid.Empty))
            {
                ResetElements(true);
            } else
            {
                InitializeElements(CurrentSpc);
            }
            if (AvailableCharacteristicNumbers.Any())
            {
                if (this._loadedCharacteristic.HasValue && AvailableCharacteristicNumbers.Any(number => number == this._loadedCharacteristic.Value))
                    CurrentCharacteristicNumber = this._loadedCharacteristic.Value;
                else
                    CurrentCharacteristicNumber = AvailableCharacteristicNumbers.First();
            }
        }

        private QsStatRetrievalItemsMultipleCharacteristics GetRetrievalItems(IReadOnlyCollection<EditorItemBase> items)
        {
            var partTextKeys = new HashSet<int>();
            var characteristicTextKeys = new Dictionary<int, HashSet<int>>();
            var characteristicTextKeysRO = new Dictionary<int, IReadOnlyCollection<int>>();
            var numericAndTextKeys = new Dictionary<int, HashSet<int>>();
            var numericAndTextKeysRO = new Dictionary<int, IReadOnlyCollection<int>>();
            var imageKeys = new Dictionary<int, HashSet<QsStatImageRequest>>();
            var imageKeysRO = new Dictionary<int, IReadOnlyCollection<QsStatImageRequest>>();
            var additionalPart = new HashSet<int>();
            var additionalCharacteristic = new Dictionary<int, HashSet<int>>();
            var additionalCharacteristicRO = new Dictionary<int, IReadOnlyCollection<int>>();
            HashSet<int> keyRetrievalSet = null;
            HashSet<QsStatImageRequest> imageRetrievalSet = null;
            foreach (var item in items)
            {
                switch (item.GetType().Name)
                {
                    case nameof(TextGPEditorItem):
                        TextGPEditorItem textItem = (TextGPEditorItem)item;
                        if (textItem.TextType == TextType.Qdas && textItem.QdasNumber != null)
                        {
                            switch (textItem.QdasNumber.EvaluationType)
                            {
                                case TextEvaluationType.NumericAndTextResults:
                                    if (!numericAndTextKeys.TryGetValue(textItem.CharacteristicNumber, out keyRetrievalSet))
                                    {
                                        keyRetrievalSet = new HashSet<int>();
                                        numericAndTextKeys[textItem.CharacteristicNumber] = keyRetrievalSet;
                                        numericAndTextKeysRO[textItem.CharacteristicNumber] = keyRetrievalSet;
                                    }
                                    keyRetrievalSet.Add(textItem.QdasNumber.EvaluationNumber);
                                    break;
                                case TextEvaluationType.Part:
                                    partTextKeys.Add(textItem.QdasNumber.EvaluationNumber);
                                    break;
                                case TextEvaluationType.Characteristic:
                                    if (!characteristicTextKeys.TryGetValue(textItem.CharacteristicNumber, out keyRetrievalSet))
                                    {
                                        keyRetrievalSet = new HashSet<int>();
                                        characteristicTextKeys[textItem.CharacteristicNumber] = keyRetrievalSet;
                                        characteristicTextKeysRO[textItem.CharacteristicNumber] = keyRetrievalSet;
                                    }
                                    keyRetrievalSet.Add(textItem.QdasNumber.EvaluationNumber);
                                    break;
                                case TextEvaluationType.AdditionalPart:
                                    additionalPart.Add(textItem.QdasNumber.EvaluationNumber);
                                    break;
                                case TextEvaluationType.AdditionalCharacteristic:
                                    if (!additionalCharacteristic.TryGetValue(textItem.CharacteristicNumber, out keyRetrievalSet))
                                    {
                                        keyRetrievalSet = new HashSet<int>();
                                        additionalCharacteristic[textItem.CharacteristicNumber] = keyRetrievalSet;
                                        additionalCharacteristicRO[textItem.CharacteristicNumber] = keyRetrievalSet;
                                    }
                                    keyRetrievalSet.Add(textItem.QdasNumber.EvaluationNumber);
                                    break;
                                default:
                                    throw new NotImplementedException($"Unknown evaluation value of {textItem.QdasNumber.EvaluationType} for {nameof(TextEvaluationType)} encountered during data retrieval.");
                            }
                        }
                        break;
                    case nameof(ImageGPEditorItem):
                        ImageGPEditorItem imageItem = (ImageGPEditorItem)item;
                        if (imageItem.ImageSourceType == ImageSourceType.Qdas && imageItem.EvaluationCode.HasValue)
                        {
                            if (!imageKeys.TryGetValue(imageItem.CharacteristicNumber, out imageRetrievalSet))
                            {
                                imageRetrievalSet = new HashSet<QsStatImageRequest>();
                                imageKeys[imageItem.CharacteristicNumber] = imageRetrievalSet;
                                imageKeysRO[imageItem.CharacteristicNumber] = imageRetrievalSet;
                            }
                            var request = new QsStatImageRequest(imageItem.EvaluationCode.Value, imageItem.PercentageWidth, imageItem.PercentageHeight);
                            imageRetrievalSet.Add(request);
                        }
                        break;
                }
            }
            return new QsStatRetrievalItemsMultipleCharacteristics()
            {
                PartTextKeys = partTextKeys,
                CharacteristicTextKeys = characteristicTextKeysRO,
                NumericAndTextKeys = numericAndTextKeysRO,
                ImageKeys = imageKeysRO,
                AdditionalPartKeys = additionalPart,
                AdditionalCharacteristicKeys = additionalCharacteristicRO
            };
        }

        private void ResetElements(bool resetSpcPath)
        {
            foreach (var item in ContainedElements)
            {
                switch (item)
                {
                    case TextGPEditorItem textItem:
                        if (!resetSpcPath && textItem.TextType == TextType.SpcNumber || textItem.TextType == TextType.UserText)
                            continue;
                        textItem.Text = String.Empty;
                        break;
                    case ImageGPEditorItem imageItem when imageItem.ImageSourceType==ImageSourceType.Qdas:
                        imageItem.Image = null;
                        break;
                }
            }
        }

        private void InitializeElements(SpcPath spcPath)
        {
            foreach (var item in ContainedElements)
            {
                switch (item)
                {
                    case TextGPEditorItem textItem:
                        switch (textItem.TextType)
                        {
                            case TextType.SpcNumber:
                                if (String.IsNullOrEmpty(spcPath.HintLine))
                                {
                                    textItem.Text = $"SPC_{spcPath.SpcNumber:D3}";
                                }
                                else
                                {
                                    textItem.Text = spcPath.HintLine;
                                }
                                break;
                            case TextType.Qdas:
                                textItem.Text = String.Empty;
                                break;
                            //do nothing for TextType.UserText
                        }
                        break;
                    case ImageGPEditorItem imageItem when imageItem.ImageSourceType==ImageSourceType.Qdas:
                        imageItem.Image = null;
                        break;
                }
            }
        }

        private void UpdateElements(IEnumerable<EditorItemBase> items, QsStatPartResultDTO data)
        {
            string textResult = null;
            QsStatCharacteristicResultDTO tmpCharacteristicResult = null;
            foreach (var item in items)
            {
                switch (item)
                {
                    case TextGPEditorItem textItem:
                        if (textItem.TextType == TextType.Qdas && textItem.QdasNumber != null)
                        {
                            switch (textItem.QdasNumber.EvaluationType)
                            {
                                case TextEvaluationType.NumericAndTextResults:

                                    if (data.CharacteristicResults.TryGetValue(textItem.CharacteristicNumber, out tmpCharacteristicResult) &&
                                        tmpCharacteristicResult.NumericAndTextResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out Tuple<string, double> tmpNumericText))
                                    {
                                        textItem.Text = tmpNumericText.Item1;
                                    }
                                    else
                                        textItem.Text = String.Empty;
                                    break;
                                case TextEvaluationType.Part:
                                    if (data.PartTextResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out textResult))
                                    {
                                        textItem.Text = textResult;
                                    }
                                    else
                                        textItem.Text = String.Empty;
                                    break;
                                case TextEvaluationType.Characteristic:
                                    if (data.CharacteristicResults.TryGetValue(textItem.CharacteristicNumber, out tmpCharacteristicResult) &&
                                        tmpCharacteristicResult.CharacteristicTextResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out textResult))
                                    {
                                        textItem.Text = textResult;
                                    }
                                    else
                                        textItem.Text = String.Empty;
                                    break;
                                case TextEvaluationType.AdditionalPart:
                                    if (data.MeasuringResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out textResult))
                                    {
                                        textItem.Text = textResult;
                                    }
                                    else
                                        textItem.Text = String.Empty;
                                    break;
                                case TextEvaluationType.AdditionalCharacteristic:
                                    if (data.CharacteristicResults.TryGetValue(textItem.CharacteristicNumber, out tmpCharacteristicResult) &&
                                        tmpCharacteristicResult.MeasuringResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out textResult))
                                    {
                                        textItem.Text = textResult;
                                    }
                                    else
                                        textItem.Text = String.Empty;
                                    break;
                                default:
                                    throw new NotImplementedException($"Unknown evaluation value of {textItem.QdasNumber.EvaluationType} for {nameof(TextEvaluationType)} encountered during data retrieval.");
                            }
                        }
                        break;
                    case ImageGPEditorItem imageItem:
                        UpdateImageItem(data, imageItem);
                        break;
                }
            }
        }

        private void UpdateImageElements(IEnumerable<EditorItemBase> items, QsStatPartResultDTO data)
        {
            foreach (var imageItem in items.OfType<ImageGPEditorItem>()){
                UpdateImageItem(data, imageItem);
            }
        }

        private static void UpdateImageItem(QsStatPartResultDTO data, ImageGPEditorItem imageItem)
        {
            if (imageItem.ImageSourceType == ImageSourceType.Qdas && imageItem.EvaluationCode.HasValue)
            {
                var request = new QsStatImageRequest(imageItem.EvaluationCode.Value, imageItem.PercentageWidth, imageItem.PercentageHeight);
                if (data.CharacteristicResults.TryGetValue(imageItem.CharacteristicNumber, out QsStatCharacteristicResultDTO tmpCharacteristicResult) &&
                    tmpCharacteristicResult.ImageResults.TryGetValue(request, out byte[] tmpImage))
                {
                    try
                    {
                        if (tmpImage != null)
                            imageItem.Image = tmpImage;
                        else
                            imageItem.Image = null;//TODO maybe retrieve a no data image?
                    }
                    catch
                    {
                        imageItem.Image = null;
                    }
                }
                else
                    imageItem.Image = null;
            }
        }

        #region properties
        internal object ParentView { get; set; }

        public double ViewHeight
        {
            get { return _viewHeight; }
            set { SetProperty(ref _viewHeight, value); }
        }

        public double ViewWidth
        {
            get { return _viewWidth; }
            set { SetProperty(ref _viewWidth, value); }
        }

        public IReadOnlyCollection<EditorItemBase> ContainedElements
        {
            get { return _readonlyElements; }
            private set { this._readonlyElements = value; }
        }

        public AspectRatio AspectRatio { get; private set; }

        public IReadOnlyList<SpcPath> OrderedSpcs
        {
            get { return _orderedSpcs; }
            private set
            {
                SetProperty(ref _orderedSpcs, value);
            }
        }

        public IReadOnlyList<DataSet> DataSets
        {
            get { return _datasetsForCurrentSpc; }
            private set { SetProperty(ref _datasetsForCurrentSpc, value); }
        }

        private void SetDatasets(IReadOnlyList<DataSet> newDatasets)
        {
            this._datasetsForCurrentSpc = newDatasets;
            RaisePropertyChanged(nameof(DataSets));
            UpdateDatasetNavigation();
        }

        public SpcPath CurrentSpc
        {
            get { return this._currentSpc; }
            private set { SetProperty(ref _currentSpc, value); }
        }

        public Guid CurrentDataSetGuid
        {
            get { return this._currentDatasetGuid; }
            private set
            {
                if (SetProperty(ref _currentDatasetGuid, value))
                {
                    UpdateDatasetNavigation();
                }
            }
        }

        public bool HasNextDataset
        {
            get { return _hasNextDataset; }
            private set {
                if(SetProperty(ref _hasNextDataset, value))
                {
                    NextDatasetCommand.RaiseCanExecuteChanged();
                }
            }
        }
        public bool HasPreviousDataset
        {
            get { return _hasPreviousDataset; }
            private set {
                if (SetProperty(ref _hasPreviousDataset, value))
                {
                    PreviousDatasetCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int CurrentCharacteristicNumber
        {
            get { return _currentCharacteristicNumber; }
            private set
            {
                if (SetProperty(ref _currentCharacteristicNumber, value))
                {
                    UpdateCharacteristicNavigation();
                }
            }
        }

        public IReadOnlyList<int> AvailableCharacteristicNumbers
        {
            get { return _enabledCharacteristics; }
            private set { SetProperty(ref _enabledCharacteristics, value); }
        }

        private void SetAvailableCharacteristicNumbers(IReadOnlyList<int> newNumbers)
        {
            this.AvailableCharacteristicNumbers = newNumbers;
            UpdateCharacteristicNavigation();
        }

        public bool HasNextCharacteristic
        {
            get { return _hasNextCharacteristic; }
            private set {
                if (SetProperty(ref _hasNextCharacteristic, value)){
                    NextCharacteristicCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public bool HasPreviousCharacteristic
        {
            get { return _hasPreviousCharacteristic; }
            private set {
                if (SetProperty(ref _hasPreviousCharacteristic, value))
                {
                    PreviousCharacteristicCommand.RaiseCanExecuteChanged();
                }
            }
        }

        #region commands
        public AsyncCommand<SpcPath> LoadSpcCommand { get; private set; }

        public AsyncCommand LoadedCommand { get; private set; }
        public ICommand SizeChangedCommand {
            get { return _sizeChangedCommand; }
            private set { SetProperty(ref _sizeChangedCommand, value); }
        }

        public AsyncCommand RefreshDatasetCommand { get; private set; }

        public DelegateCommand FirstDatasetCommand { get; private set; }

        public DelegateCommand NextDatasetCommand { get; private set; }

        public DelegateCommand PreviousDatasetCommand { get; private set; }

        public DelegateCommand LastDatasetCommand { get; private set; }

        public DelegateCommand FirstCharacteristicCommand { get; private set; }

        public DelegateCommand NextCharacteristicCommand { get; private set; }

        public DelegateCommand PreviousCharacteristicCommand { get; private set; }

        public DelegateCommand LastCharacteristicCommand { get; private set; }

        #endregion
        #endregion
        #region command handlers
        private async Task ChangeSpcHelperAsync(SpcPath newSpc)
        {
            this._spcDataCts.Cancel();
            this._spcDataCts = new CancellationTokenSource();
            CurrentSpc = newSpc;
            IReadOnlyList<DataSet> availableDatasets;

            if (this._spcPathsAndDatasets.TryGetValue(CurrentSpc, out FolderDatasets folderDs))
            {
                this._currentDatasetsChangedHandler = GenerateSpcPathDatasetsChangedEventHandler(CurrentSpc);
                PropertyChangedEventManager.AddHandler(folderDs, this._currentDatasetsChangedHandler, nameof(FolderDatasets.DataSets));
                availableDatasets = folderDs.DataSets;
            }else
            {
                this._currentDatasetsChangedHandler = null;
                availableDatasets = new List<DataSet>();
            }
            Guid firstDsId = availableDatasets.Any() ? availableDatasets.First().Guid : Guid.Empty;
            ResetElements(false);
            this._currentDatasetGuid = firstDsId;
            RaisePropertyChanged(nameof(CurrentDataSetGuid));
            SetDatasets(availableDatasets);
            this._currentCharacteristicNumber = -1;
            RaisePropertyChanged(nameof(CurrentCharacteristicNumber));
            SetAvailableCharacteristicNumbers(new List<int>());
            if (Guid.Equals(CurrentSpc.Guid, Guid.Empty))
            {
                ResetElements(true);
            } else
            {
                InitializeElements(CurrentSpc);
            }
            await this._calculation.RequestLoadData(this.CurrentSpc, firstDsId, this._spcDataCts.Token).ConfigureAwait(false);
        }

        private void FirstCharacteristicCommandHandler()
        {
            this._characteristicCts.Cancel();
            if (!AvailableCharacteristicNumbers.Any() || CurrentCharacteristicNumber == AvailableCharacteristicNumbers.First())
                return;
            CurrentCharacteristicNumber = AvailableCharacteristicNumbers.First();
        }

        private void PreviousCharacteristicCommandHandler()
        {
            this._characteristicCts.Cancel();
            int index = AvailableCharacteristicNumbers.ToList().IndexOf(CurrentCharacteristicNumber);
            if (index < 0)
                return;
            CurrentCharacteristicNumber = AvailableCharacteristicNumbers.ElementAt(index - 1);
        }

        private void NextCharacteristicCommandHandler()
        {
            this._characteristicCts.Cancel();
            int index = AvailableCharacteristicNumbers.ToList().IndexOf(CurrentCharacteristicNumber);
            if (index < 0 || index >= this.AvailableCharacteristicNumbers.Count -1)
                return;
            CurrentCharacteristicNumber = AvailableCharacteristicNumbers.ElementAt(index + 1);
        }

        private void LastCharacteristicCommandHandler()
        {
            this._characteristicCts.Cancel();
            if (!AvailableCharacteristicNumbers.Any() || CurrentCharacteristicNumber == AvailableCharacteristicNumbers.Last())
                return;
            CurrentCharacteristicNumber = AvailableCharacteristicNumbers.Last();
        }

        public string GetQdasImageItemDetailsForPosition(Size imageSize, ImageGPEditorItem item, Point pos)
        {
            var cts = this._spcDataCts.Token;
            int characteristic = item.CharacteristicNumber != 0 ? item.CharacteristicNumber : CurrentCharacteristicNumber;
            int charaIndex = characteristic - 1;
            if (item.ImageSourceType != ImageSourceType.Qdas || item.Image == null || !item.EvaluationCode.HasValue || imageSize.Width <= 1 || imageSize.Height <= 1 || characteristic < 1)
                return String.Empty;

            var data = this._loadedDataset.ParsedData;
            int valueNr;
            try
            {
                cts.ThrowIfCancellationRequested();
                if (data.CharacteristicsData[charaIndex].iMerkmal_Art != (short)iMerkmal_Art.imaVARIABEL)
                    return string.Empty;

                valueNr = this._calculation.GetClickedValueNr(item.EvaluationCode.Value, characteristic, imageSize.Width, imageSize.Height, pos.X, pos.Y).Result;             
                cts.ThrowIfCancellationRequested();
                if (valueNr < 1)
                    return String.Empty;
                var valueNrData = data.CharacteristicsValues.ElementAt(valueNr-1)[charaIndex];
                string result = $"{valueNr} / {valueNrData.dMesswert} / {valueNrData.sDatumZeit} / #{valueNrData.sChargenNr}";
                cts.ThrowIfCancellationRequested();
                return result;
            }
            catch {
                return String.Empty;
            }         
        }

        private void FirstDatasetCommandHandler()
        {
            this._spcDataCts.Cancel();
            this._spcDataCts = new CancellationTokenSource();
            this._characteristicCts = new CancellationTokenSource();
            if (!DataSets.Any() || DataSets[0].Guid.Equals(CurrentDataSetGuid))
                return;
            ResetElements(false);
            var nextDataSetGuid = DataSets[0].Guid;
            CurrentDataSetGuid = nextDataSetGuid;
            this._currentCharacteristicNumber = -1;
            SetAvailableCharacteristicNumbers(new List<int>());
            this._calculation.RequestLoadData(this.CurrentSpc, nextDataSetGuid, this._spcDataCts.Token).ConfigureAwait(false);
        }

        private void PreviousDatasetCommandHandler()
        {
            this._spcDataCts.Cancel();
            this._spcDataCts = new CancellationTokenSource();
            int index = this.DataSets.ToList().FindIndex(item => Guid.Equals(item.Guid, CurrentDataSetGuid));
            if (index < 1)
                return;
            ResetElements(false);
            var nextDataSetGuid = DataSets[index - 1].Guid;
            CurrentDataSetGuid = nextDataSetGuid;
            this._currentCharacteristicNumber = -1;
            SetAvailableCharacteristicNumbers(new List<int>());
            this._calculation.RequestLoadData(this.CurrentSpc, nextDataSetGuid, this._spcDataCts.Token).ConfigureAwait(false);
        }

        private void NextDatasetCommandHandler()
        {
            this._spcDataCts.Cancel();
            this._spcDataCts = new CancellationTokenSource();
            this._characteristicCts = new CancellationTokenSource();
            int index = this.DataSets.ToList().FindIndex(item => Guid.Equals(item.Guid, CurrentDataSetGuid));
            if (index < 0 || index >= this.DataSets.Count-1)
                return;
            ResetElements(false);
            var nextDataSetGuid = DataSets[index + 1].Guid;
            CurrentDataSetGuid = nextDataSetGuid;
            this._currentCharacteristicNumber = -1;
            SetAvailableCharacteristicNumbers(new List<int>());
            this._calculation.RequestLoadData(this.CurrentSpc, nextDataSetGuid, this._spcDataCts.Token).ConfigureAwait(false);
        }

        private void LastDatasetCommandHandler()
        {
            this._spcDataCts.Cancel();
            this._spcDataCts = new CancellationTokenSource();
            this._characteristicCts = new CancellationTokenSource();
            if (!DataSets.Any() || DataSets.Last().Guid.Equals(CurrentDataSetGuid))
                return;
            ResetElements(false);
            var nextDataSetGuid = DataSets[DataSets.Count-1].Guid;
            CurrentDataSetGuid = nextDataSetGuid;
            this._currentCharacteristicNumber = -1;
            SetAvailableCharacteristicNumbers(new List<int>());
            this._calculation.RequestLoadData(this.CurrentSpc, nextDataSetGuid, this._spcDataCts.Token).ConfigureAwait(false);
        }

        private async Task SizeChangedCommandHandlerAsync(CancellationToken ct)
        {
            var newCts = new CancellationTokenSource();
            this._spcDataCts.Cancel();
            this._spcDataCts = newCts;
            this._displaySize = new Size(ViewWidth, ViewHeight);
            await GetNewDataAndUpdateAsync(newCts.Token);
        }

        private async Task ChangeSpcCommandAsync(SpcPath spcPath, CancellationToken ct)
        {
            await ChangeSpcHelperAsync(spcPath);
        }

        private async Task LoadedCommandHandlerAsync(CancellationToken ct)
        {
            var newCts = new CancellationTokenSource();
            this._characteristicCts.Cancel();
            this._characteristicCts = newCts;
            this._displaySize = new Size(ViewWidth, ViewHeight);
            SizeChangedCommand = new AsyncCommand(SizeChangedCommandHandlerAsync);
            await GetNewDataAndUpdateAsync(newCts.Token);        
        }

        private async Task RefreshDataSetCommandHandlerAsync()
        {
            this._spcDataCts.Cancel();
            this._spcDataCts = new CancellationTokenSource();
            this._characteristicCts = new CancellationTokenSource();
            if (!DataSets.Any())
                return;
            ResetElements(false);
            await Task.Run(async()=>await this._calculation.RequestLoadData(this.CurrentSpc, CurrentDataSetGuid, this._spcDataCts.Token));
        }

        private async Task GetNewDataAndUpdateAsync(CancellationToken ct)
        {
            if (CurrentCharacteristicNumber == -1)
            {
                return;
            }
            //maybe split this function and move the update and task.run one level upwards?
            var data = await Task.Run(async()=> await this._calculation.RequestAllData(this._loadedDataset, this._retrievalItems, CurrentCharacteristicNumber, this._displaySize, ct));
            ct.ThrowIfCancellationRequested();
            UpdateElements(ContainedElements, data);
        }
        #endregion

        #region helpers
        private void UpdateCharacteristicNavigation()
        {
            int index = this._enabledCharacteristics.ToList().IndexOf(CurrentCharacteristicNumber);
            HasNextCharacteristic = index >= 0 && index < this._enabledCharacteristics.Count - 1;
            HasPreviousCharacteristic = index > 0;
            FirstCharacteristicCommand.RaiseCanExecuteChanged();
            LastCharacteristicCommand.RaiseCanExecuteChanged();
        }

        private void UpdateDatasetNavigation()
        {
            int index = this._datasetsForCurrentSpc.ToList().FindIndex(item => Guid.Equals(item.Guid, CurrentDataSetGuid));
            HasNextDataset = index >= 0 && index < this._datasetsForCurrentSpc.Count - 1;
            HasPreviousDataset = index > 0;
            FirstDatasetCommand.RaiseCanExecuteChanged();
            LastDatasetCommand.RaiseCanExecuteChanged();
        }

        private Tuple<SpcPath, FolderDatasets> GetSpcPathDataOrFirstAvailable(SpcPath spc, IReadOnlyDictionary<SpcPath, FolderDatasets> availableData)
        {
            SpcPath selectedSpcPath = null;
            FolderDatasets selectedFolderDatasets = null;
            var toFindGuid = this._currentSpc?.Guid ?? Guid.Empty;
            var found = availableData.Where(kvp => kvp.Key.Guid.Equals(toFindGuid));
            if (found.Any())
            {
                var elem = found.First();
                selectedSpcPath = elem.Key;
                selectedFolderDatasets = elem.Value;
            }
            else
            {
                var elem = availableData.First();
                selectedSpcPath = elem.Key;
                selectedFolderDatasets = elem.Value;
            }
            return new Tuple<SpcPath, FolderDatasets>(selectedSpcPath, selectedFolderDatasets);
        }
        #endregion
    }
}
