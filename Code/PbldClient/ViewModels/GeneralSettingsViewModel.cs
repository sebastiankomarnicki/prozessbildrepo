﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using Promess.Pbld.Data.Settings;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class GeneralSettingsViewModel : ViewModel<IGeneralSettingsView>
    {
        private GeneralSettings _settings;

        [ImportingConstructor]
        public GeneralSettingsViewModel(IGeneralSettingsView view) : base(view)
        {
        }

        public bool IsValid { get; set; }

        internal void Initialize(GeneralSettings settings)
        {
            Settings = settings;
        }

        public GeneralSettings Settings
        {
            get { return _settings; }
            internal set { SetProperty(ref _settings, value); }
        }
    }
}
