﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class SelectQdasNumberViewModel : ViewModel<ISelectQdasNumberView>
    {
        private bool _dialogResult;
        private IReadOnlyCollection<Tuple<QdasNumber, string>> _items;
        private QdasNumber _selectedQdasNumber;
        private DelegateCommand _okCommand;

        [ImportingConstructor]
        public SelectQdasNumberViewModel(ISelectQdasNumberView view) : base(view)
        {
            this._dialogResult = false;
            this._okCommand = new DelegateCommand(OkCommandHandler, ()=>SelectedQdasNumber != null);
        }

        public void Initialize(IReadOnlyCollection<Tuple<QdasNumber, string>> items)
        {
            this.Items = items;
        }

        public IReadOnlyCollection<Tuple<QdasNumber, string>> Items
        {
            get { return _items; }
            private set { SetProperty(ref _items, value); }
        }

        public QdasNumber SelectedQdasNumber
        {
            get { return _selectedQdasNumber; }
            set { if(SetProperty(ref _selectedQdasNumber, value))
                {
                    _okCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand OkCommand
        {
            get { return _okCommand; }
        }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        private void OkCommandHandler()
        {
            this._dialogResult = true;
            ViewCore.Close();
        }
    }
}
