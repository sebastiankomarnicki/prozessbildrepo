﻿using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using Promess.QsStat.Services;
using Promess.QsStat.Data;
using Promess.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Data;
using System.Collections.Specialized;
using System.Collections.ObjectModel;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.Shared)]
    public class PartOverviewViewModel : ViewModel<IPartOverviewView>
    {
        #region fields
        private readonly QsStatService _qsStatService;

        private List<QdasNumberWrapper> _characteristicColumnConfig;
        private CollectionView _characteristicsView;
        private SpcPath _displaySpcPath;
        private Guid _fileGuid;
        private ObservableCollection<QsStatCharacteristicResultDTO> _characteristicData;
        #endregion

        [ImportingConstructor]
        public PartOverviewViewModel(IPartOverviewView view,
            QsStatService qsStatService) : base(view)
        {
            this._qsStatService = qsStatService;
            this._fileGuid = Guid.Empty;
            this._displaySpcPath = null;
        }

        public void Initialize(object parentView, ICollection<QdasNumberWrapper> initialColumns)
        {
            this.ParentView = parentView;
            this._characteristicColumnConfig = new List<QdasNumberWrapper>(initialColumns);
            PropertyChangedEventManager.AddHandler(this._qsStatService, SpcPathResultsChangedHandler, nameof(QsStatService.SpcPathResults));
            PropertyChangedEventManager.AddHandler(this._qsStatService, CharacteristicKFieldsChangedHandler, nameof(QsStatService.CharacteristicKFields));
            PropertyChangedEventManager.AddHandler(this._qsStatService, StatResultChangedHandler, nameof(QsStatService.StatResult));
            GenerateDataView();
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshDataHandler);
        }

        public void SetToDisplayPart(SpcPath displaySpcPath, Guid fileGuid)
        {
            this._displaySpcPath = displaySpcPath;
            this._fileGuid = fileGuid;
            GenerateDataView();
        }

        internal void UpdateCharacteristicColumnConfig(List<QdasNumberWrapper> newConfig)
        {
            this._characteristicColumnConfig = newConfig;
            RaisePropertyChanged(nameof(CharacteristicColumnConfig));
        }

        #region properties
        public object ParentView { get; private set; }

        public CollectionView CharacteristicsView
        {
            get { return _characteristicsView; }
            private set { SetProperty(ref _characteristicsView, value); }
        }

        public List<QdasNumberWrapper> CharacteristicColumnConfig
        {
            get { return _characteristicColumnConfig; }
            private set { SetProperty(ref _characteristicColumnConfig, value); }
        }

        public IReadOnlyDictionary<int, string> CharacteristicKFields
        {
            get { return _qsStatService.CharacteristicKFields; }
        }

        public IReadOnlyDictionary<int, string> NumericAndTextKFields
        {
            get { return _qsStatService.StatResult; }
        }
        #endregion

        private void GenerateDataView()
        {
            var dataList = new List<QsStatCharacteristicResultDTO>();
           
            SpcPathEvaluationDTO spcData = null;
            if (_displaySpcPath!=null && _qsStatService.SpcPathResults.TryGetValue(_displaySpcPath, out spcData))
            {
                if (spcData.EvaluatedData.TryGetValue(this._fileGuid, out QsStatPartResultDTO partData))
                {
                    foreach (var characteristic in partData.CharacteristicResults.Where(kvp => kvp.Value.CharacteristicMetaData.Enabled).OrderBy(kvp => kvp.Key))
                        dataList.Add(characteristic.Value);
                }
            }

            this._characteristicData = new ObservableCollection<QsStatCharacteristicResultDTO>(dataList);
            var view = (CollectionView)CollectionViewSource.GetDefaultView(this._characteristicData);
            CharacteristicsView = view;
        }

        #region event handlers
        private void SpcPathResultsChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            DispatchHelper.BeginInvoke(SpcPathResultsChangedHandlerImpl);
        }

        private void SpcPathResultsChangedHandlerImpl()
        {
            GenerateDataView();
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshDataHandler);
        }

        private void CharacteristicKFieldsChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            RaisePropertyChanged(nameof(CharacteristicKFields));
        }

        private void StatResultChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            RaisePropertyChanged(nameof(NumericAndTextKFields));
        }

        private void RefreshDataHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add)
                return;
            var replacedData = e.NewItems[0] as KeyValuePair<SpcPath, SpcPathEvaluationDTO>?;
            if (!replacedData.HasValue)
                throw new ArgumentOutOfRangeException($"{nameof(NotifyCollectionChangedEventArgs)} is missing the replaced element");
                
            DispatchHelper.BeginInvoke(()=>RefreshDataHandlerImpl(replacedData.Value));
        }

        private void RefreshDataHandlerImpl(KeyValuePair<SpcPath, SpcPathEvaluationDTO> replacedData)
        {
            var spcPath = replacedData.Key;
            if (!spcPath.Equals(this._displaySpcPath))
                return;
            var newData = replacedData.Value;
            var dataList = new List<QsStatCharacteristicResultDTO>();
            this._characteristicData.Clear();
            if (newData.EvaluatedData.TryGetValue(this._fileGuid, out QsStatPartResultDTO partData))
            {
                //note mbe: this will fire a lot of notifications. maybe replace? do not know how to notify the view then
                foreach (var characteristic in partData.CharacteristicResults.Where(kvp => kvp.Value.CharacteristicMetaData.Enabled).OrderBy(kvp => kvp.Key))
                    this._characteristicData.Add(characteristic.Value);
            }
        }
        #endregion
    }
}
