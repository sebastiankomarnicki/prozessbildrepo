﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class SelectQdasNumberWrapperViewModel : ViewModel<ISelectQdasNumberWrapperView>
    {
        #region fields
        private bool _dialogResult;

        private TextEvaluationType? _selectedEvaluationType;
        private QdasNumber _selectedItem;
        private DelegateCommand _okCommand;

        private IReadOnlyCollection<Tuple<QdasNumber, string>> _items;
        private IReadOnlyDictionary<TextEvaluationType, IReadOnlyCollection<Tuple<QdasNumber, string>>> _allItems;
        private string _customName;
        #endregion

        [ImportingConstructor]
        public SelectQdasNumberWrapperViewModel(ISelectQdasNumberWrapperView view) : base(view)
        {
            this._dialogResult = false;
            this._okCommand = new DelegateCommand(OkCommandHandler, () => _selectedItem !=null);
        }

        public void Initialize(string title, QdasNumberWrapper initiallySelected, IReadOnlyDictionary<TextEvaluationType, IReadOnlyCollection<Tuple<QdasNumber, string>>> itemsDictionary)
        {
            this.Title = title;
            this._allItems = itemsDictionary;
            this.AvailableTextEvaluationTypes = itemsDictionary.Keys.ToList();
            this.SelectedItem = initiallySelected?.QdasNumber;
            this.SelectedEvaluationType = initiallySelected?.QdasNumber.EvaluationType??AvailableTextEvaluationTypes.First();
            this._items = this._allItems[this.SelectedEvaluationType.Value];
            this.CustomName = initiallySelected?.CustomName??"";
            PropertyChangedEventManager.AddHandler(this, SelectedItemChangedHandler, nameof(SelectedItem));
            PropertyChangedEventManager.AddHandler(this, SelectedTextEvaluationTypeChangedHandler, nameof(SelectedEvaluationType));
        }

        public DelegateCommand OkCommand
        {
            get { return _okCommand; }
        }

        public TextEvaluationType? SelectedEvaluationType
        {
            get { return _selectedEvaluationType; }
            set
            {
                if (SetProperty(ref _selectedEvaluationType, value))
                {
                    SelectedItem = null;
                }
            }
        }

        public string CustomName
        {
            get { return _customName; }
            set { SetProperty(ref _customName, value); }
        }

        public QdasNumber SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                SetProperty(ref _selectedItem, value);
            }
        }

        public IReadOnlyCollection<Tuple<QdasNumber, string>> Items
        {
            get { return _items; }
            private set { SetProperty(ref _items, value); }
        }

        public IReadOnlyCollection<TextEvaluationType> AvailableTextEvaluationTypes { get; private set; }

        public string Title { get; private set; }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        #region helpers
        private void OkCommandHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }

        private void SelectedItemChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            CustomName = "";
            OkCommand.RaiseCanExecuteChanged();
        }

        private void SelectedTextEvaluationTypeChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            if (SelectedEvaluationType.HasValue)
            {
                Items = _allItems[SelectedEvaluationType.Value];
            }else
            {
                Items = null;
            }
        }
        #endregion
    }
}
