﻿using Promess.Language;
using Promess.Pbld.IViews;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class AddItemViewModel : ViewModel<IAddItemView>
    {
        #region fields
        private readonly DelegateCommand _addItemCommand;

        private string _newItemName;
        private string _status;
        private Func<string, bool> _tryAddItem;
        #endregion

        [ImportingConstructor]
        public AddItemViewModel(IAddItemView view) : base(view)
        {
            _newItemName = "";
            _status = "";
            _addItemCommand = new DelegateCommand(TryAddItem, ()=>!String.IsNullOrWhiteSpace(NewItemName) && NewItemName.Length<=30);
            PropertyChangedEventManager.AddHandler(this, AddItemPropertyChanged, "");
        }

        public void Initialize(Func<string, bool> tryAddItem)
        {
            this._tryAddItem = tryAddItem;
        }

        public void ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
        }

        #region properties
        public string NewItemName
        {
            get { return _newItemName; }
            set
            {
                SetProperty(ref _newItemName, value);
            }
        }

        public string Status
        {
            get { return _status; }
            private set
            {
                SetProperty(ref _status, value);
            }
        }

        public ICommand TryAddItemCommand
        {
            get { return _addItemCommand; }
        }
        #endregion

        private void TryAddItem()
        {
            if (_tryAddItem(NewItemName.Trim()))
            {
                ViewCore.Close();
            }else
            {
                Status = LanguageManager.Current.CurrentLanguageData.Translate("NameAlreadyPresent") as String;
            }
        }

        private void AddItemPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(NewItemName)))
                _addItemCommand.RaiseCanExecuteChanged();
        }
    }
}
