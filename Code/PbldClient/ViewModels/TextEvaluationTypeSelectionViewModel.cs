﻿using Promess.Language;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.IViews;
using Promess.QsStat.Services;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class TextEvaluationTypeSelectionViewModel : ViewModel<ITextEvaluationTypeSelectionView>
    {
        private bool _dialogResult;

        private TextEvaluationType? _selectedEvaluationType;
        private int? _selectedItem;
        private DelegateCommand _okCommand;

        private IReadOnlyDictionary<TextEvaluationType, IReadOnlyDictionary<int, string>> _items;

        [ImportingConstructor]
        public TextEvaluationTypeSelectionViewModel(ITextEvaluationTypeSelectionView view, IQsStatService qsStatService) : base(view)
        {
            _dialogResult = false;
            var dict = new Dictionary<TextEvaluationType, IReadOnlyDictionary<int, string>>();
            dict[TextEvaluationType.NumericAndTextResults] = qsStatService.StatResult;
            dict[TextEvaluationType.Part] = qsStatService.PartKFields;
            dict[TextEvaluationType.Characteristic] = qsStatService.CharacteristicKFields;
            dict[TextEvaluationType.AdditionalPart] = GenerateAdditionalQueriesPartDict();
            dict[TextEvaluationType.AdditionalCharacteristic] = GenerateAdditionalQueriesCharacteristicsDict();
            this._items = dict;
            this._okCommand = new DelegateCommand(OkCommandHandler, () => _selectedItem.HasValue);
        }

        internal void Initialize(QdasNumber qdasNumber)
        {
            if (qdasNumber is null)
            {
                this.SelectedEvaluationType = TextEvaluationType.Part;
            }
            else
            {
                this.SelectedEvaluationType = qdasNumber.EvaluationType;
                this.SelectedItem = qdasNumber.EvaluationNumber;
            }
        }

        private IReadOnlyDictionary<int, string> GenerateAdditionalQueriesPartDict()
        {
            var result = new Dictionary<int, string>();
            result[(int)AdditionalQueriesPart.NumberParts] = (string)LanguageManager.Current.CurrentLanguageData.Translate("NumberParts");
            result[(int)AdditionalQueriesPart.NumberOk] = (string)LanguageManager.Current.CurrentLanguageData.Translate("NumberOk");
            result[(int)AdditionalQueriesPart.NumberControlLimitViolation] = (string)LanguageManager.Current.CurrentLanguageData.Translate("NumberControlLimitViolation");
            result[(int)AdditionalQueriesPart.NumberWarningLimitViolation] = (string)LanguageManager.Current.CurrentLanguageData.Translate("NumberWarningLimitViolation");
            return result;
        }

        private IReadOnlyDictionary<int, string> GenerateAdditionalQueriesCharacteristicsDict()
        {
            var result = new Dictionary<int, string>();
            result[(int)AdditionalQueriesCharacteristic.LastMeasurementValue] = (string)LanguageManager.Current.CurrentLanguageData.Translate("LastMeasurementValue");
            result[(int)AdditionalQueriesCharacteristic.LastMeasurementDateTime] = (string)LanguageManager.Current.CurrentLanguageData.Translate("LastMeasurementDateTime");
            result[(int)AdditionalQueriesCharacteristic.LastMeasurementBatchNumber] = (string)LanguageManager.Current.CurrentLanguageData.Translate("LastMeasurementBatchNumber");
            result[(int)AdditionalQueriesCharacteristic.LastMeasurementOperator] = (string)LanguageManager.Current.CurrentLanguageData.Translate("LastMeasurementOperator");           
            return result;
        }

        private void OkCommandHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }

        public DelegateCommand OkCommand
        {
            get { return _okCommand; }
        }

        public TextEvaluationType? SelectedEvaluationType
        {
            get { return _selectedEvaluationType; }
            set
            {
                if (SetProperty(ref _selectedEvaluationType, value))
                {
                    SelectedItem = null;
                }
            }
        }

        public int? SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (SetProperty(ref _selectedItem, value))
                {
                    OkCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public IReadOnlyDictionary<TextEvaluationType, IReadOnlyDictionary<int, string>> Items
        {
            get { return _items; }
        }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }
    }
}
