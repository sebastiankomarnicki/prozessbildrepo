﻿using Promess.Pbld.Data.Settings.PresetEditorItems;
using Promess.Pbld.IViews;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class PropertiesFontAndColorViewModel : ViewModel<IPropertiesFontAndColorView>
    {
        private IReadOnlyCollection<int> _textSizes;

        [ImportingConstructor]
        public PropertiesFontAndColorViewModel(IPropertiesFontAndColorView view) : base(view) {
            this._textSizes = new List<int>() { 10, 12, 14, 18, 22, 26 };
        }

        public void Setup(IPresetFontAndColor data)
        {
            this.Data = data;
        }

        public IPresetFontAndColor Data { get; private set; }
        public IReadOnlyCollection<int> TextSizes
        {
            get { return _textSizes; }
        }
    }
}
