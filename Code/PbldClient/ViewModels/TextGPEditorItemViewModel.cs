﻿using Promess.Language;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class TextGPEditorItemViewModel : ViewModel<ITextGPPropertiesView>
    {
        #region fields
        private readonly ExportFactory<SelectFromDictionaryWithFilterViewModel> _selectFromDictionaryWithFilterVMFactory;
        private readonly ExportFactory<TextEvaluationTypeSelectionViewModel> _textEvaluationTypeSelectionVMFactory;
        private readonly ExportFactory<PropertiesRepresentationTextViewModel> _propertiesBorderExtendedVMFactory;
        private readonly ExportFactory<PropertiesFontAndColorExtendedViewModel> _propertiesFontColorExtVMFactory;

        private object _borderExtendedView;
        private object _fontAndColorExtendedView;
        private TextGPEditorItem _data;
        private DelegateCommand _selectTextKNumberCommand;
        #endregion

        [ImportingConstructor]
        public TextGPEditorItemViewModel(ITextGPPropertiesView view,
            ExportFactory<TextEvaluationTypeSelectionViewModel> textEvaluationTypeSelectionVMFactory,
            ExportFactory<SelectFromDictionaryWithFilterViewModel> selectTextQsStatResultVMFactory,
            ExportFactory<PropertiesRepresentationTextViewModel> propertiesBorderExtendedVMFactory,
            ExportFactory<PropertiesFontAndColorExtendedViewModel> propertiesFontColorExtVMFactory) : base(view)
        {
            this._textEvaluationTypeSelectionVMFactory = textEvaluationTypeSelectionVMFactory;
            this._selectFromDictionaryWithFilterVMFactory = selectTextQsStatResultVMFactory;
            this._propertiesBorderExtendedVMFactory = propertiesBorderExtendedVMFactory;
            this._propertiesFontColorExtVMFactory = propertiesFontColorExtVMFactory;
            this._selectTextKNumberCommand = new DelegateCommand(SelectTextKNumberHandler);
        }

        public void Setup(object parentView, TextGPEditorItem data)
        {
            this.ParentView = parentView;
            this.Data = data;
            var borderVm = this._propertiesBorderExtendedVMFactory.CreateExport().Value;
            borderVm.Setup(this.Data);
            this.BorderExtendedView = borderVm.View;
            var fontColorExtVM = this._propertiesFontColorExtVMFactory.CreateExport().Value;
            fontColorExtVM.Setup(this.Data);
            this.FontAndColorExtendedView = fontColorExtVM.View;
        }

        public object ParentView { get; private set; }

        public object BorderExtendedView {
            get { return _borderExtendedView; }
            private set { SetProperty(ref _borderExtendedView, value); }
        }

        public object FontAndColorExtendedView {
            get { return _fontAndColorExtendedView; }
            private set { SetProperty(ref _fontAndColorExtendedView, value); }
        }

        public TextGPEditorItem Data
        {
            get { return _data; }
            private set
            {
                SetProperty(ref _data, value);
            }
        }

        public ICommand SelectKNumberCommand
        {
            get { return _selectTextKNumberCommand; }
        }

        private void SelectTextKNumberHandler()
        {
            var vm = _textEvaluationTypeSelectionVMFactory.CreateExport().Value;
            vm.Initialize(Data.QdasNumber);
            if (vm.ShowDialog(ParentView ?? View))
            {
                Data.QdasNumber = new QdasNumber(vm.SelectedEvaluationType.Value, vm.SelectedItem.Value);
            }
        }

        private IReadOnlyDictionary<Predicate<object>, string> GetFilters()
        {
            var filters = new Dictionary<Predicate<object>, string>();
            filters.Add(new Predicate<object>(x => true), (string)LanguageManager.Current.CurrentLanguageData.Translate("All"));
            filters.Add(new Predicate<object>(x => ((KeyValuePair<int, string>)x).Key >= 1000 && ((KeyValuePair<int, string>)x).Key <= 1999), (string)LanguageManager.Current.CurrentLanguageData.Translate("PartData"));
            filters.Add(new Predicate<object>(x => ((KeyValuePair<int, string>)x).Key >= 2000 && ((KeyValuePair<int, string>)x).Key <= 2999), (string)LanguageManager.Current.CurrentLanguageData.Translate("CharData"));
            return filters;
        }
    }
}
