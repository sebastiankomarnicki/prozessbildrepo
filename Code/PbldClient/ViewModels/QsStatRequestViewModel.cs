﻿using Promess.Common.Util.Async;
using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class QsStatRequestViewModel : ViewModel<IQsStatRequestView>
    {
        private NotifyTaskCompletion<byte[]> _requestedImage;

        [ImportingConstructor]
        public QsStatRequestViewModel(IQsStatRequestView view) : base(view)
        {
        }

        public NotifyTaskCompletion<byte[]> RequestedImage {
            get { return _requestedImage; }
            internal set { SetProperty(ref _requestedImage, value); }
        }
    }
}
