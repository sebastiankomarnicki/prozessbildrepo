﻿using Promess.Pbld.Data.Settings.PresetEditorItems;
using Promess.Pbld.IViews;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class PropertiesFontAndColorExtendedViewModel: ViewModel<IPropertiesFontAndColorExtendedView>
    {
        private IReadOnlyCollection<int> _textSizes;

        [ImportingConstructor]
        public PropertiesFontAndColorExtendedViewModel(IPropertiesFontAndColorExtendedView view) : base(view) {
            this._textSizes = new List<int>() { 10, 12, 14, 18, 22, 26 };
        }

        public void Setup(IPresetFontAndColorExtended data)
        {
            this.Data = data;
        }

        public IPresetFontAndColorExtended Data { get; private set; }
        public IReadOnlyCollection<int> TextSizes
        {
            get { return _textSizes; }
        }
    }
}
