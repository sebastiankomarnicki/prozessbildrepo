﻿using Promess.Common.Util;
using Promess.Common.Util.Async;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.QsStat.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class PanelViewModel : ViewModel<IPanelView>
    {
        private double _viewWidth;
        private double _viewHeight;

        private PanelItemModel _data;
        private ReadOnlyObservableCollection<EditorItemBase> _readonlyElements;

        private ProcessState _processState;

        private bool _requestedNewData;
        private bool _hasNextPart;
        private bool _hasPreviousPart;
        private bool _hasNextCharacteristic;
        private bool _hasPreviousCharacteristic;

        private AsyncCommand _refreshCmd;
        private AsyncCommand _nextDatasetCmd;
        private AsyncCommand _previousDatasetCmd;
        private AsyncCommand _nextCharacteristicCmd;
        private AsyncCommand _previousCharacteristicCmd;

        public event EventHandler Loaded;
        public event SizeChangedEventHandler SizeChanged;

        [ImportingConstructor]
        public PanelViewModel(IPanelView view, ConfigurationService _configurationService) : base(view)
        {
            this.ConfigurationService = _configurationService;
            this._processState = ProcessState.TooFewValues;
            this._requestedNewData = true;
            this._hasNextPart = false;
            this._hasPreviousPart = false;
            this._hasNextCharacteristic = false;
            this._hasPreviousPart = false;
        }

        public void Initialize(PanelItemModel model, SpcPath spcPath, AsyncCommand refreshCommand, AsyncCommand nextDatasetCmd, AsyncCommand previousDatasetCommand,
            AsyncCommand nextCharacteristicCommand, AsyncCommand previousCharacteristicCommand)
        {
            this._data = model;
            this._readonlyElements = new ReadOnlyObservableCollection<EditorItemBase>(model.Items);
            this._refreshCmd = refreshCommand;
            this._nextDatasetCmd = nextDatasetCmd;
            this._previousDatasetCmd = previousDatasetCommand;
            this._nextCharacteristicCmd = nextCharacteristicCommand;
            this._previousCharacteristicCmd = previousCharacteristicCommand;
            this.SpcPath = spcPath;
            InitializeStaticData(spcPath);
            LoadedCommand = new DelegateCommand(LoadedCommandHandler);
            SizeChangedCommand = new DelegateCommand<SizeChangedEventArgs>(SizeChangedCommandHandler);
        }

        private void InitializeStaticData(SpcPath spcPath)
        {
            foreach (var item in this._readonlyElements)
            {
                switch (item.GetType().Name)
                {
                    case nameof(TextEditorItem):
                        TextEditorItem textItem = (TextEditorItem)item;
                        if (textItem.TextType == TextType.SpcNumber)
                        {
                            if (String.IsNullOrEmpty(spcPath.HintLine))
                            {
                                textItem.Text = String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("SPCFormat"), spcPath.SpcNumber);
                            }else
                            {
                                textItem.Text = spcPath.HintLine;
                            }
                        }
                        break;
                }
            }
        }

        public void UpdateData(SpcPathEvaluationDTO newData)
        {
            ProcessState = newData.ProcessState;
            QsStatPartResultDTO partData = null;
            QsStatCharacteristicResultDTO charaData = null;
            if (newData.SelectedPartGuid.Equals(Guid.Empty))
            { 
                HasNextPart = false;
                HasPreviousPart = false;
                HasNextCharacteristic = false;
                HasPreviousCharacteristic = false;
            }
            else
            {
                partData = newData.EvaluatedData[newData.SelectedPartGuid];
                int i = 0;
                foreach (var part in newData.EvaluatedData.Values)
                {
                    if (part.Equals(partData))
                    {
                        HasNextPart = i < newData.EvaluatedData.Count - 1;
                        HasPreviousPart = i != 0;
                        break;
                    }
                    i++;
                }
                if (newData.SelectedCharacteristicIndex == -1)
                {
                    HasPreviousCharacteristic = false;
                    HasNextCharacteristic = false;
                } else
                {
                    charaData = partData.CharacteristicResults[newData.SelectedCharacteristicIndex];
                    var enabledIndices = partData.CharacteristicResults.Where(kvp => kvp.Value.CharacteristicMetaData.Enabled).Select(kvp => kvp.Key);
                    HasNextCharacteristic = enabledIndices.Any(index => index > newData.SelectedCharacteristicIndex);
                    HasPreviousCharacteristic = enabledIndices.Any(index => index < newData.SelectedCharacteristicIndex);
                }
            }

            IReadOnlyDictionary<int, string> partTextResults = partData?.PartTextResults ?? new Dictionary<int, string>();
            IReadOnlyDictionary<int, string> charaTextResults = charaData?.CharacteristicTextResults ?? new Dictionary<int, string>();
            IReadOnlyDictionary<int, Tuple<string, double>> charaNumericAndTextResults = charaData?.NumericAndTextResults ?? new Dictionary<int, Tuple<string, double>>();
            IReadOnlyDictionary<QsStatImageRequest, byte[]> charaImageResults = charaData?.ImageResults ?? new Dictionary<QsStatImageRequest, byte[]>();
            IReadOnlyDictionary<int, string> additionalPartTextResults = partData?.MeasuringResults ?? new Dictionary<int, string>();
            IReadOnlyDictionary<int, string> additionalCharaTextResults = charaData?.MeasuringResults ?? new Dictionary<int, string>();
            string tmpText = null;
            Tuple<string,double> tmpNumericText = null;
            string tmpCharaText = null;
            byte[] tmpImage = null;
            foreach (var item in this._readonlyElements)
            {
                switch (item)
                {
                    case TextEditorItem textItem:
                        if (textItem.TextType == TextType.Qdas && textItem.QdasNumber != null)
                        {
                            switch (textItem.QdasNumber.EvaluationType)
                            {
                                case TextEvaluationType.NumericAndTextResults:
                                    if (charaNumericAndTextResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out tmpNumericText))
                                        textItem.Text = tmpNumericText.Item1;
                                    else
                                        textItem.Text = "";
                                    break;
                                case TextEvaluationType.Part:
                                    if (partTextResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out tmpText))
                                        textItem.Text = tmpText;
                                    else
                                        textItem.Text = "";
                                    break;
                                case TextEvaluationType.Characteristic:
                                    if (charaTextResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out tmpCharaText))
                                        textItem.Text = tmpCharaText;
                                    else
                                        textItem.Text = "";
                                    break;
                                case TextEvaluationType.AdditionalPart:
                                    if (additionalPartTextResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out tmpText))
                                        textItem.Text = tmpText;
                                    else
                                        textItem.Text = "";
                                    break;
                                case TextEvaluationType.AdditionalCharacteristic:
                                    if (additionalCharaTextResults.TryGetValue(textItem.QdasNumber.EvaluationNumber, out tmpCharaText))
                                        textItem.Text = tmpCharaText;
                                    else
                                        textItem.Text = "";
                                    break;
                                default:
                                    throw new NotImplementedException($"Unknown evaluation value of {textItem.QdasNumber.EvaluationType} for {nameof(TextEvaluationType)} encountered during data retrieval.");
                            }
                        }
                        break;
                    case ImageEditorItem imageItem:
                        if (imageItem.ImageSourceType == ImageSourceType.Qdas && imageItem.EvaluationCode.HasValue)
                        {
                            var request = new QsStatImageRequest(imageItem.EvaluationCode.Value, imageItem.PercentageWidth, imageItem.PercentageHeight);
                            if (charaImageResults.TryGetValue(request, out tmpImage) && tmpImage != null)
                            {
                                try
                                {
                                    imageItem.Image = tmpImage;
                                }
                                catch
                                {
                                    imageItem.Image = null;
                                }

                            }
                            else
                                imageItem.Image = null;
                        }
                        break;
                }
            }
            RequestedNewData = false;
        }

        internal object ParentView { get; set; }

        public double ViewHeight
        {
            get { return _viewHeight; }
            set { SetProperty(ref _viewHeight, value); }
        }

        public double ViewWidth
        {
            get { return _viewWidth; }
            set { SetProperty(ref _viewWidth, value); }
        }

        public ICommand LoadedCommand { get; private set; }
        public DelegateCommand <SizeChangedEventArgs> SizeChangedCommand { get; private set; }

        public ICommand RefreshCommand { get { return _refreshCmd; } }

        public ICommand NextDatasetCommand { get { return _nextDatasetCmd; } }

        public ICommand PreviousDatasetCommand { get { return _previousDatasetCmd; } }

        public ICommand NextCharacteristicCommand { get { return _nextCharacteristicCmd; } }

        public ICommand PreviousCharacteristicCommand { get { return _previousCharacteristicCmd; } }

        public ConfigurationService ConfigurationService { get; private set; }

        public SpcPath SpcPath { get; private set; }

        public bool RequestedNewData
        {
            get { return _requestedNewData; }
            set
            {
                if (SetProperty(ref _requestedNewData, value))
                {
                    _nextDatasetCmd.RaiseCanExecuteChanged();
                    _previousDatasetCmd.RaiseCanExecuteChanged();
                    _nextCharacteristicCmd.RaiseCanExecuteChanged();
                    _previousCharacteristicCmd.RaiseCanExecuteChanged();
                }
            }
        }

        public bool HasPreviousPart
        {
            get { return _hasPreviousPart; }
            private set
            {
                if (SetProperty(ref _hasPreviousPart, value))
                {
                    _previousDatasetCmd.RaiseCanExecuteChanged();
                }
            }
        }

        public bool HasNextPart
        {
            get { return _hasNextPart; }
            private set
            {
                if (SetProperty(ref _hasNextPart, value))
                {
                    _nextDatasetCmd.RaiseCanExecuteChanged();
                }
            }
        }

        public bool HasPreviousCharacteristic
        {
            get { return _hasPreviousCharacteristic; }
            private set
            {
                if (SetProperty(ref _hasPreviousCharacteristic, value))
                {
                    _previousCharacteristicCmd.RaiseCanExecuteChanged();
                }
            }
        }

        public bool HasNextCharacteristic
        {
            get { return _hasNextCharacteristic; }
            private set
            {
                if (SetProperty(ref _hasNextCharacteristic, value))
                {
                   _nextCharacteristicCmd.RaiseCanExecuteChanged();
                }
            }
        }

        public ProcessState ProcessState
        {
            get { return _processState; }
            set { SetProperty(ref _processState, value); }
        }

        public ReadOnlyObservableCollection<EditorItemBase> ContainedElements
        {
            get { return _readonlyElements; }
            private set
            {
                SetProperty(ref _readonlyElements, value);
            }
        }

        public PanelItemModel PanelItemModel
        {
            get { return _data; }
        }

        public string PanelName
        {
            get { return _data.Name; }
        }

        private void LoadedCommandHandler()
        {
            Loaded?.Invoke(this, new EventArgs());
        }

        private void SizeChangedCommandHandler(SizeChangedEventArgs ea)
        {
            SizeChanged?.Invoke(this, ea);
        }
    }
}
