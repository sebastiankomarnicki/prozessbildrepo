﻿using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Editor;
using System.ComponentModel;
using System.Linq;
using System.Waf.Applications;
using System.Waf.Applications.Services;

namespace Promess.Pbld.ViewModels
{
    /// <summary>
    /// Extends <see cref="BaseEditorViewModel{TView}"/> by copy, paste and delete operations
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    public abstract class BaseEditorViewModelExtended<TView> : BaseEditorViewModel<TView>
        where TView:IView
    {
        #region fields
        private EditorItemModelBase _firstSelected;
        private EditorItemBase _copiedItem;

        private DelegateCommand _copyFirstSelectedCommand;
        private DelegateCommand _pasteCommand;
        private DelegateCommand _deleteSelectedCommand;
        private DelegateCommand<EditorItemModelBase> _deleteSpecificCommand;
        private DelegateCommand<EditorItemModelBase> _copySpecificCommand;
        #endregion

        public BaseEditorViewModelExtended(TView view, IMessageService messageService) : base(view, messageService)
        {
            this._firstSelected = null;
            this._copyFirstSelectedCommand = new DelegateCommand(CopyFirstSelectedCommandHandler, () => this._selected.Count == 1);
            this._pasteCommand = new DelegateCommand(PasteCommandHandler, () => this._copiedItem != null);
            this._deleteSelectedCommand = new DelegateCommand(DeleteSelected, () => _selected.Count > 0);
            this._deleteSpecificCommand = new DelegateCommand<EditorItemModelBase>(DeleteSpecificCommandHandler);
            this._copySpecificCommand = new DelegateCommand<EditorItemModelBase>(CopySpecificCommandHandler);
        }

        #region properties
        public EditorItemModelBase FirstSelected
        {
            get { return _firstSelected; }
            private set { SetProperty(ref _firstSelected, value); }
        }

        public DelegateCommand CopyFirstSelectedCommand { get { return _copyFirstSelectedCommand; } }
        public DelegateCommand PasteCommand { get { return _pasteCommand; } }
        public DelegateCommand DeleteSelectedCommand { get { return _deleteSelectedCommand; } }
        public DelegateCommand<EditorItemModelBase> CopySpecificCommand { get { return _copySpecificCommand; } }
        public DelegateCommand<EditorItemModelBase> DeleteSpecificCommand { get { return _deleteSpecificCommand; } }
        #endregion

        #region event handlers
        protected override void ProcessIsSelectedChanged(EditorItemModelBase model)
        {
            base.ProcessIsSelectedChanged(model);
            this.FirstSelected = this._selected.FirstOrDefault();
        }
        #endregion   

        public override void ResetData()
        {
            base.ResetData();
            this.FirstSelected = null;
        }

        #region command handlers
        protected override void ClearSelectedItems()
        {
            base.ClearSelectedItems();
            FirstSelected = null;
        }

        protected virtual void AddItem(EditorItemModelBase model)
        {
            model.Item.ZIndex = this._elements.Count;
            this._elements.Add(model);
            PropertyChangedEventManager.AddHandler(model, IsSelectedChanged, nameof(EditorItemModelBase.IsSelected));
        }

        protected virtual void RemoveItem(EditorItemModelBase model)
        {
            if (_elements.Remove(model))
            {
                PropertyChangedEventManager.RemoveHandler(model, IsSelectedChanged, nameof(EditorItemModelBase.IsSelected));
                if (model.IsSelected)
                {
                    _selected.Remove(model);
                }
                foreach (var elem in _elements)
                {
                    if (elem.Item.ZIndex > model.Item.ZIndex)
                        elem.Item.ZIndex--;
                }
            }
        }

        private void DeleteSelected()
        {
            if (_selected.Count > 1 && !this._messageService.ShowYesNoQuestion((string)LanguageManager.Current.CurrentLanguageData.Translate("QConfirmDeleteElements")))
                return;
            while (_selected.Count > 0)
                RemoveItem(_selected.ElementAt(0));
            SelectedCheckCommandsRaiseCanExecuteChanged();
        }

        private void DeleteSpecificCommandHandler(EditorItemModelBase toDelete)
        {
            RemoveItem(toDelete);
            SelectedCheckCommandsRaiseCanExecuteChanged();
        }

        private void CopyFirstSelectedCommandHandler()
        {
            var toCopy = _selected.FirstOrDefault();
            CopyElement(toCopy);            
        }

        private void CopySpecificCommandHandler(EditorItemModelBase toCopy)
        {
            CopyElement(toCopy);
        }

        private void CopyElement(EditorItemModelBase toCopy)
        {
            if (toCopy == null)
                return;
            SavePercentage(toCopy.Item);
            this._copiedItem = SerializeHelper.CreateDataContractCopy(toCopy.Item);

            PasteCommand.RaiseCanExecuteChanged();
        }    

        private void PasteCommandHandler()
        {
            if (this._copiedItem == null)
                return;
            var copy = SerializeHelper.CreateDataContractCopy(this._copiedItem);
            RecalculateActualSizes(copy);
            var wrapped = GenerateItemModel(copy);
            ClearSelectedItems();
            AddItem(wrapped);
            wrapped.IsSelected = true;
        }
        #endregion

        #region abstract members
        protected abstract EditorItemModelBase GenerateItemModel(EditorItemBase elem);
        #endregion

        #region helpers
        protected override void SelectedCheckCommandsRaiseCanExecuteChanged()
        {
            base.SelectedCheckCommandsRaiseCanExecuteChanged();
            DeleteSelectedCommand.RaiseCanExecuteChanged();
            CopyFirstSelectedCommand.RaiseCanExecuteChanged();
            PasteCommand.RaiseCanExecuteChanged();
        }
        #endregion
    }
}
