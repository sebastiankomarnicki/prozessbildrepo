﻿using Promess.Common.Util;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Editor;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Promess.Pbld.ViewModels
{
    public abstract class BaseEditorViewModel<TView> : ViewModel<TView>, IDimensionsWithGrid
        where TView:IView
    {
        #region fields
        protected readonly IMessageService _messageService;

        private GridPointDensity? _gridPointDensity;
        private double _viewWidth;
        private double _viewHeight;

        protected ObservableCollection<EditorItemModelBase> _elements;
        protected HashSet<EditorItemModelBase> _selected;

        private IReadOnlyCollection<Tuple<GridPointDensity?, string>> _availableGridOptions;      
        private ReadOnlyObservableCollection<EditorItemModelBase> _readonlyElements;
        private GridSnap _gridSnap;
        private DrawingBrush _gridBrush;
        private double _moveXAccu;
        private double _moveYAccu;

        private DelegateCommand<Vector> _onMoveCommand;
        private DelegateCommand _clearSelectionCommand;
        private DelegateCommand _bringToFrontCommand;
        private DelegateCommand _sendToBackCommand;
        private DelegateCommand _bringForwardCommand;
        private DelegateCommand _sendBackwardCommand;
        private DelegateCommand _propertiesSelectedCommand;

        protected Random _rnd;
        #endregion

        public BaseEditorViewModel(TView view, IMessageService messageService) : base(view)
        {
            this._messageService = messageService;

            this._gridPointDensity = null;
            this._rnd = new Random();
            this.LastRecalculateSize = new Size(0, 0);
            this._selected = new HashSet<EditorItemModelBase>();
            this._gridSnap = new GridSnap();
            this._gridBrush = null;
            this._moveXAccu = this._moveYAccu = 0;

            this.InitMoveCommand = new DelegateCommand(InitMoveOperationHandler);
            this._onMoveCommand = new DelegateCommand<Vector>(MoveSelectedItems, x => x != null);
            this._clearSelectionCommand = new DelegateCommand(ClearSelectedItems);
            this._bringToFrontCommand = new DelegateCommand(BringSelectedToFront, () => _selected.Count > 0);
            this._sendToBackCommand = new DelegateCommand(BringSelectedToBack, () => _selected.Count > 0);
            this._bringForwardCommand = new DelegateCommand(BringSelectedForward, () => _selected.Count > 0);
            this._sendBackwardCommand = new DelegateCommand(BringSelectedBackward, () => _selected.Count > 0);
            this._propertiesSelectedCommand = new DelegateCommand(PropertiesSelected, () => _selected.Count == 1);

            LoadedCommand = new DelegateCommand(LoadedCommandHandler);
            SizeChangedCommand = new DelegateCommand<SizeChangedEventArgs>(SizeChangedCommandHandler);
            SelectGridDensityCommand = new DelegateCommand<GridPointDensity?>(SelectGridDensityCommandHandler);
            GenerateAvailableGridOptions();
        }

        protected void Initialize(IList<EditorItemModelBase> elems)
        {
            ResetData();
            InitializeContainedElements(elems);
            SetXYDimensions();
        }

        #region properties
        public IReadOnlyCollection<Tuple<GridPointDensity?, string>> AvailableGridOptions
        {
            get { return _availableGridOptions; }
        }

        public GridSnap GridSnap { get { return _gridSnap; } }

        public DrawingBrush GridBrush
        {
            get { return _gridBrush; }
            private set { SetProperty(ref _gridBrush, value); }
        }

        public GridPointDensity? GridPointDensity
        {
            get { return _gridPointDensity; }
            private set
            {
                SetProperty(ref _gridPointDensity, value);
            }
        }

        public double ViewHeight
        {
            get { return _viewHeight; }
            set { SetProperty(ref _viewHeight, value); }
        }

        public double ViewWidth
        {
            get { return _viewWidth; }
            set { SetProperty(ref _viewWidth, value); }
        }

        public double MinXYDimension
        {
            get;
            private set;
        }

        public ReadOnlyObservableCollection<EditorItemModelBase> ContainedElements
        {
            get { return _readonlyElements; }
            private set
            {
                SetProperty(ref _readonlyElements, value);
            }
        }

        public ICommand InitMoveCommand { get; private set; }
        public DelegateCommand<Vector> MoveCommand { get { return _onMoveCommand; } }

        public DelegateCommand ClearSelectionCommand { get { return _clearSelectionCommand; } }

        public DelegateCommand BringForwardCommand { get { return _bringForwardCommand; } }
        public DelegateCommand BringToFrontCommand { get { return _bringToFrontCommand; } }
        public DelegateCommand SendBackwardCommand { get { return _sendBackwardCommand; } }
        public DelegateCommand SendToBackCommand { get { return _sendToBackCommand; } }
        public DelegateCommand PropertiesSelectedCommand { get { return _propertiesSelectedCommand; } }
        public ICommand LoadedCommand { get; private set; }
        public DelegateCommand<SizeChangedEventArgs> SizeChangedCommand { get; private set; }
        public DelegateCommand<GridPointDensity?> SelectGridDensityCommand { get; private set; }

        protected Size LastRecalculateSize { get; private set; }
        #endregion

        #region event handlers
        protected void IsSelectedChanged(object sender, PropertyChangedEventArgs e)
        {
            var model = (EditorItemModelBase)sender;
            ProcessIsSelectedChanged(model);
        }

        protected virtual void ProcessIsSelectedChanged(EditorItemModelBase model)
        {
            if (model.IsSelected)
            {
                this._selected.Add(model);
                if (this._selected.Count == 1 || this._selected.Count == 2)
                {
                    SelectedCheckCommandsRaiseCanExecuteChanged();
                }
            }
            else
            {
                this._selected.Remove(model);
                if (this._selected.Count == 0 || this._selected.Count == 1)
                {
                    SelectedCheckCommandsRaiseCanExecuteChanged();
                }
            }
        }
        #endregion

        protected virtual void SetXYDimensions()
        {
            if (Double.IsNaN(ViewWidth) || Double.IsNaN(ViewHeight) || ViewWidth <= 0 || ViewHeight <= 0)
                return;
            //always set the dimension, otherwise GridSnap has no information Size information when the density changes. Keep this order.
            if (this._gridSnap.RecalculateSnapPoints(new Size(ViewWidth, ViewHeight)) && this.GridPointDensity.HasValue)
            {
                GridBrush = GenerateGridBrush(this._gridSnap.StepX, this._gridSnap.StepY);
            }
            else
            {
                GridBrush = null;
            }
            this.MinXYDimension = Math.Min(ViewWidth, ViewHeight) * EditorItemBase.MINDIMENSIONPERCENTAGE;
            if (ContainedElements != null)
            {
                foreach (var elem in ContainedElements)
                {
                    RecalculateActualSizes(elem.Item);
                }
                this.LastRecalculateSize = new Size(ViewWidth, ViewHeight);
            }
            if (this._selected.Count == 1)
            {
                var item = this._selected.First().Item;
            }            
        }

        internal void SavePercentages()
        {
            if (LastRecalculateSize.Width <= 0 || LastRecalculateSize.Height <= 0)
            {
                Debug.WriteLine("Could not save panel, invalid width or height");
                return;
            }
            foreach (var elem in ContainedElements)
                SavePercentage(elem.Item);
        }

        public virtual void ResetData()
        {
            if (_elements == null)
            {
                return;
            }

            foreach (var model in _elements)
            {
                PropertyChangedEventManager.RemoveHandler(model, IsSelectedChanged, nameof(EditorItemModelBase.IsSelected));
            }
            _selected.Clear();
            _elements.Clear();
            SelectedCheckCommandsRaiseCanExecuteChanged();
        }

        protected void CopyOriginalSizes(EditorItemBase source, EditorItemBase destination)
        {
            destination.X = source.X;
            destination.Y = source.Y;
        }

        #region abstract members
        protected abstract void RecalculateActualSizes(EditorItemBase elem);
        protected abstract void SavePercentage(EditorItemBase elem);
        #endregion

        #region command handlers
        private void SelectGridDensityCommandHandler(GridPointDensity? gridPointDensity)
        {
            AdjustGridSnap(gridPointDensity);
        }

        private void LoadedCommandHandler()
        {
            SetXYDimensions();
        }

        private void SizeChangedCommandHandler(SizeChangedEventArgs ea)
        {
            SavePercentages();
            SetXYDimensions();
        }

        private void InitMoveOperationHandler()
        {
            this._moveXAccu = this._moveYAccu = 0;
            if (GridPointDensity.HasValue)
                AlignSelectedElementsToGrid();
        }

        private void MoveSelectedItems(Vector delta)
        {
            Vector actualDelta;
            Point snapPoint;
            double actualShiftX, actualShiftY;
            this._moveXAccu += delta.X;
            this._moveYAccu += delta.Y;
            if (this.GridPointDensity.HasValue)
                actualDelta = this._gridSnap.ConvertShiftToSnapShift(new Vector(this._moveXAccu, this._moveYAccu));
            else
                actualDelta = new Vector(this._moveXAccu, this._moveYAccu);

            if (Math.Abs(actualDelta.X) < 0.1 && Math.Abs(actualDelta.Y) < 0.1)
                return;
            actualShiftX = actualDelta.X;
            actualShiftY = actualDelta.Y;
            foreach (var item in _selected)
            {
                var bounds = item.Item.GetBoundingRect();
                actualShiftX = CalculatePossibleShift(-bounds.Left, ViewWidth - bounds.Right, actualShiftX);
                actualShiftY = CalculatePossibleShift(-bounds.Top, ViewHeight - bounds.Bottom, actualShiftY);
            }
            //because of double arithmetic and not considering the actual starting point for the shift some shifts to the edges are denied
            if (this.GridPointDensity.HasValue)
                actualDelta = this._gridSnap.ConvertShiftToSnapShift(new Vector(actualShiftX, actualShiftY));
            else
                actualDelta = new Vector(actualShiftX, actualShiftY);
            if (Math.Abs(actualDelta.X) < 0.1 && Math.Abs(actualDelta.Y) < 0.1)
                return;
            this._moveXAccu -= actualDelta.X;
            this._moveYAccu -= actualDelta.Y;
            foreach (var item in _selected)
            {
                snapPoint = new Point(item.Item.X + actualDelta.X, item.Item.Y + actualDelta.Y);
                if (this.GridPointDensity.HasValue)
                    snapPoint = this._gridSnap.Snap(snapPoint);
                item.Item.ShiftPosition(new Vector(snapPoint.X-item.Item.X, snapPoint.Y-item.Item.Y));
            }
        }

        private void BringSelectedForward()
        {
            var orderedSelected = _selected.OrderByDescending(x => x.Item.ZIndex).ToList();
            var totalElements = _elements.Count;
            for (int i = 0; i < orderedSelected.Count; i++)
            {
                var currentElement = orderedSelected.ElementAt(i);
                if (currentElement.Item.ZIndex >= totalElements - 1 - i)
                    continue;
                int newIndex = currentElement.Item.ZIndex + 1;
                var switchElement = _elements.First(x => x.Item.ZIndex == newIndex);
                switchElement.Item.ZIndex = currentElement.Item.ZIndex;
                currentElement.Item.ZIndex = newIndex;
            }
        }

        private void BringSelectedBackward()
        {
            var orderedSelected = _selected.OrderBy(x => x.Item.ZIndex).ToList();
            var totalElements = _elements.Count;
            for (int i = 0; i < orderedSelected.Count; i++)
            {
                var currentElement = orderedSelected.ElementAt(i);
                if (currentElement.Item.ZIndex <= i)
                    continue;
                int newIndex = currentElement.Item.ZIndex - 1;
                var switchElement = _elements.First(x => x.Item.ZIndex == newIndex);
                switchElement.Item.ZIndex = currentElement.Item.ZIndex;
                currentElement.Item.ZIndex = newIndex;
            }
        }

        private void BringSelectedToFront()
        {
            int nextIndexSelected = _elements.Count - 1;
            int nextIndexNotSelected = _elements.Count - 1 - _selected.Count;
            var orderedAll = _elements.OrderByDescending(x => x.Item.ZIndex);
            foreach (var elem in orderedAll)
            {
                if (_selected.Contains(elem))
                {
                    elem.Item.ZIndex = nextIndexSelected;
                    nextIndexSelected--;
                }
                else
                {
                    elem.Item.ZIndex = nextIndexNotSelected;
                    nextIndexNotSelected--;
                }
            }
        }

        private void BringSelectedToBack()
        {
            int nextIndexSelected = 0;
            int nextIndexNotSelected = _selected.Count;
            var orderedAll = _elements.OrderBy(x => x.Item.ZIndex);
            foreach (var elem in orderedAll)
            {
                if (_selected.Contains(elem))
                {
                    elem.Item.ZIndex = nextIndexSelected;
                    nextIndexSelected++;
                }
                else
                {
                    elem.Item.ZIndex = nextIndexNotSelected;
                    nextIndexNotSelected++;
                }
            }
        }

        protected virtual void ClearSelectedItems()
        {
            while (_selected.Count > 0)
            {
                _selected.First().IsSelected = false;
            }
        }

        private void PropertiesSelected()
        {
            _selected.First().ShowSettingsCommand.Execute(null);
        }
        #endregion

        #region helpers
        private void GenerateAvailableGridOptions()
        {
            List<Tuple<GridPointDensity?, string>> options = new List<Tuple<GridPointDensity?, string>>();
            options.Add(new Tuple<GridPointDensity?, string>(null, "GridPointDensityNone"));
            options.Add(new Tuple<GridPointDensity?, string>(Editor.GridPointDensity.Sparse, "GridPointDensitySparse"));
            options.Add(new Tuple<GridPointDensity?, string>(Editor.GridPointDensity.Medium, "GridPointDensityMedium"));
            options.Add(new Tuple<GridPointDensity?, string>(Editor.GridPointDensity.Dense, "GridPointDensityDense"));
            this._availableGridOptions = options.AsReadOnly();
        }

        private void AdjustGridSnap(GridPointDensity? gridPointDensity)
        {
            if (!gridPointDensity.HasValue)
            {
                this.GridPointDensity = gridPointDensity;
                this.GridBrush = null;
            }               
            else if (GridSnap.RecalculateSnapPoints(gridPointDensity.Value))
            {
                this.GridPointDensity = gridPointDensity.Value;
                this.GridBrush = GenerateGridBrush(GridSnap.StepX, GridSnap.StepY);
            }
            else
            {
                //TODO error message
                this.GridPointDensity = null;
                this.GridBrush = null;
            }
        }

        private DrawingBrush GenerateGridBrush(double stepX, double stepY)
        {
            var pen = new Pen() {
                Brush = SystemColors.ControlDarkDarkBrush,
                DashStyle = new DashStyle(new double[] { 0, stepX, 0, stepY },0) };
            var rectangle = new Rect(0, 0, stepX, stepY);
            var rectangleGeom = new RectangleGeometry() { Rect = rectangle};
            var drawing = new GeometryDrawing() { Geometry = rectangleGeom, Pen=pen};
            var viewport = new Rect(2, 2, stepX, stepY);
            var brush = new DrawingBrush(drawing) { TileMode = TileMode.Tile, Viewport=viewport, ViewportUnits=BrushMappingMode.Absolute , Opacity=0.7};
            return brush;
        }

        private void AlignSelectedElementsToGrid()
        {
            double shiftX, shiftY;
            Point snapPoint;
            double snapX, snapY;
            foreach (var item in _selected)
            {
                var bounds = item.Item.GetBoundingRect();
                snapPoint = this._gridSnap.Snap(new Point(item.Item.X, item.Item.Y));
                if (snapPoint.X + bounds.Width < ViewWidth)
                {
                    snapX = snapPoint.X;
                }
                else
                {
                    snapX = this._gridSnap.SnapXLower(item.Item.X);
                }
                if (snapPoint.Y + bounds.Height < ViewHeight)
                {
                    snapY = snapPoint.Y;
                }
                else
                {
                    snapY = this._gridSnap.SnapYLower(item.Item.Y);               
                }
                Debug.Assert(snapX + bounds.Width <= ViewWidth);
                Debug.Assert(snapY + bounds.Height <= ViewHeight);
                shiftX = snapX - item.Item.X;
                shiftY = snapY - item.Item.Y;
                item.Item.ShiftPosition(new Vector(shiftX, shiftY));
            }
        }

        private double CalculatePossibleShift(double lowestValue, double highestValue, double desiredShift)
        {
            if (desiredShift >= 0)
            {
                return Math.Min(highestValue, desiredShift);
            }
            else
            {
                return Math.Max(lowestValue, desiredShift);
            }
        }

        private void InitializeContainedElements(IList<EditorItemModelBase> elems)
        {
            this._elements = new ObservableCollection<EditorItemModelBase>(elems);
            SanitizeZIndices(elems.Select(elem => elem.Item));
            ContainedElements = new ReadOnlyObservableCollection<EditorItemModelBase>(_elements);
        }

        private void SanitizeZIndices(IEnumerable<EditorItemBase> elems)
        {
            var sorted = elems.OrderBy(x => x.ZIndex);
            int i = 0;
            foreach (var elem in sorted)
            {
                elem.ZIndex = i;
                i++;
            }
        }

        protected virtual void SelectedCheckCommandsRaiseCanExecuteChanged()
        {
            BringForwardCommand.RaiseCanExecuteChanged();
            BringToFrontCommand.RaiseCanExecuteChanged();
            SendBackwardCommand.RaiseCanExecuteChanged();
            SendToBackCommand.RaiseCanExecuteChanged();
            PropertiesSelectedCommand.RaiseCanExecuteChanged();
        }
        #endregion
    }
}
