﻿using Promess.Pbld.Data.Settings.PresetEditorItems;
using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class PropertiesRepresentationTextViewModel : ViewModel<IPropertiesRepresentationTextView>
    {
        [ImportingConstructor]
        public PropertiesRepresentationTextViewModel(IPropertiesRepresentationTextView view) : base(view) { }

        public void Setup(IPresetRepresentationText data)
        {
            this.Data = data;
        }

        public IPresetRepresentationText Data { get; private set; }
    }
}
