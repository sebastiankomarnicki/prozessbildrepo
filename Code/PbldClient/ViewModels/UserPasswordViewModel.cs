﻿using Promess.Pbld.IViews;
using System;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserPasswordViewModel:ViewModel<IUserPasswordView>
    {
        #region fields
        private bool _dialogResult;
        #endregion

        [ImportingConstructor]
        public UserPasswordViewModel(IUserPasswordView view) : base(view)
        {
            this._dialogResult = false;
            this.OkCommand = new DelegateCommand(OkCommandHandler);
        }

        public void Setup(String title)
        {
            this.Title = title;
        }

        public string Title { get; private set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public ICommand OkCommand { get; private set; }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        private void OkCommandHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }
    }
}
