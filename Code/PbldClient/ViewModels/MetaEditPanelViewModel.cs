﻿using Promess.Common.Util;
using Promess.Pbld.IViews;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Input;
using System.Diagnostics;
using Promess.Language;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Services;
using System.Waf.Applications.Services;
using System.Windows.Data;
using System.Windows;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class MetaEditPanelViewModel : ViewModel<IMetaEditPanelView>
    {
        #region fields
        private readonly IMainService _mainService;
        private readonly IMessageService _messageService;
        private readonly IFileDialogService _fileDialogservice;
        private readonly ConfigurationService _configurationService;

        private readonly ExportFactory<AddItemViewModel> _addItemViewModelFactory;
        private readonly ExportFactory<TextInputViewModel> _textInputViewModel;

        private ObservableCollection<PanelItemModel> _knownPanelItemModels;

        private ICommand _createNewPanelCommand;
        private DelegateCommand<PanelItemModel> _openPanelCommand;
        private DelegateCommand _deleteCurrentPanelCommand;
        private DelegateCommand _savePanelCommand;
        private DelegateCommand _saveAsPanelCommand;
        private ICommand _importCommand;
        private DelegateCommand _exportCommand;

        private bool _abort;
        #endregion

        [ImportingConstructor]
        public MetaEditPanelViewModel(IMetaEditPanelView view, IMainService mainService, ConfigurationService configurationService,
            IMessageService messageService, IFileDialogService fileDialogService, EditPanelViewModel panelEditorViewModel,
            ExportFactory<AddItemViewModel> addPanelViewModelFactory, ExportFactory<TextInputViewModel> textInputViewModel) : base(view)
        {
            this._mainService = mainService;
            this._messageService = messageService;
            this._fileDialogservice = fileDialogService;
            this._configurationService = configurationService;

            this.EditVM = panelEditorViewModel;
            this._addItemViewModelFactory = addPanelViewModelFactory;
            this._textInputViewModel = textInputViewModel;

            this._knownPanelItemModels = new ObservableCollection<PanelItemModel>();

            CreateNewPanelCommand = new DelegateCommand(TryCreateNewPanelCommandHelper);
            OpenPanelCommand = new DelegateCommand<PanelItemModel>(x => OpenPanelCommandHelper(x));
            DeleteCurrentPanelCommand = new DelegateCommand(DeleteCurrentPanelCommandHandler, () => EditVM.Data != null);
            SavePanelCommand = new DelegateCommand(SavePanelCommandHelper, () => EditVM.Data != null);
            SaveAsPanelCommand = new DelegateCommand(SaveAsPanelCommandHelper, () => EditVM.Data !=null);
            ImportCommand = new DelegateCommand(ImportCommandHelper);
            ExportCommand = new DelegateCommand(ExportCommandHelper, () => EditVM.Data != null);

            this._abort = false;
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            PropertyChangedEventManager.AddHandler(this._mainService, IsAuthenticatedChangedHandler, nameof(IMainService.IsAuthenticated));
        }

        /// <summary>
        /// Initialize the panel editor with the given parent view.
        /// </summary>
        /// <param name="parentView"></param>
        /// <returns></returns>
        public bool Initialize(object parentView)
        {
            this.ParentView = parentView;
            this.EditVM.ParentView = parentView;
            var copyResult = _configurationService.GetPanelsCopy();
            if (!copyResult.Item1)
                return false;
            this._knownPanelItemModels = new ObservableCollection<PanelItemModel>(copyResult.Item2);
            InitializeKnownPanelItemModelsView();
            return true;
        }

        private void InitializeKnownPanelItemModelsView()
        {
            var collectionView = CollectionViewSource.GetDefaultView(_knownPanelItemModels);
            collectionView.SortDescriptions.Add(new SortDescription(nameof(PanelItemModel.Name), ListSortDirection.Ascending));
            SortedPanelItemModelsView = collectionView;
        }

        #region properties
        public object ParentView { get; private set; }

        public ICollectionView SortedPanelItemModelsView
        {
            get; private set;
        }

        public EditPanelViewModel EditVM
        {
            get; private set;
        }
        #endregion

        #region commands
        public ICommand CreateNewPanelCommand
        {
            get { return _createNewPanelCommand; }
            private set
            {
                SetProperty(ref _createNewPanelCommand, value);
            }
        }

        public DelegateCommand<PanelItemModel> OpenPanelCommand
        {
            get { return _openPanelCommand; }
            private set
            {
                SetProperty(ref _openPanelCommand, value);
            }
        }

        public DelegateCommand DeleteCurrentPanelCommand
        {
            get { return _deleteCurrentPanelCommand; }
            private set
            {
                SetProperty(ref _deleteCurrentPanelCommand, value);
            }
        }

        public DelegateCommand SavePanelCommand
        {
            get { return _savePanelCommand; }
            private set
            {
                SetProperty(ref _savePanelCommand, value);
            }
        }

        public DelegateCommand SaveAsPanelCommand
        {
            get { return _saveAsPanelCommand; }
            private set
            {
                SetProperty(ref _saveAsPanelCommand, value);
            }
        }

        public ICommand ImportCommand
        {
            get { return _importCommand; }
            private set
            {
                SetProperty(ref _importCommand, value);
            }
        }

        public DelegateCommand ExportCommand
        {
            get { return _exportCommand; }
            private set
            {
                SetProperty(ref _exportCommand, value);
            }
        }

        public DelegateCommand CloseForParentCommand { get; internal set; }
        #endregion

        #region helpers
        public void Close()
        {
            if (EditVM.Data?.GetIsDirty() ?? false && !this._abort)
            {
                bool answer = _messageService.ShowYesNoQuestion(ParentView,
                    (string)LanguageManager.Current.CurrentLanguageData.Translate("QPanelUnsavedChanges"));
                if (answer)
                {
                    EditVM.SavePercentages();
                    answer = _configurationService.TrySavePanel(EditVM.Data);
                    if (!answer)
                    {
                        _messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("ESavePanel"));
                    }
                }
            }
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            PropertyChangedEventManager.RemoveHandler(this._mainService, IsAuthenticatedChangedHandler, nameof(IMainService.IsAuthenticated));
        }

        private void TryCreateNewPanelCommandHelper()
        {
            ProcessPanelIsDirtyAndReset();
            var vm = _addItemViewModelFactory.CreateExport().Value;
            vm.Initialize(TryCreateNewPanel);
            vm.ShowDialog(ParentView);
        }

        private void OpenPanelCommandHelper(PanelItemModel toOpen)
        {
            if (_knownPanelItemModels.Contains(toOpen))
            {
                ProcessPanelIsDirtyAndReset();
                EditVM.Initialize(toOpen);
                SavePanelCommand.RaiseCanExecuteChanged();
                SaveAsPanelCommand.RaiseCanExecuteChanged();
                ExportCommand.RaiseCanExecuteChanged();
                DeleteCurrentPanelCommand.RaiseCanExecuteChanged();
            }
        }

        private void DeleteCurrentPanelCommandHandler()
        {
            var panel = EditVM.Data;
            if (!_knownPanelItemModels.Contains(panel))
                return;
            if (_configurationService.PanelInUse(panel)){
                this._messageService.ShowError((string) LanguageManager.Current.CurrentLanguageData.Translate("EPanelIsInUse"));
                return;
            }
            if (!this._messageService.ShowYesNoQuestion(ParentView,
                String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("QPanelDelete"), panel.Name)))
            {
                return;
            }
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            bool deleted = _configurationService.TryDeletePanel(panel);
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            if (deleted)
            {
                this._knownPanelItemModels.Remove(EditVM.Data);
                EditVM.ResetData();
                DeleteCurrentPanelCommand.RaiseCanExecuteChanged();
            }
        }

        private bool ProcessPanelIsDirtyAndReset()
        {
            if (EditVM.Data?.GetIsDirty() ?? false)
            {
                //deny saving on import?
                bool answer = _messageService.ShowYesNoQuestion(ParentView,
                    (string)LanguageManager.Current.CurrentLanguageData.Translate("QPanelUnsavedChanges"));
                if (answer)
                {
                    EditVM.SavePercentages();
                    WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
                    answer = _configurationService.TrySavePanel(EditVM.Data);
                    WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
                    if (!answer)
                    {
                        _messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("ESavePanel"));
                        return false;
                    }
                }
                if (!answer)
                {
                    var retrieved = this._configurationService.GetSavedPanelCopy(EditVM.CurrentDataName);
                    int index = this._knownPanelItemModels.IndexOf(EditVM.Data);
                    //conditions should always be true, just to prevent some weird bug.
                    if (retrieved.Item1 && index >= 0)
                    {                        
                        this._knownPanelItemModels[index] = retrieved.Item2;
                    }
                }
                EditVM.ResetData();
            }
            return true;
        }
        private bool TryCreateNewPanel(string panelName)
        {
            if (!CheckNameValid(panelName))
                return false;
            EditVM.ResetData();
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            var creationResult = _configurationService.CreateNewAndSave(panelName);
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            if (!creationResult.Item1)
            {
                _messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("ECreatePanel"));
                return false;
            }
            _knownPanelItemModels.Add(creationResult.Item2);
            EditVM.Initialize(creationResult.Item2);
            return true;
        }

        private void SavePanelCommandHelper()
        {
            EditVM.SavePercentages();
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            this._configurationService.TrySavePanel(EditVM.Data);
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
        }

        private void SaveAsPanelCommandHelper()
        {
            var vm = _textInputViewModel.CreateExport().Value;
            string title = (string) LanguageManager.Current.CurrentLanguageData.Translate("SaveAs");
            string header = (string)LanguageManager.Current.CurrentLanguageData.Translate("NewNameColon");
            void handler(object s, EventArgs e) { vm.Close(); }
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), handler);
            vm.Initialize(title, header, TrySavePanelAs, CheckNameValid);
            vm.ShowDialog(ParentView);
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), handler);
        }

        private bool CheckNameValid(string proposedName)
        {
            if (String.IsNullOrWhiteSpace(proposedName))
                return false;
            if (proposedName.Length > 30)
                return false;
            string trimmed = proposedName.Trim();
            if (_knownPanelItemModels.Any(x => String.Equals(x.Name, trimmed, StringComparison.OrdinalIgnoreCase)))
            {
                return false;
            }
            return true;
        }

        private Tuple<bool, string> TrySavePanelAs(string name)
        {
            Debug.Assert(CheckNameValid(name));
            string trimmedName = name.Trim();
            if (_knownPanelItemModels.Any(x => String.Equals(x.Name, trimmedName, StringComparison.OrdinalIgnoreCase)))
            {
                return new Tuple<bool, string>(false, (string)LanguageManager.Current.CurrentLanguageData.Translate("PanelNameAlreadyPresent"));
            }
            EditVM.SavePercentages();
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            var creationResult = _configurationService.CreateNewAndSave(trimmedName, EditVM.Data);
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            if (!creationResult.Item1)
                return new Tuple<bool, string>(false, (string)LanguageManager.Current.CurrentLanguageData.Translate("ECreatePanel"));
            var newPanelItemModel = creationResult.Item2;
            _knownPanelItemModels.Remove(EditVM.Data);
            var copyResult = _configurationService.GetSavedPanelCopy(EditVM.Data);
            if (!copyResult.Item1)
            {
                return new Tuple<bool, string>(false, (string)LanguageManager.Current.CurrentLanguageData.Translate("ELoadPanel"));
            }
            _knownPanelItemModels.Add(copyResult.Item2);
            _knownPanelItemModels.Add(newPanelItemModel);
            EditVM.Initialize(newPanelItemModel);
            return new Tuple<bool, string>(true, "");
        }

        private void ExportCommandHelper()
        {
            Debug.Assert(EditVM.Data != null);
            if (EditVM.Data.GetIsDirty())
            {
                bool answer = _messageService.ShowYesNoQuestion(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("QExportPanelUnsavedChanges"));
                if (answer)
                {
                    EditVM.SavePercentages();
                    answer = _configurationService.TrySavePanel(EditVM.Data);
                    if (!answer)
                    {
                        _messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("ESavePanel"));
                        return;
                    }    
                }
            }
            FileType ft = new FileType("xml", ".xml");
            var result = _fileDialogservice.ShowSaveFileDialog(ParentView, ft, String.Empty);
            if (result.IsValid)
            {
                var copyResult = _configurationService.GetSavedPanelCopy(EditVM.Data);
                if (!copyResult.Item1)
                {
                    _messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("ELoadPanel"));
                    return;
                }
                try
                {
                    SerializeHelper.SerializeToFile(result.FileName, copyResult.Item2);
                }
                catch (Exception)
                {
                    _messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("ESaveToFile"));
                }   
            }
        }

        private void ImportCommandHelper()
        {
            ProcessPanelIsDirtyAndReset();
            FileType ft = new FileType("xml", ".xml");
            //TODO cancel import of panel in follower mode? need to make ShowOpenFileDialog cancellable or abort afterward
            var result = _fileDialogservice.ShowOpenFileDialog(ParentView, ft);
            if (!result.IsValid)
                return;
            PanelItemModel extracted;
            try
            {
                extracted = SerializeHelper.DeserializeFromFile<PanelItemModel>(result.FileName);
            }
            catch
            {
                _messageService.ShowError(ParentView, String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("EProcessFile"), result.FileName));
                return;
            }
            string name = extracted.Name.Trim();
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            var saveResult = _configurationService.CreateNewAndSave(name, extracted);
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            while (!saveResult.Item1)
            {
                _messageService.ShowMessage(ParentView, 
                    String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("ImportPanelNameNotValid"), name));
                var vm = _textInputViewModel.CreateExport().Value;
                string title = (string)LanguageManager.Current.CurrentLanguageData.Translate("SaveAs");
                string header = (string)LanguageManager.Current.CurrentLanguageData.Translate("NewNameColon");
                vm.Initialize(title, header, (x) => new Tuple<bool, string>(true,""), CheckNameValid);
                vm.ShowDialog(ParentView);
                if (!vm.DialogResult)
                    return;
                name = vm.Input.Trim();
                WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
                saveResult = _configurationService.CreateNewAndSave(name, extracted);
                WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
            }
            _knownPanelItemModels.Add(saveResult.Item2);
            EditVM.Initialize(saveResult.Item2);
        }
        #endregion

        #region event handlers
        private void PanelsChangedHandler(object sender, EventArgs e)
        {
            this._abort = true;
            this.CloseForParentCommand?.Execute(null);
        }

        private void IsAuthenticatedChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            this._abort = true;
            this.CloseForParentCommand?.Execute(null);
        }
        #endregion
    }
}
