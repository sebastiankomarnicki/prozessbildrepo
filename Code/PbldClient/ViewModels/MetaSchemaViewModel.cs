﻿using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Schema;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.Pbld.ViewModels.SchemaViewModelHelpers;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class MetaSchemaViewModel : ViewModel<IMetaSchemaView>
    {
        #region fields
        private readonly IMessageService _messageService;
        private readonly ConfigurationService _configurationService;
        private readonly ExportFactory<SchemaViewModel> _schemaVMFactory;

        private object _parentView;
        private DelegateCommand<SpcPath> _showListViewCommand;
        private ICommand _showEditViewCommand;
        private object _currentView;
        private Area _currentArea;
        private IMainService _mainService;
        #endregion

        [ImportingConstructor]
        public MetaSchemaViewModel(IMetaSchemaView view, IMessageService messageService, ConfigurationService configurationService, IMainService mainService,
            ExportFactory<SchemaViewModel> schemaVMFactory) : base(view)
        {
            this._messageService = messageService;
            this._configurationService = configurationService;
            this._mainService = mainService;
            this._schemaVMFactory = schemaVMFactory;
        }

        public void Initialize(object parentView, DelegateCommand<SpcPath> showListViewCommand, ICommand showEditViewCommand)
        {
            this._parentView = parentView;
            this._showListViewCommand = showListViewCommand;
            this._showEditViewCommand = showEditViewCommand;
            this._currentArea = this._configurationService.GetCurrentAreaCopyOrNull();
            PropertyChangedEventManager.AddHandler(this._configurationService, HandleAreaChanged, nameof(ConfigurationService.CurrentAreaName));
            PropertyChangedEventManager.AddHandler(this._mainService, HandleIsAuthenticatedChanged, nameof(IMainService.IsAuthenticated));
            if (this._currentArea is object)
                ShowSchemaView();
            else
                CurrentView = null;
        }

        public object CurrentView
        {
            get { return _currentView; }
            private set { SetProperty(ref _currentView, value); }
        }

        public double ViewHeight
        {
            get;set;
        }

        public double ViewWidth
        {
            get; set;
        }

        private void ShowSchemaView()
        {
            var vm = this._schemaVMFactory.CreateExport().Value;
            vm.Initialize(this._showEditViewCommand, this._currentArea, this._showListViewCommand);
            CurrentView = vm.View;
        }
        #region event handlers
        private void HandleIsAuthenticatedChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this._currentArea != null)
                ShowSchemaView();
            else
                CurrentView = null;
        }

        private void HandleAreaChanged(object sender, PropertyChangedEventArgs e)
        {
            this._currentArea = this._configurationService.GetCurrentAreaCopyOrNull();
            if (this._currentArea != null)
                ShowSchemaView();
            else
                CurrentView = null;
        }
        #endregion
    }
}
