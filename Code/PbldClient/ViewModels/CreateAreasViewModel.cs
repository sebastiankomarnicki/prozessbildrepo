﻿using Promess.Pbld.Data;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class CreateAreasViewModel : ViewModel<ICreateAreasView>
    {
        private readonly ConfigurationService _configurationService;

        private bool _dialogResult;
        private Areas _areas;

        private AreaSelectionViewModel _areasSelectionVM;
        private AreaSpcPathsViewModel _areaSpcPathsVM;

        private ICommand _okCommand;

        [ImportingConstructor]
        public CreateAreasViewModel(ICreateAreasView view, ConfigurationService configurationService,
            AreaSelectionViewModel areasSelectionVM, AreaSpcPathsViewModel areaSpcPathsVM
            ) : base(view)
        {
            this._dialogResult = false;
            this._configurationService = configurationService;
            this._areasSelectionVM = areasSelectionVM;
            this._areaSpcPathsVM = areaSpcPathsVM;
            this._okCommand = new DelegateCommand(OkCommandHandler);
        }

        public void Setup()
        {
            this._areas = this._configurationService.GetAreasCopy();
            this._areasSelectionVM.Initialize(this.View, this._areas);
            this._areaSpcPathsVM.Initialize(this.View, this._areasSelectionVM.SelectedArea);
            PropertyChangedEventManager.AddHandler(this._areasSelectionVM, SelectedAreaChangedHandler, nameof(AreaSelectionViewModel.SelectedArea));
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.AreasChanged), AreasChangedHandler);
            this.AreasView = this._areasSelectionVM.View;
            this.AreaSpcPathView = this._areaSpcPathsVM.View;
        }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        public Areas Areas { get { return this._areas; } }

        public object AreasView { get; private set; }
        public object AreaSpcPathView { get; private set; }

        public ICommand OkCommand { get { return _okCommand; } }

        private void OkCommandHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }

        #region event handlers
        private void SelectedAreaChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            this._areaSpcPathsVM.SetArea(this._areasSelectionVM.SelectedArea);
        }

        private void AreasChangedHandler(object sender, EventArgs e)
        {
            this._areas = this._configurationService.GetAreasCopy();
            this._areasSelectionVM.ChangeAreas(Areas);
            this._areaSpcPathsVM.SetArea(Areas.SelectedArea);
        }
        #endregion
    }
}
