﻿using Promess.Language;
using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class SelectFromDictionaryWithFilterViewModel : ViewModel<ISelectFromDictionaryWithFilter>
    {
        #region fields
        private bool _dialogResult;
        private string _header;
        private IReadOnlyDictionary<Predicate<object>, string> _filters;
        private Predicate<object> _currentFilter;
        private int? _selectedKNumber;
        private DelegateCommand _saveCommand;
        private ICollectionView _selectedFilterView;
        #endregion

        [ImportingConstructor]
        public SelectFromDictionaryWithFilterViewModel(ISelectFromDictionaryWithFilter view) : base(view)
        {
            _dialogResult = false;
            _saveCommand = new DelegateCommand(OkHandler);
        }

        public void Initialize(IReadOnlyDictionary<int, string> kNumberTextDictionary, IReadOnlyDictionary<Predicate<object>,string> filter, string header)
        {
            this._header = header;
            var collectionView = CollectionViewSource.GetDefaultView(kNumberTextDictionary);
            collectionView.SortDescriptions.Add(new SortDescription(nameof(KeyValuePair<int,string>.Key), ListSortDirection.Ascending));
            _selectedFilterView = collectionView;
            this._filters = filter;
            CurrentFilter = this._filters.Count >= 1?this._filters.First().Key:null;
            _selectedFilterView.Filter = CurrentFilter;
            PropertyChangedEventManager.AddHandler(this, HandleFilterChanged, nameof(CurrentFilter));
        }

        #region properties
        public string Header
        {
            get { return _header; }
        }

        public ICommand SaveCommand
        {
            get { return _saveCommand; }
        }

        public IReadOnlyDictionary<Predicate<object>, string> Filters
        {
            get { return _filters; }
        } 

        public Predicate<object> CurrentFilter
        {
            get { return _currentFilter; }
            set
            {
                SetProperty(ref _currentFilter, value);
            }
        }


        public ICollectionView FilteredKNumbersView
        {
            get { return _selectedFilterView; }
        }

        public int? SelectedKNumber
        {
            get { return _selectedKNumber; }
            set
            {
                SetProperty(ref _selectedKNumber, value);
            }
        }
        #endregion

        #region helper functions
        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        private void SetupFilters()
        {
            var filters = new Dictionary<Predicate<object>, string>();
            filters.Add(new Predicate<object>(x => true), (string)LanguageManager.Current.CurrentLanguageData.Translate("All"));
            filters.Add(new Predicate<object>(x => ((KeyValuePair<int,string>) x).Key >= 1000 && ((KeyValuePair<int, string>)x).Key<=1999), (string)LanguageManager.Current.CurrentLanguageData.Translate("PartData"));
            filters.Add(new Predicate<object>(x => ((KeyValuePair<int, string>)x).Key >= 2000 && ((KeyValuePair<int, string>)x).Key <= 2999), (string)LanguageManager.Current.CurrentLanguageData.Translate("CharData"));
            _filters = filters;
            _currentFilter = _filters.First().Key;
        }

        private void HandleFilterChanged(object sender, PropertyChangedEventArgs e)
        {
            if (CurrentFilter != null)
            {
                _selectedFilterView.Filter = CurrentFilter;
            }
        }
        #endregion

        #region command helpers
        private void OkHandler()
        {
            _dialogResult = _selectedKNumber.HasValue;
            ViewCore.Close();
        }
        #endregion
    }
}
