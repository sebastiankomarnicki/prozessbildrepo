﻿using Promess.Common.Util;
using Promess.Pbld.IViews;
using Promess.QsStat.Data;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class ConfigQsStatisticViewModel : ViewModel<IConfigQsStatisticView>
    {
        #region fields
        private bool _dialogResult;

        private readonly QsStatService _qsStatService;

        private int? _selectedPrinter;
        private int? _selectedStrategy;
        private QsReportEntry _selectedReport;
        private int? _selectedLanguage;
        #endregion

        [ImportingConstructor]
        public ConfigQsStatisticViewModel(IConfigQsStatisticView view, QsStatService qsStatService) : base(view)
        {
            this._dialogResult = false;
            this._qsStatService = qsStatService;
            this.OkCommand = new DelegateCommand(OkCommandHandler);
            this.TogglePrinterCommand = new DelegateCommand<int>(id => TogglePrinterCommandHandler(id));
            this.SelectStrategyCommand = new DelegateCommand<int>(id => SelectStrategyCommandHandler(id));
            this.SelectReportCommand = new DelegateCommand<QsReportEntry>(report => SelectReportCommandHandler(report));
            this.SelectLanguageCommand = new DelegateCommand<int>(id => SelectLanguageCommandHandler(id));
        }

        /// <summary>
        /// At least one of the Dictionaries/List must be non null, only non null is supposed to be shown in the view
        /// </summary>
        /// <param name="strategies">IReadOnlyDictionary of strategies</param>
        /// <param name="reportsEnabled">IReadOnlyList of Reports</param>
        /// <param name="printersEnabled">IReadOnly dictionary of available printers</param>
        public void Initialize(IReadOnlyDictionary<int, string> strategies, int? selectedStrategy, IReadOnlyList<QsReportEntry> reports, string selectedReport, IReadOnlyDictionary<int, string> printers, int? selectedPrinter, IReadOnlyDictionary<int, QsLanguageEntry> languages, int? selectedLanguage)
        {  
            this.PrintersEnabled = printers!=null;
            if (strategies != null)
            {
                this.EvalStrategiesEnabled = true;
                var strategiesView = (CollectionView)CollectionViewSource.GetDefaultView(strategies);
                var strategiesGroupDescription = new PropertyGroupDescription("Key");
                strategiesView.GroupDescriptions.Add(strategiesGroupDescription);
                this.StrategiesView = strategiesView;
                this.SelectedEvalStrategy = selectedStrategy;
            }
            if (reports != null)
            {
                this.ReportsEnabled = true;
                var reportsView = (CollectionView)CollectionViewSource.GetDefaultView(reports);
                var reportsGroupDescription = new PropertyGroupDescription(nameof(QsReportEntry.Id));
                reportsView.GroupDescriptions.Add(reportsGroupDescription);
                this.ReportsView = reportsView;
                if (!String.IsNullOrWhiteSpace(selectedReport))
                {
                    var maybeFound = reports.FirstOrDefault(report => String.Equals(report.FileName, selectedReport, StringComparison.OrdinalIgnoreCase));
                    if (maybeFound!=null)
                        this.SelectedReport = maybeFound;
                }
            }
            if (printers != null)
            {
                var printersView = (CollectionView)CollectionViewSource.GetDefaultView(printers);
                var printersGroupDescription = new PropertyGroupDescription("Key");
                printersView.GroupDescriptions.Add(printersGroupDescription);
                this.PrintersView = printersView;

                this.SelectedPrinter = selectedPrinter;
            }
            if (languages != null)
            {
                this.LanguagesEnabled = true;
                var languagesView = (CollectionView)CollectionViewSource.GetDefaultView(languages);
                var languagesGroupDescription = new PropertyGroupDescription("Key");
                languagesView.GroupDescriptions.Add(languagesGroupDescription);
                this.LanguagesView = languagesView;
                this.SelectedLanguage = selectedLanguage;
            }
        }

        public bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            return _dialogResult;
        }

        internal void Close()
        {
            _dialogResult = false;
            ViewCore.Close();
        }

        #region properties
        public CollectionView StrategiesView
        {
            get;private set;
        }

        public bool EvalStrategiesEnabled { get; private set; }

        public int? SelectedEvalStrategy {
            get { return _selectedStrategy; }
            set { SetProperty(ref _selectedStrategy, value); }
        }

        public ICommand SelectStrategyCommand { get; private set; }

        public CollectionView ReportsView
        {
            get; private set;
        }

        public bool ReportsEnabled { get; private set; }

        public QsReportEntry SelectedReport {
            get { return _selectedReport; }
            set { SetProperty(ref _selectedReport, value); }
        }

        public ICommand SelectReportCommand { get; private set; }

        public CollectionView PrintersView
        {
            get; private set;
        }

        public bool PrintersEnabled { get; private set; }

        public int? SelectedPrinter {
            get { return _selectedPrinter; }
            set { SetProperty(ref _selectedPrinter, value); }
        }

        public ICommand TogglePrinterCommand { get; private set; }

        public CollectionView LanguagesView
        {
            get; private set;
        }

        public bool LanguagesEnabled { get; private set; }

        public int? SelectedLanguage
        {
            get { return _selectedLanguage; }
            set { SetProperty(ref _selectedLanguage, value); }
        }

        public ICommand SelectLanguageCommand { get; private set; }

        public ICommand OkCommand { get;private set; }
        #endregion

        #region helpers
        private void OkCommandHandler()
        {
            _dialogResult = true;
            ViewCore.Close();
        }

        private void TogglePrinterCommandHandler(int id)
        {
            if (this.SelectedPrinter == id)
                this.SelectedPrinter = null;
            else
                this.SelectedPrinter = id;
        }

        private void SelectStrategyCommandHandler(int id)
        {
            this.SelectedEvalStrategy = id;
        }

        private void SelectReportCommandHandler(QsReportEntry report)
        {
            this.SelectedReport = report;
        }

        private void SelectLanguageCommandHandler(int id)
        {
            this.SelectedLanguage = id;
        }
        #endregion
    }
}
