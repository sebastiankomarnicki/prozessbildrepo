﻿using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Reflection;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class AboutViewModel:ViewModel<IAboutView>
    {
        [ImportingConstructor]
        public AboutViewModel(IAboutView view) : base(view)
        {
        }

        public void Setup(string qCOMVersion)
        {
            this.Version = GetVersionAsText();
            this.QCOMVersion = qCOMVersion;
            OkCommand = new DelegateCommand(() => ViewCore.Close());
        }

        public ICommand OkCommand { get; private set; }

        public string Version
        {
            get; private set;
        }

        public string QCOMVersion
        {
            get; private set;
        }

        private string GetVersionAsText()
        {
            var version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
            return string.Format($"{version.FileMajorPart}.{version.FileMinorPart}.{version.FileBuildPart}");
        }

        public void ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
        }
    }
}
