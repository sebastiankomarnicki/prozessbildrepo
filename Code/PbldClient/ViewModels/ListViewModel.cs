﻿using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.QsStat.Data;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Collections.Specialized;
using System.Data;
using Promess.Common.Util;
using System.Windows.Input;
using Promess.Language;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.ViewModels.ListViewModelHelpers;
using System.Windows;
using System.Waf.Applications.Services;
using Promess.Pbld.Services.CacheService;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class ListViewModel : ViewModel<IListView>
    {
        private readonly IMessageService _messageService;
        private readonly QsStatService _qsStatService;
        private readonly SelectedDataCacheService _selectedDataCacheService;
        private readonly SpcPathOverviewViewModel _spcPathOverviewVM;
        private readonly PartOverviewViewModel _partOverviewVM;
        private readonly MetaQsStatRequestViewModel _metaQsStatRequestVM;
        private readonly ExportFactory<LabelSelectionViewModel> _labelSelectionVMFactory;
        private readonly ExportFactory<EditQdasNumberWrapperColumnsViewModel> _editQdasNumberWrapperColumnsVMFactory;

        private IReadOnlyCollection<SpcPathNodeData> _spcPathData;
        private ListViewSettings _listViewSettings;

        private ProcessState _currentAreaState;

        private readonly DelegateCommand<object> _treeSelectedItemChangedCommand;
        private readonly DelegateCommand _editPartGridColumnsCommand;
        private readonly DelegateCommand _editCharacteristicGridColumnsCommand;
        private readonly DelegateCommand _editPartLabelsCommand;
        private readonly DelegateCommand _editCharacteristicLabelsCommand;
        private readonly DelegateCommand<SpcPath> _refreshSpcPathCommand;

        private object _currentView;
        private object _selectedItem;
        private bool _isExpanded;
        private bool _isSelected;

        [ImportingConstructor]
        public ListViewModel(IListView view, IMessageService messageService, ConfigurationService configurationService, QsStatService qsStatService, SelectedDataCacheService selectedDataCacheService,
            SpcPathOverviewViewModel spcPathOverviewVM, PartOverviewViewModel partOverviewVM, MetaQsStatRequestViewModel metaQsStatRequestVM,
            ExportFactory<LabelSelectionViewModel> labelSelectionVMFactory, ExportFactory<EditQdasNumberWrapperColumnsViewModel> editQdasNumberWrapperColumnsVMFactory) : base(view)
        {
            this._messageService = messageService;
            this._qsStatService = qsStatService;
            this.ConfigurationService = configurationService;
            this._selectedDataCacheService = selectedDataCacheService;
            this._spcPathOverviewVM = spcPathOverviewVM;
            this._partOverviewVM = partOverviewVM;
            this._metaQsStatRequestVM = metaQsStatRequestVM;
            
            this._labelSelectionVMFactory = labelSelectionVMFactory;
            this._editQdasNumberWrapperColumnsVMFactory = editQdasNumberWrapperColumnsVMFactory;

            this._treeSelectedItemChangedCommand = new DelegateCommand<object>(TreeSelectedItemChangedCommandHandler);
            this._editPartLabelsCommand = new DelegateCommand(EditPartLabelsCommandHandler);
            this._editCharacteristicLabelsCommand = new DelegateCommand(EditCharacteristicLabelsCommandHandler);
            this._refreshSpcPathCommand = new DelegateCommand<SpcPath>(spcPath => RefreshSpcPathCommandHandler(spcPath));
            this._editPartGridColumnsCommand = new DelegateCommand(EditPartGridColumnsCommandHandler);
            this._editCharacteristicGridColumnsCommand = new DelegateCommand(EditCharacteristicGridColumnsCommandHandler);
        }

        public void Initialize(object parentView)
        {
            this.ParentView = parentView;
            this._metaQsStatRequestVM.Setup(parentView);
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), ListViewSettingsChangedHandler);
            this._listViewSettings = ConfigurationService.GetListViewSettingsCopy();
            PropertyChangedEventManager.AddHandler(this._qsStatService, SpcPathResultsChangedHandler, nameof(QsStatService.SpcPathResults));
            PropertyChangedEventManager.AddHandler(this._qsStatService, QsStatServicePartKFieldsChangedHandler, nameof(QsStatService.PartKFields));
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshDataHandler);
            GenerateDataList();
            CurrentAreaState = MeasurementHelper.EvaluateState(SpcPathNodeData.Select(node => node.ProcessState));
            this._spcPathOverviewVM.Initialize(parentView, this._listViewSettings.PartGridQdasNumbers);
            this._partOverviewVM.Initialize(parentView, this._listViewSettings.CharacteristicGridQdasNumbers);

            PreselectNode();
        }

        private void PreselectNode()
        {
            var selection = this._selectedDataCacheService.ListViewDataSelection;
            SpcPathNodeData spcNode= GetPreselectionSpcNode(SpcPathNodeData, selection);
            if (spcNode == null)
            {
                this.IsSelected = true;
                SelectedItem = null;
                ShowAllSpc();
                return;
            }
            
            PartNodeData partNode = GetPreselectionSpcNode(spcNode, selection);
            if (partNode == null)
            {
                spcNode.IsSelected = true;
                SelectedItem = spcNode;
                ShowSpc(spcNode);
                return;
            }
            spcNode.IsExpanded = true;          
            CharacteristicNodeData charaNode = GetPreselectionSpcNode(partNode, selection);
            if (charaNode == null)
            {
                partNode.IsSelected = true;
                SelectedItem = partNode;
                ShowPart(partNode, spcNode.SpcPath);
                return;
            }
            partNode.IsExpanded = true;
            charaNode.IsSelected = true;
            SelectedItem = charaNode;
            CharacteristicPreselection(selection);
            ShowCharacteristic(charaNode, spcNode.SpcPath, partNode.PartReferenceGuid);
        }

        private void CharacteristicPreselection(ListViewDataSelection selection)
        {
            if (selection.SelectedPage.HasValue)
                this._metaQsStatRequestVM.SelectedPage = selection.SelectedPage.Value;
            if (!String.IsNullOrWhiteSpace(selection.SelectedReportDefFile))
                this._metaQsStatRequestVM.SelectedReportDefFile = selection.SelectedReportDefFile;
        }

        private CharacteristicNodeData GetPreselectionSpcNode(PartNodeData partNode, ListViewDataSelection selection)
        {
            if (!selection.SelectedCharacteristicIndex.HasValue)
                return null;
            return partNode.CharacteristicNodes.FirstOrDefault(item => item.CharacteristicIndex == selection.SelectedCharacteristicIndex.Value);
        }

        private static SpcPathNodeData GetPreselectionSpcNode(IReadOnlyCollection<SpcPathNodeData> spcPathNodeData, ListViewDataSelection selection)
        {
            if (!selection.SelectedSpcPathGuid.HasValue)
                return null;
            return spcPathNodeData.FirstOrDefault(node => node.SpcPath.Guid == selection.SelectedSpcPathGuid.Value);
        }

        private PartNodeData GetPreselectionSpcNode(SpcPathNodeData spcNode, ListViewDataSelection selection)
        {
            if (!selection.SelectedDataSetGuid.HasValue)
                return null;
            return spcNode.PartNodeData.FirstOrDefault(item => item.PartReferenceGuid == selection.SelectedDataSetGuid.Value);
        }

        public object ParentView { get; private set; }
        public ConfigurationService ConfigurationService { get; private set; }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { SetProperty(ref this._isExpanded, value); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref this._isSelected, value); }
        }

        public IReadOnlyCollection<SpcPathNodeData> SpcPathNodeData
        {
            get { return _spcPathData; }
            private set { SetProperty(ref _spcPathData, value); }
        }

        public ProcessState CurrentAreaState
        {
            get { return _currentAreaState; }
            private set { SetProperty(ref _currentAreaState, value); }
        }

        public object CurrentView
        {
            get { return _currentView; }
            private set { SetProperty(ref _currentView, value); }
        }

        public object SelectedItem
        {
            get { return _selectedItem; }
            set { SetProperty(ref _selectedItem, value); }
        }

        public string PartSeparator
        {
            get { return _listViewSettings.PartLabelSeparator; }
        }

        public IReadOnlyDictionary<int, string> PartKFields
        {
            get { return _qsStatService.PartKFields; }
        }

        public string CharacteristicSeparator
        {
            get { return _listViewSettings.CharacteristicLabelSeparator; }
        }

        public ICommand EditPartLabelsCommand
        {
            get { return _editPartLabelsCommand; }
        }

        public ICommand EditCharacteristicLabelsCommand
        {
            get { return _editCharacteristicLabelsCommand; }
        }

        public DelegateCommand<SpcPath> RefreshSpcPathCommand
        {
            get { return _refreshSpcPathCommand; }
        }

        public DelegateCommand<object> TreeSelectedItemChangedCommand
        {
            get { return _treeSelectedItemChangedCommand; }
        }

        public ICommand EditPartGridColumnsCommand
        {
            get { return _editPartGridColumnsCommand; }
        }

        public ICommand EditCharacteristicGridColumnsCommand
        {
            get { return _editCharacteristicGridColumnsCommand; }
        }

        #region event handlers
        private void ListViewSettingsChangedHandler(object sender, EventArgs e)
        {
            this._listViewSettings = this.ConfigurationService.GetListViewSettingsCopy();
            this._spcPathOverviewVM.UpdatePartColumnConfig(this._listViewSettings.PartGridQdasNumbers);
            this._partOverviewVM.UpdateCharacteristicColumnConfig(this._listViewSettings.CharacteristicGridQdasNumbers);
            UpdatePartNodeLabels();
            UpdateCharacteristicNodeLabels();
            RaisePropertyChanged(nameof(PartSeparator));
            RaisePropertyChanged(nameof(CharacteristicSeparator));         
        }

        private void QsStatServicePartKFieldsChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            RaisePropertyChanged(nameof(PartKFields));
        }

        private void SpcPathResultsChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            DispatchHelper.BeginInvoke(SpcPathResultsChangedHandlerImpl);
        }

        private void SpcPathResultsChangedHandlerImpl()
        {
            GenerateDataList();
            CollectionChangedEventManager.AddHandler(this._qsStatService.SpcPathResults, RefreshDataHandler);
        }
        #endregion

        #region command handlers
        private void TreeSelectedItemChangedCommandHandler(object theItem)
        {
            if (theItem == null)
            {
                this.CurrentView = null;
                return;
            }
            switch (theItem)
            {
                case SpcPathNodeData spcNode:
                    ShowSpc(spcNode);
                    this._selectedDataCacheService.ListViewDataSelection.Select(spcNode.SpcPath);
                    break;
                case PartNodeData partNode:
                    SpcPath spcPath = FindBelongingSpcPath(partNode);
                    if (spcPath == null)
                    {
                        //should not happen
                        this.CurrentView = null;
                        return;
                    }
                    ShowPart(partNode, spcPath);
                    this._selectedDataCacheService.ListViewDataSelection.Select(spcPath, partNode.PartReferenceGuid);
                    break;
                case CharacteristicNodeData charaNode:
                    Tuple<bool, SpcPath, Guid> charaNodeSearchResult = FindBelongingSpcPathAndFileGuid(charaNode);
                    if (!charaNodeSearchResult.Item1)
                    {
                        //TODO do something here, not yet sure what, but just returning is wrong because of the old result
                        this.CurrentView = null;
                        return;
                    }
                    ShowCharacteristic(charaNode, charaNodeSearchResult.Item2, charaNodeSearchResult.Item3);
                    this._selectedDataCacheService.ListViewDataSelection.Select(charaNodeSearchResult.Item2, charaNodeSearchResult.Item3, charaNode.CharacteristicIndex, this._metaQsStatRequestVM.SelectedReportDefFile, this._metaQsStatRequestVM.SelectedPage);
                    break;
                default:
                    ShowAllSpc();
                    this._selectedDataCacheService.ListViewDataSelection.ResetSelection();
                    break;
            }
        }

        private void ShowSpc(SpcPathNodeData spcNode)
        {
            this._spcPathOverviewVM.ShowSpc(spcNode.SpcPath);
            this.CurrentView = this._spcPathOverviewVM.View;
        }

        private void ShowPart(PartNodeData partNode, SpcPath spcPath)
        {
            this._partOverviewVM.SetToDisplayPart(spcPath, partNode.PartReferenceGuid);
            this.CurrentView = this._partOverviewVM.View;
        }

        private void ShowCharacteristic(CharacteristicNodeData charaNode, SpcPath spcPath, Guid dataSetGuid)
        {
            this._metaQsStatRequestVM.SetGraphicEvaluationData(spcPath, dataSetGuid, charaNode.CharacteristicIndex + 1);
            this._metaQsStatRequestVM.SetValidReportDefFile();
            //use characteristic number as page number on selection
            this._metaQsStatRequestVM.RequestPage(charaNode.CharacteristicIndex + 1);
            this.CurrentView = this._metaQsStatRequestVM.View;
        }

        private void ShowAllSpc()
        {
            this._spcPathOverviewVM.ShowAllSpc();
            this.CurrentView = this._spcPathOverviewVM.View;
        }

        private void EditPartLabelsCommandHandler()
        {
            var vm = _labelSelectionVMFactory.CreateExport().Value;
            string heading = (string)LanguageManager.Current.CurrentLanguageData.Translate("CompositionPartLabel");
            vm.Initialize(heading, new List<QdasNumber>(this._listViewSettings.PartLabelQdasNumbers), TextEvaluationType.Part, this._listViewSettings.PartLabelSeparator);
            void handler(object s, EventArgs e) { vm.Cancel(); }
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), handler);
            if (vm.ShowDialog(ParentView))
            {
                this._listViewSettings.PartLabelQdasNumbers = vm.SelectedLabels.Select(tupel => tupel.Item1).ToList();
                this._listViewSettings.PartLabelSeparator = vm.SelectedSeparator;
                if (!ConfigurationService.TrySetListViewSettings(this._listViewSettings))
                {
                    this._messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("EListViewSettingSaveFailed"));
                    this._listViewSettings = ConfigurationService.GetListViewSettingsCopy();
                }
            }
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), handler);
        }
            

        private void EditCharacteristicLabelsCommandHandler()
        {
            var vm = _labelSelectionVMFactory.CreateExport().Value;
            string heading = (string)LanguageManager.Current.CurrentLanguageData.Translate("CompositionCharacteristicLabel");
            vm.Initialize(heading, new List<QdasNumber>(this._listViewSettings.CharacteristicLabelQdasNumbers), TextEvaluationType.Characteristic, this._listViewSettings.CharacteristicLabelSeparator);
            void handler(object s, EventArgs e) { vm.Cancel(); }
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), handler);
            if (vm.ShowDialog(ParentView))
            {
                this._listViewSettings.CharacteristicLabelQdasNumbers = vm.SelectedLabels.Select(tupel => tupel.Item1).ToList();
                this._listViewSettings.CharacteristicLabelSeparator = vm.SelectedSeparator;
                if (!ConfigurationService.TrySetListViewSettings(this._listViewSettings))
                {
                    this._messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("EListViewSettingSaveFailed"));
                    this._listViewSettings = ConfigurationService.GetListViewSettingsCopy();
                }
            }
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), handler);
        }

        private void RefreshSpcPathCommandHandler(SpcPath spcPath)
        {
            this._qsStatService.RequestRefresh(spcPath);
        }

        private void EditPartGridColumnsCommandHandler()
        {
            string title = (string) LanguageManager.Current.CurrentLanguageData.Translate("EditPartGridColumns");
            var availableWithTranslation = _qsStatService.PartKFields.ToDictionary(entry=>entry.Key, entry=>entry.Value);
            List<Tuple<QdasNumber, string>> availableItems = new List<Tuple<QdasNumber, string>>();
            var lookup = new Dictionary<TextEvaluationType, List<Tuple<QdasNumber, string>>>
            {
                [TextEvaluationType.Part] = new List<Tuple<QdasNumber, string>>()
            };
            var selected = new List<Tuple<QdasNumberWrapper, string>>();
            EditQdasNumberWrapperColumnsViewModel vm;
            EventHandler<EventArgs> handler;
            foreach (var item in this._listViewSettings.PartGridQdasNumbers)
            {
                switch (item.QdasNumber.EvaluationType)
                {
                    case TextEvaluationType.Part:
                        string translation = "";
                        if (availableWithTranslation.TryGetValue(item.QdasNumber.EvaluationNumber, out translation))
                        {
                            availableWithTranslation.Remove(item.QdasNumber.EvaluationNumber);
                        }
                        selected.Add(new Tuple<QdasNumberWrapper, string>(new QdasNumberWrapper(item.CustomName, item.QdasNumber), translation));
                        break;
                    default:
                        selected.Add(new Tuple<QdasNumberWrapper, string>(new QdasNumberWrapper(item.CustomName, item.QdasNumber), ""));
                        break;
                }
            }
            foreach (var item in availableWithTranslation)
            {
                availableItems.Add(new Tuple<QdasNumber, string>(new QdasNumber(TextEvaluationType.Part, item.Key), item.Value));
            }
            lookup[TextEvaluationType.Part] = availableItems;
            vm = _editQdasNumberWrapperColumnsVMFactory.CreateExport().Value;
            vm.Initialize(title, selected, lookup);
            handler = (s, e) => { vm.Cancel(); };
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), handler);
            if (vm.ShowDialog(ParentView))
            {
                this._listViewSettings.PartGridQdasNumbers = vm.SelectedQdasNumbersWithTranslation.Select(item => item.Item1).ToList();
                if (!ConfigurationService.TrySetListViewSettings(this._listViewSettings))
                {
                    this._messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("EListViewSettingSaveFailed"));
                    this._listViewSettings = ConfigurationService.GetListViewSettingsCopy();
                }
            }
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), handler);
        }

        private void EditCharacteristicGridColumnsCommandHandler()
        {
            string title = (string)LanguageManager.Current.CurrentLanguageData.Translate("EditCharacteristicGridColumns");
            var characteristicFieldsWithTranslation = _qsStatService.CharacteristicKFields.ToDictionary(entry => entry.Key, entry => entry.Value);
            var numericAndTextWithTranslation = _qsStatService.StatResult.ToDictionary(entry => entry.Key, entry => entry.Value);
            foreach (var kvp in _qsStatService.PartKFields)
                break;

            var lookup = new Dictionary<TextEvaluationType, List<Tuple<QdasNumber, string>>>
            {
                [TextEvaluationType.Part] = new List<Tuple<QdasNumber, string>>()
            };
            var selected = new List<Tuple<QdasNumberWrapper, string>>();
            foreach (var item in this._listViewSettings.CharacteristicGridQdasNumbers)
            {
                string translation = "";
                switch (item.QdasNumber.EvaluationType)
                {
                    case TextEvaluationType.Characteristic:
                        if (characteristicFieldsWithTranslation.TryGetValue(item.QdasNumber.EvaluationNumber, out translation))
                        {
                            characteristicFieldsWithTranslation.Remove(item.QdasNumber.EvaluationNumber);
                        }
                        selected.Add(new Tuple<QdasNumberWrapper, string>(new QdasNumberWrapper(item.CustomName, item.QdasNumber), translation));
                        break;
                    case TextEvaluationType.NumericAndTextResults:
                        if (numericAndTextWithTranslation.TryGetValue(item.QdasNumber.EvaluationNumber, out translation))
                        {
                            numericAndTextWithTranslation.Remove(item.QdasNumber.EvaluationNumber);
                        }
                        selected.Add(new Tuple<QdasNumberWrapper, string>(new QdasNumberWrapper(item.CustomName, item.QdasNumber), translation));
                        break;
                    default:
                        selected.Add(new Tuple<QdasNumberWrapper, string>(new QdasNumberWrapper(item.CustomName, item.QdasNumber), ""));
                        break;
                }
            }
            List<Tuple<QdasNumber, string>> availableItems = new List<Tuple<QdasNumber, string>>();
            foreach (var item in characteristicFieldsWithTranslation)
            {
                availableItems.Add(new Tuple<QdasNumber, string>(new QdasNumber(TextEvaluationType.Characteristic, item.Key), item.Value));
            }
            lookup[TextEvaluationType.Characteristic] = availableItems;
            availableItems = new List<Tuple<QdasNumber, string>>();
            foreach (var item in numericAndTextWithTranslation)
            {
                availableItems.Add(new Tuple<QdasNumber, string>(new QdasNumber(TextEvaluationType.NumericAndTextResults, item.Key), item.Value));
            }
            lookup[TextEvaluationType.NumericAndTextResults] = availableItems;
            var vm = _editQdasNumberWrapperColumnsVMFactory.CreateExport().Value;
            vm.Initialize(title, selected, lookup);
            void handler(object s, EventArgs e) { vm.Cancel(); }
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), handler);
            if (vm.ShowDialog(ParentView))
            {
                this._listViewSettings.CharacteristicGridQdasNumbers = vm.SelectedQdasNumbersWithTranslation.Select(item => item.Item1).ToList();
                if (!ConfigurationService.TrySetListViewSettings(this._listViewSettings))
                {
                    this._messageService.ShowError(ParentView, (string)LanguageManager.Current.CurrentLanguageData.Translate("EListViewSettingSaveFailed"));
                    this._listViewSettings = ConfigurationService.GetListViewSettingsCopy();
                }
            }
            WeakEventManager<ConfigurationService, EventArgs>.RemoveHandler(this.ConfigurationService, nameof(ConfigurationService.ListViewSettingsChanged), handler);
        }
        #endregion

        #region helper functions
        private void GenerateDataList()
        {
            var dataList = new List<SpcPathNodeData>();
            foreach (var spcPathResult in _qsStatService.SpcPathResults.OrderBy(kvp => kvp.Key.SpcNumber))
            {
                ICollection<PartNodeData> partNodeCollection = GeneratePartNodeCollection(spcPathResult.Value.EvaluatedData.Values);
                dataList.Add(new SpcPathNodeData( spcPathResult.Key, partNodeCollection, spcPathResult.Value.ProcessState));
            }
            SpcPathNodeData = dataList;
        }

        private ICollection<PartNodeData> GeneratePartNodeCollection(IEnumerable<QsStatPartResultDTO> partResults)
        {
            var partNodes = new List<PartNodeData>();
            foreach (var partResult in partResults)
            {
                List<CharacteristicNodeData> characteristicNodes = GenerateCharacteristicNodeCollection(partResult.CharacteristicResults);
                var labelsAndState = GenerateNodeLabels(_listViewSettings.PartLabelQdasNumbers, partResult.PartTextResults);
                var partNode = new PartNodeData(partResult.DatasetReferenceGuid, labelsAndState.Item1, characteristicNodes, partResult.ProcessState);
                partNodes.Add(partNode);
            }
            return partNodes;
        }

        private List<CharacteristicNodeData> GenerateCharacteristicNodeCollection(IReadOnlyDictionary<int, QsStatCharacteristicResultDTO> characteristicResults)
        {
            var characteristicNodes = new List<CharacteristicNodeData>();
            var indices = characteristicResults.Keys.OrderBy(key => key);
            foreach (var index in indices)
            {
                var characteristicData = characteristicResults[index];
                if (!characteristicData.CharacteristicMetaData.Enabled)
                    continue;
                var labelsAndState = GenerateNodeLabels(_listViewSettings.CharacteristicLabelQdasNumbers, characteristicData.CharacteristicTextResults);
                var node = new CharacteristicNodeData(index, labelsAndState.Item1, characteristicData.CharacteristicMetaData.State)
                {
                    MissingLabelValues = labelsAndState.Item2
                };
                characteristicNodes.Add(node);
            }
            return characteristicNodes;
        }

        private void RefreshDataHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add)
                return;
            var replacedData = e.NewItems[0] as KeyValuePair<SpcPath, SpcPathEvaluationDTO>?;
            if (!replacedData.HasValue)
                throw new ArgumentOutOfRangeException($"{nameof(NotifyCollectionChangedEventArgs)} is missing the replaced element");
            DispatchHelper.BeginInvoke(()=>RefreshDataHandlerImpl(replacedData.Value));
        }

        private void RefreshDataHandlerImpl(KeyValuePair<SpcPath, SpcPathEvaluationDTO> replacedData)
        {
            var spcPath = replacedData.Key;
            var newData = replacedData.Value;
            var spcNode = SpcPathNodeData.First(node => node.SpcPath.Equals(spcPath));
            spcNode.ProcessState = newData.ProcessState;
            var toRemoveGuids = spcNode.PartNodeData.Select(node => node.PartReferenceGuid).Except(newData.EvaluatedData.Keys);
            var toRemoveParts = spcNode.PartNodeData.Where(node => toRemoveGuids.Contains(node.PartReferenceGuid)).ToList();
            spcNode.BeginPartNodeDataBulkUpdate();
            foreach (var item in toRemoveParts)
            {
                spcNode.PartNodeData.Remove(item);
            }
            foreach (var part in newData.EvaluatedData.Values)
            {
                PartNodeData toUpdatePart = spcNode.PartNodeData.FirstOrDefault(partNode => partNode.PartReferenceGuid.Equals(part.DatasetReferenceGuid));
                if (toUpdatePart != null)
                {
                    UpdatePartNode(toUpdatePart, part);
                }
                else
                {
                    var characteristics = GenerateCharacteristicNodeCollection(part.CharacteristicResults);
                    var labelsAndMissing = GenerateNodeLabels(_listViewSettings.PartLabelQdasNumbers, part.PartTextResults);
                    var node = new PartNodeData(part.DatasetReferenceGuid, labelsAndMissing.Item1, characteristics, part.ProcessState)
                    {
                        MissingLabelValues = labelsAndMissing.Item2
                    };
                    spcNode.PartNodeData.Add(node);
                }
            }
            spcNode.EndPartNodeDataBulkUpdate();
            CurrentAreaState = MeasurementHelper.EvaluateState(SpcPathNodeData.Select(node => node.ProcessState));
        }

        private void UpdatePartNode(PartNodeData toUpdatePart, QsStatPartResultDTO newPartData)
        {
            if (toUpdatePart.MissingLabelValues)
            {
                var labelsAndMissing = GenerateNodeLabels(_listViewSettings.PartLabelQdasNumbers, newPartData.PartTextResults);
                toUpdatePart.Labels = labelsAndMissing.Item1;
                toUpdatePart.MissingLabelValues = labelsAndMissing.Item2;
            }
            toUpdatePart.ProcessState = newPartData.ProcessState;
            foreach (var characteristic in toUpdatePart.CharacteristicNodes)
            {
                if (newPartData.CharacteristicResults.TryGetValue(characteristic.CharacteristicIndex, out QsStatCharacteristicResultDTO characteristicsData))
                {
                    if (characteristic.MissingLabelValues)
                    {
                        var labelsAndMissing = GenerateNodeLabels(_listViewSettings.CharacteristicLabelQdasNumbers, characteristicsData.CharacteristicTextResults);
                        characteristic.Labels = labelsAndMissing.Item1;
                        characteristic.MissingLabelValues = labelsAndMissing.Item2;
                    }
                    characteristic.ProcessState = characteristicsData.CharacteristicMetaData.State;
                }
                
            }
        }

        private void UpdatePartNodeLabels()
        {
            var data = _qsStatService.SpcPathResults;
            foreach (var spcNode in SpcPathNodeData)
            {
                if (!data.TryGetValue(spcNode.SpcPath, out SpcPathEvaluationDTO spcData))
                    continue;
                foreach (var partNode in spcNode.PartNodeData)
                {
                    if (!spcData.EvaluatedData.TryGetValue(partNode.PartReferenceGuid, out QsStatPartResultDTO partData))
                        continue;
                    var labelsAndMissing = GenerateNodeLabels(_listViewSettings.PartLabelQdasNumbers, partData.PartTextResults);
                    partNode.Labels = labelsAndMissing.Item1;
                    partNode.MissingLabelValues = labelsAndMissing.Item2;
                }
            }
        }

        private void UpdateCharacteristicNodeLabels()
        {
            var data = _qsStatService.SpcPathResults;
            QsStatCharacteristicResultDTO characteristicData;
            foreach (var spcNode in SpcPathNodeData)
            {
                if (!data.TryGetValue(spcNode.SpcPath, out SpcPathEvaluationDTO spcData))
                    continue;
                foreach (var partNode in spcNode.PartNodeData)
                {
                    if (!spcData.EvaluatedData.TryGetValue(partNode.PartReferenceGuid, out QsStatPartResultDTO partData))
                        continue;
                    foreach (var characteristicNode in partNode.CharacteristicNodes)
                    {
                        characteristicData = partData.CharacteristicResults[characteristicNode.CharacteristicIndex];
                        var labelsAndMissing = GenerateNodeLabels(_listViewSettings.CharacteristicLabelQdasNumbers, characteristicData.CharacteristicTextResults);
                        characteristicNode.Labels = labelsAndMissing.Item1;
                        characteristicNode.MissingLabelValues = labelsAndMissing.Item2;
                    }
                }
            }
        }

        private Tuple<List<string>, bool> GenerateNodeLabels(IEnumerable<QdasNumber> lookupKeys, IReadOnlyDictionary<int, string> data)
        {
            var result = new List<string>();
            bool missingValues = false;
            string tmpValue = null;
            foreach (var key in lookupKeys)
            {
                if (data.TryGetValue(key.EvaluationNumber, out tmpValue))
                {
                    missingValues = true;
                    result.Add(tmpValue);
                }else
                {
                    result.Add(String.Empty);
                }
            }
            return new Tuple<List<string>, bool>(result, missingValues);
        }

        private SpcPath FindBelongingSpcPath(PartNodeData partNode)
        {
            foreach (var spcPathNode in this._spcPathData)
            {
                if (spcPathNode.PartNodeData.Contains(partNode))
                    return spcPathNode.SpcPath;
            }
            return null;
        }

        private Tuple<bool, SpcPath, Guid> FindBelongingSpcPathAndFileGuid(CharacteristicNodeData charaNode)
        {
            foreach (var spcNode in this._spcPathData)
            {
                foreach (var partNode in spcNode.PartNodeData)
                {
                    var maybeFound = partNode.CharacteristicNodes.FirstOrDefault(node => node.Equals(charaNode));
                    if (maybeFound != null)
                    {
                        return new Tuple<bool, SpcPath, Guid>(true, spcNode.SpcPath, partNode.PartReferenceGuid);
                    }
                }
            }
            return new Tuple<bool, SpcPath, Guid>(false, null, Guid.Empty);
        }
        #endregion
    }
}
