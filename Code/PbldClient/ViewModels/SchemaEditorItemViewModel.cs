﻿using Promess.Common.Services;
using Promess.Language;
using Promess.Pbld.Data.Schema;
using Promess.Pbld.IViews;
using Promess.Pbld.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class SchemaEditorItemViewModel:ViewModel<ISchemaItemPropertiesView>
    {
        #region fields
        private readonly IFileDialogServiceExtended _fileDialogService;
        private readonly IMessageService _messageService;

        private SchemaItem _data;
        private object _parentView;
        #endregion

        [ImportingConstructor]
        public SchemaEditorItemViewModel(ISchemaItemPropertiesView view,
            IFileDialogServiceExtended fileDialogService, IMessageService messageService) : base(view)
        {
            this._fileDialogService = fileDialogService;
            this._messageService = messageService;
            SelectImageCommand = new DelegateCommand(SelectImageCommandHandler);
        }

        internal void Setup(object parentView, SchemaItem data)
        {
            this._parentView = parentView;
            this._data = data;
        }

        public SchemaItem Data
        {
            get { return _data; }
        }

        public ICommand SelectImageCommand
        {
            get; private set;
        }

        private void SelectImageCommandHandler()
        {
            FileType ft = new FileType("jpg, png, bmp", new string[] { "*.jpg", "*.png", "*.bmp" });
            object parentView = this._parentView ?? View;
            var filename = this._fileDialogService.ShowOpenFileDialog(parentView, ft, Data.ImagePath, true).FirstOrDefault();
            if (String.IsNullOrWhiteSpace(filename))
                return;

            var trimmedPath = filename.Trim();
            try
            {
                byte[] image = ImageHelper.GetImageFromPathAsByteArray(trimmedPath);
                Data.ImagePath = trimmedPath;
                Data.Image = image;
            }
            catch
            {
                string message = (string)LanguageManager.Current.CurrentLanguageData.Translate("ELoadingFile");
                this._messageService.ShowError(parentView, message);
            }
        }
    }
}
