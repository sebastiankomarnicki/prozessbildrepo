﻿using Promess.Language;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class TextEditorItemViewModel : ViewModel<ITextPropertiesView>
    {
        #region fields
        private readonly ExportFactory<SelectFromDictionaryWithFilterViewModel> _selectFromDictionaryWithFilterVMFactory;
        private readonly ExportFactory<TextEvaluationTypeSelectionViewModel> _textEvaluationTypeSelectionVMFactory;
        private readonly ExportFactory<PropertiesRepresentationTextViewModel> _propertiesBorderExtendedVMFactory;
        private readonly ExportFactory<PropertiesFontAndColorExtendedViewModel> _propertiesFontColorExtVMFactory;

        private object _borderExtendedView;
        private object _fontAndColorExtendedView;
        private TextEditorItem _data;
        private DelegateCommand _selectTextKNumberCommand;
        private readonly IReadOnlyCollection<int> _textSizes;
        #endregion

        [ImportingConstructor]
        public TextEditorItemViewModel(ITextPropertiesView view,
            ExportFactory<TextEvaluationTypeSelectionViewModel> textEvaluationTypeSelectionVMFactory,
            ExportFactory<SelectFromDictionaryWithFilterViewModel> selectTextQsStatResultVMFactory,
            ExportFactory<PropertiesRepresentationTextViewModel> propertiesBorderExtendedVMFactory,
            ExportFactory<PropertiesFontAndColorExtendedViewModel> propertiesFontColorExtVMFactory) : base(view)
        {
            this._textEvaluationTypeSelectionVMFactory = textEvaluationTypeSelectionVMFactory;
            this._selectFromDictionaryWithFilterVMFactory = selectTextQsStatResultVMFactory;
            this._propertiesBorderExtendedVMFactory = propertiesBorderExtendedVMFactory;
            this._propertiesFontColorExtVMFactory = propertiesFontColorExtVMFactory;
            this._selectTextKNumberCommand = new DelegateCommand(SelectTextKNumberHandler);
            this._textSizes = new List<int>() { 10, 12, 14, 18, 22, 26 };
        }

        public void Setup(object parentView, TextEditorItem data)
        {
            this.ParentView = parentView;
            this.Data = data;
            var borderVm = this._propertiesBorderExtendedVMFactory.CreateExport().Value;
            borderVm.Setup(this.Data);
            this.BorderExtendedView = borderVm.View;
            var fontColorExtVM = this._propertiesFontColorExtVMFactory.CreateExport().Value;
            fontColorExtVM.Setup(this.Data);
            this.FontAndColorExtendedView = fontColorExtVM.View;
        }

        public object ParentView { get; private set; }

        public object BorderExtendedView
        {
            get { return _borderExtendedView; }
            private set { SetProperty(ref _borderExtendedView, value); }
        }

        public object FontAndColorExtendedView
        {
            get { return _fontAndColorExtendedView; }
            private set { SetProperty(ref _fontAndColorExtendedView, value); }
        }

        public TextEditorItem Data
        {
            get { return _data; }
            private set
            {
                SetProperty(ref _data, value);
            }
        }

        public IReadOnlyCollection<int> TextSizes
        {
            get { return _textSizes; }
        }

        public ICommand SelectKNumberCommand
        {
            get { return _selectTextKNumberCommand; }
        }

        private void SelectTextKNumberHandler()
        {
            var vm = _textEvaluationTypeSelectionVMFactory.CreateExport().Value;
            vm.Initialize(Data.QdasNumber);
            if (vm.ShowDialog(ParentView ?? View)){
                Data.QdasNumber = new QdasNumber(vm.SelectedEvaluationType.Value, vm.SelectedItem.Value);
            }
        }

        private IReadOnlyDictionary<Predicate<object>, string> GetFilters()
        {
            var filters = new Dictionary<Predicate<object>, string>();
            filters.Add(new Predicate<object>(x => true), (string)LanguageManager.Current.CurrentLanguageData.Translate("All"));
            filters.Add(new Predicate<object>(x => ((KeyValuePair<int, string>)x).Key >= 1000 && ((KeyValuePair<int, string>)x).Key <= 1999), (string)LanguageManager.Current.CurrentLanguageData.Translate("PartData"));
            filters.Add(new Predicate<object>(x => ((KeyValuePair<int, string>)x).Key >= 2000 && ((KeyValuePair<int, string>)x).Key <= 2999), (string)LanguageManager.Current.CurrentLanguageData.Translate("CharData"));
            return filters;
        }
    }
}
