﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class EllipseEditorItemViewModel:ViewModel<IEllipsePropertiesView>
    {
        #region fields
        private EllipseEditorItem _data;
        #endregion

        [ImportingConstructor]
        public EllipseEditorItemViewModel(IEllipsePropertiesView view) : base(view)
        {
        }

        public object ParentView { get; set; }

        public EllipseEditorItem Data
        {
            get { return _data; }
            set
            {
                SetProperty(ref _data, value);
            }
        }
    }
}
