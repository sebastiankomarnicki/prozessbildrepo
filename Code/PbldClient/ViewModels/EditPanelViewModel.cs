﻿using Promess.Common.Util;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.Editor;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Waf.Applications.Services;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class EditPanelViewModel : BaseEditorViewModelExtended<IEditPanelView>
    {
        #region Fields
        private readonly ExportFactory<PropertiesViewModel<IImagePropertiesView>> _propertiesImageVMFactory;
        private readonly ExportFactory<PropertiesViewModel<ITextPropertiesView>> _propertiesTextVMFactory;
        private readonly ExportFactory<PropertiesViewModel<IButtonPropertiesView>> _propertiesButtonVMFactory;
        private readonly ExportFactory<TextEditorItemViewModel> _textEditorItemViewModelFactory;
        private readonly ExportFactory<ImageEditorItemViewModel> _imageEditorItemViewModelFactory;
        private readonly ExportFactory<ButtonEditorItemViewModel> _buttonEditorItemViewModelFactory;

        private DelegateCommand _addImageCommand;
        private DelegateCommand _addTextBoxCommand;
        private DelegateCommand _addButtonCommand;

        private PresetGraphicSettings _presetGraphicSettings;

        private PanelItemModel _data;
        #endregion

        [ImportingConstructor]
        public EditPanelViewModel(IEditPanelView view, IConfigurationService configurationService, IMessageService messageService,
            ExportFactory<PropertiesViewModel<IImagePropertiesView>> propertiesImageVMFactory, ExportFactory<PropertiesViewModel<ITextPropertiesView>> propertiesTextVMFactory, ExportFactory<PropertiesViewModel<IButtonPropertiesView>> propertiesButtonVMFactory,
            ExportFactory<TextEditorItemViewModel> textEditorItemViewModelFactory, ExportFactory<ImageEditorItemViewModel> imageEditorItemViewModelFactory,
            ExportFactory<ButtonEditorItemViewModel> buttonEditorItemViewModelFactory
            ) : base(view, messageService)
        {
            this._propertiesImageVMFactory = propertiesImageVMFactory;
            this._propertiesTextVMFactory = propertiesTextVMFactory;
            this._propertiesButtonVMFactory = propertiesButtonVMFactory;
            this._textEditorItemViewModelFactory = textEditorItemViewModelFactory;
            this._imageEditorItemViewModelFactory = imageEditorItemViewModelFactory;
            this._buttonEditorItemViewModelFactory = buttonEditorItemViewModelFactory;
            this._presetGraphicSettings = configurationService.GetPresetGraphicSettingsCopy();

            this._addButtonCommand = new DelegateCommand(AddButtonCommandHandler, ()=> Data!=null);
            this._addImageCommand = new DelegateCommand(AddImageCommandHandler, () => Data != null);
            this._addTextBoxCommand = new DelegateCommand(AddTextBoxCommandHandler, () => Data!=null);
            this.SelectAspectRatioCommand = new DelegateCommand<AspectRatio>(SelectAspectRatioCommandHandler, _ => Data != null);
        }

        public void Initialize(PanelItemModel data)
        {
            var elems = new List<EditorItemModelBase>();
            foreach (var elem in data.Items)
            {
                elems.Add( GenerateItemModel(elem));
            }
            base.Initialize(elems);
            this.Data = data;
            RaisePropertyChanged(nameof(CurrentDataName));
            UpdateCommandsCanExecute();
        }

        private void UpdateCommandsCanExecute()
        {
            AddTextBoxCommand.RaiseCanExecuteChanged();
            AddImageCommand.RaiseCanExecuteChanged();
            AddButtonCommand.RaiseCanExecuteChanged();
            SelectAspectRatioCommand.RaiseCanExecuteChanged();
        }

        #region Properties
        public string CurrentDataName
        {
            get { return Data?.Name; }
        }

        public PanelItemModel Data
        {
            get { return _data; }
            private set { SetProperty(ref _data, value); }
        }

        internal object ParentView { get; set; }
      
        public DelegateCommand AddTextBoxCommand { get { return _addTextBoxCommand; } }
        public DelegateCommand AddImageCommand { get { return _addImageCommand; } }
        public DelegateCommand AddButtonCommand { get { return _addButtonCommand; } }
        public DelegateCommand<AspectRatio> SelectAspectRatioCommand { get; private set; }
        #endregion

        private void InitRectangleBase(RectangleEditorItem item)
        {
            item.X = _rnd.Next(0, 50);
            item.Y = _rnd.Next(0, 50);
            item.Width = 50;
            item.Height = 50;
            item.IsDirty = true;
        }

        public override void ResetData()
        {
            base.ResetData();
            this.Data = null;
            UpdateCommandsCanExecute();
            RaisePropertyChanged(nameof(CurrentDataName));
        }

        protected override void SavePercentage(EditorItemBase elem)
        {
            elem.PercentageX = elem.X / LastRecalculateSize.Width;
            elem.PercentageY = elem.Y / LastRecalculateSize.Height;
            switch (elem)
            {
                case LineEditorItem line:
                    line.PercentageX1 = line.X1 / LastRecalculateSize.Width;
                    line.PercentageX2 = line.X2 / LastRecalculateSize.Width;
                    line.PercentageY1 = line.Y1 / LastRecalculateSize.Height;
                    line.PercentageY2 = line.Y2 / LastRecalculateSize.Height;
                    break;
                case RectangleEditorItem rectangle:
                    rectangle.PercentageWidth = rectangle.Width / LastRecalculateSize.Width;
                    rectangle.PercentageHeight = rectangle.Height / LastRecalculateSize.Height;
                    break;
                case CircleEditorItem circle:
                    circle.PercentageDiameter = circle.Diameter / Math.Min(LastRecalculateSize.Height, LastRecalculateSize.Width);
                    break;
                default:
                    throw new NotImplementedException($"{elem.GetType()} is not supported in {nameof(SavePercentage)}.");
            }
        }

        protected override void RecalculateActualSizes(EditorItemBase elem)
        {
            bool oldIsDirty = elem.IsDirty;
            elem.X = elem.PercentageX * ViewWidth;
            elem.Y = elem.PercentageY * ViewHeight;
            switch (elem)
            {
                case LineEditorItem line:
                    line.X1 = line.PercentageX1 * ViewWidth;
                    line.X2 = line.PercentageX2 * ViewWidth;
                    line.Y1 = line.PercentageY1 * ViewHeight;
                    line.Y2 = line.PercentageY2 * ViewHeight;
                    break;
                case RectangleEditorItem rectangle:
                    rectangle.Width = rectangle.PercentageWidth * ViewWidth;
                    rectangle.Height = rectangle.PercentageHeight * ViewHeight;
                    break;
                case CircleEditorItem circle:
                    circle.Diameter = circle.PercentageDiameter * Math.Min(ViewHeight, ViewWidth);
                    break;
                default:
                    throw new NotImplementedException($"{elem.GetType()} is not supported in {nameof(RecalculateActualSizes)}.");
            }
            elem.IsDirty = oldIsDirty;
        }

        private void AddItem(EditorItemBase elem)
        {
            var wrapped = GenerateItemModel(elem);
            AddItem(wrapped);
            ClearSelectedItems();
            wrapped.IsSelected = true;
        }

        protected override void AddItem(EditorItemModelBase model)
        {
            base.AddItem(model);
            Data.Items.Add(model.Item);
        }

        protected override EditorItemModelBase GenerateItemModel(EditorItemBase elem)
        {
            EditorItemModelBase model;
            switch (elem)
            {
                case ButtonEditorItem buttonItem:
                    model = new RectangleEditorItemModel<ButtonEditorItem>(this, buttonItem, EditButtonEditorItemSettings);
                    break;
                case TextEditorItem textItem:
                    model = new RectangleEditorItemModel<TextEditorItem>(this, textItem, EditTextEditorItemSettings);
                    break;
                case ImageEditorItem imageItem:
                    model = new RectangleEditorItemModel<ImageEditorItem>(this, imageItem, EditImageEditorItemSettings);
                    break;
                default:
                    throw new NotImplementedException($"The container implementation for {elem.GetType()} is missing.");
            }

            elem.IsDirty = false;
            PropertyChangedEventManager.AddHandler(model, IsSelectedChanged, nameof(EditorItemModelBase.IsSelected));
            return model;
        }

        protected override void RemoveItem(EditorItemModelBase model)
        {
            base.RemoveItem(model);
            Data.Items.Remove(model.Item);
        }

        protected ButtonEditorItem EditButtonEditorItemSettings(ButtonEditorItem item)
        {
            var propVM = _propertiesButtonVMFactory.CreateExport().Value;
            var vm = _buttonEditorItemViewModelFactory.CreateExport().Value;
            var itemCopy = SerializeHelper.CreateDataContractCopy(item);
            CopyOriginalSizes(item, itemCopy);
            vm.Setup(propVM.View, itemCopy);
            propVM.Setup(vm);
            return ProcessPropertiesViewModel(propVM, item, itemCopy);
        }

        protected ImageEditorItem EditImageEditorItemSettings(ImageEditorItem item)
        {
            var propVM = _propertiesImageVMFactory.CreateExport().Value;
            var vm = _imageEditorItemViewModelFactory.CreateExport().Value;
            var itemCopy = SerializeHelper.CreateDataContractCopy(item);
            CopyOriginalSizes(item, itemCopy);
            vm.Setup(propVM.View, itemCopy);
            propVM.Setup(vm);       
            return ProcessPropertiesViewModel(propVM, item, itemCopy);
        }

        protected TextEditorItem EditTextEditorItemSettings(TextEditorItem item)
        {
            var propVM = _propertiesTextVMFactory.CreateExport().Value;
            var textVM = _textEditorItemViewModelFactory.CreateExport().Value;
            var itemCopy = SerializeHelper.CreateDataContractCopy(item);
            CopyOriginalSizes(item, itemCopy);
            textVM.Setup(propVM.View, itemCopy);
            propVM.Setup(textVM);
            return ProcessPropertiesViewModel(propVM, item, itemCopy);
        }

        private void CopyOriginalSizes(RectangleEditorItem source, RectangleEditorItem destination)
        {
            base.CopyOriginalSizes(source, destination);
            destination.Width = source.Width;
            destination.Height = source.Height;
        }

        private T ProcessPropertiesViewModel<TView, T>(PropertiesViewModel<TView> propVM, T original, T copy)
            where TView: IView
            where T : EditorItemBase
        {
            if (propVM.ShowDialog(ParentView))
            {
                var index = Data.Items.IndexOf(original);
                Data.Items[index] = copy;
                return copy;
            }
            else
            {
                return original;
            }
        }

        #region command handler
        private void AddButtonCommandHandler()
        {
            var item = new ButtonEditorItem();
            this._presetGraphicSettings.PresetButton.OverwritePresets(item);
            InitRectangleBase(item);
            AddItem(item);          
        }

        private void AddImageCommandHandler()
        {
            var item = new ImageEditorItem();
            this._presetGraphicSettings.PresetGraphic.OverwritePresets(item);
            InitRectangleBase(item);
            AddItem(item);            
        }

        private void AddTextBoxCommandHandler()
        {
            var item = new TextEditorItem(); 
            this._presetGraphicSettings.PresetText.OverwritePresets(item);
            InitRectangleBase(item);
            AddItem(item);          
        }

        private void SelectAspectRatioCommandHandler(AspectRatio aspectRatio)
        {
            this.Data.AspectRatio = aspectRatio;
        }
        #endregion
    }
}
