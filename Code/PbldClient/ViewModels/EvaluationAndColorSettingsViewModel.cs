﻿using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class EvaluationAndColorSettingsViewModel : ViewModel<IEvaluationAndColorSettingsView>
    {

        [ImportingConstructor]
        public EvaluationAndColorSettingsViewModel(IEvaluationAndColorSettingsView view, IconLookupService iconLookupService) : base(view)
        {
            this.IconLookupService = iconLookupService;
        }

        internal void Initialize(EvaluationAndColorSettings settings)
        {
            EvaluationAndColorElements = GenerateEvaluationAndColorElements(settings);
            AvailableIcons = IconLookupService.GetDefinedImageElements();
            Settings = settings;
        }

        private List<Tuple<ProcessState, EvaluationAndColorElement>> GenerateEvaluationAndColorElements(EvaluationAndColorSettings settings)
        {
            List<Tuple<ProcessState, EvaluationAndColorElement>> evaluationAndColorElements = new List<Tuple<ProcessState, EvaluationAndColorElement>>();
            evaluationAndColorElements.Add(new Tuple<ProcessState, EvaluationAndColorElement>(ProcessState.Ok, settings.ProcessOk));
            evaluationAndColorElements.Add(new Tuple<ProcessState, EvaluationAndColorElement>(ProcessState.Endangered, settings.ProcessEndangered));
            evaluationAndColorElements.Add(new Tuple<ProcessState, EvaluationAndColorElement>(ProcessState.OutOfControl, settings.ProcessOoC));
            evaluationAndColorElements.Add(new Tuple<ProcessState, EvaluationAndColorElement>(ProcessState.StabilityViolation, settings.StabilityViolation));
            evaluationAndColorElements.Add(new Tuple<ProcessState, EvaluationAndColorElement>(ProcessState.ActionLimitViolationXq, settings.ActionLimitViolationXq));
            evaluationAndColorElements.Add(new Tuple<ProcessState, EvaluationAndColorElement>(ProcessState.ActionLimitViolationS, settings.ActionLimitViolationS));
            evaluationAndColorElements.Add(new Tuple<ProcessState, EvaluationAndColorElement>(ProcessState.DeficientPart, settings.DeficientPart));
            evaluationAndColorElements.Add(new Tuple<ProcessState, EvaluationAndColorElement>(ProcessState.TooFewValues, settings.NoOrTooFewValues));
            return evaluationAndColorElements;
        }

        internal void UpdateSettings(EvaluationAndColorSettings settings)
        {
            EvaluationAndColorElements = GenerateEvaluationAndColorElements(settings);
            Settings = settings;
            RaisePropertyChanged(nameof(Settings));
            RaisePropertyChanged(nameof(EvaluationAndColorElements));
        }

        public ICommand BackgroundColorChangedCommand { get; }

        public List<Tuple<ProcessState, EvaluationAndColorElement>> EvaluationAndColorElements { get; private set; }

        public EvaluationAndColorSettings Settings { get; private set; }

        public IconLookupService IconLookupService { get; private set; }

        public IReadOnlyList<IconElement> AvailableIcons { get; private set; }
    }
}
