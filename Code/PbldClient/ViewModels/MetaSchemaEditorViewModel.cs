﻿using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class MetaSchemaEditorViewModel:ViewModel<IMetaEditSchemaView>
    {
        #region Fields
        private readonly IMainService _mainService;
        private readonly IMessageService _messageService;
        private readonly ConfigurationService _configurationService;
        private readonly EditSchemaViewModel _schemaEditVM;

        private Area _currentArea;
        private object _currentView;
        private bool _abort;
        #endregion

        [ImportingConstructor]
        public MetaSchemaEditorViewModel(IMetaEditSchemaView view, 
            IMainService mainService, ConfigurationService configurationService, IMessageService messageService,
        EditSchemaViewModel schemaEditVM):base(view)
        {
            this._mainService = mainService;
            this._configurationService = configurationService;
            this._messageService = messageService;
            this._schemaEditVM = schemaEditVM;
            this._currentView = null;

            //TODO what about changed layout?
            PropertyChangedEventManager.AddHandler(this._mainService, IsAuthenticatedChangedHandler, nameof(IMainService.IsAuthenticated));
            PropertyChangedEventManager.AddHandler(this._mainService, WindowStateChangedHander, nameof(IMainService.WindowState));
        }

        internal void Setup(object parentView)
        {
            this.ParentView = parentView;
            PropertyChangedEventManager.AddHandler(this._configurationService, HandleAreaChanged, nameof(ConfigurationService.CurrentAreaName));

            this._currentArea = this._configurationService.GetCurrentAreaCopyOrNull();          
            if (this._currentArea == null)
            {
                CurrentView = null;
                this.CloseForParentCommand?.Execute(null);
                return;
            }
            this._schemaEditVM.Initialize(parentView, () => CloseForParentCommand?.Execute(null), this._currentArea);
            CurrentView = this._schemaEditVM.View;
        }

        #region Properties
        public object ParentView { get; private set; }
        public double ViewHeight { get; set; }
        public double ViewWidth { get; set; }
        public object CurrentView {
            get { return _currentView;}
            private set { SetProperty(ref _currentView, value); }
        }
        public EditSchemaViewModel EditVM
        {
            get { return this._schemaEditVM; }
        }

        public ICommand CloseForParentCommand { get; internal set; }
        #endregion

        #region helpers
        public void Close()
        {
            PropertyChangedEventManager.RemoveHandler(this._configurationService, HandleAreaChanged, nameof(ConfigurationService.CurrentAreaName));
            if (EditVM.GetIsDirty() && !this._abort)
            {
                bool answer = _messageService.ShowYesNoQuestion(ParentView,
                    (string)LanguageManager.Current.CurrentLanguageData.Translate("QOverviewUnsavedChanges"));
                if (answer)
                {
                    SaveChangesForCurrentArea();
                }
            }
            PropertyChangedEventManager.RemoveHandler(this._mainService, IsAuthenticatedChangedHandler, nameof(IMainService.IsAuthenticated));
            PropertyChangedEventManager.RemoveHandler(this._mainService, WindowStateChangedHander, nameof(IMainService.WindowState));
        }

        private void SaveChangesForCurrentArea()
        {
            if (this._currentArea is null)
                return;

            this._schemaEditVM.SavePercentages();
            SaveAreas();
        }

        private void SaveAreas()
        {
            var areas = this._configurationService.GetAreasCopy();
            areas.RemoveArea(areas.SelectedArea);
            areas.TryAddArea(this._currentArea);
            areas.SelectArea(this._currentArea);
            //this change of the area is supposed to be caught by the handler
            if (!this._configurationService.TrySetAreas(areas))
            {
                this._messageService.ShowError(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("ESetAreasFailed"));
            }
        }
        #endregion

        #region event handlers
        private void IsAuthenticatedChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            this._abort = true;
            this.CloseForParentCommand?.Execute(null);
        }

        private void WindowStateChangedHander(object sender, PropertyChangedEventArgs e)
        {
            this.CloseForParentCommand?.Execute(null);
        }

        private void HandleAreaChanged(object sender, PropertyChangedEventArgs e)
        {
            this._currentArea = this._configurationService.GetCurrentAreaCopyOrNull();
            if (this._currentArea == null)
            {
                this._abort = true;
                CurrentView = null;
                this.CloseForParentCommand?.Execute(null);
                return;
            }
            this._schemaEditVM.SetAreaAndGenerateItems(this._currentArea);
            CurrentView = this._schemaEditVM.View;  
        }
        #endregion
    }
}
