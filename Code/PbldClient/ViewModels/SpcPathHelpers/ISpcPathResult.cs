﻿namespace Promess.Pbld.ViewModels.SpcPathHelpers
{
    internal interface ISpcPathResult
    {
        bool IsValid();
        void CancelOutstandingOperations();
        void RemoveEventHandlers();
    }
}
