﻿using Promess.Common.Util.Async;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.QsStat.Data;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class MetaQsStatRequestViewModel:ViewModel<IMetaQsStatRequestView>
    {
        private readonly QsStatService _qsStatService;
        private readonly IMessageService _messageService;
        private readonly ConfigurationService _configurationService;
        private readonly QsStatRequestViewModel _qsStatRequestVM;

        private object _parentView;
        private int _selectedCharacteristicNumber;
        private int _selectedPage;
        private SpcPath _spcPath;
        private Guid _datasetGuid;
        private CancellationTokenSource _cts;
        private Task<byte[]> _graphTask;
        private int? _selectedReportId;
        private string _selectedReportDefFile;
        private bool _isValid;

        private AsyncCommand _printCommand;
        private AsyncCommand _printAllCommand;
        private DelegateCommand _selectPageCommand;

        [ImportingConstructor]
        public MetaQsStatRequestViewModel(IMetaQsStatRequestView view,
            QsStatService qsStatService, IMessageService messageService, ConfigurationService configurationService,
            QsStatRequestViewModel qsStatRequestVM) : base(view)
        {
            this._qsStatService = qsStatService;
            this._messageService = messageService;
            this._configurationService = configurationService;
            this._qsStatRequestVM = qsStatRequestVM;
            this._cts = new CancellationTokenSource();
        }

        public void Setup(object parentView)
        {
            this._parentView = parentView;
            this.CurrentView = this._qsStatRequestVM.View;
            this._graphTask = Task.FromResult<byte[]>(null);
            this._selectPageCommand = new DelegateCommand(RequestSelectedPage, ()=>IsValid && !String.IsNullOrWhiteSpace(this.SelectedReportDefFile));
            FirstPageCommand = new DelegateCommand(() => RequestPage(1), ()=> SelectedPage > 1);
            PreviousPageCommand = new DelegateCommand(() => RequestPage(SelectedPage - 1), () => SelectedPage > 1);
            NextPageCommand = new DelegateCommand(() => RequestPage(SelectedPage + 1), () => SelectedPage > 0);
            this._printCommand = new AsyncCommand(PrintAsync, CheckPrintSettingsValid);
            this._printAllCommand = new AsyncCommand(PrintAllAsync, CheckPrintSettingsValid);
            SetValidReportDefFile();
            PropertyChangedEventManager.AddHandler(this._qsStatService, new EventHandler<PropertyChangedEventArgs>((s,e)=>RaisePropertyChanged(nameof(AvailableReports))), nameof(QsStatService.Reports));
        }

        public object CurrentView { get; private set; }

        public IReadOnlyList<QsReportEntry> AvailableReports {
            get { return this._qsStatService.Reports; }
        }

        public ICommand PrintPageCommand { get { return _printCommand; } }
        public ICommand PrintAllPagesCommand { get { return _printAllCommand; } }
        public ICommand SelectPageCommand { get { return _selectPageCommand; } }
        public DelegateCommand FirstPageCommand { get; private set; }
        public DelegateCommand PreviousPageCommand { get; private set; }
        public DelegateCommand NextPageCommand { get; private set; }


        public int SelectedPage {
            get { return _selectedPage; }
            set { if (SetProperty(ref _selectedPage, value))
                {
                    UpdatePageDependentCommands();
                }
            }
        }

        public string SelectedReportDefFile
        {
            get { return _selectedReportDefFile; }
            set { if (SetProperty(ref _selectedReportDefFile, value))
                {
                    this._printCommand.RaiseCanExecuteChanged();
                    this._printAllCommand.RaiseCanExecuteChanged();
                    this._selectPageCommand.RaiseCanExecuteChanged();
                }
            }
        }

        public int? SelectedReportId
        {
            get { return _selectedReportId; }
            set { SetProperty(ref _selectedReportId, value); }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                if (SetProperty(ref _isValid, value))
                {
                    this._selectPageCommand.RaiseCanExecuteChanged();
                    this._printAllCommand.RaiseCanExecuteChanged();
                    this._printCommand.RaiseCanExecuteChanged();
                }
            }
        }

        internal void SetValidReportDefFile()
        {
            this.SelectedReportDefFile = this._configurationService.QsStatSettings.ReportFile;
            if (IsReportDefFileInvalid(this._selectedReportDefFile, AvailableReports))
                this.SelectedReportDefFile = AvailableReports.FirstOrDefault()?.FileName??String.Empty;
        }

        private static bool IsReportDefFileInvalid(string selectedReportDefFile, IReadOnlyList<QsReportEntry> availableReports)
        {
            if (String.IsNullOrWhiteSpace(selectedReportDefFile))
                return true;
            return !availableReports.Any(report => String.Equals(selectedReportDefFile, report.FileName, StringComparison.Ordinal));
        }

        internal void SetGraphicEvaluationData(SpcPath spcPath, Guid dataSetGuid, int characteristicNumber)
        {
            if (!this._cts.IsCancellationRequested)
                this._cts.Cancel();
            this._cts = new CancellationTokenSource();
            this._spcPath = spcPath;
            this._datasetGuid = dataSetGuid;
            this._selectedCharacteristicNumber = characteristicNumber;
            //the qs stat display logic mostly seems to ignore set characteristic, page matters more and for the common reports the page number equals the characteristics number
            this.SelectedPage = characteristicNumber;
            this._graphTask = Task.FromResult<byte[]>(null);
            this._qsStatRequestVM.RequestedImage = new NotifyTaskCompletion<byte[]>(this._graphTask);
            PropertyChangedEventManager.AddHandler(this._configurationService, QsStatSettingsChangedHandler, nameof(this._configurationService.QsStatSettings));
        }

        internal void RequestPage(int pageNr)
        {
            this.SelectedPage = pageNr;
            RequestSelectedPage();            
        }

        private void RequestSelectedPage()
        {
            if (String.IsNullOrWhiteSpace(this.SelectedReportDefFile))
                return;
            if (!this._graphTask.IsCompleted && !this._cts.IsCancellationRequested)
                this._cts.Cancel();
            if (this._cts.IsCancellationRequested)
                this._cts = new CancellationTokenSource();

            this._graphTask = this._qsStatService.RequestGraphicEvaluation(this._spcPath, this._datasetGuid, this.SelectedPage, this._selectedCharacteristicNumber, this.SelectedReportDefFile, this._cts.Token);
            this._qsStatRequestVM.RequestedImage = new NotifyTaskCompletion<byte[]>(this._graphTask);
        }

        private async Task PrintAsync()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(this.SelectedReportDefFile))
                    return;
                await Task.Run(async()=>await this._qsStatService.RequestPrintReport(this._spcPath, this._datasetGuid, this.SelectedPage, this.SelectedReportDefFile, default));
            }
            catch
            {
                this._messageService.ShowError((string)LanguageManager.Current.CurrentLanguageData.Translate("EPrintRequest"));
            }      
        }

        private async Task PrintAllAsync()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(this.SelectedReportDefFile))
                    return;
                await Task.Run(async () => await this._qsStatService.RequestPrintReport(this._spcPath, this._datasetGuid, null, this.SelectedReportDefFile, default));
            }
            catch
            {
                this._messageService.ShowError((string)LanguageManager.Current.CurrentLanguageData.Translate("EPrintRequest"));
            }
        }

        private bool CheckPrintSettingsValid()
        {
            return this.IsValid && this._configurationService.QsStatSettings.Printer is object && !String.IsNullOrWhiteSpace(this.SelectedReportDefFile);
        }

        private void QsStatSettingsChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            this.SelectedReportDefFile = this._configurationService.QsStatSettings.ReportFile;   
        }

        private void UpdatePageDependentCommands()
        {
            FirstPageCommand.RaiseCanExecuteChanged();
            PreviousPageCommand.RaiseCanExecuteChanged();
            NextPageCommand.RaiseCanExecuteChanged();
        }
    }
}
