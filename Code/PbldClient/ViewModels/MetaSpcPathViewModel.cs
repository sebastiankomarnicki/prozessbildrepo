﻿using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.Pbld.Utility;
using Promess.Pbld.ViewModels.SpcPathHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class MetaSpcPathViewModel:ViewModel<IMetaSpcPathView>
    {
        #region fields
        private readonly IMessageService _messageService;
        private readonly IReadOnlyList<PanelItemModel> _availablePanels;

        private readonly ExportFactory<SpcPathViewModel> _spcPathVMFactory;
        private readonly ExportFactory<SpcPathListViewModel> _spcPathListVMFactory;

        private Func<short, bool> _isSpcNumberDuplicate;

        private List<SpcPath> _spcPaths;
        private ISpcPathResult _spcPathResult;
        private object _currentView;
        private short _nextSpcNumber;
        private int _numberOfDefinedSpcPaths;

        private bool _dialogResult;
        #endregion

        [ImportingConstructor]
        public MetaSpcPathViewModel(IMetaSpcPathView view,
            IMessageService messageService, ConfigurationService configurationService,
            ExportFactory<SpcPathViewModel> spcPathVMFactory, ExportFactory<SpcPathListViewModel> spcPathListVMFactory) : base(view)
        {
            this._messageService = messageService;
            this._availablePanels = configurationService.GetPanels();
            this._spcPathVMFactory = spcPathVMFactory;
            this._spcPathListVMFactory = spcPathListVMFactory;
            this._spcPaths = new List<SpcPath>();
            this.OkCommand = new DelegateCommand(OkCommandHandler);
            this._dialogResult = false;
        }

        public bool Setup(short nextFreeSpcNumber, int numberOfDefinedSpcPaths, Func<short, bool> isSpcNumberDuplicate)
        {
            if (!_availablePanels.Any())
                return false;

            BaseSetup(nextFreeSpcNumber, numberOfDefinedSpcPaths, isSpcNumberDuplicate);
            var spcPath = GenerateSpcPath(null, null);      
            this._spcPaths.Add(spcPath);
            SetupSpcVM(spcPath);
            return true;
        }

        public void Setup(short nextFreeSpcNumber, int numberOfDefinedSpcPaths, SpcPath spcPath, Func<short, bool> isSpcNumberDuplicate)
        {
            BaseSetup(nextFreeSpcNumber, numberOfDefinedSpcPaths-1, isSpcNumberDuplicate);
            this._spcPaths.Add(spcPath);
            SetupSpcVM(spcPath);
        }

        private void BaseSetup(short nextFreeSpcNumber, int numberOfDefinedSpcPaths, Func<short, bool> isSpcNumberDuplicate)
        {
            this._isSpcNumberDuplicate = isSpcNumberDuplicate;
            this._nextSpcNumber = nextFreeSpcNumber;
            this._numberOfDefinedSpcPaths = numberOfDefinedSpcPaths;
        }

        private void SetupSpcVM(SpcPath data)
        {
            var vm = this._spcPathVMFactory.CreateExport().Value;
            vm.Initialize(data, this._isSpcNumberDuplicate, TryTransitionToSpcPathList);
            this.CurrentView = vm.View;
            this._spcPathResult = vm;
        }

        private bool TryTransitionToSpcPathList(SpcPath toModify, string root)
        {
            if (!TryAddMultipleSpcPaths(toModify, root).success)
            {
                return false;
            }
            this._spcPathResult.RemoveEventHandlers();
            SetupSpcListVM();
            return true;
        }

        internal (bool success, IReadOnlyCollection<SpcPath> added) TryAddMultipleSpcPaths(SpcPath toModify, string root)
        {
            this._spcPathResult.CancelOutstandingOperations();
            (bool success, var paths) = FileSystemHelper.GetLeafDirectories(root);
            if (!success)
            {
                this._messageService.ShowError(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("EAccessFileSystem"));
                return (false, new SpcPath[0]);
            }
            toModify.DataPath = paths.First();
            paths.RemoveAt(0);

            int pathsToAddCount = HowManyPathsCanBeAdded(paths.Count);
            if (pathsToAddCount < paths.Count)
            {
                this._messageService.ShowWarning(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("WMaxNumberSpcPathsExceeded"));
                paths = paths.Take(pathsToAddCount).ToList();
            }

            var template = this._spcPaths.First();
            IReadOnlyCollection<SpcPath> generated;
            if (paths.Count > 1) {
                generated = GenerateSpcPaths(template, paths);
                this._spcPaths.AddRange(generated);
            }
            else
            {
                generated = new SpcPath[0];
            }
                
            return (true, generated);
        }

        private void SetupSpcListVM()
        {
            var vm = this._spcPathListVMFactory.CreateExport().Value;
            vm.Setup(this, this._spcPaths.AsReadOnly(), TryTransitionToSpcPathVM);
            this._spcPathResult = vm;
            this.CurrentView = vm.View;
        }

        private void TryTransitionToSpcPathVM()
        {
            if (this._spcPaths.Count != 1)
                return;

            this._spcPathResult.RemoveEventHandlers();
            SetupSpcVM(this._spcPaths.First());
        }

        private List<SpcPath> GenerateSpcPaths(SpcPath template, IEnumerable<string> paths)
        {
            var result = new List<SpcPath>();
            short nextSpcNumber = this._nextSpcNumber;
            SpcPath tmpSpc;
            foreach(var path in paths)
            {
                tmpSpc = GenerateSpcPath(template, path);
                result.Add(tmpSpc);
            }
            return result;
        }

        internal bool RemoveSpc(SpcPath toRemove)
        {
            return this._spcPaths.Remove(toRemove);
        }

        internal (bool success, SpcPath maybeGenerated) TryGenerateAndAddSpcPath(SpcPath maybeNullTemplate)
        {
            if (!CanSpcPathBeAdded())
            {
                this._messageService.ShowWarning(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("WMaxNumberSpcPathsExceeded"));
                return (false, null);
            }
            var generated = GenerateAndAddSpcPath(maybeNullTemplate, String.Empty);

            return (true, generated);
        }

        private SpcPath GenerateAndAddSpcPath(SpcPath maybeNullTemplate, string dir)
        {
            var spcPath = GenerateSpcPath(maybeNullTemplate, dir);
            this._spcPaths.Add(spcPath);
            return spcPath;
        }

        private SpcPath GenerateSpcPath(SpcPath maybeNullTemplate, string maybeNullDir)
        {
            string panelName = maybeNullTemplate?.PanelConfigurationName ?? this._availablePanels.First().Name;
            var spcPath = new SpcPath(GetNextSpcNumber(), panelName);
            spcPath.DataPath = maybeNullDir??String.Empty;
            //TODO copy something from template, if it exists?
            return spcPath;
        }

        private short GetNextSpcNumber()
        {
            short result = this._nextSpcNumber;
            this._nextSpcNumber++;
            this._nextSpcNumber = Math.Min(this._nextSpcNumber, SpcPath.MAXSPCNUMBER);
            return result;
        }

        private int HowManyPathsCanBeAdded(int toAddCount)
        {
            int possibleToAdd = Area.MAXPATHS - (this._numberOfDefinedSpcPaths + this._spcPaths.Count);
            return Math.Min(possibleToAdd, toAddCount);
        }

        private bool CanSpcPathBeAdded()
        {
            return Area.MAXPATHS > (this._numberOfDefinedSpcPaths + this._spcPaths.Count);
        }

        public Func<short, bool> IsSpcNumberDuplicate
        {
            get { return this._isSpcNumberDuplicate; }
        }

        #region properties
        public object CurrentView
        {
            get { return _currentView; }
            private set { SetProperty(ref _currentView, value); }
        }
        public ICommand OkCommand { get; private set; }
        #endregion

        #region command handlers
        private void OkCommandHandler()
        {
            if (!IsDataValid())
            {
                _messageService.ShowWarning(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("WSpcParameterNotValid"));
                return;
            }
            this._dialogResult = true;
            ViewCore.Close();
        }
        #endregion

        internal IEnumerable<SpcPath> GetSpcPaths()
        {
            return _spcPaths;
        }

        internal bool ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
            this._spcPathResult.CancelOutstandingOperations();
            return _dialogResult;
        }

        private bool IsDataValid()
        {
            return _spcPathResult.IsValid();
        }

        internal void Close()
        {
            this._dialogResult = false;
            this._spcPathResult.CancelOutstandingOperations();
            ViewCore.Close();
        }
    }
}
