﻿using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;
using System.Windows.Data;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class AreaSelectionViewModel : ViewModel<IAreaSelectionView>
    {
        private const int _maximumNumberAreas = 255;

        private ExportFactory<TextInputViewModel> _textInputVMFactory;
        private ExportFactory<ExportAreaViewModel> _exportAreaVMFactory;

        private object _parentView;

        private Areas _areas;
        private Area _selectedArea;
        private ICollectionView _sortedAreasView;
        private DelegateCommand _addAreaCommand;
        private DelegateCommand _renameAreaCommand;
        private DelegateCommand _removeAreaCommand;
        private DelegateCommand<Area> _deSelectAreaCmd;
        private DelegateCommand _openExportAreaDialogCommand;

        [ImportingConstructor]
        public AreaSelectionViewModel(IAreaSelectionView view, IMainService mainService, 
            ExportFactory<TextInputViewModel> textInputVMFactory, ExportFactory<ExportAreaViewModel> exportAreaVMFactory) : base(view)
        {
            this._textInputVMFactory = textInputVMFactory;
            this._exportAreaVMFactory = exportAreaVMFactory;
            this._addAreaCommand = new DelegateCommand(AddAreaHandler, CanAddAreaHandler);
            this._renameAreaCommand = new DelegateCommand(RenameAreaHandler, () => SelectedArea != null);
            this._removeAreaCommand = new DelegateCommand(DeleteAreaHandler, () => SelectedArea != null);
            this._deSelectAreaCmd = new DelegateCommand<Area>(AreaDeSelectedHandler);
            this.ExportEnabled = mainService.OperatingMode == OperatingMode.leader;
            if (this.ExportEnabled)
            {
                this._openExportAreaDialogCommand = new DelegateCommand(OpenExportAreaDialogCommandHandler, () => SelectedArea != null);
            }
            else
            {
                this._openExportAreaDialogCommand = new DelegateCommand(() => { }, () => false);
            }
        }

        public void Initialize(object parentView, Areas areas)
        {
            this._parentView = parentView;
            this._areas = areas;
            SetAreasView();
            PropertyChangedEventManager.AddHandler(this, SelectedAreaChangedHandler, nameof(SelectedArea));
            CollectionChangedEventManager.AddHandler(this._areas.AvailableAreas, AvailableAreasChangedHandler);
        }

        private void SetAreasView()
        {
            var collectionView = new ListCollectionView(this._areas.AvailableAreas);
            collectionView.SortDescriptions.Add(new SortDescription(nameof(Area.Name), ListSortDirection.Ascending));
            collectionView.IsLiveSorting = true;
            this.SortedAreasView = collectionView;
        }

        internal void ChangeAreas(Areas areas)
        {
            CollectionChangedEventManager.RemoveHandler(this._areas.AvailableAreas, AvailableAreasChangedHandler);
            this._areas = areas;
            SetAreasView();
            CollectionChangedEventManager.AddHandler(this._areas.AvailableAreas, AvailableAreasChangedHandler);
        }

        #region event handlers
        private void SelectedAreaChangedHandler(object sender, PropertyChangedEventArgs args)
        {
            this._renameAreaCommand.RaiseCanExecuteChanged();
            this._removeAreaCommand.RaiseCanExecuteChanged();
            this._openExportAreaDialogCommand.RaiseCanExecuteChanged();
        }

        private void AvailableAreasChangedHandler(object sender, NotifyCollectionChangedEventArgs args)
        {
            this._addAreaCommand.RaiseCanExecuteChanged();
        }
        #endregion

        #region properties
        public Areas Areas
        {
            get { return _areas; }
            private set { SetProperty(ref _areas, value); }
        }

        /// <summary>
        /// View of <see cref="Area"/> objects.
        /// </summary>
        public ICollectionView SortedAreasView
        {
            get { return _sortedAreasView; }
            private set { SetProperty(ref _sortedAreasView, value); }
        }
        public bool ExportEnabled { get; private set; }

        public ICommand AddAreaCommand { get { return _addAreaCommand; } }
        public ICommand RenameAreaCommand { get { return _renameAreaCommand; } }
        public ICommand RemoveAreaCommand { get { return _removeAreaCommand; } }
        public ICommand DeSelectAreaCommand { get { return _deSelectAreaCmd; } }
        public ICommand OpenExportAreaDialogCommand { get { return _openExportAreaDialogCommand; } }

        public Area SelectedArea
        {
            get { return _selectedArea; }
            set { SetProperty(ref _selectedArea, value); }
        }
        #endregion

        #region helpers
        private void DeleteAreaHandler()
        {
            _areas.RemoveArea(SelectedArea);
        }

        private void AddAreaHandler()
        {
            var vm = _textInputVMFactory.CreateExport().Value;
            string title = (string)LanguageManager.Current.CurrentLanguageData.Translate("SaveAs");
            string header = (string)LanguageManager.Current.CurrentLanguageData.Translate("NewAreaName");
            vm.Initialize(title, header, TryAddArea, CheckNameValidAdd);
            EventHandler<PropertyChangedEventArgs> closeHandler = new EventHandler<PropertyChangedEventArgs>((s, e) => vm.Close());
            PropertyChangedEventManager.AddHandler(this, closeHandler, nameof(Areas));
            vm.ShowDialog(_parentView);
            PropertyChangedEventManager.RemoveHandler(this, closeHandler, nameof(Areas));
        }

        private bool CheckNameValidAdd(string newAreaName)
        {
            var trimmedName = newAreaName.Trim();
            return trimmedName.Length>0 && trimmedName.Length<=50 && !_areas.DoesAreaNameAlreadyExist(trimmedName);
        }

        private void RenameAreaHandler()
        {
            var vm = _textInputVMFactory.CreateExport().Value;
            string title = (string)LanguageManager.Current.CurrentLanguageData.Translate("Rename");
            string header = (string)LanguageManager.Current.CurrentLanguageData.Translate("RenameArea");
            EventHandler<PropertyChangedEventArgs> closeHandler = new EventHandler<PropertyChangedEventArgs>((s, e) => vm.Close());
            PropertyChangedEventManager.AddHandler(this, closeHandler, nameof(Areas));
            vm.Initialize(title, header, TryRenameArea, CheckNameValidRename);
            vm.ShowDialog(_parentView);
            PropertyChangedEventManager.RemoveHandler(this, closeHandler, nameof(Areas));
        }

        private Tuple<bool, string> TryRenameArea(string newName)
        {
            var trimmedName = newName.Trim();
            if (String.Equals(SelectedArea.Name, trimmedName,StringComparison.OrdinalIgnoreCase))
                return new Tuple<bool, string>(true, "");

            if (!_areas.TryRenameArea(SelectedArea, trimmedName))
                return new Tuple<bool, string>(false, (string)LanguageManager.Current.CurrentLanguageData.Translate("WInvalidName"));

            return new Tuple<bool, string>(true, "");
        }

        private bool CheckNameValidRename(string newName)
        {
            var trimmedName = newName.Trim();
            if (trimmedName.Length == 0)
                return false;
            return !_areas.DoesAreaNameAlreadyExist(newName);
        }
        private Tuple<bool, string> TryAddArea(string newAreaName)
        {
            var trimmedName = newAreaName.Trim();
            if (trimmedName.Length < 1)
            {
                return new Tuple<bool, string>(false, (string)LanguageManager.Current.CurrentLanguageData.Translate("WInvalidName"));
            }
            if (_areas.AvailableAreas.Count > _maximumNumberAreas)
            {
                return new Tuple<bool, string>(false, (string)LanguageManager.Current.CurrentLanguageData.Translate("WAreaLimitReached"));
            }
            var newArea = new Area(newAreaName);
            var creationResult = this._areas.TryAddArea(newArea);
            if (!creationResult)
                return new Tuple<bool, string>(false, (string)LanguageManager.Current.CurrentLanguageData.Translate("ECreateArea"));
            SelectedArea = newArea;
            return new Tuple<bool, string>(true, "");
        }

        private bool CanAddAreaHandler()
        {
            return _areas.AvailableAreas.Count <= _maximumNumberAreas;
        }

        private void AreaDeSelectedHandler(Area area)
        {
            if (area == null)
                return;
            if (area.Equals(this._areas.SelectedArea))
            {
                this._areas.SelectArea(null);
            }else
            {
                this._areas.SelectArea(area);
            }
        }

        private void OpenExportAreaDialogCommandHandler()
        {
            var vm = _exportAreaVMFactory.CreateExport().Value;
            vm.Initialize(_selectedArea);
            vm.ShowDialog(_parentView);
        }
        #endregion
    }
}
