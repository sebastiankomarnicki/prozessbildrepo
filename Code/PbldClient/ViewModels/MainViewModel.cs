﻿using Promess.Common.Services;
using Promess.Common.Util;
using Promess.Common.Util.Async;
using Promess.Language;
using Promess.Pbld.Data;
using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.IViews;
using Promess.Pbld.Services;
using Promess.Pbld.Utility;
using Promess.QsStat.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Waf.Applications.Services;
using System.Windows;
using System.Windows.Input;

namespace Promess.Pbld.ViewModels
{
    [Export]
    public class MainViewModel : ViewModel<IMainView>
    {
        #region fields
        private readonly IMessageService _messageService;
        private readonly ConfigurationService _configurationService;
        private readonly IFileDialogServiceExtended _fileDialogService;
        private readonly QsStatService _qsStatService;
        private readonly ModifiedConfigurationManagerPbld _configurationManager;
        private readonly SelectedDataCacheService _selectedDataCacheService;
        private readonly PathsAndResourceService _pathsAndResourceService;

        private readonly ExportFactory<EditGraphicProjectViewModel> _editGraphicProjectViewModel;
        private readonly ExportFactory<GraphicPageViewModel> _graphicPageVMFactory;
        private readonly ExportFactory<MetaEditPanelViewModel> _metaPanelEditorVMFactory;
        private readonly ExportFactory<MetaPanelViewModel> _metaPanelVMFactory;
        private readonly ExportFactory<MetaSchemaEditorViewModel> _metaSchemaEditorVMFactory;
        private readonly ExportFactory<MetaSchemaViewModel> _metaSchemaVMFactory;
        private readonly ExportFactory<OptionsViewModel> _optionsVMFactory;
        private readonly ExportFactory<CreateAreasViewModel> _createAreasVMFactory;
        private readonly ExportFactory<SetAreaViewModel> _setAreaVMFactory;
        private readonly ExportFactory<ListViewModel> _listVMFactory;
        private readonly ExportFactory<ConfigQsStatisticViewModel> _configQsStatisticVMFactory;
        private readonly ExportFactory<UserPasswordViewModel> _userPasswordVMFactory;
        private readonly ExportFactory<PresetMetaViewModel> _presetMetaVMFactory;
        private readonly ExportFactory<TextInputViewModel> _textInputVMFactory;
        private readonly ExportFactory<AboutViewModel> _aboutVMFactory;

        private object _currentView;
        private bool _menuVisible;
        private SubViewType _lastSubViewType;
        private ReadOnlyCollection<GraphicItemModel> _availableGraphicPages;
        private GraphicItemModel _lastGraphicPage;
        #endregion

        #region events
        public event EventHandler Closing;
        public event EventHandler Unloading;
        #endregion

        [ImportingConstructor]
        public MainViewModel(IMainView view, IMessageService messageService, IFileDialogServiceExtended fileDialogService,
            MainService mainService, ConfigurationService configurationService, PathsAndResourceService pathsAndResourceService,
            QsStatService qsStatService, SelectedDataCacheService selectedDataCacheService, ModifiedConfigurationManagerPbld configurationManager,
            ExportFactory<MetaEditPanelViewModel> metaPanelEditorVMFactory, ExportFactory<MetaPanelViewModel> metaPanelVMFactory,
            ExportFactory<MetaSchemaEditorViewModel> metaSchemaEditorVMFactory, ExportFactory<MetaSchemaViewModel> metaSchemaVMFactory,
            ExportFactory<OptionsViewModel> optionsVMFactory, ExportFactory<CreateAreasViewModel> createAreasVMFactory,
            ExportFactory<SetAreaViewModel> setAreaVMFactory, ExportFactory<ListViewModel> listVMFactory,
            ExportFactory<ConfigQsStatisticViewModel> configQsStatisticVMFactory, ExportFactory<EditGraphicProjectViewModel> editGraphicProjectViewModel,
            ExportFactory<GraphicPageViewModel> graphicPageVMFactory, ExportFactory<UserPasswordViewModel> userPasswordVMFactory,
            ExportFactory<PresetMetaViewModel> presetMetaVMFactory, ExportFactory<TextInputViewModel> textInputVMFactory,
            ExportFactory<AboutViewModel> aboutVMFactory) : base(view)
        {
            this._messageService = messageService;
            this._fileDialogService = fileDialogService;
            this._configurationService = configurationService;
            this._pathsAndResourceService = pathsAndResourceService;
            this._qsStatService = qsStatService;
            this._selectedDataCacheService = selectedDataCacheService;
            this._configurationManager = configurationManager;

            this.MainService = mainService;

            this._metaPanelEditorVMFactory = metaPanelEditorVMFactory;
            this._metaPanelVMFactory = metaPanelVMFactory;
            this._metaSchemaEditorVMFactory = metaSchemaEditorVMFactory;
            this._metaSchemaVMFactory = metaSchemaVMFactory;
            this._optionsVMFactory = optionsVMFactory;
            this._createAreasVMFactory = createAreasVMFactory;
            this._setAreaVMFactory = setAreaVMFactory;
            this._listVMFactory = listVMFactory;
            this._configQsStatisticVMFactory = configQsStatisticVMFactory;
            this._editGraphicProjectViewModel = editGraphicProjectViewModel;
            this._graphicPageVMFactory = graphicPageVMFactory;
            this._userPasswordVMFactory = userPasswordVMFactory;
            this._presetMetaVMFactory = presetMetaVMFactory;
            this._textInputVMFactory = textInputVMFactory;
            this._aboutVMFactory = aboutVMFactory;

            ShowPanelEditorCommand = new DelegateCommand(ShowPanelEditorCommandHandler, () => MainService.IsAuthenticated);
            ShowPanelCommand = new DelegateCommand(ShowPanelCommandHandler, ()=> LastSubViewType != SubViewType.Panel);
            ShowListCommand = new DelegateCommand<SpcPath>(spcPath=>ShowListCommandHandler(spcPath), ignore => LastSubViewType != SubViewType.List);
            ShowMetaSchemaEditorCommand = new DelegateCommand(ShowMetaSchemaEditorCommandHandler, () => this.MainService.WindowState == WindowState.Maximized && MainService.IsAuthenticated);
            ShowOverviewCommand = new DelegateCommand(ShowOverviewCommandHandler, () => LastSubViewType != SubViewType.Overview);
            ShowQsConfigCommand = new AsyncCommand(ShowQsConfigCommandHandlerAsync, () => MainService.IsAuthenticated);
            ShowAboutCommand = new DelegateCommand(ShowAboutCommandHandler);

            ShowEditProjectCommand = new DelegateCommand(ShowEditProjectCommandHandler, ()=> MainService.IsAuthenticated && AvailableGraphicPages != null);
            ShowLoadProjectCommand = new DelegateCommand(ShowLoadProjectCommandHandler);
            ShowGraphicPage = new DelegateCommand<GraphicItemModel>(ShowGraphicPageCommandHandler);
            ShowOptionsCommand = new DelegateCommand(ShowOptionsCommandHandler, ()=> MainService.IsAuthenticated);
            ShowDefineAreasCommand = new DelegateCommand(ShowDefineAreasCommandHandler, ()=>MainService.IsAuthenticated);
            ShowSetSelectedAreaCommand = new DelegateCommand(ShowSetSelecedAreaCommandHandler);
            ShowPresetMetaGraphicsCommand = new DelegateCommand(ShowPresetMetaGraphicsCommandHandler, () => MainService.IsAuthenticated);
            CloseCommand = new DelegateCommand(() => ViewCore.Close());

            ModuleChangeCommand = new AsyncCommand<int>(async (module, _) => await ChangeModuleCommandHandlerAsync(module));

            LoadGraphicProject();
            PropertyChangedEventManager.AddHandler(LanguageManager.Current, LanguageChanged, nameof(LanguageManager.CurrentLanguage));
            PropertyChangedEventManager.AddHandler(this._qsStatService, ModulesChanged, nameof(QsStatService.AvailableModules));
            PropertyChangedEventManager.AddHandler(this._qsStatService, SelectedModuleChanged, nameof(QsStatService.SelectedModule));
            PropertyChangedEventManager.AddHandler(this._configurationService, LoadGraphicProject, nameof(Services.ConfigurationService.GraphicProjectWrapper));
            PropertyChangedEventManager.AddHandler(this.MainService, IsAuthenticatedChanged, nameof(IMainService.IsAuthenticated));
            PropertyChangedEventManager.AddHandler(this, AvailableGraphicPagesChanged, nameof(AvailableGraphicPages));
            PropertyChangedEventManager.AddHandler(this.MainService, WindowStateChanged, nameof(IMainService.WindowState));

            WeakEventManager<QsStatService, EventArgs>.AddHandler(this._qsStatService, nameof(QsStatService.ModuleInvalidEvent), QsModuleInvalidHandler);
            WeakEventManager<QsStatService, EventArgs>.AddHandler(this._qsStatService, nameof(QsStatService.QsSettingsChangedEvent), QsStatServiceSettingsChangedHandler);
            WeakEventManager<QsStatService, EventArgs>.AddHandler(this._qsStatService, nameof(QsStatService.UserPasswordInvalidEvent), QsStatServiceLoginInvalidHandler);

            view.Closing += ViewClosing;
            this._menuVisible = true;
        }

        #region properties
        public MainService MainService { get; private set; }

        public IConfigurationService ConfigurationService { get { return _configurationService; } }

        public QsStatService QsStatService { get { return _qsStatService; } }

        public DelegateCommand ShowPanelEditorCommand { get; private set; }
        public DelegateCommand ShowPanelCommand { get; private set; }
        public DelegateCommand<SpcPath> ShowListCommand { get; private set; }
        public DelegateCommand ShowOverviewCommand { get; private set; }
        public DelegateCommand ShowOptionsCommand { get; private set; }
        public DelegateCommand ShowDefineAreasCommand { get; private set; }
        public DelegateCommand ShowSetSelectedAreaCommand { get; private set; }
        public AsyncCommand ShowQsConfigCommand { get; private set; }
        public DelegateCommand ShowEditProjectCommand { get; private set; }
        public DelegateCommand ShowMetaSchemaEditorCommand { get; private set; }
        public DelegateCommand ShowLoadProjectCommand { get; private set; }
        public DelegateCommand<GraphicItemModel> ShowGraphicPage { get; private set; }
        public ICommand ShowAboutCommand { get; private set; }

        public DelegateCommand ShowPresetMetaGraphicsCommand { get; private set; }


        public object CurrentView
        {
            get { return _currentView; }
            private set
            {
                if (!Object.Equals(_currentView, value))
                {
                    if (Unloading != null)
                    {
                        Unloading(this, null);
                        Unloading = null;
                    }
                    SetProperty(ref _currentView, value);
                }
            }
        }

        public bool MenuVisible
        {
            get { return _menuVisible; }
            private set
            {
                SetProperty(ref _menuVisible, value);
            }
        }

        public GraphicItemModel LastGraphicPage
        {
            get { return _lastGraphicPage; }
            private set { SetProperty(ref _lastGraphicPage, value); }
        }

        public ReadOnlyCollection<GraphicItemModel> AvailableGraphicPages
        {
            get { return _availableGraphicPages; }
            private set { SetProperty(ref _availableGraphicPages, value); }
        }
        #endregion

        #region Language
        public DelegateCommand<CultureInfo> LanguageChangeCommand
        {
            get; internal set;
        }

        public IEnumerable<CultureInfo> SupportedLanguageCodes
        {
            get { return LanguageManager.Current.Languages; }
        }

        public CultureInfo CurrentLanguage
        {
            get { return LanguageManager.Current.CurrentLanguage; }
        }

        public ICommand CloseCommand { get; private set; }

        private void LanguageChanged(object sender, PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(CurrentLanguage));
            RaisePropertyChanged(nameof(QsStatService));
        }
        #endregion Language

        #region qs stat module
        public AsyncCommand<int> ModuleChangeCommand
        {
            get; internal set;
        }

        public IReadOnlyDictionary<int, string> AvailableModules
        {
            get { return _qsStatService.AvailableModules; }
        }

        public int? SelectedModule
        {
            get { return _qsStatService.SelectedModule; }
        }

        private void SelectedModuleChanged(object sender, PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(SelectedModule));
        }

        private void ModulesChanged(object sender, PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(AvailableModules));
        }
        #endregion

        public void Show()
        {
            ViewCore.Show();
        }

        public void Close()
        {
            ViewCore.Close();
        }

        #region helper logic
        private SubViewType LastSubViewType
        {
            get { return _lastSubViewType; }
            set { if(SetProperty(ref _lastSubViewType, value))
                {
                    ShowPanelCommand.RaiseCanExecuteChanged();
                    ShowListCommand.RaiseCanExecuteChanged();
                    ShowOverviewCommand.RaiseCanExecuteChanged();
                }
            }
        }

        private void ShowPanelEditorCommandHandler()
        {
            var vm = _metaPanelEditorVMFactory.CreateExport().Value;
            if (!vm.Initialize(View))
            {
                _messageService.ShowError(View, (string) LanguageManager.Current.CurrentLanguageData.Translate("ELoadPanel"));
                return;
            }
            MenuVisible = false;
            LastSubViewType = SubViewType.Other;
            CurrentView = vm.View;
            //TODO open last view? lastsubviewtype must not be changed in that case
            var handler = new EventHandler((sender, e) => { vm.Close(); MenuVisible = true; });
            vm.CloseForParentCommand = new DelegateCommand(() => {
                CurrentView = null;
                Unloading -= handler;               
            });

            Unloading += handler;
        }

        private void ShowMetaSchemaEditorCommandHandler()
        {
            var vm = _metaSchemaEditorVMFactory.CreateExport().Value;
            MenuVisible = false;
            LastSubViewType = SubViewType.Other;
            CurrentView = vm.View;
            vm.Setup(View);
            var handler = new EventHandler((s, e) => { vm.Close(); MenuVisible = true; });
            vm.CloseForParentCommand = new DelegateCommand(() => {
                CurrentView = null;
                Unloading -= handler;
                ShowOverviewCommandHandler();
            });

            Unloading += handler;
        }

        private void ShowEditProjectCommandHandler()
        {
            var vm = _editGraphicProjectViewModel.CreateExport().Value;
            var projectWrapper = _configurationService.GraphicProjectWrapper;
            vm.Initialize(View, projectWrapper.ProjectPath, SerializeHelper.CreateDataContractCopy(projectWrapper.MaybeGraphicProject));
            MenuVisible = false;
            LastSubViewType = SubViewType.Other;
            CurrentView = vm.View;
            //TODO open last view? lastsubviewtype must not be changed in that case. Or load the first graphics page if any?
            var handler = new EventHandler((sender, e) => { vm.Close(); MenuVisible = true; });
            vm.CloseForParentCommand = new DelegateCommand(() => {
                CurrentView = null;
                Unloading -= handler;
                GraphicProject maybeProject=null;
                try
                {
                    if (!String.IsNullOrWhiteSpace(projectWrapper.ProjectPath))
                    {
                        maybeProject = SerializeHelper.DeserializeFromFile<GraphicProject>(projectWrapper.ProjectPath);
                        _configurationService.GraphicProjectWrapper = new GraphicProjectWrapper(projectWrapper.ProjectPath, maybeProject);
                    }
                }
                catch
                {
                    string message = (string)LanguageManager.Current.CurrentLanguageData.Translate("EReloadGraphicsProject");
                    this._messageService.ShowError(this.View, message);
                }
            });

            Unloading += handler;
        }

        private void ShowLoadProjectCommandHandler()
        {
            var ft = new FileType("gprj", "*.gprj");
            string defaultFileOrDirectory;
            if (!String.IsNullOrWhiteSpace(this._configurationService.GraphicProjectWrapper.ProjectPath))
            {
                defaultFileOrDirectory = this._configurationService.GraphicProjectWrapper.ProjectPath;
            }
            else
            {
                defaultFileOrDirectory = this._pathsAndResourceService.GraphicProjectPath;
            }
            var result = this._fileDialogService.ShowOpenFileDialog(this.View, ft, defaultFileOrDirectory, !MainService.IsAuthenticated);
            if (!result.Any())
                return;
            var filename = result.First().Trim();
            GraphicProject loaded;
            try
            {
                if (File.Exists(filename))
                {
                    loaded = SerializeHelper.DeserializeFromFile<GraphicProject>(filename, true);
                }
                else
                {
                    loaded = new GraphicProject();
                    SerializeHelper.SerializeToFile(filename, loaded);
                }
                var pageSettings = this._configurationManager.GetSectionAndAddIfMissing<GraphicPageSettings>(Constants.GraphicProject);
                pageSettings.ProjectPath = filename;
                this._configurationManager.Save(ConfigurationSaveMode.Modified);
                this._configurationService.GraphicProjectWrapper = new GraphicProjectWrapper(filename, loaded);
            }
            catch
            {
                string message = (string)LanguageManager.Current.CurrentLanguageData.Translate("ELoadingFile");
                this._messageService.ShowError(this.View, message);
            }
        }

        private void ShowPanelCommandHandler()
        {
            var vm = _metaPanelVMFactory.CreateExport().Value;

            vm.Initialize(View, ShowListCommand);
            CurrentView = vm.View;
            LastSubViewType = SubViewType.Panel;
        }

        private void ShowOverviewCommandHandler()
        {
            var vm = _metaSchemaVMFactory.CreateExport().Value;
            vm.Initialize(View, ShowListCommand, ShowMetaSchemaEditorCommand);
            CurrentView = vm.View;
            LastSubViewType = SubViewType.Overview;
        }

        private void ShowListCommandHandler(SpcPath preselectedSpcPath=null)
        {
            if (preselectedSpcPath is object)
                this._selectedDataCacheService.ListViewDataSelection.Select(preselectedSpcPath);
            var vm = _listVMFactory.CreateExport().Value;
            vm.Initialize(View);
            CurrentView = vm.View;
            LastSubViewType = SubViewType.List;
        }

        private void ShowOptionsCommandHandler()
        {
            var vm = _optionsVMFactory.CreateExport().Value;
            vm.Initialize();
            if (vm.ShowDialog(View))
            {             
                _configurationService.GeneralSettings = vm.GeneralSettings;
                _configurationService.EvaluationAndColorSettings = vm.EvalAndColorSettings;
                _qsStatService.SetSettings(_configurationService.GeneralSettings, _configurationService.EvaluationAndColorSettings);
                if (MainService.OperatingMode == OperatingMode.follower)
                {
                    _configurationService.TransferFollowerSettings = vm.TransferFollowerSettings;
                }
                
            }
        }

        private void ShowDefineAreasCommandHandler()
        {
            var vm = _createAreasVMFactory.CreateExport().Value;
            vm.Setup();
            if (vm.ShowDialog(View))
            {
                if (_configurationService.TrySetAreas(vm.Areas))
                {
                    _qsStatService.SetArea(vm.Areas.SelectedArea);
                }
                else
                {
                    this._messageService.ShowError(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("ESetAreasFailed"));
                }
            }
        }

        private void ShowAboutCommandHandler()
        {
            var vm = _aboutVMFactory.CreateExport().Value;
            vm.Setup(this._qsStatService.QCOMVersion?.VersionString);
            vm.ShowDialog(View);
        }

        private void ShowSetSelecedAreaCommandHandler()
        {
            var vm = _setAreaVMFactory.CreateExport().Value;
            vm.Setup();
            if (vm.ShowDialog(View))
            {
                var areas = vm.GetAreas();
                if (_configurationService.TrySetAreas(areas))
                {
                    _qsStatService.SetArea(areas.SelectedArea);
                }
                else
                {
                    this._messageService.ShowError(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("ESetAreasFailed"));
                }
            }
        }

        private void ShowPresetMetaGraphicsCommandHandler()
        {
            var vm = _presetMetaVMFactory.CreateExport().Value;
            vm.Setup();
            if (vm.ShowDialog(View))
            {
                if (!this._configurationService.TrySetPresetGraphicSettings(vm.PresetsGraphic))
                {
                    this._messageService.ShowError(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("ESetPresetGraphicFailed"));
                }
            }
        }

        private async Task ShowQsConfigCommandHandlerAsync()
        {
            var vm = _configQsStatisticVMFactory.CreateExport().Value;
            var strategies = _qsStatService.Strategies;
            var reports = _qsStatService.Reports;
            var printers = _qsStatService.Printers;
            var languages = _qsStatService.Languages;
            var acceptableQsSettings = _configurationManager.GetSectionAndAddIfMissingAsAcceptableSettings<QsStatSettings>(Constants.QsStat);
            var qsSettings = acceptableQsSettings.Settings;
            int? selectedPrinter = null;
            if (qsSettings.Printer!= null && printers.ContainsKey(qsSettings.Printer.Item1) && String.Equals(qsSettings.Printer.Item2, printers[qsSettings.Printer.Item1], StringComparison.OrdinalIgnoreCase))
            {
                selectedPrinter = qsSettings.Printer.Item1;
            }
            vm.Initialize(strategies, qsSettings.Strategy, reports, qsSettings.ReportFile, printers, selectedPrinter, languages, qsSettings.Language);
            EventHandler<PropertyChangedEventArgs> closeHandler = new EventHandler<PropertyChangedEventArgs>((s, e) => vm.Close());
            PropertyChangedEventManager.AddHandler(this._configurationService, closeHandler, nameof(this._configurationService.QsStatSettings));
            if (vm.ShowDialog(View))
            {
                PropertyChangedEventManager.RemoveHandler(this._configurationService, closeHandler, nameof(this._configurationService.QsStatSettings));
                qsSettings.Strategy = vm.SelectedEvalStrategy;
                qsSettings.ReportFile = vm.SelectedReport?.FileName;
                if (vm.SelectedPrinter.HasValue)
                    qsSettings.Printer = new Tuple<int, string>(vm.SelectedPrinter.Value, printers[vm.SelectedPrinter.Value]);
                else
                    qsSettings.Printer = null;
                qsSettings.Language = vm.SelectedLanguage;
                acceptableQsSettings.AcceptChanges();
                
                this._configurationService.QsStatSettings = acceptableQsSettings.Settings;
                await Task.Run(async()=>await this._qsStatService.SetQsParametersAsync(acceptableQsSettings.Settings));
            }
            else
            {
                PropertyChangedEventManager.RemoveHandler(this._configurationService, closeHandler, nameof(this._configurationService.QsStatSettings));
            }            
        }

        private void ShowGraphicPageCommandHandler(GraphicItemModel graphicPage)
        {
            if (graphicPage.Equals(LastGraphicPage) && LastSubViewType == SubViewType.Graphic)
                return;
       
            var vm = this._graphicPageVMFactory.CreateExport().Value;
            var itemsCopy = SerializeHelper.CreateDataContractCopy(graphicPage.Items);
            vm.Initialize(View, itemsCopy, graphicPage.AspectRatio);
            LastGraphicPage = graphicPage;
            CurrentView = vm.View;
            LastSubViewType = SubViewType.Graphic;
        }

        private async Task ChangeModuleCommandHandlerAsync(int module)
        {
            var acceptableQsSettings = _configurationManager.GetSectionAndAddIfMissingAsAcceptableSettings<QsStatSettings>(Constants.QsStat);
            acceptableQsSettings.Settings.Module = module;
            bool settingsAccepted = await Task.Run(async () => await this._qsStatService.SetQsParametersAsync(acceptableQsSettings.Settings));
            if (!settingsAccepted)
            {
                acceptableQsSettings.AcceptChanges();
                this._configurationService.QsStatSettings = acceptableQsSettings.Settings;
            }
        }

        private void LoadGraphicProject(object sender, PropertyChangedEventArgs e)
        {
            LoadGraphicProject();
        }

        private void LoadGraphicProject()
        {
            var project = _configurationService.GraphicProjectWrapper.MaybeGraphicProject;
            if (project == null)
            {
                AvailableGraphicPages = null;
                LastGraphicPage = null;
            }
            else
            {
                AvailableGraphicPages = new ReadOnlyCollection<GraphicItemModel>(project.GraphicPages);
                if (LastSubViewType != SubViewType.Graphic)
                {
                    LastGraphicPage = AvailableGraphicPages.FirstOrDefault();
                }
                else
                {
                    LastGraphicPage = null;
                    if (AvailableGraphicPages.Any())
                    {
                        ShowGraphicPageCommandHandler(AvailableGraphicPages.First());
                    }
                }                
            }
        }

        private void ViewClosing(object sender, CancelEventArgs e)
        {
            CurrentView = null;
            Closing?.Invoke(this,e);
        }

        private void IsAuthenticatedChanged(object sender, PropertyChangedEventArgs e)
        {
            ShowQsConfigCommand.RaiseCanExecuteChanged();
            ShowDefineAreasCommand.RaiseCanExecuteChanged();
            ShowEditProjectCommand.RaiseCanExecuteChanged();
            ShowOptionsCommand.RaiseCanExecuteChanged();
            ShowPanelEditorCommand.RaiseCanExecuteChanged();
            ShowPresetMetaGraphicsCommand.RaiseCanExecuteChanged();
            ShowMetaSchemaEditorCommand.RaiseCanExecuteChanged();
        }

        private void AvailableGraphicPagesChanged(object sender, PropertyChangedEventArgs e)
        {
            ShowEditProjectCommand.RaiseCanExecuteChanged();
        }

        private void WindowStateChanged(object sender, PropertyChangedEventArgs e)
        {
            ShowMetaSchemaEditorCommand.RaiseCanExecuteChanged();
        }
        #endregion

        #region qsstatservice event handlers
        private async void QsStatServiceLoginInvalidHandler(object sender, EventArgs e)
        {
            if (Application.Current.CheckAccess())
            {
               await QsStatServiceLoginInvalidHandlerImplAsync();
            }
            else
            {
                await Application.Current.Dispatcher.Invoke(QsStatServiceLoginInvalidHandlerImplAsync);
            }
        }

        private async Task QsStatServiceLoginInvalidHandlerImplAsync()
        {
            var retryVm = this._userPasswordVMFactory.CreateExport().Value;
            retryVm.Setup((string)LanguageManager.Current.CurrentLanguageData.Translate("QsStatLogin"));
            if (retryVm.ShowDialog(this.View))
            {
                var acceptableQsSettings = _configurationManager.GetSectionAndAddIfMissingAsAcceptableSettings<QsStatSettings>(Constants.QsStat);
                acceptableQsSettings.Settings.User = retryVm.Username;
                acceptableQsSettings.Settings.Password = retryVm.Password;
                if (!await this._qsStatService.SetQsParametersAsync(acceptableQsSettings.Settings))
                {
                    acceptableQsSettings.AcceptChanges();
                    this._configurationService.QsStatSettings = acceptableQsSettings.Settings;
                }
            }
        }

        private async void QsStatServiceSettingsChangedHandler(object sender, EventArgs e)
        {
            if (Application.Current.CheckAccess())
            {
                await QsStatServiceSettingsChangedHandlerImplAsync();
            }
            else
            {
                await Application.Current.Dispatcher.Invoke(QsStatServiceSettingsChangedHandlerImplAsync);
            }
        }

        private async Task QsStatServiceSettingsChangedHandlerImplAsync()
        {
            var acceptableQsSettings = this._configurationManager.GetSectionAndAddIfMissingAsAcceptableSettings<QsStatSettings>(Constants.QsStat);
            await this._qsStatService.UpdateQsSettings(acceptableQsSettings.Settings);
            acceptableQsSettings.AcceptChanges();
            this._configurationService.QsStatSettings = acceptableQsSettings.Settings;
        }

        private void QsModuleInvalidHandler(object sender, EventArgs e)
        {
            if (Application.Current.CheckAccess())
            {
                QsModuleInvalidHandlerImpl();
            }
            else
            {
                Application.Current.Dispatcher.Invoke(QsModuleInvalidHandlerImpl);
            }
        }

        private void QsModuleInvalidHandlerImpl()
        {
            this._messageService.ShowError(View, (string)LanguageManager.Current.CurrentLanguageData.Translate("EModuleError"));
        }
        #endregion

        private enum SubViewType
        {
            Other,
            Panel,
            List,
            Graphic,
            Overview
        }
    }
}
