﻿using Promess.Pbld.IViews;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Waf.Applications;
using System.Windows.Input;
using Promess.Common.Services;
using Promess.Pbld.Services;
using Promess.Common.Util;
using System.ComponentModel;
using Promess.Pbld.Data;
using System.Waf.Applications.Services;
using System.Diagnostics;
using Promess.Pbld.ViewModels.SchemaViewModelHelpers;
using Promess.Pbld.Data.Schema;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Editor;
using System.Windows;

namespace Promess.Pbld.ViewModels
{
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class EditSchemaViewModel : BaseEditorViewModel<IEditSchemaView>
    {
        #region fields
        private readonly IFileDialogServiceExtended _fileDialogService;

        private readonly ExportFactory<PropertiesViewModel<ISchemaItemPropertiesView>> _propertiesSchemaItemVMFactory;
        private readonly ExportFactory<SchemaEditorItemViewModel> _editSchemaItemVMFactory;
        private readonly PathsAndResourceService _pathsAndResourceService;

        private bool _areaIsDirty;
        private object _parentView;
        #endregion

        [ImportingConstructor]
        public EditSchemaViewModel(IEditSchemaView view,
            IconLookupService iconLookupService, ConfigurationService configurationService,
            IFileDialogServiceExtended fileDialogService, IMessageService messageService, PathsAndResourceService pathsAndResourceService,
            ExportFactory<PropertiesViewModel<ISchemaItemPropertiesView>> propertiesSchemaItemVMFactory, ExportFactory<SchemaEditorItemViewModel> editSchemaItemVMFactory) : base(view, messageService)
        {
            this.IconLookupService = iconLookupService;
            this.ConfigurationService = configurationService;
            this._fileDialogService = fileDialogService;
            this._pathsAndResourceService = pathsAndResourceService;
            this._propertiesSchemaItemVMFactory = propertiesSchemaItemVMFactory;
            this._editSchemaItemVMFactory = editSchemaItemVMFactory;
            this.SelectAspectRatioCommand = new DelegateCommand<AspectRatio>(SelectAspectRatioCommandHandler);
        }

        internal void Initialize(object parentView, Action saveChanges, Area currentArea)
        {
            this._parentView = parentView;
            this.SaveChangesCommand = new DelegateCommand(saveChanges);
            this.ToggleShowErrorCounterCommand = new DelegateCommand(ToggleShowErrorCounterCommandHandler);
            this.ToggleShowIconCommand = new DelegateCommand(ToggleShowIconCommandHandler);
            this.EqualizeSizeCommand = new DelegateCommand<EditorSchemaMetaItem>(reference => EqualizeSizeCommandHandler(reference));
            this.EquializeSizeToSelectedCommand = new DelegateCommand(EquializeSizeToSelectedCommandHandler, () => this._selected.Count == 1);
            this.SelectBackgroundImageCommand = new DelegateCommand(SelectBackgroundImageCommandHandler);
            this.RemoveBackgroundImageCommand = new DelegateCommand(RemoveBackgroundImageCommandHandler);
            this.ToggleShowImageCommand = new DelegateCommand(ToggleShowImageCommandHandler);
            this.ShowSchemaItemPropertiesCommand = new DelegateCommand<EditorSchemaMetaItem>(ShowSchemaItemPropertiesCommandHandler);
            SetAreaAndGenerateItems(currentArea);
            DummyOverview = new SimpleProcessStateOverview(0, 0, 0);
        }

        internal void SetAreaAndGenerateItems(Area area)
        {
            if (this.CurrentArea is object)
                WeakEventManager<Area, PropertyChangedEventArgs>.RemoveHandler(this.CurrentArea, nameof(Area.PropertyChanged), HandleAreaPropertiesChanged);
            WeakEventManager<Area, PropertyChangedEventArgs>.AddHandler(area, nameof(Area.PropertyChanged), HandleAreaPropertiesChanged);
            this._areaIsDirty = false;
            IList<EditorItemModelBase> editorItems = GenerateEditorItems(area);
            base.Initialize(editorItems);
            this.CurrentArea = area;
        }

        private void HandleAreaPropertiesChanged(object sender, PropertyChangedEventArgs e)
        {
            this._areaIsDirty = true;
        }

        private IList<EditorItemModelBase> GenerateEditorItems(Area area)
        {
            SchemaItem schemaItem = null;
            var result = new List<EditorItemModelBase>();
            EditorSchemaMetaItem editorItem;
            foreach (var spcPath in area.ReadonlyPaths)
            {
                if (!area.SpcGuidSchemaItem.TryGetValue(spcPath.Guid, out schemaItem))
                {
                    schemaItem = new SchemaItem() { PercentageX = this._rnd.Next(100) * 0.002, PercentageY = this._rnd.Next(100) * 0.002, PercentageWidth = 0.04, PercentageHeight = 0.02 };
                    schemaItem.IsDirty = true;
                    area.SpcGuidSchemaItem[spcPath.Guid] = schemaItem;
                }
                editorItem = new EditorSchemaMetaItem(this, spcPath, schemaItem, (item) => EditSchemaEditorItemSettings(item, spcPath));
                PropertyChangedEventManager.AddHandler(editorItem, IsSelectedChanged, nameof(EditorItemModelBase.IsSelected));
                result.Add(editorItem);
            }
            return result;
        }

        internal bool GetIsDirty()
        {
            if (CurrentArea is null)
                return false;
            bool isDirty = this._areaIsDirty;
            isDirty |= ContainedElements.Any(elem => elem.Item.IsDirty);
            return isDirty;
        }

        private void CopyOriginalSizes(SchemaItem source, SchemaItem destination)
        {
            base.CopyOriginalSizes(source, destination);
            destination.Width = source.Width;
            destination.Height = source.Height;
        }

        public DelegateCommand ToggleShowErrorCounterCommand
        {
            get; private set;
        }

        public DelegateCommand ToggleShowIconCommand
        {
            get; private set;
        }

        public DelegateCommand SelectBackgroundImageCommand
        {
            get; private set;
        }

        public DelegateCommand RemoveBackgroundImageCommand
        {
            get; private set;
        }

        public ICommand SaveChangesCommand
        {
            get;private set;
        }

        public DelegateCommand<AspectRatio> SelectAspectRatioCommand
        {
            get; private set;
        }

        public ConfigurationService ConfigurationService
        {
            get; private set;
        }

        public IconLookupService IconLookupService
        {
            get; private set;
        }

        public SimpleProcessStateOverview DummyOverview
        {
            get;private set;
        }

        public DelegateCommand<EditorSchemaMetaItem> EqualizeSizeCommand
        {
            get; private set;
        }

        public DelegateCommand EquializeSizeToSelectedCommand
        {
            get; private set;
        }

        public DelegateCommand<EditorSchemaMetaItem> LoadImageCommand
        {
            get; private set;
        }

        public DelegateCommand<EditorSchemaMetaItem> ShowSchemaItemPropertiesCommand
        {
            get; private set;
        }
        public ICommand ToggleShowImageCommand
        {
            get; private set;
        }

        public Area CurrentArea
        {
            get; private set;
        }

        #region command handlers
        private void ToggleShowErrorCounterCommandHandler()
        {
            CurrentArea.ShowErrorCounter = !CurrentArea.ShowErrorCounter;
        }

        private void ToggleShowIconCommandHandler()
        {
            CurrentArea.ShowIcon = !CurrentArea.ShowIcon;
        }

        private void EquializeSizeToSelectedCommandHandler()
        {
            EqualizeSizeHelper((EditorSchemaMetaItem)this._selected.First());
        }

        private void EqualizeSizeCommandHandler(EditorSchemaMetaItem reference)
        {
            EqualizeSizeHelper(reference);
        }

        private void EqualizeSizeHelper(EditorSchemaMetaItem reference)
        {
            Debug.Assert(reference != null);
            SchemaItem referenceItem = (SchemaItem)reference.Item;
            SchemaItem item;
            foreach (EditorSchemaMetaItem elem in ContainedElements)
            {
                if (elem.Equals(reference))
                    continue;
                item = (SchemaItem)elem.Item;
                item.Width = referenceItem.Width;
                item.Height = referenceItem.Height;
                if (item.X + item.Width > ViewWidth)
                    item.X = ViewWidth - item.Width;
                if (item.Y + item.Height > ViewHeight)
                    item.Y = ViewHeight - item.Height;
            }
        }

        private void SelectBackgroundImageCommandHandler()
        {
            FileType ft = new FileType("jpg, png, bmp", new string[] { "*.jpg", "*.png", "*.bmp" });
            var baseDirOrFile = String.IsNullOrWhiteSpace(CurrentArea.SchemaFilePath) ? this._pathsAndResourceService.PicturePath : CurrentArea.SchemaFilePath;
            var result = _fileDialogService.ShowOpenFileDialog(this._parentView ?? View, ft, CurrentArea.SchemaFilePath, true);
            if (!result.Any())
                return;
            var filename = result.FirstOrDefault();
            if (!String.IsNullOrWhiteSpace(filename))
            {
                CurrentArea.SchemaFilePath = filename.Trim();
            }
            else
            {
                CurrentArea.SchemaFilePath = String.Empty;
            }
        }

        private void RemoveBackgroundImageCommandHandler()
        {
            CurrentArea.SchemaFilePath = String.Empty;
        }

        private void ToggleShowImageCommandHandler()
        {
            CurrentArea.ShowImage = !CurrentArea.ShowImage;
        }

        private void ShowSchemaItemPropertiesCommandHandler(EditorSchemaMetaItem editorItem)
        {
            editorItem.ShowSettingsCommand.Execute(null);    
        }

        private void SelectAspectRatioCommandHandler(AspectRatio aspectRatio)
        {
            this.CurrentArea.AspectRatio = aspectRatio;
        }
        #endregion

        #region methods from abstract class
        protected override void RecalculateActualSizes(EditorItemBase elem)
        {
            bool oldIsDirty = elem.IsDirty;
            elem.X = elem.PercentageX * ViewWidth;
            elem.Y = elem.PercentageY * ViewHeight;
            switch (elem)
            {
                case RectangleEditorItem rectangle:
                    rectangle.Width = rectangle.PercentageWidth * ViewWidth;
                    rectangle.Height = rectangle.PercentageHeight * ViewHeight;
                    break;
                default:
                    throw new NotImplementedException($"{elem.GetType()} is not supported in {nameof(RecalculateActualSizes)}.");
            }
            elem.IsDirty = oldIsDirty;
        }

        protected override void SavePercentage(EditorItemBase elem)
        {
            elem.PercentageX = elem.X / LastRecalculateSize.Width;
            elem.PercentageY = elem.Y / LastRecalculateSize.Height;
            switch (elem)
            {
                case SchemaItem schemaItem:
                    schemaItem.PercentageWidth = schemaItem.Width / LastRecalculateSize.Width;
                    schemaItem.PercentageHeight = schemaItem.Height / LastRecalculateSize.Height;
                    break;
                default:
                    throw new NotImplementedException($"{elem.GetType()} is not supported in {nameof(SavePercentage)}.");
            }
            elem.IsDirty = false;
        }

        protected SchemaItem EditSchemaEditorItemSettings(SchemaItem item, SpcPath spcPath)
        {
            var propVM = this._propertiesSchemaItemVMFactory.CreateExport().Value;
            var vm = this._editSchemaItemVMFactory.CreateExport().Value;
            var schemaItemCopy = SerializeHelper.CreateDataContractCopy(item);
            CopyOriginalSizes(item, schemaItemCopy);
            vm.Setup(propVM.View, schemaItemCopy);
            propVM.Setup(vm);
            if (!propVM.ShowDialog(this._parentView))
                return item;
            this.CurrentArea.SpcGuidSchemaItem[spcPath.Guid] = schemaItemCopy;
            return schemaItemCopy;
        }

        protected override void SelectedCheckCommandsRaiseCanExecuteChanged()
        {
            base.SelectedCheckCommandsRaiseCanExecuteChanged();
            EquializeSizeToSelectedCommand.RaiseCanExecuteChanged();
        }
        #endregion
    }
}
