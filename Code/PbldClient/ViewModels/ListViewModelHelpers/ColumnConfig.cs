﻿using System.Collections.Generic;

namespace Promess.Pbld.ViewModels.ListViewModelHelpers
{
    public class ColumnConfig
    {
        public IEnumerable<Column> Columns { get; set; }
    }
}
