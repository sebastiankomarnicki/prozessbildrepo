﻿using Promess.Pbld.Converters;
using Promess.Pbld.Data.Editor;
using Promess.QsStat.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace Promess.Pbld.ViewModels.ListViewModelHelpers
{
    public class SpcQdasColumnsToDynamicGridViewConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return Binding.DoNothing;
            var config = value as ICollection<QdasNumberWrapper>;
            if (config == null)
                return Binding.DoNothing;
            var dictConverter = new DictionaryIntStringConverter();

            var gridView = new GridView();
            gridView.AllowsColumnReorder = false;
            foreach (var column in config)
            {
                Binding binding;
                switch (column.QdasNumber.EvaluationType)
                {
                    case TextEvaluationType.Part:
                        binding = new Binding($"Item2.{nameof(QsStatPartResultDTO.PartTextResults)}") {
                            Converter = dictConverter,
                            ConverterParameter = column.QdasNumber.EvaluationNumber
                        };
                        break;
                    default:
                        continue;
                }

                var gridColumn = new GridViewColumn { Header = column, DisplayMemberBinding = binding };
                gridView.Columns.Add(gridColumn);
            }
            return gridView;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
