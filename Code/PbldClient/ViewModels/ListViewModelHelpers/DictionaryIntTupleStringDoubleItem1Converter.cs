﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace Promess.Pbld.ViewModels.ListViewModelHelpers
{
    public class DictionaryIntTupleStringDoubleItem1Converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var dict = value as IReadOnlyDictionary<int, Tuple<string, double>>;
            var key = parameter as int?;
            if (dict == null || !key.HasValue)
                return Binding.DoNothing;
            Tuple<string, double> result;
            if (dict.TryGetValue(key.Value, out result))
                return result.Item1;
            else
                return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
