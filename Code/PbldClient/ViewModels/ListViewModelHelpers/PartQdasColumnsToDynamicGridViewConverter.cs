﻿using Promess.Pbld.Converters;
using Promess.Pbld.Data.Editor;
using Promess.QsStat.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace Promess.Pbld.ViewModels.ListViewModelHelpers
{
    public class PartQdasColumnsToDynamicGridViewConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return Binding.DoNothing;
            var config = value as List<QdasNumberWrapper>;
            if (config == null)
                return Binding.DoNothing;
            var dictConverter = new DictionaryIntStringConverter();
            var dictTupleConverter = new DictionaryIntTupleStringDoubleItem1Converter();
            var gridView = new GridView();
            gridView.AllowsColumnReorder = false;
            foreach (var column in config)
            {
                Binding binding;
                switch (column.QdasNumber.EvaluationType)
                {
                    case TextEvaluationType.Characteristic:
                        binding = new Binding($"{nameof(QsStatCharacteristicResultDTO.CharacteristicTextResults)}")
                        {
                            Converter = dictConverter,
                            ConverterParameter = column.QdasNumber.EvaluationNumber
                        };
                        break;
                    case TextEvaluationType.NumericAndTextResults:
                        binding = new Binding($"{nameof(QsStatCharacteristicResultDTO.NumericAndTextResults)}")
                        {
                            Converter = dictTupleConverter,
                            ConverterParameter = column.QdasNumber.EvaluationNumber
                        };
                        break;
                    default:
                        continue;
                }

                var gridColumn = new GridViewColumn { Header = column, DisplayMemberBinding = binding };
                gridView.Columns.Add(gridColumn);
            }
            return gridView;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
