﻿using Promess.Pbld.Data.Measurements;
using System;
using System.Collections.Generic;
using System.Waf.Foundation;

namespace Promess.Pbld.ViewModels.ListViewModelHelpers
{
    public class PartNodeData : Model
    {
        private ProcessState _processState;
        private IReadOnlyCollection<string> _labels;
        private bool _isExpanded;
        private bool _isSelected;

        public PartNodeData(Guid partReferenceGuid, IReadOnlyCollection<string> labels, IReadOnlyCollection<CharacteristicNodeData> characteristicNodes, ProcessState processState)
        {
            this.PartReferenceGuid = partReferenceGuid;
            this._labels = labels;
            this.CharacteristicNodes = characteristicNodes;
            this._processState = processState;
            MissingLabelValues = false;
        }

        public Guid PartReferenceGuid { get; }

        public IReadOnlyCollection<CharacteristicNodeData> CharacteristicNodes { get; }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { SetProperty(ref this._isExpanded, value); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref this._isSelected, value); }
        }

        public ProcessState ProcessState
        {
            get { return _processState; }
            internal set { SetProperty(ref _processState, value); }
        }

        public IReadOnlyCollection<string> Labels {
            get { return _labels; }
            internal set { SetProperty(ref _labels, value); }
        }

        public bool MissingLabelValues { get; set; }
    }
}
