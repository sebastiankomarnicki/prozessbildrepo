﻿using Promess.Pbld.Data;
using Promess.Pbld.Data.Measurements;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Waf.Foundation;
using Microsoft.VisualStudio.Language.Intellisense;

namespace Promess.Pbld.ViewModels.ListViewModelHelpers
{
    public class SpcPathNodeData:Model
    {
        private ProcessState _processState;
        private BulkObservableCollection<PartNodeData> _data;
        private bool _isExpanded;
        private bool _isSelected;

        public SpcPathNodeData(SpcPath spcPath, IEnumerable<PartNodeData> data, ProcessState processState)
        {
            this.SpcPath = spcPath;
            this._data = new BulkObservableCollection<PartNodeData>();
            this._data.AddRange(data);
            this._processState = processState;
        }

        public SpcPath SpcPath { get; }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { SetProperty(ref this._isExpanded, value); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref this._isSelected, value); }
        }

        public ProcessState ProcessState
        {
            get { return _processState; }
            internal set { SetProperty(ref _processState, value); }
        }

        public ObservableCollection<PartNodeData> PartNodeData {
            get { return _data; }
        }

        internal void BeginPartNodeDataBulkUpdate()
        {
            _data.BeginBulkOperation();
        }

        internal void EndPartNodeDataBulkUpdate()
        {
            _data.EndBulkOperation();
        }
    }
}
