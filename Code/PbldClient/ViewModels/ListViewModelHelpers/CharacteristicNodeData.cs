﻿using Promess.Pbld.Data.Measurements;
using System.Collections.Generic;
using System.Waf.Foundation;

namespace Promess.Pbld.ViewModels.ListViewModelHelpers
{
    public class CharacteristicNodeData:Model
    {
        private ProcessState _processState;
        private IReadOnlyCollection<string> _labels;
        private bool _isExpanded;
        private bool _isSelected;

        public CharacteristicNodeData(int characteristicIndex, IReadOnlyCollection<string> labels, ProcessState processState)
        {
            this.CharacteristicIndex = characteristicIndex;
            this._processState = processState;
            this._labels = labels;
            this.MissingLabelValues = false;
        }

        public bool IsExpanded
        {
            get { return _isExpanded; }
            set { SetProperty(ref this._isExpanded, value); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref this._isSelected, value); }
        }

        public int CharacteristicIndex { get; }

        public IReadOnlyCollection<string> Labels {
            get { return _labels; }
            internal set { SetProperty(ref _labels, value); }
        }

        public ProcessState ProcessState
        {
            get { return _processState; }
            internal set { SetProperty(ref _processState, value); }
        }

        public bool MissingLabelValues { get; set; }
    }
}
