﻿namespace Promess.Pbld.ViewModels.ListViewModelHelpers
{
    public class Column
    {
        public string Header { get; set; }
        public int PartKNumber { get; set; }
    }
}
