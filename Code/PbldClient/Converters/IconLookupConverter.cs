﻿using Promess.Pbld.Data.Settings;
using Promess.Pbld.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Retrieve an <see cref="ImageSource"/> for an <see cref="IconElement"/> using an <see cref="IconLookupService"/>
    /// </summary>
    [ValueConversion(typeof(object[]), typeof(ImageSource))]
    public class IconLookupConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            IconElement elem = values[0] as IconElement;
            IconLookupService ils = values[1] as IconLookupService;
            if (elem == null || ils == null)
                return Binding.DoNothing;
            Tuple<bool, ImageSource> result = ils.TryGetImageSource(elem);
            if (!result.Item1)
                return Binding.DoNothing;
            else
                return result.Item2;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
