﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Convert a percentage (between 0 and 1) to a star sized <see cref="GridLength"/>.
    /// </summary>
    [ValueConversion(typeof(double), typeof(GridLength))]
    public class PercentageToGridLengthStarConverter : IValueConverter
    {
        /// <summary>
        /// Do the conversion, provide parameter to get 1-percentage as star size
        /// </summary>
        /// <param name="value">Percentage</param>
        /// <param name="targetType">Ignored</param>
        /// <param name="parameter">Use 1-percentage if true</param>
        /// <param name="culture">Ignored</param>
        /// <returns><see cref="GridLength"/> representation</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || parameter == DependencyProperty.UnsetValue)
                return DependencyProperty.UnsetValue;
            double? percentage = value as double?;
            bool invert = parameter as bool? ?? false;

            if (!percentage.HasValue || percentage<0 || percentage>1)
                return Binding.DoNothing;

            double starValue = invert?1-percentage.Value: percentage.Value;
            return new GridLength(starValue, GridUnitType.Star);
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
