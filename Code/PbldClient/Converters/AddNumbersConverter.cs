﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to sum multiple values. Casting of the values to <see cref="double"/> must be possible.
    /// </summary>
    [ValueConversion(typeof(Object[]), typeof(double))]
    public class AddNumbersConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                return Binding.DoNothing;
            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            double sum = 0;
            try
            {
                foreach (object toAdd in values)
                {
                    sum += (double)toAdd;
                }
            }
            catch
            {
                return Binding.DoNothing;
            }
            
            return sum;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
