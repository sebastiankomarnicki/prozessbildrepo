﻿using Promess.Pbld.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// One way converter to provide language lookup key for <see cref="GridPointDensity"/>
    /// </summary>
    [ValueConversion(typeof(GridPointDensity), typeof(string))]
    public class GridPointDensityToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            GridPointDensity? density = value as GridPointDensity?;
            if (!density.HasValue)
                return Binding.DoNothing;

            switch (density.Value)
            {
                case GridPointDensity.Sparse:
                    return "GridPointDensitySparse";
                case GridPointDensity.Medium:
                    return "GridPointDensityMedium";
                case GridPointDensity.Dense:
                    return "GridPointDensityDense";
                default:
                    throw new NotImplementedException($"Case {density.Value} for {nameof(GridPointDensity)} not implemented for {nameof(GridPointDensityToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
