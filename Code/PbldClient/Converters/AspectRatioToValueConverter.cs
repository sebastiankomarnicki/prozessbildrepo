﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter for providing the associated numerical value of an entry of <see cref="AspectRatio"/>.
    /// </summary>
    [ValueConversion(typeof(AspectRatio), typeof(double))]
    public class AspectRatioToValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            AspectRatio? original = value as AspectRatio?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case AspectRatio.None:
                    return 0;
                case AspectRatio.FourToThree:
                    return 4d/3;
                case AspectRatio.SixteenToTen:
                    return 1.6;
                case AspectRatio.SixteenToNine:
                    return 16d/9;
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(AspectRatio)} not implemented for {nameof(AspectRatioToValueConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
