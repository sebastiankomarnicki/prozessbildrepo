﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="TextEvaluationType"/>
    /// </summary>
    [ValueConversion(typeof(TextEvaluationType), typeof(string))]
    public class TextEvaluationTypeToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            TextEvaluationType? type = value as TextEvaluationType?;
            if (!type.HasValue)
                return Binding.DoNothing;

            switch (type.Value)
            {
                case TextEvaluationType.Part:
                    return "Part";
                case TextEvaluationType.Characteristic:
                    return "Characteristic";
                case TextEvaluationType.NumericAndTextResults:
                    return "NumericAndTextResults";
                case TextEvaluationType.AdditionalPart:
                    return "AdditionalPartResults";
                case TextEvaluationType.AdditionalCharacteristic:
                    return "AdditionalCharacteristicResults";
                default:
                    throw new NotImplementedException($"Case {type.Value} for {nameof(ButtonFunction)} not implemented for {nameof(TextEvaluationType)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
