﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Two way conveter for <see cref="Color"/> and <see cref="SolidColorBrush"/>
    /// </summary>
    [ValueConversion(typeof(Color), typeof(SolidColorBrush))]
    public class ColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            Color? original = value as Color?;
            if (!original.HasValue)
                return Binding.DoNothing;
            return new SolidColorBrush(original.Value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SolidColorBrush brush = value as SolidColorBrush;
            if (brush == null)
                return Binding.DoNothing;
            return brush.Color;
        }
    }
}
