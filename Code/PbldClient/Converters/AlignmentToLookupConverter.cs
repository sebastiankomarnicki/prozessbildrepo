﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="HorizontalAlignment"/>
    /// </summary>
    [ValueConversion(typeof(HorizontalAlignment), typeof(String))]
    public class AlignmentToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == System.Windows.DependencyProperty.UnsetValue)
                return value;

            HorizontalAlignment? original = value as HorizontalAlignment?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case HorizontalAlignment.Left:
                    return "Left";
                case HorizontalAlignment.Center:
                    return "Centered";
                case HorizontalAlignment.Right:
                    return "Right";
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(HorizontalAlignment)} not implemented for {nameof(AlignmentToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
