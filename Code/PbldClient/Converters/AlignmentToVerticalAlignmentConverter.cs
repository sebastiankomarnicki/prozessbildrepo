﻿using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows;

namespace Promess.Pbld.Converters
{
    [ValueConversion(typeof(Data.Editor.VerticalAlignment), typeof(System.Windows.VerticalAlignment))]
    public class AlignmentToVerticalAlignmentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            Data.Editor.VerticalAlignment? original = value as Data.Editor.VerticalAlignment?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case Data.Editor.VerticalAlignment.Bottom:
                    return System.Windows.VerticalAlignment.Bottom;
                case Data.Editor.VerticalAlignment.Middle:
                    return System.Windows.VerticalAlignment.Center;
                case Data.Editor.VerticalAlignment.Top:
                    return System.Windows.VerticalAlignment.Top;
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(Data.Editor.VerticalAlignment)} not implemented for {nameof(AlignmentToVerticalAlignmentConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
