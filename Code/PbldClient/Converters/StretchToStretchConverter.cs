﻿using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows;
using System.Windows.Media;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to convert between abstracted stretch (<see cref="Data.Editor.Stretch"/>) to concrete stretch (<see cref="Stretch"/>) for UI
    /// </summary>
    [ValueConversion(typeof(Data.Editor.Stretch), typeof(Stretch))]
    public class StretchToStretchConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            Data.Editor.Stretch? original = value as Data.Editor.Stretch?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case Data.Editor.Stretch.None:
                    return Stretch.None;
                case Data.Editor.Stretch.Uniform:
                    return Stretch.Uniform;
                case Data.Editor.Stretch.UniformToFill:
                    return Stretch.UniformToFill;
                case Data.Editor.Stretch.Fill:
                    return Stretch.Fill;
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(Data.Editor.Stretch)} not implemented for {nameof(Converters.StretchToStretchConverter)}.");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            Stretch? original = value as Stretch?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case Stretch.None:
                    return Data.Editor.Stretch.None;
                case Stretch.Uniform:
                    return Data.Editor.Stretch.Uniform;
                case Stretch.UniformToFill:
                    return Data.Editor.Stretch.UniformToFill;
                case Stretch.Fill:
                    return Data.Editor.Stretch.Fill;
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(Stretch)} not implemented for {nameof(Converters.StretchToStretchConverter)}.");
            }
        }
    }
}