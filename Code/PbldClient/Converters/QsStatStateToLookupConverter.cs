﻿using Promess.QsStat.Data;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="QsStatState"/>
    /// </summary>
    [ValueConversion(typeof(QsStatState), typeof(String))]
    public class QsStatStateToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            QsStatState? state = value as QsStatState?;
            if (!state.HasValue)
                return Binding.DoNothing;
            switch (state.Value)
            {
                case QsStatState.Starting:
                    return "QCOMStarting";
                case QsStatState.RetrievingConnectionParameters:
                    return "QCOMRetrievingParams";
                case QsStatState.Connecting:
                    return "QCOMConnecting";
                case QsStatState.RetrievingMetadata:
                    return "QCOMRetrievingMeta";
                case QsStatState.Started:
                    return "QCOMRunning";
                case QsStatState.MissingSettings:
                    return "QCOMMissingSettings";
                case QsStatState.ShuttingDown:
                    return "QCOMShuttingDown";
                case QsStatState.Shutdown:
                    return "QCOMShutdown";
                case QsStatState.Faulted:
                    return "QCOMFaulted";
                default:
                    throw new NotImplementedException($"Case {state.Value} for {nameof(QsStatState)} not implemented for {nameof(QsStatStateToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
