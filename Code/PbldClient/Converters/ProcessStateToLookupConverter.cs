﻿using Promess.Pbld.Data.Measurements;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="ProcessState"/>
    /// </summary>
    [ValueConversion(typeof(ProcessState), typeof(string))]
    public class ProcessStateToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            ProcessState? state = value as ProcessState?;
            if (!state.HasValue)
                return Binding.DoNothing;

            switch (state.Value)
            {
                case ProcessState.Ok:
                    return "ProcessStateOk";
                case ProcessState.Endangered:
                    return "ProcessStateEndangered";
                case ProcessState.OutOfControl:
                    return "ProcessStateOoC";
                case ProcessState.StabilityViolation:
                    return "ProcessStateSV";
                case ProcessState.ActionLimitViolationXq:
                    return "ProcessStateALVXq";
                case ProcessState.ActionLimitViolationS:
                    return "ProcessStateALVS";
                case ProcessState.DeficientPart:
                    return "ProcessStateDeficientPart";
                case ProcessState.TooFewValues:
                    return "ProcessStateTooFewValues";
                default:
                    throw new NotImplementedException($"Case {state.Value} for {nameof(ProcessState)} not implemented for {nameof(ProcessStateToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
