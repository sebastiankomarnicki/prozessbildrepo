﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="LineWidth"/>
    /// </summary>
    [ValueConversion(typeof(LineWidth), typeof(String))]
    public class LineWidthToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            LineWidth? original = value as LineWidth?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case LineWidth.Thin:
                    return "LineWidthThin";
                case LineWidth.Medium:
                    return "LineWidthMedium";
                case LineWidth.Thick:
                    return "LineWidthThick";
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(LineWidth)} not implemented for {nameof(LineWidthToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
