﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide a <see cref="GridLength"/> for two data bound values which are multiplied. Intended for a percentage and reference value as input.
    /// </summary>
    public class PercentageSizeToGridLengthMVConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            if (values.Length < 2)
                return Binding.DoNothing;
            double? maybePercentage = values[0] as double?;
            double? maybeActualSize = values[1] as double?;
            if (!maybePercentage.HasValue || !maybeActualSize.HasValue)
                return Binding.DoNothing;
            return new GridLength(maybePercentage.Value * maybeActualSize.Value);
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
