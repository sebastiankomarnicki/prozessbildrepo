﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="BorderType"/>
    /// </summary>
    [ValueConversion(typeof(BorderType), typeof(String))]
    public class BorderTypeToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            BorderType? original = value as BorderType?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case BorderType.Flat:
                    return "Flat";
                case BorderType.Lowered:
                    return "Lowered";
                case BorderType.Raised:
                    return "Raised";
                case BorderType.Borderless:
                    return "Borderless";
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(BorderType)} not implemented for {nameof(BorderTypeToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
