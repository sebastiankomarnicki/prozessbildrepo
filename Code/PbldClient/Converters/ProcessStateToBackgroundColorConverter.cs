﻿using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide the background color for a <see cref="ProcessState"/> given <see cref="EvaluationAndColorSettings"/> 
    /// </summary>
    [ValueConversion(typeof(object[]), typeof(Color))]
    public class ProcessStateToBackgroundColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            ProcessState? ps = values[0] as ProcessState?;
            EvaluationAndColorSettings settings = values[1] as EvaluationAndColorSettings;
            if (!ps.HasValue || settings == null)
                return Binding.DoNothing;

            switch (ps.Value)
            {
                case ProcessState.ActionLimitViolationS:
                    return settings.ActionLimitViolationS.BackgroundColor;
                case ProcessState.ActionLimitViolationXq:
                    return settings.ActionLimitViolationXq.BackgroundColor;
                case ProcessState.DeficientPart:
                    return settings.DeficientPart.BackgroundColor;
                case ProcessState.Endangered:
                    return settings.ProcessEndangered.BackgroundColor;
                case ProcessState.Ok:
                    return settings.ProcessOk.BackgroundColor;
                case ProcessState.OutOfControl:
                    return settings.ProcessOoC.BackgroundColor;
                case ProcessState.StabilityViolation:
                    return settings.StabilityViolation.BackgroundColor;
                case ProcessState.TooFewValues:
                    return settings.NoOrTooFewValues.BackgroundColor;
                default:
                    throw new ArgumentException($"Invalid {typeof(ProcessState)} value: {ps.Value}");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
