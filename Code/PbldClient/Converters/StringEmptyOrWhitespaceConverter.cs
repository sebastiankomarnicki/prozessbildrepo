﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Forward call to <see cref="String.IsNullOrWhiteSpace(string)"/>
    /// </summary>
    [ValueConversion(typeof(string), typeof(bool))]
    public class StringEmptyOrWhitespaceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            string original = value as string;
            return String.IsNullOrWhiteSpace(original);
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
