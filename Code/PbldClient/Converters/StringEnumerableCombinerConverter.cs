﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Flatten given text elements by joining them with defined symbol
    /// </summary>
    [ValueConversion(typeof(object[]), typeof(string))]
    public class StringEnumerableCombinerConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            IEnumerable<string> toFlatten = values[0] as IEnumerable<string>;
            if (toFlatten == null)
                return Binding.DoNothing;
            string concatSymbol = values[1] as string ?? String.Empty;
            return String.Join(concatSymbol, toFlatten);
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
