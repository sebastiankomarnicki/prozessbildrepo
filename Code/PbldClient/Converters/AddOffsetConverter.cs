﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Add a fixed value as offset to the value to convert. Intended for use with calculated minimum width or height on load with a <see cref="UserControl"/> because the value is sometimes to small.
    /// </summary>
    [ValueConversion(typeof(double), typeof(double))]
    public class AddOffsetConverter : IValueConverter
    {
        /// <summary>
        /// Offset to add
        /// </summary>
        public const double OFFSET = 20;

        /// <summary>
        /// Add offset to value.
        /// </summary>
        /// <param name="value">Base value</param>
        /// <param name="targetType">Ignored</param>
        /// <param name="parameter">Ignored</param>
        /// <param name="culture">Ignored</param>
        /// <returns>Base value plus offset</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return DependencyProperty.UnsetValue;

            double? baseValue = value as double?;
            if (!baseValue.HasValue || Double.IsNaN(baseValue.Value))
                return Binding.DoNothing;

            return baseValue.Value + OFFSET;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
