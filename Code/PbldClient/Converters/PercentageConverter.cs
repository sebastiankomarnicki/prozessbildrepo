﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter two multiply two data bound values. Intended for converting percentages of something with a reference value to an actual value.
    /// </summary>
    public class PercentageConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            if (values.Length != 2)
                return Binding.DoNothing;
            double? percentage = values[0] as double?;
            double? referenceValue = values[1] as double?;
            if (!percentage.HasValue || !referenceValue.HasValue)
                return Binding.DoNothing;
            return percentage.Value * referenceValue.Value;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
