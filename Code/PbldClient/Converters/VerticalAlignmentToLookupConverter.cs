﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="VerticalAlignment"/>
    /// </summary>
    [ValueConversion(typeof(VerticalAlignment), typeof(String))]
    public class VerticalAlignmentToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == System.Windows.DependencyProperty.UnsetValue)
                return value;

            VerticalAlignment? original = value as VerticalAlignment?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case VerticalAlignment.Bottom:
                    return "Bottom";
                case VerticalAlignment.Middle:
                    return "Middle";
                case VerticalAlignment.Top:
                    return "Top";
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(VerticalAlignment)} not implemented for {nameof(VerticalAlignmentToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
