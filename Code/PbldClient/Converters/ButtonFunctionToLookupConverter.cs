﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="ButtonFunction"/>
    /// </summary>
    [ValueConversion(typeof(ButtonFunction), typeof(string))]
    public class ButtonFunctionToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            ButtonFunction? func = value as ButtonFunction?;
            if (!func.HasValue)
                return Binding.DoNothing;

            switch (func.Value)
            {
                case ButtonFunction.NextCharacteristic:
                    return "NextCharacteristic";
                case ButtonFunction.PreviousCharacteristic:
                    return "PreviousCharacteristic";
                case ButtonFunction.NextDataset:
                    return "NextDataSet";
                case ButtonFunction.PreviousDataset:
                    return "PreviousDataSet";
                case ButtonFunction.Refresh:
                    return "Refresh";
                default:
                    throw new NotImplementedException($"Case {func.Value} for {nameof(ButtonFunction)} not implemented for {nameof(ButtonFunctionToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
