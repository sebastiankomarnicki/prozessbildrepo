﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// General converter to retrieve a value from a <see cref="Dictionary{int, string}"/> by providing a static key
    /// </summary>
    public class DictionaryIntStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            var dict = value as IReadOnlyDictionary<int, string>;
            var key = parameter as int?;
            if (dict == null || !key.HasValue)
                return Binding.DoNothing;
            string result;
            if (dict.TryGetValue(key.Value, out result))
                return result;
            else
                return Binding.DoNothing;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
