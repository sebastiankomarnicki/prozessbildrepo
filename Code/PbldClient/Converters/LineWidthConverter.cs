﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide a value for the GUI for <see cref="LineWidth"/>
    /// </summary>
    [ValueConversion(typeof(LineWidth), typeof(double))]
    public class LineWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            LineWidth? original = value as LineWidth?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case LineWidth.Thin:
                    return 1;
                case LineWidth.Medium:
                    return 3;
                case LineWidth.Thick:
                    return 5;
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(LineWidth)} not implemented for {nameof(LineWidthConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
