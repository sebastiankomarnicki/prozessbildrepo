﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter for providing the associated numerical value of an entry of <see cref="AspectRatio"/>.
    /// </summary>
    [ValueConversion(typeof(AspectRatio), typeof(double))]
    public class AspectRatioToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            AspectRatio? original = value as AspectRatio?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case AspectRatio.None:
                    return "AspectRatioNone";
                case AspectRatio.FourToThree:
                    return "AspectRatioFourToThree";
                case AspectRatio.SixteenToTen:
                    return "AspectRatioSixteenToTen";
                case AspectRatio.SixteenToNine:
                    return "AspectRatioSixteenToNine";
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(AspectRatio)} not implemented for {nameof(AspectRatioToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
