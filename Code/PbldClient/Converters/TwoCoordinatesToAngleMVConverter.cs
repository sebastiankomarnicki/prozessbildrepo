﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Convert two coordinates (line) to an angle to a horizontal plance. For applying a rotation to the elements with respect to the line.
    /// </summary>
    public class TwoCoordinatesToAngleMVConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length<4)
                return Binding.DoNothing;
            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;
            double? x1 = values[0] as double?;
            double? y1 = values[1] as double?;
            double? x2 = values[2] as double?;
            double? y2 = values[3] as double?;
            if (!x1.HasValue || !y1.HasValue || !x2.HasValue || !y2.HasValue)
                return Binding.DoNothing;
            double a = y2.Value - y1.Value;
            double b = x2.Value - x1.Value;
            return Math.Atan2(a, b) * 180 / Math.PI;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
