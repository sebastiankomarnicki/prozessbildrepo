﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide language lookup key for <see cref="LineEnding"/>
    /// </summary>
    [ValueConversion(typeof(LineEnding), typeof(String))]
    public class LineEndingToLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            LineEnding? original = value as LineEnding?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case LineEnding.None:
                    return "LineEndingNone";
                case LineEnding.Arrow:
                    return "LineEndingArrow";
                case LineEnding.Circle:
                    return "LineEndingCircle";
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(LineEnding)} not implemented for {nameof(LineEndingToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
