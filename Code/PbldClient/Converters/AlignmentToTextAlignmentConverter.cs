﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    [ValueConversion(typeof(Data.Editor.HorizontalAlignment), typeof(TextAlignment))]
    public class AlignmentToTextAlignmentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            Data.Editor.HorizontalAlignment? original = value as Data.Editor.HorizontalAlignment?;
            if (!original.HasValue)
                return Binding.DoNothing;
            switch (original.Value)
            {
                case Data.Editor.HorizontalAlignment.Left:
                    return TextAlignment.Left;
                case Data.Editor.HorizontalAlignment.Center:
                    return TextAlignment.Center;
                case Data.Editor.HorizontalAlignment.Right:
                    return TextAlignment.Right;
                default:
                    throw new NotImplementedException($"Case {original.Value} for {nameof(Data.Editor.HorizontalAlignment)} not implemented for {nameof(Converters.AlignmentToTextAlignmentConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
