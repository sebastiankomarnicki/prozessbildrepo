﻿using Promess.Pbld.Data.Editor;
using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Convert a <see cref="QdasNumber"/> to its textual representation.
    /// </summary>
    [ValueConversion(typeof(QdasNumber), typeof(string))]
    public class QdasNumberConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            QdasNumber original = value as QdasNumber;
            if (original == null)
                return Binding.DoNothing;
            string prefix;
            switch (original.EvaluationType)
            {
                case TextEvaluationType.NumericAndTextResults:
                    prefix = "R";
                    break;
                case TextEvaluationType.Part:
                case TextEvaluationType.Characteristic:
                    prefix = "K";
                    break;
                case TextEvaluationType.AdditionalPart:
                    prefix = "Y";
                    break;
                case TextEvaluationType.AdditionalCharacteristic:
                    prefix = "Z";
                    break;
                default:
                    throw new NotImplementedException($"{nameof(QdasNumberConverter)} does not implement a conversion for value {original.EvaluationType} of {nameof(TextEvaluationType)}.");
            }
            return $"{prefix} - {original.EvaluationNumber}";
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
