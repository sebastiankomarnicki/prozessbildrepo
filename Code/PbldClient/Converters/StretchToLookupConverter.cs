﻿using Promess.Pbld.Data.Editor;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// One way converter to provide language lookup key for <see cref="Stretch"/>
    /// </summary>
    [ValueConversion(typeof(Stretch), typeof(string))]
    public class StretchToLookupConverter:IValueConverter 
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            Stretch? stretch = value as Stretch?;
            if (!stretch.HasValue)
                return Binding.DoNothing;

            switch (stretch.Value)
            {
                case Stretch.None:
                    return "StretchNone";
                case Stretch.Uniform:
                    return "StretchUniform";
                case Stretch.UniformToFill:
                    return "StretchUniformToFill";
                case Stretch.Fill:
                    return "StretchFill";
                default:
                    throw new NotImplementedException($"Case {stretch.Value} for {nameof(Stretch)} not implemented for {nameof(StretchToLookupConverter)}.");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
