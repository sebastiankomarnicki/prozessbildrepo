﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Convert a path to an image to a <see cref="BitmapImage"/>
    /// </summary>
    [ValueConversion(typeof(string),typeof(BitmapImage))]
    public class ImagePathToBitmapConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            string path = value as string;
            if (String.IsNullOrWhiteSpace(path))
                return null;
            var uri = new Uri(path);
            var bitmap = new BitmapImage();
            try
            {
                bitmap.BeginInit();
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.UriSource = uri;
                bitmap.EndInit();
            }catch
            {
                //swallow exception if file unavailable
                return null;
            }
            return bitmap;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
