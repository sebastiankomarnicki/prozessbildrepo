﻿using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide the <see cref="IconElement"/> for a <see cref="ProcessState"/> given <see cref="EvaluationAndColorSettings"/> and <see cref="IconLookupConverter"/>
    /// </summary>
    public class ProcessStateToIconElementConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length!=3)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            ProcessState? ps = values[0] as ProcessState?;
            EvaluationAndColorSettings settings = values[1] as EvaluationAndColorSettings;
            IconLookupService ils = values[2] as IconLookupService;
            if (!ps.HasValue || settings == null || ils == null)
                return Binding.DoNothing;
            EvaluationAndColorElement element;
            switch (ps.Value)
            {
                case ProcessState.ActionLimitViolationS:
                    element = settings.ActionLimitViolationS;
                    break;
                case ProcessState.ActionLimitViolationXq:
                    element = settings.ActionLimitViolationXq;
                    break;
                case ProcessState.DeficientPart:
                    element = settings.DeficientPart;
                    break;
                case ProcessState.Endangered:
                    element = settings.ProcessEndangered;
                    break;
                case ProcessState.Ok:
                    element = settings.ProcessOk;
                    break;
                case ProcessState.OutOfControl:
                    element = settings.ProcessOoC;
                    break;
                case ProcessState.StabilityViolation:
                    element = settings.StabilityViolation;
                    break;
                case ProcessState.TooFewValues:
                    element = settings.NoOrTooFewValues;
                    break;
                default:
                    throw new ArgumentException($"Invalid {typeof(ProcessState)} value: {ps.Value}");
            }
            var maybeFound = ils.TryGetImageSource(element.IconElement);
            if (maybeFound.Item1)
                return maybeFound.Item2;
            return Binding.DoNothing;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
