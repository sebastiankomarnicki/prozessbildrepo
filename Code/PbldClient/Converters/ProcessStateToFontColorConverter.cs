﻿using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide the font color for a <see cref="ProcessState"/> given <see cref="EvaluationAndColorSettings"/> 
    /// </summary>
    [ValueConversion(typeof(object[]), typeof(Color))]
    public class ProcessStateToFontColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            ProcessState? ps = values[0] as ProcessState?;
            EvaluationAndColorSettings settings = values[1] as EvaluationAndColorSettings;
            if (!ps.HasValue || settings == null)
                return Binding.DoNothing;

            switch (ps.Value)
            {
                case ProcessState.ActionLimitViolationS:
                    return settings.ActionLimitViolationS.FontColor;
                case ProcessState.ActionLimitViolationXq:
                    return settings.ActionLimitViolationXq.FontColor;
                case ProcessState.DeficientPart:
                    return settings.DeficientPart.FontColor;
                case ProcessState.Endangered:
                    return settings.ProcessEndangered.FontColor;
                case ProcessState.Ok:
                    return settings.ProcessOk.FontColor;
                case ProcessState.OutOfControl:
                    return settings.ProcessOoC.FontColor;
                case ProcessState.StabilityViolation:
                    return settings.StabilityViolation.FontColor;
                case ProcessState.TooFewValues:
                    return settings.NoOrTooFewValues.FontColor;
                default:
                    throw new ArgumentException($"Invalid {typeof(ProcessState)} value: {ps.Value}");
            }
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
