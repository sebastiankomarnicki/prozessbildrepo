﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// General converter to retrieve a value from a data bound <see cref="Dictionary{int, string}"/> by a data bound key.
    /// </summary>
    public class DictionaryIntStringMVConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            if (values.Length != 2)
                return Binding.DoNothing;

            var dict = values[0] as IReadOnlyDictionary<int, string>;
            var key = values[1] as int?;
            if (dict == null || !key.HasValue)
                return Binding.DoNothing;
            string result;
            if (dict.TryGetValue(key.Value, out result))
                return result;
            else
                return Binding.DoNothing;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
