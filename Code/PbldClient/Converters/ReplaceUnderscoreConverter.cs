﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Convert given text by replacing underscores with blanks
    /// </summary>
    [ValueConversion(typeof(String), typeof(String))]
    public class ReplaceUnderscoreConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            String text = value as string;
            if (text == null)
                return Binding.DoNothing;
            return text.Replace('_', ' ');
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
