﻿using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace Promess.Pbld.Converters
{
    /// <summary>
    /// Converter to provide the font weight for a <see cref="ProcessState"/> given <see cref="EvaluationAndColorSettings"/> 
    /// </summary>
    [ValueConversion(typeof(object[]), typeof(FontWeight))]
    public class ProcessStateToFontWeightConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2)
                return Binding.DoNothing;

            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;

            ProcessState? ps = values[0] as ProcessState?;
            EvaluationAndColorSettings settings = values[1] as EvaluationAndColorSettings;
            if (!ps.HasValue || settings == null)
                return Binding.DoNothing;
            bool isBold;
            switch (ps.Value)
            {
                case ProcessState.ActionLimitViolationS:
                    isBold = settings.ActionLimitViolationS.BoldInTree;
                    break;
                case ProcessState.ActionLimitViolationXq:
                    isBold = settings.ActionLimitViolationXq.BoldInTree;
                    break;
                case ProcessState.DeficientPart:
                    isBold = settings.DeficientPart.BoldInTree;
                    break;
                case ProcessState.Endangered:
                    isBold = settings.ProcessEndangered.BoldInTree;
                    break;
                case ProcessState.Ok:
                    isBold =  settings.ProcessOk.BoldInTree;
                    break;
                case ProcessState.OutOfControl:
                    isBold = settings.ProcessOoC.BoldInTree;
                    break;
                case ProcessState.StabilityViolation:
                    isBold = settings.StabilityViolation.BoldInTree;
                    break;
                case ProcessState.TooFewValues:
                    isBold = settings.NoOrTooFewValues.BoldInTree;
                    break;
                default:
                    throw new ArgumentException($"Invalid {typeof(ProcessState)} value: {ps.Value}");
            }
            if (isBold)
                return FontWeights.Bold;
            else
                return FontWeights.Normal;
        }

        /// <summary>
        /// Not supported
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
