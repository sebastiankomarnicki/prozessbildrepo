﻿using Promess.Common.Util;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace Promess.Pbld.Controls
{
    /// <summary>
    /// Control for dragging something
    /// </summary>
    public class DragThumbCommand : Thumb
    {
        public static readonly DependencyProperty MoveCommandProperty =
            DependencyProperty.Register(nameof(MoveCommand),
                typeof(DelegateCommand<Vector>),
                typeof(DragThumbCommand),
                null
                );

        public DelegateCommand<Vector> MoveCommand
        {
            get { return (DelegateCommand<Vector>)GetValue(MoveCommandProperty); }
            set { SetValue(MoveCommandProperty, value); }
        }

        public DragThumbCommand()
        {
            this.DragDelta += DragThumbCommand_DragDelta;
        }

        void DragThumbCommand_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Vector movement = new Vector(e.HorizontalChange, e.VerticalChange);
            MoveCommand.Execute(movement);
            e.Handled = true;
        }
    }
}
