﻿using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Data.Settings;
using Promess.Pbld.Services;
using Promess.Pbld.Utility;
using Promess.Pbld.ViewModels;
using Promess.QsStat.Services;
using System;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Threading.Tasks;
using System.Waf.Applications;
using System.Windows;

namespace Promess.Pbld.Controllers
{
    /// <summary>
    /// Main controller for running and shutting down the program. Handles some view model independent state changes whcih affect the overall application
    /// </summary>
    [Export(typeof(IModuleController)), Export]
    internal class ModuleController : IModuleController
    {
        private readonly Lazy<MainViewModel> _mainViewModel;

        private readonly MainService _mainService;
        private readonly IconLookupService _iconLookupService;
        private readonly QsStatService _qsStatService;
        private readonly ConfigurationService _configurationService;
        private readonly ModifiedConfigurationManagerPbld _configurationManager;
        private readonly FollowerService _followerService;

        [ImportingConstructor]
        public ModuleController(Lazy<MainViewModel> mainViewModel, MainService mainService, IconLookupService iconLookupService,
            ConfigurationService configurationService, QsStatService qsStatService, FollowerService followerService,
            ModifiedConfigurationManagerPbld configurationManager)
        {
            this._mainViewModel = mainViewModel;
            this._mainService = mainService;

            this._iconLookupService = iconLookupService;
            this._qsStatService = qsStatService;
            this._configurationService = configurationService;
            this._followerService = followerService;

            this._configurationManager = configurationManager;
        }

        private MainViewModel MainViewModel
        {
            get { return _mainViewModel.Value; }
        }

        public void Initialize()
        {
            MainViewModel.LanguageChangeCommand = new DelegateCommand<CultureInfo>(newLanguage => TryChangeLanguage(newLanguage));
            _mainService.MainView = MainViewModel.View;
            _iconLookupService.Initialize();
            var panelsCopy = _configurationService.GetPanelsCopy();
            var panels = panelsCopy.Item1 ? panelsCopy.Item2 : null;
            var selectedArea = _configurationService.GetCurrentAreaCopyOrNull();
            var acceptableQsStatSettings = _configurationManager.GetSectionAndAddIfMissingAsAcceptableSettings<QsStatSettings>(Constants.QsStat);
            bool qsSettingsChanged = _qsStatService.Initialize(selectedArea, panels, _configurationService.GeneralSettings, _configurationService.EvaluationAndColorSettings, _configurationService.GetListViewSettingsCopy(), acceptableQsStatSettings.Settings);
            if (qsSettingsChanged)
            {
                acceptableQsStatSettings.AcceptChanges();
                _configurationService.QsStatSettings = acceptableQsStatSettings.Settings;
            }
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.ListViewSettingsChanged), ListViewSettingsChangedHandler);
            WeakEventManager<ConfigurationService, EventArgs>.AddHandler(this._configurationService, nameof(ConfigurationService.PanelsChanged), PanelsChangedHandler);
        }

        private void TryChangeLanguage(CultureInfo newUILanguage)
        {
            if (LanguageManager.Current.TryChangeLanguage(newUILanguage))
            {
                var acceptableLanguageSettings = this._configurationManager.GetSectionAndAddIfMissingAsAcceptableSettings<LanguageSettings>(Constants.Language);
                acceptableLanguageSettings.Settings.Culture = newUILanguage;
                acceptableLanguageSettings.AcceptChanges();
            }
        }

        /// <summary>
        /// Run the controller by showing the main view, starting the <see cref="QsStatService"/> and adjust for follower mode
        /// </summary>
        public void Run()
        {
            MainViewModel.Show();
            Task.Run(async () => await this._qsStatService.RunAsync()).Wait();
            if (this._mainService.OperatingMode == Data.OperatingMode.follower)
            {
                this._followerService.Start();
            }
        }

        /// <summary>
        /// Shut down controller
        /// </summary>
        public void Shutdown()
        {
            _qsStatService.Dispose();
            _followerService.Dispose();
        }

        #region event handlers
        private void ListViewSettingsChangedHandler(object sender, EventArgs e)
        {
            this._qsStatService.UpdateGeneralRetrievalKeys(this._configurationService.GetListViewSettingsCopy());
        }
        private void PanelsChangedHandler(object sender, EventArgs e)
        {
            var panelsCopy = this._configurationService.GetPanelsCopy();
            var panels = panelsCopy.Item1 ? panelsCopy.Item2 : null;
            _qsStatService.SetPanelItemModels(panels);
        }
        #endregion
    }
}
