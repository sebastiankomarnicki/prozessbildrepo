﻿namespace Promess.Pbld.Data
{
    public enum OperatingMode
    {
        normal = 0,
        leader = 1,
        follower = 2
    }
}
