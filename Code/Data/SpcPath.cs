﻿using Promess.Pbld.Data.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Waf.Foundation;

namespace Promess.Pbld.Data
{
    /// <summary>
    /// Configuration for a SPCPath, which is a folder for data retrieval
    /// </summary>
    [DataContract(Name = "SpcPath")]
    public class SpcPath:ValidatableModel, IExtensibleDataObject
    {
        public const short MAXSPCNUMBER=999; 

        private short _spcNumber;
        private string _dataPath;
        private string _hintLine;
        private string _panelConfigurationName;
        private short _showDataNumberDays;
        private short _refreshInterval;
        private string _computerName;
        private bool _deleteOldData;

        public SpcPath(short spcNumber, string panelConfigurationName)
        {
            this.SpcNumber = spcNumber;
            this.PanelConfigurationName = panelConfigurationName;
            this.DataPath = "";
            this.HintLine = "";
            this.ShowDataNumberDays = 5;
            this.RefreshInMinutes = 10;
            this.ComputerName = "";
            this.DeleteOldData = false;
            this.Guid = Guid.NewGuid();
        }

        [DataMember(Name = "SpcNumber")]
        [Range(1,MAXSPCNUMBER, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public short SpcNumber
        {
            get { return _spcNumber; }
            set { SetPropertyAndValidate(ref _spcNumber, value); }
        }

        [DataMember(Name = "DataPath")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public string DataPath
        {
            get { return _dataPath; }
            set { SetPropertyAndValidate(ref _dataPath,value); }
        }

        [DataMember(Name = "HintLine")]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationStringLengthError))]
        public string HintLine
        {
            get { return _hintLine; }
            set { SetPropertyAndValidate(ref _hintLine, value); }
        }

        [DataMember(Name = "PanelConfigurationName")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public string PanelConfigurationName
        {
            get { return _panelConfigurationName; }
            set
            {
                Debug.Assert(value != null);
                SetPropertyAndValidate(ref _panelConfigurationName, value);
            }
        }

        [DataMember(Name = "ShowDataNumberDays")]
        [Range(1,999, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public short ShowDataNumberDays
        {
            get { return _showDataNumberDays; }
            set
            {
                SetPropertyAndValidate(ref _showDataNumberDays, value);
            }
        }

        [DataMember(Name = "RefreshInterval")]
        [Range(1,999, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public short RefreshInMinutes
        {
            get {  return _refreshInterval;  }
            set
            {
                SetPropertyAndValidate(ref _refreshInterval, value);
            }
        }

        [DataMember(Name = "ComputerName")]
        public string ComputerName
        {
            get { return _computerName; }
            set
            {
                SetPropertyAndValidate(ref _computerName, value);
            }
        }

        [DataMember(Name = "DeleteOldData")]
        public bool DeleteOldData
        {
            get { return _deleteOldData; }
            set
            {
                SetProperty(ref _deleteOldData, value);
            }
        }

        [DataMember(Name = "Identifier")]
        public Guid Guid
        {
            get; private set;
        }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion
    }
}