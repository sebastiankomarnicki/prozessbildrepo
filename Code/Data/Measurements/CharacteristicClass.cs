﻿namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Known characteristic classes. From AQDEF K2005, values from definitions.
    /// </summary>
    public enum CharacteristicClass
    {
        Unimportant=0,
        LittleImportant=1,
        Important = 2,
        Significant = 3,
        Critical = 4
    }
}
