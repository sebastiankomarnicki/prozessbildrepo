﻿using System.Collections.Generic;

namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Container for queried data
    /// </summary>
    public class QueriedData
    {
        public QueriedData(IReadOnlyDictionary<int, string> partData, IReadOnlyCollection<int> notRetrievedPartKeys, IReadOnlyDictionary<int, IReadOnlyDictionary<int, string>> characteristicsData, IReadOnlyDictionary<int, IReadOnlyCollection<int>> notRetrievedCharacteristicsKeys, IReadOnlyDictionary<int, string> measuringData, IReadOnlyDictionary<int, IReadOnlyDictionary<int, string>> characteristicsMeasuringData)
        {
            this.PartData = partData;
            this.NotRetrievedPartKeys = notRetrievedPartKeys;
            this.CharacteristicsData = characteristicsData;
            this.NotRetrievedCharacteristicsKeys = notRetrievedCharacteristicsKeys;
            this.MeasuringData = measuringData;
            this.CharacteristicsMeasuringData = characteristicsMeasuringData;
        }

        public IReadOnlyDictionary<int, string> PartData { get; private set; }

        public IReadOnlyCollection<int> NotRetrievedPartKeys { get; private set; }

        public IReadOnlyDictionary<int, IReadOnlyDictionary<int, string>> CharacteristicsData { get; private set; }

        public IReadOnlyDictionary<int, IReadOnlyCollection<int>> NotRetrievedCharacteristicsKeys { get; private set; }

        public IReadOnlyDictionary<int, string> MeasuringData { get; private set; }

        public IReadOnlyDictionary<int, IReadOnlyDictionary<int, string>> CharacteristicsMeasuringData { get; private set; }

    }
}
