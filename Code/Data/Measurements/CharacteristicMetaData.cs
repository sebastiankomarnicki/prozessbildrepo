﻿namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Meta data for a characteristic. Is it enabled and how was it evaluated?
    /// </summary>
    public class CharacteristicMetaData
    {
        public CharacteristicMetaData(bool enabled, ProcessState state)
        {
            this.State = state;
            this.Enabled = enabled;
        }

        public ProcessState State { get; private set; }
        public bool Enabled { get; private set; }
    }
}
