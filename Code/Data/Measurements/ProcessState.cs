﻿namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Process state evaluation. Assigned numbers according to severance of state, higher is worse
    /// </summary>
    public enum ProcessState
    {
        Ok = 0,
        Endangered = 1,
        OutOfControl = 2,
        StabilityViolation =3,
        ActionLimitViolationXq = 5,
        ActionLimitViolationS = 4,
        DeficientPart = 6,
        TooFewValues = 7,
        FailedEvaluation = 8
    }
}
