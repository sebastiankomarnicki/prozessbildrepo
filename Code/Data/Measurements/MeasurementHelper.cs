﻿using System.Collections.Generic;
using System.Linq;

namespace Promess.Pbld.Data.Measurements
{
    public static class MeasurementHelper
    {
        /// <summary>
        /// Get worst <see cref="ProcessState"/> of enabled characteristics. 
        /// </summary>
        /// <param name="charaMetaEvaluation">Meta data of characteristics</param>
        /// <returns>Worst state</returns>
        public static ProcessState EvaluateState(IEnumerable<CharacteristicMetaData> charaMetaEvaluation)
        {
            var states = charaMetaEvaluation.Where(chara => chara.Enabled).Select(chara => chara.State);
            return EvaluateState(states);
        }

        /// <summary>
        /// Get worst <see cref="ProcessState"/> of given process states
        /// </summary>
        /// <param name="processStates">States to evaluate</param>
        /// <returns>Worst state or <see cref="ProcessState.TooFewValues"/> if there aren't any.</returns>
        public static ProcessState EvaluateState(IEnumerable<ProcessState> processStates)
        {
            if (!processStates.Any())
                return ProcessState.TooFewValues;
            ProcessState result = ProcessState.Ok;
            foreach (var state in processStates)
            {
                if (result < state)
                    result = state;
            }
            return result;
        }
    }
}
