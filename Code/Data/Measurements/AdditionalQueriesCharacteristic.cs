﻿namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Non qsStat queries for a characteristic
    /// </summary>
    public enum AdditionalQueriesCharacteristic
    {
        LastMeasurementValue=0,
        LastMeasurementDateTime=1,
        LastMeasurementBatchNumber=2,
        LastMeasurementOperator=3,
    }
}
