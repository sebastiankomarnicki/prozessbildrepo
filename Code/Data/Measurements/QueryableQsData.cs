﻿using Promess.Common.QDas;
using Promess.Common.QDas.Catalogs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Queryable for AQDEF data and custom entries.
    /// </summary>
    public class QueryableQsData
    {
        private const int DEFAULT_DECIMAL_PLACES = 3;
        private readonly TS_TEILEDATEN _partData;
        private readonly TS_MERKMALSDATEN[] _characteristicsData;
        private readonly IReadOnlyList<TS_MERKMAL_IST[]> _parsedDfxData;
        private readonly byte[] _dfqData;
        private readonly DateTime _lastDsChangeTime;

        public QueryableQsData()
        {
            this._partData = new TS_TEILEDATEN();
            this._characteristicsData = new TS_MERKMALSDATEN[0];
            this._parsedDfxData = new List<TS_MERKMAL_IST[]>().AsReadOnly();
            this._dfqData = new byte[0];
            this._lastDsChangeTime = DateTime.MinValue;
        }

        public QueryableQsData(TS_TEILEDATEN partData, TS_MERKMALSDATEN[] characteristicsData, IReadOnlyList<TS_MERKMAL_IST[]> parsedDfxData, byte[] dfqData, DateTime lastDsChangeTime)
        {
            this._partData = partData;
            this._characteristicsData = characteristicsData;
            this._parsedDfxData = parsedDfxData;
            this._dfqData = dfqData;
            this._lastDsChangeTime = lastDsChangeTime;
        }
        public byte[] DfqData { get { return _dfqData; } }

        /// <summary>
        /// Returns part data. Do not modify.
        /// </summary>
        public TS_TEILEDATEN PartData { get { return _partData; } }
        /// <summary>
        /// Return characteristics data. Do not modify.
        /// </summary>
        public TS_MERKMALSDATEN[] CharacteristicsData { get { return _characteristicsData; } }

        public IReadOnlyList<TS_MERKMAL_IST[]> CharacteristicsValues { get { return _parsedDfxData; } }

        public DateTime LastDsChangeTime { get { return _lastDsChangeTime; } }

        public bool TryGetPartData(int knumber, out string value)
        {
            if (knumber >= short.MinValue && knumber <= short.MaxValue)
            {
                return TryGetPartData((short)knumber, out value);
            }
            else
            {
                value = String.Empty;
                return false;
            }        
        }

        public bool TryGetPartData(short knumber, out string value)
        {
            if (this._partData.TryGetField(knumber, out object returned))
            {
                value = returned?.ToString();
                return true;
            }
            else
            {
                value = string.Empty;
                return false;
            }
        }

        public bool TryGetCharacteristicsData(int knumber, int characteristicNumber, out string value)
        {
            if (knumber >= short.MinValue && knumber <= short.MaxValue)
            {
                return TryGetCharacteristicsData((short)knumber, characteristicNumber, out value);
            }
            else
            {
                value = String.Empty;
                return false;
            }
        }

        public bool TryGetCharacteristicsData(short knumber, int characteristicNumber, out string value)
        {
            if (characteristicNumber < 1 || characteristicNumber > this._characteristicsData.Length)
            {
                value = String.Empty;
                return false;
            }
        
            if (this._characteristicsData[characteristicNumber-1].TryGetField(knumber, out object returned))
            {
                value = returned?.ToString();
                return true;
            }
            else
            {
                value = String.Empty;
                return false;
            }
        }

        public bool TryGetAdditionalPartData(int additionalNumberPart, IReadOnlyList<int> enabledCharacteristicNumbers, out string value)
        {
            if (!Enum.IsDefined(typeof(AdditionalQueriesPart), additionalNumberPart))
            {
                value = String.Empty;
                return false;
            }
            return TryGetAdditionalDataPart((AdditionalQueriesPart)additionalNumberPart, enabledCharacteristicNumbers, out value);
        }

        public bool TryGetAdditionalCharacteristicData(int additionalNumberCharacteristic, int characteristicNumber, out string value)
        {
            if (!Enum.IsDefined(typeof(AdditionalQueriesCharacteristic), additionalNumberCharacteristic))
            {
                value = String.Empty;
                return false;
            }
            return TryGetAdditionalDataCharacteristic((AdditionalQueriesCharacteristic)additionalNumberCharacteristic, characteristicNumber, out value);
        }

        private bool TryGetAdditionalDataCharacteristic(AdditionalQueriesCharacteristic additionalNumberCharacteristic, int characteristicNumber, out string value)
        {
            switch (additionalNumberCharacteristic)
            {
                case AdditionalQueriesCharacteristic.LastMeasurementValue:
                    return GetLastMeasurementValueForCharacteristic(characteristicNumber, out value);
                case AdditionalQueriesCharacteristic.LastMeasurementDateTime:
                    return GetLastMeasurementDateTimeForCharacteristic(characteristicNumber, out value);
                case AdditionalQueriesCharacteristic.LastMeasurementBatchNumber:
                    return GetLastMeasurementBatchNumberForCharacteristic(characteristicNumber, out value);
                case AdditionalQueriesCharacteristic.LastMeasurementOperator:
                    return GetLastOperatorNameForCharacteristic(characteristicNumber, out value);
                default:
                    value = string.Empty;
                    return false;
            }
        }

        private bool TryGetAdditionalDataPart(AdditionalQueriesPart additionalNumberPart, IReadOnlyList<int> enabledCharacteristicNumbers, out string value)
        {
            Debug.Assert(enabledCharacteristicNumbers.All(number => number<= this._characteristicsData.Length));
            int linesOk, limitViolations, rejections;
            switch (additionalNumberPart)
            {
                case AdditionalQueriesPart.NumberParts:
                    value = this._parsedDfxData.Count.ToString();
                    return true;
                case AdditionalQueriesPart.NumberOk:
                    if (GetMeasurementLineEvaluations(enabledCharacteristicNumbers, out linesOk, out limitViolations, out rejections))
                    {
                        value = linesOk.ToString();
                        return true;
                    }
                    else
                    {
                        value = string.Empty;
                        return false;
                    }
                case AdditionalQueriesPart.NumberControlLimitViolation:
                    if (GetMeasurementLineEvaluations(enabledCharacteristicNumbers, out linesOk, out limitViolations, out rejections))
                    {
                        value = limitViolations.ToString();
                        return true;
                    }
                    else
                    {
                        value = string.Empty;
                        return false;
                    }
                case AdditionalQueriesPart.NumberWarningLimitViolation:
                    if (GetMeasurementLineEvaluations(enabledCharacteristicNumbers, out linesOk, out limitViolations, out rejections))
                    {
                        value = rejections.ToString();
                        return true;
                    }
                    else
                    {
                        value = string.Empty;
                        return false;
                    }
                default:
                    value = string.Empty;
                    return false;
            }
        }

        public bool GetMeasurementLineEvaluations(IEnumerable<int> enabledCharacteristicNumbers, out int linesOk,  out int limitViolations, out int rejections)
        {
            linesOk = 0;
            limitViolations = 0;
            rejections = 0;
            var toCheckIndices = (from number in enabledCharacteristicNumbers where _characteristicsData[number-1].iMerkmal_Art == (short)iMerkmal_Art.imaVARIABEL select number-1).ToList();
            if (!toCheckIndices.Any())
            {
                return false;
            }
            bool limitViolation, rejected;

            foreach (var line in this._parsedDfxData)
            {
                limitViolation = false;
                rejected = false;
                foreach (var index in toCheckIndices)
                {
                    if (!limitViolation && HasLimitViolation(this._characteristicsData[index], line[index]))
                    {
                        limitViolations++;
                        limitViolation = true;
                    }
                    if (!rejected && IsRejected(this._characteristicsData[index], line[index]))
                    {
                        rejections++;
                        rejected = true;
                    }
                    if (limitViolation && rejected)
                        break;
                }
                if (!limitViolation && !rejected)
                    linesOk++;
            }
            return true;
        }

        /// <summary>
        /// Check if variable characteristic is rejected
        /// </summary>
        /// <param name="header">header data of characteristic</param>
        /// <param name="data">mesurement data</param>
        /// <returns></returns>
        private bool IsRejected(TS_MERKMALSDATEN header, TS_MERKMAL_IST data)
        {
            Debug.Assert(data.dMesswert.HasValue);
            //variable measurement data should always have a measurement value
            if (!data.dMesswert.HasValue || !header.dLage_UEG.HasValue || !header.dLage_OEG.HasValue)
                return false;
            double measurement = data.dMesswert.Value;

            return measurement > header.dLage_OEG.Value || measurement < header.dLage_UEG.Value;
        }


        /// <summary>
        /// Check limit violation for variable characteristics
        /// </summary>
        /// <param name="header">header data of characteristic</param>
        /// <param name="data">mesurement data</param>
        /// <returns></returns>
        private bool HasLimitViolation(TS_MERKMALSDATEN header, TS_MERKMAL_IST data)
        {
            Debug.Assert(data.dMesswert.HasValue);
            //variable measurement data should always have a measurement value
            if (!data.dMesswert.HasValue || !(header.LowerSpecificationLimit.HasValue && header.UpperSpecificationLimit.HasValue) && !(header.LowerAllowance.HasValue && header.UpperAllowance.HasValue))
                return false;
            double measurement = data.dMesswert.Value;
            double lowerLimit, upperLimit;
            if (header.LowerSpecificationLimit.HasValue)
            {
                lowerLimit = header.LowerSpecificationLimit.Value;
                upperLimit = header.UpperSpecificationLimit.Value;
            }
            else
            {
                lowerLimit = header.LowerAllowance.Value;
                upperLimit = header.UpperAllowance.Value;
            }
            return measurement > upperLimit || measurement < lowerLimit;
        }

        public bool GetLastOperatorNameForCharacteristic(int characteristicNumber, out string value)
        {
            if (characteristicNumber < 1 || characteristicNumber > this._characteristicsData.Length || !_parsedDfxData.Any())
            {
                value = String.Empty;
                return false;
            }

            TS_MERKMAL_IST data;
            for (int i = _parsedDfxData.Count - 1; i >= 0; i--)
            {
                data = _parsedDfxData.Last()[characteristicNumber - 1];
                if (data.lPrueferNr.HasValue)
                {
                    if (_partData.OperatorCatalog.TryGetCatalogEntriesAndQdasKeysForKey(data.lPrueferNr.Value, out OperatorCatalogEntry operatorEntry))
                    {
                        value = operatorEntry.Name2;
                        return true;
                    }
                    else
                    {
                        break;
                    }
                }
            }         
            value = String.Empty;
            return false;
        }

        public bool GetLastMeasurementBatchNumberForCharacteristic(int characteristicNumber, out string value)
        {
            if (characteristicNumber < 1 || characteristicNumber > this._characteristicsData.Length || !_parsedDfxData.Any())
            {
                value = String.Empty;
                return false;
            }

            var data = _parsedDfxData.Last()[characteristicNumber - 1];
            if (String.IsNullOrEmpty(data.sChargenNr))
            {
                value = String.Empty;
                return false;
            }
            else
            {
                value = data.sChargenNr;
                return true;
            }           
        }

        public bool GetLastMeasurementDateTimeForCharacteristic(int characteristicNumber, out string value)
        {
            if (characteristicNumber < 1 || characteristicNumber > this._characteristicsData.Length || !_parsedDfxData.Any())
            {
                value = String.Empty;
                return false;
            }

            var data = _parsedDfxData.Last()[characteristicNumber - 1];
            if (data.HasDateTime)
            {
                value = data.sDatumZeit.Value.ToString();
                return true;
            }
            else
            {
                value = String.Empty;
                return false;
            }
        }

        public bool GetLastMeasurementValueForCharacteristic(int characteristicNumber, out string value)
        {
            if (characteristicNumber < 1 || characteristicNumber > this._characteristicsData.Length || !_parsedDfxData.Any())
            {
                value = String.Empty;
                return false;
            }

            var header = _characteristicsData[characteristicNumber - 1];
            var data = _parsedDfxData.Last()[characteristicNumber - 1];
            switch (header.iMerkmal_Art)
            {
                case (short)iMerkmal_Art.imaVARIABEL:
                    int decimalPlaces = header.iNachkommastellen ?? DEFAULT_DECIMAL_PLACES;
                    if (!data.dMesswert.HasValue)
                    {
                        value = String.Empty;
                        return false;
                    }
                    value = data.dMesswert.Value.ToString($"F{decimalPlaces}");
                    return true;
                case (short)iMerkmal_Art.imaATTRIBUTIV:
                    if (!data.iAnzahl_Fehler.HasValue || !data.iStichprobenUmfang.HasValue)
                    {
                        value = String.Empty;
                        return false;
                    }
                    value = $"{data.iStichprobenUmfang.Value}/{data.iAnzahl_Fehler.Value}";
                    return true;
                    
                default:
                    value = String.Empty;
                    return false;
            }
        }
    }
}
