﻿using Promess.Pbld.Data.Settings;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Foundation;

namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// All data in a folder with settings for that folder
    /// </summary>
    public class FolderDatasets:Model
    {
        private readonly bool _deleteOldData;
        private List<DataSet> _datasets;      
        private short _maxDataAgeInDays;
        private GeneralSettings _generalSettings;
        private SemaphoreSlim _sema;
        private Encoding _qcomEncoding;

        public FolderDatasets(bool deleteOldData, short maxDataAgeInDays, GeneralSettings generalSettings, Encoding qcomEncoding)
        {
            this._deleteOldData = deleteOldData;
            this._maxDataAgeInDays = maxDataAgeInDays;
            this._datasets = new List<DataSet>();
            this._generalSettings = generalSettings;
            this._sema = new SemaphoreSlim(1);
            this._qcomEncoding = qcomEncoding;
            LastRefreshTime = DateTime.MinValue;
        }

        public DateTime LastRefreshTime { get; set; }

        public IReadOnlyList<DataSet> DataSets { get { return _datasets; } }

        /// <summary>
        /// Check internal data against new files, remove not present, update updated and create new representations if required.
        /// </summary>
        /// <param name="files">Files in destination</param>
        /// <param name="executeDt">Timestamp for when the file list was created</param>
        /// <returns>Task representing a refresh operation. Result of the Task is true if data was changed, otherwise false.</returns>
        public async Task<bool> RemoveNotPresentAndCreateNewAsync(List<string> files, DateTime executeDt)
        {
            int index;
            var newDatasets = new List<DataSet>();
            var toReloadDatasets = new List<DataSet>();
            DateTime oldestDt = executeDt - TimeSpan.FromDays(this._maxDataAgeInDays);
            GeneralSettings currentSettings;
            bool datasetChanged = false;

            await this._sema.WaitAsync();
            currentSettings = this._generalSettings;
            this._sema.Release();

            foreach (var current in DataSets)
            {
                index = files.FindIndex(file => String.Equals(current.MainFile, file, StringComparison.Ordinal));
                if (index == -1)
                {
                    datasetChanged = true;
                    continue;
                }
                else
                {
                    toReloadDatasets.Add(current);
                    files.RemoveAt(index);
                }
            }
            var reloadTasks = new List<Task<bool>>();
            var oldChangeTime = new Dictionary<DataSet, DateTime>();
            foreach (var ds in toReloadDatasets)
            {
                oldChangeTime[ds] = ds.LastChangeTime;
                reloadTasks.Add(ds.ReloadAsync(oldestDt, this._deleteOldData));
            }
            var tasks = new List<Task<Tuple<bool,DataSet>>>();
            foreach(var filename in files)
            {
                tasks.Add(TryGenerateDataSetAsync(filename, oldestDt, this._deleteOldData, this._generalSettings));
            }
            var reloadResults = await Task.WhenAll(reloadTasks);
            for (int i = 0; i < reloadResults.Length; i++)
            {
                var currentDs = toReloadDatasets[i];
                if (!reloadResults[i])
                {
                    datasetChanged = true;
                    continue;
                }
                newDatasets.Add(currentDs);
                if (oldChangeTime[currentDs] != currentDs.LastChangeTime)
                    datasetChanged = true;
            }
            var generateResults = await Task.WhenAll(tasks);
            foreach (var successAnddataset in generateResults)
            {
                if (!successAnddataset.Item1)
                    continue;
                var ds = successAnddataset.Item2;
                if (ds.FailedToLoad)
                    continue;
                newDatasets.Add(ds);
                datasetChanged = true;
            }
            if (datasetChanged)
            {
                this._datasets = newDatasets;
                RaisePropertyChanged(nameof(DataSets));
            }
            return datasetChanged;
        }

        public async Task SetGeneralSettingsAsync(GeneralSettings generalSettings)
        {
            await this._sema.WaitAsync();
            var updateTasks = new List<Task>(DataSets.Count);
            foreach (var ds in DataSets)
            {
                updateTasks.Add(ds.SetSettings(generalSettings));
            }
            await Task.WhenAll(updateTasks);
            this._generalSettings = generalSettings;
            this._sema.Release();
        }

        public async Task UpdateEncodingForCQOM(Encoding encoding)
        {
            if (encoding == this._qcomEncoding)
                return;
            await this._sema.WaitAsync();
            this._qcomEncoding = encoding;
            foreach (var ds in DataSets)
                await ds.UpdateQCOMEncoding(this._qcomEncoding);
            this._sema.Release();
        }

        private async Task<Tuple<bool,DataSet>> TryGenerateDataSetAsync(string filename, DateTime oldestDt, bool deleteOldData, GeneralSettings generalSettings)
        {
            DataSet ds = new DataSet(filename, generalSettings, this._qcomEncoding);
            var success = await ds.LoadAsync(oldestDt, deleteOldData);
            return new Tuple<bool, DataSet>(success, ds);
        }
    }
}
