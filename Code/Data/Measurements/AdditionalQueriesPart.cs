﻿namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Non qsStat queries for a part
    /// </summary>
    public enum AdditionalQueriesPart
    {
        NumberParts=4,
        NumberOk=5,
        NumberControlLimitViolation=6,
        NumberWarningLimitViolation=7
    }
}
