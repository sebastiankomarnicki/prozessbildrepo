﻿namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Supported characteristic types. From AQDEF K2004, values from definitions. make sure the checking code is altered as well if new types are supported
    /// </summary>
    public enum CharacteristicType
    {
        Variable = 0,
        Attributive =1
    }
}
