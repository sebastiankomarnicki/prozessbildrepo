﻿using Promess.Common.QDas.Settings;
using Promess.Common.QDas;
using Promess.Common.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Promess.Pbld.Data.Settings;
using System.Diagnostics;

namespace Promess.Pbld.Data.Measurements
{
    /// <summary>
    /// Process AQDEF data consisting of a dfd and dfx file or dfq file
    /// </summary>
    public sealed class DataSet:IDisposable
    {
        private const short ATTRIBUTE = 2;
        private const int TOTAL_NUMBER_CHARACTERISTICS = 100;
        private const int REQUIRED_CPK_VALUE = 8520;
        private const int REQUIRED_CP_VALUE = 8521;
        private const int FIXED_CPK_VALUE = 8522;
        private const int FIXED_CP_VALUE = 8523;

        private const int FILLING_DELETED_ATTRIBUTE = 256;
        private const string EMPTY_ATTRIBUTE_TEXT = "255";

        internal const int DELETEDFXMult = 3;
        private static readonly Encoding _globalDefaultEncoding = Encoding.GetEncoding(1252);
        private static readonly QdasValueLineSettings _globalQdasSettings = new QdasValueLineSettings();
        private GeneralSettings _datasetSettings;
        private bool _datasetSettingsChanged;
        private const int RETRIES = 3;
        private const int DELAY = 50;

        private readonly SemaphoreSlim _semaphore;

        private readonly bool _isDfqFile;
        private readonly string _dfd_q_File;
        private string _dfxFile;
        private string _dfdFileData;
        private string _dfxFileData;
        private Encoding _qcomEncoding;

        private QueryableQsData _generatedQueryable;
        private DateTime _dfxDate;
        private long _dfxStartPos;
        private long _dfxEndPos;//vb6 function FileLen       
        private QdasDfdReadResult _parsedDfdData;
        private DfxData _parsedDfxData;
        private Encoding _encoding;
        private Encoding _dfxEncoding;//only when separate dfx file

        private Exception _exception;

        /// <summary>
        /// Create a data set for a specific data source, constraints and retrieval encoding.
        /// </summary>
        /// <param name="dfd_q_file">Data source</param>
        /// <param name="datasetSettings">Data point and time constraints</param>
        /// <param name="outputEncoding">Output encoding</param>
        public DataSet(string dfd_q_file, GeneralSettings datasetSettings, Encoding outputEncoding)
        {
            this._isDfqFile = dfd_q_file.EndsWith(".dfq", StringComparison.OrdinalIgnoreCase);
            this._dfd_q_File = dfd_q_file;
            this._dfxDate = DateTime.MinValue;
            this.Guid = Guid.NewGuid();
            this._dfxStartPos = 0;
            this._dfxEndPos = 0;
            this._datasetSettings = datasetSettings;
            this._datasetSettingsChanged = false;
            this._generatedQueryable = null;
            this._qcomEncoding = outputEncoding;
            this._semaphore = new SemaphoreSlim(1, 1);
        }

        public Guid Guid { get; private set; }

        /// <summary>
        /// Load the data from the previously defined data source.
        /// </summary>
        /// <param name="refreshTs">Refresh timestamp</param>
        /// <param name="deleteOldData">Set if file(s) should be deleted if measuring data is too old.</param>
        /// <returns>Task representing the load operation. When data could be loaded the result of the task is true, otherwise false.</returns>
        internal async Task<bool> LoadAsync(DateTime refreshTs, bool deleteOldData)
        {
            FailedToLoad = true;
            FailedToLoadSome = false;
            this._exception = null;
            //not sure if data should be cleared here or some information is neccessary for continuing
            this._dfxFileData = "";
            this._dfxStartPos = 0;
            this._dfxEndPos = 0;
            
            if (this._isDfqFile)
            {
                this._dfxFile = "";
            }
            else
            {
                this._dfxFile = this._dfd_q_File.Substring(0, this._dfd_q_File.Length - 4) + ".dfx";               
            }
            await this._semaphore.WaitAsync();
            try
            {              
                using (var fs = await RetryHelper.DoAndRetryAsync(() => File.Open(this._dfd_q_File,FileMode.Open,FileAccess.ReadWrite,FileShare.Delete), RETRIES, TimeSpan.FromMilliseconds(DELAY)))
                {
                    string dfq_d_FileData;
                    using (var sr = new StreamReader(fs, DataSet._globalDefaultEncoding, true, 4096, true))
                    {
                        this._encoding = sr.CurrentEncoding;
                        dfq_d_FileData = await sr.ReadToEndAsync();
                    }
                    
                    FailedToLoad = false;
                    if (this._isDfqFile)
                    {
                        this._dfxEndPos = fs.Position;
                        this._dfxDate = File.GetLastWriteTimeUtc(this._dfd_q_File);
                        if (deleteOldData && this._dfxDate < refreshTs - TimeSpan.FromDays(1))
                        {
                            File.Delete(this._dfd_q_File);
                            return false;
                        }
                        this._dfxEndPos = fs.Position;
                        (this._dfdFileData, this._dfxFileData) = SplitDfqData(dfq_d_FileData);
                    }
                    else
                    {
                        this._dfdFileData = dfq_d_FileData.TrimEnd();
                        try
                        {
                            using (var dfxFs = File.Open(this._dfxFile, FileMode.Open, FileAccess.ReadWrite, FileShare.Delete))
                            {
                                this._dfxDate = File.GetLastWriteTimeUtc(this._dfxFile);
                                if (deleteOldData && this._dfxDate < refreshTs - TimeSpan.FromDays(1))
                                {
                                    File.Delete(this._dfxFile);
                                    File.Delete(this._dfd_q_File);
                                    return false;
                                }
                                using (var dfxSr = new StreamReader(dfxFs, DataSet._globalDefaultEncoding, true))
                                {
                                    this._dfxEncoding = dfxSr.CurrentEncoding;
                                    this._dfxFileData = await dfxSr.ReadToEndAsync();
                                    this._dfxEndPos = dfxFs.Position;
                                }
                            }
                        }
                        catch (FileNotFoundException)
                        {
                            //this is no error for the dfx file, there might be no data                        
                        }
                        catch
                        {
                            FailedToLoadSome = true;
                            return false;
                        }
                        if (String.IsNullOrWhiteSpace(this._dfxFileData) && deleteOldData)
                        {
                            File.Delete(this._dfd_q_File);
                            return false;
                        }
                    }
                    (var valid, var headerResult) = QdasLesen.ParseQdasDfdData(this._dfdFileData);
                    if (!valid)
                    {
                        FailedToLoad = true;
                    }
                    else
                    {
                        this._parsedDfdData = headerResult;
                        this._parsedDfxData = QdasLesen.ParseQdasDFXData(this._dfxFileData, this._parsedDfdData.CharacteristicsData, DataSet._globalQdasSettings);
                        await RemoveTooManyDfxLines(fs, this._datasetSettings.MaxNumberOfValues);                       
                    }
                }
            }
            catch(Exception e)
            {
                _exception = e;
            }
            finally
            {
                this._semaphore.Release();
            }
            return !FailedToLoad;
        }

        internal async Task SetSettings(GeneralSettings datasetSettings)
        {
            await this._semaphore.WaitAsync();
            this._datasetSettings = datasetSettings;
            this._generatedQueryable = null;
            this._semaphore.Release();
        }

        internal async Task UpdateQCOMEncoding(Encoding qcomEcoding)
        {
            await this._semaphore.WaitAsync();
            this._qcomEncoding = qcomEcoding;
            this._generatedQueryable = null;
            this._semaphore.Release();
        }

        /// <summary>
        /// Overwrite dfx data if dfx line limit is exceeded.
        /// </summary>
        /// <param name="dfdFs">Target stream if this is a dfq file, otherwise only a lock</param>
        /// <param name="maxDfxLines">Maximum number of lines which is multuplied by <see cref="DataSet.DELETEDFXMult"/></param>
        /// <returns>Task representing the removal operation</returns>
        internal async Task RemoveTooManyDfxLines(FileStream dfdFs, int maxDfxLines)
        {
            int toRemoveDfxLines = this._parsedDfxData.Data.Count - maxDfxLines * DataSet.DELETEDFXMult;
            if (toRemoveDfxLines <= 0)
                return;

            this._parsedDfxData = new DfxData(this._parsedDfxData.Data.Skip(toRemoveDfxLines).ToList(), this._parsedDfxData.LineData.Skip(toRemoveDfxLines).ToList());
            if (this._isDfqFile)
            {
                if (this._dfxStartPos == 0)
                {
                    dfdFs.SetLength(0);
                    using (var writer = new StreamWriter(dfdFs, this._encoding, 4096, true))
                    {
                        await writer.WriteAsync(this._parsedDfdData.RecognizedRawData[100]);
                        foreach (var line in this._parsedDfdData.RecognizedRawData.Where(kvp => kvp.Key != 100).OrderBy(kvp => kvp.Key).Select(kvp => kvp.Value))
                        {
                            await writer.WriteLineAsync(line);
                        }
                        foreach (var line in this._parsedDfdData.UnrecognizedRawData)
                        {
                            await writer.WriteLineAsync(line);
                        }
                    }
                    await dfdFs.FlushAsync();
                    this._dfxStartPos = dfdFs.Position;
                }
                else
                {
                    dfdFs.SetLength(this._dfxStartPos);
                }
               
                await WriteLinesToFs(dfdFs, this._encoding, this._parsedDfxData.LineData);
                await dfdFs.FlushAsync();
                this._dfxEndPos = dfdFs.Position;
                this._dfxDate = File.GetLastWriteTimeUtc(this._dfd_q_File);
            }
            else
            {
                try
                {
                    using (var dfxFs = File.Open(this._dfxFile, FileMode.Create, FileAccess.Write, FileShare.Delete))
                    {
                        await WriteLinesToFs(dfxFs, this._dfxEncoding, this._parsedDfxData.LineData);
                        await dfxFs.FlushAsync();
                        this._dfxEndPos = dfxFs.Position;
                    }
                    this._dfxDate = File.GetLastWriteTimeUtc(this._dfxFile);
                }
                catch
                {
                    //only reducing file size, not a problem when it cannot be executed
                }
            }
        }

        /// <summary>
        /// Write given dfx lines to a stream using the encoding
        /// </summary>
        /// <param name="fs">Target stream, will stay open</param>
        /// <param name="encoding">Encoding for writing the data</param>
        /// <param name="toWriteLines">Dfx lines to write</param>
        /// <returns>Task representing the operation</returns>
        private async Task WriteLinesToFs(FileStream fs, Encoding encoding, IEnumerable<DfxLine> toWriteLines)
        {
            using (var writer = new StreamWriter(fs, encoding, 4096, true))
            {
                foreach (var line in toWriteLines)
                {
                    if (!string.IsNullOrWhiteSpace(line.MeasurementLine))
                        await writer.WriteLineAsync(line.MeasurementLine);
                    foreach(var entry in line.LineEntries)
                        await writer.WriteLineAsync(entry.Item2);
                }
            }
        }

        internal async Task<bool> ReloadAsync(DateTime refreshTs, bool deleteOldData)
        {
            FailedToLoad = true;
            FailedToLoadSome = false;

            //if there was an error on first LoadAsncy call (or not performed), perform initial load
            if (this._parsedDfdData is null)
                return await LoadAsync(refreshTs, deleteOldData);

            await this._semaphore.WaitAsync();
            bool datasetSettingsChanged = this._datasetSettingsChanged;
            try
            {           
                using (var fs = RetryHelper.DoAndRetry(() => File.Open(this._dfd_q_File, FileMode.Open, FileAccess.ReadWrite, FileShare.Delete), RETRIES, TimeSpan.FromMilliseconds(DELAY)))
                {
                    string newDfxData;
                    if (this._isDfqFile)
                    {
                        DateTime lastWriteTime = File.GetLastWriteTimeUtc(this._dfd_q_File);
                        if (deleteOldData && lastWriteTime < refreshTs - TimeSpan.FromDays(1))
                        {
                            File.Delete(this._dfd_q_File);
                            return false;
                        }
                        else
                        {
                            if (lastWriteTime.Equals(this._dfxDate))
                            {
                                if (datasetSettingsChanged)
                                {
                                    newDfxData = String.Empty;
                                    this._generatedQueryable = null;
                                }
                                else
                                {
                                    return true;
                                }
                            }
                            else
                            {
                                if (this._dfxEndPos > 0)
                                    fs.Seek(this._dfxEndPos, SeekOrigin.Begin);
                                using (var dfxSr = new StreamReader(fs, this._encoding, true))
                                {
                                    newDfxData = await dfxSr.ReadToEndAsync();
                                    this._dfxEndPos = fs.Position;
                                }
                                this._dfxDate = File.GetLastWriteTimeUtc(this._dfd_q_File);
                                this._generatedQueryable = null;
                            }                           
                        }
                    }
                    else
                    {
                        try
                        {
                            using (var dfxFs = File.Open(this._dfxFile, FileMode.Open, FileAccess.ReadWrite, FileShare.Delete))
                            {
                                DateTime lastWriteTime = File.GetLastWriteTimeUtc(this._dfxFile);
                                if (deleteOldData && lastWriteTime < refreshTs - TimeSpan.FromDays(1))
                                {
                                    File.Delete(this._dfxFile);
                                    File.Delete(this._dfd_q_File);
                                    return false;
                                }else
                                {
                                    if (lastWriteTime.Equals(this._dfxDate))
                                    {
                                        if (datasetSettingsChanged)
                                        {
                                            newDfxData = String.Empty;
                                            this._generatedQueryable = null;
                                        }
                                        else
                                        {
                                            return true;
                                        }
                                    }
                                    else
                                    {
                                        dfxFs.Seek(_dfxEndPos, SeekOrigin.Begin);
                                        using (var dfxSr = new StreamReader(dfxFs))
                                        {
                                            newDfxData = await dfxSr.ReadToEndAsync();
                                            this._dfxEndPos = dfxFs.Position;
                                        }
                                        this._dfxDate = File.GetLastWriteTimeUtc(this._dfxFile);
                                        this._generatedQueryable = null;
                                    }                                    
                                }
                            }
                        }
                        catch (FileNotFoundException)
                        {
                            File.Delete(_dfd_q_File);
                            return false;
                        }
                        catch
                        {
                            FailedToLoadSome = true;
                            return true;
                        }
                    }
                    //TODO get and use qdasssettings
                    var newParsedDfxData = QdasLesen.ParseQdasDFXData(newDfxData, this._parsedDfdData.CharacteristicsData, DataSet._globalQdasSettings);
                    this._parsedDfxData = new DfxData(this._parsedDfxData.Data.Concat(newParsedDfxData.Data).ToList(),
                                                    this._parsedDfxData.LineData.Concat(newParsedDfxData.LineData).ToList());
                    await RemoveTooManyDfxLines(fs, this._datasetSettings.MaxNumberOfValues);
                    this._datasetSettingsChanged = false;
                }
            }
            catch (Exception e)
            {
                _exception = e;
                FailedToLoad = true;
            }finally
            {
                this._semaphore.Release();
            }
            //TODO really want to keep the file? will never vanish
            return true;
        }

        public string MainFile
        {
            get { return _dfd_q_File; }
        }

        public bool FailedToLoad
        {
            get;private set;
        }

        public bool FailedToLoadSome
        {
            get; private set;
        }

        internal QdasDfdReadResult ParsedDfdData
        {
            get { return _parsedDfdData; }
        }

        internal DfxData ParsedDfxData
        {
            get { return _parsedDfxData; }
        }

        public DateTime LastChangeTime { get { return _dfxDate; } }


        /// <summary>
        /// Generate queryable item for the processed data.
        /// </summary>
        /// <returns>Task with the result</returns>
        public async Task<QueryableQsData> GetQueryableQsDataAsync()
        {        
            await _semaphore.WaitAsync();
            try
            {
                if (this._generatedQueryable is object)
                    return this._generatedQueryable;

                byte[] dfd, dfx, preamble, total, newline;
                bool skipDfxData = this._parsedDfxData.Data.Count < this._datasetSettings.MinimalNumberOfValues;
                Encoding encoding = this._qcomEncoding;
                preamble = encoding.GetPreamble();
                if (this._datasetSettings.CpCpkFromPlanning)
                {
                    dfd = encoding.GetBytes(_dfdFileData);
                }
                else
                {
                    //TODO should not be generated again if only the dfx part changed
                    dfd = encoding.GetBytes(GenerateDfdData(this._parsedDfdData, this._datasetSettings));
                }
                newline = encoding.GetBytes("\r\n");
                DateTime? oldestTime = null;
                if (this._datasetSettings.EnableTimeLimit)
                    oldestTime = DateTime.UtcNow - TimeSpan.FromDays(this._datasetSettings.MaxEvalTimeSpan);
                if (skipDfxData)
                {
                    dfx = new byte[0];
                }
                else
                {
                    dfx = encoding.GetBytes(GenerateDfxData(this._parsedDfxData, this._datasetSettings.ShowDeletedMeasurements, this._datasetSettings.MaxNumberOfValues, oldestTime));
                }
                
                long totalLength = preamble.LongLength + dfd.Length + newline.Length + dfx.Length;
                if (totalLength > int.MaxValue)
                {
                    total = new byte[0];
                }
                else
                {
                    total = new byte[totalLength];
                    int offset = 0;
                    preamble.CopyTo(total, offset);
                    offset += preamble.Length;
                    dfd.CopyTo(total, offset);
                    offset += dfd.Length;
                    newline.CopyTo(total, offset);
                    offset += newline.Length;
                    dfx.CopyTo(total, offset);
                }
                IReadOnlyList<TS_MERKMAL_IST[]> measuringData;
                if (skipDfxData)
                {
                    measuringData = new List<TS_MERKMAL_IST[]>();
                }
                else if (this._datasetSettings.MaxNumberOfValues < this._parsedDfxData.Data.Count)
                {
                    measuringData = this._parsedDfxData.Data.Skip(this._parsedDfxData.Data.Count - this._datasetSettings.MaxNumberOfValues).ToList();
                }
                else
                {
                    measuringData = this._parsedDfxData.Data;
                }
                this._generatedQueryable = new QueryableQsData(this._parsedDfdData.PartsData, this._parsedDfdData.CharacteristicsData, measuringData, total, this._dfxDate);
                return this._generatedQueryable;
            }
            finally
            {
                this._semaphore.Release();
            }            
        }

        /// <summary>
        /// Generate dfx text from parsed data, applying replacements and limitations.
        /// </summary>
        /// <param name="parsedDfxData">Parsed data to transform</param>
        /// <param name="replaceDeletedAttribute">Replace filling attribute (used as deleted) with empty?</param>
        /// <param name="maxDfxLines">Max number of dfx lines to write</param>
        /// <param name="oldestAllowedDataTs">Time filter applied to the max number of dfxlines</param>
        /// <returns>Transformed dfx content as text</returns>
        internal static string GenerateDfxData(DfxData parsedDfxData, bool replaceDeletedAttribute, int maxDfxLines, DateTime? oldestAllowedDataTs)
        {
            StringBuilder sb = new StringBuilder();
            string[] measurementSplit;
            string[] characteristicsSplit;
            DfxLine lineData;
            TS_MERKMAL_IST[] parsedData;
            bool requiresReplacement;
            int firstLine = Math.Max(0, parsedDfxData.Data.Count - maxDfxLines);
            for (int i=firstLine; i<parsedDfxData.Data.Count;i++)
            {
                lineData = parsedDfxData.LineData[i];
                parsedData = parsedDfxData.Data[i];
                requiresReplacement = replaceDeletedAttribute && parsedData.Any(chara => chara.iAttribut == FILLING_DELETED_ATTRIBUTE);
                if (!String.IsNullOrWhiteSpace(lineData.MeasurementLine))
                {
                    if (requiresReplacement)
                    {
                        measurementSplit = lineData.MeasurementLine.Split(QdasLesen.CharacteristicSeparator);
                        Debug.Assert(measurementSplit.Length == parsedData.Length);
                        for (int j = 0; j < measurementSplit.Length && j < parsedData.Length; j++)
                        {
                            if (parsedData[j].iAttribut != FILLING_DELETED_ATTRIBUTE)
                                continue;

                            characteristicsSplit = measurementSplit[j].Split(QdasLesen.AdditionalDataSeparator);
                            switch (parsedData[j].iMerkmal_Art)
                            {
                                case (short) iMerkmal_Art.imaATTRIBUTIV when characteristicsSplit.Length >= 3:
                                    characteristicsSplit[2] = EMPTY_ATTRIBUTE_TEXT;
                                    measurementSplit[j] = String.Join(QdasLesen.AdditionalDataSeparator + "", characteristicsSplit);
                                    break;
                                case (short)iMerkmal_Art.imaVARIABEL when characteristicsSplit.Length >= 2:
                                    characteristicsSplit[1] = EMPTY_ATTRIBUTE_TEXT;
                                    measurementSplit[j] = String.Join(QdasLesen.AdditionalDataSeparator + "", characteristicsSplit);
                                    break;
                            }
                        }
                        sb.AppendLine(String.Join(QdasLesen.CharacteristicSeparator+"", measurementSplit));
                    }
                    else
                    {
                        sb.AppendLine(lineData.MeasurementLine);
                    }
                }
                foreach (var lineEntry in lineData.LineEntries)
                {
                    switch (lineEntry.Item1)
                    {
                        case ATTRIBUTE when requiresReplacement:
                            int spaceIndex = lineEntry.Item2.IndexOf(' ');
                            if (spaceIndex > 0)
                            {
                                if (short.TryParse(lineEntry.Item2.Substring(spaceIndex+1), out short attribute) && attribute == FILLING_DELETED_ATTRIBUTE)
                                {
                                    sb.AppendLine($"{lineEntry.Item2.Substring(0, spaceIndex+1)}{EMPTY_ATTRIBUTE_TEXT}");
                                    break;
                                }
                            }
                            sb.AppendLine(lineEntry.Item2);
                            break;
                        default:
                            sb.AppendLine(lineEntry.Item2);
                            break;
                    }
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Generate dfd data from parsed data and replace K8520-K8523 according to provided settings
        /// </summary>
        /// <param name="parsedDfdData">Parsed data</param>
        /// <param name="datasetSettings">Settings with replacement information (cp_k values)</param>
        /// <returns>Generated dfd data as text</returns>
        private string GenerateDfdData(QdasDfdReadResult parsedDfdData, GeneralSettings datasetSettings)
        {
            var sb = new StringBuilder();
            if (!parsedDfdData.RecognizedRawData.TryGetValue(TOTAL_NUMBER_CHARACTERISTICS, out string line))
            {
                return String.Empty;
            }
            sb.AppendLine(line);
            foreach(var kvp in parsedDfdData.RecognizedRawData.OrderBy(kvp => kvp.Key))
            {
                switch (kvp.Key)
                {
                    case TOTAL_NUMBER_CHARACTERISTICS: break;//already written
                    case REQUIRED_CPK_VALUE:
                        if (datasetSettings.CpCpkFromPlanning)
                        {
                            sb.AppendLine(kvp.Value);
                        }
                        else
                        {
                            if (datasetSettings.CpCpkReplaceMissing)
                            {
                                foreach( var characteristic in parsedDfdData.CharacteristicsData)
                                {
                                    sb.AppendLine($"K{kvp.Key:D4}/{characteristic.CharacteristicIndex} {characteristic.dCp_Wert_gefordert?? datasetSettings.EndangeredCp}");
                                }
                            }
                            else
                            {
                                sb.AppendLine($"K{kvp.Key:D4}/0 {datasetSettings.EndangeredCp}");
                            }
                        }
                        break;
                    case REQUIRED_CP_VALUE:
                        if (datasetSettings.CpCpkFromPlanning)
                        {
                            sb.AppendLine(kvp.Value);
                        }
                        else
                        {
                            if (datasetSettings.CpCpkReplaceMissing)
                            {
                                foreach (var characteristic in parsedDfdData.CharacteristicsData)
                                {
                                    sb.AppendLine($"K{kvp.Key:D4}/{characteristic.CharacteristicIndex} {characteristic.dCpk_Wert_gefordert ?? datasetSettings.EndangeredCpk}");
                                }
                            }
                            else
                            {
                                sb.AppendLine($"K{kvp.Key:D4}/0 {datasetSettings.EndangeredCpk}");
                            }
                        }
                        break;
                    case FIXED_CPK_VALUE:
                        if (datasetSettings.CpCpkFromPlanning)
                        {
                            sb.AppendLine(kvp.Value);
                        }
                        else
                        {
                            if (datasetSettings.CpCpkReplaceMissing)
                            {
                                foreach (var characteristic in parsedDfdData.CharacteristicsData)
                                {
                                    sb.AppendLine($"K{kvp.Key:D4}/{characteristic.CharacteristicIndex} {characteristic.dCp_Wert_gefixt ?? datasetSettings.OutOfControlCp}");
                                }
                            }
                            else
                            {
                                sb.AppendLine($"K{kvp.Key:D4}/0 {datasetSettings.OutOfControlCp}");
                            }
                        }
                        break;
                    case FIXED_CP_VALUE:
                        if (datasetSettings.CpCpkFromPlanning)
                        {
                            sb.AppendLine(kvp.Value);
                        }
                        else
                        {
                            if (datasetSettings.CpCpkReplaceMissing)
                            {
                                foreach (var characteristic in parsedDfdData.CharacteristicsData)
                                {
                                    sb.AppendLine($"K{kvp.Key:D4}/{characteristic.CharacteristicIndex} {characteristic.dCpk_Wert_gefixt ?? datasetSettings.OutOfControlCpk}");
                                }
                            }
                            else
                            {
                                sb.AppendLine($"K{kvp.Key:D4}/0 {datasetSettings.OutOfControlCpk}");
                            }
                        }
                        break;
                    default:
                        sb.AppendLine(kvp.Value);
                        break;

                }
            }
            foreach (var entry in parsedDfdData.UnrecognizedRawData)
            {
                sb.AppendLine(entry);
            }
            return sb.ToString();
        }

        public Exception LastException{
            get { return _exception; }
            }

        internal static (string dfdData, string dfxData) SplitDfqData(string data)
        {
            string line;
            var dfdSb = new StringBuilder();
            string dfxData;
            using (var reader = new StringReader(data))
            {
                while((line=reader.ReadLine())!=null)
                {
                    if (IsDfdLine(line))
                        dfdSb.AppendLine(line);
                    else
                        break;
                }
                if (line != null)
                {
                    dfxData = line;
                    var rest = reader.ReadToEnd();
                    if (!String.IsNullOrEmpty(rest))
                    {
                        dfxData += $"\r\n{rest}";
                    }
                }
                else
                {
                    dfxData = String.Empty;
                }
            }
            string dfd = dfdSb.ToString().TrimEnd();
            return (dfd, dfxData);
        }

        private static bool IsDfdLine(string line)
        {
            if (line == null)
                return false;
            string trimmedLine = line.TrimStart();
            if (trimmedLine.Length == 0)
                return true;
            if (trimmedLine.StartsWith("K00", StringComparison.OrdinalIgnoreCase) || !trimmedLine.StartsWith("K", StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }
            return true;
        }

        #region IDisposable
        private bool _isDisposed = false;

        public void Dispose()
        {
            if (this._isDisposed)
                return;

            this._semaphore.Dispose();
            this._isDisposed = true;
        }
        #endregion
    }
}
