﻿using Promess.Pbld.Data.Settings.PresetEditorItems;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Base for image editor items. Image either from a file or from qsStat evaluation.
    /// </summary>
    [DataContract(Name = "ImageEditorItem")]
    public class ImageEditorItem:RectangleEditorItem, IPresetRepresentationGraphic
    {
        #region fields
        [DataMember(Name = "BorderType")]
        private BorderType _borderType;
        [DataMember(Name = "ImageSourceType")]
        private ImageSourceType _source;
        [DataMember(Name = "QdasEvaluation")]
        private int? _evaluationCode;
        [DataMember(Name = "ImagePath")]
        private string _imagePath;
        [DataMember(Name = "Image")]
        private byte[] _image;
        [DataMember(Name = "HorizontalAlignment")]
        private HorizontalAlignment _horizontalAlignment;
        [DataMember(Name = "VerticalAlignment")]
        private VerticalAlignment _verticalAlignment;
        [DataMember(Name = "Stretch")]
        private Stretch _stretch;
        #endregion

        #region presentation
        public BorderType Border
        {
            get { return _borderType; }
            set
            {
                SetProperty(ref _borderType, value);
            }
        }

        public HorizontalAlignment HorizontalAlignment
        {
            get { return _horizontalAlignment; }
            set
            {
                SetProperty(ref _horizontalAlignment, value);
            }
        }

        public VerticalAlignment VerticalAlignment
        {
            get { return _verticalAlignment; }
            set
            {
                SetProperty(ref _verticalAlignment, value);
            }
        }

        public Stretch Stretch
        {
            get { return _stretch; }
            set
            {
                SetProperty(ref _stretch, value);
            }
        }
        #endregion

        public ImageSourceType ImageSourceType
        {
            get
            {
                return _source;
            }
            set
            {
                SetProperty(ref _source, value);
            }
        }

        public int? EvaluationCode
        {
            get
            {
                return _evaluationCode;
            }

            set
            {
                SetProperty(ref _evaluationCode, value);
            }
        }

        public string ImagePath
        {
            get
            {
                return _imagePath;
            }

            set
            {
                SetProperty(ref _imagePath, value);
            }
        }

        public byte[] Image
        {
            get
            {
                return _image;
            }

            set
            {
                SetProperty(ref _image, value);
            }
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(ImageSourceType)))
            {
                ClearUnusedEntriesForSourceType();
            }
            base.OnPropertyChanged(e);
        }

        protected void ClearUnusedEntriesForSourceType()
        {
            switch (ImageSourceType)
            {
                case ImageSourceType.File:
                    EvaluationCode = null;
                    break;
                case ImageSourceType.Qdas:
                    ImagePath = String.Empty;
                    Image = null;
                    break;
                default:
                    throw new NotImplementedException($"Case {ImageSourceType} not implemented for {nameof(ImageSourceType)}");
            }
        }
    }
}
