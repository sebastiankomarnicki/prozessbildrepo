﻿namespace Promess.Pbld.Data.Editor
{
    public enum VerticalAlignment
    {
        Top,
        Middle,
        Bottom
    }
}
