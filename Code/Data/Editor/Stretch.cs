﻿namespace Promess.Pbld.Data.Editor
{
    public enum Stretch
    {
        None,
        Uniform,
        UniformToFill,
        Fill
    }
}
