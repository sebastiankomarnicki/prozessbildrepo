﻿using System.Runtime.Serialization;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Wrapper for a requested data point. Either belongs to qsStat/AQDEF or custom definition
    /// </summary>
    [DataContract(Name = "QdasNumber")]
    public sealed class QdasNumber
    {
        [DataMember(Name = "EvaluationType")]
        private TextEvaluationType _evalType;
        [DataMember(Name = "EvaluationNumber")]
        private int _evalNumber;

        public QdasNumber(TextEvaluationType evalType, int evalNumber)
        {
            this._evalType = evalType;
            this._evalNumber = evalNumber;
        }

        public TextEvaluationType EvaluationType
        {
            get { return _evalType; }
        }

        public int EvaluationNumber
        {
            get { return _evalNumber; }
        }

    }
}
