﻿using Promess.Pbld.Data.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Base for rectangular editor items with common behavior
    /// </summary>
    [DataContract(Name = "RectangleEditorItem")]
    public class RectangleEditorItem: EditorItemBase
    {
        private double _actualHeight;       
        private double _actualWidth;
        [DataMember(Name = "RoundedCorners")]
        private bool _roundedCorners;
        [DataMember(Name = "BackColor")]
        private Color _backColor;
        [DataMember(Name = "BorderColor")]
        private Color _borderColor;
        [DataMember(Name = "BorderThickness")]
        private int _borderThickness;
        [DataMember(Name = "ShowBorder")]
        private bool _showBorder;

        public RectangleEditorItem():base()
        {
            _backColor = Colors.Transparent;
            _borderColor = SystemColors.ActiveBorderColor;
            _borderThickness = 1;
            _showBorder = true;
            _actualHeight = _actualWidth = Double.NaN;
            PercentageWidth = MINDIMENSIONPERCENTAGE;
            PercentageHeight = MINDIMENSIONPERCENTAGE;
        }

        [DataMember(Name = "PercentageHeight")]
        [Range(MINDIMENSIONPERCENTAGE,1, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double PercentageHeight
        {
            get;set;
        }

        [DataMember(Name = "PercentageWidth")]
        [Range(MINDIMENSIONPERCENTAGE,1, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double PercentageWidth
        {
            get;set;
        }

        public double Height
        {
            get
            {
                return _actualHeight;
            }
            set
            {
                SetProperty(ref _actualHeight, value);
            }
        }

        public double Width
        {
            get
            {
                return _actualWidth;
            }
            set
            {
                SetProperty(ref _actualWidth, value);
            }
        }

        public bool RoundedCorners
        {
            get
            {
                return _roundedCorners;
            }
            set
            {
                SetProperty(ref _roundedCorners, value);
            }
        }

        public Color BackColor
        {
            get { return _backColor; }
            set { SetProperty(ref _backColor, value); }
        }

        public Color BorderColor
        {
            get { return _borderColor; }
            set { SetProperty(ref _borderColor, value); }
        }

        public bool ShowBorder
        {
            get { return _showBorder; }
            set {
                SetProperty(ref _showBorder, value);
            }
        }

        //if this is ever set by a user add validation (maybe 1 to 5?)
        public int BorderThickness
        {
            get { return _borderThickness; }
            set { SetProperty(ref _borderThickness, value); }
        }

        public override Rect GetBoundingRect()
        {
            return new Rect(X, Y, Width, Height);
        }

        public override void ShiftPosition(Vector delta)
        {
            X += delta.X;
            Y += delta.Y;
        }
    }
}
