﻿namespace Promess.Pbld.Data.Editor
{
    public enum LineWidth
    {
        Thin,
        Medium,
        Thick
    }
}
