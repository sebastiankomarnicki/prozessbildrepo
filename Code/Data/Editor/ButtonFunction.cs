﻿namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Abstraction for different button functions
    /// </summary>
    public enum ButtonFunction
    {
        NextCharacteristic,
        PreviousCharacteristic,
        NextDataset,
        PreviousDataset,
        Refresh
    }
}
