﻿namespace Promess.Pbld.Data.Editor
{
    public enum AspectRatio
    {
        None=0,
        FourToThree=1,
        SixteenToTen=2,
        SixteenToNine=3
    }
}
