﻿using Promess.Pbld.Data.Settings.PresetEditorItems;
using System;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Editor item for a line
    /// </summary>
    [DataContract(Name = "LineEditorItem")]
    public class LineEditorItem : EditorItemBase, IPresetLine
    {     
        private double _x1;      
        private double _y1;     
        private double _x2;       
        private double _y2;
        [DataMember(Name = "P1LineEnding")]
        private LineEnding _p1LineEnding;
        [DataMember(Name = "P2LineEnding")]
        private LineEnding _p2LineEnding;
        [DataMember(Name = "LineColor")]
        private Color _lineColor;
        [DataMember(Name = "LineWidth")]
        private LineWidth _lineWidth;

        public LineEditorItem():base()
        {
            _p1LineEnding = LineEnding.None;
            _p2LineEnding = LineEnding.None;
            _lineWidth = LineWidth.Medium;
            _lineColor = Color.FromRgb(0, 0, 0);
            _x1 = _x2 = _y1 = _y2 = Double.NaN;
            PercentageX2 = MINDIMENSIONPERCENTAGE;
            PercentageY2 = MINDIMENSIONPERCENTAGE;
        }

        [DataMember(Name = "PercentageX1")]
        public double PercentageX1
        {
            get;set;
        }

        [DataMember(Name = "PercentageX2")]
        public double PercentageX2
        {
            get;set;
        }

        [DataMember(Name = "PercentageY1")]
        public double PercentageY1
        {
            get; set;
        }

        [DataMember(Name = "PercentageY2")]
        public double PercentageY2
        {
            get; set;
        }

        public double X1
        {
            get
            {
                return _x1;
            }
            set
            {
                SetProperty(ref _x1, value);
            }
        }

        public double Y1
        {
            get
            {
                return _y1;
            }
            set
            {
                SetProperty(ref _y1, value);
            }
        }

        public double X2
        {
            get
            {
                return _x2;
            }
            set
            {
                SetProperty(ref _x2, value);
            }
        }

        public double Y2
        {
            get
            {
                return _y2;
            }
            set
            {
                SetProperty(ref _y2, value);
            }
        }
   
        public LineEnding P1LineEnding
        {
            get { return _p1LineEnding; }
            set { SetProperty(ref _p1LineEnding, value); }
        }

        public LineEnding P2LineEnding
        {
            get { return _p2LineEnding; }
            set { SetProperty(ref _p2LineEnding, value); }
        }

        public Color LineColor
        {
            get { return _lineColor; }
            set { SetProperty(ref _lineColor, value); }
        }

        public LineWidth LineWidth
        {
            get { return _lineWidth; }
            set { SetProperty(ref _lineWidth, value); }
        }

        public override void ShiftPosition(Vector delta)
        {
            X += delta.X;
            Y += delta.Y;
        }

        public override Rect GetBoundingRect()
        {
            var tmpX = Math.Min(X1, X2);
            var tmpY = Math.Min(Y1, Y2);

            return new Rect(X + tmpX, Y + tmpY, Math.Abs(X1 - X2), Math.Abs(Y1 - Y2));
        }
    }
}
