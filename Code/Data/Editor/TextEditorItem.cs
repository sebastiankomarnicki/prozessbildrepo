﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using Promess.Pbld.Data.Settings.PresetEditorItems;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Text editor item with common behavior
    /// </summary>
    [DataContract(Name = "TextEditorItem")]
    public class TextEditorItem:RectangleEditorItem, IPresetRepresentationText, IPresetFontAndColor, IPresetFontAndColorExtended
    {
        #region fields
        [DataMember (Name = "Text")]
        private string _text;
        [DataMember (Name = "QdasNumber")]
        private QdasNumber _qdasNumber;
        [DataMember (Name = "TextType")]
        private TextType _textType;
        [DataMember(Name = "HorizontalAlignment")]
        private HorizontalAlignment _horizontalAlignment;
        [DataMember(Name = "VerticalAlignment")]
        private VerticalAlignment _verticalAlignment;
        [DataMember(Name = "OverwriteBackground")]
        private bool _overwriteBackground;
        private FontFamily _font;
        [DataMember(Name = "FontFamily")]
        private string _fontFamilyRepForSerialization;
        [DataMember(Name = "FontColor")]
        private Color _fontColor;
        [DataMember(Name = "BorderType")]
        private BorderType _borderType;
        [DataMember(Name = "ScaleFont")]
        private bool _scaleFont;
        [DataMember(Name = "FontSize")]
        private int _fontSize;
        [DataMember(Name = "BoldFont")]
        private bool _boldFont;
        [DataMember(Name = "ItalicFont")]
        private bool _italicFont;
        #endregion

        public TextEditorItem():base()
        {
            _text = string.Empty;
            _textType = TextType.UserText;
            _horizontalAlignment = HorizontalAlignment.Left;
            _verticalAlignment = VerticalAlignment.Middle;
            _overwriteBackground = false;
            //TODO get default font from somewhere, include System.Drawing again
            //_font = System.Drawing.SystemFonts.DefaultFont;
            _font = new FontFamily("Verdana");
            _fontColor = SystemColors.WindowTextColor;
            _borderType = BorderType.Flat;
            _scaleFont = false;
            _fontSize = 12;
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals(nameof(TextType)))
            {
                if (TextType != TextType.UserText)
                {
                    Text = "";
                }
            }
            base.OnPropertyChanged(e);
        }

        #region label

        public string Text
        {
            get
            {
                return _text;
            }

            set
            {
                SetProperty(ref _text, value);
            }
        }

        public QdasNumber QdasNumber
        {
            get
            {
                return _qdasNumber;
            }

            set
            {
                SetProperty(ref _qdasNumber, value);
            }
        }

        public TextType TextType
        {
            get { return _textType; }
            set
            {
                SetProperty(ref _textType, value);
            }
        }
        #endregion

        #region presentation
        public HorizontalAlignment HorizontalAlignment
        {
            get { return _horizontalAlignment; }
            set
            {
                SetProperty(ref _horizontalAlignment, value);
            }
        }

        public VerticalAlignment VerticalAlignment
        {
            get { return _verticalAlignment; }
            set
            {
                SetProperty(ref _verticalAlignment, value);
            }
        }
        public BorderType Border
        {
            get { return _borderType; }
            set
            {
                SetProperty(ref _borderType, value);
            }
        }
        #endregion

        #region font & color
        public bool OverwriteBackground
        {
            get { return _overwriteBackground; }
            set { SetProperty(ref _overwriteBackground, value); }
        }

        public FontFamily Font
        {
            get { return _font; }
            set { SetProperty(ref _font, value); }
        }

        public Color FontColor
        {
            get { return _fontColor; }
            set { SetProperty(ref _fontColor, value); }
        }

        public bool ScaleFont
        {
            get { return _scaleFont; }
            set { SetProperty(ref _scaleFont, value); }
        }

        public int FontSize
        {
            get { return _fontSize; }
            set { SetProperty(ref _fontSize, value); }
        }

        public bool BoldFont
        {
            get { return _boldFont; }
            set { SetProperty(ref _boldFont, value); }
        }

        public bool ItalicFont
        {
            get { return _italicFont; }
            set { SetProperty(ref _italicFont, value); }
        }
        #endregion

        #region serialization
        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            _fontFamilyRepForSerialization = new FontFamilyConverter().ConvertToInvariantString(_font);
        }

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            _font = (FontFamily) new FontFamilyConverter().ConvertFromInvariantString(_fontFamilyRepForSerialization);
        }
        #endregion
    }
}
