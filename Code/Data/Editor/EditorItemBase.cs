﻿using Promess.Pbld.Data.GraphicPage;
using Promess.Pbld.Data.Schema;
using Promess.Pbld.Data.Validation;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Waf.Foundation;
using System.Windows;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Base for editor items with position and Z-Index. Abstract Methods need to be implemented.
    /// </summary>
    [DataContract]
    [KnownType(typeof(TextEditorItem)), KnownType(typeof(ImageEditorItem)),KnownType(typeof(EllipseEditorItem)),
     KnownType(typeof(LineEditorItem)), KnownType(typeof(CircleEditorItem)),KnownType(typeof(RectangleEditorItem)),
     KnownType(typeof(ButtonEditorItem)), KnownType(typeof(ImageGPEditorItem)), KnownType(typeof(TextGPEditorItem)),
     KnownType(typeof(SchemaItem))]
    public abstract class EditorItemBase: ValidatableModel, IEditorItem, IExtensibleDataObject
    {
        public const double MINDIMENSIONPERCENTAGE = 0.025;
        #region fields
        private double _actualX;
        private double _actualY;
        [DataMember(Name = "ZIndex")]
        private int _zindex;
        #endregion

        public EditorItemBase()
        {
            _actualX = _actualY = Double.NaN;
            _zindex = 0;
        }

        [DataMember(Name = "PercentageX")]
        [Range(0, 1 - MINDIMENSIONPERCENTAGE, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double PercentageX
        {
            get; set;
        }

        [DataMember(Name = "PercentageY")]
        [Range(0, 1 - MINDIMENSIONPERCENTAGE, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double PercentageY
        {
            get; set;
        }

        public double X
        {
            get
            {
                return _actualX;
            }
            set
            {
                SetProperty(ref _actualX, value);
            }
        }

        public double Y
        {
            get
            {
                return _actualY;
            }
            set
            {
                SetProperty(ref _actualY, value);
            }
        }

        public int ZIndex {
            get { return _zindex; }
            set {
                SetProperty(ref _zindex, value);
            }
        }

        public abstract Rect GetBoundingRect();
        public abstract void ShiftPosition(Vector delta);

        #region dirty flag
        public bool IsDirty { get; set; }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            IsDirty = true;
        }
        #endregion

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion

        #region serialization
        [OnDeserialized()]
        private void OnDeserialized(StreamingContext context)
        {
            //usage of property setter during deserialization leads to set dirty
            this.IsDirty = false;
        }
        #endregion
    }
}
