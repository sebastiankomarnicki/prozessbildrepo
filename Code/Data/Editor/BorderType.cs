﻿namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Abstraction for different types of borders
    /// </summary>
    public enum BorderType
    {
        Flat,
        Raised,
        Lowered,
        Borderless
    }
}
