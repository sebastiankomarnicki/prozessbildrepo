﻿namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Abstraction for horizontal alignment
    /// </summary>
    public enum HorizontalAlignment
    {
        Left,
        Right,
        Center
    }
}
