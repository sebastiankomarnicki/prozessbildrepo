﻿namespace Promess.Pbld.Data.Editor
{
    public interface IEditorItem
    {
        double X { get; set; }
        double Y { get; set; }
        int ZIndex { get; set; }
        bool IsDirty { get; set; }
    }
}
