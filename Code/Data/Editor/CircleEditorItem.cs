﻿using System;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;

namespace Promess.Pbld.Data.Editor
{
    [DataContract(Name ="CircleEditorItem")]
    public class CircleEditorItem : EditorItemBase
    {
        private double _actualDiameter;
        [DataMember(Name = "BackColor")]
        private Color _backColor;
        [DataMember(Name = "BorderColor")]
        private Color _borderColor;
        [DataMember(Name = "BorderThickness")]
        private int _borderThickness;
        [DataMember(Name = "ShowBorder")]
        private bool _showBorder;

        public CircleEditorItem():base()
        {
            _backColor = Colors.Transparent;
            _borderColor = SystemColors.ActiveBorderColor;
            _borderThickness = 1;
            _showBorder = true;
            _actualDiameter = Double.NaN;
            PercentageDiameter = MINDIMENSIONPERCENTAGE;
        }

        [DataMember(Name = "PercentageDiameter")]
        public double PercentageDiameter
        {
            get; set;
        }

        public double Diameter
        {
            get
            {
                return _actualDiameter;
            }
            set
            {
                SetProperty(ref _actualDiameter, value);
            }
        }

        public Color BackColor
        {
            get { return _backColor; }
            set { SetProperty(ref _backColor, value); }
        }

        public Color BorderColor
        {
            get { return _borderColor; }
            set { SetProperty(ref _borderColor, value); }
        }

        public int BorderThickness
        {
            get { return _borderThickness; }
            set { SetProperty(ref _borderThickness, value); }
        }

        public bool ShowBorder
        {
            get { return _showBorder; }
            set { SetProperty(ref _showBorder, value); }
        }

        public override Rect GetBoundingRect()
        {
            return new Rect(X, Y, Diameter, Diameter);
        }

        public override void ShiftPosition(Vector delta)
        {
            X += delta.X;
            Y += delta.Y;
        }
    }
}
