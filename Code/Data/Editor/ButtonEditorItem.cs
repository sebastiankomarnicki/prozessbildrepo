﻿using System.Runtime.Serialization;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Editor item for a button with specific functions
    /// </summary>
    [DataContract(Name = "ButtonEditorItem")]
    public class ButtonEditorItem:TextEditorItem
    {
        [DataMember(Name = "ButtonFunction")]
        private ButtonFunction _itemsFunction;

        /// <summary>
        /// Identifier for the function to execute when the button is clicked, notifies on change
        /// </summary>
        public ButtonFunction Function
        {
            get
            {
                return _itemsFunction;
            }
            set
            {
                SetProperty(ref _itemsFunction, value);
            }
        }
    }
}
