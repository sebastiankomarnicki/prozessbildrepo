﻿using Promess.Pbld.Data.Validation;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Waf.Foundation;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Wrapper for to retrieve data point with custom name
    /// </summary>
    [DataContract(Name = "QdasNumberWrapper")]
    public sealed class QdasNumberWrapper:ValidatableModel
    {
        [DataMember(Name = "CustomName")]
        private string _customName;
        [DataMember(Name = "QdasNumber")]
        private QdasNumber _qdasNumber;

        public QdasNumberWrapper(TextEvaluationType text, int number):this("", new QdasNumber(text, number))
        { }

        public QdasNumberWrapper(string customName, QdasNumber qdasNumber)
        {
            this._customName = customName;
            this._qdasNumber = qdasNumber;
        }

        public QdasNumber QdasNumber
        {
            get { return _qdasNumber; }
        }

        [StringLength(20, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationStringLengthError))]
        public string CustomName
        {
            get { return _customName; }
            set { SetPropertyAndValidate(ref _customName, value); }
        }
    }
}
