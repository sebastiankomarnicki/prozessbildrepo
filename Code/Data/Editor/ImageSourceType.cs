﻿namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Abstraction for an image type
    /// </summary>
    public enum ImageSourceType
    {
        File,
        Qdas
    }
}
