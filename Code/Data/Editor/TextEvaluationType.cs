﻿namespace Promess.Pbld.Data.Editor
{
    public enum TextEvaluationType
    {
        NumericAndTextResults,
        Part,
        Characteristic,
        AdditionalPart,
        AdditionalCharacteristic
    }
}
