﻿using Promess.Common.Util;
using Promess.Pbld.Data.Validation;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Waf.Foundation;

namespace Promess.Pbld.Data.Editor
{
    /// <summary>
    /// Model for an editable panel
    /// </summary>
    [DataContract(Name = "PanelItemModel")]
    public class PanelItemModel:Model
    {
        [DataMember(Name = "PanelName")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        private string _name;
        [DataMember(Name = "PanelItems")]
        private ObservableCollection<EditorItemBase> _items;
        [DataMember(Name = "AspectRatio")]
        private AspectRatio _aspectRatio;

        private bool _isDirty;

        public PanelItemModel(string name) : this()
        {
            this._name = name;
            this._isDirty = true;
        }

        private PanelItemModel()
        {
            _items = new ObservableCollection<EditorItemBase>();
            CollectionChangedEventManager.AddHandler(_items, ItemsCollectionChanged);
        }

        public String Name
        {
            get { return _name; }
            private set
            {
                SetProperty(ref _name, value);
            }
        }

        public ObservableCollection<EditorItemBase> Items
        {
            get { return _items; }
            private set
            {
                SetProperty(ref _items, value);
            }
        }

        public AspectRatio AspectRatio
        {
            get { return _aspectRatio; }
            set { SetProperty(ref _aspectRatio, value); }
        }
        
        #region dirty flag
        public bool GetIsDirty()
        {
            if (_isDirty)
                return true;
            return _items.Any(elem => elem.IsDirty);
        }

        public void ResetDirty()
        {
            _isDirty = false;
            foreach (var elem in _items)
                elem.IsDirty = false;
        }

        /// <summary>
        /// This serializes the object from and to memory. Exceptions are not caught during this process
        /// </summary>
        /// <param name="target"></param>
        public void DeepCopyItemsInto(PanelItemModel target)
        {
            target.Items = SerializeHelper.CreateDataContractCopy(this.Items);
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            this._isDirty = true;
        }
        private void ItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this._isDirty = true;
        }

        #endregion

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            if (_items == null)
                _items = new ObservableCollection<EditorItemBase>();
            CollectionChangedEventManager.AddHandler(_items, ItemsCollectionChanged);
        }
    }
}
