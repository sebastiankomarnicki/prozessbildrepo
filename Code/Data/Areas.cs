﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Waf.Foundation;

namespace Promess.Pbld.Data
{
    /// <summary>
    /// Collection of defined <see cref="Area"/>s. Enforces constraints for operation on items and selection logic. 
    /// </summary>
    [DataContract(Name = "Areas")]
    public class Areas:Model, IExtensibleDataObject
    {
        [DataMember(Name = "Areas")]
        private ObservableCollection<Area> _areas;
        private ReadOnlyObservableCollection<Area> _availableAreas;
        [DataMember(Name = "SelectedAreaName")]
        private String _selectedAreaName;
        private Area _selectedArea;

        public Areas()
        {
            _areas = new ObservableCollection<Area>();
            _availableAreas = new ReadOnlyObservableCollection<Area>(_areas);
            _selectedAreaName = null;
        }

        public String SelectedAreaName
        {
            get { return _selectedAreaName; }
            private set
            {
                SetProperty(ref _selectedAreaName, value);
            }
        }

        public Area SelectedArea
        {
            get { return _selectedArea; }
            private set
            {
                SetProperty(ref _selectedArea, value);
            }
        }

        public ReadOnlyObservableCollection<Area> AvailableAreas
        {
            get { return _availableAreas; }
        }

        public bool DoesAreaNameAlreadyExist(string trimmedName)
        {
            return _areas.Any(area => String.Equals(area.Name, trimmedName, StringComparison.OrdinalIgnoreCase));
        }

        public bool TryAddArea(Area toAdd)
        {
            if (DoesAreaNameAlreadyExist(toAdd.Name))
            {
                return false;
            }else
            {
                _areas.Add(toAdd);
                return true;
            }
        }

        public bool TryRenameArea(Area area, string trimmedName)
        {
            if (area==null || !_areas.Contains(area))
                return false;
            if (trimmedName.Length == 0 || trimmedName.Length>50 || DoesAreaNameAlreadyExist(trimmedName))
                return false;

            area.Name = trimmedName;
            if (area.Equals(SelectedArea))
                SelectedAreaName = trimmedName;

            return true;
        }

        public void RemoveArea(Area area)
        {
            if (_areas.Remove(area))
            {
                if (String.Equals(SelectedArea, area))
                    SelectArea(null);
            }
        }

        /// <summary>
        /// Used to select an area. Given area must be part of AvailableAreas. Null removes the selection.
        /// </summary>
        /// <param name="area">null or Area in AvailableAreas</param>
        public void SelectArea(Area area)
        {
            if (area == null || AvailableAreas.Contains(area))
            {
                SelectedArea = area;
                SelectedAreaName = area?.Name;
            }
        }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion

        [OnDeserialized()]
        private void OnDeserialized(StreamingContext context)
        {
            _availableAreas = new ReadOnlyObservableCollection<Area>(_areas);
            if (_selectedAreaName != null)
            {
                Area foundArea = _availableAreas.FirstOrDefault(area => String.Equals(area.Name, _selectedAreaName, StringComparison.Ordinal));
                if (foundArea != null)
                {
                    _selectedArea = foundArea;
                }else
                {
                    //error case, signal somehow? though the user cannot do anything
                    Debug.Fail("Selected area could not be found and reconstructed.");
                    _selectedAreaName = null;
                }
            }
        }
    }
}
