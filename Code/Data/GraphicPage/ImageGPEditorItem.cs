﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Validation;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Promess.Pbld.Data.GraphicPage
{
    /// <summary>
    /// Image editor item for a graphic project
    /// </summary>
    [DataContract(Name = "ImageGPEditorItem")]
    public class ImageGPEditorItem:ImageEditorItem
    {
        #region fields
        [DataMember(Name = "CharacteristicNumber")]
        private int _characteristicNumber;
        #endregion

        [Range(0,999, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public int CharacteristicNumber
        {
            get { return _characteristicNumber; }
            set
            {
                SetPropertyAndValidate(ref _characteristicNumber, value);
            }
        }
    }
}
