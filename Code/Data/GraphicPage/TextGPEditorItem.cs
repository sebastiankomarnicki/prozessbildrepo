﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Validation;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Promess.Pbld.Data.GraphicPage
{
    /// <summary>
    /// Text editor item for a graphic project
    /// </summary>
    [DataContract(Name = "TextGPEditorItem")]
    public class TextGPEditorItem:TextEditorItem
    {
        #region fields
        //FIXME validation logic
        [DataMember(Name = "CharacteristicNumber")]
        private int _characteristicNumber;
        #endregion

        [Range(0,999, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public int CharacteristicNumber
        {
            get { return _characteristicNumber; }
            set
            {
                SetPropertyAndValidate(ref _characteristicNumber, value);
            }
        }
    }
}
