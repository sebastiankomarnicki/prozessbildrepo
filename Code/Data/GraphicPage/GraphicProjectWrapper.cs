﻿namespace Promess.Pbld.Data.GraphicPage
{
    /// <summary>
    /// Wrapper for a graphic project path and the found graphic project, if any
    /// </summary>
    public class GraphicProjectWrapper
    {
        private readonly string _projectPath;
        private readonly GraphicProject _maybeGraphicProject;
        
        public GraphicProjectWrapper(string projectPath, GraphicProject maybeGraphicProject)
        {
            this._projectPath = projectPath;
            this._maybeGraphicProject = maybeGraphicProject;
        } 

        public string ProjectPath { get { return _projectPath; } }
        public GraphicProject MaybeGraphicProject { get { return _maybeGraphicProject; } }
    }
}
