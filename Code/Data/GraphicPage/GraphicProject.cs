﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Waf.Foundation;

namespace Promess.Pbld.Data.GraphicPage
{
    /// <summary>
    /// Model for an editable graphic project
    /// </summary>
    [DataContract(Name = "GraphicProjectModel")]
    public class GraphicProject:ValidatableModel, IExtensibleDataObject
    {
        private const int _MAXPAGES = 20;

        [DataMember(Name = "GraphicPages")]
        public ObservableCollection<GraphicItemModel> _graphicPages;
        private ReadOnlyObservableCollection<GraphicItemModel> _availableGraphicPages;
        private bool _isDirty;

        public GraphicProject()
        {
            this._graphicPages = new ObservableCollection<GraphicItemModel>();
            this._availableGraphicPages = new ReadOnlyObservableCollection<GraphicItemModel>(_graphicPages);
            this._isDirty = true;
            CollectionChangedEventManager.AddHandler(_graphicPages, GraphicPagesCollectionChanged);
        }

        public ReadOnlyObservableCollection<GraphicItemModel> GraphicPages
        {
            get { return _availableGraphicPages; }
        }

        public bool TryAddGraphicPage(GraphicItemModel page)
        {
            if (!CanAddPage())
                return false;
            if (DoesPageNameAlreadyExist(page.Name))
                return false;

            this._graphicPages.Add(page);
            return true;
        }

        public void RemoveGraphicPage(GraphicItemModel page)
        {
            this._graphicPages.Remove(page);
        }

        public bool TryRenamePage(GraphicItemModel page, string trimmedName)
        {
            if (trimmedName.Length == 0 || DoesPageNameAlreadyExist(trimmedName))
                return false;
            page.Name = trimmedName;
            return true;
        }

        public bool GetIsDirty()
        {
            if (_isDirty)
                return true;
            return GraphicPages.Any(page => page.GetIsDirty());
        }

        public void ResetDirty()
        {
            _isDirty = false;
            foreach (var graphics in GraphicPages)
                graphics.ResetDirty();
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            this._isDirty = true;
        }

        private void GraphicPagesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this._isDirty = true;
        }


        public bool DoesPageNameAlreadyExist(string name)
        {
            return this.GraphicPages.Any(page => String.Equals(page.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        private bool CanAddPage()
        {
            return GraphicPages.Count <= _MAXPAGES;
        }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion

        [OnDeserialized()]
        private void OnDeserialized(StreamingContext context)
        {
            this._availableGraphicPages = new ReadOnlyObservableCollection<GraphicItemModel>(_graphicPages);
            CollectionChangedEventManager.AddHandler(this._graphicPages, GraphicPagesCollectionChanged);
            //usage of property setter during deserialization leads to set dirty
            this._isDirty = false;
        }
    }
}
