﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Validation;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Waf.Foundation;

namespace Promess.Pbld.Data.GraphicPage
{
    /// <summary>
    /// Model for an editable graphic page
    /// </summary>
    [DataContract(Name = "GraphicItemModel")]
    public class GraphicItemModel:Model, IExtensibleDataObject
    {
        [DataMember(Name = "GraphicPageName")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        private string _name;
        [DataMember(Name = "GraphicsPageItems")]
        private ObservableCollection<EditorItemBase> _items;
        [DataMember(Name = "AspectRatio")]
        private AspectRatio _aspectRatio;

        private bool _isDirty;

        public GraphicItemModel(string name) : this()
        {
            this._name = name;
        }

        private GraphicItemModel()
        {
            this._items = new ObservableCollection<EditorItemBase>();
            CollectionChangedEventManager.AddHandler(this._items, ItemsCollectionChanged);
        }

        public String Name
        {
            get { return _name; }
            internal set
            {
                SetProperty(ref _name, value);
            }
        }

        public ObservableCollection<EditorItemBase> Items
        {
            get { return _items; }
            private set
            {
                SetProperty(ref _items, value);
            }
        }

        public AspectRatio AspectRatio
        {
            get { return _aspectRatio; }
            set { SetProperty(ref _aspectRatio, value); }
        }

        #region dirty flag
        public bool GetIsDirty()
        {
            if (_isDirty)
                return true;
            return _items.Any(elem => elem.IsDirty);
        }

        public void ResetDirty()
        {
            _isDirty = false;
            foreach (var elem in _items)
                elem.IsDirty = false;
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            _isDirty = true;
        }
        private void ItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            _isDirty = true;
        }

        #endregion

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            if (_items == null)
                _items = new ObservableCollection<EditorItemBase>();
            CollectionChangedEventManager.AddHandler(_items, ItemsCollectionChanged);
        }
    }
}
