﻿using Promess.Common.Settings.Extensions;
using Promess.Pbld.Data.Validation;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace Promess.Pbld.Data.Settings
{
    /// <summary>
    /// Settings for different process evaluations
    /// </summary>
    public class EvaluationAndColorSettings:ConfigurationSectionExtension
    {
        [ConfigurationProperty("ProcessOk", DefaultValue = null)]
        public EvaluationAndColorElement ProcessOk
        {
            get { return GetValue<EvaluationAndColorElement>("ProcessOk"); }
            set
            {
                this["ProcessOk"] = value;
            }
        }

        [ConfigurationProperty("ProcessEndangered")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public EvaluationAndColorElement ProcessEndangered
        {
            get { return GetValue<EvaluationAndColorElement>("ProcessEndangered"); }
            set
            {
                this["ProcessEndangered"] = value;
            }
        }

        [ConfigurationProperty("ProcessOoC")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public EvaluationAndColorElement ProcessOoC
        {
            get { return GetValue<EvaluationAndColorElement>("ProcessOoC"); }
            set
            {
                this["ProcessOoC"] = value;
            }
        }

        [ConfigurationProperty("StabilityViolation")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public EvaluationAndColorElement StabilityViolation
        {
            get { return GetValue<EvaluationAndColorElement>("StabilityViolation"); }
            set
            {
                this["StabilityViolation"] = value;
            }
        }

        [ConfigurationProperty("ActionLimitViolationXq")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public EvaluationAndColorElement ActionLimitViolationXq
        {
            get { return GetValue<EvaluationAndColorElement>("ActionLimitViolationXq"); }
            set
            {
                this["ActionLimitViolationXq"] = value;
            }
        }

        [ConfigurationProperty("ActionLimitViolationS")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public EvaluationAndColorElement ActionLimitViolationS
        {
            get { return GetValue<EvaluationAndColorElement>("ActionLimitViolationS"); }
            set
            {
                this["ActionLimitViolationS"] = value;
            }
        }

        [ConfigurationProperty("DeficientPart")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public EvaluationAndColorElement DeficientPart
        {
            get { return GetValue<EvaluationAndColorElement>("DeficientPart"); }
            set
            {
                this["DeficientPart"] = value;
            }
        }

        [ConfigurationProperty("NoOrTooFewValues")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        public EvaluationAndColorElement NoOrTooFewValues
        {
            get { return GetValue<EvaluationAndColorElement>("NoOrTooFewValues"); }
            set
            {
                this["NoOrTooFewValues"] = value;
            }
        }
    }
}
