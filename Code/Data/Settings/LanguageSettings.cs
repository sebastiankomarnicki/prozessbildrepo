﻿using Promess.Common.Settings.Extensions;
using System.Configuration;
using System.Globalization;

namespace Promess.Pbld.Data.Settings
{
    public class LanguageSettings:ConfigurationSectionExtension
    {
        [ConfigurationProperty("Culture")]
        public CultureInfo Culture
        {
            get { return GetValue<CultureInfo>("Culture"); }
            set
            {
                this["Culture"] = value;
            }
        }
    }
}
