﻿using Promess.Common.Settings.Extensions;
using System;
using System.Configuration;

namespace Promess.Pbld.Data.Settings
{
    /// <summary>
    /// Export configuration for a follower
    /// </summary>
    public class TransferFollowerSettings:ConfigurationSectionExtension
    {
        [ConfigurationProperty("TransferPath", DefaultValue = "")]
        public String TransferPath
        {
            get { return GetValue<String>("TransferPath"); }
            set
            {
                this["TransferPath"] = value;
            }
        }

        [ConfigurationProperty("AddUserNameToTransferPath", DefaultValue = false)]
        public bool AddUserNameToTransferPath
        {
            get { return GetValue<bool>("AddUserNameToTransferPath"); }
            set
            {
                this["AddUserNameToTransferPath"] = value;
            }
        }

        [ConfigurationProperty("GraphicProjectsTransferPath", DefaultValue = "")]
        public String GraphicProjectTransferPath
        {
            get { return GetValue<String>("GraphicProjectsTransferPath"); }
            set
            {
                this["GraphicProjectsTransferPath"] = value;
            }
        }

        [ConfigurationProperty("OverwriteGraphicPages", DefaultValue = false)]
        public bool OverwriteGraphicPages
        {
            get { return GetValue<bool>("OverwriteGraphicPages"); }
            set
            {
                this["OverwriteGraphicPages"] = value;
            }
        }
    }
}
