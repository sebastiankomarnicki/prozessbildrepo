﻿using Promess.Pbld.Data.Editor;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    public interface IPresetRepresentationGraphic:IPresetRepresentationText
    {
        Stretch Stretch { get; set; }
    }
}
