﻿using Promess.Pbld.Data.Editor;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    public interface IPresetRepresentationBorder
    {
        BorderType Border { get; set; }
    }
}
