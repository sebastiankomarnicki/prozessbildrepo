﻿using System.Windows;
using System.Runtime.Serialization;
using System.Windows.Media;
using Promess.Pbld.Data.Editor;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    /// <summary>
    /// Preset settings for a line element
    /// </summary>
    [DataContract(Name ="PresetLine")]
    public class PresetLine : IPresetLine, IExtensibleDataObject
    {
        public PresetLine()
        {
            P1LineEnding = LineEnding.None;
            P2LineEnding = LineEnding.None;
            LineColor = SystemColors.WindowTextColor;
            LineWidth = LineWidth.Medium;
        }

        [DataMember(Name = "P1LineEnding")]
        public LineEnding P1LineEnding { get; set; }
        [DataMember(Name = "P2LineEnding")]
        public LineEnding P2LineEnding { get; set; }
        [DataMember(Name = "LineColor")]
        public Color LineColor { get; set; }
        [DataMember(Name = "LineWidth")]
        public LineWidth LineWidth { get; set; }

        public void OverwritePresets<T>(T destination) where T : IPresetLine
        {
            destination.P1LineEnding = this.P1LineEnding;
            destination.P2LineEnding = this.P2LineEnding;
            destination.LineColor = this.LineColor;
            destination.LineWidth = this.LineWidth;
        }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion
    }
}
