﻿using Promess.Pbld.Data.Editor;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    public interface IPresetRepresentationText:IPresetRepresentationBorder
    {
        HorizontalAlignment HorizontalAlignment { get; set; }
        VerticalAlignment VerticalAlignment { get; set; }
    }
}
