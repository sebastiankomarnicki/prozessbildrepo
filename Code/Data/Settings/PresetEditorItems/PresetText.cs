﻿using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using Promess.Pbld.Data.Editor;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    /// <summary>
    /// Preset settings for a text elements
    /// </summary>
    [DataContract(Name = "PresetText")]
    public class PresetText : IPresetRepresentationText, IPresetFontAndColorExtended, IExtensibleDataObject
    {
        [DataMember(Name = "FontFamily")]
        private string _fontFamilyRepForSerialization;

        //font transformed for serialization
        public FontFamily Font { get; set; }
        [DataMember(Name = "HorizontalAlignment")]
        public Editor.HorizontalAlignment HorizontalAlignment { get; set; }
        [DataMember(Name = "VerticalAlignment")]
        public Editor.VerticalAlignment VerticalAlignment { get; set; }
        [DataMember(Name = "Border")]
        public BorderType Border { get; set; }
        [DataMember(Name = "FontColor")]
        public Color FontColor { get; set; }
        [DataMember(Name = "ScaleFont")]
        public bool ScaleFont { get; set; }
        [DataMember(Name = "FontSize")]
        public int FontSize { get; set; }
        [DataMember(Name = "OverwriteBackground")]
        public bool OverwriteBackground { get; set; }
        [DataMember(Name = "BackColor")]
        public Color BackColor { get; set; }
        [DataMember(Name = "BoldFont")]
        public bool BoldFont { get; set; }
        [DataMember(Name = "ItalicFont")]
        public bool ItalicFont { get; set; }

        public PresetText()
        {
            Font = new FontFamily("Verdana");
            FontColor = SystemColors.WindowTextColor;
            Border = BorderType.Flat;
            ScaleFont = false;
            FontSize = 12;
            OverwriteBackground = false;
            BackColor = Colors.Transparent;
            HorizontalAlignment = Editor.HorizontalAlignment.Left;
            BoldFont = false;
            ItalicFont = false;
        }

        public void OverwritePresets<T>(T destination) where T : IPresetFontAndColor, IPresetRepresentationText, IPresetFontAndColorExtended
        {
            destination.Font = this.Font;
            destination.FontColor = this.FontColor;
            destination.ScaleFont = this.ScaleFont;
            destination.FontSize = this.FontSize;
            destination.BoldFont = this.BoldFont;
            destination.ItalicFont = this.ItalicFont;

            destination.HorizontalAlignment = this.HorizontalAlignment;
            destination.VerticalAlignment = this.VerticalAlignment;
            destination.Border = this.Border;

            destination.OverwriteBackground = this.OverwriteBackground;
            destination.BackColor = this.BackColor;
        }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion

        #region serialization
        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this._fontFamilyRepForSerialization = new FontFamilyConverter().ConvertToInvariantString(this.Font);
        }

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            this.Font = (FontFamily)new FontFamilyConverter().ConvertFromInvariantString(this._fontFamilyRepForSerialization);
        }
        #endregion
    }
}
