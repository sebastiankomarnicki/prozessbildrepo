﻿using System.Windows.Media;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    public interface IPresetFontAndColor
    {
        FontFamily Font { get; set; }
        Color FontColor { get; set; }
        bool ScaleFont { get; set; }
        int FontSize { get; set; }
        bool BoldFont { get; set; }
        bool ItalicFont { get; set; }
    }
}
