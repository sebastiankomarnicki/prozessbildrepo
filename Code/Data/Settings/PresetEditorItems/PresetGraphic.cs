﻿using System.Runtime.Serialization;
using Promess.Pbld.Data.Editor;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    /// <summary>
    /// Preset setting for a graphic element
    /// </summary>
    [DataContract(Name="PresetGraphic")]
    public class PresetGraphic : IPresetRepresentationGraphic, IExtensibleDataObject
    {
        [DataMember(Name = "Border")]
        public BorderType Border { get; set; }
        [DataMember(Name = "HorizontalAlignment")]
        public HorizontalAlignment HorizontalAlignment { get; set; }
        [DataMember(Name = "VerticalAlignment")]
        public VerticalAlignment VerticalAlignment { get; set; }
        [DataMember(Name = "Stretch")]
        public Stretch Stretch { get; set; }

        public PresetGraphic()
        {
            Border = BorderType.Flat;
            HorizontalAlignment = HorizontalAlignment.Left;
            VerticalAlignment = VerticalAlignment.Top;
            Stretch = Stretch.Fill;
        }

        public void OverwritePresets<T>(T destination) where T : IPresetRepresentationGraphic
        {
            destination.Border = this.Border;
            destination.HorizontalAlignment = this.HorizontalAlignment;
            destination.VerticalAlignment = this.VerticalAlignment;
            destination.Stretch = this.Stretch;
        }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion
    }
}
