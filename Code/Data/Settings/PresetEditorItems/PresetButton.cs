﻿using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    /// <summary>
    /// Preset settings for a button element
    /// </summary>
    [DataContract(Name ="PresetButton")]
    public class PresetButton : IPresetFontAndColor, IExtensibleDataObject
    {
        [DataMember(Name = "FontFamily")]
        private string _fontFamilyRepForSerialization;

        //font transformed for serialization
        public FontFamily Font { get; set; }
        [DataMember(Name = "FontColor")]
        public Color FontColor { get; set; }
        [DataMember(Name = "ScaleFont")]
        public bool ScaleFont { get; set; }
        [DataMember(Name = "FontSize")]
        public int FontSize { get; set; }
        [DataMember(Name = "BoldFont")]
        public bool BoldFont { get; set; }
        [DataMember(Name = "ItalicFont")]
        public bool ItalicFont { get; set; }

        public PresetButton()
        {
            Font = new FontFamily("Verdana");
            FontColor = SystemColors.WindowTextColor;
            ScaleFont = false;
            FontSize = 12;
            BoldFont = false;
            ItalicFont = false;
        }

        public void OverwritePresets<T>(T destination) where T : IPresetFontAndColor
        {
            destination.Font = this.Font;
            destination.FontColor = this.FontColor;
            destination.ScaleFont = this.ScaleFont;
            destination.FontSize = this.FontSize;
            destination.BoldFont = this.BoldFont;
            destination.ItalicFont = this.ItalicFont;
        }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion

        #region serialization
        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this._fontFamilyRepForSerialization = new FontFamilyConverter().ConvertToInvariantString(this.Font);
        }

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            this.Font = (FontFamily)new FontFamilyConverter().ConvertFromInvariantString(this._fontFamilyRepForSerialization);
        }
        #endregion
    }
}
