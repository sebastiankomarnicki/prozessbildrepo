﻿using Promess.Pbld.Data.Editor;
using System.Windows.Media;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    public interface IPresetLine
    {
        LineEnding P1LineEnding { get; set; }
        LineEnding P2LineEnding { get; set; }
        Color LineColor { get; set; }
        LineWidth LineWidth { get; set; }
    }
}
