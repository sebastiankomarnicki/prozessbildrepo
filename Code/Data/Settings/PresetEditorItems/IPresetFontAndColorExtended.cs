﻿using System.Windows.Media;

namespace Promess.Pbld.Data.Settings.PresetEditorItems
{
    public interface IPresetFontAndColorExtended:IPresetFontAndColor
    {
        bool OverwriteBackground { get; set; }
        Color BackColor { get; set; }
    }
}
