﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Promess.Pbld.Data.Settings
{
    /// <summary>
    /// Data container for exporting settings for the program running in follower mode
    /// </summary>
    [DataContract(Name="ExportSettings")]
    public class ExportSettings:IExtensibleDataObject
    {
        [DataMember(Name = "AreaFile")]
        [Required]
        public string AreaFile { get; set; }
        [Required]
        [DataMember(Name = "PanelsFile")]
        public string PanelsFile { get; set; }
        [DataMember(Name = "GraphicPageFiles")]
        public List<string> GraphicPageFiles { get; set; }
        [DataMember(Name = "BackgroundImageFile")]
        public string BackgroundImageFile { get; set; }
        [DataMember(Name = "GeneralSettingsFile")]
        public string GeneralSettingsFile { get; set; }
        [DataMember(Name = "EvalAndColorSettingsFile")]
        public string EvalAndColorSettingsFile { get; set; }
        [DataMember(Name = "ListViewSettingsFile")]
        public string ListViewSettingsFile { get; set; }
        [DataMember(Name = "QsStatSettingsFile")]
        public string QsStatSettingsFile { get; set; }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion
    }
}
