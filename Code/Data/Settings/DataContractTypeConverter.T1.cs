﻿using Promess.Common.Util;
using System;
using System.ComponentModel;
using System.Globalization;

namespace Promess.Pbld.Data.Settings
{
    /// <summary>
    /// Type converter to help with <see cref="Promess.Common.Settings.Extensions.ConfigurationSectionExtension"/> property serialization
    /// when the elements have a <see cref="string"/> serialization which the general processing logic cannot use.
    /// </summary>
    /// <typeparam name="T">Type to (de)serialize</typeparam>
    public class DataContractTypeConverter<T>:TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType.Equals(typeof(string)))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value is String)
            {
                string elem = value as String;
                return SerializeHelper.Deserialize<T>(elem);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value != null && !(value is T))
                throw new ArgumentException($"Invalid {typeof(T)}", "value");

            if (destinationType.Equals(typeof(string)))
            {
                T elem = (T)value;
                return SerializeHelper.Serialize(elem);
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
