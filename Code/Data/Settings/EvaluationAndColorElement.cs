﻿using Promess.Common.Settings.Extensions;
using Promess.Pbld.Data.Validation;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Runtime.CompilerServices;
using System.Windows.Media;

namespace Promess.Pbld.Data.Settings
{
    /// <summary>
    /// Configuration for displaying a process evaluation
    /// </summary>
    public class EvaluationAndColorElement:ConfigurationElementExtension, INotifyPropertyChanged
    {
        [ConfigurationProperty("IsRequired", DefaultValue = false)]
        public bool IsRequired
        {
            get { return GetValue<bool>("IsRequired"); }
            set
            {
                this["IsRequired"] = value;
            }
        }

        [ConfigurationProperty("IsEnabled", DefaultValue=true)]
        public bool IsEnabled
        {
            get { return GetValue<bool>("IsEnabled"); }
            set
            {
                this["IsEnabled"] = value;
                OnPropertyChanged();
            }
        }

        [ConfigurationProperty("BackgroundColor", DefaultValue="#808080")]
        public Color BackgroundColor
        {
            get { return GetValue<Color>("BackgroundColor"); }
            set
            {
                this["BackgroundColor"] = value;
                OnPropertyChanged();
            }
        }

        [ConfigurationProperty("FontColor", DefaultValue = "#000000")]
        public Color FontColor
        {
            get { return GetValue<Color>("FontColor"); }
            set
            {
                this["FontColor"] = value;
                OnPropertyChanged();
            }
        }

        [ConfigurationProperty("TreeColor", DefaultValue = "#808080")]
        public Color TreeColor
        {
            get { return GetValue<Color>("TreeColor"); }
            set
            {
                this["TreeColor"] = value;
                OnPropertyChanged();
            }
        }

        [ConfigurationProperty("BoldInTree", DefaultValue = false)]
        public bool BoldInTree
        {
            get { return GetValue<bool>("BoldInTree"); }
            set
            {
                this["BoldInTree"] = value;
                OnPropertyChanged();
            }
        }

        [ConfigurationProperty("IconElement")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        [TypeConverter(typeof(DataContractTypeConverter<IconElement>))]
        public IconElement IconElement
        {
            get { return GetValue<IconElement>("IconElement"); }
            set
            {
                this["IconElement"] = value;
                OnPropertyChanged();
            }
        }

        #region property changed
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
