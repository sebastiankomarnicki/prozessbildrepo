﻿using Promess.Common.Settings.Extensions;
using System;
using System.ComponentModel;
using System.Configuration;

namespace Promess.Pbld.Data.Settings
{
    /// <summary>
    /// User settings for qsStat
    /// </summary>
    public class QsStatSettings:ConfigurationSectionExtension
    {
        [ConfigurationProperty("User", DefaultValue="")]
        public string User {
            get { return GetValue<string>("User"); }
            set { this["User"] = value; }
            }
        [ConfigurationProperty("Password", DefaultValue ="")]
        public string Password {
            get { return GetValue<string>("Password"); }
            set { this["Password"] = value; }
        }
        [ConfigurationProperty( "Module", DefaultValue = 20)]
        public int? Module {
            get { return GetValue<int?>("Module"); }
            set { this["Module"] = value; }
        }
        [ConfigurationProperty("Language", DefaultValue=49)]
        public int? Language {
            get { return GetValue<int?>("Language"); }
            set { this["Language"] = value; }
        }
        [ConfigurationProperty( "Strategy", DefaultValue =null)]
        public int? Strategy {
            get { return GetValue<int?>("Strategy"); }
            set { this["Strategy"] = value; }
        }
        [ConfigurationProperty( "ReportFile", DefaultValue = "")]
        public string ReportFile {
            get { return GetValue<string>("ReportFile"); }
            set { this["ReportFile"] = value; }
        }
        [ConfigurationProperty("Printer", DefaultValue = null)]
        [TypeConverter(typeof(DataContractTypeConverter<Tuple<int,string>>))]
        public Tuple<int, string> Printer
        {
            get { return GetValue<Tuple<int,string>>("Printer"); }
            set { this["Printer"] = value; }
        }
    }
}
