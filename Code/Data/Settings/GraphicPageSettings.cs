﻿using Promess.Common.Settings.Extensions;
using System;
using System.Configuration;

namespace Promess.Pbld.Data.Settings
{
    public class GraphicPageSettings:ConfigurationSectionExtension
    {
        [ConfigurationProperty("ProjectPath", DefaultValue = "")]
        public string ProjectPath
        {
            get { return GetValue<String>("ProjectPath"); }
            set { this["ProjectPath"] = value; }
        }
    }
}
