﻿using Promess.Common.Settings.Extensions;
using Promess.Pbld.Data.Validation;
using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace Promess.Pbld.Data.Settings
{
    public class GeneralSettings:ConfigurationSectionExtension
    {
        [ConfigurationProperty("EnableTimeLimit", DefaultValue = false)]
        public bool EnableTimeLimit
        {
            get { return GetValue<bool>("EnableTimeLimit"); }
            set
            {
                this["EnableTimeLimit"] = value;
            }
        }

        [ConfigurationProperty("MinimalNumberOfValues", DefaultValue=10)]
        [Range(5,999, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public int MinimalNumberOfValues
        {
            get { return GetValue<int>("MinimalNumberOfValues"); }
            set {
                this["MinimalNumberOfValues"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("MaxNumberOfValues", DefaultValue = 100)]
        [Range(5, 999, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        [GreaterOrEqualThan("MinimalNumberOfValues")]
        public int MaxNumberOfValues
        {
            get { return GetValue<int>("MaxNumberOfValues"); }
            set {
                this["MaxNumberOfValues"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("MaxEvalTimeSpan", DefaultValue = 5)]
        [Range(1, 30, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public int MaxEvalTimeSpan
        {
            get { return GetValue<int>("MaxEvalTimeSpan"); }
            set {
                this["MaxEvalTimeSpan"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("CpCpkFromPlanning", DefaultValue = true)]
        public bool CpCpkFromPlanning
        {
            get { return GetValue<bool>("CpCpkFromPlanning"); }
            set
            {
                this["CpCpkFromPlanning"] = value;
            }
        }

        [ConfigurationProperty("CpCpkReplaceMissing", DefaultValue = true)]
        public bool CpCpkReplaceMissing
        {
            get { return GetValue<bool>("CpCpkReplaceMissing"); }
            set
            {
                this["CpCpkReplaceMissing"] = value;
            }
        }

        [ConfigurationProperty("OutOfControlCp", DefaultValue = 1.0)]
        [Range(0.01,10, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double OutOfControlCp
        {
            get { return GetValue<double>("OutOfControlCp"); }
            set
            {
                this["OutOfControlCp"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("OutOfControlCpk", DefaultValue = 1.33)]
        [Range(0.01, 10, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double OutOfControlCpk
        {
            get { return GetValue<double>("OutOfControlCpk"); }
            set
            {
                this["OutOfControlCpk"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("EndangeredCp", DefaultValue = 1.0)]
        [Range(0.01, 10, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double EndangeredCp
        {
            get { return GetValue<double>("EndangeredCp"); }
            set
            {
                this["EndangeredCp"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("EndangeredCpk", DefaultValue = 1.33)]
        [Range(0.01, 10, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double EndangeredCpk
        {
            get { return GetValue<double>("EndangeredCpk"); }
            set
            {
                this["EndangeredCpk"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("CalcCpkS", DefaultValue =false)]
        public bool CalcCpkS
        {
            get
            {
                return GetValue<bool>("CalcCpkS");
            }
            set
            {
                this["CalcCpkS"] = value;
            }
        }

        [ConfigurationProperty("CalcCpkSMin", DefaultValue = 3)]
        [Range(3,int.MaxValue, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public int CalcCpkSMin
        {
            get
            {
                return GetValue<int>("CalcCpkSMin");
            }
            set
            {
                this["CalcCpkSMin"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("CalcCpkSMax", DefaultValue = 75)]
        [Range(3, int.MaxValue, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        [GreaterOrEqualThan(nameof(CalcCpkSMin))]
        public int CalcCpkSMax
        {
            get
            {
                return GetValue<int>("CalcCpkSMax");
            }
            set
            {
                this["CalcCpkSMax"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("RejectEvaluationMultipleParts", DefaultValue = false)]
        public bool RejectEvaluationMultipleParts
        {
            get
            {
                return GetValue<bool>("RejectEvaluationMultipleParts");
            }
            set
            {
                this["RejectEvaluationMultipleParts"] = value;
            }
        }

        [ConfigurationProperty("RejectEvaluationNoParts", DefaultValue = 2)]
        [Range(2,300, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public int RejectEvaluationNoParts
        {
            get
            {
                return GetValue<int>("RejectEvaluationNoParts");
            }
            set
            {
                this["RejectEvaluationNoParts"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("LimitActionControlLimitSamples", DefaultValue = false)]
        public bool LimitActionControlLimitSamples
        {
            get
            {
                return GetValue<bool>("LimitActionControlLimitSamples");
            }
            set
            {
                this["LimitActionControlLimitSamples"] = value;
            }
        }

        [ConfigurationProperty("ActionControlLimitNoSamples", DefaultValue = 1)]
        [Range(1, 25, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public int ActionControlLimitNoSamples
        {
            get
            {
                return GetValue<int>("ActionControlLimitNoSamples");
            }
            set
            {
                this["ActionControlLimitNoSamples"] = value;
                ValidateModelProperty(value);
            }
        }

        [ConfigurationProperty("ShowDeletedMeasurements", DefaultValue = false)]
        public bool ShowDeletedMeasurements
        {
            get
            {
                return GetValue<bool>("ShowDeletedMeasurements");
            }
            set
            {
                this["ShowDeletedMeasurements"] = value;
            }
        }

        [ConfigurationProperty("ConvertNetworkToUNC", DefaultValue = true)]
        public bool ConvertNetworkToUNC
        {
            get
            {
                return GetValue<bool>("ConvertNetworkToUNC");
            }
            set
            {
                this["ConvertNetworkToUNC"] = value;
            }
        }

        [ConfigurationProperty("EvaluateUnimportant", DefaultValue = true)]
        public bool EvaluateUnimportant
        {
            get
            {
                return GetValue<bool>("EvaluateUnimportant");
            }
            set
            {
                this["EvaluateUnimportant"] = value;
            }
        }

        [ConfigurationProperty("EvaluateLittleImportant", DefaultValue = true)]
        public bool EvaluateLittleImportant
        {
            get
            {
                return GetValue<bool>("EvaluateLittleImportant");
            }
            set
            {
                this["EvaluateLittleImportant"] = value;
            }
        }

        [ConfigurationProperty("EvaluateImportant", DefaultValue = true)]
        public bool EvaluateImportant
        {
            get
            {
                return GetValue<bool>("EvaluateImportant");
            }
            set
            {
                this["EvaluateImportant"] = value;
            }
        }

        [ConfigurationProperty("EvaluateSignificant", DefaultValue = true)]
        public bool EvaluateSignificant
        {
            get
            {
                return GetValue<bool>("EvaluateSignificant");
            }
            set
            {
                this["EvaluateSignificant"] = value;
            }
        }

        [ConfigurationProperty("EvaluateCritical", DefaultValue = true)]
        public bool EvaluateCritical
        {
            get
            {
                return GetValue<bool>("EvaluateCritical");
            }
            set
            {
                this["EvaluateCritical"] = value;
            }
        }
    }
}
