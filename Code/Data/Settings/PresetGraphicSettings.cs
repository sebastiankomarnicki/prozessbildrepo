﻿using Promess.Pbld.Data.Settings.PresetEditorItems;
using System.Runtime.Serialization;

namespace Promess.Pbld.Data.Settings
{
    /// <summary>
    /// Preset configurations for editor graphical elements
    /// </summary>
    [DataContract(Name = "PresetGraphicSettings")]
    public class PresetGraphicSettings:IExtensibleDataObject
    {
        #region fields
        [DataMember(Name = "TextItem", IsRequired = true)]
        private PresetText _textItem;
        [DataMember(Name = "ImageItem", IsRequired = true)]
        private PresetGraphic _imageItem;
        [DataMember(Name = "ButtonItem", IsRequired = true)]
        private PresetButton _buttonItem;
        [DataMember(Name = "LineItem", IsRequired = true)]
        private PresetLine _lineItem;
        #endregion

        public PresetGraphicSettings()
        {
            this._textItem = new PresetText();
            this._imageItem = new PresetGraphic();
            this._buttonItem = new PresetButton();
            this._lineItem = new PresetLine();
        }

        #region properties
        public PresetText PresetText { get { return this._textItem; } }
        public PresetGraphic PresetGraphic { get { return this._imageItem; } }
        public PresetButton PresetButton { get { return this._buttonItem; } }
        public PresetLine PresetLine { get { return this._lineItem; } }
        #endregion

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion
    }
}
