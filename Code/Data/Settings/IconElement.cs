﻿using System;
using System.Runtime.Serialization;

namespace Promess.Pbld.Data.Settings
{
    [DataContract(Name = "ImageIcon") ]
    public class IconElement:IEquatable<IconElement>, IExtensibleDataObject
    {
        [DataMember(Name = "IsResource")]
        private bool _isResourceIcon;
        [DataMember(Name = "Name")]
        private string _iconName;

        public IconElement(bool isResourceIcon, string iconName)
        {
            this._isResourceIcon = isResourceIcon;
            this._iconName = iconName;
        }

        public bool IsResourceIcon
        {
            get { return _isResourceIcon; }
            private set
            {
                _isResourceIcon = value;
            }
        }

        public string IconName
        {
            get {return _iconName; }
            private set
            {
                _iconName = value;
            }
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as IconElement);
        }

        public bool Equals(IconElement other)
        {
            if (other == null)
                return false;
            if (other.GetType() != GetType())
                return false;

            return IsResourceIcon.Equals(other.IsResourceIcon) && String.Equals(IconName, other.IconName);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 31 + _isResourceIcon.GetHashCode();
                hash = hash * 31 + (_iconName ?? "").GetHashCode();
                return hash;
            }
        }

        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion
    }
}
