﻿using Promess.Pbld.Data.Editor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Waf.Foundation;

namespace Promess.Pbld.Data.Settings
{
    /// <summary>
    /// Settings for the list view
    /// </summary>
    [DataContract(Name = "ListViewSettings")]
    public sealed class ListViewSettings:Model
    {
        #region constants
        private const int PART_NUMBER = 1001;
        private const int PART_DESCRIPTION = 1002;
        private const int DRAWING_NUMBER_TEXT = 1041;
        private const int CONTRACT = 1053;
        private const int MACHINE_NUMBER = 1083;
        private const int WORK_CYCLE_OPERATION = 1086;
        private const int DEVICE = 1114;
        private const int CHARACTERISTIC_NUMBER = 2001;
        private const int CHARACTERISTIC_DESCRIPTION = 2002;
        private const int NOMINAL_VALUE = 2101;
        private const int LOWER_SPECIFICATION_LIMIT = 2110;
        private const int UPPER_SPECIFICATION_LIMIT = 2111;
        private const int LOWER_ALLOWANCE = 2112;
        private const int UNIT_DESCRIPTION = 2142;
        private const int TEST_BEGIN = 2411;

        //names represent retrieved result for qsStat module 20, might be something different for other modules
        private const int AVERAGE = 1000;
        private const int STANDARD_DEVIATION = 2100;
        private const int POTENTIAL_CAPABILITY_INDEX = 5210;
        private const int CRITICAL_CAPABILITY_INDEX = 5220;
        private const int NUMBER_OF_VALUES_OUTSIDE_TOLERANCE = 5530;
        private const int TOTAL_VALUES = 6301;
        private const int TOTAL_EVALUATED = 6302;
        #endregion

        [DataMember(Name = "PartLabelQdasNumbers")]
        private List<QdasNumber> _partLabelQdasNumbers;
        [DataMember(Name = "CharacteristicLabelQdasNumbers")]
        private List<QdasNumber> _characteristicLabelQdasNumbers;
        [DataMember(Name = "PartLabelSeparator")]
        private string _partLabelSeparator;
        [DataMember(Name = "CharacteristicLabelSeparator")]
        private string _characteristicLabelSeparator;
        [DataMember(Name = "PartGridQdasNumbers")]
        private List<QdasNumberWrapper> _partGridQdasNumbers;
        [DataMember(Name = "CharacteristicGridQdasNumbers")]
        private List<QdasNumberWrapper> _characteristicGridQdasNumbers;


        public ListViewSettings()
        {
            _partLabelSeparator = "_";
            _characteristicLabelSeparator = " / ";
            _partLabelQdasNumbers = GetDefaultListViewPartLabels();
            _characteristicLabelQdasNumbers = GetDefaultListViewCharacteristicLabels();
            _partGridQdasNumbers = GetDefaultPartGrid();
            _characteristicGridQdasNumbers = GetDefaultCharacteristicGrid();
        }

        public List<QdasNumber> PartLabelQdasNumbers
        {
            get
            {
                return _partLabelQdasNumbers;
            }
            set
            {
                SetProperty(ref _partLabelQdasNumbers, value);
            }
        }

        public List<QdasNumber> CharacteristicLabelQdasNumbers
        {
            get
            {
                return _characteristicLabelQdasNumbers;
            }
            set
            {
                SetProperty(ref _characteristicLabelQdasNumbers, value);
            }
        }

        public String PartLabelSeparator
        {
            get
            {
                return _partLabelSeparator;
            }
            set
            {
                SetProperty(ref _partLabelSeparator, value);
            }
        }

        public String CharacteristicLabelSeparator
        {
            get
            {
                return _characteristicLabelSeparator;
            }
            set
            {
                SetProperty(ref _characteristicLabelSeparator, value);
            }
        }

        public List<QdasNumberWrapper> PartGridQdasNumbers
        {
            get
            {
                return _partGridQdasNumbers;
            }
            set
            {
                SetProperty(ref _partGridQdasNumbers, value);
            }
        }

        public List<QdasNumberWrapper> CharacteristicGridQdasNumbers
        {
            get
            {
                return _characteristicGridQdasNumbers;
            }
            set
            {
                SetProperty(ref _characteristicGridQdasNumbers, value);
            }
        }

        #region helper initializers
        private List<QdasNumber> GetDefaultListViewPartLabels()
        {
            var result = new List<QdasNumber>
            {
                new QdasNumber(TextEvaluationType.Part, CONTRACT),
                new QdasNumber(TextEvaluationType.Part, PART_NUMBER),
                new QdasNumber(TextEvaluationType.Part, PART_DESCRIPTION),
                new QdasNumber(TextEvaluationType.Part, DEVICE)
            };
            return result;
        }

        private List<QdasNumber> GetDefaultListViewCharacteristicLabels()
        {
            var result = new List<QdasNumber>
            {
                new QdasNumber(TextEvaluationType.Characteristic, CHARACTERISTIC_NUMBER),
                new QdasNumber(TextEvaluationType.Characteristic, CHARACTERISTIC_DESCRIPTION)
            };
            return result;
        }

        private List<QdasNumberWrapper> GetDefaultPartGrid()
        {
            var result = new List<QdasNumberWrapper>
            {
                new QdasNumberWrapper(TextEvaluationType.Part, PART_NUMBER),
                new QdasNumberWrapper(TextEvaluationType.Part, PART_DESCRIPTION),
                new QdasNumberWrapper(TextEvaluationType.Part, DRAWING_NUMBER_TEXT),
                new QdasNumberWrapper(TextEvaluationType.Part, WORK_CYCLE_OPERATION),
                new QdasNumberWrapper(TextEvaluationType.Part, MACHINE_NUMBER)
            };
            return result;
        }

        private List<QdasNumberWrapper> GetDefaultCharacteristicGrid()
        {
            var result = new List<QdasNumberWrapper>
            {
                new QdasNumberWrapper(TextEvaluationType.Characteristic, NOMINAL_VALUE),
                new QdasNumberWrapper(TextEvaluationType.Characteristic, CHARACTERISTIC_NUMBER),
                new QdasNumberWrapper(TextEvaluationType.NumericAndTextResults, TOTAL_VALUES),
                new QdasNumberWrapper(TextEvaluationType.Characteristic, LOWER_ALLOWANCE),
                new QdasNumberWrapper(TextEvaluationType.Characteristic, LOWER_SPECIFICATION_LIMIT),
                new QdasNumberWrapper(TextEvaluationType.Characteristic, TEST_BEGIN),
                new QdasNumberWrapper(TextEvaluationType.NumericAndTextResults, AVERAGE),
                new QdasNumberWrapper(TextEvaluationType.Characteristic, UPPER_SPECIFICATION_LIMIT),
                new QdasNumberWrapper(TextEvaluationType.Characteristic, UNIT_DESCRIPTION),
                new QdasNumberWrapper(TextEvaluationType.NumericAndTextResults, TOTAL_EVALUATED),
                new QdasNumberWrapper(TextEvaluationType.NumericAndTextResults, NUMBER_OF_VALUES_OUTSIDE_TOLERANCE),
                new QdasNumberWrapper(TextEvaluationType.NumericAndTextResults, STANDARD_DEVIATION),
                new QdasNumberWrapper(TextEvaluationType.NumericAndTextResults, POTENTIAL_CAPABILITY_INDEX),
                new QdasNumberWrapper(TextEvaluationType.NumericAndTextResults, CRITICAL_CAPABILITY_INDEX)
            };
            return result;
        }
        #endregion

        [OnDeserialized()]
        void OnDeserialized(StreamingContext context)
        {
            if (_partLabelQdasNumbers == null || !_partLabelQdasNumbers.Any())
            {
                _partLabelQdasNumbers = GetDefaultListViewPartLabels();
            }
            if (_characteristicLabelQdasNumbers == null || !_characteristicLabelQdasNumbers.Any())
            {
                _characteristicLabelQdasNumbers = GetDefaultListViewCharacteristicLabels();
            }
            if (_partGridQdasNumbers == null || !_partGridQdasNumbers.Any())
            {
                _partGridQdasNumbers = GetDefaultPartGrid();
            }
            if (_characteristicGridQdasNumbers == null || !_characteristicGridQdasNumbers.Any())
            {
                _characteristicGridQdasNumbers = GetDefaultCharacteristicGrid();
            }
        }
    }
}