﻿using Promess.Common.Util;
using Promess.Language;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Pbld.Data.Validation
{
    /// <summary>
    /// Attribute for validation when annotated element must be greater or equal to another property
    /// </summary>
    public class GreaterOrEqualThanAttribute:ValidationAttribute
    {
        public GreaterOrEqualThanAttribute(string otherPropertyName)
        {
            OtherPropertyName = otherPropertyName;
        }

        public string OtherPropertyName { get; private set; }
        public string ErrorMessageKey { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //both values null must be handled by using the required attribute, so success can be returned if one value is null
            IComparable castedValue = value as IComparable;
            if (castedValue == null)
                return ValidationResult.Success;
            var otherValue = DataUtil.GetOtherValue(validationContext, OtherPropertyName);
            if (castedValue.CompareTo(otherValue)<0)
            {
                var text = (String)LanguageManager.Current.CurrentLanguageData.Translate(ErrorMessageKey ?? "GreaterOrEqualThanAttributeGenericErrorMessage");
                return new ValidationResult(text, new List<String>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}
