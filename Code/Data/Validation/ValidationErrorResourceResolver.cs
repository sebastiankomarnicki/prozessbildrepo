﻿using Promess.Language;
using System.Runtime.CompilerServices;

namespace Promess.Pbld.Data.Validation
{
    /// <summary>
    /// Resolver for validation error. Property names are used for lookup, do not rename without also changing the language definitions.
    /// </summary>
    public static class ValidationErrorResourceResolver
    {
        public static string ValidationRangeError => LoadErrorByName();
        public static string ValidationStringLengthError => LoadErrorByName();
        public static string ValidationRequiredError => LoadErrorByName();

        private static string LoadErrorByName([CallerMemberName] string methodName = "")
        {
            return (string) LanguageManager.Current.CurrentLanguageData.Translate(methodName);
        }
    }
}
