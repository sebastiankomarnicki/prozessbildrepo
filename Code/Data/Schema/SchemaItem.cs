﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Validation;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Promess.Pbld.Data.Schema
{
    /// <summary>
    /// Configuration for a visual representation of a <see cref="SpcPath"/>.
    /// </summary>
    [DataContract(Name = "SchemaItem")]
    public class SchemaItem : RectangleEditorItem
    {
        #region fields
        [DataMember(Name = "ImagePath")]
        private string _imagePath;
        [DataMember(Name = "Image")]
        private byte[] _image;
        [DataMember(Name = "HorizontalAlignment")]
        private HorizontalAlignment _horizontalAlignment;
        [DataMember(Name = "VerticalAlignment")]
        private VerticalAlignment _verticalAlignment;
        [DataMember(Name = "ImageSplitPercentage")]
        private double _imageSplitPercentage;
        [DataMember(Name = "Stretch")]
        private Stretch _stretch;
        #endregion

        public SchemaItem() : base()
        {
            _horizontalAlignment = HorizontalAlignment.Center;
            _verticalAlignment = VerticalAlignment.Middle;
            _stretch = Stretch.Fill;
            _imagePath = String.Empty;
            _imageSplitPercentage = 0.5;
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set { SetProperty(ref _imagePath, value); }
        }

        public byte[] Image
        {
            get { return _image; }
            set { SetProperty(ref _image, value); }
        }

        public HorizontalAlignment HorizontalAlignment
        {
            get { return _horizontalAlignment; }
            set { SetProperty(ref _horizontalAlignment, value); }
        }

        public VerticalAlignment VerticalAlignment
        {
            get { return _verticalAlignment; }
            set { SetProperty(ref _verticalAlignment, value); }
        }

        public Stretch Stretch
        {
            get { return _stretch; }
            set { SetProperty(ref _stretch, value); }
        }

        /// <summary>
        /// Split percentage for the image in relation to the size shared with other elements
        /// </summary>
        [Range(0, 1, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRangeError))]
        public double ImageSplitPercentage
        {
            get { return _imageSplitPercentage; }
            set { SetProperty(ref _imageSplitPercentage, value); }
        }

        public bool WasGenerated { get; set; }
    }
}