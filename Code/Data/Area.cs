﻿using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Schema;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Waf.Foundation;
using Microsoft.VisualStudio.Language.Intellisense;
using Promess.Pbld.Data.Validation;

namespace Promess.Pbld.Data
{
    /// <summary>
    /// Collection of <see cref="SpcPath"/> which are processed together. Enforces constraints and contains information for production schema.
    /// </summary>
    [DataContract]
    public class Area: ValidatableModel, IExtensibleDataObject
    {
        public const int MAXPATHS = 255;
        private string _name;

        [DataMember(Name = "Paths")]
        [Required]
        private BulkObservableCollection<SpcPath> _paths;
        private ReadOnlyObservableCollection<SpcPath> _readonlyPaths;
        private string _schemaFilePath;
        private bool _showErrorCounter;
        private bool _showIcon;
        private bool _showImage;
        private Dictionary<Guid,SchemaItem> _spcGuidToSchemaItem;
        private AspectRatio _aspectRatio;

        public Area(string name)
        {
            SetDefaults();
            this._name = name;
            this._paths = new BulkObservableCollection<SpcPath>();
            this._readonlyPaths = new ReadOnlyObservableCollection<SpcPath>(_paths);
            this._spcGuidToSchemaItem = new Dictionary<Guid, SchemaItem>();
        }

        private void SetDefaults()
        {
            _schemaFilePath = String.Empty;
            _showErrorCounter = false;
            _showIcon = false;
            _showImage = false;
            if (_spcGuidToSchemaItem == null)
                _spcGuidToSchemaItem = new Dictionary<Guid, SchemaItem>();
        }

        [DataMember(Name = "Name")]
        [Required(ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationRequiredError))]
        [StringLength(50, ErrorMessageResourceType = typeof(ValidationErrorResourceResolver), ErrorMessageResourceName = nameof(ValidationErrorResourceResolver.ValidationStringLengthError))]
        public string Name
        {
            get { return _name; }
            internal set { SetPropertyAndValidate(ref _name, value); }
        }

        public ReadOnlyObservableCollection<SpcPath> ReadonlyPaths
        {
            get { return _readonlyPaths; }
        }

        [DataMember(Name = "SchemaFilePath")]
        public string SchemaFilePath
        {
            get { return _schemaFilePath; }
            set { SetProperty(ref _schemaFilePath, value); }
        }

        [DataMember(Name = "ShowErrorCounter")]
        public bool ShowErrorCounter
        {
            get { return _showErrorCounter; }
            set { SetProperty(ref _showErrorCounter, value); }
        }

        [DataMember(Name = "ShowIcon")]
        public bool ShowIcon
        {
            get { return _showIcon; }
            set { SetProperty(ref _showIcon, value); }
        }

        [DataMember(Name="ShowImage")]
        public bool ShowImage
        {
            get { return _showImage; }
            set { SetProperty(ref _showImage, value); }
        }

        public bool AddSpcPaths(IEnumerable<SpcPath> toAdd)
        {
            try
            {
                this._paths.BeginBulkOperation();
                return toAdd.Select(spcPath => AddSpcPath(spcPath)).All(x=>x);
            }
            finally
            {
                this._paths.EndBulkOperation();
            }    
        }

        public bool AddSpcPath(SpcPath toAdd)
        {
            if (toAdd.Guid.Equals(Guid.Empty) || _paths.Any(path => path.Guid.Equals(toAdd.Guid)))
                throw new ArgumentOutOfRangeException("Guid must not be empty or already present.");
            if (!CanAddPath())
                return false;
            if (DoesSpcNumberAlreadyExist(toAdd.SpcNumber))
                return false;

            _paths.Add(toAdd);
            return true;
        }

        public bool ReplaceSpcPath(SpcPath oldPath, SpcPath newPath)
        {
            if (newPath.Guid.Equals(Guid.Empty) ||  !oldPath.Guid.Equals(newPath.Guid) && _paths.Any(path => path.Guid.Equals(newPath.Guid)))
                throw new ArgumentOutOfRangeException("Guids must match or not already be in use as well as not be Empty.");
            int index = _paths.IndexOf(oldPath);
            if (index < 0)
                return false;
            if (oldPath.SpcNumber != newPath.SpcNumber && DoesSpcNumberAlreadyExist(newPath.SpcNumber))
                return false;

            _paths[index] = newPath;
            return true;
        }

        public bool CanAddPath()
        {
            return _paths.Count <= MAXPATHS;
        }

        public bool DoesSpcNumberAlreadyExist(short spcNumber)
        {
            return _paths.Any(path => path.SpcNumber == spcNumber);
        }

        public short? GetNextFreeSpcNumber()
        {
            if (ReadonlyPaths.Count >= MAXPATHS)
                return null;

            for (short i = 1; i <= ReadonlyPaths.Count + 1; i++)
            {
                if (!ReadonlyPaths.Any(sp => sp.SpcNumber == i))
                    return i;
            }
            //cannot be reached
            return null;
        }

        public bool RemoveSpcPath(SpcPath toRemove)
        {
            if (_paths.Remove(toRemove))
            {
                _spcGuidToSchemaItem.Remove(toRemove.Guid);
                return true;
            }else
            {
                return false;
            }
        }

        [DataMember(Name = "SchemaItems")]
        public Dictionary<Guid, SchemaItem> SpcGuidSchemaItem
        {
            get { return _spcGuidToSchemaItem; }
            set { SetProperty(ref _spcGuidToSchemaItem, value); }
        }

        [DataMember(Name = "AspectRatio")]
        public AspectRatio AspectRatio
        {
            get { return _aspectRatio; }
            set { SetProperty(ref _aspectRatio, value); }
        }


        #region IExtensibleDataObject
        private ExtensionDataObject _extensionData;

        public virtual ExtensionDataObject ExtensionData
        {
            get { return _extensionData; }
            set { _extensionData = value; }
        }
        #endregion

        protected void OnDeserializingHandler(StreamingContext context)
        {
            this.SetDefaults();
        }

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            this._readonlyPaths = new ReadOnlyObservableCollection<SpcPath>(this._paths);
        }
    }
}
