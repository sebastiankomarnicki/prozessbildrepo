﻿using System.Collections.Specialized;
using System.Windows.Controls;
using Microsoft.Xaml.Behaviors;

namespace Promess.Common.Behaviors
{
    /// <summary>
    /// Auto scrolling behavior for <see cref="ListBox"/>. Scrolls to last item when an item is added.
    /// </summary>
    public class AutoScrollBehavior:Behavior<ListBox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            var collection = AssociatedObject.Items.SourceCollection as INotifyCollectionChanged;
            
            if (collection!=null)
                collection.CollectionChanged += collection_CollectionChanged;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            var collection = AssociatedObject.Items.SourceCollection as INotifyCollectionChanged;

            if (collection != null)
                collection.CollectionChanged -= collection_CollectionChanged;
        }


        private void collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
                ScrollToLastItem();
        }

        private void ScrollToLastItem()
        {
            int count = AssociatedObject.Items.Count;
            if (count > 0)
            {
                AssociatedObject.ScrollIntoView(AssociatedObject.Items[count - 1]);
            }
        }
    }
}
