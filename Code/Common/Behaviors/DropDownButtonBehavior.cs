﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Microsoft.Xaml.Behaviors;

namespace Promess.Common.Behaviors
{
    /// <summary>
    /// Behavior to display a context menu when a <see cref="Button"/> is clicked as
    /// a drop down menu.
    /// </summary>
    public class DropDownButtonBehavior:Behavior<Button>
    {
        private long _attachedCount;
        private bool _isContextMenuOpen;

        /// <summary>
        /// Attaches handler for clicking the button
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.AddHandler(Button.ClickEvent,
                new RoutedEventHandler(AssociatedObject_Click), true);
        }

        private void AssociatedObject_Click(object sender, RoutedEventArgs e)
        {
            Button source = sender as Button;
            if (source == null || source.ContextMenu == null)
                return;
            if (_isContextMenuOpen)
                return;

            source.ContextMenu.AddHandler(ContextMenu.ClosedEvent,
                new RoutedEventHandler(ContextMenu_Closed), true);
            Interlocked.Increment(ref _attachedCount);
            source.ContextMenu.PlacementTarget = source;
            source.ContextMenu.Placement = PlacementMode.Bottom;
            source.ContextMenu.IsOpen = true;
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            _isContextMenuOpen = false;
            ContextMenu cm = sender as ContextMenu;
            if (cm == null)
                return;

            cm.RemoveHandler(ContextMenu.ClosedEvent, new RoutedEventHandler(ContextMenu_Closed));
            Interlocked.Decrement(ref _attachedCount);
        }

        /// <summary>
        /// Detaches handler for clicking the button
        /// </summary>
        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.RemoveHandler(Button.ClickEvent, new RoutedEventHandler(AssociatedObject_Click));
        }
    }
}
