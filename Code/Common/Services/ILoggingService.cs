﻿using System;

namespace Promess.Common.Services
{
    /// <summary>
    /// Base for a logging service with different severity levels
    /// </summary>
    public interface ILoggingService
    {
        void Warning(String message);

        void Info(String message);

        void Error(String message, Exception e);

        void ClearLogEntries();
    }
}
