﻿using System;
using System.Collections.Generic;
using System.Waf.Applications.Services;

namespace Promess.Common.Services
{
    /// <summary>
    /// Additional options for a service providing file access.
    /// </summary>
    public interface IFileDialogServiceExtended:IFileDialogService
    {
        /// <summary>
        /// Shows the open folder dialog box that allows a user to specify a folder that should be selected.
        /// </summary>
        /// <param name="owner">The window that owns this OpenFolderDialog.</param>
        /// <param name="defaultDirectoryName">Default directory name. The directory name is used as initial directory when it is specified.</param>
        /// <returns>A FileDialogResult object which contains the directory name selected by the user.</returns>
        String ShowOpenFolderDialog(object owner, string defaultDirectoryName);

        /// <summary>
        /// Show dialog for selecting a file of a given type.
        /// </summary>
        /// <param name="owner">Visual owner of this dialog</param>
        /// <param name="fileType">Type of the selectable file</param>
        /// <param name="selectedFileMustExist">Set whether the file must exist</param>
        /// <returns>Collection with selected file if any.</returns>
        ICollection<string> ShowOpenFileDialog(object owner, FileType fileType, bool selectedFileMustExist);

        /// <summary>
        /// Show dialog for selecting a file of a given type with preselected file.
        /// </summary>
        /// <param name="owner">Visual owner of this dialog</param>
        /// <param name="fileType">Type of the selectable file</param>
        /// <param name="defaultFileName">Preselected default file</param>
        /// <param name="selectedFileMustExist">Set whether the file must exist</param>
        /// <returns>Collection with selected file if any.</returns>
        ICollection<string> ShowOpenFileDialog(object owner, FileType fileType, string defaultFileName, bool selectedFileMustExist);

        /// <summary>
        /// Show dialog for selecting multiple files of a given type
        /// </summary>
        /// <param name="owner">Visual owner of this dialog</param>
        /// <param name="fileType">Type of the selectable files</param>
        /// <returns>Collection with the selection, if any</returns>
        ICollection<string> ShowOpenFilesDialog(object owner, FileType fileType);

        /// <summary>
        /// Show dialog for selecting multiple files of a given type
        /// </summary>
        /// <param name="owner">Visual owner of this dialog</param>
        /// <param name="fileType">Type of the selectable files</param>
        /// <param name="baseDirectory">Base directory when showing the dialog</param>
        /// <returns>Collection with the selection, if any</returns>
        ICollection<string> ShowOpenFilesDialog(object owner, FileType fileType, string baseDirectory);

        /// <summary>
        /// Show dialog for selecting multiple files of given types with default selected display type
        /// </summary>
        /// <param name="owner">Visual owner of this dialog</param>
        /// <param name="fileType">Types of the selectable files</param>
        /// <param name="defaultFileType">Type which is selected by default</param>
        /// <returns>Collection with the selection, if any</returns>
        ICollection<string> ShowOpenFilesDialog(object owner, IEnumerable<FileType> fileTypes, FileType defaultFileType);
    }
}
