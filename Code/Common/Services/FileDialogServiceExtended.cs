﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Waf.Applications.Services;
using System.Waf.Presentation.Services;
using Ookii.Dialogs.Wpf;
using System.IO;
using System.Windows;
using Microsoft.Win32;
using System.Diagnostics;

namespace Promess.Common.Services
{
    [Export(typeof(IFileDialogServiceExtended))]
    public class FileDialogServiceExtended : FileDialogService, IFileDialogServiceExtended
    {
        /// <inheritdoc/>
        public String ShowOpenFolderDialog(object owner, string defaultDirectoryName)
        {
            VistaFolderBrowserDialog browserDialog = new VistaFolderBrowserDialog();
            return ShowFolderBrowserDialog(owner, browserDialog, defaultDirectoryName);
        }

        private String ShowFolderBrowserDialog(object owner, VistaFolderBrowserDialog browserDialog, string defaultDirectoryName)
        {
            if (string.IsNullOrWhiteSpace(defaultDirectoryName))
            {
                defaultDirectoryName = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }

            string directory = EnsureDirectoryEndsWithSeparator(defaultDirectoryName);          
            browserDialog.SelectedPath = directory;
            try
            {
                if (browserDialog.ShowDialog(owner as Window) == true)
                {
                    return browserDialog.SelectedPath;
                }
            }
            catch { }
            return null;
        }

        private static string EnsureDirectoryEndsWithSeparator(string directory)
        {
            directory = directory.TrimEnd();
            if (directory.Equals("."))
                return directory;
            if (!directory.EndsWith(Path.DirectorySeparatorChar + "") && !directory.EndsWith(Path.AltDirectorySeparatorChar + ""))
            {
                directory += Path.DirectorySeparatorChar;
            }
            return directory;
        }

        /// <inheritdoc/>
        public ICollection<string> ShowOpenFileDialog(object owner, FileType fileType, bool selectedFileMustExist)
        {
            OpenFileDialog dialog = new OpenFileDialog() { CheckFileExists = selectedFileMustExist };
            return ShowFileDialog(owner, dialog, new FileType[] { fileType }, fileType);
        }

        /// <inheritdoc/>
        public ICollection<string> ShowOpenFileDialog(object owner, FileType fileType, string defaultFileName, bool selectedFileMustExist)
        {
            OpenFileDialog dialog = new OpenFileDialog() { CheckFileExists = selectedFileMustExist };
            return ShowFileDialog(owner, dialog, new FileType[] { fileType }, fileType, defaultFileName);
        }

        /// <inheritdoc/>
        public ICollection<String> ShowOpenFilesDialog(object owner, FileType fileType)
        {
            return ShowOpenFilesDialog(owner, new FileType[] { fileType }, fileType);
        }

        /// <inheritdoc/>
        public ICollection<String> ShowOpenFilesDialog(object owner, IEnumerable<FileType> fileTypes, FileType defaultFileType)
        {
            OpenFileDialog dialog = new OpenFileDialog() { Multiselect = true, CheckFileExists=true };
            dialog.CheckFileExists = true;
            return ShowFileDialog(owner, dialog, fileTypes, defaultFileType);
        }

        /// <inheritdoc/>
        public ICollection<string> ShowOpenFilesDialog(object owner, FileType fileType, string baseDirectory)
        {
            OpenFileDialog dialog = new OpenFileDialog() { Multiselect = true, CheckFileExists=true };
            baseDirectory = EnsureDirectoryEndsWithSeparator(baseDirectory);
            return ShowFileDialog(owner, dialog, new FileType[] { fileType }, fileType, baseDirectory);
        }

        private ICollection<String> ShowFileDialog(object owner, OpenFileDialog dialog, IEnumerable<FileType> fileTypes, FileType defaultFileType, string defaultFileName="")
        {
            if (fileTypes == null) { throw new ArgumentNullException("fileTypes"); }
            if (!fileTypes.Any()) { throw new ArgumentException("The fileTypes collection must contain at least one item."); }

            TrySetDefaultDirAndFile(dialog, defaultFileName);

            dialog.Filter = CreateFilter(fileTypes);
            int filterIndex = fileTypes.ToList().IndexOf(defaultFileType);
            if (filterIndex >= 0) { dialog.FilterIndex = filterIndex + 1; }

            try
            {
                if (dialog.ShowDialog(owner as Window) == true)
                {
                    filterIndex = dialog.FilterIndex - 1;
                    if (filterIndex >= 0 && filterIndex < fileTypes.Count())
                    {
                        defaultFileType = fileTypes.ElementAt(filterIndex);
                    }
                    else
                    {
                        defaultFileType = null;
                    }
                    return dialog.FileNames;
                }
            }
            catch { }
            return new string[0];
        }

        /// <summary>
        /// Set default directory and file name if applicable. Error when processing input does not set these.
        /// </summary>
        private static void TrySetDefaultDirAndFile(OpenFileDialog dialog, string defaultFileName)
        {
            if (!string.IsNullOrEmpty(defaultFileName))
            {
                try
                {
                    dialog.InitialDirectory = Path.GetDirectoryName(defaultFileName);
                    dialog.FileName = Path.GetFileName(defaultFileName);
                }
                catch { }            
            }
        }

        private static string CreateFilter(IEnumerable<FileType> fileTypes)
        {
            Debug.Assert(fileTypes.Any());
            var sb = new StringBuilder();
            foreach (FileType fileType in fileTypes)
            {
                sb.Append(fileType.Description);
                sb.Append('|');
                sb.Append(String.Join(";", fileType.FileExtensions));
                sb.Append('|');
            }
            sb.Length = sb.Length - 1;
            return sb.ToString();
        }
    }
}
