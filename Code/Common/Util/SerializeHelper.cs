﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Promess.Common.Util
{
    /// <summary>
    /// Helper for serialization
    /// </summary>
    public class SerializeHelper
    {
        private static XmlWriterSettings _xmlWriterSettings;

        static SerializeHelper()
        {
            _xmlWriterSettings = new XmlWriterSettings()
            {
                NewLineOnAttributes = true,
                Indent = true,
                IndentChars = "\t",
                Encoding = Encoding.UTF8
            };
        }

        /// <summary>
        /// Create a copy of an item using its <see cref="DataContract"/>, which must be defined
        /// </summary>
        /// <typeparam name="T">Type of the item</typeparam>
        /// <param name="toCopy">Item to copy</param>
        /// <returns>A copy</returns>
        /// <exception cref="SerializationException">There is a problem with the instance being serialized.</exception>
        /// <exception cref="System.ServiceModel.QuotaExceededException">The maximum number of objects to serialize has been exceeded. Check <see cref="DataContractSerializer.MaxItemsInObjectGraph"/></exception>
        public static T CreateDataContractCopy<T>(T toCopy)
        {
            if (typeof(T).GetMembers().Any(x=>x.IsDefined(typeof(DataContractAttribute),false)))
            {
                throw new InvalidOperationException($"The class of the given instance must implement {nameof(DataContractAttribute)}");
            }
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            T copy = default(T);
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, toCopy);
                stream.Position = 0;
                copy = (T)serializer.ReadObject(stream);
            }
            return copy;
        }

        /// <summary>
        /// Create a <see cref="String"/> representation of an item using its <see cref="DataContract"/>, which must be defined
        /// </summary>
        /// <param name="obj">Item to serialize</param>
        /// <returns><see cref="String"/> representation</returns>
        /// <exception cref="SerializationException">There is a problem with the instance being serialized.</exception>
        public static string Serialize(Object obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                DataContractSerializer ser = new DataContractSerializer(obj.GetType());
                ser.WriteObject(ms, obj);
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }

        /// <summary>
        /// Serialize an item to a file using its <see cref="DataContract"/>
        /// </summary>
        /// <typeparam name="T">Type of the item</typeparam>
        /// <param name="file">Target file for the serialization result</param>
        /// <param name="data">Item to serialize</param>
        /// <exception cref="Exception">An exception can occur due to file access</exception>
        public static void SerializeToFile<T>(string file, T data)
        {
            using (Stream stream = new FileStream(file, FileMode.Create, FileAccess.Write))
            {
                SerializeToStream<T>(stream, data);
            }
        }

        /// <summary>
        /// Serialize an item as xml to a <see cref="Stream"/> using its <see cref="DataContract"/>
        /// </summary>
        /// <typeparam name="T">Type of the item</typeparam>
        /// <param name="stream">Serialization target</param>
        /// <param name="data">Item to serialize</param>
        /// <exception cref="SerializationException">There is a problem with the instance being serialized.</exception>
        public static void SerializeToStream<T>(Stream stream, T data)
        {
            using (var tmpWriter = XmlWriter.Create(stream, _xmlWriterSettings))
            {
                using (XmlDictionaryWriter writer = XmlDictionaryWriter.CreateDictionaryWriter(tmpWriter))
                {
                    writer.WriteStartDocument();
                    DataContractSerializer ser = new DataContractSerializer(typeof(T));
                    ser.WriteObject(writer, data);
                }
            }
        }

        /// <summary>
        /// Deserialize item from file using its <see cref="DataContract"/>
        /// </summary>
        /// <typeparam name="T">Type of the item</typeparam>
        /// <param name="file">File containing serialized item</param>
        /// <param name="mustExist">Must the file exist?</param>
        /// <returns>Deserialized item or <c>default(T)</c> when there was nothing to deserialize</returns>
        /// <exception cref="Exception">An exception can occur due to file access</exception>
        public static T DeserializeFromFile<T>(string file, bool mustExist = false)
        {
            FileMode mode = mustExist ? FileMode.Open : FileMode.OpenOrCreate;
            using (Stream stream = new FileStream(file, mode, FileAccess.Read))
            {
                return DeserializeFromStream<T>(stream);
            }
        }

        /// <summary>
        /// Try to deserialize an item from a stream using its <see cref="DataContract"/>
        /// </summary>
        /// <typeparam name="T">Type of the item</typeparam>
        /// <param name="stream">Source</param>
        /// <returns>Deserialized item, <c>default(T)</c> or null</returns>
        public static T DeserializeFromStream<T>(Stream stream)
        {
            try
            {
                if (stream.Length == 0)
                {
                    return default(T);
                }
            }
            catch (NotSupportedException) { }
          
            DataContractSerializer ser = new DataContractSerializer(typeof(T));
            return (T)ser.ReadObject(stream);
        }

        /// <summary>
        /// Deserialize an item using its <see cref="DataContract"/> from a raw xml <see cref="String"/>
        /// </summary>
        /// <typeparam name="T">Type of the item</typeparam>
        /// <param name="rawXml">Item serialized as xml</param>
        /// <returns>Deserialized item</returns>
        public static T Deserialize<T>(string rawXml)
        {
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(rawXml)))
            {
                DataContractSerializer ser = new DataContractSerializer(typeof(T));
                return (T)ser.ReadObject(ms);
            }
        }
    }
}
