﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Promess.Common.Util
{
    /// <summary>
    /// Grid with <see cref="ChildMargin" /> property to implement spacing of lines and columns by replacing(!) the margin of its children.
    /// Note: Not needed anymore if ever ported to Xamarin because there are properties for this behavior.
    /// </summary>
    public class GridWithSpacing:Grid
    {
        /// <summary>
        /// Identifies the <see cref="ChildMargin" /> dependency property.
        /// </summary>
        public static readonly DependencyProperty ChildMarginProperty =
            DependencyProperty.Register("ChildMargin", typeof(Thickness), typeof(GridWithSpacing),
                new FrameworkPropertyMetadata(new Thickness(0,0,0,0), FrameworkPropertyMetadataOptions.AffectsMeasure, OnChildMarginChanged));

        public Thickness ChildMargin
        {
            get { return (Thickness)GetValue(ChildMarginProperty); }
            set { SetValue(ChildMarginProperty, value); }
        }

        public GridWithSpacing():base()
        {
            Loaded += new RoutedEventHandler(GridWithSpacing_Loaded);
        }

        private void GridWithSpacing_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (UIElement child in this.Children)
            {
                // FrameworkElement introduces the MarginProperty
                if (child is FrameworkElement)
                {
                    Binding binding = new Binding();
                    binding.Source = this;
                    binding.Path = new PropertyPath("ChildMargin");

                    // replace childs margin binding
                    BindingOperations.SetBinding(child, FrameworkElement.MarginProperty, binding);
                }
            }
            e.Handled = true;
        }

        private static void OnChildMarginChanged(DependencyObject item, DependencyPropertyChangedEventArgs args)
        {
            GridWithSpacing grid = item as GridWithSpacing;

            grid.UpdateLayout();
        }
    }
}

