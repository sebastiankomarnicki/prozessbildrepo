﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Promess.Common.Util
{
    /// <summary>
    /// Helper for retrying operations
    /// </summary>
    public static class RetryHelper
    {
        /// <summary>
        /// Will retry executing a function multiple times with a delay between retries until retry count is exhausted.
        /// Useful for when a function sporadically throws exceptions independent of its parameters.
        /// </summary>
        /// <typeparam name="T">Return type of the function</typeparam>
        /// <param name="func">Function to execute</param>
        /// <param name="times">Number of times to execute, should be >1</param>
        /// <param name="delay">Delay before first retry</param>
        /// <returns>Result of the function</returns>
        /// <exception cref="AggregateException">Will be thrown after retries are exhausted. Contains all caught exceptions</exception>
        public static T DoAndRetry<T>(Func<T> func, int times, TimeSpan delay)
        {
            var exceptions = new List<Exception>();
            for (int i = 0; i < times; i++)
            {
                try
                {
                    if (i > 0)
                        Thread.Sleep(delay);

                    return func();
                }catch(Exception e)
                {
                    exceptions.Add(e);
                }
            }
            throw new AggregateException(exceptions);
        }

        /// <summary>
        /// Async version of <see cref="DoAndRetry{T}(Func{T}, int, TimeSpan)"/>
        /// </summary>
        /// <typeparam name="T">Return type of function</typeparam>
        /// <param name="func">Function to execute</param>
        /// <param name="times">Number of times to execute, should be >1</param>
        /// <param name="delay">Delay before first retry</param>
        /// <returns>Result of the function</returns>
        /// <exception cref="AggregateException">Will be thrown after retries are exhausted. Contains all caught exceptions</exception>
        public static async Task<T> DoAndRetryAsync<T>(Func<T> func, int times, TimeSpan delay)
        {
            var exceptions = new List<Exception>();
            for (int i = 0; i < times; i++)
            {
                try
                {
                    if (i > 0)
                        await Task.Delay(delay);

                    return func();
                }
                catch (Exception e)
                {
                    exceptions.Add(e);
                }
            }
            throw new AggregateException(exceptions);
        }
    }
}
