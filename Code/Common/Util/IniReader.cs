﻿using System.Text;
using System.Runtime.InteropServices;

namespace Promess.Common.Util
{
    /// <summary>
    /// Class to read ini files in unicode. <see cref="capacity"/> for maximum entry length.
    /// </summary>
    public class IniReader
    {
        [DllImport("kernel32", CharSet=CharSet.Unicode)]
        private static extern int GetPrivateProfileString(string section, string key,
            string defaultValue, StringBuilder value, int size, string filePath);

        /// <summary>
        /// Maximum length when reading an ini line
        /// </summary>
        public const int capacity = 512;

        private readonly string _filepath;

        /// <summary>
        /// Create reader for specific file
        /// </summary>
        /// <param name="filepath"></param>
        public IniReader(string filepath)
        {
            this._filepath = filepath;
        }

        /// <summary>
        /// Read a <see cref="System.String"/> value
        /// </summary>
        /// <param name="section">Section of the key</param>
        /// <param name="key">Key for the value</param>
        /// <param name="defaultValue">default</param>
        /// <returns>Read value or default</returns>
        public string ReadValue(string section, string key, string defaultValue = "")
        {
            StringBuilder builder = new StringBuilder(capacity);
            GetPrivateProfileString(section, key, defaultValue, builder, builder.Capacity, _filepath);
            return builder.ToString();
        }

        /// <summary>
        /// Read a value and interpret it as <see cref="bool"/>
        /// </summary>
        /// <param name="section">Section of the key</param>
        /// <param name="key">Key for the value</param>
        /// <param name="defaultValue">default</param>
        /// <returns>Interpreted value or default when value could not be parsed or was missing</returns>
        public bool GetBool(string section, string key, bool defaultValue)
        {
            string value = ReadValue(section, key);
            if (int.TryParse(value, out int parsedValue))
            {
                int notDefault = defaultValue ? 0 : 1;
                if (parsedValue == notDefault)
                {
                    return !defaultValue;
                }
                else
                {
                    return defaultValue;
                }
            }
            else
            {
                return defaultValue;
            }
        }
    }
}
