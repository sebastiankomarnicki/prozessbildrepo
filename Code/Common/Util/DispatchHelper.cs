﻿using System;
using System.Windows;

namespace Promess.Common.Util
{
    /// <summary>
    /// Helper for elevating executing methods on applications main thread by dispatching to the thread if required.
    /// </summary>
    public static class DispatchHelper
    {
        /// <summary>
        /// Ensure that the given function is invoked on the applications main thread (GUI). Either by invoking directly when of the correct thread or dispatching.
        /// </summary>
        /// <typeparam name="T">Return type of the function</typeparam>
        /// <param name="toExecute">Function to invoke</param>
        /// <returns>Result of the function</returns>
        public static T Invoke<T>(Func<T> toExecute)
        {
            if (Application.Current.CheckAccess())
            {
                return toExecute();
            }
            else
            {
                return Application.Current.Dispatcher.Invoke(toExecute);
            }
        }

        /// <summary>
        /// Ensure that the given action is invoked on the applications main thread (GUI). Either by invoking directly when of the correct thread or dispatching.
        /// </summary>
        /// <param name="toExecute">Action to invoke</param>
        public static void Invoke(Action toExecute)
        {
            if (Application.Current.CheckAccess())
            {
                toExecute();
            }
            else
            {
                Application.Current.Dispatcher.Invoke(toExecute);
            }
        }

        /// <summary>
        /// Ensure that the given action is invoked on the applications main thread (GUI). Either by invoking directly when of the correct thread or dispatch scheduled for the main thread. This blocks the main thread when action is executed.
        /// </summary>
        /// <param name="toExecute">Action to execute</param>
        public static void BeginInvoke(Action toExecute)
        {
            if (Application.Current.CheckAccess())
            {
                toExecute();
            }
            else
            {
                Application.Current.Dispatcher.BeginInvoke(toExecute);
            }
        }
    }
}
