﻿using System.Windows;

namespace Promess.Common.Util
{
    /// <summary>
    /// Helps with Binding if binding target is not part of the visual tree.
    /// Original code from Thomas Levesque
    /// </summary>
    public class BindingProxy : Freezable
    {
        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register(
                nameof(Data), typeof(object), typeof(BindingProxy),new UIPropertyMetadata(null)
                    );

        public object Data
        {
            get { return GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        protected override Freezable CreateInstanceCore()
        {
            return new BindingProxy();
        }


    }
}
