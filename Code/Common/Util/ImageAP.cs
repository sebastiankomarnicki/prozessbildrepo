﻿using System.Windows;
using System.Windows.Media;

namespace Promess.Common.Util { 
    /// <summary>
    /// Attached property for an image
    /// </summary>
    public class ImageAP
    {
        public static readonly DependencyProperty ImageProperty;

        static ImageAP()
        {
            var metadata = new FrameworkPropertyMetadata((ImageSource)null);
            ImageProperty = DependencyProperty.RegisterAttached("Image",
                                                                typeof(ImageSource),
                                                                typeof(ImageAP), metadata);
        }

        public static ImageSource GetImage(DependencyObject obj)
        {
            return (ImageSource)obj.GetValue(ImageProperty);
        }

        public static void SetImage(DependencyObject obj, ImageSource value)
        {
            obj.SetValue(ImageProperty, value);
        }
    }
}
