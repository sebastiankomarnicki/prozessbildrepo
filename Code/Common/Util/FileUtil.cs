﻿using System;
using System.IO;

namespace Promess.Common.Util
{
    /// <summary>
    /// Utility class for file operations
    /// </summary>
    public static class FileUtil
    {
        /// <summary>
        /// Adds subfolders from one path to another path
        /// </summary>
        /// <param name="basePath">Path to which folders are added</param>
        /// <param name="dataPath">Path from which subfolders are taken. First entry will be ignored, assumed as drive or share.</param>
        /// <param name="depth">Maximum depth (number of folders) to add</param>
        /// <returns>New path with added folders</returns>
        public static string GetPath(string basePath, string dataPath, int depth)
        {
            string result = basePath;
            if (depth >= 0)
            {
                char[] sepArr = new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar };
                string[] entries = dataPath.Split(sepArr, StringSplitOptions.RemoveEmptyEntries);
                for (int i = Math.Max(1, entries.Length - depth); i < entries.Length; i++)
                {
                    result = Path.Combine(result, entries[i]);
                }
            }
            if (!result.EndsWith(Path.DirectorySeparatorChar + "") && !result.EndsWith(Path.AltDirectorySeparatorChar + ""))
            {
                result += Path.DirectorySeparatorChar;
            }
            return result;
        }
    }
}
