﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading;

namespace Promess.Common.Util.Async
{
    /// <summary>
    /// Simple version of an observable concurrent dictionary. No batch operations, meaning it is not optimal to use collection
    /// manipulation functions for many elements. Rather replace the whole collection if this is the case, otherwise many events will be fired and synchronization will be slow.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class SimpleObservableConcurrentDictionary<TKey,TValue> :IDictionary<TKey,TValue>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        private readonly ConcurrentDictionary<TKey, TValue> _wrappedDict;
        private readonly SynchronizationContext _syncContext;

        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        public SimpleObservableConcurrentDictionary()
        {
            _syncContext = AsyncOperationManager.SynchronizationContext;
            _wrappedDict = new ConcurrentDictionary<TKey, TValue>();
        }

        private void UpdateWithNotification(TKey key, TValue value)
        {
            _wrappedDict[key] = value;
            NotifyObserversOfAddOrUpdate(key, value);
        }

        private bool TryAddWithNotification(TKey key, TValue value)
        {
            bool result = _wrappedDict.TryAdd(key, value);
            if (result)
                NotifyObserversOfAdd(key, value);
            return result;
        }

        private bool TryRemoveWithNotification(TKey key, out TValue value)
        {
            bool result = _wrappedDict.TryRemove(key, out value);
            if (result)
                NotifyObserversOfRemove(key, value);
            return result;
        }

        private void NotifyObserversOfRemove(TKey key, TValue value)
        {
            var collectionHandler = CollectionChanged;
            var propertyHandler = PropertyChanged;
            if (collectionHandler != null || propertyHandler != null)
            {
                _syncContext.Post(s =>
                {
                    if (collectionHandler != null)
                    {
                        collectionHandler(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, new KeyValuePair<TKey, TValue>(key, value)));
                    }
                    if (propertyHandler != null)
                    {
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Count)));
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Keys)));
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Values)));
                    }
                }, null);
            }
        }

        private void NotifyObserversOfAddOrUpdate(TKey key, TValue value)
        {
            var collectionHandler = CollectionChanged;
            var propertyHandler = PropertyChanged;
            if (collectionHandler != null || propertyHandler != null)
            {
                _syncContext.Post(s =>
                {
                    if (collectionHandler != null)
                    {
                        collectionHandler(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new KeyValuePair<TKey, TValue>(key, value)));
                    }
                    if (propertyHandler != null)
                    {
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Count)));
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Keys)));
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Values)));
                    }
                }, null);
            }
        }

        private void NotifyObserversOfAdd(TKey key, TValue value)
        {
            var collectionHandler = CollectionChanged;
            var propertyHandler = PropertyChanged;
            if (collectionHandler != null || propertyHandler != null)
            {
                _syncContext.Post(s =>
                {
                    if (collectionHandler != null)
                    {
                        collectionHandler(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, new KeyValuePair<TKey, TValue>(key, value)));
                    }
                    if (propertyHandler != null)
                    {
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Count)));
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Keys)));
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Values)));
                    }
                }, null);
            }
        }

        private void NotifyObserversOfClear()
        {
            var collectionHandler = CollectionChanged;
            var propertyHandler = PropertyChanged;
            if (collectionHandler != null || propertyHandler != null)
            {
                _syncContext.Post(s =>
                {
                    if (collectionHandler != null)
                    {
                        collectionHandler(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    }
                    if (propertyHandler != null)
                    {
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Count)));
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Keys)));
                        propertyHandler(this, new PropertyChangedEventArgs(nameof(Values)));
                    }
                }, null);
            }
        }

        /// <inheritdoc/>
        public TValue this[TKey key]
        {
            get
            {
                return _wrappedDict[key];
            }

            set
            {
                UpdateWithNotification(key, value);
            }
        }

        public int Count
        {
            get
            {
                return _wrappedDict.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return ((ICollection<KeyValuePair<TKey, TValue>>)_wrappedDict).IsReadOnly;
            }
        }

        public ICollection<TKey> Keys
        {
            get
            {
                return _wrappedDict.Keys;
            }
        }

        public ICollection<TValue> Values
        {
            get
            {
                return _wrappedDict.Values;
            }
        }

        /// <summary>
        /// Add an item with key and value with notification
        /// </summary>
        /// <param name="item">Item containing key and value to add</param>
        public void Add(KeyValuePair<TKey, TValue> item)
        {
            TryAddWithNotification(item.Key, item.Value);
        }

        /// <summary>
        /// Add a value with a specific key and notification
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value</param>
        public void Add(TKey key, TValue value)
        {
            TryAddWithNotification(key, value);
        }

        /// <summary>
        /// Clear collection and notify of changed collection, keys, values and count
        /// </summary>
        public void Clear()
        {
            _wrappedDict.Clear();
            NotifyObserversOfClear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _wrappedDict.Contains(item);
        }

        public bool ContainsKey(TKey key)
        {
            return _wrappedDict.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            ((ICollection<KeyValuePair<TKey, TValue>>)_wrappedDict).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _wrappedDict.GetEnumerator();
        }

        /**
         * Tries to remove the entry with the given key.
         * Use at your own risk, functionality might change.
         */
        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            TValue tmp;
            return TryRemoveWithNotification(item.Key, out tmp);
        }

        public bool Remove(TKey key)
        {
            TValue tmp;
            return TryRemoveWithNotification(key, out tmp);
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _wrappedDict.TryGetValue(key, out value);
        }

        /// <inheritdoc/>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
