﻿using System.ComponentModel;

namespace Promess.Common.Util.Async
{
    public interface IExtendedAsyncCommand:IAsyncCommand, INotifyPropertyChanged
    {
        /// <summary>
        /// Execution state of the command. Can be null.
        /// </summary>
        NotifyTaskCompletion Execution { get; }
    }
}
