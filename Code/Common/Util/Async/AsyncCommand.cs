﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Promess.Common.Util.Async
{
    /// <summary>
    /// Async version of Command by Stephen Cleary.
    /// Provides async command functionality by abstracting the operation through a <see cref="NotifyTaskCompletion"/>.
    /// </summary>
    public class AsyncCommand : AsyncCommandBase, IExtendedAsyncCommand
    {
        private readonly Func<CancellationToken, Task> _command;
        private readonly CancelAsyncCommand _cancelCommand;
        private NotifyTaskCompletion _execution;
        private Func<bool> _canExecute;

        public AsyncCommand(Func<Task> command, Func<bool> canExecute=null) : this(async _ => await command(), canExecute) { }

        public AsyncCommand(Func<CancellationToken, Task> command, Func<bool> canExecute=null)
        {
            _command = command;
            _canExecute = canExecute ?? (() => true);
            _cancelCommand = new CancelAsyncCommand();
            _execution = new NotifyTaskCompletion(Task.CompletedTask);
            
        }

        public override bool CanExecute(object parameter)
        {
            return Execution.IsCompleted && _canExecute();
        }

        public override async Task ExecuteAsync(object parameter)
        {
            _cancelCommand.NotifyCommandStarting();
            Execution = new NotifyTaskCompletion(_command(_cancelCommand.Token));
            RaiseCanExecuteChanged();
            await Execution.TaskCompletion;
            _cancelCommand.NotifyCommandFinished();
            RaiseCanExecuteChanged();
        }

        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
        }

        public NotifyTaskCompletion Execution
        {
            get { return _execution; }
            private set
            {
                _execution = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}