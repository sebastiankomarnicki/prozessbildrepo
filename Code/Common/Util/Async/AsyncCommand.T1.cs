﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Promess.Common.Util.Async
{
    /// <summary>
    /// Async version of Command by Stephen Cleary.
    /// Provides async command functionality by abstracting the operation through a <see cref="NotifyTaskCompletion"/>.
    /// </summary>
    public class AsyncCommand<TParam> : AsyncCommandBase, IExtendedAsyncCommand
    {
        private readonly Func<TParam, CancellationToken, Task> _command;
        private readonly CancelAsyncCommand _cancelCommand;
        private NotifyTaskCompletion _execution;
        private Func<TParam, bool> _canExecute;

        public AsyncCommand(Func<TParam, CancellationToken, Task> command, Func<TParam, bool> canExecute = null)
        {
            _command = command;
            _canExecute = canExecute ?? (_ => true);
            _cancelCommand = new CancelAsyncCommand();
            _execution = new NotifyTaskCompletion(Task.CompletedTask);
        }

        public override bool CanExecute(object parameter)
        {
            return CanExecute((TParam)parameter);
        }

        public bool CanExecute(TParam parameter)
        {
            return Execution.IsCompleted && _canExecute(parameter);
        }

        public override async Task ExecuteAsync(object parameter)
        {
            await ExecuteAsync((TParam)parameter);
        }

        public async Task ExecuteAsync(TParam parameter)
        {
            _cancelCommand.NotifyCommandStarting();
            Execution = new NotifyTaskCompletion(_command(parameter, _cancelCommand.Token));
            RaiseCanExecuteChanged();
            await Execution.TaskCompletion;
            _cancelCommand.NotifyCommandFinished();
            RaiseCanExecuteChanged();
        }

        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
        }

        public NotifyTaskCompletion Execution
        {
            get { return _execution; }
            private set
            {
                _execution = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
