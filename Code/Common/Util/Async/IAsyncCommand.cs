﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace Promess.Common.Util.Async
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
    }
}