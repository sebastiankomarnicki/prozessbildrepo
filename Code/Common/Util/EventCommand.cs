﻿using System.Windows;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;

namespace Promess.Common.Util
{
    /// <summary>
    /// original source: https://social.msdn.microsoft.com/Forums/security/en-US/5cd586e7-640f-447b-9040-e9270173abf7/passing-drop-event-data-in-a-command-parameter-using-mvvm-and-the-interactivity-framework
    /// Command for also getting the args for events when using interactivity
    /// </summary>
    public sealed class EventCommand : TriggerAction<DependencyObject>
    {
        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(EventCommand), null);

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(EventCommand), null);

        public static readonly DependencyProperty InvokeParameterProperty =
            DependencyProperty.Register("InvokeParameter", typeof(object), typeof(EventCommand), null);

        private string _commandName;

        public object InvokeParameter
        {
            get
            {
                return this.GetValue(InvokeParameterProperty);
            }
            set
            {
                this.SetValue(InvokeParameterProperty, value);
            }
        }

        public ICommand Command
        {
            get
            {
                return (ICommand) this.GetValue(CommandProperty);
            }
            set
            {
                this.SetValue(CommandProperty, value);
            }
        }

        public object CommandParameter
        {
            get
            {
                return this.GetValue(CommandParameterProperty);
            }
            set
            {
                this.SetValue(CommandParameterProperty, value);
            }
        }

        public object Sender { get; set; }

        public string CommandName
        {
            get
            {
                return this._commandName;
            }

            set
            {
                if (this.CommandName != value)
                {
                    this._commandName = value;
                }
            }
        }

        protected override void Invoke(object parameter)
        {
            this.InvokeParameter = parameter;
            if (this.AssociatedObject!=null)
            {
                ICommand command = this.Command;
                if ((command != null) && command.CanExecute(this.CommandParameter))
                    command.Execute(this.CommandParameter);
            }
        }
    }
}
