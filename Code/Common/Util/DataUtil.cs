﻿using System.ComponentModel.DataAnnotations;

namespace Promess.Common.Util
{
    public static class DataUtil
    {
        /// <summary>
        /// Helper to retrieve the value of a property from a context
        /// </summary>
        /// <param name="theContext">Context, may not be <c>null</c></param>
        /// <param name="propertyName">Name of the property</param>
        /// <returns>Value of the property or <c>null</c> if not found.</returns>
        public static object GetOtherValue(ValidationContext theContext, string propertyName)
        {
            var propertyInfo = theContext.ObjectType.GetProperty(propertyName);
            if (propertyInfo != null)
            {
                return propertyInfo.GetValue(theContext.ObjectInstance, null);
            }
            return null;
        }
    }
}
