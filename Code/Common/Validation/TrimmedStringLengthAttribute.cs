﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.Validation
{
    /// <summary>
    /// Extension of <see cref="StringLengthAttribute"/> which trims the value before validation
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public sealed class TrimmedStringLengthAttribute : StringLengthAttribute
    {
        public TrimmedStringLengthAttribute(int maximumLength) : base(maximumLength)
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return base.IsValid(value);
            else
                return base.IsValid(((string)value).Trim());
        }
    }
}
