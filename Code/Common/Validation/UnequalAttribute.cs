﻿using Promess.Language;
using Promess.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.Validation
{
    /// <summary>
    /// For comparing annotated with another property. Only valid if values are not equal. Can provide a translation lookup keyto use in case of failed validation.
    /// </summary>
    public class UnequalAttribute:ValidationAttribute
    {
        public UnequalAttribute(string otherPropertyName)
        {
            OtherPropertyName = otherPropertyName;
        }

        public string OtherPropertyName { get; set; }

        public string ErrorMessageKey { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //both values null must be handled by using the required attribute, so success can be returned if one value is null
            if (value == null)
                return ValidationResult.Success;
            var otherValue = DataUtil.GetOtherValue(validationContext, OtherPropertyName);
            if (value.Equals(otherValue))
            {
                var text = (String)LanguageManager.Current.CurrentLanguageData.Translate(ErrorMessageKey?? "UnequalAttributeIsEqualGeneric");
                return new ValidationResult(text, new List<String>() { validationContext.MemberName });
            }
            return ValidationResult.Success;
        }
    }
}
