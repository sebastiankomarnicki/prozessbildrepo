﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.Validation
{
    /// <summary>
    /// Validation for all validations of an object
    /// </summary>
    class ValidateObjectAttribute:ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            var context = new ValidationContext(value, null, null);

            Validator.TryValidateObject(value, context, results, true);
            if (results.Count > 0)
            {
                var compositeResults = new CompositeValidationResult(String.Format("Validation for {0} failed.", validationContext.DisplayName));
                results.ForEach(compositeResults.AddResult);
                return compositeResults;
            }
            return ValidationResult.Success;
        }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Type name does not correspond to file name")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Type can be moved to separate file")]
    internal class CompositeValidationResult:ValidationResult
    {
        private readonly List<ValidationResult> _results = new List<ValidationResult>();

        public IEnumerable<ValidationResult> Results
        {
            get { return _results; }
        }

        public CompositeValidationResult(string errorMessage) : base(errorMessage) { }
        public CompositeValidationResult(string errorMessage, IEnumerable<string> memberNames)
            : base(errorMessage, memberNames){ }

        protected CompositeValidationResult(ValidationResult validationResult)
            : base(validationResult){ }

        internal void AddResult(ValidationResult validationResult)
        {
            _results.Add(validationResult);
        }
    }
}
