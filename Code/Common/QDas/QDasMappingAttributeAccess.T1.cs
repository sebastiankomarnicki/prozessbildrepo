﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Helper class for accessing <see cref="QdasMappingAttribute"/> attributed properties of a class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QDasMappingAttributeAccess<T>
    {
        private Dictionary<short, Tuple<string, QdasMappingAttribute>> _conversionTable;

        public QDasMappingAttributeAccess()
        {
            _conversionTable = new Dictionary<short, Tuple<string, QdasMappingAttribute>>();
            var properties = typeof(T).GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                var attributes = pi.GetCustomAttributes(false);
                var qdasMappings = attributes.Where(attr => attr.GetType() == typeof(QdasMappingAttribute));
                if (qdasMappings != null)
                {
                    foreach (var qdasMapping in qdasMappings)
                    {
                        QdasMappingAttribute qma = qdasMapping as QdasMappingAttribute;
                        _conversionTable.Add(qma.KNumber, new Tuple<string, QdasMappingAttribute>(pi.Name, qma));
                    }
                }
            }
        }

        /// <summary>
        /// Tries to get the property name for a k-number
        /// </summary>
        /// <param name="kNumber">Target k-number</param>
        /// <param name="propertyName">Out parameter with the name of property on success</param>
        /// <returns>Property with k-number was found</returns>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            Tuple<string, QdasMappingAttribute> fieldMetaData;
            if (!_conversionTable.TryGetValue(kNumber, out fieldMetaData))
            {
                propertyName = String.Empty;
                return false;
            }
            else
            {
                propertyName = fieldMetaData.Item1;
                return true;
            }
        }

        /// <summary>
        /// Tries to get the <see cref="QdasMappingAttribute"/> for a given k-number
        /// </summary>
        /// <param name="kNumber">Target k-number</param>
        /// <param name="mapping">Out parameter with the attribute of the k-number</param>
        /// <returns>Attribute for k-number was found</returns>
        public bool TryGetQdasMappingAttribute(short kNumber, out QdasMappingAttribute mapping)
        {
            Tuple<string, QdasMappingAttribute> fieldMetaData = null;
            mapping = null;
            if (!_conversionTable.TryGetValue(kNumber, out fieldMetaData))
                return false;
            mapping = fieldMetaData.Item2;
            return true;
        }

        /// <summary>
        /// Try set data for k-number on some instance and automatically try applying conversion if data types don't match
        /// </summary>
        /// <param name="instance">Target for the data</param>
        /// <param name="knumber">K-number for which data is to be set</param>
        /// <param name="value">Value to set</param>
        /// <returns>Success of operation</returns>
        public bool TrySetField(T instance,short knumber, object value)
        {
            Tuple<string, QdasMappingAttribute> fieldMetaData = null;
            if (!_conversionTable.TryGetValue(knumber, out fieldMetaData))
                return false;
            string fieldName = fieldMetaData.Item1;
            QdasMappingAttribute mappingInfo = fieldMetaData.Item2;
            object conversionResult;
            try
            {
                conversionResult = QdasValueConversions.Convert(value, mappingInfo.FieldType, mappingInfo.SpecialConversion);
            }
            catch
            {
                Debug.Assert(false, $"Got data which was incorrect format for mapping.\nKNumber: {knumber}\tType: {mappingInfo.FieldType}\tData: {value}");
                return false;
            }
            PropertyInfo pi = instance.GetType().GetProperty(fieldName, BindingFlags.Public | BindingFlags.Instance);
            var underlyingType = Nullable.GetUnderlyingType(pi.PropertyType);
            if (underlyingType !=null && conversionResult!=null){
                conversionResult = Convert.ChangeType(conversionResult, underlyingType);
            }    

            pi.SetValue(instance, conversionResult);
            return true;
        }

        /// <summary>
        /// Get value of a field identified by a k-number
        /// </summary>
        /// <param name="instance">Data source</param>
        /// <param name="knumber">Target k-number</param>
        /// <param name="value">Out parameter, retrieved vaule</param>
        /// <returns>Success of retrieval</returns>
        public bool TryGetField(T instance,short knumber, out object value)
        {
            Tuple<string, QdasMappingAttribute> fieldMetaData = null;
            if (!_conversionTable.TryGetValue(knumber, out fieldMetaData))
            {
                value = null;
                return false;
            }
            string fieldName = fieldMetaData.Item1;
            PropertyInfo pi = instance.GetType().GetProperty(fieldName, BindingFlags.Public | BindingFlags.Instance);
            value = pi.GetValue(instance);
            return true;
        }

        /// <summary>
        /// Check if a k-number is defined for the type
        /// </summary>
        /// <param name="KNumber">k-number to check</param>
        /// <returns>k-number is mapped</returns>
        public bool DefinesMappableKField(short KNumber)
        {
            return _conversionTable.ContainsKey(KNumber);
        }

        /// <summary>
        /// Get all mapped k-numbers for the type
        /// </summary>
        /// <returns></returns>
        public ISet<short> GetDefinedKNumbers()
        {
            var keys = new HashSet<short>(_conversionTable.Keys);
            return keys;
        }
    }
}
