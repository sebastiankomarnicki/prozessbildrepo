﻿using System;
using System.Collections.Generic;
using Promess.Common.Util;

namespace Promess.Common.QDas.Settings
{
    /// <summary>
    /// Settings to define custom order and k-numbers for a value line of measurement data.
    /// See EntwicklungsAuftrag1154 for the measurement program.
    /// </summary>
    public class QdasValueLineSettings
    {
        public const string SECTION_NAME = "QDAS";
        public const string ENTRY_NAME = "DataLayout";

        /// <summary>
        /// Default value line k-numbers according to aqdef
        /// </summary>
        private static List<short> _defaultImport = new List<short> { 1,2,4,5,6,7,8,10,11,12};

        /// <summary>
        /// Values read by <see cref="GetFromFile(string)"/>
        /// </summary>
        public List<short> ValueLineKNumbers { get; set; }

        /// <summary>
        /// Factory method for retrieving the settings from an ini file.
        /// Section is <see cref="SECTION_NAME"/> with entry <see cref="ENTRY_NAME"/> with comma separated measuring data k-numbers.
        /// </summary>
        /// <param name="iniFile"></param>
        /// <returns>Settings initialized from file</returns>
        public static QdasValueLineSettings GetFromFile(string iniFile)
        {
            var qs = new QdasValueLineSettings();
            var iniReader = new IniReader(iniFile);
            string line = iniReader.ReadValue(SECTION_NAME, ENTRY_NAME);
            if (String.IsNullOrWhiteSpace(line))
            {
                qs.ValueLineKNumbers = new List<short>();
                return qs;
            }
                
            line = line.Trim();
            string[] splitLine = line.Split(',');
            HashSet<short> foundKNumbers = new HashSet<short>();
            List<short> valueLineKNumbers = new List<short>();
            String tmp;
            foreach(String linePart in splitLine)
            {
                tmp = linePart.Trim();
                if (!tmp.StartsWith("K",StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new FormatException(tmp);
                }
                tmp = tmp.Substring(1);
                short kNumber = 0;
                if (!short.TryParse(tmp, out kNumber))
                {
                    throw new FormatException(tmp);
                }

                if (kNumber < 1 || kNumber >= 1000 || foundKNumbers.Contains(kNumber))
                    throw new ArgumentException(kNumber.ToString());

                valueLineKNumbers.Add(kNumber);
                foundKNumbers.Add(kNumber);
            }
            if (!foundKNumbers.Contains(1) || !foundKNumbers.Contains(4))
                throw new ArgumentException(line);
            qs.ValueLineKNumbers = valueLineKNumbers;
            return qs;
        }
    }
}
