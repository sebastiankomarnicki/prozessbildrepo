﻿using System;
using System.Collections.Generic;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Container for a measurement line and additional data. Complete data is referred to as a line.
    /// See different measurement line formats in AQDEF.
    /// </summary>
    public class DfxLine
    {
        /// <summary>
        /// Create container with the value line, if any, and additional line data, if any.
        /// Either value line should not be <c>String.Empty</c> or additional data contain entries.
        /// </summary>
        /// <param name="measurementLine">Value line</param>
        /// <param name="lineEntries">Additional line data consisting of a list of (k-number, complete entry). In a dfx file this is something like 'K0053/1 Order1234'.</param>
        public DfxLine(string measurementLine, IReadOnlyList<Tuple<short, string>> lineEntries)
        {
            this.MeasurementLine = measurementLine;
            this.LineEntries = lineEntries;
        }

        public String MeasurementLine { get; private set; }
        public IReadOnlyList<Tuple<short, string>> LineEntries { get; private set; }
    }
}
