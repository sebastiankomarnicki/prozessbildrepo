﻿using Promess.Common.QDas.Exceptions;
using Promess.Common.QDas.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Code for supporting AQDEF catalogs, like the gage catalog.
    /// Supported catalogs consist of a main catalog, subcatalogs, catalog entries and references of subcatalogs to catalog entries.
    /// By definition the main catalog contains all catalog entries
    /// </summary>
    /// <typeparam name="T">Type of the catalog entries of this catalog</typeparam>
    public class MainCatalog<T>:IMainCatalog where T: class, ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private short _mainCatalogKNumber;
        private short _eventToSubcatalogKNumber;
        private int _catalogNameLength;
        private string _name;
        private Dictionary<int, string> _subCatalogRefToName;
        private Dictionary<int, HashSet<int>> _subCatalogRefToEntryRefs;
        private Dictionary<int, T> _entryRefToCatalogEntry;
        private ISet<short> _definedKNumbers;

        public MainCatalog(short mainCatalogKNumber, int catalogNameLength, short eventToSubcatalogKNumber)
        {
            this._name = "";
            this._mainCatalogKNumber = mainCatalogKNumber;
            this._catalogNameLength = catalogNameLength;
            this._eventToSubcatalogKNumber = eventToSubcatalogKNumber;
            this._subCatalogRefToName = new Dictionary<int, string>();
            this._subCatalogRefToEntryRefs = new Dictionary<int, HashSet<int>>();
            this._entryRefToCatalogEntry = new Dictionary<int, T>();
        }

        /// <inheritdoc/>
        public short MainCatalogKNumber
        {
            get { return _mainCatalogKNumber; }
        }
        /// <inheritdoc/>
        public string MainCatalogName
        {
            get { return _name; }
        }

        /// <inheritdoc/>
        public IEnumerable<Tuple<int, ISupportKNumberDataManipulation>> GetCatalogEntriesAndQdasKeys()
        {
            foreach (var entry in _entryRefToCatalogEntry)
            {
                yield return new Tuple<int, ISupportKNumberDataManipulation>(entry.Key, entry.Value);
            }
        }

        /// <inheritdoc/>
        public bool TryGetCatalogEntriesAndQdasKeysForKey(int key, out T value)
        {
            return _entryRefToCatalogEntry.TryGetValue(key, out value);
        }

        /// <inheritdoc/>
        public IEnumerable<int> GetQdasKeysInSubcatalog(int subcatalogKey)
        {
            return _subCatalogRefToEntryRefs[subcatalogKey];
        }

        /// <inheritdoc/>
        public IEnumerable<Tuple<int,string>> GetSubcatalogsAndQdasKeys()
        {
            foreach(var entry in _subCatalogRefToName)
            {
                yield return new Tuple<int, string>(entry.Key, entry.Value);
            }
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbersOfEntries()
        {
            return KNumberQdasUtil.GetRequiredKNumbers<T>();
        }

        /// <inheritdoc/>
        public void AddData(short kNumber, int reference, string data)
        {
            if (kNumber == _mainCatalogKNumber)
            {
                if (reference < 0)
                    throw new InvalidCatalogQdasLineException(kNumber, reference, data);

                string limited;
                if (data.Length> _catalogNameLength)
                {
                    limited = data.Substring(0, _catalogNameLength);
                }else
                {
                    limited = data;
                }
                if (reference == 0)
                {
                    _name = limited;
                }else
                {
                    _subCatalogRefToName[reference] = limited;
                }

            }else if (kNumber == _eventToSubcatalogKNumber)
            {
                if (reference <= 0)
                    throw new InvalidCatalogQdasLineException(kNumber, reference, data);

                int entryRef = int.Parse(data, CultureInfo.InvariantCulture);

                HashSet<int> entries = null;
                if (_subCatalogRefToEntryRefs.TryGetValue(reference, out entries))
                {
                    entries.Add(entryRef);
                }else
                {
                    _subCatalogRefToEntryRefs[reference] = new HashSet<int>() { entryRef };
                }
            }else
            {
                if (reference <= 0)
                    throw new InvalidCatalogQdasLineException(kNumber, reference, data);

                T entry = null;
                if (_entryRefToCatalogEntry.TryGetValue(reference, out entry))
                {
                    entry.TrySetField(kNumber, data);
                }else
                {
                    entry = Activator.CreateInstance<T>();
                    entry.TrySetField(kNumber, data);
                    _entryRefToCatalogEntry[reference] = entry;
                }
            }
        }

        /// <inheritdoc/>
        public bool IsConsistent()
        {
            var keysOfAllEntries = new HashSet<int>(_entryRefToCatalogEntry.Keys);
            var referencesSubcatalogs = new HashSet<int>(_subCatalogRefToName.Keys);
            var subcatalogsWithReferences = new HashSet<int>(_subCatalogRefToEntryRefs.Keys);

            if (!subcatalogsWithReferences.IsSubsetOf(referencesSubcatalogs))
                return false;

            foreach (var subcatEntries in _subCatalogRefToEntryRefs)
            {
                if (!subcatEntries.Value.IsSubsetOf(keysOfAllEntries))
                    return false;
            }
            return true;
        }

        /// <inheritdoc/>
        public bool ContainsData()
        {
            return _subCatalogRefToEntryRefs.Count > 0 || _subCatalogRefToName.Count > 0;
        }

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            if (this._definedKNumbers == null)
            {
                T instance = Activator.CreateInstance<T>();
                ISet<short> definedKNumbers = instance.GetDefinedKNumbers();
                definedKNumbers.Add(_mainCatalogKNumber);
                definedKNumbers.Add(_eventToSubcatalogKNumber);
                this._definedKNumbers = definedKNumbers;
            }

            return this._definedKNumbers;
        }

        /// <inheritdoc/>
        public Tuple<ICollection<int>, ICollection<int>> RemoveSubcatalogsAndEntriesWORequiredValues()
        {
            List<int> removedSubcatalogReferences = new List<int>();
            List<int> removedEntryReferences = new List<int>();
            foreach (var subcatEntry in _subCatalogRefToName)
            {
                if (String.IsNullOrEmpty(subcatEntry.Value))
                    removedSubcatalogReferences.Add(subcatEntry.Key);
            }
            foreach (var key in removedSubcatalogReferences)
            {
                _subCatalogRefToName.Remove(key);
            }
            ICollection<short> requiredKNumbers = GetRequiredKNumbersOfEntries();
            foreach (var eventEntry in _entryRefToCatalogEntry)
            {
                object value=null;
                String convertedValue;
                foreach (var requiredKNumber in requiredKNumbers)
                {
                    if (!eventEntry.Value.TryGetField(requiredKNumber, out value)){
                        throw new NotImplementedException($"associated knumber not found {requiredKNumber} {typeof(T)} .\n please contact promess.");
                    }else
                    {
                        Debug.Assert(value == null || value.GetType().Equals(typeof(String)),
                            "the cast is supposed to fail if there ever is a catalog entry with a non string required field"
                            );
                        convertedValue = (String)value;
                        if (String.IsNullOrEmpty(convertedValue))
                        {
                            removedEntryReferences.Add(eventEntry.Key);
                            break;
                        }
                    }
                }
            }
            foreach (var key in removedEntryReferences)
            {
                _entryRefToCatalogEntry.Remove(key);
            }
            return new Tuple<ICollection<int>, ICollection<int>>(removedSubcatalogReferences, removedEntryReferences); 
        }
    }
}
