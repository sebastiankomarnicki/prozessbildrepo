﻿using System;
using System.Windows.Markup;
using System.Windows.Data;
using System.ComponentModel;
using Promess.Common.Converters;

namespace Promess.Common.Extensions
{
    /// <summary>
    /// <see cref="MarkupExtension"/> for retrieving the language entry for a given key in the current language 
    /// </summary>
    public class LocalisedTextExtension : MarkupExtension
	{
		private Binding _lookupBinding;

		public LocalisedTextExtension()
		{
			_lookupBinding = UITextLookupConverter.CreateBinding("");
		}

        public LocalisedTextExtension(String key):this()
        {
            Key = key;
        }

        /// <summary>
        /// Key for the language lookup
        /// </summary>
		[ConstructorArgument("key"), DefaultValue("")]
        public string Key
        {
            get { return (string)_lookupBinding.ConverterParameter; }
            set { _lookupBinding.ConverterParameter = value; }
        }

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return _lookupBinding.ProvideValue(serviceProvider);
		}


	}
}
