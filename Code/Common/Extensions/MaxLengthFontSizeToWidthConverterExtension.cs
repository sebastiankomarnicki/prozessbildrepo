﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;

namespace Promess.Common.Extensions
{
    /// <summary>
    /// Approximate the maximum size for a text element by using the maximal text size
    /// </summary>
    public class MaxLengthFontSizeToWidthConverterExtension : MarkupExtension, IMultiValueConverter
    {
        private static MaxLengthFontSizeToWidthConverterExtension _converter;
        private static double _singleCharacterSpacing;
        //maybe use some dictionary here if the language uses non arabic letters?
        private static readonly char _sizeChar = 'X';
        private int _offset;

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            if (_converter == null)
            {
                _converter = new MaxLengthFontSizeToWidthConverterExtension() { _offset = 10 };
                RecalculateMaxLength();
                //TODO set listener to current language or TextElement.FontFamilyProperty.DefaultMetadata.DefaultValue ?
            }
            return _converter;
        }

        /// <summary>
        /// Convert input to maximal size
        /// </summary>
        /// <param name="values">Two elements, first being the maximum number of characters and second the font size</param>
        /// <param name="targetType">ignored</param>
        /// <param name="parameter">ignored</param>
        /// <param name="culture">ignored</param>
        /// <returns></returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length < 2)
            {
                return Binding.DoNothing;
            }
            int? maxLength = (int?)values[0];
            double? fontSize = (double?)values[1];
            if (!maxLength.HasValue || maxLength.Value < 1 || !fontSize.HasValue || fontSize.Value<=0)
                return Binding.DoNothing;
            return _singleCharacterSpacing * fontSize.Value * maxLength.Value + _offset;
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private static void RecalculateMaxLength()
        {
            Typeface typeface = new Typeface((FontFamily)TextElement.FontFamilyProperty.DefaultMetadata.DefaultValue,
                FontStyles.Italic,
                FontWeights.Normal,
                FontStretches.Normal
                );
            //FormattedText ft = new FormattedText("X",
            //    CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
            //    typeface, 11, Brushes.Black);
            //_singleCharacterSpacing = ft.Width;
            //note: alternate approach would be to create a FormattedText instance, feed it some string and retrieve the width


            if (!typeface.TryGetGlyphTypeface(out GlyphTypeface glyphTypeFace))
            {
                throw new InvalidOperationException("Internal error, no glyphtypeface found.");
                //would need to use the constructor, which also requires the device indepentent pixels value. where to retrieve?
                //var ft = new FormattedText("x",
                //    CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                //    typeface, 1, Brushes.Black);
                //_singleCharacterSpacing = ft.Width;
            }
            else
            {
                _singleCharacterSpacing = glyphTypeFace.AdvanceWidths[glyphTypeFace.CharacterToGlyphMap[_sizeChar]];
            }
        }

    }
}
