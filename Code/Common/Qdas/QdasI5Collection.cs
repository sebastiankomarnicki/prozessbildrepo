﻿using System.Collections.Generic;

namespace Promess.Common.QDas
{
    static class QdasI5Collection
    {
        /// <summary>
        /// Parses an AQDEF I5 collection line into a list of <c>short</c>
        /// </summary>
        /// <param name="input">Line to parse</param>
        /// <returns>List of the parsed numbers</returns>
        public static IList<short> Parse(string input)
        {
            List<short> result = new List<short>();
            foreach(string entry in input.Split(','))
            {
                result.Add(short.Parse(entry));
            }
            return result;
        }
    }
}
