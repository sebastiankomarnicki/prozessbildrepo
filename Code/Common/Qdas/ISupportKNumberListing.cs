﻿using System.Collections.Generic;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Interface intended for getting the defined k-numbers
    /// </summary>
    public interface ISupportKNumberListing
    {
        /// <summary>
        /// Get the k-numbers defined on this type
        /// </summary>
        /// <returns>Set of defined k-numbers</returns>
        ISet<short> GetDefinedKNumbers();
    }
}
