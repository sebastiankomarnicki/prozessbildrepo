﻿using System;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using Promess.Common.QDas.Settings;

namespace Promess.Common.QDas
{
    //FIXME mbe: some enum fields are still numbers instead of enums, cause they were defined in vb6 like that
    /// <summary>
    /// vb6 class of the DatenManager as base for reading AQDEF files
    /// </summary>
    public static class QdasLesen
    {
        public static readonly char CharacteristicSeparator = Convert.ToChar(15);
        public static readonly char AdditionalDataSeparator = Convert.ToChar(20);

        /// <summary>
        /// Reads from the given datastream
        /// </summary>
        /// <param name="dataStream">stream containing the data in qdas dfd format</param>
        /// <param name="stTeiledaten">out parameter</param>
        /// <param name="astMerkmalsdaten"></param>
        /// <returns></returns>
        public static (bool valid, QdasDfdReadResult parsed) QdasDfdDateiLesen(Stream dataStream, Encoding defaultEncoding)
        {
            using (var sr = new StreamReader(new BufferedStream(dataStream), defaultEncoding, true))
            {
                var data = sr.ReadToEnd();
                return ParseQdasDfdData(data);
            }
        }

        /// <summary>
        /// Parse assumed AQDEF dfd (header) data into container
        /// </summary>
        /// <param name="input">dfd data</param>
        /// <returns>Value tuple of parse success and if so a valid parse result</returns>
        public static (bool valid, QdasDfdReadResult parsed) ParseQdasDfdData(String input)
        {
            var unrecognizedLines = new List<string>();
            var recognizedLines = new SortedDictionary<short, string>();
            var partSplitResults = new Dictionary<short, QdasLineSplitResult>();
            var characteristicSplitResults = new Dictionary<short, List<QdasLineSplitResult>>();
            var stTeiledaten = new TS_TEILEDATEN();
            TS_MERKMALSDATEN[] astMerkmalsdaten;
            iErrCodes iErrCode = iErrCodes.iecno_error;
            
            using (var sr = new StringReader(input))
            {
                QdasLineSplitResult result;
                string readText;
                while ((readText = sr.ReadLine())!=null)
                {
                    result = QdasZeile_Split(readText);

                    if (result.Key.Equals(String.Empty) || !result.HasData)
                        continue;
                    if (result.KeyValue != QdasLineSplitResult.InvalidKeyValue)
                    {
                        if (recognizedLines.TryGetValue(result.KeyValue, out string alreadySaved))
                        {
                            recognizedLines[result.KeyValue] = $"{alreadySaved}\r\n{readText}";
                            if (MightBeCharacteristicsValue(result.KeyValue))
                            {
                                //for now keep all values, process FIFO to overwrite values
                                characteristicSplitResults[result.KeyValue].Add(result);
                            }
                            else
                            {
                                //only last value valid
                                partSplitResults[result.KeyValue] = result;
                            }
                        }
                        else
                        {
                            recognizedLines[result.KeyValue] = readText;
                            if (MightBeCharacteristicsValue(result.KeyValue))
                            {
                                characteristicSplitResults[result.KeyValue] = new List<QdasLineSplitResult>() { result };
                            }
                            else
                            {
                                partSplitResults[result.KeyValue] = result;
                            }
                        }
                    }
                    else
                    {
                        unrecognizedLines.Add(readText);
                    }
                }
            }
            if (!partSplitResults.TryGetValue(100, out QdasLineSplitResult headerResult))
            {
                return (false, null);
            }

            astMerkmalsdaten = SetupCharacteristics(headerResult.Remainder[0]);
            if (astMerkmalsdaten.Length == 0)
                return (false, null);
            foreach (var entry in partSplitResults.Values)
            {
                ProcessAsDFDOtherData(entry, stTeiledaten);
            }
            foreach (var entryList in characteristicSplitResults.Values)
            {
                foreach (var entry in entryList)
                {
                    ProcessAsDFDCharacteristicsData(entry, astMerkmalsdaten);
                }
            }

            return (true, new QdasDfdReadResult(stTeiledaten, astMerkmalsdaten, iErrCode, recognizedLines, unrecognizedLines));
        }

        private static void ProcessAsDFDOtherData(QdasLineSplitResult result, TS_TEILEDATEN stTeiledaten)
        {
            iErrCodes iErrCode;
            bool autoSetSuccessful = stTeiledaten.TrySetField(result.KeyValue, result.Remainder[0].TrimStart());
            if (autoSetSuccessful)
                return;

            if (stTeiledaten.KNumberToMainCatalogLookup.TryGetValue(result.KeyValue, out IMainCatalog catalog))
            {
                catalog.AddData(result.KeyValue, result.KeyIndex, result.Remainder[0]);
                return;
            }

            if (stTeiledaten.ProcessParameter.DefinedKNumbers.Contains(result.KeyValue))
            {
                stTeiledaten.ProcessParameter.AddData(result.KeyValue, result.KeyIndex, result.Remainder[0]);
                return;
            }

            if (stTeiledaten.K4220Catalog.DefinedKNumbers.Contains(result.KeyValue))
            {
                stTeiledaten.K4220Catalog.AddData(result.KeyValue, result.KeyIndex, result.Remainder[0]);
                return;
            }
            stTeiledaten.AddUnrecognizedLine(result);
        }

        private static void ProcessAsDFDCharacteristicsData(QdasLineSplitResult result, TS_MERKMALSDATEN[] astMerkmalsdaten)
        {
            Debug.Assert(astMerkmalsdaten.Length>0&&result.Remainder.Length>0);
            int iKeyValueIndex = 0;
            int lMerkmalMin;
            int lMerkmalMax;
            // Wenn Daten vorhanden
            switch (result.KeyIndex)
            {
                case -1:  // fuer alle definierten Merkmale 
                    lMerkmalMin = 0;   
                    lMerkmalMax = result.Remainder.Length -1;
                    break;

                case 0:  // fuer alle Merkmale 
                    lMerkmalMin = 0;
                    lMerkmalMax = astMerkmalsdaten.Length - 1;
                    break;

                default: // fuer bestimmtes Merkmal 
                    //qdas file uses entry number reference, data structure here indices => -1
                    lMerkmalMin = result.KeyIndex-1;
                    lMerkmalMax = result.KeyIndex-1;
                    break;
            }

            for (int lMerkmalAkt = lMerkmalMin; lMerkmalAkt <= lMerkmalMax; lMerkmalAkt++)
            {
                //TODO this could be optimized for case 0, not lMerkmalAkt 0, new variable
                astMerkmalsdaten[lMerkmalAkt].TrySetField(result.KeyValue, result.Remainder[iKeyValueIndex].TrimStart());
                //note: mbe: maybe save data that could not be set (invalid datatype)
                if (result.KeyIndex == -1)
                {
                    iKeyValueIndex++;
                }
            }
        }

        private static bool MightBeCharacteristicsValue(int keyValue)
        {
            return (keyValue >= 2000 && keyValue < 3000) || (keyValue>=8000 && keyValue<9000);
        }

        private static TS_MERKMALSDATEN[] SetupCharacteristics(string textNumberCharacteristics)
        {
            int numberCharacteristics = int.Parse(textNumberCharacteristics);
            TS_MERKMALSDATEN[] astMerkmalsdaten = new TS_MERKMALSDATEN[numberCharacteristics];
            // Verweisfelder vorbesetzen 
            for (int i = 0; i < astMerkmalsdaten.Length; i++)
            {
                //initialization moved to constructor
                astMerkmalsdaten[i] = new TS_MERKMALSDATEN();
            }
            return astMerkmalsdaten;
        }

        private static QdasLineSplitResult ReadHeader(TextReader sr)
        {
            string readText;
            QdasLineSplitResult parsedLine;
            while ((readText = sr.ReadLine())!=null)
            {
                parsedLine = QdasZeile_Split(readText);

                if (parsedLine.Key.Equals(String.Empty) || !parsedLine.HasData)
                    continue;
                else if (parsedLine.KeyValue == 100)
                {
                    return parsedLine;
                }
                throw new FileFormatException();
            }
            throw new FileFormatException();
        }

        #region QdasDFX_Datei_Lesen
        /// <summary>
        /// Parse dfx data stream with given characteristic definitions
        /// </summary>
        /// <param name="dataStream">Stream containing the data, at position where the dfx data begins</param>
        /// <param name="originalTemplateData">Characteristics header data for all characteristics</param>
        /// <param name="qdasSettings">Settings for value line definition</param>
        /// <param name="defaultEncoding">Fallback encoding when the stream is processed</param>
        /// <returns>Parse result</returns>
        public static DfxData QdasDFXDateiLesen(Stream dataStream, TS_MERKMALSDATEN[] originalTemplateData, QdasValueLineSettings qdasSettings, Encoding defaultEncoding)
        {
            using (var sr = new StreamReader(new BufferedStream(dataStream), defaultEncoding, true))
            {
                var data = sr.ReadToEnd();
                return ParseQdasDFXData(data, originalTemplateData, qdasSettings);
            }
        }

        /// <summary>
        /// Parse dfx text with given characteristic definitions
        /// </summary>
        /// <param name="input">dfx text</param>
        /// <param name="originalTemplateData">Characteristics header data for all characteristics</param>
        /// <param name="qdasSettings">Settings for value line definition</param>
        /// <returns>Parse result</returns>
        public static DfxData ParseQdasDFXData(string input, TS_MERKMALSDATEN[] originalTemplateData, QdasValueLineSettings qdasSettings)
        {
            List<DfxLine> dfxLines = new List<DfxLine>();
            string measurementLine="";
            List<Tuple<short, string>> lineEntries = new List<Tuple<short, string>>();
            TS_MERKMAL_IST[] astMerkmal_Ist = GetInitializedCharacteristicsArray(originalTemplateData);
            List<TS_MERKMAL_IST[]> data = new List<TS_MERKMAL_IST[]>();
            //note mbe: not sure if this will work, because k20+21 are the same as k20
            ISet<int> idKeyValues = new HashSet<int>(new int[] { 1, 20, 21 });
            int merkmalMin = 0;
            int merkmalMax = 0;
            int iKeyValueIndex;

            int? firstSeenCharacteristicsId = null;

            using (var sr = new StringReader(input))
            {
                TS_MERKMAL_IST continuousData = new TS_MERKMAL_IST();
                List<short> fields = qdasSettings.ValueLineKNumbers;
                if (fields == null || fields.Count == 0)
                {
                    fields = new List<short>() { 1, 2, 4, 5, 6, 7, 8, 10, 11, 12 };
                }
                HashSet<short> continuousKFields = new HashSet<short>() { 4, 6, 7, 8, 10, 12 };
                string readLine;
                while ((readLine=sr.ReadLine())!=null)
                {
                    var line = QdasZeile_Split(readLine);
                    
                    if (CheckShouldAppendToOldData(firstSeenCharacteristicsId, line, idKeyValues))
                    {
                        data.Add(astMerkmal_Ist);
                        dfxLines.Add(new DfxLine(measurementLine, lineEntries));
                        lineEntries = new List<Tuple<short, string>>();
                        astMerkmal_Ist = GetInitializedCharacteristicsArray(originalTemplateData);
                        firstSeenCharacteristicsId = null;
                    }
                    if (!line.HasData)
                    {
                        continue;
                    }
                    if (line.Key == string.Empty)
                    {
                        firstSeenCharacteristicsId = 1;
                        measurementLine = readLine;
                        for (int merkmalAkt = 0; merkmalAkt < line.Remainder.Length; merkmalAkt++)
                        {
                            //separating character is also at the end of the last entry
                            string[] currentCharacteristicsData = line.Remainder[merkmalAkt].Split(new char[] { (char)20 });
                            TS_MERKMAL_IST current = astMerkmal_Ist[merkmalAkt];
                            //read the value
                            Tuple<bool, int> isValidAndOffset = ReadValueDataValue(current, currentCharacteristicsData, fields.IndexOf(1));
                            //read additional data
                            if (!isValidAndOffset.Item1)
                            {
                                //TODO mbe: talk about continuing with michael reimann, this is the legacy behavior
                                continue;
                            }

                            current.iAttribut = 0;//set default attribute value for read data

                            foreach (short knumber in continuousKFields)
                            {
                                continuousData.TryGetField(knumber, out object value);
                                if (value != null)
                                {
                                    current.TrySetField(knumber, value);
                                }
                            }
                            short elemNo = 0;
                            for (int index = 0; index < currentCharacteristicsData.Length && elemNo < fields.Count; index++, elemNo++)
                            {
                                string currentElement = currentCharacteristicsData[index].TrimStart();
                                short currentKNumber = fields[elemNo];
                                if (currentElement.Equals(""))
                                    continue;
                                switch (currentKNumber)
                                {
                                    case 1:
                                        //this is also called for attributive characteristics, no k number 20/21
                                        index += isValidAndOffset.Item2 - 1;//-1 due to the auto increment
                                        continue;

                                    case 6:
                                        //batch number
                                        //the qdas specification actually requires this entry to begin with "#", was ignored for old dbimport -> now too
                                        if (currentElement.StartsWith("#"))
                                            currentElement = currentElement.Substring(1);
                                        if (current.TrySetField(6, currentElement))
                                            continuousData.TrySetField(6, currentElement);
                                        continue;

                                    default:
                                        if (current.TrySetField(currentKNumber, currentElement))
                                        {
                                            if (continuousKFields.Contains(currentKNumber))
                                            {
                                                continuousData.TrySetField(currentKNumber, currentElement);
                                            }
                                        }
                                        continue;
                                }
                            }
                        }
                        for (int merkmalAkt = line.Remainder.Length; merkmalAkt < astMerkmal_Ist.Length; merkmalAkt++)
                        {
                            //this is for setting the read default values for characteristics which were not read. most likely an error case.
                            TS_MERKMAL_IST current = astMerkmal_Ist[merkmalAkt];
                            foreach (short knumber in continuousKFields)
                            {
                                continuousData.TryGetField(knumber, out object value);
                                if (value != null)
                                    current.TrySetField(knumber, value);
                            }
                        }
                    }
                    else
                    {
                        lineEntries.Add(Tuple.Create(line.KeyValue, readLine));
                        // Wenn Daten vorhanden
                        switch (line.KeyIndex)
                        {
                            case -1:  // fuer alle definierten Merkmale 
                                merkmalMin = 0;
                                merkmalMax = line.Remainder.Length - 1;
                                break;

                            case 0:  // fuer alle Merkmale 
                                merkmalMin = 0;
                                merkmalMax = astMerkmal_Ist.Length - 1;
                                break;

                            default: // fuer bestimmtes Merkmal 
                                merkmalMin = line.KeyIndex - 1;
                                merkmalMax = line.KeyIndex - 1;
                                break;
                        }
                        iKeyValueIndex = 0;
                        if (!firstSeenCharacteristicsId.HasValue && idKeyValues.Contains(line.KeyValue))
                        {
                            if (line.KeyIndex == 0)
                                throw new ArgumentOutOfRangeException($"Key index zero is not allow for K {line.KeyValue}");
                            firstSeenCharacteristicsId = line.KeyIndex;
                        }

                        for (int merkmalAkt = merkmalMin; merkmalAkt <= merkmalMax; merkmalAkt++)
                        {
                            //TODO this could be optimized for case 0, only convert the data once
                            astMerkmal_Ist[merkmalAkt].TrySetField(line.KeyValue, line.Remainder[iKeyValueIndex].TrimStart());

                            if (line.KeyIndex == -1)
                            {
                                iKeyValueIndex++;
                            }
                        }
                    }
                }
                if (firstSeenCharacteristicsId.HasValue)
                {
                    data.Add(astMerkmal_Ist);
                    dfxLines.Add(new DfxLine(measurementLine, lineEntries));
                }                    
            }
            return new DfxData(data, dfxLines);
        }

        /// <summary>
        /// Read the value data from a measurement line for a given characteristic of type attributive of variable into its container
        /// </summary>
        /// <param name="characteristic">Target charactistic data</param>
        /// <param name="measurementLine">Segmented line</param>
        /// <param name="valueDataIndex">Offset for current characteristic</param>
        /// <returns>Tuple indicating if the characteristic type was recognized and offset for the value data</returns>
        private static Tuple<bool, int> ReadValueDataValue(TS_MERKMAL_IST characteristic, string[] measurementLine,int valueDataIndex)
        {
            //TODO better to use arraysegment?
            switch (characteristic.iMerkmal_Art)
            {
                case (short)iMerkmal_Art.imaVARIABEL:
                    if (measurementLine.Length < valueDataIndex+1)
                        throw new ArgumentOutOfRangeException("variable characteristic must at least contain one entry");
                    characteristic.TrySetField(1, measurementLine[valueDataIndex].TrimStart());
                    return new Tuple<bool, int>(true, 1);
                case (short)iMerkmal_Art.imaATTRIBUTIV:
                    if (measurementLine.Length < valueDataIndex + 3)
                        throw new ArgumentOutOfRangeException("attributive characteristic must at least contain three entries");
                    string sampleSize = measurementLine[valueDataIndex].TrimStart();
                    int addedOffset = sampleSize.LastIndexOf("000");
                    if (addedOffset >= 0)
                    {
                        //accept incorrect 000 (instead of four zeros) as 0
                        sampleSize = sampleSize.Substring(0, addedOffset);
                    }
                    //TODO not sure what to do on invalid format
                    characteristic.TrySetField(20, sampleSize);
                    characteristic.TrySetField(21, measurementLine[valueDataIndex + 1].TrimStart());
                    //there is an ignored field here, should always be zero->skipped
                    return new Tuple<bool, int>(true, 3);
                default:
                    return new Tuple<bool, int>(false, -1);
            }
        }

        private static bool CheckShouldAppendToOldData(int? firstSeenCharacteristicsId, QdasLineSplitResult line, ISet<int> idKeyValues)
        {
            return firstSeenCharacteristicsId.HasValue && (
                !line.HasData ||
                line.Key==string.Empty ||
                idKeyValues.Contains(line.KeyValue) && (firstSeenCharacteristicsId == line.KeyIndex||firstSeenCharacteristicsId==0)
                );
        }

        private static TS_MERKMAL_IST[] GetInitializedCharacteristicsArray(TS_MERKMALSDATEN[] originalTemplateData)
        {
            Debug.Assert(originalTemplateData.Length > 0);
            TS_MERKMAL_IST[] result = new TS_MERKMAL_IST[originalTemplateData.Length];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new TS_MERKMAL_IST
                {
                    //set attribute to not write, because an array is used as default and there cannot be any omitted elements
                    iAttribut = 256,
                    iMerkmal_Art = originalTemplateData[i].iMerkmal_Art
                };
            }
            return result;
        }
        #endregion

        public static QdasLineSplitResult QdasZeile_Split(string sZeile)
        {
            // -------------------------------------------------------------------
            // Erstellung  : xx.01.01 wd
            // Aenderung   : 19.02.04 wd a Fehlende Merkmalsnummer bei K Variablen
            // -------------------------------------------------------------------
            // Beschreibung:
            // Diese Funktion zerlegt eine QDAS Dateizeile in
            //
            // die Kennung ( Sonderfall: "" fuer keine Kennung ),
            //
            // die Merkmalsnummer ( Sonderfaelle: -1 fuer keine, 0 fuer alle Merkmale )
            //
            // und
            //
            // die Werte als Stringfeld.
            // -------------------------------------------------------------------
            // Rueckgabewert:
            // Fehlercode
            // -------------------------------------------------------------------
            var result = new QdasLineSplitResult() { Error = iErrCodes.iecno_error };

            int iCharPos = 0;

            sZeile = sZeile.TrimStart();
            //note mbe: Kxxxx is used as a key in a switch statement, switch cannot be used with comparer as far as I know
            if (sZeile.StartsWith("K", StringComparison.OrdinalIgnoreCase))
            { // Key definiert
                result.SetKey(sZeile.Substring(0, Math.Min(5, sZeile.Length)));
                sZeile = sZeile.Substring(5);

                if (sZeile.StartsWith("/"))
                { // Index angehaengt
                    sZeile = sZeile.Substring(1);
                    iCharPos = sZeile.IndexOf(' ');
                    if (iCharPos >= 1)
                    {
                        if (short.TryParse(sZeile.Substring(0, iCharPos), out short keyIndex))
                        {
                            result.KeyIndex = keyIndex;
                        }
                    }
                    else
                    {
                        // Merkmalsnummer fehlt !
                        //default value for missing key index is the missing value
                    }
                    sZeile = sZeile.Substring(iCharPos+1); // auf Daten positionieren
                }
                else
                {
                    sZeile = sZeile.Substring(Math.Min(1, sZeile.Length)); // 1 Leerzeichen ueberspringen
                }
            }
            else
            {
                result.SetKey("");
                //result.KeyIndex= missingOrNoKey;
            }
            //TODO mbe: talk with michael reimann about what is considered data
            if (sZeile.Length == 0)
            {
                result.Remainder = new string[0];
            }else
            {
                result.Remainder = sZeile.Split(QdasLesen.CharacteristicSeparator);
            }
            return result;
        }

        /// <summary>
        /// Splits dfq data into dfd data and dfx data
        /// </summary>
        /// <param name="dfqData">Text in dfq format</param>
        /// <returns>Pair of dfd and dfx data</returns>
        public static ValueTuple<string, string> SplitDfqData(string dfqData)
        {
            string lastLine;
            var dfdData = new StringBuilder();
            var dfxData = new StringBuilder();
            using (var dfqFs = new StringReader(dfqData))
            {
                lastLine = dfqFs.ReadLine();
                while (IsDfdLine(lastLine))
                {
                    dfdData.AppendLine(lastLine);
                    lastLine = dfqFs.ReadLine();
                }
                if (lastLine != null)
                {
                    dfxData.AppendLine(lastLine);
                    dfxData.Append(dfqFs.ReadToEnd());
                }
            }
            return (dfdData.ToString(), dfxData.ToString());
        }

        /// <summary>
        /// Check if the given line is a dfd line.
        /// Assumes that the line corresponds to AQDEF, meaning only empty lines (spaces tabs) or lines beginning with a K are interpreted.
        /// </summary>
        /// <param name="line">Line to check</param>
        /// <returns>Assumption about the line</returns>
        private static bool IsDfdLine(string line)
        {
            if (line == null)
                return false;
            string trimmedLine = line.TrimStart();
            if (trimmedLine.Length == 0)
                return true;
            if (trimmedLine.StartsWith("K00") || !trimmedLine.StartsWith("K"))
            {
                return false;
            }
            return true;
        }
    }
}