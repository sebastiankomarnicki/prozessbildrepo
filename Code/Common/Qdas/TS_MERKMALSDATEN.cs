﻿using System;
using System.Collections.Generic;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Container for the header data of a single characteristic
    /// </summary>
    public class TS_MERKMALSDATEN: ISupportKNumberDataManipulation
    {
        private static QDasMappingAttributeAccess<TS_MERKMALSDATEN> _kMappings;

        private readonly List<QdasLineSplitResult> _unrecognizedKeyData;

        static TS_MERKMALSDATEN()
        {
            _kMappings = new QDasMappingAttributeAccess<TS_MERKMALSDATEN>();
        }

        #region KNumbers
        [QdasMapping(2001, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sMerkmal_Nummer_Text { get; set; }
        [QdasMapping(2002, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string sMerkmal_Bezeichnung { get; set; }
        [QdasMapping(2003, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sMerkmal_Bezeichnung_kurz { get; set; }
        [QdasMapping(2004, QdasFieldType.I5)]
        public short? iMerkmal_Art { get; set; }
        [QdasMapping(2005, QdasFieldType.I5)]
        public short? iMerkmal_Klasse { get; set; }
        [QdasMapping(2006, QdasFieldType.I5)]
        public short? iDoku_Pflicht { get; set; }
        [QdasMapping(2007, QdasFieldType.I5)]
        public short? iRegelungsart { get; set; }
        [QdasMapping(2008, QdasFieldType.I5)]
        public short? GroupType { get; set; }
        [QdasMapping(2009, QdasFieldType.I5)]
        public short? MeasuredQuentity { get; set; }
        
        [QdasMapping(2011, QdasFieldType.I5)]
        public short? iVerteilungsart { get; set; }
        
        [QdasMapping(2013, QdasFieldType.F)]
        public double? dKlassenweite { get; set; }
        //K2014 entfallen note mbe: original is I2
        [Obsolete]
        [QdasMapping(2014, QdasFieldType.I3)]
        public short? iOrdinalklassen_Anzahl { get; set; }
        [QdasMapping(2015, QdasFieldType.I3)]
        public short? iArt_der_Abnutzung { get; set; }
        [QdasMapping(2016, QdasFieldType.I3)]
        public short? Measurement100Percent { get; set; }
        [QdasMapping(2017, QdasFieldType.I3)]
        public byte? AlarmDetectionType { get; set; }
        [QdasMapping(2018, QdasFieldType.I3)]
        public byte? AcceptanceLimitExtended { get; set; }

        [QdasMapping(2019, QdasFieldType.I5)]
        public short? iOrdinalklassen_Katalog { get; set; }
        //K2020 entfallen note mbe: original is I1
        [Obsolete]
        [QdasMapping(2020, QdasFieldType.I3)]
        public short? iAuto_Erkennung { get; set; }
        [QdasMapping(2021, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sVerknuepfungstext { get; set; }
        [QdasMapping(2022, QdasFieldType.I5)]
        public short? iNachkommastellen { get; set; }
        [QdasMapping(2023, QdasFieldType.I3)]
        public short? iTransformation_Art { get; set; }
        [QdasMapping(2024, QdasFieldType.F)]
        public double? dTransformation_Parameter_a { get; set; }
        [QdasMapping(2025, QdasFieldType.F)]
        public double? dTransformation_Parameter_b { get; set; }
        [QdasMapping(2026, QdasFieldType.F)]
        public double? dTransformation_Parameter_c { get; set; }
        [QdasMapping(2027, QdasFieldType.F)]
        public double? dTransformation_Parameter_d { get; set; }
        [QdasMapping(2028, QdasFieldType.I3)]
        public short? iNatuerliche_Verteilung { get; set; }

        [QdasMapping(2030, QdasFieldType.I5)]
        public short? iGruppen_Nummer { get; set; }
        [QdasMapping(2031, QdasFieldType.I5)]
        public short? iGruppenelement_Nummer { get; set; }

        [QdasMapping(2035, QdasFieldType.D)]
        public DateTime? CalibrationDate { get; set; }

        [QdasMapping(2041, QdasFieldType.I3)]
        public short? iErfassungsart { get; set; }
        [QdasMapping(2042, QdasFieldType.I5)]
        public short? iErfassungsgeraet_Nummer { get; set; }

        [QdasMapping(2043, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sErfassungsgeraet_Name { get; set; }
        [QdasMapping(2044, QdasFieldType.I5)]
        public short? iErfassungsgeraet_Index { get; set; }
        [QdasMapping(2045, QdasFieldType.I3)]
        public short? iErfassung_Kanal { get; set; }

        [QdasMapping(2046, QdasFieldType.I3)]
        public short? iErfassung_Subkanal { get; set; }
        [QdasMapping(2047, QdasFieldType.I3)]
        public short? iSoftwareanforderung_Index { get; set; }
        [QdasMapping(2048, QdasFieldType.I3)]
        public short? TakeoverChannel { get; set; }
        [QdasMapping(2049, QdasFieldType.I3)]
        public short? ChannelInitializationIndex { get; set; }

        [QdasMapping(2051, QdasFieldType.I3)]
        public short? iSchnittstelle { get; set; }
        //Note: K2052 is an i5 field in AQDEF, but can have larger values
        [QdasMapping(2052, QdasFieldType.I10)]
        public int? iBaudrate { get; set; }
        [QdasMapping(2053, QdasFieldType.I3)]
        public short? IrqNumber { get; set; }
        [QdasMapping(2054, QdasFieldType.I3)]
        public short? iParitaet { get; set; }
        [QdasMapping(2055, QdasFieldType.I3)]
        public short? iDatenbits { get; set; }
        [QdasMapping(2056, QdasFieldType.I3)]
        public short? iStopbits { get; set; }

        [QdasMapping(2060, QdasFieldType.I5)]
        public short? EventCatalog { get; set; }
        [QdasMapping(2061, QdasFieldType.I5)]
        public short? iProzessparameter_Katalog { get; set; }
        [QdasMapping(2062, QdasFieldType.I5)]
        public short? iNest_Katalog { get; set; }
        [QdasMapping(2063, QdasFieldType.I5)]
        public short? iMaschinen_Katalog { get; set; }
        [QdasMapping(2064, QdasFieldType.I5)]
        public short? iPruefmittel_Katalog { get; set; }
        [QdasMapping(2065, QdasFieldType.I5)]
        public short? iPruefer_Katalog { get; set; }
        [QdasMapping(2066, QdasFieldType.I5)]
        public short? iK0061_Katalog { get; set; }
        [QdasMapping(2067, QdasFieldType.I5)]
        public short? iK0062_Katalog { get; set; }
        [QdasMapping(2068, QdasFieldType.I5)]
        public short? iK0063_Katalog { get; set; }

        [QdasMapping(2071, QdasFieldType.F)]
        public double? dAdditionskonstante { get; set; }
        [QdasMapping(2072, QdasFieldType.F)]
        public double? dMultiplikationsfaktor { get; set; }
        [QdasMapping(2073, QdasFieldType.F)]
        public double? DimensionCalibrationMaster { get; set; }
        [QdasMapping(2074, QdasFieldType.F)]
        public double? ActualOffsetCalibrationMeasurements { get; set; }
        [QdasMapping(2075, QdasFieldType.F)]
        public double? AmplificationFactorCalibrationMeasurements { get; set; }
        [QdasMapping(2076, QdasFieldType.D)]
        public DateTime? CalibrationDate2 { get; set; }

        [QdasMapping(2080, QdasFieldType.I5)]
        public short? CharacteristicStatus { get; set; }

        [QdasMapping(2090, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMerkmal_Code { get; set; }
        [QdasMapping(2091, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string CharacteristicIndex { get; set; }
        [QdasMapping(2092, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string CharacteristicText { get; set; }
        [QdasMapping(2093, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string ProcessingStatus { get; set; }

        [QdasMapping(2095, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string ElementCode { get; set; }
        [QdasMapping(2096, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string ElementIndex { get; set; }
        [QdasMapping(2097, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string ElementText { get; set; }
        [QdasMapping(2098, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string ElementAddress { get; set; }

        [QdasMapping(2100, QdasFieldType.F)]
        public double? dSollwert { get; set; }
        [QdasMapping(2101, QdasFieldType.F)]
        public double? dNennmass { get; set; }
        [QdasMapping(2102, QdasFieldType.F)]
        public double? Pmax { get; set; }
        [QdasMapping(2103, QdasFieldType.A, QdasSpecialConversion.A2)]
        public string ToleranceClass { get; set; }
        [QdasMapping(2104, QdasFieldType.I3)]
        public byte? ToleranceValue { get; set; }
        [QdasMapping(2105, QdasFieldType.I5)]
        public short? PartsOkCensoring { get; set; }

        [QdasMapping(2110, QdasFieldType.F)]
        public double? LowerSpecificationLimit { get; set; }
        [QdasMapping(2111, QdasFieldType.F)]
        public double? UpperSpecificationLimit { get; set; }
        [QdasMapping(2112, QdasFieldType.F)]
        public double? LowerAllowance { get; set; }
        [QdasMapping(2113, QdasFieldType.F)]
        public double? UpperAllowance { get; set; }
        [QdasMapping(2114, QdasFieldType.F)]
        public double? dSchrottgrenze_unten { get; set; }
        [QdasMapping(2115, QdasFieldType.F)]
        public double? dSchrottgrenze_oben { get; set; }
        [QdasMapping(2116, QdasFieldType.F)]
        public double? LowerAcceptanceLimit { get; set; }
        [QdasMapping(2117, QdasFieldType.F)]
        public double? UpperAcceptanceLimit { get; set; }

        [QdasMapping(2120, QdasFieldType.I3)]
        public short? iArt_der_Grenze_unten { get; set; }
        [QdasMapping(2121, QdasFieldType.I3)]
        public short? iArt_der_Grenze_oben { get; set; }

        [QdasMapping(2130, QdasFieldType.F)]
        public double? LowerPlausibilityLimit { get; set; }
        [QdasMapping(2131, QdasFieldType.F)]
        public double? UpperPlausibilityLimit { get; set; }

        [QdasMapping(2135, QdasFieldType.F)]
        public double? LowerClassLimit { get; set; }
        [QdasMapping(2136, QdasFieldType.F)]
        public double? UpperClassLimit { get; set; }

        [QdasMapping(2137, QdasFieldType.I3)]
        public byte? NumberOfClasses { get; set; }
        [QdasMapping(2138, QdasFieldType.I3)]
        public byte? ClassificationSource { get; set; }
        [QdasMapping(2139, QdasFieldType.I3)]
        public byte? ClassificationModelSource { get; set; }

        [QdasMapping(2141, QdasFieldType.I5)]
        public short? UnitEntry { get; set; }
        [QdasMapping(2142, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sEinheit_Bezeichnung { get; set; }
        [QdasMapping(2143, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string UnitRelativeAxis { get; set; }
        [QdasMapping(2144, QdasFieldType.F)]
        public double? AdditionConstantRelativeAxis { get; set; }
        [QdasMapping(2145, QdasFieldType.F)]
        public double? MultiplicationFactorRelativeAxis { get; set; }
        [QdasMapping(2146, QdasFieldType.I3)]
        public byte? DecimalPlacesRelativeAxis { get; set; }

        [QdasMapping(2151, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sToleranz_Text { get; set; }
        [QdasMapping(2152, QdasFieldType.F)]
        public double? CalculatedTolerance { get; set; }

        [QdasMapping(2160, QdasFieldType.I10)]
        public int? lLosumfang { get; set; }
        [QdasMapping(2161, QdasFieldType.F)]
        public double? dKosten_Nacharbeit { get; set; }
        [QdasMapping(2162, QdasFieldType.F)]
        public double? dKosten_Ausschuss { get; set; }
        [QdasMapping(2163, QdasFieldType.F)]
        public double? dFehlerkosten { get; set; }

        [QdasMapping(2170, QdasFieldType.F)]
        public double? CensoringPointTime { get; set; }
        [QdasMapping(2171, QdasFieldType.F)]
        public double? Extrapolation { get; set; }
        [QdasMapping(2172, QdasFieldType.F)]
        public double? PermittedFailureRate { get; set; }
        [QdasMapping(2173, QdasFieldType.F)]
        public double? FailureFreeTime { get; set; }
        [QdasMapping(2174, QdasFieldType.I3)]
        public byte? PartsField { get; set; }
        [QdasMapping(2175, QdasFieldType.I3)]
        public byte? ConditionsFreqSums { get; set; }
        [QdasMapping(2176, QdasFieldType.I3)]
        public byte? LifeDistAEckel { get; set; }
        [QdasMapping(2177, QdasFieldType.F)]
        public double? LifeTimeX1 { get; set; }
        [QdasMapping(2178, QdasFieldType.F)]
        public double? LifeTimeX2 { get; set; }

        [QdasMapping(2180, QdasFieldType.F)]
        public double? WeibullB { get; set; }
        [QdasMapping(2181, QdasFieldType.F)]
        public double? WeibullT { get; set; }
        [QdasMapping(2182, QdasFieldType.F)]
        public double? SumProbBX1 { get; set; }
        [QdasMapping(2183, QdasFieldType.F)]
        public double? SumProbBX2 { get; set; }
        [QdasMapping(2185, QdasFieldType.I10)]
        public int? NumberPartsEckel { get; set; }
        [QdasMapping(2186, QdasFieldType.F)]
        public double? UsageTimePartsEckel { get; set; }

        [QdasMapping(2201, QdasFieldType.F)]
        public double? dProzessstreuung { get; set; }
        [QdasMapping(2202, QdasFieldType.I3)]
        public byte? iAuswertetyp { get; set; }

        [QdasMapping(2205, QdasFieldType.I5)]
        public short? iAnzahl_Teile { get; set; }
        [QdasMapping(2206, QdasFieldType.I5)]
        public short? NumberDevices { get; set; }
        [QdasMapping(2207, QdasFieldType.I5)]
        public short? NumberTestIntervals { get; set; }

        [QdasMapping(2210, QdasFieldType.I5)]
        public short? MasterCatalogueRef { get; set; }

        [QdasMapping(2211, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sNormal_Nummer_Text { get; set; }
        [QdasMapping(2212, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sNormal_Bezeichnung { get; set; }
        [QdasMapping(2213, QdasFieldType.F)]
        public double? dNormal_Istwert { get; set; }
        [QdasMapping(2214, QdasFieldType.F)]
        public double? dNormal_Temperatur { get; set; }
        [QdasMapping(2215, QdasFieldType.I5)]
        public short? MasterNumber { get; set; }
        [QdasMapping(2216, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string MasterSerialNumber { get; set; }
        [QdasMapping(2217, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string MasterManufacturer { get; set; }

        [QdasMapping(2220, QdasFieldType.I5)]
        public short? iAnzahl_Pruefer { get; set; }
        [QdasMapping(2221, QdasFieldType.I5)]
        public short? iAnzahl_Messungen { get; set; }
        [QdasMapping(2222, QdasFieldType.I5)]
        public short? NoReferenceMeasurements { get; set; }

        [QdasMapping(2225, QdasFieldType.F)]
        public double? dCg_Wert_ermittelt { get; set; }
        [QdasMapping(2226, QdasFieldType.F)]
        public double? dCgk_Wert_ermittelt { get; set; }
        [QdasMapping(2227, QdasFieldType.F)]
        public double? dAbweichung_GC_Typ3_Typ1 { get; set; }
        [QdasMapping(2228, QdasFieldType.F)]
        public double? SgType1Stability { get; set; }

        [QdasMapping(2243, QdasFieldType.A,QdasSpecialConversion.A80)]
        public string sZeichnung_Dateiname { get; set; }
        [QdasMapping(2244,QdasFieldType.I5)]
        public short? DrawingReferencePointX { get; set; }
        [QdasMapping(2245,QdasFieldType.I5)]
        public short? DrawingReferencePointY { get; set; }
        [QdasMapping(2246,QdasFieldType.I5)]
        public short? DrawingReferencePointZ { get; set; }

        [Obsolete]
        [QdasMapping(2251, QdasFieldType.F)]
        public double? dNormal_Istwert_unten { get; set; }
        [QdasMapping(2252, QdasFieldType.F)]
        public double? dNormal_Istwert_oben { get; set; }

        [QdasMapping(2261,QdasFieldType.A,QdasSpecialConversion.A40)]
        public string ReferencePartNumber { get; set; }
        [QdasMapping(2262,QdasFieldType.A, QdasSpecialConversion.A40)]
        public string ReferencePartDescription { get; set; }
        [QdasMapping(2263, QdasFieldType.F)]
        public double? ReferencePartActualValue { get; set; }
        [QdasMapping(2264, QdasFieldType.F)]
        public double? ReferencePartTemperature { get; set; }
        [QdasMapping(2265, QdasFieldType.I3)]
        public byte? ReferencePartNumberNum { get; set; }
        [QdasMapping(2266, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string ReferencePartSerialNumber { get; set; }

        [QdasMapping(2281,QdasFieldType.A,QdasSpecialConversion.A40)]
        public string CalibrationPartNumberMiddle { get; set; }
        [QdasMapping(2282, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string CalibrationPartDescriptionMiddle { get; set; }
        [QdasMapping(2283, QdasFieldType.F)]
        public double? CalibrationPartActualValueMiddle { get; set; }
        [QdasMapping(2284, QdasFieldType.F)]
        public double? CalibrationPartTemperatureMiddle { get; set; }
        [QdasMapping(2285,QdasFieldType.I3)]
        public byte? CalibrationPartNumberNumMiddle { get; set; }
        [QdasMapping(2286,QdasFieldType.A,QdasSpecialConversion.A40)]
        public string CalibrationPartSerialNumberMiddle { get; set; }

        [QdasMapping(2301, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sMaschine_Nummer_Text { get; set; }
        [QdasMapping(2302, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMaschine_Bezeichnung { get; set; }
        [QdasMapping(2303, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMaschine_Kostenstelle { get; set; }
        [QdasMapping(2304, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMaschine_Standort { get; set; }
        [QdasMapping(2305, QdasFieldType.I10)]
        public int? lMaschine_Nummer { get; set; }

        [QdasMapping(2306, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMaschine_Bereich_im_Werk { get; set; }

        [QdasMapping(2307, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMaschine_PTM_Nummer { get; set; }

        [QdasMapping(2311, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sFertigungsart_Text { get; set; }
        [QdasMapping(2312, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sFertigungsart_Bezeichnung { get; set; }
        [QdasMapping(2313, QdasFieldType.I5)]
        public int? lFertigungsart_Nummer { get; set; }

        [QdasMapping(2320, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sAuftrag_Nummer { get; set; }
        [QdasMapping(2321, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sAuftraggeber_Nummer_Text { get; set; }
        [QdasMapping(2322, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sAuftraggeber_Name { get; set; }
        [QdasMapping(2323, QdasFieldType.I5)]
        public int? lAuftraggeber_Nummer { get; set; }

        [QdasMapping(2331, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sWerkstueck_Nummer_Text { get; set; }
        [QdasMapping(2332, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sWerkstueck_Bezeichnung { get; set; }
        [QdasMapping(2333, QdasFieldType.I5)]
        public int? lWerkstueck_Nummer { get; set; }

        [QdasMapping(2341, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sPruefplan_Nummer_Text { get; set; }
        [QdasMapping(2342, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefplan_Name { get; set; }
        [QdasMapping(2343, QdasFieldType.D)]
        public DateTime? sPruefplan_Erstelldatum { get; set; }
        [QdasMapping(2344, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefplan_Ersteller { get; set; }

        [QdasMapping(2401, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefmittel_Nummer_Text { get; set; }
        [QdasMapping(2402, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefmittel_Bezeichnung { get; set; }
        [QdasMapping(2403, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sPruefmittel_Gruppe { get; set; }
        [QdasMapping(2404, QdasFieldType.F)]
        public double? dPruefmittel_Aufloesung { get; set; }
        [QdasMapping(2405, QdasFieldType.I5)]
        public int? lPruefmittel_Nummer { get; set; }
        [QdasMapping(2406, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefmittel_Hersteller { get; set; }
        [QdasMapping(2407, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sSPC_Geraet_Nummer_Text { get; set; }
        [QdasMapping(2408, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sSPC_Geraet_Bezeichnung { get; set; }
        [QdasMapping(2409, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sSPC_Geraet_Typ { get; set; }
        [QdasMapping(2410, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefort { get; set; }
        [QdasMapping(2411, QdasFieldType.D)]
        public DateTime? sPruefbeginn { get; set; }
        [QdasMapping(2412, QdasFieldType.D)]
        public DateTime? sPruefende { get; set; }
        [QdasMapping(2413, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string GageGraphic { get; set; }

        [QdasMapping(2415, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string GageSerialNumber { get; set; }
        [QdasMapping(2416, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string DisplayDevice { get; set; }

        [QdasMapping(2421, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sPruefer_Nummer_Text { get; set; }

        [QdasMapping(2422, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefer_Name { get; set; }
        [QdasMapping(2423, QdasFieldType.I10)]
        public int? lPruefer_Nummer { get; set; }

        [QdasMapping(2430,QdasFieldType.I5)]
        public short? SamplingType { get; set; }

        [QdasMapping(2432, QdasFieldType.I5)]
        public short? IndividualValueOutput { get; set; }

        [QdasMapping(2434, QdasFieldType.I5)]
        public short? ProofProcessCapability { get; set; }

        [QdasMapping(2436, QdasFieldType.A, QdasSpecialConversion.A10)]
        public string TestFrequency { get; set; }

        [QdasMapping(2438, QdasFieldType.A, QdasSpecialConversion.A10)]
        public string QuantityTested { get; set; }

        [QdasMapping(2440, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string AssemblyComponent { get; set; }

        [QdasMapping(2442, QdasFieldType.A, QdasSpecialConversion.A12)]
        public string AssemblyComponentMass { get; set; }

        [QdasMapping(2444, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string AssemblyComponentMaterial { get; set; }

        [QdasMapping(2446, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string SuppliersProductDescription { get; set; }

        [QdasMapping(2448, QdasFieldType.A,QdasSpecialConversion.A40)]
        public string AssemblyComponentManufacturer { get; set; }

        [QdasMapping(2501,QdasFieldType.I3)]
        public byte? DimensionAttribute { get; set; }
        [QdasMapping(2502, QdasFieldType.I3)]
        public byte? ToleranceDisplayFormat { get; set; }
        [QdasMapping(2503, QdasFieldType.I3)]
        public byte? DimensionType { get; set; }
        [QdasMapping(2504, QdasFieldType.I3)]
        public byte? DrawingChangeStatus { get; set; }
        [QdasMapping(2505, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string ViewDescription { get; set; }
        [QdasMapping(2506, QdasFieldType.I3)]
        public byte? SheetNumber { get; set; }
        [QdasMapping(2507, QdasFieldType.A, QdasSpecialConversion.A2)]
        public string DrawingFieldCharacter { get; set; }
        [QdasMapping(2508, QdasFieldType.I3)]
        public byte? DrawingFieldNumber { get; set; }
        [QdasMapping(2509, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string SheetDescription { get; set; }

        [QdasMapping(2511, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference1 { get; set; }
        [QdasMapping(2512, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference2 { get; set; }
        [QdasMapping(2513, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference3 { get; set; }
        [QdasMapping(2514, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference4 { get; set; }
        [QdasMapping(2515, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference5 { get; set; }
        [QdasMapping(2516, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference6 { get; set; }
        [QdasMapping(2517, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference7 { get; set; }
        [QdasMapping(2518, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference8 { get; set; }
        [QdasMapping(2519, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Reference9 { get; set; }
        [QdasMapping(2520, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string ReferenceSystem { get; set; }
        [QdasMapping(2521, QdasFieldType.F)]
        public double? ReferenceXDirection { get; set; }
        [QdasMapping(2522, QdasFieldType.F)]
        public double? ReferenceYDirection { get; set; }
        [QdasMapping(2523, QdasFieldType.F)]
        public double? ReferenceZDirection { get; set; }
        [QdasMapping(2524, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string CadInternalReference { get; set; }
        [QdasMapping(2525, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string RotationMatrix { get; set; }
        [QdasMapping(2526, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string CadDetail { get; set; }

        [QdasMapping(2630, QdasFieldType.F)]
        public double? CalibrationUncertainty { get; set; }

        [QdasMapping(2646, QdasFieldType.I10)]
        public int? ComponentType { get; set; }

        [QdasMapping(2654, QdasFieldType.I3)]
        public byte? Vda5Version { get; set; }

        [QdasMapping(2800, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_1 { get; set; }
        [QdasMapping(2801, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_1 { get; set; }
        [QdasMapping(2802, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_1 { get; set; }

        [QdasMapping(2810, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_2 { get; set; }
        [QdasMapping(2811, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_2 { get; set; }
        [QdasMapping(2812, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_2 { get; set; }

        [QdasMapping(2820, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_3 { get; set; }
        [QdasMapping(2821, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_3 { get; set; }
        [QdasMapping(2822, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_3 { get; set; }

        [QdasMapping(2830, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_4 { get; set; }
        [QdasMapping(2831, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_4 { get; set; }
        [QdasMapping(2832, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_4 { get; set; }

        [QdasMapping(2840, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_5 { get; set; }
        [QdasMapping(2841, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_5 { get; set; }
        [QdasMapping(2842, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_5 { get; set; }

        [QdasMapping(2850, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_6 { get; set; }
        [QdasMapping(2851, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_6 { get; set; }
        [QdasMapping(2852, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_6 { get; set; }

        [QdasMapping(2860, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_7 { get; set; }
        [QdasMapping(2861, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_7 { get; set; }
        [QdasMapping(2862, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_7 { get; set; }

        [QdasMapping(2870, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_8 { get; set; }
        [QdasMapping(2871, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_8 { get; set; }
        [QdasMapping(2872, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_8 { get; set; }

        [QdasMapping(2880, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_9 { get; set; }
        [QdasMapping(2881, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_9 { get; set; }
        [QdasMapping(2882, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_9 { get; set; }

        [QdasMapping(2890, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_10 { get; set; }
        [QdasMapping(2891, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_10 { get; set; }
        [QdasMapping(2892, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_10 { get; set; }

        [QdasMapping(2900, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBemerkung { get; set; }
        [QdasMapping(2901, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string sPruefbedingungen { get; set; }

        // leeres Feld //K2990

        //K2997 note mbe: GUI not defined in qdas
        //[QdasMapping(2997, QdasFieldType.A, QdasSpecialConversion.A255)]
        //public string sMerkmal_GUID { get; set; }
        //K2998
        //[QdasMapping(2998, QdasFieldType.A, QdasSpecialConversion.A255)]
        //public string sInterne_Konfiguration { get; set; }

        [QdasMapping(8006, QdasFieldType.F)]
        public double? dLage_Alarmgrenze_unten { get; set; }
        [QdasMapping(8007, QdasFieldType.F)]
        public double? dLage_Alarmgrenze_oben { get; set; }
        
        [QdasMapping(8010,QdasFieldType.S,QdasSpecialConversion.ChartTypeLocation)]
        public QdasChartTypeLocation ChartTypeLocation { get; set; }
        [QdasMapping(8011, QdasFieldType.F)]
        public double? dLage_Mittellage { get; set; }
        [QdasMapping(8012, QdasFieldType.F)]
        public double? dLage_UEG { get; set; }
        [QdasMapping(8013, QdasFieldType.F)]
        public double? dLage_OEG { get; set; }
        [QdasMapping(8014, QdasFieldType.F)]
        public double? dLage_UWG { get; set; }
        [QdasMapping(8015, QdasFieldType.F)]
        public double? dLage_OWG { get; set; }

        [QdasMapping(8106, QdasFieldType.F)]
        public double? LowerAlarmLimitVariation { get; set; }
        [QdasMapping(8107, QdasFieldType.F)]
        public double? UpperAlarmLimitVariation { get; set; }

        [QdasMapping(8110, QdasFieldType.S, QdasSpecialConversion.ChartTypeVariation)]
        public QdasChartTypeVariation ChartTypeVariation { get; set; }
        [QdasMapping(8111, QdasFieldType.F)]
        public double? dStreuung_Mittellage { get; set; }

        [QdasMapping(8112, QdasFieldType.F)]
        public double? dStreuung_UEG { get; set; }
        [QdasMapping(8113, QdasFieldType.F)]
        public double? dStreuung_OEG { get; set; }
        [QdasMapping(8114, QdasFieldType.F)]
        public double? dStreuung_UWG { get; set; }
        [QdasMapping(8115, QdasFieldType.F)]
        public double? dStreuung_OWG { get; set; }

        //note: K8500,K8505 (qsStat needs to be at least ME6)
        [QdasMapping(8500, QdasFieldType.I5)]
        [QdasMapping(8505, QdasFieldType.I5)]
        public short? iStichprobe_Umfang_gesamt { get; set; }
        [QdasMapping(8501, QdasFieldType.I3)]
        public byte? SubgroupType { get; set; }
        [QdasMapping(8502, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sStichprobe_Frequenz { get; set; }
        [QdasMapping(8503, QdasFieldType.I3)]
        public byte? StableSubgroupSize { get; set; }
        [QdasMapping(8504, QdasFieldType.I5)]
        public short? iStichprobe_Haeufigkeit { get; set; }
        
        [Obsolete]
        [QdasMapping(8506, QdasFieldType.I5)]
        public short? iStichprobenumfang_attributiv { get; set; }

        [Obsolete]
        [QdasMapping(8510, QdasFieldType.F)]
        public double? dCp_Wert { get; set; }
        [Obsolete]
        [QdasMapping(8511, QdasFieldType.F)]
        public double? dCpk_Wert { get; set; }
        
        [QdasMapping(8520, QdasFieldType.F)]
        public double? dCp_Wert_gefordert { get; set; }
        [QdasMapping(8521, QdasFieldType.F)]
        public double? dCpk_Wert_gefordert { get; set; }
        [QdasMapping(8522, QdasFieldType.F)]
        public double? dCp_Wert_gefixt { get; set; }
        [QdasMapping(8523, QdasFieldType.F)]
        public double? dCpk_Wert_gefixt { get; set; }
        [QdasMapping(8524, QdasFieldType.F)]
        public double? RequiredCamValue { get; set; }
        [QdasMapping(8525, QdasFieldType.F)]
        public double? RequiredNonCritCapabilityIndex { get; set; }

        [QdasMapping(8530, QdasFieldType.I5)]
        public short? processStability { get; set; }
        [QdasMapping(8531, QdasFieldType.F)]
        public double? recordedProcessCapabilityCp { get; set; }
        [QdasMapping(8532, QdasFieldType.F)]
        public double? recordedProcessCapabilityCpk { get; set; }
        
        [QdasMapping(8540, QdasFieldType.I5)]
        public short? Evaluation { get; set; }

        [QdasMapping(8600, QdasFieldType.I3)]
        public short? iKorrekturstrategie { get; set; }
        
        [QdasMapping(8610, QdasFieldType.F)]
        public double? dKorrekturgrenze_unten { get; set; }
        [QdasMapping(8611, QdasFieldType.F)]
        public double? dKorrekturgrenze_oben { get; set; }
        [QdasMapping(8612, QdasFieldType.I3)]
        public double? dPuffergroesse { get; set; }
        [QdasMapping(8613, QdasFieldType.F)]
        public double? CorrectionTargetValue { get; set; }

        #endregion

        public TS_MERKMALSDATEN()
        {
            this.ChartTypeLocation = new QdasChartTypeLocation();
            this.ChartTypeVariation = new QdasChartTypeVariation();

            this._unrecognizedKeyData = new List<QdasLineSplitResult>();
        }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this,knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        public static bool DefinesMappableKField(short kNumber)
        {
            return _kMappings.DefinesMappableKField(kNumber);
        }

        public static bool TryGetQdasMappingAttribute(short kNumber, out QdasMappingAttribute value)
        {
            return _kMappings.TryGetQdasMappingAttribute(kNumber, out value);
        }

        /// <summary>
        /// Store unrecognized k-field data
        /// </summary>
        /// <param name="line">the unreconized k-field</param>
        public void AddUnrecognizedLine(QdasLineSplitResult unrecognizedData)
        {
            this._unrecognizedKeyData.Add(unrecognizedData);
        }

        /// <summary>
        /// Retrieve unrecognized k-field data
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<QdasLineSplitResult> GetUnparsedLines()
        {
            return this._unrecognizedKeyData.AsReadOnly();
        }
    }
}
