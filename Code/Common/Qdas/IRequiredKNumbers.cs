﻿using System.Collections.Generic;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Interface for retrieving required k-numbers
    /// </summary>
    public interface IRequiredKNumbers
    {
        /// <summary>
        /// Get a collection of required k-numbers
        /// </summary>
        /// <returns>The numbers</returns>
        ICollection<short> GetRequiredKNumbers();
    }
}
