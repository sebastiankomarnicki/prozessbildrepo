﻿namespace Promess.Common.QDas
{
    /// <summary>
    /// Data container and parser for K8110 with custom data format
    /// </summary>
    public class QdasChartTypeVariation
    {
        //K8110/1 note mbe: this is mapped manually/different mechanism
        public short iStreuung_Kartenart { get; set; }
        //K8110/2
        public short? iStreuung_Streuungsschaetzer { get; set; }

        public static QdasChartTypeVariation Parse(string input)
        {
            QdasChartTypeVariation qctv = new QdasChartTypeVariation();
            string[] asWerte = input.TrimStart().Split(' ');
            qctv.iStreuung_Kartenart = short.Parse(asWerte[0]);
            if (asWerte.Length >= 2)
            {
                qctv.iStreuung_Streuungsschaetzer = short.Parse(asWerte[1]);
            }
            return qctv;
        }

        /// <summary>
        /// Create string representation of data
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (!iStreuung_Streuungsschaetzer.HasValue)
            {
                return $"{iStreuung_Kartenart}";
            }
            else
            {
                return $"{iStreuung_Kartenart} {iStreuung_Streuungsschaetzer.Value}";
            }
        }
    }
}
