﻿using System;
using System.Collections.Generic;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Interface for defining interactions for most AQDEF main catalogs
    /// </summary>
    public interface IMainCatalog:ISupportKNumberListing
    {
        /// <summary>
        /// K-Number which defines the main and subcatalogs
        /// </summary>
        short MainCatalogKNumber { get; }
        /// <summary>
        /// Name of the catalog
        /// </summary>
        string MainCatalogName { get; }
        /// <summary>
        /// Add data to a catalog. Either a subcatalog or an entry.
        /// </summary>
        /// <param name="kNumber">k-number of the entry</param>
        /// <param name="reference">Reference number</param>
        /// <param name="data">Data</param>
        void AddData(short kNumber, int reference, string data);

        /// <summary>
        /// Checks if the catalog contains data
        /// </summary>
        /// <returns>Result of the check</returns>
        bool ContainsData();
        /// <summary>
        /// Are all references to subcatalogs and catalog entries consistent (referenced entries exist)?
        /// </summary>
        /// <returns>Result of consistency check</returns>
        bool IsConsistent();

        /// <summary>
        /// Get the list of the required k-numbers for catalog entries
        /// </summary>
        /// <returns>The list</returns>
        ICollection<short> GetRequiredKNumbersOfEntries();

        /// <summary>
        /// Iterator for all catalog entries and their k-numbers
        /// </summary>
        /// <returns>Pairs of entry reference and entry for data manipulation</returns>
        IEnumerable<Tuple<int, ISupportKNumberDataManipulation>> GetCatalogEntriesAndQdasKeys();

        /// <summary>
        /// Iterator for all subcatalog references and their names
        /// </summary>
        /// <returns></returns>
        IEnumerable<Tuple<int, string>> GetSubcatalogsAndQdasKeys();

        /// <summary>
        /// Iterator for all references catalog entries for a subcatalog
        /// </summary>
        /// <param name="subcatalogKey">Reference of the subcatalog</param>
        /// <returns></returns>
        IEnumerable<int> GetQdasKeysInSubcatalog(int subcatalogKey);

        /// <summary>
        /// Remove all subcatalogs without name and catalog entries where required fields are missing or without value
        /// </summary>
        /// <returns><c>Tuple</c> of removed subcatalogs, removed entries</returns>
        Tuple<ICollection<int>, ICollection<int>> RemoveSubcatalogsAndEntriesWORequiredValues();
    }
}
