﻿namespace Promess.Common.QDas
{
    /// <summary>
    /// Error codes from converted dfd/dfx parse code
    /// </summary>
    public enum iErrCodes
    {
        iecno_error=0,
        iecEOF = -1,
        iecNoDataFile = 1,
        iecCopy = 2,
        iecopenFile = 3
    }

    /// <summary>
    /// Values for K2004, characteristic type
    /// </summary>
    public enum iMerkmal_Art {
        imaVARIABEL = 0,
        imaATTRIBUTIV = 1,
        imaVARIABEL_KLASSIERT = 2,
        imaORDINAL = 3,
        imaNOMINAL = 4
    }

    /// <summary>
    /// Values for K2120/K2121, limit type
    /// </summary>
    public enum iGrenzwertueber {
        iguALLGEMEIN = -1,
        iguOGW_UGW = 0,
        iguNACHARBEIT = 1
    }
}
