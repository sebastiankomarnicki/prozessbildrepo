﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class MachineCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<MachineCatalogEntry> _kMappings;
        static MachineCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<MachineCatalogEntry>();
        }

        [Required]
        [QdasMapping(4062, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Number { get; set; }
        [QdasMapping(4063, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name { get; set; }
        [QdasMapping(4064, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string Sector { get; set; }
        [QdasMapping(4065, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string Department { get; set; }
        [QdasMapping(4066, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string OperatingSeqNo { get; set; }
        [QdasMapping(4067, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string ExternalReferenceNo { get; set; }
        [QdasMapping(4561, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMapping(4562, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
