﻿using Promess.Common.QDas.Exceptions;
using System.Collections.Generic;

namespace Promess.Common.QDas.Catalogs
{
    public class K4220Catalogs
    {
        private K4220EventCatalog _eventCatalog;
        private K4220MeasureCatalog _measureCatalog;
        private K4220CauseCatalog _causeCatalog;

        public K4220Catalogs()
        {
            _eventCatalog = new K4220EventCatalog();
            _measureCatalog = new K4220MeasureCatalog();
            _causeCatalog = new K4220CauseCatalog();
        }

        public void AddData(short kNumber, int reference, string data)
        {
            if (reference < 0)
                throw new InvalidCatalogQdasLineException(kNumber, reference, data);

            if (reference < 10000)
            {
                _eventCatalog.AddData(kNumber, reference, data);
            }else if (reference < 20000)
            {
                _measureCatalog.AddData(kNumber, reference, data);
            }
            else
            {
                _causeCatalog.AddData(kNumber, reference, data);
            }
        }

        public ISet<short> DefinedKNumbers
        {
            get
            {
                return _eventCatalog.GetDefinedKNumbers(); 
            }
        }

        public K4220EventCatalog EventCatalog
        {
            get
            {
                return _eventCatalog;
            }
        }

        public K4220MeasureCatalog MeasureCatalog
        {
            get
            {
                return _measureCatalog;
            }
        }

        public K4220CauseCatalog CauseCatalog
        {
            get
            {
                return _causeCatalog;
            }
        }
    }
}
