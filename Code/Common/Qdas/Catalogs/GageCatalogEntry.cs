﻿using Promess.Common.QDas.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class GageCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<GageCatalogEntry> _kMappings;
        static GageCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<GageCatalogEntry>();
        }

        [Required]
        [QdasMapping(4072, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Number { get; set; }
        [Required]
        [QdasMapping(4073, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name { get; set; }
        [QdasMapping(4074, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Group { get; set; }
        [QdasMapping(4075, QdasFieldType.D)]
        public DateTime? LastCalibrationDate { get; set; }
        [QdasMapping(4076, QdasFieldType.D)]
        public DateTime? NextCalibrationDate { get; set; }
        [QdasMapping(4077, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string IpAddress { get; set; }
        [QdasMapping(4078, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string Location { get; set; }
        [QdasMapping(4079, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string ComputerModel { get; set; }

        [QdasMapping(4571, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMapping(4572, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }
        [QdasMapping(4575, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string ProgramVersion { get; set; }
        [QdasMapping(4576, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string Software { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
