﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class MaterialCatalogEntry : ISupportKNumberDataManipulation,ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<MaterialCatalogEntry> _kMappings;
        static MaterialCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<MaterialCatalogEntry>();
        }

        [Required]
        [QdasMapping(4042, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string MaterialNumber { get; set; }

        [Required]
        [QdasMapping(4043, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string MaterialName { get; set; }

        [QdasMapping(4541, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMapping(4542, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string MaterialRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
