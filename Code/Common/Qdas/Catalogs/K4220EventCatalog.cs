﻿using Promess.Common.QDas.Exceptions;
using System.Collections.Generic;
using System.Globalization;

namespace Promess.Common.QDas.Catalogs
{
    public class K4220EventCatalog : K4220BaseCatalog
    {
        public override void AddData(short kNumber, int reference, string data)
        {
            if (reference < 0 || reference >= 10000)
                throw new InvalidCatalogQdasLineException(kNumber, reference, data);

            if (kNumber == MainCatalogKNumber)
            {
                string limited;
                if (data.Length > _catalogNameLength)
                {
                    limited = data.Substring(0, _catalogNameLength);
                }
                else
                {
                    limited = data;
                }
                if (reference == 0)
                {
                    MainCatalogName = limited;
                }
                else
                {
                    _subCatalogRefToName[reference] = limited;
                }

            }
            else if (kNumber == _eventToSubcatalogKNumber)
            {
                if (reference == 0)
                    throw new InvalidCatalogQdasLineException(kNumber, reference, data);

                int entryRef = int.Parse(data, CultureInfo.InvariantCulture);

                HashSet<int> entries = null;
                if (_subCatalogRefToEntryRefs.TryGetValue(reference, out entries))
                {
                    entries.Add(entryRef);
                }
                else
                {
                    _subCatalogRefToEntryRefs[reference] = new HashSet<int>() { entryRef };
                }
            }
            else
            {
                if (reference == 0)
                    throw new InvalidCatalogQdasLineException(kNumber, reference, data);

                K4220CatalogEntry entry = null;
                if (_entryRefToCatalogEntry.TryGetValue(reference, out entry))
                {
                    entry.TrySetField(kNumber, data);
                }
                else
                {
                    entry = new K4220CatalogEntry();
                    entry.TrySetField(kNumber, data);
                    _entryRefToCatalogEntry[reference] = entry;
                }
            }
        }
    }
}
