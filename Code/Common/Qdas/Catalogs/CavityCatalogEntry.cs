﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class CavityCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<CavityCatalogEntry> _kMappings;
        static CavityCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<CavityCatalogEntry>();
        }

        [Required]
        [QdasMapping(4252, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Number { get; set; }

        [Required]
        [QdasMapping(4253, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name { get; set; }

        [QdasMapping(4751, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMapping(4752, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
