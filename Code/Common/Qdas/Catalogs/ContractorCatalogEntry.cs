﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class ContractorCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<ContractorCatalogEntry> _kMappings;
        static ContractorCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<ContractorCatalogEntry>();
        }
        [Required]
        [QdasMapping(4102, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string ContractorNumber { get; set; }
        [Required]
        [QdasMapping(4103, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string ContractorName { get; set; }

        [QdasMapping(4601, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMapping(4602, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string ContractorRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
