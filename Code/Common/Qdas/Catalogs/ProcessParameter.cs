﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class ProcessParameter : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<ProcessParameter> _kMappings;
        static ProcessParameter()
        {
            _kMappings = new QDasMappingAttributeAccess<ProcessParameter>();
        }

        [Required]
        [QdasMapping(4242, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Number { get; set; }
        [Required]
        [QdasMapping(4243, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name { get; set; }
        [QdasMapping(4244, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string ShortText { get; set; }

        [QdasMapping(4741, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMapping(4742, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <summary>
        /// Static version of <see cref="GetDefinedKNumbers"/>
        /// </summary>
        /// <returns>Set of defined k-numbers on the type</returns>
        public static ISet<short> GetDefinedKNumbersStatic()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
