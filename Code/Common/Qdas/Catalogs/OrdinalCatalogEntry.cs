﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class OrdinalCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<OrdinalCatalogEntry> _kMappings;
        static OrdinalCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<OrdinalCatalogEntry>();
        }

        [Required]
        [QdasMapping(4232, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Number { get; set; }

        [Required]
        [QdasMapping(4233, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string Description { get; set; }

        [QdasMapping(4234, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Evaluation { get; set; }

        [QdasMapping(4235, QdasFieldType.I10)]
        public int? Rank { get; set; }

        [QdasMapping(4236, QdasFieldType.I5)]
        public short? IO { get; set; }

        [QdasMapping(4237, QdasFieldType.I5)]
        public short? Validity { get; set; }

        [QdasMapping(4731, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMapping(4732, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
