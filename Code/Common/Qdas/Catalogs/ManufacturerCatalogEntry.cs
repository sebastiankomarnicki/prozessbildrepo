﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class ManufacturerCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<ManufacturerCatalogEntry> _kMappings;
        static ManufacturerCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<ManufacturerCatalogEntry>();
        }

        [Required]
        [QdasMapping(4012, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string ManufacturerNumber { get; set; }

        [Required]
        [QdasMapping(4013, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string ManufacturerName1 { get; set; }

        [QdasMapping(4014, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string ManufacturerName2 { get; set; }

        [QdasMapping(4015, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string ManufacturerDepartment { get; set; }

        [QdasMapping(4016, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string ManufacturerPlant { get; set; }

        [QdasMapping(4017, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string ManufacturerStreet { get; set; }

        [QdasMapping(4018, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string ManufacturerZipCity { get; set; }

        [QdasMapping(4019, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string ManufacturerCountry { get; set; }

        [QdasMapping(4511, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMappingAttribute(4512, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string ManufacturerRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
