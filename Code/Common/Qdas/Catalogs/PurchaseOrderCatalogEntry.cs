﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class PurchaseOrderCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<PurchaseOrderCatalogEntry> _kMappings;
        static PurchaseOrderCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<PurchaseOrderCatalogEntry>();
        }

        [Required]
        [QdasMapping(4032, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string PurchaseOrderNumber { get; set; }

        [Required]
        [QdasMapping(4033, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string PurchaseOrderName { get; set; }

        [QdasMapping(4531, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMapping(4532, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string PurchaseOrderRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
