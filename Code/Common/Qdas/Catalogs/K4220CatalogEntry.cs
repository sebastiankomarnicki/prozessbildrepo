﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class K4220CatalogEntry: ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<K4220CatalogEntry> _kMappings;
        static K4220CatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<K4220CatalogEntry>();
        }

        [Required]
        [QdasMapping(4222, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Number { get; set; }

        [Required]
        [QdasMapping(4223, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name { get; set; }

        [QdasMapping(4721, QdasFieldType.I3)]
        public byte? SpecialIdentification { get; set; }

        [QdasMapping(4722, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
