﻿using Promess.Common.QDas.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Promess.Common.QDas.Catalogs
{
    /// <summary>
    /// Base functionality for the different catalogs grouped in the event catalog, which are causes, events and measures.
    /// </summary>
    public abstract class K4220BaseCatalog:IMainCatalog
    {
        private  short _mainCatalogKNumber;
        protected short _eventToSubcatalogKNumber;
        protected int _catalogNameLength;
        private string _name;
        protected Dictionary<int, string> _subCatalogRefToName;
        protected Dictionary<int, HashSet<int>> _subCatalogRefToEntryRefs;
        protected Dictionary<int, K4220CatalogEntry> _entryRefToCatalogEntry;
        private ISet<short> _definedKNumbers;

        public K4220BaseCatalog()
        {
            _name = "";
            _mainCatalogKNumber = 4220;
            _catalogNameLength = 20;
            _eventToSubcatalogKNumber = 4221;
            _subCatalogRefToName = new Dictionary<int, string>();
            _subCatalogRefToEntryRefs = new Dictionary<int, HashSet<int>>();
            _entryRefToCatalogEntry = new Dictionary<int, K4220CatalogEntry>();
        }

        /// <inheritdoc/>
        public short MainCatalogKNumber
        {
            get { return _mainCatalogKNumber; }
            protected set { _mainCatalogKNumber = value; }
        }

        /// <inheritdoc/>
        public string MainCatalogName
        {
            get { return _name; }
            protected set { _name = value; }
        }

        /// <inheritdoc/>
        public abstract void AddData(short kNumber, int reference, string data);

        /// <inheritdoc/>
        public bool ContainsData()
        {
            return _subCatalogRefToEntryRefs.Count > 0 || _subCatalogRefToName.Count > 0;
        }

        /// <inheritdoc/>
        public IEnumerable<Tuple<int, ISupportKNumberDataManipulation>> GetCatalogEntriesAndQdasKeys()
        {
            foreach (var entry in _entryRefToCatalogEntry)
            {
                yield return new Tuple<int, ISupportKNumberDataManipulation>(entry.Key, entry.Value);
            }
        }

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            if (_definedKNumbers == null)
            {
                K4220CatalogEntry instance = new K4220CatalogEntry();
                ISet<short> definedKNumbers = instance.GetDefinedKNumbers();
                definedKNumbers.Add(_mainCatalogKNumber);
                definedKNumbers.Add(_eventToSubcatalogKNumber);
                this._definedKNumbers = definedKNumbers;
            }
            return _definedKNumbers;
        }

        /// <inheritdoc/>
        public IEnumerable<int> GetQdasKeysInSubcatalog(int subcatalogKey)
        {
            return _subCatalogRefToEntryRefs[subcatalogKey];
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbersOfEntries()
        {
            return KNumberQdasUtil.GetRequiredKNumbers<K4220CatalogEntry>();
        }

        /// <inheritdoc/>
        public IEnumerable<Tuple<int, string>> GetSubcatalogsAndQdasKeys()
        {
            foreach (var entry in _subCatalogRefToName)
            {
                yield return new Tuple<int, string>(entry.Key, entry.Value);
            }
        }

        /// <inheritdoc/>
        public bool IsConsistent()
        {
            var keysOfAllEntries = new HashSet<int>(_entryRefToCatalogEntry.Keys);
            var referencesSubcatalogs = new HashSet<int>(_subCatalogRefToName.Keys);
            var subcatalogsWithReferences = new HashSet<int>(_subCatalogRefToEntryRefs.Keys);

            if (!subcatalogsWithReferences.IsSubsetOf(referencesSubcatalogs))
                return false;

            foreach (var subcatEntries in _subCatalogRefToEntryRefs)
            {
                if (!subcatEntries.Value.IsSubsetOf(keysOfAllEntries))
                    return false;
            }
            return true;
        }

        /// <inheritdoc/>
        public Tuple<ICollection<int>, ICollection<int>> RemoveSubcatalogsAndEntriesWORequiredValues()
        {
            List<int> removedSubcatalogReferences = new List<int>();
            List<int> removedEntryReferences = new List<int>();
            foreach (var subcatEntry in _subCatalogRefToName)
            {
                if (String.IsNullOrEmpty(subcatEntry.Value))
                    removedSubcatalogReferences.Add(subcatEntry.Key);
            }
            foreach (var key in removedSubcatalogReferences)
            {
                _subCatalogRefToName.Remove(key);
            }
            ICollection<short> requiredKNumbers = GetRequiredKNumbersOfEntries();
            foreach (var eventEntry in _entryRefToCatalogEntry)
            {
                object value = null;
                String convertedValue;
                foreach (var requiredKNumber in requiredKNumbers)
                {
                    if (!eventEntry.Value.TryGetField(requiredKNumber, out value))
                    {
                        throw new NotImplementedException($"associated knumber not found {requiredKNumber} {typeof(K4220CatalogEntry)} .\n please contact promess.");
                    }
                    else
                    {
                        Debug.Assert(value == null || value.GetType().Equals(typeof(String)),
                            "the cast is supposed to fail if there ever is a catalog entry with a non string required field"
                            );
                        convertedValue = (String)value;
                        if (String.IsNullOrEmpty(convertedValue))
                        {
                            removedEntryReferences.Add(eventEntry.Key);
                            break;
                        }
                    }
                }
            }
            foreach (var key in removedEntryReferences)
            {
                _entryRefToCatalogEntry.Remove(key);
            }
            return new Tuple<ICollection<int>, ICollection<int>>(removedSubcatalogReferences, removedEntryReferences);
        }
    }
}
