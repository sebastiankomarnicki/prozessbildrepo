﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class UnitCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<UnitCatalogEntry> _kMappings;
        static UnitCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<UnitCatalogEntry>();
        }

        [Required]
        [QdasMapping(4082, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string UnitNumber { get; set; }

        [Required]
        [QdasMapping(4083, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string UnitName { get; set; }

        [QdasMapping(4581, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMapping(4582, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string UnitRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }

        #endregion
        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
