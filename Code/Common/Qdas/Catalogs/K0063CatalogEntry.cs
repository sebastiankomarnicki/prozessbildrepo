﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class K0063CatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<K0063CatalogEntry> _kMappings;
        static K0063CatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<K0063CatalogEntry>();
        }

        [Required]
        [QdasMapping(4292, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Number { get; set; }

        [Required]
        [QdasMapping(4293, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name { get; set; }

        [QdasMapping(4791, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMapping(4792, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
