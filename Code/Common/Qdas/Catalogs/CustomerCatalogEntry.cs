﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class CustomerCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<CustomerCatalogEntry> _kMappings;
        static CustomerCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<CustomerCatalogEntry>();
        }

        [Required]
        [QdasMappingAttribute(4002, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string CustomerNumber { get; set; }
        [Required]
        [QdasMappingAttribute(4003, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string CustomerName1 { get; set; }
        [QdasMappingAttribute(4004, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string CustomerName2 { get; set; }
        [QdasMappingAttribute(4005, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string CustomerDepartment { get; set; }
        [QdasMappingAttribute(4006, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string CustomerPlant { get; set; }
        [QdasMappingAttribute(4007, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string CustomerStreet { get; set; }
        [QdasMappingAttribute(4008, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string CustomerZipCity { get; set; }
        [QdasMappingAttribute(4009, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string CustomerCountry { get; set; }
        [QdasMapping(4501, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMappingAttribute(4502, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string CustomerRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
