﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class K0062CatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<K0062CatalogEntry> _kMappings;
        static K0062CatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<K0062CatalogEntry>();
        }

        [Required]
        [QdasMapping(4282, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Number { get; set; }
        [Required]
        [QdasMapping(4283, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name { get; set; }

        [QdasMapping(4781, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMapping(4782, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
