﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class ProductCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<ProductCatalogEntry> _kMappings;
        static ProductCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<ProductCatalogEntry>();
        }

        [Required]
        [QdasMapping(4112, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string ProductNumber { get; set; }
        [Required]
        [QdasMapping(4113, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string ProductName { get; set; }

        [QdasMapping(4611, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMapping(4612, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string ProductRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
