﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class SupplierCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<SupplierCatalogEntry> _kMappings;
        static SupplierCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<SupplierCatalogEntry>();
        }

        [Required]
        [QdasMapping(4022, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string SupplierNumber { get; set; }

        [Required]
        [QdasMapping(4023, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string SupplierName1 { get; set; }

        [QdasMapping(4024, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string SupplierName2 { get; set; }

        [QdasMapping(4025, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string SupplierDepartment { get; set; }

        [QdasMapping(4026, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string SupplierPlant { get; set; }

        [QdasMapping(4027, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string SupplierStreet { get; set; }

        [QdasMapping(4028, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string SupplierZipCity { get; set; }

        [QdasMapping(4029, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string SupplierCountry { get; set; }

        [QdasMapping(4521, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMapping(4522, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string SupplierRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
