﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class OperatorCatalogEntry: ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<OperatorCatalogEntry> _kMappings;
        static OperatorCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<OperatorCatalogEntry>();
        }

        [Required]
        [QdasMapping(4092, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Name1 { get; set; }
        [Required]
        [QdasMapping(4093, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name2 { get; set; }
        [QdasMapping(4094, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string Department { get; set; }
        [QdasMapping(4095, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string TelephoneNumber { get; set; }
        [QdasMapping(4096, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string FaxNumber { get; set; }
        [QdasMapping(4097, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string EMail { get; set; }
        [QdasMapping(4098, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string Position { get; set; }
        [QdasMapping(4099, QdasFieldType.A, QdasSpecialConversion.A15)]
        public string Title { get; set; }
        [QdasMapping(4591, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMapping(4592, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
