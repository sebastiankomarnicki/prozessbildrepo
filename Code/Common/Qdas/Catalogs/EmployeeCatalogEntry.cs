﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class EmployeeCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<EmployeeCatalogEntry> _kMappings;
        static EmployeeCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<EmployeeCatalogEntry>();
        }

        [Required]
        [QdasMapping(4122, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string Name1 { get; set; }
        [Required]
        [QdasMapping(4123, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string Name2 { get; set; }
        [QdasMapping(4124, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string Department { get; set; }
        [QdasMapping(4125, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string TelephoneNumber { get; set; }
        [QdasMapping(4126, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string FaxNumber { get; set; }
        [QdasMapping(4127, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string EMailAddress { get; set; }
        [QdasMapping(4128, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string Position { get; set; }
        [QdasMapping(4129, QdasFieldType.A, QdasSpecialConversion.A15)]
        public string Title { get; set; }

        [QdasMapping(4621, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;
        [QdasMapping(4622, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string Remark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
