﻿using Promess.Common.QDas.Util;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Promess.Common.QDas.Catalogs
{
    public class DrawingCatalogEntry : ISupportKNumberDataManipulation, ISupportKNumberListing, IRequiredKNumbers
    {
        private static QDasMappingAttributeAccess<DrawingCatalogEntry> _kMappings;
        static DrawingCatalogEntry()
        {
            _kMappings = new QDasMappingAttributeAccess<DrawingCatalogEntry>();
        }

        [Required]
        [QdasMapping(4052, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string DrawingNumber { get; set; }

        [Required]
        [QdasMapping(4053, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string DrawingModification { get; set; }

        [QdasMapping(4551, QdasFieldType.I3)]
        public byte OutOfUse { get; set; } = 0;

        [QdasMapping(4552, QdasFieldType.A, QdasSpecialConversion.A200)]
        public string DrawingRemark { get; set; }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <inheritdoc/>
        public ICollection<short> GetRequiredKNumbers()
        {
            return KNumberQdasUtil.GetRequiredKNumbers(this);
        }
    }
}
