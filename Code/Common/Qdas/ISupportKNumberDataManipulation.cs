namespace Promess.Common.QDas
{
    /// <summary>
    /// Interface for supporting knumber data manipulation
    /// </summary>
    public interface ISupportKNumberDataManipulation
    {
        /// <summary>
        /// Try to retrieve the name of the property for the given k-number
        /// </summary>
        /// <param name="kNumber">Target k-number</param>
        /// <param name="propertyName">Out parameter for the name of the property</param>
        /// <returns>Retrieval success</returns>
        bool TryGetPropertyName(short kNumber, out string propertyName);
        /// <summary>
        /// Try to set the item for the given k-number to a value
        /// </summary>
        /// <param name="knumber">Target k-number</param>
        /// <param name="value">Value to set</param>
        /// <returns>Set success</returns>
        bool TrySetField(short knumber, object value);
        /// <summary>
        /// Try to get the value of an item identified by the given k-number
        /// </summary>
        /// <param name="knumber">Target k-number</param>
        /// <param name="value">Out parameter for the retrieved value</param>
        /// <returns>Retrieval success</returns>
        bool TryGetField(short knumber, out object value);
    }
}
