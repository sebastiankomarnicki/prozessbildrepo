﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Helper to convert AQDEF value entries
    /// </summary>
    public static class QdasValueConversions
    {
        private static Dictionary<QdasFieldType,Type> _qdasTypeToCSTypeMappings;
        static QdasValueConversions()
        {
            _qdasTypeToCSTypeMappings = new Dictionary<QdasFieldType, Type>
            {
                { QdasFieldType.I3, typeof(byte?) },
                { QdasFieldType.I5, typeof(short?) },
                { QdasFieldType.I10, typeof(int?) },
                { QdasFieldType.F, typeof(double?) },
                { QdasFieldType.D, typeof(DateTime?) },
                { QdasFieldType.A, typeof(string) },
                { QdasFieldType.S, typeof(object) }
            };
            Debug.Assert(Enum.GetNames(typeof(QdasFieldType)).Length == _qdasTypeToCSTypeMappings.Count);
        }

        /// <summary>
        /// Tries to Convert the given value to the QdasFieldType.
        /// Special Conversion may only have a different value for
        /// QdasFieldType S or A
        /// Will throw an exception if conversion is not possible
        /// </summary>
        /// <param name="value">The value that should be converted</param>
        /// <param name="qdasTargetType">The target type for the conversion, unless it is QdasFieldType.S</param>
        /// <param name="specialConversion">Only allowed if QdasFieldType A or S is given. For A it limits the size and for S it determines the final type</param>
        /// <returns></returns>
        public static object Convert(object value, QdasFieldType qdasTargetType, QdasSpecialConversion specialConversion = QdasSpecialConversion.None)
        {
            Debug.Assert(
                (qdasTargetType == QdasFieldType.S || qdasTargetType == QdasFieldType.A) && specialConversion != QdasSpecialConversion.None ||
                qdasTargetType != QdasFieldType.S && specialConversion == QdasSpecialConversion.None,
                "Qdas field type S and A must have a special conversion value");
            switch (qdasTargetType)
            {
                case QdasFieldType.A:
                    if (value.GetType().Equals(typeof(DateTime)))
                    {
                        string tmpVal = DateTimeToDbDateString((DateTime)value);
                        return LimitString(tmpVal, specialConversion);
                    }
                    return LimitString((string)ConvertToCType(typeof(string),value), specialConversion);
                    //return LimitString(value.ToString(), specialConversion);
                case QdasFieldType.D:
                    return ConvertToDateTime(value);
                case QdasFieldType.S:
                    return ProcessFieldTypeS(value, specialConversion);
                case QdasFieldType.F:
                    if (value is string)
                        value = ((string)value).Replace(",", ".");
                    return ConvertToCType(_qdasTypeToCSTypeMappings[qdasTargetType], value);
                default:
                    return ConvertToCType(_qdasTypeToCSTypeMappings[qdasTargetType], value);
            }
        }

        private static object ProcessFieldTypeS(object value, QdasSpecialConversion specialConversion)
        {
            string valueAsString;
            switch (specialConversion)
            {
                case QdasSpecialConversion.ProcParamList:
                    valueAsString = value as string;
                    if (valueAsString == null)
                        throw new FormatException();
                    return TS_Prozessparameter.Parse(valueAsString);
                case QdasSpecialConversion.ChartTypeLocation:
                    valueAsString = value as string;
                    if (valueAsString == null)
                        throw new FormatException();
                    return QdasChartTypeLocation.Parse(valueAsString);
                case QdasSpecialConversion.ChartTypeVariation:
                    valueAsString = value as string;
                    if (valueAsString == null)
                        throw new FormatException();
                    return QdasChartTypeVariation.Parse(valueAsString);
                case QdasSpecialConversion.I5Collection:
                    if (value.GetType() is IList<short>){
                        return ((IList<short>)value).ToList();
                    }
                    valueAsString = value as string;
                    if (valueAsString == null)
                        throw new FormatException();
                    return QdasI5Collection.Parse(valueAsString);
                default:
                    throw new NotImplementedException();
            }
        }

        private static String LimitString(string toLimit, QdasSpecialConversion specialConversion)
        {
            int limit;
            switch (specialConversion)
            {
                case QdasSpecialConversion.A1:
                    limit = 1;break;
                case QdasSpecialConversion.A2:
                    limit = 2;break;
                case QdasSpecialConversion.A10:
                    limit = 10;break;
                case QdasSpecialConversion.A12:
                    limit = 10; break;
                case QdasSpecialConversion.A14:
                    limit = 14; break;
                case QdasSpecialConversion.A15:
                    limit = 15; break;
                case QdasSpecialConversion.A20:
                    limit = 20;break;
                case QdasSpecialConversion.A24:
                    limit = 24;break;
                case QdasSpecialConversion.A30:
                    limit = 30;break;
                case QdasSpecialConversion.A40:
                    limit = 40;break;
                case QdasSpecialConversion.A50:
                    limit = 50;break;
                case QdasSpecialConversion.A60:
                    limit = 60; break;
                case QdasSpecialConversion.A64:
                    limit = 64; break;
                case QdasSpecialConversion.A80:
                    limit = 80; break;
                case QdasSpecialConversion.A200:
                    limit = 200; break;
                case QdasSpecialConversion.A254:
                    limit = 254; break;
                case QdasSpecialConversion.A255:
                    limit = 255;break;
                case QdasSpecialConversion.AMax:
                    limit = int.MaxValue;
                    break;
                default:
                    throw new NotImplementedException();
            }
            if (toLimit.Length>limit)
            {
                //if a warning should be emitted, then the right place is here
                return toLimit.Substring(0, limit);
            }
            return toLimit;
        }

        private static object ConvertToDateTime(object value)
        {
            if (value is String)
            {
                return QdasDateStringToDateTime((string)value);
            }else
            {
                return ConvertToCType(typeof(DateTime), value);
            }
        }

        private static object ConvertToCType(Type targetType, object value)
        {
            if (value is null)
            {
                if (!targetType.IsValueType || (Nullable.GetUnderlyingType(targetType) != null))
                    return value;
                throw new InvalidOperationException($"Cannot convert {null} for {targetType}");
            }

            Type valueType = value.GetType();
            if (valueType.Equals(targetType))
                return value;
            Debug.Assert(valueType.IsPrimitive || valueType == typeof(string),
                "Only primitive/immutable objects can be converted, due to the case where source and target type are equal and the source value should be returned");
            TypeConverter converter = TypeDescriptor.GetConverter(targetType);

            TypeConverter converter2 = TypeDescriptor.GetConverter(valueType);
            // determine if the supplied value is of a suitable type
            if (converter.CanConvertFrom(valueType))
                {
                // return the converted value
                return converter.ConvertFrom(null,CultureInfo.InvariantCulture, value);
            }else if (converter2.CanConvertTo(targetType))
            {
                return converter2.ConvertTo(null, CultureInfo.InvariantCulture, value, targetType);
            }
            else
            {
                // try to convert from the string representation
                return converter.ConvertFromInvariantString(value.ToString());
            }
        }

        public static DateTime QdasDateStringToDateTime(string qdasString)
        {
            if (ParseQdasDate(qdasString.Trim(), out int year, out int month, out int day, out int hour, out int minute, out int second))
            {
                return new DateTime(year, month, day, hour, minute, second);
            }
            else
            {
                return new DateTime(1970, 1, 1, 0, 0, 0);
            }
        }

        public static String DateTimeToDbDateString(DateTime? dt)
        {
            const string formatString = "dd.MM.yyyy HH:mm:ss";

            if (!dt.HasValue)
                return "";

            return dt.Value.ToString(formatString, CultureInfo.InvariantCulture);
        }

        public static DateTime? DbDateStringToDateTime(string value)
        {
            const string formatString = "dd.MM.yyyy HH:mm:ss";
            DateTime tmp = DateTime.UtcNow;
            DateTime? result = null;
            if (DateTime.TryParseExact(value, formatString, CultureInfo.InvariantCulture, DateTimeStyles.None, out tmp))
            {
                result = tmp;
            }
            return result;
        }

        /// <summary>
        /// Parses the given date string into year, month, day, hour, minute.
        /// The date that can be assembled from these values might not be valid.
        /// </summary>
        /// <param name="qdasString">Date string in dqdas or old dbimport format</param>
        /// <param name="year">Encoded year</param>
        /// <param name="month">Encoded month</param>
        /// <param name="day">Encoded day</param>
        /// <param name="hour">Encoded hour</param>
        /// <param name="minute">Encoded minute</param>
        /// <param name="second"></param>
        /// <returns>True, if the format is correct and values can be parsed. 
        /// False, if the format is not recognized. Throws an exception when numbers cannot be parsed. </returns>
        private static bool ParseQdasDate(string qdasString, out int year, out int month, out int day, out int hour, out int minute, out int second)
        {
            year = month = day = hour = minute = second = 0;
            if (!FindQdasSeparatorIndex(qdasString, out int qdasSeparatorIndex))
                return false;
            string datePart = qdasString.Substring(0, qdasSeparatorIndex).Trim();
            string timePart = qdasString.Substring(qdasSeparatorIndex + 1).Trim();
            char[] separators = { '.', '/', '-' };
            string[] date = null;
            string[] time;
            bool found = false;
            bool pm = false;
            string tmp;
            foreach (char separator in separators)
            {
                if (datePart.Contains(separator))
                {
                    found = true;
                    date = datePart.Split(separator);
                    if (date.Length != 3)
                    {
                        return false;
                    }
                    for (int i = 0; i < date.Length; i++)
                    {
                        date[i] = date[i].Trim();
                    }
                    switch (separator) { 
                        case '/':
                            tmp = date[0];
                            date[0] = date[1];
                            date[1] = tmp;
                            break;
                        case '-':
                            tmp = date[0];
                            date[0] = date[2];
                            date[2] = tmp;
                            break;
                    }
                    break;
                }
            }

            if (!found)
                return false;
            year = int.Parse(date[2]);
            month = int.Parse(date[1]);
            day = int.Parse(date[0]);
            //this is different from original processing
            if (year < 0 || year > 9999 || month < 1 || month > 12 || day < 1 || day > 32)
                return false;
            if (date[2].Length <= 2)
            {
                if (year < 60)
                    year += 2000;
                else
                    year += 1900;
            }
            time = timePart.Split(':');
            if (time.Length > 3)
            {
                return false;
            }
            for (int i = 0; i < time.Length; i++)
            {
                time[i] = time[i].Trim();
            }
            tmp = time[time.Length - 1];
            if (tmp.EndsWith("a", StringComparison.OrdinalIgnoreCase))
            {
                tmp = tmp.Substring(0, tmp.Length - 1);
            }else if (tmp.EndsWith("am", StringComparison.OrdinalIgnoreCase))
            {
                tmp = tmp.Substring(0, tmp.Length - 2);
            }else if (tmp.EndsWith("p", StringComparison.OrdinalIgnoreCase))
            {
                tmp = tmp.Substring(0, tmp.Length - 1);
                pm = true;
            }else if (tmp.EndsWith("pm", StringComparison.OrdinalIgnoreCase))
            {
                tmp = tmp.Substring(0, tmp.Length - 2);
                pm = true;
            }
            time[time.Length - 1] = tmp;
            hour = int.Parse(time[0]);
            if (pm)
                hour += 12;
            minute = time.Length >= 2 ? int.Parse(time[1]) : 0;
            second = time.Length == 3 ? int.Parse(time[2]) : 0;
            if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59)
                return false;
            return true;
        }

        /// <summary>
        /// Finds the index of the separator char of the qdas/old db import date.
        /// For qdas date this is '/', otherwise ' '
        /// </summary>
        /// <param name="qdasString">The date string to search</param>
        /// <param name="separatorIndex">If found, the index. Then it is always greater or equal to one</param>
        /// <returns>true, if a separator could be found and was not at index zero</returns>
        private static bool FindQdasSeparatorIndex(string qdasString, out int separatorIndex)
        {
            int numberOfSlashes = qdasString.Count(elem => elem == '/');
            if (numberOfSlashes == 1 || numberOfSlashes == 3)
            {
                separatorIndex = qdasString.LastIndexOf('/');
            }
            else
            {
                separatorIndex = qdasString.IndexOf(' ');
            }
            if (separatorIndex <= 0)
                return false;
            return true;
        }
    }
}
