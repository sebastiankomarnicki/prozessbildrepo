﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Container for AQDEF measurement data
    /// </summary>
    public class DfxData
    {
        public IReadOnlyList<TS_MERKMAL_IST[]> Data { get; private set; }
        public IReadOnlyList<DfxLine> LineData { get; private set; }

        /// <summary>
        /// Create container
        /// </summary>
        /// <param name="data">Processed data as list of arrays of the characteristics data</param>
        /// <param name="lineData">List of the corresponding lines</param>
        public DfxData(IReadOnlyList<TS_MERKMAL_IST[]> data, IReadOnlyList<DfxLine> lineData)
        {
            Debug.Assert(data != null && lineData != null);
            Debug.Assert(data.Count == lineData.Count);
            this.Data = data;
            this.LineData = lineData;
        }
    }
}
