﻿namespace Promess.Common.QDas
{
    public class TS_Merkmal_Tmp
    {
        /// <summary>
        /// Next index for this characteristics data
        /// </summary>
        public int NextIndex { get; set; } = -1;
        /// <summary>
        /// Upper limit for values according to specification
        /// </summary>
        public double OberesLimit { get; set; } = 0;
        /// <summary>
        /// Lower limit for values according to specifications
        /// </summary>
        public double UnteresLimit { get; set; } = 0;
        /// <summary>
        /// Id of the characteristic
        /// </summary>
        public short CharacteristicId { get; set; } = -1;
        /// <summary>
        /// characteristic type, i.e. AQDEF values for attributive of variable
        /// </summary>
        public short iMerkmal_Art { get; set; }

        /// <summary>
        /// Check if the given value should be filetered because it is outside the limits
        /// </summary>
        /// <param name="measuringValue">Value to check</param>
        /// <returns>If value should be filtered or not</returns>
        public bool IsMeasuringValueFiltered(double measuringValue)
        {
            return measuringValue > OberesLimit || measuringValue < UnteresLimit;
        }
    }
}
