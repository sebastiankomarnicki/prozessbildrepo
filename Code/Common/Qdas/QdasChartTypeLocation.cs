﻿using System.Text;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Data container and parser for K8010 with custom data format
    /// </summary>
    public class QdasChartTypeLocation
    {
        public short iLage_Kartenart { get; set; }
        
        public short? iLage_Streuungsschaetzer { get; set; }
        
        public short? iLage_Erw_Grenzen { get; set; }
        
        public short? iLage_Pearsonberechnung { get; set; }

        public static QdasChartTypeLocation Parse(string encoding)
        {
            QdasChartTypeLocation lage = new QdasChartTypeLocation();
            string[] asWerte = encoding.TrimStart().Split(' ');
            lage.iLage_Kartenart = short.Parse(asWerte[0]);
            if (asWerte.Length >= 2)
            {
                lage.iLage_Streuungsschaetzer = short.Parse(asWerte[1]);
            }
            if (asWerte.Length >= 3)
            {
                lage.iLage_Erw_Grenzen = short.Parse(asWerte[2]);
            }
            if (asWerte.Length >= 4)
            {
                lage.iLage_Pearsonberechnung = short.Parse(asWerte[3]);
            }
            //TODO other values are not imported into this data structure
            return lage;
        }

        /// <summary>
        /// Create string representation of data
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(iLage_Kartenart);
            short?[] values = { iLage_Streuungsschaetzer, iLage_Erw_Grenzen, iLage_Pearsonberechnung };
            foreach (var possibleEntry in values)
            {
                if (possibleEntry.HasValue)
                {
                    sb.AppendFormat(" {0}", possibleEntry.Value);
                }
                else
                {
                    break;
                }
            }
            return sb.ToString();
        }
    }
}
