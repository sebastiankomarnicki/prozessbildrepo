﻿namespace Promess.Common.QDas
{
    /// <summary>
    /// Field types according to AQDEF
    /// </summary>
    public enum QdasFieldType
    {
        I3,
        I5,
        I10,
        F,
        D,
        A,
        S
    }
}
