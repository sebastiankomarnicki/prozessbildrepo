﻿using Promess.Common.QDas.Catalogs;
using System;
using System.Collections.Generic;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Container for part data
    /// </summary>
    public class TS_TEILEDATEN : ISupportKNumberDataManipulation, ISupportKNumberListing
    {
        private static QDasMappingAttributeAccess<TS_TEILEDATEN> _kMappings;

        private List<QdasLineSplitResult> _unrecognizedKeyData;

        static TS_TEILEDATEN()
        {
            _kMappings = new QDasMappingAttributeAccess<TS_TEILEDATEN>();
        }

        public TS_TEILEDATEN()
        {
            InitializeMainCatalogs();
            ProcessParameter = new MainCatalogProcessParameter();
            K4220Catalog = new K4220Catalogs();

            _unrecognizedKeyData = new List<QdasLineSplitResult>();
        }

        private void InitializeMainCatalogs()
        {
            HashSet<IMainCatalog> mainCatalogs = new HashSet<IMainCatalog>()
            {
                new MainCatalog<CustomerCatalogEntry>(4000,80,4001),
                new MainCatalog<ManufacturerCatalogEntry>(4010,80,4011),
                new MainCatalog<SupplierCatalogEntry>(4020,80,4021),
                new MainCatalog<PurchaseOrderCatalogEntry>(4030,80,4031),
                new MainCatalog<MaterialCatalogEntry>(4040,80,4041),
                new MainCatalog<DrawingCatalogEntry>(4050,80,4051),
                new MainCatalog<MachineCatalogEntry>(4060,80,4061),
                new MainCatalog<UnitCatalogEntry>(4080,80,4081),
                new MainCatalog<ContractorCatalogEntry>(4100,80,4101),
                new MainCatalog<ProductCatalogEntry>(4110,80,4111),
                new MainCatalog<EmployeeCatalogEntry>(4120,80,4121),
                new MainCatalog<OrdinalCatalogEntry>(4230,50,4231),
                new MainCatalog<K0061CatalogEntry>(4270,80,4271),
                new MainCatalog<K0062CatalogEntry>(4280,80,4281),
                new MainCatalog<K0063CatalogEntry>(4290,80,4291)
            };

            Dictionary<int, IMainCatalog> kNumberToMainCatalogLookup = new Dictionary<int, IMainCatalog>();
            foreach (var catalog in mainCatalogs)
            {
                foreach (var kNumber in catalog.GetDefinedKNumbers())
                {
                    kNumberToMainCatalogLookup[kNumber] = catalog;
                }
            }

            this.CavityCatalog = new MainCatalog<CavityCatalogEntry>(4250, 80, 4251);
            this.GageCatalog = new MainCatalog<GageCatalogEntry>(4070, 80, 4071);
            this.OperatorCatalog = new MainCatalog<OperatorCatalogEntry>(4090, 80, 4091);

            foreach (var catalog in new IMainCatalog[] { this.CavityCatalog, this.GageCatalog, this.OperatorCatalog })
            {
                foreach (var kNumber in catalog.GetDefinedKNumbers())
                {
                    kNumberToMainCatalogLookup[kNumber] = catalog;
                }
            }

            this.MainCatalogs = mainCatalogs;
            this.KNumberToMainCatalogLookup = kNumberToMainCatalogLookup;
        }

        #region properties
        [QdasMapping(1001, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sTeil_Nummer { get; set; }
        [QdasMapping(1002, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string sTeil_Bezeichnung { get; set; }
        [QdasMapping(1003, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sTeil_Bezeichnung_kurz { get; set; }
        [QdasMapping(1004, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sTeil_Aenderungsstand { get; set; }
        [QdasMapping(1005, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sErzeugnis { get; set; }
        [QdasMapping(1007, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sTeil_Nummer_kurz { get; set; }
        [QdasMapping(1008, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sTeiletyp { get; set; }
        [QdasMapping(1009, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sTeilecode { get; set; }
        [QdasMapping(1010, QdasFieldType.I3)]
        public short? iDoku_Pflicht { get; set; }
        [QdasMapping(1011, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sVariante { get; set; }
        [QdasMapping(1012, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sSachnummer_Zusatz { get; set; }
        [QdasMapping(1013, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sSachnummer_Index { get; set; }
        [QdasMapping(1014, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sTeileident { get; set; }
        [QdasMapping(1015, QdasFieldType.I3)]
        public short? iUntersuchungsart { get; set; }
        [QdasMapping(1016, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sZusammenbauteil { get; set; }
        [QdasMapping(1017, QdasFieldType.I3)]
        public short? iPrueplan_gesperrt { get; set; }

        [QdasMapping(1020, QdasFieldType.I5)]
        public short? iHersteller_Katalog { get; set; }
        public short? FileManufactutrerCatalog { get; set; }

        [QdasMapping(1021, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sHersteller_Nummer_Text { get; set; }
        [QdasMapping(1022, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string sHersteller_Name { get; set; }
        [QdasMapping(1023, QdasFieldType.I10)]
        public int? ManufacturerNumber { get; set; }

        [QdasMapping(1030, QdasFieldType.I5)]
        public short? iWerkstoff_Katalog { get; set; }
        public short? FileMaterialCatalog { get; set; }

        [QdasMapping(1031, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sWerkstoff_Nummer_Text { get; set; }
        [QdasMapping(1032, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sWerkstoff_Bezeichnung { get; set; }
        [QdasMapping(1033, QdasFieldType.I10)]
        public int? MaterialNumber { get; set; }

        [QdasMapping(1040, QdasFieldType.I5)]
        public short? iZeichnung_Katalog { get; set; }
        public short? FileDrawingCatalog { get; set; }

        [QdasMapping(1041, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sZeichnung_Nummer_Text { get; set; }
        [QdasMapping(1042, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sZeichnung_Aenderung { get; set; }
        [QdasMapping(1043, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sZeichnung_Index { get; set; }
        [QdasMapping(1044, QdasFieldType.I10)]
        public int? DrawingNumber { get; set; }
        [QdasMapping(1045, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sZeichnung_Gueltigkeitsdatum { get; set; }
        [QdasMapping(1046, QdasFieldType.A, QdasSpecialConversion.A60)]
        public string sZeichnung_Name { get; set; }
        [QdasMapping(1047, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sGrundzeichnung_Nummer_Text { get; set; }
        [QdasMapping(1048, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string CadDrawingFile { get; set; }

        [QdasMapping(1050, QdasFieldType.I5)]
        public short? ContractorCatalog { get; set; }
        public short? FileContractorCatalog { get; set; }

        [QdasMapping(1051, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sAuftraggeber_Nummer_Text { get; set; }
        [QdasMapping(1052, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sAuftraggeber_Bezeichnung { get; set; }
        [QdasMapping(1053, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sAuftrag { get; set; }
        [QdasMapping(1054, QdasFieldType.I10)]
        public int? ContractorNumber { get; set; }

        [QdasMapping(1060, QdasFieldType.I5)]
        public short? CustomerCatalog { get; set; }
        public short? FileCustomerCatalog { get; set; }

        [QdasMapping(1061, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sKunde_Nummer_Text { get; set; }
        [QdasMapping(1062, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sKunde_Bezeichnung { get; set; }
        [QdasMapping(1063, QdasFieldType.I10)]
        public int? CustomerNumber { get; set; }

        [QdasMapping(1070, QdasFieldType.I5)]
        public short? SupplierCatalog { get; set; }
        public short? FileSupplierCatalog { get; set; }
        [QdasMapping(1071, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sLieferant_Nummer_Text { get; set; }
        [QdasMapping(1072, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sLieferant_Bezeichnung { get; set; }
        [QdasMapping(1073, QdasFieldType.I10)]
        public int? SupplierNumber { get; set; }

        [QdasMapping(1081, QdasFieldType.A, QdasSpecialConversion.A24)]
        public string sMaschine_Nummer_Text { get; set; }
        [QdasMapping(1082, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMaschine_Bezeichnung { get; set; }
        [QdasMapping(1083, QdasFieldType.I10)]
        public int? lMaschine_Nummer { get; set; }
        [QdasMapping(1085, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMaschine_Standort { get; set; }
        [QdasMapping(1086, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sArbeitsgang { get; set; }
        [QdasMapping(1087, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sArbeitsgang_Bezeichnung { get; set; }

        [QdasMapping(1091, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string LineNumber { get; set; }
        [QdasMapping(1092, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string LineDescription { get; set; }

        [QdasMapping(1100, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sBereich_im_Werk { get; set; }
        [QdasMapping(1101, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sAbteilung { get; set; }
        [QdasMapping(1102, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sWerkstatt { get; set; }
        [QdasMapping(1103, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sKostenstelle { get; set; }
        [QdasMapping(1104, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sSchicht { get; set; }
        [QdasMapping(1105, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string DivisionNumber { get; set; }
        [QdasMapping(1106, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string DepartmentNumber { get; set; }
        [QdasMapping(1107, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string WorkshopNumber { get; set; }
        [QdasMapping(1108, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string CostCentreNumber { get; set; }

        [QdasMapping(1110, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sBestellnummer { get; set; }
        [QdasMapping(1111, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sWareneingangsnummer { get; set; }

        [QdasMapping(1112, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sWuerfel { get; set; }

        [QdasMapping(1113, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sPosition { get; set; }

        [QdasMapping(1114, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sVorrichtung { get; set; }

        [QdasMapping(1115, QdasFieldType.A, QdasSpecialConversion.A40)]
        public String sFertigungsdatum { get; set; }

        [QdasMapping(1201, QdasFieldType.A, QdasSpecialConversion.A24)]
        public string sPruefeinrichtung_Nummer_Text { get; set; }
        [QdasMapping(1202, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefeinrichtung_Bezeichnung { get; set; }
        [QdasMapping(1203, QdasFieldType.A, QdasSpecialConversion.A80)]
        public string sPruefgrund { get; set; }
        [QdasMapping(1204, QdasFieldType.D)]
        public DateTime? sPruefbeginn { get; set; }
        [QdasMapping(1205, QdasFieldType.D)]
        public DateTime? sPruefende { get; set; }
        [QdasMapping(1206, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefplatz { get; set; }
        [Obsolete]
        [QdasMapping(1207, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefplanersteller { get; set; }
        [QdasMapping(1208, QdasFieldType.I10)]
        public int? lPruefeinrichtung_Nummer { get; set; }
        [QdasMapping(1209, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sPruefart { get; set; }
        [QdasMapping(1210, QdasFieldType.I10)]
        public int? lMesstyp { get; set; }

        [QdasMapping(1211, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sNormal_Nummer_Text { get; set; }
        [QdasMapping(1212, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sNormal_Bezeichnung { get; set; }
        [QdasMapping(1215, QdasFieldType.I10)]
        public int? lNormal_Nummer { get; set; }

        [QdasMapping(1221, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sPruefer_Nummer_Text { get; set; }
        [QdasMapping(1222, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefer_Name { get; set; }
        [QdasMapping(1223, QdasFieldType.I10)]
        public int? lPruefer_Nummer { get; set; }

        [QdasMapping(1230, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sMessraum { get; set; }
        [QdasMapping(1231, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sMessprogramm_Nummer { get; set; }
        [QdasMapping(1232, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sMessprogramm_Version { get; set; }

        [QdasMapping(1301, QdasFieldType.I5)]
        public short? iMandant { get; set; }
        [QdasMapping(1302, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPrueflos { get; set; }
        [QdasMapping(1303, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sWerk { get; set; }
        [QdasMapping(1304, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string PlantNumber { get; set; }

        [QdasMapping(1311, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sFertigungsauftrag { get; set; }

        [QdasMapping(1341, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sPruefplannummer_Text { get; set; }
        [QdasMapping(1342, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefplanname { get; set; }
        [QdasMapping(1343, QdasFieldType.D)]
        public DateTime? sPruefplan_Erstelldatum { get; set; }
        //K1344 ( K1207 ? )
        [QdasMapping(1344, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sPruefplan_Ersteller { get; set; }

        [QdasMapping(1350, QdasFieldType.A, QdasSpecialConversion.A60)]
        public string OutputReportFile { get; set; }

        [QdasMapping(1800, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_1 { get; set; }
        [QdasMapping(1801, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_1 { get; set; }
        [QdasMapping(1802, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_1 { get; set; }

        [QdasMapping(1810, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_2 { get; set; }
        [QdasMapping(1811, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_2 { get; set; }
        [QdasMapping(1812, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_2 { get; set; }

        [QdasMapping(1820, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_3 { get; set; }
        [QdasMapping(1821, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_3 { get; set; }
        [QdasMapping(1822, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_3 { get; set; }

        [QdasMapping(1830, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_4 { get; set; }
        [QdasMapping(1831, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_4 { get; set; }
        [QdasMapping(1832, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_4 { get; set; }

        [QdasMapping(1840, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_5 { get; set; }
        [QdasMapping(1841, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_5 { get; set; }
        [QdasMapping(1842, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_5 { get; set; }

        [QdasMapping(1850, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_6 { get; set; }
        [QdasMapping(1851, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_6 { get; set; }
        [QdasMapping(1852, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_6 { get; set; }

        [QdasMapping(1860, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_7 { get; set; }
        [QdasMapping(1861, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_7 { get; set; }
        [QdasMapping(1862, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_7 { get; set; }

        [QdasMapping(1870, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_8 { get; set; }
        [QdasMapping(1871, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_8 { get; set; }
        [QdasMapping(1872, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_8 { get; set; }

        [QdasMapping(1880, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_9 { get; set; }
        [QdasMapping(1881, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_9 { get; set; }
        [QdasMapping(1882, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_9 { get; set; }

        [QdasMapping(1890, QdasFieldType.A, QdasSpecialConversion.A50)]
        public string sBenutzer_Feldbezeichnung_10 { get; set; }
        [QdasMapping(1891, QdasFieldType.A, QdasSpecialConversion.A1)]
        public string sBenutzer_Feldtyp_10 { get; set; }
        [QdasMapping(1892, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBenutzer_Feldinhalt_10 { get; set; }

        [QdasMapping(1900, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sBemerkung { get; set; }

        //FIXME field type not correct
        [QdasMapping(1997, QdasFieldType.A, QdasSpecialConversion.AMax)]
        public string sTeile_GUID { get; set; }
        [QdasMapping(1998, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sInterne_Konfiguration { get; set; }

        public ISet<IMainCatalog> MainCatalogs {get;private set;}
        public Dictionary<int, IMainCatalog> KNumberToMainCatalogLookup { get; private set; }

        public MainCatalogProcessParameter ProcessParameter { get;private set;}

        public K4220Catalogs K4220Catalog { get; private set; }

        public MainCatalog<GageCatalogEntry> GageCatalog { get; private set; }

        public MainCatalog<CavityCatalogEntry> CavityCatalog { get; private set; }

        public MainCatalog<OperatorCatalogEntry> OperatorCatalog { get; private set; }
        #endregion

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <inheritdoc/>
        public ISet<short> GetDefinedKNumbers()
        {
            return _kMappings.GetDefinedKNumbers();
        }

        /// <summary>
        /// Check if a field of this type is mapped to a k-number
        /// </summary>
        /// <param name="kNumber">k-number to check</param>
        /// <returns>Whether k-number is mapped</returns>
        public static bool DefinesMappableKField(short kNumber)
        {
            return _kMappings.DefinesMappableKField(kNumber);
        }

        /// <summary>
        /// Try to get the <see cref="QdasMappingAttribute"/> for a given k-number to retrieve
        /// </summary>
        /// <param name="kNumber">Target k-number</param>
        /// <param name="attribute">Contains the retrieved value of success</param>
        /// <returns>Whether the retrieval was succeeded</returns>
        public static bool TryGetQdasMappingAttribute(short kNumber, out QdasMappingAttribute attribute)
        {
            return _kMappings.TryGetQdasMappingAttribute(kNumber, out attribute);
        }

        /// <summary>
        /// Store unrecognized k-field data
        /// </summary>
        /// <param name="line">The unreconized k-field</param>
        public void AddUnrecognizedLine(QdasLineSplitResult unrecognizedData)
        {
            this._unrecognizedKeyData.Add(unrecognizedData);
        }

        /// <summary>
        /// Retrieve unrecognized k-field data
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<QdasLineSplitResult> GetUnparsedLines()
        {
            return this._unrecognizedKeyData.AsReadOnly();
        }
    }
}
