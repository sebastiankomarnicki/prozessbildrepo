﻿using System.Collections.Generic;

namespace Promess.Common.QDas
{
    public class QdasDfdReadResult
    {
        /// <summary>
        /// Generates a container for parsed dfd data.
        /// </summary>
        /// <param name="partData">part data</param>
        /// <param name="characteristicsData">characteristics data</param>
        /// <param name="errorCode">error code, legacy part</param>
        /// <param name="recognizedLines">recognized lines, must have an entry for 100, if valid</param>
        /// <param name="unrecognizedLines">unrecognized lines</param>
        public QdasDfdReadResult(TS_TEILEDATEN partData, TS_MERKMALSDATEN[] characteristicsData, iErrCodes errorCode, IReadOnlyDictionary<short, string> recognizedLines, IReadOnlyCollection<string> unrecognizedLines  )
        {
            this._partData = partData;
            this._characteristicsData = characteristicsData;
            this._errorCode = errorCode;
            this.RecognizedRawData = recognizedLines;
            this.UnrecognizedRawData = unrecognizedLines;
        }

        private TS_TEILEDATEN _partData;
        private TS_MERKMALSDATEN[] _characteristicsData;
        private iErrCodes _errorCode;

        /// <summary>
        /// Header part data
        /// </summary>
        public TS_TEILEDATEN PartsData
        {
            get
            {
                return _partData;
            }
        }

        /// <summary>
        /// Header data of all characteristics in order
        /// </summary>
        public TS_MERKMALSDATEN[] CharacteristicsData
        {
            get
            {
                return _characteristicsData;
            }
        }

        /// <summary>
        /// Error code from when reading the data
        /// </summary>
        public iErrCodes ErrorCode
        {
            get
            {
                return _errorCode;
            }
        }

        /// <summary>
        /// Recognized raw data with the k-number, in order
        /// </summary>
        public IReadOnlyDictionary<short, string> RecognizedRawData { get; private set; }
        /// <summary>
        /// Unrecognized raw lines, in order
        /// </summary>
        public IReadOnlyCollection<string> UnrecognizedRawData { get; private set; }
    }
}
