﻿using System;
using System.Collections.Generic;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Container for one measurement for a single characteristic
    /// </summary>
    public class TS_MERKMAL_IST:ISupportKNumberDataManipulation
    {
        private static QDasMappingAttributeAccess<TS_MERKMAL_IST> _kMappings;

        static TS_MERKMAL_IST()
        {
            _kMappings = new QDasMappingAttributeAccess<TS_MERKMAL_IST>();
        }


        //K2004
        public short? iMerkmal_Art { get; set; }
        //K2012 entfallen
        public short? iNacharbeit_UGW { get; set; }

        //Feld 0 / K0001
        [QdasMapping(1, QdasFieldType.F)]
        public double? dMesswert { get; set; }
        //Feld 1 / K0002
        [QdasMapping(2, QdasFieldType.I5)]
        public short? iAttribut { get; set; }
        //K0003 entfallen note mbe original is I1
        [Obsolete]
        [QdasMapping(3, QdasFieldType.I3)]
        public short? iFlag { get; set; }
        //Feld 2 / K0004
        [QdasMapping(4, QdasFieldType.D)]
        public DateTime? sDatumZeit { get; set; }
        //Feld 3 / K0005
        [QdasMapping(5, QdasFieldType.S,QdasSpecialConversion.I5Collection)]
        public IList<short> aiEreignis { get; set; }

        public IList<short> dbK4220 { get; set; }
        //Feld 4 / K0006
        [QdasMapping(6, QdasFieldType.A, QdasSpecialConversion.A14)]
        public string sChargenNr { get; set; }
        //Feld 5 / K0007
        [QdasMapping(7, QdasFieldType.I10)]
        public int? lNestNr { get; set; }
        //Feld 6 / K0008
        [QdasMapping(8, QdasFieldType.I10)]
        public int? lPrueferNr { get; set; }
        [QdasMapping(9, QdasFieldType.A, QdasSpecialConversion.A255)]
        public string sText { get; set; }
        //Feld 7 / K0010
        [QdasMapping(10, QdasFieldType.I10)]
        public int? lMaschinenNr { get; set; }
        [QdasMapping(11, QdasFieldType.S, QdasSpecialConversion.ProcParamList)]
        public IList<TS_Prozessparameter> sProzessparameter { get; set; }

        //Feld 9 / K0012
        [QdasMapping(12, QdasFieldType.I10)]
        public int? lPruefmittelNr { get; set; }
        [Obsolete]
        [QdasMapping(13, QdasFieldType.I5)]
        public short? iProzessparameterwert { get; set; }
        [QdasMapping(14, QdasFieldType.A, QdasSpecialConversion.A40)]
        public string sTeile_Ident { get; set; }
        [QdasMapping(15, QdasFieldType.I5)]
        public short? iUntersuchungszweck { get; set; }
        [QdasMapping(16, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sProduktionsnummer { get; set; }
        [QdasMapping(17, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sWerkstueckTraegerNr { get; set; }

        [QdasMapping(20, QdasFieldType.I5)]
        public int? iStichprobenUmfang { get; set; }
        [QdasMapping(21, QdasFieldType.I5)]
        public short? iAnzahl_Fehler { get; set; }
        [Obsolete]
        [QdasMapping(22, QdasFieldType.I5)]
        public short? iAnzahl_kl_UGW { get; set; }
        [Obsolete]
        [QdasMapping(23, QdasFieldType.I5)]
        public short? iAnzahl_gr_OGW { get; set; }
        [Obsolete]
        [QdasMapping(24, QdasFieldType.I5)]
        public short? iAusschuss { get; set; }
        [Obsolete]
        [QdasMapping(25, QdasFieldType.I5)]
        public short? iNacharbeit { get; set; }
        [Obsolete]
        [QdasMapping(26, QdasFieldType.I5)]
        public short? iFehlerklasse { get; set; }

        [QdasMapping(53, QdasFieldType.A, QdasSpecialConversion.A20)]
        public string sAuftragsnummer { get; set; }
        [QdasMapping(54, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sK0054 { get; set; }
        [QdasMapping(55, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sK0055 { get; set; }
        [QdasMapping(56, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sK0056 { get; set; }
        [QdasMapping(57, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sK0057 { get; set; }
        [QdasMapping(58, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sK0058 { get; set; }
        [QdasMapping(59, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sK0059 { get; set; }
        [QdasMapping(60, QdasFieldType.A, QdasSpecialConversion.A30)]
        public string sK0060 { get; set; }
        [QdasMapping(61, QdasFieldType.I10)]
        public int? lK0061 { get; set; }
        [QdasMapping(62, QdasFieldType.I10)]
        public int? lK0062 { get; set; }
        [QdasMapping(63, QdasFieldType.I10)]
        public int? lK0063 { get; set; }

        [QdasMapping(80, QdasFieldType.A,QdasSpecialConversion.A64)]
        public string SubgroupId { get; set; }
        [QdasMapping(81,QdasFieldType.I5)]
        public short? ValuePositionSubgroup { get; set; }

        //K0097 note mbe: no specification for guid
        //[QdasMapping(97, QdasFieldType.A, QdasSpecialConversion.A255)]
        //public string sWerte_GUID { get; set; }

        public bool HasDateTime
        {
            get
            {
                return sDatumZeit.HasValue;
            }
        }

        //  iProzParaAnz As Integer             ' Anzahl folgender Parameterbloecke
        //  iProzParaAkt As Integer             ' Aktueller Parameterblock
        //  astProzPara() As TS_PROZPARA        ' Feld  8 / K0011 / K0013
        public TS_MERKMAL_IST()
        {
            this.aiEreignis = new List<short>();
            this.dbK4220 = new List<short>();
            this.sProzessparameter = new List<TS_Prozessparameter>();
        }

        #region ISupportKNumberDataManipulation
        /// <inheritdoc/>
        public bool TryGetPropertyName(short kNumber, out string propertyName)
        {
            return _kMappings.TryGetPropertyName(kNumber, out propertyName);
        }

        /// <inheritdoc/>
        public bool TrySetField(short knumber, object value)
        {
            return _kMappings.TrySetField(this, knumber, value);
        }

        /// <inheritdoc/>
        public bool TryGetField(short knumber, out object value)
        {
            return _kMappings.TryGetField(this, knumber, out value);
        }
        #endregion

        /// <summary>
        /// Check if this class maps a given k-number
        /// </summary>
        /// <param name="kNumber">k-number to check</param>
        /// <returns>k-number is mapped</returns>
        public static bool DefinesMappableKField(short kNumber)
        {
            return _kMappings.DefinesMappableKField(kNumber);
        }

        /// <summary>
        /// Try to get the <see cref="QdasMappingAttribute"/> for this class for a given k-number
        /// </summary>
        /// <param name="kNumber">K-number to retrieve the attribute for</param>
        /// <param name="attribute">Out parameter for the retrieved attribute, if any</param>
        /// <returns>Success of retrieval</returns>
        public static bool TryGetQdasMappingAttribute(short kNumber, out QdasMappingAttribute attribute)
        {
            return _kMappings.TryGetQdasMappingAttribute(kNumber, out attribute);
        }
    }
}
