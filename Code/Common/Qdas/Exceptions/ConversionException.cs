﻿using System;

namespace Promess.Common.QDas.Exceptions
{
    /// <summary>
    /// Error information for when a conversion using <see cref="QdasMappingAttribute"/> fails
    /// </summary>
    public class ConversionException:Exception
    {
        public ConversionException(object notConvertibleData, QdasFieldType fieldType, int kNumber)
        {
            this.NotConvertibleData = notConvertibleData;
            this.ConvertToQdasFieldType = fieldType;
            this.ConvertToKNumber = kNumber;
        }

        public object NotConvertibleData { get; private set; }
        public QdasFieldType ConvertToQdasFieldType { get; private set; }
        public int ConvertToKNumber { get; private set; }
    }
}
