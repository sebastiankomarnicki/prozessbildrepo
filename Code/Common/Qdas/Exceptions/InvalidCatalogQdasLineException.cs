﻿using System;

namespace Promess.Common.QDas.Exceptions
{
    /// <summary>
    /// Information about an invalid catalog AQDEF entry
    /// </summary>
    public class InvalidCatalogQdasLineException:Exception
    {
        public InvalidCatalogQdasLineException(short kNumber, int reference, string data)
        {
            KNumber = kNumber;
            Reference = reference;
            LineData = data;
        }

        public short KNumber { get; private set; }
        public int Reference { get; private set; }
        public string LineData { get; private set; }
    }
}
