﻿using System;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Container for the result of a split AQDEF line, like 'K1001 Some part' or 'K2001/1 Some characteristic' without ''
    /// </summary>
    public class QdasLineSplitResult
    {
        private string[] _remainder;
        public const short InvalidOrMissingKeyIndex = -1;
        public const short InvalidKeyValue = -1;

        public string Key {
            get;private set;
        }
        
        public short KeyValue { get; private set; } = InvalidKeyValue;
        public short KeyIndex { get; set; } = InvalidOrMissingKeyIndex;
        public iErrCodes Error { get; set; } = iErrCodes.iecno_error;
        public bool HasError { get { return Error != iErrCodes.iecno_error; } }
        public bool HasData { get { return Remainder!=null && Remainder.Length > 0; } }

        /// <summary>
        /// Sets <see cref="Key"/> to the given text and tries to set <see cref="KeyValue"/> by parsing
        /// the text except the first character, otherwise <see cref="InvalidKeyValue"/>
        /// </summary>
        public void SetKey(string possibleKey)
        { 
            Key = possibleKey;
            short tmp = InvalidKeyValue;
            if (Key.Length >= 1)
                short.TryParse(Key.Substring(1), out tmp);
            KeyValue = tmp;
        }

        /// <summary>
        /// Remainder of a k-number entry, i.e. K1001 remainder
        /// </summary>
        public string[] Remainder {
            get { return _remainder; }
            set
            {
                _remainder = value ?? new string[0];
            }
        }

        /// <summary>
        /// Returns the data as k-number line
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string indexPart = "";
            if (KeyIndex != InvalidOrMissingKeyIndex)
                indexPart = $"/{KeyIndex}";
            return $"{this.Key}{indexPart} {String.Join(Convert.ToChar(15).ToString(), Remainder)}";
        }
    }
}
