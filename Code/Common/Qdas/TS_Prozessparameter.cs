﻿using System;
using System.Collections.Generic;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Stores K0011 (process parameter) information
    /// Immutable
    /// </summary>
    public class TS_Prozessparameter
    {
        public short ProcessParameterCatalogQdasID { get; private set; } = -1;
        public short ProcessParameterQdasID { get; private set; } = -1;

        /// <summary>
        /// Parse an AQDEF process parameter line into a list of process parameters 
        /// </summary>
        /// <param name="input">Input to parse</param>
        /// <returns>List of process parameters</returns>
        /// <exception cref="FormatException">Input was not in the correct format</exception>
        public static List<TS_Prozessparameter> Parse(string input)
        {
            List<TS_Prozessparameter> result = new List<TS_Prozessparameter>();
            string[] splitEntry;
            short tmpVal;
            bool success;
            TS_Prozessparameter processParameter;
            input = input.Trim();
            if (!input.StartsWith("[") || !input.EndsWith("]"))
                throw new FormatException();
            if (input.Length == 2)
                return result;

            input = input.Substring(1, input.Length - 2);
            foreach (string entry in input.Split(','))
            {
                splitEntry = entry.Split(' ');
                if (splitEntry.Length != 2)
                    throw new FormatException();
                processParameter = new TS_Prozessparameter();
                success = short.TryParse(splitEntry[0], out tmpVal);
                if (!success)
                    throw new FormatException();
                processParameter.ProcessParameterCatalogQdasID = tmpVal;
                success = short.TryParse(splitEntry[1], out tmpVal);
                if (!success)
                    throw new FormatException();
                processParameter.ProcessParameterQdasID = tmpVal;
                result.Add(processParameter);
            }
            return result;
        }
    }
}
