﻿using Promess.Common.QDas.Catalogs;
using Promess.Common.QDas.Util;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Catalog for process parameter. Differs from the normal catalog due to the catalog entries having values.
    /// See AQDEF K4240
    /// </summary>
    public class MainCatalogProcessParameter
    {
        private readonly short _mainCatalogKNumber;
        private readonly short _entryRefToSubcatalogKNumber;
        private readonly short _valueEntryToValueRefKNumber;
        private readonly int _katalogNameLength;
        private string _name;
        private Dictionary<int, string> _subCatalogRefToName;
        private Dictionary<int, HashSet<int>> _subCatalogRefToEntryRefs;
        private Dictionary<int, ProcessParameter> _entryRefToCatalogEntry;
        private Dictionary<int, ProcessParameterValue> _valueRefToValueEntry;
        private Dictionary<int, HashSet<int>> _entryRefToValueRefs;
        private ImmutableHashSet<short> _definedKNumbers;

        public MainCatalogProcessParameter()
        {
            this._name = "";
            this._mainCatalogKNumber = 4240;
            this._katalogNameLength = 80;
            this._entryRefToSubcatalogKNumber = 4241;
            this._valueEntryToValueRefKNumber = 4249;
            this._subCatalogRefToName = new Dictionary<int, string>();
            this._subCatalogRefToEntryRefs = new Dictionary<int, HashSet<int>>();
            this._entryRefToCatalogEntry = new Dictionary<int, ProcessParameter>();
            this._valueRefToValueEntry = new Dictionary<int, ProcessParameterValue>();
            this._entryRefToValueRefs = new Dictionary<int, HashSet<int>>();
        }

        /// <summary>
        /// K-Number which defines the main and subcatalogs
        /// </summary>
        public int MainCatalogKNumber
        {
            get { return _mainCatalogKNumber; }
        }

        /// <summary>
        /// Name of the catalog
        /// </summary>
        public string MainCatalogName
        {
            get { return _name; }
        }

        /// <summary>
        /// Iterator for all catalog entries and their k-numbers
        /// </summary>
        /// <returns>Pairs of entry reference and entry for data manipulation</returns>
        public IEnumerable<Tuple<int, ISupportKNumberDataManipulation>> GetCatalogEntriesAndQdasKeys()
        {
            foreach (var entry in _entryRefToCatalogEntry)
            {
                yield return new Tuple<int, ISupportKNumberDataManipulation>(entry.Key, entry.Value);
            }
        }

        /// <summary>
        /// Iterator for all references catalog entries for a subcatalog
        /// </summary>
        /// <param name="subcatalogKey">Reference of the subcatalog</param>
        /// <returns></returns>
        public IEnumerable<int> GetQdasKeysInSubcatalog(int subcatalogKey)
        {
            return _subCatalogRefToEntryRefs[subcatalogKey];
        }

        /// <summary>
        /// Iterator for all subcatalog references and their names
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tuple<int, string>> GetSubcatalogsAndQdasKeys()
        {
            foreach (var entry in _subCatalogRefToName)
            {
                yield return new Tuple<int, string>(entry.Key, entry.Value);
            }
        }

        /// <summary>
        /// Get the list of the required k-numbers for catalog entries
        /// </summary>
        /// <returns>The list</returns>
        public ICollection<short> GetRequiredKNumbersOfEntries()
        {
            return KNumberQdasUtil.GetRequiredKNumbers<ProcessParameter>();
        }

        /// <summary>
        /// Get the list of the required k-numbers for <see cref="ProcessParameterValue"/>
        /// </summary>
        /// <returns>The list</returns>
        public ICollection<short> GetRequiredKNumbersOfValues()
        {
            return KNumberQdasUtil.GetRequiredKNumbers<ProcessParameterValue>();
        }

        /// <summary>
        /// Add data to a catalog. A subcatalog, process parameter or process parameter value.
        /// </summary>
        /// <param name="kNumber">k-number of the entry</param>
        /// <param name="reference">Reference number</param>
        /// <param name="data">Data</param>
        public void AddData(short kNumber, int reference, string data)
        {
            if (kNumber == _mainCatalogKNumber)
            {
                Debug.Assert(reference >= 0);
                string limited;
                if (data.Length > _katalogNameLength)
                {
                    limited = data.Substring(0, _katalogNameLength);
                }
                else
                {
                    limited = data;
                }
                if (reference == 0)
                {
                    _name = limited;
                }
                else
                {
                    _subCatalogRefToName[reference] = limited;
                }

            }
            else if (kNumber == _entryRefToSubcatalogKNumber)
            {
                Debug.Assert(reference > 0);
                int entryRef = int.Parse(data, CultureInfo.InvariantCulture);

                if (_subCatalogRefToEntryRefs.TryGetValue(reference, out HashSet<int> entries))
                {
                    entries.Add(entryRef);
                }
                else
                {
                    _subCatalogRefToEntryRefs[reference] = new HashSet<int>() { entryRef };
                }
            }
            else if (kNumber == _valueEntryToValueRefKNumber)
            {
                Debug.Assert(reference > 0);
                int valueRef = int.Parse(data, CultureInfo.InvariantCulture);
                //TODO need to check if this is already present and throw an exception or consistency check?
                if (_entryRefToValueRefs.TryGetValue(reference, out HashSet<int> entries))
                {
                    entries.Add(valueRef);
                }
                else
                {
                    _entryRefToValueRefs[reference] = new HashSet<int>() { valueRef };
                }
            }
            else if (ProcessParameterValue.GetDefinedKNumbersStatic().Contains(kNumber))
            {
                Debug.Assert(reference > 0);
                if (_valueRefToValueEntry.TryGetValue(reference, out ProcessParameterValue entry))
                {
                    entry.TrySetField(kNumber, data);
                }
                else
                {
                    entry = new ProcessParameterValue();
                    entry.TrySetField(kNumber, data);
                    _valueRefToValueEntry[reference] = entry;
                }
            }
            else {
                Debug.Assert(reference > 0);

                if (_entryRefToCatalogEntry.TryGetValue(reference, out ProcessParameter entry))
                {
                    entry.TrySetField(kNumber, data);
                }
                else
                {
                    entry = new ProcessParameter();
                    entry.TrySetField(kNumber, data);
                    _entryRefToCatalogEntry[reference] = entry;
                }
            }
        }

        /// <summary>
        /// Are all references to subcatalogs and catalog entries consistent (referenced entries exist)?
        /// </summary>
        /// <returns>Result of consistency check</returns>
        public bool IsConsistent()
        {
            var keysOfAllEntries = new HashSet<int>(_entryRefToCatalogEntry.Keys);
            var referencesSubcatalogs = new HashSet<int>(_subCatalogRefToName.Keys);
            var subcatalogsWithReferences = new HashSet<int>(_subCatalogRefToEntryRefs.Keys);

            if (!subcatalogsWithReferences.IsSubsetOf(referencesSubcatalogs))
                return false;

            foreach (var subcatEntries in _subCatalogRefToEntryRefs)
            {
                if (!subcatEntries.Value.IsSubsetOf(keysOfAllEntries))
                    return false;
            }

            var keysOfAllValueEntries = new HashSet<int>(_valueRefToValueEntry.Keys);
            var processParametersWithValues = new HashSet<int>(_entryRefToValueRefs.Keys);
            if (!processParametersWithValues.IsSubsetOf(keysOfAllEntries))
                return false;
            var allValuesKeys = new HashSet<int>();
            int countKeys = 0;
            foreach(var entryRefs in _entryRefToValueRefs)
            {
                allValuesKeys.UnionWith(entryRefs.Value);
                countKeys += entryRefs.Value.Count;
                if (!entryRefs.Value.IsSubsetOf(keysOfAllValueEntries))
                    return false;
            }
            if (allValuesKeys.Count != countKeys)
                return false;
            return true;
        }

        /// <summary>
        /// Checks if the catalog contains data
        /// </summary>
        /// <returns>Result of the check</returns>
        public bool ContainsData()
        {
            return _subCatalogRefToEntryRefs.Count > 0 || _subCatalogRefToName.Count > 0;
        }

        /// <summary>
        /// Defined k-numbers
        /// </summary>
        public ImmutableHashSet<short> DefinedKNumbers
        {
            get
            {
                if (_definedKNumbers == null)
                {
                    ISet<short> definedKNumbers = ProcessParameter.GetDefinedKNumbersStatic();
                    definedKNumbers.UnionWith(ProcessParameterValue.GetDefinedKNumbersStatic());
                    definedKNumbers.Add(_mainCatalogKNumber);
                    definedKNumbers.Add(_entryRefToSubcatalogKNumber);
                    definedKNumbers.Add(_valueEntryToValueRefKNumber);
                    _definedKNumbers = ImmutableHashSet.Create<short>(definedKNumbers.ToArray());
                }
                return _definedKNumbers;
            }
        }

        /// <summary>
        /// Remove all subcatalogs without name and catalog entries where required fields are missing or without value
        /// </summary>
        /// <returns><c>Tuple</c> of removed subcatalogs, removed entries</returns>
        public Tuple<ICollection<int>, ICollection<int>, ICollection<int>> RemoveSubcatalogsAndEntriesWORequiredValues()
        {
            List<int> removedSubcatalogReferences = new List<int>();
            List<int> removedEntryReferences = new List<int>();
            foreach (var subcatEntry in _subCatalogRefToName)
            {
                if (String.IsNullOrEmpty(subcatEntry.Value))
                    removedSubcatalogReferences.Add(subcatEntry.Key);
            }
            foreach (var key in removedSubcatalogReferences)
            {
                _subCatalogRefToName.Remove(key);
            }
            ICollection<short> requiredKNumbers = GetRequiredKNumbersOfEntries();
            foreach (var eventEntry in _entryRefToCatalogEntry)
            {
                object value = null;
                String convertedValue;
                foreach (var requiredKNumber in requiredKNumbers)
                {
                    if (!eventEntry.Value.TryGetField(requiredKNumber, out value))
                    {
                        throw new NotImplementedException($"associated knumber not found {requiredKNumber} {typeof(ProcessParameter)} .\n please contact promess.");
                    }
                    else
                    {
                        Debug.Assert(value == null || value.GetType().Equals(typeof(String)),
                            "the cast is supposed to fail if there ever is a catalog entry with a non string required field"
                            );
                        convertedValue = (String)value;
                        if (String.IsNullOrEmpty(convertedValue))
                        {
                            removedEntryReferences.Add(eventEntry.Key);
                            break;
                        }
                    }
                }
            }
            foreach (var key in removedEntryReferences)
            {
                _entryRefToCatalogEntry.Remove(key);
            }

            List<int> removedValueReferences = new List<int>();
            ICollection<short> requiredValueKNumbers = GetRequiredKNumbersOfValues();
            foreach (var valueEntry in _valueRefToValueEntry)
            {
                object value = null;
                String convertedValue;
                foreach (var requiredKNumber in requiredValueKNumbers)
                {
                    if (!valueEntry.Value.TryGetField(requiredKNumber, out value))
                    {
                        throw new NotImplementedException($"associated knumber not found {requiredKNumber} {typeof(ProcessParameterValue)} .\n please contact promess.");
                    }
                    else
                    {
                        Debug.Assert(value == null || value.GetType().Equals(typeof(String)),
                            "the cast is supposed to fail if there ever is a catalog entry with a non string required field"
                            );
                        convertedValue = (String)value;
                        if (String.IsNullOrEmpty(convertedValue))
                        {
                            removedValueReferences.Add(valueEntry.Key);
                            break;
                        }
                    }
                }
            }
            foreach (var key in removedValueReferences)
            {
                _valueRefToValueEntry.Remove(key);
            }
            return new Tuple<ICollection<int>, ICollection<int>, ICollection<int>>(removedSubcatalogReferences, removedEntryReferences, removedValueReferences);
        }

        /// <summary>
        /// Get all <see cref="ProcessParameterValue"/> associated with a <see cref="ProcessParameter"/> by its id
        /// </summary>
        /// <param name="id">qsStat id of the <see cref="ProcessParameter"/></param>
        /// <returns>All value entries by their id</returns>
        public IEnumerable<Tuple<int,ProcessParameterValue>> GetValuesForProcessParameterId(int id)
        {
            if (!_entryRefToValueRefs.TryGetValue(id, out HashSet<int> keys))
                yield break;
            foreach (var valueId in keys)
            {
                yield return new Tuple<int, ProcessParameterValue>(valueId,_valueRefToValueEntry[valueId]);
            }
        }
    }
}
