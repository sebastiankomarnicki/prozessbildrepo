﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Promess.Common.QDas.Util
{
    /// <summary>
    /// Helper functions for k-numebr data retrieval
    /// </summary>
    public static class KNumberQdasUtil
    {
        /// <summary>
        /// Checks in this order which of the following classes defines mapping for a k-number:
        /// <see cref="TS_TEILEDATEN"/>, <see cref="TS_MERKMALSDATEN"/>, <see cref="TS_MERKMAL_IST"/>
        /// Assumes that the mapping exists. 
        /// </summary>
        /// <param name="kNumber">Target k-number</param>
        /// <returns><see cref="QdasMappingAttribute"/> for the k-number</returns>
        public static QdasMappingAttribute GetQdasMappingAttribute(short kNumber)
        {
            QdasMappingAttribute result;
            if (TS_TEILEDATEN.TryGetQdasMappingAttribute(kNumber, out result))
            {
                return result;
            }
            else if (TS_MERKMALSDATEN.TryGetQdasMappingAttribute(kNumber, out result))
            {
                return result;
            }
            else if (TS_MERKMAL_IST.TryGetQdasMappingAttribute(kNumber, out result))
            {
                return result;
            }
            //there is a test for this
            throw new ArgumentOutOfRangeException();
        }

        public static ICollection<short> GetRequiredKNumbers<T>(T data) where T:ISupportKNumberDataManipulation, IRequiredKNumbers
        {
            return GetRequiredKNumbers<T>();
        }

        /// <summary>
        /// Get all k-numbers of properties marked with <see cref="QdasMappingAttribute"/> and <see cref="RequiredAttribute"/>
        /// </summary>
        /// <typeparam name="T">Type which defines the properties</typeparam>
        /// <returns>Collection of k-numbers</returns>
        public static ICollection<short> GetRequiredKNumbers<T>() where T : ISupportKNumberDataManipulation, IRequiredKNumbers
        {
            List<PropertyInfo> requiredProperties = typeof(T).GetProperties().Where(prop => prop.IsDefined(typeof(RequiredAttribute), false) && prop.IsDefined(typeof(QdasMappingAttribute), false)).ToList();
            HashSet<short> requiredKNumbers = new HashSet<short>();
            foreach (var pi in requiredProperties)
            {
                requiredKNumbers.Add(pi.GetCustomAttribute<QdasMappingAttribute>().KNumber);
            }
            Debug.Assert(requiredKNumbers.Count > 0);
            return requiredKNumbers;
        }
    }
}
