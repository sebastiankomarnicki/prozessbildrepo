﻿using System;

namespace Promess.Common.QDas
{
    /// <summary>
    /// Attribute for mapping k-numbers
    /// </summary>
    [AttributeUsage(AttributeTargets.Property,AllowMultiple =true)]
    public class QdasMappingAttribute:Attribute
    {
        public short KNumber { get; private set; }
        public QdasFieldType FieldType { get; private set; }
        public QdasSpecialConversion SpecialConversion { get; private set; }

        /// <summary>
        /// Generate a mapping
        /// </summary>
        /// <param name="KNumber">k-number according to AQDEF</param>
        /// <param name="fieldType">Field type of the k-number</param>
        /// <param name="specialConversion">Special conversion when parsing the text representation</param>
        public QdasMappingAttribute(short KNumber, QdasFieldType fieldType, QdasSpecialConversion specialConversion = QdasSpecialConversion.None)
        {
            this.KNumber = KNumber;
            this.FieldType = fieldType;
            this.SpecialConversion = specialConversion;
        }
    }
}
