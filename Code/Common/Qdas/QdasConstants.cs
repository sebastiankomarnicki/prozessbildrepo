﻿namespace Promess.Common.QDas
{
    /// <summary>
    /// Wrapper for QDAS IDs
    /// </summary>
    public static class QdasConstants
    {
        public const int NOID = -1;

        public static bool HasId(int id)
        {
            return id != NOID;
        }
    }
}
