﻿namespace Promess.Common.QDas
{
    /// <summary>
    /// Enum with all defined special conversions for AQDEF value entries
    /// </summary>
    public enum QdasSpecialConversion
    {
        None,
        ProcParamList,
        ChartTypeLocation,
        ChartTypeVariation,
        I5Collection,
        A1,
        A2,
        A10,
        A12,
        A14,
        A15,
        A20,
        A24,
        A30,
        A40,
        A50,
        A60,
        A64,
        A80,
        A200,
        A254,
        A255,
        AMax
    }
}
