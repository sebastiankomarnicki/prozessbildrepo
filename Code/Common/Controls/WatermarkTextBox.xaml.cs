﻿using System;
using System.Windows;
using System.Windows.Controls;
namespace Promess.Common.Controls
{
    /// <summary>
    /// <see cref="TextBox"/> with watermark functionality
    /// </summary>
    public partial class WatermarkTextBox : TextBox
    {
        public WatermarkTextBox()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Text to display when <see cref="TextBox.Text"/> is <c>String.Empty</c> or <c>null</c>
        /// </summary>
        public String Watermark
        {
            get { return GetValue(WatermarkProperty) as String; }
            set { SetValue(WatermarkProperty, value); }

        }

        public static readonly DependencyProperty WatermarkProperty = DependencyProperty.Register("Watermark", typeof(String), typeof(WatermarkTextBox));
    }
}
