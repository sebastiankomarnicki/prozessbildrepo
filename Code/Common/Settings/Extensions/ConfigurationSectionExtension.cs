﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace Promess.Common.Settings.Extensions
{
    /// <summary>
    /// Base for extending <see cref="ConfigurationSection"/>. Support for typed setting and getting of elements.
    /// Error notification support, copy operations.
    /// </summary>
    public abstract class ConfigurationSectionExtension:ConfigurationSection, INotifyDataErrorInfo
    {
        /// <summary>
        /// Gets a property, attribute, or child element of this configuration element.
        /// </summary>
        /// <typeparam name="T">Type of the element</typeparam>
        /// <param name="elementName">The name of the <see cref="ConfigurationProperty"/> to access.</param>
        /// <exception cref="InvalidCastException">Element could not be cast to the given type</exception>
        /// <returns>Retrieved value</returns>
        public T GetValue<T>([CallerMemberName] string elementName="")
        {
            return (T)base[elementName];
        }

        /// <summary>
        /// Sets a property, attribute, or child element of this configuration element.
        /// </summary>
        /// <typeparam name="T">Type of the element</typeparam>
        /// <param name="value">Value of the element</param>
        /// <param name="elementName">The name of the <see cref="ConfigurationProperty"/> to access.</param>
        /// <exception cref="ConfigurationErrorsException">Section property is read-only or locked</exception>
        public void SetValue<T>(T value, [CallerMemberName] string elementName = "")
        {
            base[elementName] = value;
        }

        /// <summary>
        /// Return a copy of this section (section information will be missing), which will not lead to changes in the configuration on manipulation.
        /// </summary>
        /// <returns></returns>
        public ConfigurationSectionExtension Copy()
        {
            Type myType = this.GetType();
            ConfigurationSectionExtension theCopy = Activator.CreateInstance(myType) as ConfigurationSectionExtension;
            this.Copy(theCopy, this.SectionInformation.Name);
            return theCopy;
        }

        private void Copy(ConfigurationSectionExtension destination, string sectionName)
        {
            Debug.Assert(this.GetType().Equals(destination.GetType()));
            Debug.Assert(!sectionName.Equals(string.Empty));
            string serializedSection = SerializeSection(this, sectionName, ConfigurationSaveMode.Full);
            System.Xml.XmlReader xmlReader = new System.Xml.XmlTextReader(new System.IO.StringReader(serializedSection));
            destination.DeserializeSection(xmlReader);
        }

        /// <summary>
        /// Copies only the data of the calling object to the parameter. The section name of the parameter is used.
        /// </summary>
        /// <param name="destination">Destination for the data of this object, must be of _same_ type</param>
        public void CopyDataOnly(ConfigurationSectionExtension destination)
        {
            if (!this.GetType().Equals(destination.GetType()))
            {
                throw new ArgumentException(String.Format("Source type({0}) must be the same as the destination type{1}.", this.GetType().AssemblyQualifiedName, destination.GetType().AssemblyQualifiedName));
            }
            this.Copy(destination, destination.SectionInformation.Name);
        }

        #region INotifyDataErrorInfo
        /// <inheritdoc/>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        private IDictionary<string, IList<string>> _errors = new Dictionary<string, IList<string>>();

        private void RaiseErrorsChanged(string propertyName)
        {
                ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        /// <inheritdoc/>
        public bool HasErrors
        {
            get
            {
                return _errors.Count > 0;
            }
        }

        /// <inheritdoc/>
        public IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                // Provide all the error collections.
                return (_errors.Values);
            }
            else
            {
                // Provice the error collection for the requested property
                // (if it has errors).
                if (_errors.ContainsKey(propertyName))
                {
                    return (_errors[propertyName]);
                }
                else
                {
                    return null;
                }
            }
        }

        protected void ValidateModelProperty(object value, [CallerMemberName] string propertyName = null)
        {
            if (_errors.ContainsKey(propertyName))
                _errors.Remove(propertyName);

            ICollection<ValidationResult> validationResults = new List<ValidationResult>();
            ValidationContext validationContext =
                new ValidationContext(this, null, null) { MemberName = propertyName };
            if (!Validator.TryValidateProperty(value, validationContext, validationResults))
            {
                _errors.Add(propertyName, new List<string>());
                foreach (ValidationResult validationResult in validationResults)
                {
                    _errors[propertyName].Add(validationResult.ErrorMessage);
                }
            }
            RaiseErrorsChanged(propertyName);
        }
        #endregion
    }
}
