﻿using System.Configuration;
using System.Runtime.CompilerServices;

namespace Promess.Common.Settings.Extensions
{
    /// <summary>
    /// Extension for supporting typed setting and getting of values.
    /// </summary>
    public abstract class ConfigurationElementExtension : ConfigurationElement
    {
        /// <summary>
        /// Gets a property, attribute, or child element of this configuration element.
        /// </summary>
        /// <typeparam name="T">Type of the element</typeparam>
        /// <param name="elementName">The name of the <see cref="ConfigurationProperty"/> to access.</param>
        /// <exception cref="System.InvalidCastException">Element could not be cast to the given type</exception>
        /// <returns>Retrieved value</returns>
        public T GetValue<T>([CallerMemberName] string elementName="")
        {
            return (T) base[elementName];
        }

        /// <summary>
        /// Sets a property, attribute, or child element of this configuration element.
        /// </summary>
        /// <typeparam name="T">Type of the element</typeparam>
        /// <param name="value">Value of the element</param>
        /// <param name="elementName">The name of the <see cref="ConfigurationProperty"/> to access.</param>
        /// <exception cref="ConfigurationErrorsException">Section property is read-only or locked</exception>
        protected virtual void SetValue<T>(T value, [CallerMemberName] string elementName="")
        {
            base[elementName] = value;
        }
    }
}
