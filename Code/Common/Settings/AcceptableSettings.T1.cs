﻿namespace Promess.Common.Settings
{
    /// <summary>
    /// Wrapper for a <see cref="Extensions.ConfigurationSectionExtension"/> to allow changes to it before accepting or discarding them.
    /// </summary>
    /// <typeparam name="T">The concrete type of the section</typeparam>
    public class AcceptableSettings<T> where T : Extensions.ConfigurationSectionExtension
    {
        private T _originalSettings;
        private T _workingCopy;
        private ModifiedConfigurationManager _ownerCm;

        public AcceptableSettings(T originalSettings, ModifiedConfigurationManager ownerCm)
        {
            _originalSettings = originalSettings;
            _workingCopy = originalSettings.Copy() as T;
            this._ownerCm = ownerCm;
        }

        /// <summary>
        /// Write changes back to the original settings and save back to <see cref="ModifiedConfigurationManager"/>
        /// </summary>
        public void AcceptChanges()
        {
            _workingCopy.CopyDataOnly(_originalSettings);
            _ownerCm.Save(System.Configuration.ConfigurationSaveMode.Modified);
        }

        /// <summary>
        /// Temporary settings which can be accepted or discarded
        /// </summary>
        public T Settings
        {
            get
            {
                return _workingCopy;
            }
        }


    }
}
