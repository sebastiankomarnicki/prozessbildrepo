﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using System.Windows;

namespace Promess.Common.Settings
{
    /// <summary>
    /// Wrapper for a Configuration to easier support custom locations and adding/retrieving sections
    /// </summary>
    public class ModifiedConfigurationManager
    {
        private Configuration _configuration;

        private const string _protectionProvider = "DataProtectionConfigurationProvider";

        /// <summary>
        /// Loaded <see cref="Configuration"/>
        /// </summary>
        public Configuration Configuration
        {
            get
            {
                return _configuration;
            }

            set
            {
                _configuration = value;
            }

        }

        /// <summary>
        /// Protection provider text for en-/decrypting
        /// </summary>
        public String ProtectionProvider
        {
            get { return _protectionProvider; }
        }

        /// <summary>
        /// Loads a configuration from the path with the name of the default application domain name ending with .config.
        /// Presents user with dialog options for overwriting the configuration on error or exiting the application.
        /// </summary>
        /// <param name="path"></param>
        /// <returns>Loaded <see cref="Configuration"/></returns>
        public static Configuration GetConfigurationFromPath(string path)
        {
            Trace.Assert(path!=null,"Configuration was initialized with an empty path");
            path = Path.Combine(path, Assembly.GetEntryAssembly().GetName().Name + ".config");
            return GetConfigurationFromFile(path);
        }

        /// <summary>
        /// Loads a configuration from a file.
        /// Presents user with dialog options for overwriting the configuration on error or exiting the application.
        /// </summary>
        /// <param name="configFileNameAndLocation">Config file</param>
        /// <returns>Loaded <see cref="Configuration"/></returns>
        public static Configuration GetConfigurationFromFile(string configFileNameAndLocation)
        {
            ExeConfigurationFileMap exeConfigurationFileMap = new ExeConfigurationFileMap();
            exeConfigurationFileMap.ExeConfigFilename = configFileNameAndLocation;
            Configuration configuration=null;
            try
            {
                configuration =
                    ConfigurationManager.OpenMappedExeConfiguration(exeConfigurationFileMap, ConfigurationUserLevel.None);
            }
            catch (ConfigurationErrorsException)
            {
                var result = MessageBox.Show("Die Konfiguration konnte nicht gelesen werden, soll sie überschrieben werden? Andernfalls beendet sich das Programm.\n" +
                        "The configuration could not be read, should it be replaced? Otherwise the program will be closed.",
                        "Replace", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        File.Delete(configFileNameAndLocation);
                        configuration =
                            ConfigurationManager.OpenMappedExeConfiguration(exeConfigurationFileMap, ConfigurationUserLevel.None);
                    }catch
                    {
                        MessageBox.Show("Die Konfiguration konnte nicht ersetzt werden, das Programm wird bendet.\n"+
                            "The configuration could not be replaced, the program is shutting down.",
                            "Shutdown",MessageBoxButton.OK,MessageBoxImage.Information);
                        Environment.Exit(0);
                    }
                }
                else
                {
                    Environment.Exit(0);
                }
            }
            return configuration;
        }

        /// <summary>
        /// Will get a <see cref="ConfigurationSection"/> with the given name and subtype from the configuration.
        /// <see cref="ConfigurationSection"/> must have a parameterless constructor. Section will be created if not
        /// present in configuration.
        /// </summary>
        /// <typeparam name="T">Subtype of the <see cref="ConfigurationSection"/> to retrieve.</typeparam>
        /// <param name="name">Section name</param>
        /// <param name="created">true when the section was created, otherwise false.</param>
        /// <returns>Retrieved data</returns>
        public T GetSectionAndAddIfMissing<T>(String name, out bool created) where T: ConfigurationSection
        {
            T result = Configuration.GetSection(name) as T;
            if (result == null)
            {
                result = Activator.CreateInstance<T>();
                _configuration.Sections.Add(name, result);
                _configuration.Save(ConfigurationSaveMode.Modified);
                created = true;
                //reference code has a ConfigurationManager.RefreshSection(NAME) here, which is not working for custom config files
            }else
            {
                created = false;
            }
            return result;
        }

        /// <summary>
        /// <see cref="GetSectionAndAddIfMissing{T}(string, out bool)"/> call where out parameter is discarded
        /// </summary>
        /// <typeparam name="T">Subtype of the <see cref="ConfigurationSection"/> to retrieve.</typeparam>
        /// <param name="name">Section name</param>
        /// <returns>Retrieved data</returns>
        public T GetSectionAndAddIfMissing<T>(String name) where T: ConfigurationSection
        {
            return GetSectionAndAddIfMissing<T>(name, out bool discard);
        }

        /// <summary>
        /// Retrieves a <see cref="ConfigurationSection"/> and wraps it in an <see cref="AcceptableSettings{T}"/> so changes are only
        /// propagated to configuration if accepted.
        /// </summary>
        /// <typeparam name="T">Subtype of the <see cref="ConfigurationSection"/> to retrieve.</typeparam>
        /// <param name="name">Section name</param>
        /// <returns>Wrapped section</returns>
        public AcceptableSettings<T> GetSectionAndAddIfMissingAsAcceptableSettings<T>(String name) where T :Promess.Common.Settings.Extensions.ConfigurationSectionExtension
        {
            var retrievedSection = GetSectionAndAddIfMissing<T>(name);
            return new AcceptableSettings<T>(retrievedSection,this);
        }

        /// <summary>
        /// Exposes <see cref="Configuration.Save(ConfigurationSaveMode, true)"/>
        /// </summary>
        /// <param name="csm">Save mode</param>
        public void Save(ConfigurationSaveMode csm)
        {
            Configuration.Save(csm,true);
        }
    }
}
