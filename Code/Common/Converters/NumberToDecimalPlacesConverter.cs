﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Common.Converters
{
    /// <summary>
    /// One way converter to get the number of decimal places of a number
    /// </summary>
    [ValueConversion(typeof(double),typeof(int))]
    public class NumberToDecimalPlacesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            double? number = (double?)value;
            if (!number.HasValue)
            {
                return Binding.DoNothing;
            }
            return (int) Math.Ceiling(Math.Log(number.Value));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
