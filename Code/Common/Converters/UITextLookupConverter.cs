﻿using System;
using System.Windows.Data;
using System.Globalization;
using Promess.Language;

namespace Promess.Common.Converters
{
    /// <summary>
    /// One way converter for retrieving the text of a text key for the current language
    /// </summary>
    [ValueConversion(typeof(ITranslationProvider),typeof(string))]
	public class UITextLookupConverter : IValueConverter
	{
        private static UITextLookupConverter _sharedConverter;

        static UITextLookupConverter()
        {
            _sharedConverter = new UITextLookupConverter();
        }

        public static Binding CreateBinding(string key)
        {
            Binding languageBinding = new Binding("CurrentLanguageData")
            {
                Source = LanguageManager.Current,
                Converter = _sharedConverter,
                ConverterParameter = key,
                Mode=BindingMode.OneWay
            };
            return languageBinding;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string key = parameter as string;
            LanguageData langData = value as LanguageData;
            if (langData==null||key == null)
                return Binding.DoNothing;

            return langData.Translate(key);
        }

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
    }
}
