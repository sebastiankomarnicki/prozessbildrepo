﻿using System;
using System.Windows.Data;
using System.Globalization;
using Promess.Language;
using System.Windows;

namespace Promess.Common.Converters
{ 
    /// <summary>
    /// One way converter to retrieve the text for a lookup key for the current language
    /// </summary>
    [ValueConversion(typeof(string), typeof(string))]
    public class TextLookupConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            string key = value as string;
            if (key == null)
                return Binding.DoNothing;

            return LanguageManager.Current.CurrentLanguageData.Translate(key);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
