﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Common.Converters
{
    /// <summary>
    /// One way converter to check if the given value is null
    /// </summary>
    [ValueConversion(typeof(object), typeof(bool))]
    public class IsNullConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;
            return value == null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
