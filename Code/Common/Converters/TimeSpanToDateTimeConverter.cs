﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Common.Converters
{
    /// <summary>
    /// Converter for <see cref="TimeSpan"/> to <see cref="DateTime"/>. <see cref="DateTime.MinValue"/> is used as an offset for the conversion.
    /// </summary>
    [ValueConversion(typeof(TimeSpan),typeof(DateTime))]
    public class TimeSpanToDateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            TimeSpan? ts = value as TimeSpan?;
            if (!ts.HasValue)
            {
                return Binding.DoNothing;
            }
            return DateTime.MinValue + ts;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime? dt = value as DateTime?;
            if (!dt.HasValue)
            {
                return Binding.DoNothing;
            }
            return dt.Value.Subtract(DateTime.MinValue);
        }
    }
}
