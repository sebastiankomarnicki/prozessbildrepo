﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Promess.Common.Converters
{
    /// <summary>
    /// One way universal value converter
    /// original source: http://blog.scottlogic.com/2010/07/09/a-universal-value-converter-for-wpf.html
    /// alternate url: http://www.codeproject.com/Articles/92944/A-Universal-Value-Converter-for-WPF
    /// </summary>
    public class UniversalValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType)
        {
            return Convert(value, targetType, null, null);
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            // obtain the conveter for the target type
            TypeConverter converter = TypeDescriptor.GetConverter(targetType);

            try
            {
                // determine if the supplied value is of a suitable type
                if (converter.CanConvertFrom(value.GetType()))
                {
                    // return the converted value
                    return converter.ConvertFrom(value);
                }
                else
                {
                    // try to convert from the string representation
                    return converter.ConvertFrom(value.ToString());
                }
            }
            catch (Exception)
            {
                return value;
            }
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }


    }
}
