﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Promess.Common.Converters
{
    /// <summary>
    /// One way converter to join all entries of a <c>string</c> collection on comma
    /// </summary>
    [ValueConversion(typeof(ICollection<string>), typeof(string))]
    public class FlattenStringCollectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;
            ICollection<string> toConvert = value as ICollection<string>;
            if (toConvert == null)
                return Binding.DoNothing;
            return String.Join(",",toConvert);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
