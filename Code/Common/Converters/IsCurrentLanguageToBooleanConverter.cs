﻿using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows;
using System.Linq;

namespace Promess.Common.Converters
{
    /// <summary>
    /// One way multi value converter to check if the provided <see cref="CultureInfo"/>s (languages) are equal
    /// </summary>
    [ValueConversion(typeof(CultureInfo[]), typeof(bool))]
	public class IsCurrentLanguageToBooleanConverter : IMultiValueConverter
	{

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Any(x => x == DependencyProperty.UnsetValue))
                return DependencyProperty.UnsetValue;
            if (values == null || values.Length != 2)
                return false;
            CultureInfo lang1 = values[0] as CultureInfo;
            CultureInfo lang2 = values[1] as CultureInfo;

            return lang1 == null ? false:lang1.Equals(lang2);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
	}
}
