﻿using System;
using System.Windows.Data;
using System.Globalization;
using Promess.Language;
using System.Windows;

namespace Promess.Common.Converters
{
    /// <summary>
    /// One way converter to retrieve the language name from the given <see cref="CultureInfo"/>
    /// </summary>
    [ValueConversion(typeof(CultureInfo), typeof(string))]
	public class LanguageNameConverter : IValueConverter
	{

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue)
                return value;

            CultureInfo currentLang = value as CultureInfo;
            string langName = LanguageManager.Current.TranslationProvider.GetLanguageDisplayName(currentLang);
            return langName?? currentLang.TwoLetterISOLanguageName;
        }

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}


	}
}
