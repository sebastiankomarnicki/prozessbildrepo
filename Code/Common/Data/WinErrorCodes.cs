﻿namespace Promess.Common.Data
{
    /// <summary>
    /// Some error codes for <see cref="System.Runtime.InteropServices.COMException"/>
    /// </summary>
    public static class WinErrorCodes
    {
        public const int E_PENDING = (int)(0x8000000A - 0x100000000);   
        public const int E_FAIL = (int)(0x80004005 - 0x100000000);
        public const int E_UNEXPECTED = (int)(0x8000FFFF - 0x100000000);
        public const int E_LAST = (int)(0x800401BF - 0x100000000);
        public const int E_ACCESSDENIED = (int)(0x80070005 - 0x100000000);
    }
}
