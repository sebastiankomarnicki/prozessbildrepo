﻿using Promess.Common.Services;
using Promess.Common.Util;
using Promess.Language;
using Promess.Pbld.Services;
using System;
using System.ComponentModel.Composition;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Waf.Applications.Services;
using System.Waf.Foundation;

namespace Promess.Authentication.Services
{
    /// <summary>
    /// Service for authentication
    /// </summary>
    [Export(typeof(IAuthenticationService)), PartCreationPolicy(CreationPolicy.Shared),Export]
    public class AuthenticationService : Model, IAuthenticationService
    {
        private InternalUserData _noDefinedUser = new InternalUserData("PROMESS", "¯­°²ª¬º­");
        
        private Users _users;
        private PathsAndResourceService _pathsResourceService;
        private IMessageService _messageService;
        private ILoggingService _loggingService;

        [ImportingConstructor]
        public AuthenticationService(IMessageService messageService, PathsAndResourceService pathsResourceService, ILoggingService loggingService)
        {
            this._pathsResourceService = pathsResourceService;
            this._messageService = messageService;
            this._loggingService = loggingService;
        }

        /// <summary>
        /// Initialize the service with an anonymous identity and get users defined in <see cref="PathsAndResourceService.Users"/>
        /// </summary>
        public void Initialize()
        {
            CustomPrincipal anonymousPrincipal = new CustomPrincipal();
            AppDomain.CurrentDomain.SetThreadPrincipal(anonymousPrincipal);
            try
            {
                this._users = SerializeHelper.DeserializeFromFile<Users>(_pathsResourceService.Users) ?? new Users();
            }
            catch 
            {
                this._users = new Users();
                _messageService.ShowError((string)LanguageManager.Current.CurrentLanguageData.Translate("ErrLoadUsers"));
            }
            
        }

        /// <summary>
        /// Try to authenticate the user
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="clearTextPassword">Password as clear text</param>
        /// <returns>Result of the authentication</returns>
        public AuthenticationResult AuthenticateUser(string username, string clearTextPassword)
        {
            var res = AuthenticateUserInternal(username, clearTextPassword);
            switch (res.Status)
            {
                case AuthenticationResult.AuthenticationStatus.PasswordUserInvalid:
                    _loggingService.Info(
                        String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("InfoInvalidLogin"),
                        username));
                    break;
                case AuthenticationResult.AuthenticationStatus.Valid:
                    _loggingService.Info(
                        String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("InfoLoggedIn"),
                        username));
                    break;
                default:
                    //exception in case a new enum value was added or unexpected occured, requires code change
                    throw new NotImplementedException();
            }
            return res;
        }

        /// <summary>
        /// Check if the user is in the user list or if this is list is empty for the standard user. Generate encoding of password against saved password.
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="clearTextPassword">Password as clear text</param>
        /// <returns>Result of the authentication</returns>
        private AuthenticationResult AuthenticateUserInternal(string username, string clearTextPassword)
        {
            string encodedPw = OriginalEncrypt(clearTextPassword);
            if (!_users.HasUsers())
            {
                if (_noDefinedUser.Username.Equals(username) && _noDefinedUser.HashedPassword.Equals(encodedPw))
                {
                    AuthenticateUserHelper(username);
                    return new ValidAuthenticationResult(new User(username));
                }else
                {
                    return new PasswordUserInvalidAuthenticationResult();
                }
            }else
            {
                if (_users.IsUserValid(username, encodedPw))
                {
                    AuthenticateUserHelper(username);
                    return new ValidAuthenticationResult(new User(username));
                }else
                {
                    return new PasswordUserInvalidAuthenticationResult();
                }
            }
        }

        /// <summary>
        /// Authenticate user by setting <see cref="Thread.CurrentPrincipal"/> to a <see cref="CustomIdentity"/> of the user
        /// </summary>
        /// <param name="username">User name for the identity</param>
        private void AuthenticateUserHelper(string username)
        {
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            if (customPrincipal == null)
                throw new ArgumentException("The application's default thread principal must be set to a CustomPrincipal object on startup.");

            //Authenticate the user
            customPrincipal.Identity = new CustomIdentity(username);
            RaisePropertyChanged(nameof(IsAuthenticated));
            RaisePropertyChanged(nameof(CurrentUserName));
        }

        /// <summary>
        /// Logout by setting <see cref="Thread.CurrentPrincipal"/> to <see cref="AnonymousIdentity"/>
        /// </summary>
        public void Logout()
        {
            //Get the current principal object
            CustomPrincipal customPrincipal = Thread.CurrentPrincipal as CustomPrincipal;
            if (customPrincipal == null)
                throw new ArgumentException("The application's default thread principal must be set to a CustomPrincipal object on startup.");
            _loggingService.Info(
                String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("InfoLoggedOff"),
                CurrentUserName));
            //Authenticate the user
            customPrincipal.Identity = new AnonymousIdentity();
            RaisePropertyChanged(nameof(IsAuthenticated));
            RaisePropertyChanged(nameof(CurrentUserName));
        }

        /// <summary>
        /// Try to create a new user with the given name and password. Persist changes.
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="clearTextPassword">Password as clear text</param>
        /// <returns>Result of the operation</returns>
        public AuthenticationResult CreateUser(string username, string clearTextPassword)
        {
            if (_users.TryAddUser(username, OriginalEncrypt(clearTextPassword)))
            {
                SerializeHelper.SerializeToFile<Users>(_pathsResourceService.Users, _users);
                _loggingService.Info(
                    String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("InfoCreatedUser"),
                    CurrentUserName, username));
                return new ValidAuthenticationResult(new User(username));
            }
            else
            {
                _loggingService.Info(
                    String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("InfoDuplicateUser"),
                    CurrentUserName, username));
                return new UserAlreadyExistsAuthenticationResult();
            }  
        }

        /// <summary>
        /// Original encrypt funtion of the old process picture. A simple XOR.
        /// </summary>
        /// <param name="clearTextPassword">Password as clear text</param>
        /// <returns>Encrypted password</returns>
        public static string OriginalEncrypt(string clearTextPassword)
        {
            //Warning: this is dangerous for different cultures
            char[] tmpPwd = clearTextPassword.Trim().ToUpperInvariant().ToCharArray();
            for (int i = 0; i < tmpPwd.Length; i++)
            {
                tmpPwd[i] = (char)((int) tmpPwd[i] ^ 0xFF);
            }

            return new string(tmpPwd);
        }

        /// <summary>
        /// Alternate, simple, method for encrypting a password. Use salt and generate hash.
        /// </summary>
        /// <param name="clearTextPassword">Password as clear text</param>
        /// <param name="salt">Salt for the hash algorithm</param>
        /// <returns>Hashed password</returns>
        private string CalculateHash(string clearTextPassword, string salt)
        {
            // Convert the salted password to a byte array
            byte[] saltedHashBytes = Encoding.UTF8.GetBytes(clearTextPassword + salt);
            // Use the hash algorithm to calculate the hash
            HashAlgorithm algorithm = new SHA256Managed();
            byte[] hash = algorithm.ComputeHash(saltedHashBytes);
            // Return the hash as a base64 encoded string to be compared to the stored password
            return Convert.ToBase64String(hash);
        }

        public bool IsAuthenticated
        {
            get { return Thread.CurrentPrincipal.Identity.IsAuthenticated; }
        }

        public String CurrentUserName
        {
            get
            {
                if (IsAuthenticated)
                    return Thread.CurrentPrincipal.Identity.Name;
                return String.Empty;
            }
        }

    }
}
