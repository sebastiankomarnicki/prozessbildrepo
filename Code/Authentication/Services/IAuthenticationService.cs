﻿using System;

namespace Promess.Authentication.Services
{
    /// <summary>
    /// Simple interface for a service providing authentification 
    /// </summary>
    public interface IAuthenticationService
    {
        AuthenticationResult CreateUser(string username, string clearTextPassword);

        AuthenticationResult AuthenticateUser(string username, string password);

        String CurrentUserName { get; }

        bool IsAuthenticated { get; }
    }
}
