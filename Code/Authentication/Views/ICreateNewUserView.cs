﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Waf.Applications;

namespace Promess.Authentication.Views
{
    public interface ICreateNewUserView:IView
    {
        void ShowDialog(object owner);

        void Close();
    }
}
