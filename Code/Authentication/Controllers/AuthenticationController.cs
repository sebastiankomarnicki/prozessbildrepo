﻿using Promess.Authentication.ViewModels;
using Promess.Common.Services;
using Promess.Language;
using Promess.Pbld.Services;
using Promess.Pbld.ViewModels;
using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Waf.Applications;

namespace Promess.Authentication.Controllers
{
    /// <summary>
    /// Controller for setting up data flow and navigation between main model of application and authentication functionality
    /// </summary>
    [Export(typeof(IModuleController))]
    internal class AuthenticationController:IModuleController
    {
        private readonly IMainService _mainService;
        private readonly Services.AuthenticationService _authenticationService;
        private readonly ILoggingService _loggingService;

        private readonly MainViewModel _mainViewModel;

        private readonly ExportFactory<LoginViewModel> _loginViewModelFactory;
        private readonly ExportFactory<CreateNewUserViewModel> _createNewUserViewModelFactory;

        private DelegateCommand _showLoginViewCommand;
        private DelegateCommand _showCreateUserViewCommand;
        private DelegateCommand _logoutCommand;

        [ImportingConstructor]
        public AuthenticationController(IMainService mainService, Services.AuthenticationService authenticationService, ILoggingService loggingService,
            MainViewModel mainViewModel,
            ExportFactory<LoginViewModel> loginViewModelFactory, ExportFactory<CreateNewUserViewModel> createNewUserViewModelFactory)
        {
            this._mainService = mainService;
            this._authenticationService = authenticationService;
            this._loggingService = loggingService;

            this._mainViewModel = mainViewModel;

            this._loginViewModelFactory = loginViewModelFactory;
            this._createNewUserViewModelFactory = createNewUserViewModelFactory;


        }

        public void Initialize()
        {
            _authenticationService.Initialize();
            this._showLoginViewCommand = new DelegateCommand(ShowLoginWindow, () => !_authenticationService.IsAuthenticated);
            this._showCreateUserViewCommand = new DelegateCommand(ShowCreateNewUserWindow, () => _authenticationService.IsAuthenticated);
            this._logoutCommand = new DelegateCommand(this._authenticationService.Logout, () => _authenticationService.IsAuthenticated);
            PropertyChangedEventManager.AddHandler(_authenticationService,
                new EventHandler<PropertyChangedEventArgs>((sender, e) => {
                    UpdateCommands();
                    //TODO when the solution is restructured change the binding in the main project to use the authenticationservice.isauthenticated
                    SetIsAuthenticatedForMainProject();
                }), nameof(Services.AuthenticationService.IsAuthenticated));
            PropertyChangedEventManager.AddHandler(_authenticationService,
                new EventHandler<PropertyChangedEventArgs>((sender, e) => {
                    _mainService.CurrentUserName = _authenticationService.CurrentUserName;
                }), nameof(Services.AuthenticationService.CurrentUserName));

            this._mainService.ShowLoginViewCommand = this._showLoginViewCommand;
            this._mainService.ShowCreateNewUserViewCommand = this._showCreateUserViewCommand;
            this._mainService.LogoutCommand = this._logoutCommand;
        }

        private void SetIsAuthenticatedForMainProject()
        {
            _mainService.IsAuthenticated = _authenticationService.IsAuthenticated;
        }

        public void Run()
        {
        }

        public void Shutdown()
        {
        }

        private void ShowLoginWindow()
        {
            var loginViewModel = _loginViewModelFactory.CreateExport().Value;
            loginViewModel.ShowDialog(_mainService.MainView);
        }

        private void ShowCreateNewUserWindow()
        {
            var createNewUserViewModel = _createNewUserViewModelFactory.CreateExport().Value;
            if (_mainService.IsAuthenticated)
            {
                //should always be authenticated when getting into the show part
                _loggingService.Info(
                    String.Format((string)LanguageManager.Current.CurrentLanguageData.Translate("InfoOpenedNewUser"),
                    _mainService.CurrentUserName));
            }
            createNewUserViewModel.ShowDialog(_mainService.MainView);
        }

        private void UpdateCommands()
        {
            _showLoginViewCommand.RaiseCanExecuteChanged();
            _showCreateUserViewCommand.RaiseCanExecuteChanged();
            _logoutCommand.RaiseCanExecuteChanged();
        }
    }
}
