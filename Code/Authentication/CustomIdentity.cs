﻿using System.Security.Principal;

namespace Promess.Authentication
{
    /// <summary>
    /// Representation of user identity
    /// </summary>
    public class CustomIdentity : IIdentity
    {
        public CustomIdentity(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }

        #region IIdentity Members
        /// <summary>
        /// Unused authentication type, included because of interface
        /// </summary>
        public string AuthenticationType { get { return "Custom authentication"; } }

        public bool IsAuthenticated { get { return !string.IsNullOrEmpty(Name); } }
        #endregion
    }
}
