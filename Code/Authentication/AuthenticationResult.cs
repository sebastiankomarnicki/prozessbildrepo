﻿using System;

namespace Promess.Authentication
{
    /// <summary>
    /// Abstract base for information about an authentication 
    /// </summary>
    public abstract class AuthenticationResult
    {
        public enum AuthenticationStatus
        {
            Valid,
            PasswordUserInvalid,
            UserAlreadyExists
        }

        public AuthenticationStatus Status { get; private set; }

        protected AuthenticationResult(AuthenticationStatus status)
        {
            Status = status;
        }
    }

    public class PasswordUserInvalidAuthenticationResult:AuthenticationResult
    {
        public PasswordUserInvalidAuthenticationResult() : base(AuthenticationStatus.PasswordUserInvalid) { }
    }

    public class UserAlreadyExistsAuthenticationResult:AuthenticationResult
    {
        public UserAlreadyExistsAuthenticationResult() : base(AuthenticationStatus.UserAlreadyExists) { }
    }

    public class ValidAuthenticationResult:AuthenticationResult
    { 
        public User User { get; private set; }

        /// <summary>
        /// Create a result for a successful authentication wrapping the authenticated user
        /// </summary>
        /// <param name="user"></param>
        public ValidAuthenticationResult(User user):base(AuthenticationStatus.Valid)
        {
            if (user == null) throw new ArgumentNullException("user");
            User = user;
        }
    }

}
