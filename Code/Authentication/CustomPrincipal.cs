﻿using System;
using System.Security.Principal;

namespace Promess.Authentication
{
    /// <summary>
    /// Representation of user authenticationy. Roles not yet needed, not implemented.
    /// </summary>
    public class CustomPrincipal : IPrincipal
    {
        private CustomIdentity _identity;

        public CustomIdentity Identity
        {
            get { return _identity ?? new AnonymousIdentity(); }
            set { _identity = value; }
        }

        #region IPrincipal Members
        IIdentity IPrincipal.Identity
        {
            get { return this.Identity; }
        }

        /// <summary>
        /// Not implemented, required by <see cref="IPrincipal"/>
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException">Not implemented</exception>
        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
