﻿using Promess.Authentication.Services;
using Promess.Authentication.Views;
using Promess.Common.Services;
using Promess.Language;
using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Waf.Applications;
using System.Windows.Controls;
using System.Windows.Input;

namespace Promess.Authentication.ViewModels
{
    /// <summary>
    /// View model for logging in
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class LoginViewModel:ViewModel<ILoginView>
    {
        private readonly IAuthenticationService _authenticationService;

        private readonly DelegateCommand _loginCommand;

        private string _username;
        private string _status;

        [ImportingConstructor]
        public LoginViewModel(ILoginView view, IAuthenticationService authenticationService, ILoggingService loggingService):base(view)
        {
            this._authenticationService = authenticationService;
            this._loginCommand = new DelegateCommand(TryLogin, param => CanTryLogin());
        }

        private void UpdateCommands()
        {
            _loginCommand.RaiseCanExecuteChanged();
        }

        [StringLength(20, MinimumLength = 1)]
        public string Username
        {
            get { return _username; }
            set { if (SetProperty(ref _username, value))
                {
                    UpdateCommands();//or register PropertyChangedEventManager in constructor or create user class:ValidationModel and use IDataErrorInfo
                }
            }
        }

        /// <summary>
        /// Translated feedback from when executing <see cref="LoginCommand"/>
        /// </summary>
        public string Status
        {
            get { return _status; }
            set { SetProperty(ref _status, value); }
        }

        public ICommand LoginCommand
        {
            get { return _loginCommand; }
        }

        public void ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
        }

        private bool CanTryLogin()
        {
            return !(String.IsNullOrWhiteSpace(Username));
        }

        private void TryLogin(object parameter)
        {
            PasswordBox passwordBox = parameter as PasswordBox;
            string clearTextPassword = passwordBox.Password.Trim().ToUpper();
            string usedUserName = Username.Trim().ToUpper();
            try
            {
                if (String.IsNullOrEmpty(usedUserName))
                {
                    Status = LanguageManager.Current.CurrentLanguageData.Translate("MissingUsername") as String;
                    return;
                }
                if (String.IsNullOrEmpty(clearTextPassword))
                {
                    Status = LanguageManager.Current.CurrentLanguageData.Translate("MissingPassword") as String;
                    return;
                }
                //Validate credentials through the authentication service
                AuthenticationResult ar = _authenticationService.AuthenticateUser(usedUserName, clearTextPassword);
                switch (ar.Status)
                {
                    case AuthenticationResult.AuthenticationStatus.PasswordUserInvalid:
                        Status = LanguageManager.Current.CurrentLanguageData.Translate("InvalidUserOrPwd") as String;
                        return;
                    case AuthenticationResult.AuthenticationStatus.Valid:
                        ViewCore.Close();
                        return;
                    default:
                        throw new InvalidOperationException("Internal error, unexpected AuthenticationStatus.");
                }
            }
            catch (Exception)
            {
                Status = LanguageManager.Current.CurrentLanguageData.Translate("UnexpectedError") as String;
            }
        }
    }
}
