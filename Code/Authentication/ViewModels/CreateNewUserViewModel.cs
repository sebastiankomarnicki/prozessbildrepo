﻿using Promess.Authentication.Services;
using Promess.Authentication.Views;
using Promess.Language;
using System;
using System.ComponentModel.Composition;
using System.ComponentModel.DataAnnotations;
using System.Waf.Applications;
using System.Windows.Controls;
using System.Windows.Input;

namespace Promess.Authentication.ViewModels
{
    /// <summary>
    /// View model for creating a new user
    /// </summary>
    [Export, PartCreationPolicy(CreationPolicy.NonShared)]
    public class CreateNewUserViewModel : ViewModel<ICreateNewUserView>
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly DelegateCommand _createNewUserCommand;

        private string _username;
        private string _status;

        [ImportingConstructor]
        public CreateNewUserViewModel(ICreateNewUserView view, IAuthenticationService authenticationService) : base(view)
        {
            this._authenticationService = authenticationService;
            this._createNewUserCommand = new DelegateCommand(TryCreateNewUser, param => CanTryCreateNewUser());
        }

        public void ShowDialog(object owner)
        {
            ViewCore.ShowDialog(owner);
        }

        [StringLength(20, MinimumLength = 1)]
        public string Username
        {
            get { return _username; }
            set
            {
                if (SetProperty(ref _username, value))
                {
                    _createNewUserCommand.RaiseCanExecuteChanged();
                }
            }
        }

        /// <summary>
        /// Translated feedback from when executing <see cref="CreateNewUserCommand"/>
        /// </summary>
        public string Status
        {
            get { return _status; }
            private set { SetProperty(ref _status, value); }
        }

        public ICommand CreateNewUserCommand
        {
            get { return _createNewUserCommand; }
        }

        private bool CanTryCreateNewUser()
        {
            return !(String.IsNullOrWhiteSpace(Username));
        }

        private void TryCreateNewUser(object parameter)
        {
            object[] parameters = parameter as object[];
            PasswordBox pb1 = parameters[0] as PasswordBox;
            PasswordBox pb2 = parameters[1] as PasswordBox;
            string clearTextPassword1 = pb1.Password.Trim().ToUpper();
            string clearTextPassword2 = pb2.Password.Trim().ToUpper();
            //use invariant version of ToUpper? problem with already used usernames?
            string usedUserName = Username.Trim().ToUpper();
            if (String.IsNullOrEmpty(usedUserName))
            {
                Status = LanguageManager.Current.CurrentLanguageData.Translate("MissingUsername") as String;
                return;
            }
            if (String.IsNullOrEmpty(clearTextPassword1) || String.IsNullOrEmpty(clearTextPassword1))
            {
                Status = LanguageManager.Current.CurrentLanguageData.Translate("MissingPassword") as String;
                return;
            }
            if (!clearTextPassword1.Equals(clearTextPassword2))
            {
                Status = LanguageManager.Current.CurrentLanguageData.Translate("UnequalPassword") as String;
                return;
            }
            try
            {
                AuthenticationResult ar = _authenticationService.CreateUser(usedUserName, clearTextPassword1);
                switch (ar.Status)
                {
                    case AuthenticationResult.AuthenticationStatus.UserAlreadyExists:
                        Status = LanguageManager.Current.CurrentLanguageData.Translate("InvalidUserOrPwd") as String;
                        return;
                    case AuthenticationResult.AuthenticationStatus.Valid:
                        ViewCore.Close();
                        return;
                    default:
                        throw new InvalidOperationException("Internal error, unexpected AuthenticationStatus.");
                }
            }
            catch (Exception)
            {
                Status = LanguageManager.Current.CurrentLanguageData.Translate("UnexpectedError") as String;
            }
        }
    }
}
