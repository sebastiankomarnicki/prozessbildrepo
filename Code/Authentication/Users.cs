﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Promess.Authentication
{
    /// <summary>
    /// Data class for handling and persisting users.
    /// </summary>
    [DataContract(Name ="Users")]
    public sealed class Users
    {
        [DataMember(Name = "KnownUsers")]
        private List<InternalUserData> _users;

        public Users()
        {
            this._users = new List<InternalUserData>();
        }

        public bool TryAddUser(string username, string encryptedPassword)
        {
            if (_users.Any(elem => elem.Username.Equals(username)))
                return false;
            _users.Add(new InternalUserData(username, encryptedPassword));
            return true;
        }

        public bool HasUsers()
        {
            return _users.Count > 0;
        }

        public bool IsUserValid(string username, string encryptedPassword)
        {
            return _users.Any(user => user.Username.Equals(username) && user.HashedPassword.Equals(encryptedPassword));
        }

        [OnDeserialized]
        void OnDeserialized(StreamingContext context)
        {
            if (this._users == null)
                _users = new List<InternalUserData>();
        }
    }
}
