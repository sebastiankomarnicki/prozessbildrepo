﻿namespace Promess.Authentication
{
    /// <summary>
    /// Provide an anonymous identity
    /// </summary>
    public class AnonymousIdentity : CustomIdentity
    {
        public AnonymousIdentity()
            : base(string.Empty)
        { }
    }
}
