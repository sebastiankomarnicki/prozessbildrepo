﻿using System.Runtime.Serialization;

namespace Promess.Authentication
{
    [DataContract(Name = "User")]
    internal sealed class InternalUserData
    {
        [DataMember(Name = "Username")]
        private string _username;
        [DataMember(Name = "Password")]
        private string _hashedPassword;

        public InternalUserData(string username, string hashedPassword)
        {
            Username = username;
            HashedPassword = hashedPassword;
        }
        public string Username
        {
            get { return _username; }
            private set { _username = value; }
        }

        public string HashedPassword
        {
            get { return _hashedPassword; }
            private set { _hashedPassword = value; }
        }
    }
}
