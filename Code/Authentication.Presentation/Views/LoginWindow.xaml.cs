﻿using Promess.Authentication.Views;
using System;
using System.ComponentModel.Composition;
using System.Windows;

namespace Promess.Pbld.Authentication.Presentation.Views
{
    /// <summary>
    /// Interaction logic for LoginWindow2.xaml
    /// </summary>
    [Export(typeof(ILoginView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class LoginWindow : Window, ILoginView
    {
        protected static DependencyProperty InitialMinHeightProperty = DependencyProperty.Register(
        "InitialMinHeight", typeof(double), typeof(LoginWindow)
        );

        protected static DependencyProperty InitialMinWidthProperty = DependencyProperty.Register(
            "InitialMinWidth", typeof(double), typeof(LoginWindow)
            );

        public LoginWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Window_Loaded);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitialMinHeight = this.ActualHeight;
            InitialMinWidth = this.ActualWidth;
        }

        protected double InitialMinHeight
        {
            get { return (Double)GetValue(InitialMinHeightProperty); }
            set { SetValue(InitialMinHeightProperty, value); }
        }

        protected double InitialMinWidth
        {
            get { return (Double)GetValue(InitialMinWidthProperty); }
            set { SetValue(InitialMinWidthProperty, value); }
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
