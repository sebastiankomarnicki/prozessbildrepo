﻿using System;
using System.ComponentModel.Composition;
using System.Security.Permissions;
using System.Windows;
using Promess.Authentication.Views;

namespace Promess.Pbld.Authentication.Presentation.Views
{
    /// <summary>
    /// Interaction logic for CreateNewUserWindow.xaml
    /// </summary>
    [PrincipalPermission(SecurityAction.Demand)]
    [Export(typeof(ICreateNewUserView)), PartCreationPolicy(CreationPolicy.NonShared)]
    public partial class CreateNewUserWindow : Window, ICreateNewUserView
    {
        protected static DependencyProperty InitialMinHeightProperty = DependencyProperty.Register(
            "InitialMinHeight", typeof(double), typeof(CreateNewUserWindow)
            );

        protected static DependencyProperty InitialMinWidthProperty = DependencyProperty.Register(
            "InitialMinWidth", typeof(double), typeof(CreateNewUserWindow)
            );

        public CreateNewUserWindow()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Window_Loaded);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitialMinHeight = this.ActualHeight;
            InitialMinWidth = this.ActualWidth;
        }

        protected double InitialMinHeight
        {
            get { return (Double)GetValue(InitialMinHeightProperty); }
            set { SetValue(InitialMinHeightProperty, value); }
        }

        protected double InitialMinWidth
        {
            get { return (Double)GetValue(InitialMinWidthProperty); }
            set { SetValue(InitialMinWidthProperty, value); }
        }

        public void ShowDialog(object owner)
        {
            Owner = owner as Window;
            ShowDialog();
        }
    }
}
