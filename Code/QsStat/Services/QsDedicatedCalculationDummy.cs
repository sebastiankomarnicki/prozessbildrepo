﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using Promess.QsStat.Data;

namespace Promess.QsStat.Services
{
    internal sealed class QsDedicatedCalculationDummy : IQsDedicatedCalculation, INotifyPropertyChanged
    {
#pragma warning disable 0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore 0067

        public QsDedicatedLoaded LoadedDataset => QsDedicatedCalculation.NODATA;

        public Tuple<int, QsStatPartResultDTO> CurrentData => Tuple.Create(-1, new QsStatPartResultDTO(Guid.Empty));

        public IReadOnlyDictionary<SpcPath, FolderDatasets> SpcPathFolderDatasets => new Dictionary<SpcPath,FolderDatasets>();

        public Task<QsStatPartResultDTO> RequestAllData(QsDedicatedLoaded loaded, QsStatRetrievalItemsMultipleCharacteristics toRetrieve, int selectedCharacteristic, Size displaySize, CancellationToken ct)
        {
            throw new TaskCanceledException();
        }

        public Task<QsDedicatedLoaded> RequestLoadData(SpcPath spcPath, Guid dataGuid, CancellationToken ct)
        {
            return Task.FromResult(LoadedDataset);
        }

        public void Shutdown()
        {
        }

        public Task UpdateSettings(GeneralSettings generalSettings)
        {
            return Task.CompletedTask;
        }

        public Task UpdateSpc(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders)
        {
            return Task.CompletedTask;
        }

        Task<QsStatPartResultDTO> IQsDedicatedCalculation.RequestAllData(QsDedicatedLoaded loaded, QsStatRetrievalItemsMultipleCharacteristics toRetrieve, int selectedCharacteristic, Size displaySize, CancellationToken ct)
        {
            return Task.FromResult(new QsStatPartResultDTO(loaded.DatasetGuid));
        }

        public Task<int> GetClickedValueNr(int graphicNr, int characteristicNumber, double width, double height, double x, double y)
        {
            return Task.FromResult(0);
        }

        public (QsDedicatedLoaded loadedDataset, int? selectedCharacteristicNumber) GetCurrentDataSelection()
        {
            return (LoadedDataset, null);
        }

        public void Dispose()
        {
        }
    }
}
