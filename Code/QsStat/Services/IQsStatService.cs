﻿using Promess.QsStat.Data;
using System.Collections.Generic;

namespace Promess.QsStat.Services
{
    /// <summary>
    /// Provides general information about stStat and evaluated data
    /// </summary>
    public interface IQsStatService
    {
        /// <summary>
        /// Available qdas part K-fields for querying in the form of query key and name in the current language
        /// </summary>
        IReadOnlyDictionary<int, string> PartKFields { get; }
        /// <summary>
        /// Available qdas characteristic K-fields for querying in the form of query key and name in the current language
        /// </summary>
        IReadOnlyDictionary<int, string> CharacteristicKFields { get; }
        /// <summary>
        /// Available qdas ids for querying evaluation results in the form of query key and name in the current language
        /// </summary>
        IReadOnlyDictionary<int, string> StatResult { get; }
        /// <summary>
        /// Available qdas ids for querying graphic results in the form of query key and name in the current language
        /// </summary>
        IReadOnlyDictionary<int, string> Graphics { get; }

        /// <summary>
        /// Version information about qsStat
        /// </summary>
        QsStatVersion QCOMVersion { get; }

        /// <summary>
        /// Current evaluation state
        /// </summary>
        QsStatState QsStatState { get; }
    }
}
