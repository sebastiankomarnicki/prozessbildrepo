﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Promess.Common.Util.Async;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using Promess.QsStat.Data;

namespace Promess.QsStat.Services
{
    internal class QsPeriodicalCalculationDummy : IQsPeriodicalCalculation
    { 

        public Task ShutdownAsync()
        {
            return Task.CompletedTask;
        }

        public void Start(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, IReadOnlyCollection<PanelItemModel> panelItemModels, GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, ListViewSettings listViewSettings, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> data)
        {
            return;
        }

        public void UpdateGeneralRetrievalKeys(ListViewSettings settings)
        {
        }

        public void UpdatePanelItemModels(IReadOnlyCollection<PanelItemModel> panelItemModels)
        {
            return;
        }

        public void SetPanelHeightAndWidth(string panelName, double height, double width)
        {
            return;
        }

        public void UpdateSettings(GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> spcPathResults)
        {
            return;
        }

        public void UpdateSpc(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> evaluationData)
        {
            return;
        }
        public Task ManualRequest(SpcPath spcPath, ManualRequestTypes manualRequest)
        {
            return Task.CompletedTask;
        }
    }
}
