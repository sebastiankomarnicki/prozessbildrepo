﻿using Promess.Pbld.Data;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using Promess.QsStat.Data;
using Promess.QsStat.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Foundation;
using System.Windows;

namespace Promess.QsStat.Services
{
    public class QsDedicatedCalculation:Model,IQsDedicatedCalculation, IDisposable
    {
        public static readonly QsDedicatedLoaded NODATA = new QsDedicatedLoaded(new SpcPath(-1, ""), Guid.Empty, new QueryableQsData(), 0, new List<int>().AsReadOnly());

        private readonly PoolItem<int> _connection;
        private readonly ResourceService<QsStatWrapper> _qsStatService;
        private readonly object _lock;

        private CancellationTokenSource _loadDataCts;
        private CancellationTokenSource _getDataCts;
        private CancellationTokenSource _shutdownCts;
        private QsDedicatedLoaded _loadedDataset;
        private QsStatPartResultDTO _currentData;
        private int? _currentCharacteristicNumber;
        private GeneralSettings _generalSettings;
        private IReadOnlyDictionary<SpcPath, FolderDatasets> _spcPathFolders;
        private EventHandler<PropertyChangedEventArgs> _currentFoldersChangedHandler;       

        private QsDedicatedCalculation(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, ResourceService<QsStatWrapper> qsStatService, PoolItem<int> connection, GeneralSettings generalSettings, QsDedicatedLoaded presetLoaded)
        { 
            this._qsStatService = qsStatService;
            this._connection = connection;
            this._shutdownCts = new CancellationTokenSource();
            this._getDataCts = new CancellationTokenSource();
            this._loadDataCts = new CancellationTokenSource();
            this._loadedDataset = presetLoaded;
            this._currentData = new QsStatPartResultDTO(presetLoaded.DatasetGuid);
            this._currentCharacteristicNumber = null;
            this._lock = new object();
            this._generalSettings = generalSettings;
            this._spcPathFolders = spcPathFolders;
        }

        internal static async Task<QsDedicatedCalculation> CreateAsync(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, ResourceService<QsStatWrapper> qsStatService, ResourcePool<int> connectionPool, GeneralSettings generalSettings, QsDedicatedLoaded tryReload=null, CancellationToken ct = default)
        {
            //TODO maybe use a worker task again for queued work for an async shutdown using taskcompletionsource?
            QsDedicatedCalculation result;
            var connection = await connectionPool.GetResourceAsync(ct).ConfigureAwait(false);
            if (tryReload is object)
            {
                if (spcPathFolders.TryGetValue(tryReload.SpcPath, out FolderDatasets folderDs)){
                    if (folderDs.DataSets.Any(item=>Guid.Equals(item.Guid, tryReload.DatasetGuid)))
                    {
                        result = new QsDedicatedCalculation(spcPathFolders, qsStatService, connection, generalSettings, tryReload);
                        result._currentFoldersChangedHandler = result.GenerateFoldersChangedHandler(tryReload.SpcPath, tryReload.DatasetGuid);
                        PropertyChangedEventManager.AddHandler(folderDs, result._currentFoldersChangedHandler, nameof(FolderDatasets.DataSets));
                        //just kickoff loading
                        _ = result.RequestLoadData(tryReload.SpcPath, tryReload.DatasetGuid);
                        return result;
                    }
                }
            } 
            result = new QsDedicatedCalculation(spcPathFolders, qsStatService, connection, generalSettings, QsDedicatedCalculation.NODATA);
            if (spcPathFolders.Any())
            {
                var minSpc = spcPathFolders.Keys.Min(elem => elem.SpcNumber);
                var spcPath = spcPathFolders.Keys.First(elem => elem.SpcNumber == minSpc);
                if (spcPathFolders.TryGetValue(spcPath, out FolderDatasets folderDs))
                {
                    var ds = folderDs.DataSets.FirstOrDefault();
                    result._currentFoldersChangedHandler = result.GenerateFoldersChangedHandler(spcPath, ds?.Guid??Guid.Empty);
                    PropertyChangedEventManager.AddHandler(folderDs, result._currentFoldersChangedHandler, nameof(FolderDatasets.DataSets));                   
                    if (ds is object)
                    {
                        //just kickoff loading
                        _ = result.RequestLoadData(spcPath, ds.Guid);
                    }
                }
            }
            return result;
        }

        internal void ResetData()
        {
            lock (_lock)
            {
                this._loadDataCts.Cancel();
                this._getDataCts.Cancel();
                LoadedDataset = QsDedicatedCalculation.NODATA;
                this._currentCharacteristicNumber = null;
            }
        }

        /// <inheritdoc/>
        public async Task<QsDedicatedLoaded> RequestLoadData(SpcPath spcPath, Guid dataGuid, CancellationToken ct = default)
        {
            this._shutdownCts.Token.ThrowIfCancellationRequested();
            var newCts = new CancellationTokenSource();
            GeneralSettings settings;
            FolderDatasets folders = null;
            DataSet ds;
            QsDedicatedLoaded result;

            lock (_lock)
            {
                this._loadDataCts.Cancel();
                this._getDataCts.Cancel();
                settings = this._generalSettings;
                if (!this._spcPathFolders.TryGetValue(spcPath, out folders))
                    throw new TaskCanceledException();

                this._currentCharacteristicNumber = null;
                if (this._loadedDataset.SpcPath != spcPath)
                {
                    if (this._currentFoldersChangedHandler != null)
                        PropertyChangedEventManager.RemoveHandler(folders, this._currentFoldersChangedHandler, nameof(FolderDatasets.DataSets));
                    this._currentFoldersChangedHandler = GenerateFoldersChangedHandler(spcPath, dataGuid);
                    PropertyChangedEventManager.AddHandler(folders, this._currentFoldersChangedHandler, nameof(FolderDatasets.DataSets));
                }
                var datasets = folders.DataSets;
                ds = folders.DataSets.FirstOrDefault(item => item.Guid.Equals(dataGuid));
                
                if (ds == null)
                {
                    result = QsDedicatedCalculation.NODATA;
                    LoadedDataset = result;
                    return result;
                }
                this._loadDataCts = newCts;   
            }
            var data = ds.GetQueryableQsDataAsync();

            try
            {
                using (var lts = CancellationTokenSource.CreateLinkedTokenSource(newCts.Token, this._shutdownCts.Token, ct))
                {
                    result = await this._qsStatService.Enqueue(
                    async (qsStat) =>
                    {
                        var retrievedData = await data.ConfigureAwait(false);
                        return await LoadDataInternalAsync(qsStat, this._connection, spcPath, dataGuid, retrievedData, settings, lts.Token).ConfigureAwait(false);
                    }, PriorityType.High).ConfigureAwait(false);
                }
            }catch (ObjectDisposedException)
            {
                throw new TaskCanceledException();
            }       
            lock (_lock)
            {
                newCts.Token.ThrowIfCancellationRequested();
                LoadedDataset = result;
                this._currentData = new QsStatPartResultDTO(dataGuid);
                return result;
            }
        }

        private EventHandler<PropertyChangedEventArgs> GenerateFoldersChangedHandler(SpcPath spcPath, Guid dataGuid)
        {
            return new EventHandler<PropertyChangedEventArgs>((s, e) =>
            {
                lock (_lock)
                {
                    if (!Guid.Equals(this.LoadedDataset.DatasetGuid, dataGuid))
                        return;
                }
                DataSet toUse = null;
                if (this._spcPathFolders.TryGetValue(spcPath, out FolderDatasets fs))
                {
                    var datasets = fs.DataSets;
                    toUse = datasets.FirstOrDefault(item => item.Guid.Equals(dataGuid));
                    if (toUse is null)
                    {
                        toUse = datasets.FirstOrDefault();
                    }                  
                    if (toUse is object)
                    {
                        //just kickoff loading
                        _ = RequestLoadData(spcPath, toUse.Guid);
                    }
                        
                }
                //do nothing here, might only happen if spcPathFolders was replaced and then this handler is invalid anyway                 
            });
        }

        /// <inheritdoc/>
        public async Task<QsStatPartResultDTO> RequestAllData(QsDedicatedLoaded loaded, QsStatRetrievalItemsMultipleCharacteristics toRetrieve, int selectedCharacteristic, Size displaySize, CancellationToken ct = default)
        {
            var newCts = new CancellationTokenSource();
            CancellationToken loadDataToken;
            QsDedicatedLoaded characteristicsInfo;
            QsStatPartResultDTO result;
            lock (_lock)
            {
                characteristicsInfo = this._loadedDataset;
                if (!characteristicsInfo.Equals(loaded))
                    throw new TaskCanceledException();

                this._getDataCts.Cancel();
                this._getDataCts = newCts;
                loadDataToken = this._loadDataCts.Token;            
            }

            if (characteristicsInfo.EnabledCharacteristics.Any())
            {
                if (selectedCharacteristic < 1)
                    selectedCharacteristic = characteristicsInfo.EnabledCharacteristics.First();

                (bool incomplete, QueriedData queried) = GetAllData(characteristicsInfo, toRetrieve, selectedCharacteristic);

                toRetrieve = GenerateNewRetrievalItems(queried, toRetrieve);

                if (incomplete)
                {
                    using (var linkedTokenSource = CancellationTokenSource.CreateLinkedTokenSource(newCts.Token, loadDataToken, this._shutdownCts.Token, ct))
                    {
                        result = await this._qsStatService.Enqueue(
                            (qsStat) =>
                            {
                                return GetAllData(qsStat, this._connection, toRetrieve, characteristicsInfo, selectedCharacteristic, displaySize, newCts.Token);
                            }, PriorityType.High).ConfigureAwait(false);
                        linkedTokenSource.Token.ThrowIfCancellationRequested();
                    }
                }
                else
                {
                    result = new QsStatPartResultDTO(characteristicsInfo.DatasetGuid);
                }

                result = MergeQueriedData(characteristicsInfo, queried, result);
            }
            else
            {
                result = new QsStatPartResultDTO(characteristicsInfo.DatasetGuid);
                selectedCharacteristic = -1;
            }
            
            lock (_lock)
            {
                newCts.Token.ThrowIfCancellationRequested();
                loadDataToken.ThrowIfCancellationRequested();
                this._currentData = result;
                if (selectedCharacteristic > 0)
                {
                    this._currentCharacteristicNumber = selectedCharacteristic;
                }
                else
                {
                    this._currentCharacteristicNumber = null;
                }
                return result;
            }
        }

        /// <summary>
        /// Merges the given query data. Assumes that there are no duplicate keys in the provided data
        /// and that qsQueried only contains data from qsStat (no measuring results for part or characteristic)
        /// </summary>
        /// <param name="characteristicsInfo">Information about to nerge data, in case <paramref name="qsQueried"/> is not filled</param>
        /// <param name="queried">Query result from parsed data</param>
        /// <param name="qsQueried">Query result from qsstat</param>
        /// <returns>merged result</returns>
        private QsStatPartResultDTO MergeQueriedData(QsDedicatedLoaded characteristicsInfo, QueriedData queried, QsStatPartResultDTO qsQueried)
        {
            Dictionary<int, string> partTextResults = new Dictionary<int, string>(queried.PartData.Count+qsQueried.PartTextResults.Count);
            Dictionary<int, string> measuringResults = new Dictionary<int, string>(queried.MeasuringData.Count + qsQueried.MeasuringResults.Count);
            Dictionary<int, QsStatCharacteristicResultDTO> characteristicResults = new Dictionary<int, QsStatCharacteristicResultDTO>(characteristicsInfo.TotalCharacteristics+1);
            QsStatCharacteristicResultDTO characteristicResult = null;
            Dictionary<int, string> characteristicTextResults;
            bool incomplete = qsQueried.CharacteristicResults.Any() ? false : qsQueried.Incomplete;
            foreach (var item in queried.PartData)
                partTextResults.Add(item.Key, item.Value);
            foreach (var item in qsQueried.PartTextResults)
                partTextResults.Add(item.Key, item.Value);

            foreach (var item in queried.MeasuringData)
                measuringResults.Add(item.Key, item.Value);
            foreach (var item in qsQueried.MeasuringResults)
                measuringResults.Add(item.Key, item.Value);

            for (int i = 0; i < characteristicsInfo.TotalCharacteristics; i++)
            {
                characteristicTextResults = new Dictionary<int, string>();
                CharacteristicMetaData characteristicMetaData = null;
                IReadOnlyDictionary<int, Tuple<string, double>> numericAndTextResults = null;
                IReadOnlyDictionary<QsStatImageRequest, byte[]> imageResults = null;
                IReadOnlyDictionary<int, string> characteristicMeasuringResults = null;
                bool characteristicIncomplete = false;

                if (queried.CharacteristicsData.TryGetValue(i, out IReadOnlyDictionary<int, string> characteristicData))
                {
                    foreach (var item in characteristicData)
                        characteristicTextResults.Add(item.Key, item.Value);   
                }

                if (queried.CharacteristicsMeasuringData.TryGetValue(i, out IReadOnlyDictionary<int, string> retrieved))
                {
                    characteristicMeasuringResults = retrieved;
                }
                else
                {
                    characteristicMeasuringResults = new Dictionary<int, string>();
                }

                if (qsQueried.CharacteristicResults.TryGetValue(i, out characteristicResult)){
                    foreach (var item in characteristicResult.CharacteristicTextResults)
                        characteristicTextResults.Add(item.Key, item.Value);

                    characteristicMetaData = characteristicResult.CharacteristicMetaData;
                    numericAndTextResults = characteristicResult.NumericAndTextResults;
                    imageResults = characteristicResult.ImageResults;
                    characteristicIncomplete = characteristicResult.Incomplete;
                }
                else
                {
                    bool enabled = i == 0 || characteristicsInfo.EnabledCharacteristics.Contains(i);                  
                    characteristicMetaData = new CharacteristicMetaData(enabled, ProcessState.Ok);
                    numericAndTextResults = new Dictionary<int, Tuple<string, double>>();
                    imageResults = new Dictionary<QsStatImageRequest, byte[]>();
                    characteristicIncomplete = false;                  
                }
                characteristicResults[i] = new QsStatCharacteristicResultDTO(characteristicMetaData, characteristicTextResults, numericAndTextResults, imageResults, characteristicIncomplete, characteristicMeasuringResults);
            }
            return new QsStatPartResultDTO(partTextResults, characteristicResults, qsQueried.DatasetReferenceGuid, incomplete, qsQueried.LastDsChangeTime, measuringResults);
        }

        /// <summary>
        /// Generate query items for qsstat, does not contain elements for other query
        /// </summary>
        /// <param name="queried">Other queried data for missing keys</param>
        /// <param name="toRetrieve">Query keys only for qsstat</param>
        /// <returns>Unified key object for querying</returns>
        private QsStatRetrievalItemsMultipleCharacteristics GenerateNewRetrievalItems(QueriedData queried, QsStatRetrievalItemsMultipleCharacteristics toRetrieve)
        {
            return new QsStatRetrievalItemsMultipleCharacteristics()
            {
                PartTextKeys = queried.NotRetrievedPartKeys,
                CharacteristicTextKeys = queried.NotRetrievedCharacteristicsKeys,
                NumericAndTextKeys = toRetrieve.NumericAndTextKeys,
                ImageKeys = toRetrieve.ImageKeys
            };
        }

        private async Task<QsDedicatedLoaded> LoadDataInternalAsync(QsStatWrapper qsStat, PoolItem<int> connection, SpcPath spcPath, Guid dataGuid, QueryableQsData data, GeneralSettings settings, CancellationToken ct)
        {
            var totalLoadedCharacteristics = await qsStat.LoadAndEvaluateDataAsync(connection.Resource, data.DfqData, ct).ConfigureAwait(false);
            List<int> enabledCharacteristics = new List<int>();
            for (int i = 1; i <= totalLoadedCharacteristics && !ct.IsCancellationRequested; i++)
            {
                ct.ThrowIfCancellationRequested();
                //throw exception if retrieval failed?
                
                if (data.CharacteristicsData[i-1].iMerkmal_Klasse.HasValue)
                {
                    if (QsUtility.CheckIfCharacteristicClassEnabled(settings, data.CharacteristicsData[i - 1].iMerkmal_Klasse.Value))
                        enabledCharacteristics.Add(i);
                }
            }
            ct.ThrowIfCancellationRequested();
            return new QsDedicatedLoaded(spcPath, dataGuid, data, totalLoadedCharacteristics, enabledCharacteristics);
        }

        /// <inheritdoc/>
        public async Task<int> GetClickedValueNr(int graphicId, int characteristicNumber, double width, double height, double x, double y)
        {
            CancellationToken loadDataToken = this._loadDataCts.Token;
            var result = await this._qsStatService.Enqueue(
                    (qsStat) =>
                    {
                        loadDataToken.ThrowIfCancellationRequested();
                        return qsStat.GetGraphValueAtPosition(this._connection.Resource, graphicId, characteristicNumber, (int)width, (int)height, (int)x,(int) y);
                    }, PriorityType.High).ConfigureAwait(false);

            return result;
        }

        private (bool incomplete, QueriedData queriedData) GetAllData(QsDedicatedLoaded loadedData, QsStatRetrievalItemsMultipleCharacteristics toRetrieve, int selectedCharacteristic)
        {
            var parsedData = loadedData.ParsedData;
            bool incomplete = toRetrieve.NumericAndTextKeys.Any() || toRetrieve.ImageKeys.Any();
            var partData = new Dictionary<int, string>();
            var unavailblePartKeys = new HashSet<int>();
            var characteristicsData = new Dictionary<int, IReadOnlyDictionary<int, string>>();
            var unavailableCharacteristicKeys = new Dictionary<int, IReadOnlyCollection<int>>();
            //missing measuring data only possible when there is a bug
            var measuringData = new Dictionary<int, string>();
            var characteristicsMeasuringData = new Dictionary<int, IReadOnlyDictionary<int, string>>();

            foreach (var number in toRetrieve.PartTextKeys)
            {
                if (number > 0 && number <= short.MaxValue && parsedData.TryGetPartData((short) number, out string retrieved))
                {
                    partData[number] = retrieved;
                }
                else
                {
                    unavailblePartKeys.Add(number);
                    incomplete = true;
                }
            }

            foreach (var number in toRetrieve.AdditionalPartKeys)
            {
                if (parsedData.TryGetAdditionalPartData(number, loadedData.EnabledCharacteristics, out string value))
                {
                    measuringData[number] = value;
                }
            }

            foreach(var characteristicsEntry in toRetrieve.CharacteristicTextKeys)
            {
                int characteristic;
                switch (characteristicsEntry.Key)
                {
                    case 0 when selectedCharacteristic <= 0:
                        continue;
                    case 0:
                        characteristic = selectedCharacteristic;
                        break;
                    default:
                        characteristic = characteristicsEntry.Key;
                        break;
                }

                var characteristicRetrieved = new Dictionary<int, string>();
                var characteristicUnavailable = new HashSet<int>();
                foreach (var number in characteristicsEntry.Value)
                {
                    if (number > 0 && number <= short.MaxValue && parsedData.TryGetCharacteristicsData((short) number, characteristic, out string retrieved))
                    {
                        characteristicRetrieved[number] = retrieved;
                    }
                    else
                    {
                        characteristicUnavailable.Add(number);
                        incomplete = true;
                    }
                }
                if (characteristicRetrieved.Any())
                    characteristicsData[characteristicsEntry.Key] = characteristicRetrieved;
                if (characteristicUnavailable.Any())
                    unavailableCharacteristicKeys[characteristicsEntry.Key] = characteristicUnavailable;              
            }

            foreach(var retrievalItems in toRetrieve.AdditionalCharacteristicKeys)
            {
                int characteristic;
                switch (retrievalItems.Key)
                {
                    case 0 when selectedCharacteristic <= 0:
                        continue;
                    case 0:
                        characteristic = selectedCharacteristic;
                        break;
                    default:
                        characteristic = retrievalItems.Key;
                        break;
                }
                var characteristicRetrieved = new Dictionary<int, string>();
                foreach (var number in retrievalItems.Value)
                {
                    if (parsedData.TryGetAdditionalCharacteristicData(number, characteristic, out string value)){
                        characteristicRetrieved[number] = value;
                    }
                }
                if (characteristicRetrieved.Any())
                    characteristicsMeasuringData[retrievalItems.Key] = characteristicRetrieved;
            }

            var queried = new QueriedData(partData, unavailblePartKeys, characteristicsData, unavailableCharacteristicKeys, measuringData, characteristicsMeasuringData);
            return (incomplete, queried);
        }

        private QsStatPartResultDTO GetAllData(IQsStatDataRetrieval qsStat, PoolItem<int> connection, QsStatRetrievalItemsMultipleCharacteristics toRetrieve, QsDedicatedLoaded characteristicsInfo, int selectedCharacteristic, Size displaySize, CancellationToken ct)
        {
            var partTextResults = qsStat.GetPartData(connection.Resource, toRetrieve.PartTextKeys, ct);
            var characteristicsDataLookup = new Dictionary<int, QsStatCharacteristicResultDTO>();
            
            for (int i = 0; i <= characteristicsInfo.TotalCharacteristics; i++)
            {
                ct.ThrowIfCancellationRequested();

                int queryCharacteristicsNumber = i;
                if (i == 0)
                {
                    if (selectedCharacteristic == -1)
                    {
                        continue;
                    }else
                    {
                        queryCharacteristicsNumber = selectedCharacteristic;
                    }
                }
                bool incomplete = false;
                var characteristicsTextResults = new Dictionary<int, string>();
                var numericAndTestResults = new Dictionary<int, Tuple<string, double>>();
                var imageResults = new Dictionary<QsStatImageRequest, byte[]>();
                if (toRetrieve.CharacteristicTextKeys.TryGetValue(i, out IReadOnlyCollection<int> characteristicTextKeys))
                {
                    characteristicsTextResults = qsStat.GetCharacteristicData(connection.Resource, characteristicTextKeys, queryCharacteristicsNumber, ct);
                    incomplete = incomplete || characteristicTextKeys.Count > characteristicsTextResults.Count;
                }
                else
                {
                    incomplete = true;
                }
                if (toRetrieve.NumericAndTextKeys.TryGetValue(i, out IReadOnlyCollection<int> numericAndTextKeys))
                {
                    numericAndTestResults = qsStat.GetTextResult(connection.Resource, numericAndTextKeys, queryCharacteristicsNumber, ct);
                    incomplete = incomplete || numericAndTextKeys.Count > numericAndTestResults.Count;
                }
                else
                {
                    incomplete = true;
                }
                if (toRetrieve.ImageKeys.TryGetValue(i, out IReadOnlyCollection<QsStatImageRequest> imageKeys))
                {
                    imageResults = qsStat.GetGraphResult(connection.Resource, imageKeys, queryCharacteristicsNumber, displaySize, ct);
                    incomplete = incomplete || imageKeys.Count > imageResults.Count;
                }
                else
                {
                    incomplete = true;
                }
                //the metadata here is lying
                characteristicsDataLookup[i] = new QsStatCharacteristicResultDTO(new CharacteristicMetaData(characteristicsInfo.EnabledCharacteristics.Contains(queryCharacteristicsNumber), ProcessState.Ok), characteristicsTextResults, numericAndTestResults, imageResults, incomplete);
            }
            //TODO most likely need to save the current characteristics number too
            ct.ThrowIfCancellationRequested();
            return new QsStatPartResultDTO(partTextResults, characteristicsDataLookup, characteristicsInfo.DatasetGuid, toRetrieve.PartTextKeys.Count > partTextResults.Count, characteristicsInfo.ParsedData.LastDsChangeTime);
        }

        /// <inheritdoc/>
        public void Shutdown()
        {
            this._shutdownCts.Cancel();
            //deregister event handlers, if there are any
        }

        /// <inheritdoc/>
        public Task UpdateSettings(GeneralSettings generalSettings)
        {
            QsDedicatedLoaded lastPart;
            lock (this._lock)
            {
                this._generalSettings = generalSettings;
                lastPart = LoadedDataset;
            }
            return RequestLoadData(lastPart.SpcPath, lastPart.DatasetGuid);
        }

        /// <inheritdoc/>
        public Task UpdateSpc(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders)
        {
            lock (_lock)
            {
                this.SpcPathFolderDatasets = spcPathFolders;
                
                ResetData();
                if (spcPathFolders.Keys.Any())
                {
                    var firstSpcPath = spcPathFolders.Keys.OrderBy(spcPath => spcPath.SpcNumber).First();
                    var datasetId = spcPathFolders[firstSpcPath].DataSets.FirstOrDefault()?.Guid ?? Guid.Empty;
                    return RequestLoadData(firstSpcPath, datasetId);
                }
                else
                {
                    return Task.CompletedTask;
                }
            }           
        }

        /// <inheritdoc/>
        public (QsDedicatedLoaded loadedDataset, int? selectedCharacteristicNumber) GetCurrentDataSelection()
        {
            lock (_lock)
            {
                return (LoadedDataset, this._currentCharacteristicNumber);
            }
        }

        #region properties
        /// <inheritdoc/>
        public IReadOnlyDictionary<SpcPath, FolderDatasets> SpcPathFolderDatasets
        {
            get { return _spcPathFolders; }
            private set { SetProperty(ref _spcPathFolders, value); }
        }

        /// <inheritdoc/>
        public QsDedicatedLoaded LoadedDataset
        {
            get { return _loadedDataset; }
            private set {
                SetProperty(ref _loadedDataset, value);
            }
        }
        #endregion

        public void Cancel()
        {
            _loadDataCts.Cancel();
            _loadDataCts = new CancellationTokenSource();
        }


        #region IDisposable
        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this._disposed)
                return;
            this._getDataCts.Cancel();
            this._loadDataCts.Cancel();
            this._shutdownCts.Cancel();
            if (disposing)
            {
                if (this._getDataCts != null)
                {                
                    this._getDataCts.Dispose();
                    this._getDataCts = null;
                }
                if (this._loadDataCts != null)
                {
                    this._loadDataCts.Dispose();
                    this._loadDataCts = null;
                }
                if (this._shutdownCts != null)
                {
                    this._shutdownCts.Dispose();
                    this._shutdownCts = null;
                }
                if (this._connection != null)
                {
                    this._connection.Dispose();
                }
            }

            this._disposed = true;
        }

        ~QsDedicatedCalculation()
        {
            Dispose(false);
        }

        #endregion
    }
}
