using Promess.Common.Util.Async;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using Promess.QsStat.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Promess.QsStat.Services
{
    /// <summary>
    /// Service for periodical data evaluation
    /// </summary>
    internal interface IQsPeriodicalCalculation
    {
        /// <summary>
        /// Start the periodcal data evaluation with the given parameters
        /// </summary>
        /// <param name="spcPathFolders">Data source targets</param>
        /// <param name="panelItemModels">Retrieval item definitions of panels</param>
        /// <param name="generalSettings">Settings for evaluation</param>
        /// <param name="evalColorSettings">Definition for to consider violations</param>
        /// <param name="listViewSettings">Retrieval items for the list view</param>
        /// <param name="data">Container for evaluated data</param>
        void Start(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, IReadOnlyCollection<PanelItemModel> panelItemModels, GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, ListViewSettings listViewSettings, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> data);
        /// <summary>
        /// Shutdown of the service
        /// </summary>
        /// <returns></returns>
        Task ShutdownAsync();
        /// <summary>
        /// Update evaluation settings and data target
        /// </summary>
        /// <param name="generalSettings">Settings for evaluation</param>
        /// <param name="evalColorSettings">Definition for to consider violations</param>
        /// <param name="evaluationResults">Container for evaluated data</param>
        void UpdateSettings(GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> evaluationResults);
        /// <summary>
        /// Update to retrieve data for the panels
        /// </summary>
        /// <param name="panelItemModels">Retrieval item definitions of panels</param>
        void UpdatePanelItemModels(IReadOnlyCollection<PanelItemModel> panelItemModels);
        /// <summary>
        /// Update data source targets and container for evaluated data
        /// </summary>
        /// <param name="spcPathFolders">New data source targets</param>
        /// <param name="evaluationData">New container for evaluated data</param>
        void UpdateSpc(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> evaluationData);
        /// <summary>
        /// Update items which are always retrieved
        /// </summary>
        /// <param name="settings">New items to always retrieve</param>
        void UpdateGeneralRetrievalKeys(ListViewSettings settings);
        /// <summary>
        /// Data navigation for a <see cref="SpcPathEvaluationDTO"/>
        /// </summary>
        /// <param name="spcPath">Navigation target</param>
        /// <param name="manualRequest">Navigation description</param>
        /// <returns>Task for the navigation</returns>
        Task ManualRequest(SpcPath spcPath, ManualRequestTypes manualRequest);
        /// <summary>
        /// Set dimensions for a panel
        /// </summary>
        /// <param name="panelName">Name of the panel as identifier</param>
        /// <param name="height">Height for this panel</param>
        /// <param name="width">Width for this panel</param>
        void SetPanelHeightAndWidth(string panelName, double height, double width);
    }
}
