﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using Promess.QsStat.Data;

namespace Promess.QsStat.Services
{
    /// <summary>
    /// Service for permanently loading data for faster evaluation
    /// </summary>
    public interface IQsDedicatedCalculation:INotifyPropertyChanged, IDisposable
    {
        /// <summary>
        /// Shutdown of the calculation.
        /// </summary>
        void Shutdown();
        /// <summary>
        /// Update evaluation criteria
        /// </summary>
        /// <param name="generalSettings">New settings</param>
        /// <returns>Task for the operation</returns>
        Task UpdateSettings(GeneralSettings generalSettings);
        /// <summary>
        /// Change available data sources and results
        /// </summary>
        /// <param name="spcPathFolders">Available data sources</param>
        /// <returns>Task for the operation</returns>
        Task UpdateSpc(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders);
        /// <summary>
        /// Request new loading data for a data set
        /// </summary>
        /// <param name="spcPath">Path of the data set</param>
        /// <param name="dataGuid">Id of the data set</param>
        /// <param name="ct">Cancellation</param>
        /// <returns>Evaluation task with the result of loaded data</returns>
        Task<QsDedicatedLoaded> RequestLoadData(SpcPath spcPath, Guid dataGuid, CancellationToken ct);
        /// <summary>
        /// Retrieve data for currently loaded data with consideration to the given parameters
        /// </summary>
        /// <param name="loaded">Assumed loaded data</param>
        /// <param name="toRetrieve">All items that should be queried from the evaluation</param>
        /// <param name="selectedCharacteristic">Characteristic number for retrievals of characteristic with value zero</param>
        /// <param name="displaySize">Panel size for graphic requests</param>
        /// <param name="ct">Cancellation of operation</param>
        /// <returns>Task with result. Will be cancelled if assumed data does not equal loaded</returns>
        Task<QsStatPartResultDTO> RequestAllData(QsDedicatedLoaded loaded, QsStatRetrievalItemsMultipleCharacteristics toRetrieve, int selectedCharacteristic, Size displaySize, CancellationToken ct);
        /// <summary>
        /// Get value number (not index!) for a click in a qsStat graphic
        /// </summary>
        /// <param name="graphicNr">qsStat graphic id</param>
        /// <param name="characteristicNumber">Characteristic number of the graphic</param>
        /// <param name="width">Width of the graphic</param>
        /// <param name="height">Height of the graphic</param>
        /// <param name="x">x click position in the graphic</param>
        /// <param name="y">y click position in the graphic</param>
        /// <returns>Task with identified value number. Value less than one represents failed request.</returns>
        Task<int> GetClickedValueNr(int graphicNr, int characteristicNumber, double width, double height, double x, double y);
        
        /// <summary>
        /// Get information about currently loaded data and selected characteristic number
        /// </summary>
        /// <returns>tuple literal of loaded data and selected characteristic number</returns>
        (QsDedicatedLoaded loadedDataset, int? selectedCharacteristicNumber) GetCurrentDataSelection();

        /// <summary>
        /// Information about the current loaded. No guarantee on which thread the change notification will be raised
        /// </summary>
        QsDedicatedLoaded LoadedDataset { get; }
        /// <summary>
        /// Information about the current data selection. No guarantee on which thread the change notification will be raised
        /// </summary>
        IReadOnlyDictionary<SpcPath, FolderDatasets> SpcPathFolderDatasets { get; }

        
    }
}
