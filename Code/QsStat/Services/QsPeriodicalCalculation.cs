﻿using Promess.Common.Util.Async;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using Promess.QsStat.Data;
using Promess.QsStat.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.CompilerServices;
using Promess.Common.QDas;

#if DEBUG
[assembly: InternalsVisibleTo("QsStat.Tests")]
#endif
namespace Promess.QsStat.Services
{
    public class QsPeriodicalCalculation : IQsPeriodicalCalculation, IDisposable
    {
        private const int MIN_NUMBER_VALUES = 5;
        private readonly Size FALLBACK_PANEL_SIZE = Size.Empty;

        private readonly Object _lock;

        private ResourceService<QsStatWrapper> _qsStatService;
        private ResourcePool<int> _connectionPool;
        private CancellationTokenSource _cts;
        private Task _periodicalTask;
        private TaskCompletionSource<bool> _completeWait;

        private GeneralSettings _generalSettings;
        private EvaluationAndColorSettings _evalColorSettings;
        private IReadOnlyDictionary<SpcPath, FolderDatasets> _spcPathFolders;
        private SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> _spcPathResults;
        private Dictionary<SpcPath, ManualRequestTypes> _spcChangeRequests;
        private TaskCompletionSource<bool> _spcChangeRequestReset;
        private Dictionary<String, QsStatRetrievalItems> _spcPathRetrievalItems;
        private Dictionary<string, Size> _panelNameToSize;
        private QsStatRetrievalItems _alwaysRetrieveItems;
        private bool _forceLoadMissingKeys;

        internal QsPeriodicalCalculation(ResourceService<QsStatWrapper> qsStatService, ResourcePool<int> connectionPool)
        {
            this._qsStatService = qsStatService;
            this._connectionPool = connectionPool;
            this._lock = new object();
        }

        /// <inheritdoc/>
        public void Start(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, IReadOnlyCollection<PanelItemModel> panelItemModels, GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, ListViewSettings listViewSettings, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> data)
        {
            //TODO Better to use initialize pattern?
            this._spcPathFolders = spcPathFolders;
            this._generalSettings = generalSettings;
            this._evalColorSettings = evalColorSettings;
            this._spcPathResults = data;
            this._panelNameToSize = new Dictionary<string, Size>();
            this._forceLoadMissingKeys = true;
            this._spcPathRetrievalItems = CreateSpcPathRetrievalItems(panelItemModels);
            this._alwaysRetrieveItems =  CreateGeneralRetrievalItems(listViewSettings);
            StartTask();
        }

        private static Dictionary<string, QsStatRetrievalItems> CreateSpcPathRetrievalItems(IReadOnlyCollection<PanelItemModel> panelItemModels)
        {
            var spcPathRetrievalItems = new Dictionary<string, QsStatRetrievalItems>();
            foreach (var panelItemModel in panelItemModels)
            {
                var retrievalItems = new QsStatRetrievalItems();
                foreach (var editorItem in panelItemModel.Items)
                {
                    switch (editorItem.GetType().Name)
                    {
                        case nameof(TextEditorItem):
                            TextEditorItem textItem = (TextEditorItem)editorItem;
                            if (textItem.TextType == TextType.Qdas && textItem.QdasNumber != null)
                            {
                                switch (textItem.QdasNumber.EvaluationType)
                                {
                                    case TextEvaluationType.NumericAndTextResults:
                                        retrievalItems.NumericAndTextKeys.Add(textItem.QdasNumber.EvaluationNumber);
                                        break;
                                    case TextEvaluationType.Part:
                                        retrievalItems.PartTextKeys.Add(textItem.QdasNumber.EvaluationNumber);
                                        break;
                                    case TextEvaluationType.Characteristic:
                                        retrievalItems.CharacteristicTextKeys.Add(textItem.QdasNumber.EvaluationNumber);
                                        break;
                                    case TextEvaluationType.AdditionalPart:
                                        retrievalItems.AdditionalPartKeys.Add(textItem.QdasNumber.EvaluationNumber);
                                        break;
                                    case TextEvaluationType.AdditionalCharacteristic:
                                        retrievalItems.AdditionalCharacteristicKeys.Add(textItem.QdasNumber.EvaluationNumber);
                                        break;
                                    default:
                                        throw new NotImplementedException($"Unknown evaluation value of {textItem.QdasNumber.EvaluationType} for {nameof(TextEvaluationType)} encountered during data retrieval.");
                                }
                            }
                            break;
                        case nameof(ImageEditorItem):
                            ImageEditorItem imageItem = (ImageEditorItem)editorItem;
                            if (imageItem.ImageSourceType == ImageSourceType.Qdas && imageItem.EvaluationCode.HasValue)
                            {
                                retrievalItems.ImageKeys.Add(new QsStatImageRequest(imageItem.EvaluationCode.Value, imageItem.PercentageWidth, imageItem.PercentageHeight));
                            }
                            break;
                    }
                }
                spcPathRetrievalItems[panelItemModel.Name] = retrievalItems;
            }
            return spcPathRetrievalItems;
        }

        private static QsStatRetrievalItems CreateGeneralRetrievalItems(ListViewSettings listViewSettings)
        {
            var retrievalItems = new QsStatRetrievalItems();
            retrievalItems.PartTextKeys.UnionWith(listViewSettings.PartLabelQdasNumbers.Select(item => item.EvaluationNumber));
            retrievalItems.PartTextKeys.UnionWith(listViewSettings.PartGridQdasNumbers.Select(item => item.QdasNumber.EvaluationNumber));
            retrievalItems.CharacteristicTextKeys.UnionWith(listViewSettings.CharacteristicLabelQdasNumbers.Select(item => item.EvaluationNumber));
            foreach (var entry in listViewSettings.CharacteristicGridQdasNumbers)
            {
                switch (entry.QdasNumber.EvaluationType)
                {
                    case TextEvaluationType.Characteristic:
                        retrievalItems.CharacteristicTextKeys.Add(entry.QdasNumber.EvaluationNumber);
                        break;
                    case TextEvaluationType.NumericAndTextResults:
                        retrievalItems.NumericAndTextKeys.Add(entry.QdasNumber.EvaluationNumber);
                        break;
                    case TextEvaluationType.AdditionalPart:
                        retrievalItems.AdditionalPartKeys.Add(entry.QdasNumber.EvaluationNumber);
                        break;
                    case TextEvaluationType.AdditionalCharacteristic:
                        retrievalItems.AdditionalCharacteristicKeys.Add(entry.QdasNumber.EvaluationNumber);
                        break;
                }
            }
            return retrievalItems;
        }

        /// <inheritdoc/>
        public void UpdateSettings (GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> spcPathResults)
        {
            Debug.Assert(generalSettings != null && evalColorSettings != null);
            lock (_lock)
            {
                this._cts.Cancel();
                this._generalSettings = generalSettings;
                this._evalColorSettings = evalColorSettings;
                this._spcPathResults = spcPathResults;
                StartTask();
            }
        }

        /// <inheritdoc/>
        public void UpdateGeneralRetrievalKeys(ListViewSettings settings)
        {
            lock (this._lock)
            {
                this._alwaysRetrieveItems = CreateGeneralRetrievalItems(settings);
                foreach (var spcPath in this._spcPathFolders.Keys)
                {
                    if (!this._spcChangeRequests.ContainsKey(spcPath))
                        this._spcChangeRequests[spcPath] = ManualRequestTypes.Refresh;
                }
                this._forceLoadMissingKeys = true;
                this._completeWait.TrySetResult(true);
            }
        }

        /// <inheritdoc/>
        public void UpdatePanelItemModels(IReadOnlyCollection<PanelItemModel> panelItemModels)
        {
            lock (_lock)
            {
                this._forceLoadMissingKeys = true;
                this._spcPathRetrievalItems = CreateSpcPathRetrievalItems(panelItemModels);
                this._completeWait.TrySetResult(true);
            }
        }

        /// <inheritdoc/>
        public void SetPanelHeightAndWidth(string panelName, double height, double width)
        {
            lock (_lock)
            {
                if (this._panelNameToSize.TryGetValue(panelName, out Size oldSize))
                {
                    if (oldSize.Width == width && oldSize.Height == height)
                        return;
                }
                this._panelNameToSize[panelName] = new Size(width, height);
                this._forceLoadMissingKeys = true;
                this._completeWait.TrySetResult(true);
            }
        }

        /// <inheritdoc/>
        public void UpdateSpc(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> evaluationData)
        {
            Debug.Assert(spcPathFolders != null);
            lock (_lock)
            {
                this._cts.Cancel();
                this._spcPathFolders = spcPathFolders;
                this._spcPathResults = evaluationData;
                StartTask();
            }
        }

        private void StartTask()
        {
            lock (_lock)
            {
                _cts = new CancellationTokenSource();
                _spcChangeRequests = new Dictionary<SpcPath, ManualRequestTypes>();
                _spcChangeRequestReset?.TrySetCanceled();
                _spcChangeRequestReset = new TaskCompletionSource<bool>();
                _cts.Token.Register(() => this._spcChangeRequestReset.TrySetCanceled());
                _completeWait = new TaskCompletionSource<bool>();
                this._periodicalTask = Task.Run(async ()=>await PeriodicalDataUpdateAsync(_cts.Token).ConfigureAwait(false));
            }
        }

        /// <inheritdoc/>
        public async Task ShutdownAsync()
        {
            try
            {
                _cts.Cancel();
                await _periodicalTask.ConfigureAwait(false);
            }catch(AggregateException) { /* NOOP, cancel throws TaskCanceledExceptions */}
            
        }

        private async Task PeriodicalDataUpdateAsync(CancellationToken ct)
        {
            List<Task> tasks = new List<Task>();
            GeneralSettings generalSettings;
            EvaluationAndColorSettings evalColorSettings;
            IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders;
            Dictionary<String, QsStatRetrievalItems> retrievalItems;
            SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> spcPathResults;
            Dictionary<SpcPath, ManualRequestTypes> spcChangeRequests;
            TaskCompletionSource<bool> spcChangeRequestReset;
            QsStatRetrievalItems alwaysRetrieveItems;
            Dictionary<string, Size> panelSizes;
            bool forceLoadMissingKeys;
            Task continueTask;
            while (!ct.IsCancellationRequested)
            {
                try
                {
                    lock (this._lock)
                    {
                        if (ct.IsCancellationRequested)
                            return;
                        generalSettings = this._generalSettings;
                        evalColorSettings = this._evalColorSettings;
                        spcPathFolders = this._spcPathFolders;
                        retrievalItems = this._spcPathRetrievalItems;
                        spcPathResults = this._spcPathResults;
                        spcChangeRequests = this._spcChangeRequests;
                        spcChangeRequestReset = this._spcChangeRequestReset;
                        this._spcChangeRequestReset = new TaskCompletionSource<bool>();
                        alwaysRetrieveItems = this._alwaysRetrieveItems;
                        forceLoadMissingKeys = this._forceLoadMissingKeys;
                        panelSizes = new Dictionary<string, Size>(this._panelNameToSize);
                        this._forceLoadMissingKeys = false;
                        if (this._spcChangeRequests.Any())
                            this._spcChangeRequests = new Dictionary<SpcPath, ManualRequestTypes>();
                        if (this._completeWait.Task.IsCompleted)
                            this._completeWait = new TaskCompletionSource<bool>();
                        continueTask = this._completeWait.Task;
                        if (spcPathFolders.Count < 1)
                            return;
                    }
                    //alternative different processing if there are any requests, only process the request loop
                    foreach (var spcPathFolder in spcPathFolders)
                    {
                        var oldEvaluation = spcPathResults[spcPathFolder.Key];
                        var panelName = spcPathFolder.Key.PanelConfigurationName;
                        var panelItems = retrievalItems[panelName];
                        if (!panelSizes.TryGetValue(panelName, out Size panelSize))
                            panelSize = this.FALLBACK_PANEL_SIZE;
                        ManualRequestTypes? maybeRequest = null;
                        if (spcChangeRequests.TryGetValue(spcPathFolder.Key, out ManualRequestTypes tmpRequest))
                        {
                            maybeRequest = tmpRequest;
                        }
                        var task = ProcessItemAsync(spcPathFolder.Key, generalSettings, evalColorSettings, spcPathFolder.Value, oldEvaluation, maybeRequest, panelItems, alwaysRetrieveItems, panelSize, forceLoadMissingKeys, ct);
                        tasks.Add(task.ContinueWith((ancendental) => {
                            if (ancendental.IsFaulted)
                            {
                                //FIXME set some error message somewhere
                                return;
                            }
                            var res = ancendental.Result;
                            if (res.Item1)
                            {
                                spcPathResults[spcPathFolder.Key] = res.Item2;
                            }
                        }, ct));
                    }
                    await Task.WhenAll(tasks).ConfigureAwait(false);
                    tasks.Clear();
                    spcChangeRequestReset.TrySetResult(true);
                    TimeSpan waitTs = GetWaitTime(spcPathFolders, spcPathResults);
                    if (waitTs.Ticks > 0)
                    {
                        await Task.WhenAny(Task.Delay(waitTs, ct), continueTask).ConfigureAwait(false);
                    }
                }
                catch (OperationCanceledException) { /* NOOP, exception expected when calculation is canceled */}
            }   
        }

        private async Task<Tuple<bool, SpcPathEvaluationDTO>> ProcessItemAsync(SpcPath spcPath, GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, FolderDatasets folderDataset, SpcPathEvaluationDTO oldData, ManualRequestTypes? maybeRequest, QsStatRetrievalItems spcPathRetrievalItems, QsStatRetrievalItems generalRetrievalItems, Size panelSize, bool loadMissingKeys, CancellationToken ct)
        {
            try
            {
                ct.ThrowIfCancellationRequested();
                DateTime newRefreshTime = DateTime.UtcNow;
                bool shouldRefreshData = spcPath.RefreshInMinutes <= (newRefreshTime - folderDataset.LastRefreshTime).TotalMinutes || (maybeRequest.HasValue && maybeRequest.Value.Equals(ManualRequestTypes.Refresh));
                bool valueRetrieved = false;
                bool datasetsChanged = false;
                var allRetrievedData = new List<QsStatPartResultDTO>();
                if (shouldRefreshData)
                {
                    try
                    {
                        var dfqFiles = Directory.GetFiles(spcPath.DataPath, "*.dfq");
                        var dfdFiles = Directory.GetFiles(spcPath.DataPath, "*.dfd");
                        folderDataset.LastRefreshTime = newRefreshTime;
                        datasetsChanged = await folderDataset.RemoveNotPresentAndCreateNewAsync(dfdFiles.Union(dfqFiles).ToList(), newRefreshTime).ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                        oldData.Exception = e;
                        return new Tuple<bool, SpcPathEvaluationDTO>(true, oldData);
                    }
                }
                var currentSpcPathRetrievalItems = CombineRetrievalItems(new QsStatRetrievalItems[] { spcPathRetrievalItems, generalRetrievalItems });
                if (datasetsChanged || maybeRequest.HasValue || loadMissingKeys)
                {
                    if (!folderDataset.DataSets.Any())
                    {
                        bool emitNewData = !oldData.SelectedPartGuid.Equals(Guid.Empty);
                        return new Tuple<bool, SpcPathEvaluationDTO>(emitNewData, new SpcPathEvaluationDTO());
                    }
                    Tuple<Guid, int> selectedPartAndCharacteristic = GetToEvaluateDsGuidAndCharaIndex(folderDataset.DataSets, oldData, maybeRequest);
                    var selectedDsGuid = selectedPartAndCharacteristic.Item1;
                    int selectedCharacteristicIndex = selectedPartAndCharacteristic.Item2;
                    int oldSelectedCharacteristicIndex = oldData.SelectedCharacteristicIndex;
                    bool loadMissingKeysRequired;
                    foreach (var currentDs in folderDataset.DataSets)
                    {
                        ct.ThrowIfCancellationRequested();

                        if (!oldData.EvaluatedData.TryGetValue(currentDs.Guid, out QsStatPartResultDTO oldPartResult))
                        {
                            oldPartResult = new QsStatPartResultDTO(currentDs.Guid);
                            loadMissingKeysRequired = true;
                        }
                        else
                        {
                            loadMissingKeysRequired = loadMissingKeys || oldPartResult.Incomplete;
                        }
                        if (currentDs.Guid.Equals(selectedDsGuid))
                        {
                            if (currentDs.LastChangeTime != oldPartResult.LastDsChangeTime || !selectedDsGuid.Equals(oldData.SelectedPartGuid) ||
                                (selectedDsGuid.Equals(oldData.SelectedPartGuid) && selectedCharacteristicIndex != oldData.SelectedCharacteristicIndex) ||
                                    loadMissingKeysRequired)
                            {
                                valueRetrieved = true;
                                Debug.WriteLine($"enqueue1:{currentDs.Guid}");
                                var res = await _qsStatService.Enqueue(
                                    async (qsStat) =>
                                    {
                                    using (var poolCon = await this._connectionPool.GetResourceAsync(ct).ConfigureAwait(false))
                                            return await EvaluateAndLoadDataSetAsync(qsStat, poolCon, currentDs, generalSettings, evalColorSettings, currentSpcPathRetrievalItems, generalRetrievalItems, panelSize, selectedCharacteristicIndex, oldPartResult, ct).ConfigureAwait(false);
                                    }).ConfigureAwait(false);
                                Debug.WriteLine($"~enqueue1:{currentDs.Guid}");
                                if (res.Item1)
                                {
                                    selectedCharacteristicIndex = res.Item2;
                                    allRetrievedData.Add(res.Item3);
                                }
                                else
                                {
                                    selectedCharacteristicIndex = -1;
                                    allRetrievedData.Add(new QsStatPartResultDTO(currentDs.Guid));
                                }
                                continue;
                            }
                            else
                            {
                                allRetrievedData.Add(oldPartResult);
                                continue;
                            }
                        }
                        else
                        {
                            if (currentDs.LastChangeTime != oldPartResult.LastDsChangeTime || loadMissingKeysRequired)
                            {
                                valueRetrieved = true;
                                Debug.WriteLine($"enqueue2:{currentDs.Guid}");
                                var res = await _qsStatService.Enqueue(
                                    async (qsStat) =>
                                    {
                                        using (var poolCon = await this._connectionPool.GetResourceAsync(ct).ConfigureAwait(false))
                                            return await EvaluateAndLoadDataSetAsync(qsStat, poolCon, currentDs, generalSettings, evalColorSettings, generalRetrievalItems, panelSize, oldPartResult, ct).ConfigureAwait(false);
                                    }).ConfigureAwait(false);
                                Debug.WriteLine($"~enqueue2:{currentDs.Guid}");
                                if (res.Item1)
                                {
                                    allRetrievedData.Add(res.Item2);
                                }
                                else
                                {
                                    allRetrievedData.Add(oldPartResult);
                                    continue;
                                }
                            }
                            else
                            {
                                allRetrievedData.Add(oldPartResult);
                            }
                            continue;
                        }
                    }
                    if (valueRetrieved || datasetsChanged)
                        return new Tuple<bool, SpcPathEvaluationDTO>(true, new SpcPathEvaluationDTO(allRetrievedData, selectedDsGuid, selectedCharacteristicIndex));
                }
                return new Tuple<bool, SpcPathEvaluationDTO>(false, oldData);
            }
            catch (Exception ex) when (ex is OperationCanceledException || ex is ObjectDisposedException)
            {
                return new Tuple<bool, SpcPathEvaluationDTO>(false, oldData);
            }
        }

        private Tuple<Guid, int> GetToEvaluateDsGuidAndCharaIndex(IReadOnlyList<DataSet> dataSets, SpcPathEvaluationDTO oldData, ManualRequestTypes? maybeRequest)
        {
            if (oldData.SelectedPartGuid.Equals(Guid.Empty) || !dataSets.Any(ds => ds.Guid.Equals(oldData.SelectedPartGuid)))
            {
                return new Tuple<Guid, int>(dataSets.First().Guid, -1);
            }
            if (!maybeRequest.HasValue || maybeRequest.Value == ManualRequestTypes.Refresh)
            {
                return new Tuple<Guid, int>(oldData.SelectedPartGuid, oldData.SelectedCharacteristicIndex);
            }
            if (maybeRequest.Value == ManualRequestTypes.NextPart || maybeRequest.Value == ManualRequestTypes.PreviousPart)
            { 
                int i = 0;
                foreach (var ds in dataSets)
                {
                    if (ds.Guid.Equals(oldData.SelectedPartGuid))
                        break;
                    i++;
                }
                if (maybeRequest.Value == ManualRequestTypes.NextPart)
                {
                    if (i < dataSets.Count - 1)
                    {
                        return new Tuple<Guid, int>(dataSets[i + 1].Guid, -1);
                    }
                }else
                {
                    if (i > 0)
                    {
                        return new Tuple<Guid, int>(dataSets[i - 1].Guid, -1);
                    }
                }
                return new Tuple<Guid, int>(oldData.SelectedPartGuid, oldData.SelectedCharacteristicIndex);
            }else
            {
                //characteristic
                if (oldData.SelectedCharacteristicIndex==-1)
                    return new Tuple<Guid, int>(oldData.SelectedPartGuid, oldData.SelectedCharacteristicIndex);
                var partData = oldData.EvaluatedData[oldData.SelectedPartGuid];
                var charaIndices = partData.CharacteristicResults.Where(kvp => kvp.Value.CharacteristicMetaData.Enabled).Select(kvp => kvp.Key);
                if (maybeRequest.Value == ManualRequestTypes.NextCharacteristic)
                {
                    var filteredIndices = charaIndices.Where(index => index > oldData.SelectedCharacteristicIndex);
                    if (filteredIndices.Any())
                        return new Tuple<Guid, int>(oldData.SelectedPartGuid, filteredIndices.Min());
                }
                else
                {
                    var filteredIndices = charaIndices.Where(index => index < oldData.SelectedCharacteristicIndex);
                    if (filteredIndices.Any())
                        return new Tuple<Guid, int>(oldData.SelectedPartGuid, filteredIndices.Max());
                }
                return new Tuple<Guid, int>(oldData.SelectedPartGuid, oldData.SelectedCharacteristicIndex);
            }
        }

        private static TimeSpan GetWaitTime(IReadOnlyDictionary<SpcPath, FolderDatasets> spcPathFolders, IDictionary<SpcPath, SpcPathEvaluationDTO> spcData)
        {
            DateTime nextRefreshTime = DateTime.MaxValue;
            foreach (var spcPathFolder in spcPathFolders)
            {
                var spcPath = spcPathFolder.Key;
                var folderDs = spcPathFolder.Value;
                if (spcData.TryGetValue(spcPath, out SpcPathEvaluationDTO oldData))
                {
                    if (oldData.EvaluatedData.Any(entry => entry.Value.Incomplete))
                    {
                        return TimeSpan.Zero;
                    }
                }
                DateTime nextSpcRefresh = folderDs.LastRefreshTime + TimeSpan.FromMinutes(spcPath.RefreshInMinutes);
                if (nextSpcRefresh < nextRefreshTime)
                    nextRefreshTime = nextSpcRefresh;
            }
            var waitTs = nextRefreshTime - DateTime.UtcNow;
            return waitTs;
        }

        private static async Task<Tuple<bool, int, QsStatPartResultDTO>> EvaluateAndLoadDataSetAsync(QsStatWrapper qsStat, PoolItem<int> conWrapper, DataSet dataSet, GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, QsStatRetrievalItems selectedItemsToRetrieve, QsStatRetrievalItems generalItemsToRetrieve, Size panelSize, int characteristicIndex, QsStatPartResultDTO oldPartData, CancellationToken ct)
        {
            QueryableQsData queryable = await dataSet.GetQueryableQsDataAsync().ConfigureAwait(false);
            var result = await EvaluateDataAsync(qsStat, conWrapper, queryable, generalSettings, evalColorSettings, ct).ConfigureAwait(false);
            QsStatPartResultDTO refreshedData = null;
            if (result.Item1 && !ct.IsCancellationRequested)
            {
                if (characteristicIndex == -1)
                    characteristicIndex = GetFirstEnabledCharacteristicIndex(result.Item2);
                var toRefreshPartKeys = new HashSet<int>(selectedItemsToRetrieve.PartTextKeys.Except(oldPartData.PartTextResults.Select(kvp => kvp.Key)));
                var qsRefreshPartKeys = new HashSet<int>();
                Dictionary<int, string> partTextResults = new Dictionary<int, string>();
                
                foreach (int key in toRefreshPartKeys)
                {
                    if (queryable.TryGetPartData(key, out string retrieved))
                    {
                        partTextResults.Add(key, retrieved);
                    }
                    else
                    {
                        qsRefreshPartKeys.Add(key);
                    }
                }
                var qsPartResults = qsStat.GetPartData(conWrapper.Resource, toRefreshPartKeys, ct);
                foreach (var kvp in qsPartResults)
                    partTextResults[kvp.Key] = kvp.Value;

                foreach (var kvp in oldPartData.PartTextResults)
                    partTextResults[kvp.Key] = kvp.Value;

                var additionalResultsPart = new Dictionary<int, string>();
                var enabledCharacteristics = result.Item2.Select((item, i) => new { item, i }).Where(elem => elem.item.Enabled).Select(elem => elem.i + 1).ToList();
                foreach (var key in selectedItemsToRetrieve.AdditionalPartKeys)
                    if (queryable.TryGetAdditionalPartData(key, enabledCharacteristics, out string value))
                        additionalResultsPart[key] = value;

                var characteristicsDataLookup = new Dictionary<int, QsStatCharacteristicResultDTO>();
                QsStatRetrievalItems itemsForCharacteristic;
                Debug.WriteLine($"chara:{conWrapper.Resource}");
                for (int i = 0; i < result.Item2.Count && !ct.IsCancellationRequested; i++)
                { 
                    if (i == characteristicIndex)
                    {
                        itemsForCharacteristic = selectedItemsToRetrieve;
                    }else
                    {
                        itemsForCharacteristic = generalItemsToRetrieve;
                    }                  
                    var characteristicResult = GetCharacteristicResult(qsStat, conWrapper, queryable, itemsForCharacteristic, panelSize, oldPartData, result.Item2[i], i, ct);
                    characteristicsDataLookup.Add(i, characteristicResult);
                }
                Debug.WriteLine($"~chara:{conWrapper.Resource}");
                refreshedData = new QsStatPartResultDTO(partTextResults, characteristicsDataLookup, oldPartData.DatasetReferenceGuid, toRefreshPartKeys.Count > partTextResults.Count, queryable.LastDsChangeTime, additionalResultsPart);
            }
            return new Tuple<bool, int, QsStatPartResultDTO>(result.Item1, characteristicIndex, refreshedData);
        }

        private async Task<Tuple<bool, QsStatPartResultDTO>> EvaluateAndLoadDataSetAsync(QsStatWrapper conWrapper, PoolItem<int> connection, DataSet dataSet, GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, QsStatRetrievalItems generalItemsToRetrieve, Size panelSize, QsStatPartResultDTO oldPartData, CancellationToken ct)
        {
            var queryable = await dataSet.GetQueryableQsDataAsync().ConfigureAwait(false);
            var result = await EvaluateDataAsync(conWrapper, connection, queryable, generalSettings, evalColorSettings, ct).ConfigureAwait(false);
            var retrieved = RetrieveData(conWrapper, connection, queryable, generalItemsToRetrieve, panelSize, oldPartData, result, ct);
            return retrieved;
        }

        internal static Tuple<bool, QsStatPartResultDTO> RetrieveData(IQsStatDataRetrieval qsStat, PoolItem<int> connection, QueryableQsData queryable, QsStatRetrievalItems generalItemsToRetrieve, Size panelSize, QsStatPartResultDTO oldPartData, Tuple<bool, IReadOnlyList<CharacteristicMetaData>> result, CancellationToken ct)
        {
            QsStatPartResultDTO refreshedData = null;
            if (result.Item1 && !ct.IsCancellationRequested)
            {
                Dictionary<int, string> partTextResults = new Dictionary<int, string>();
                var toRefreshPartKeys = new HashSet<int>(generalItemsToRetrieve.PartTextKeys.Except(oldPartData.PartTextResults.Select(kvp => kvp.Key)));
                var qsRefreshPartKeys = new HashSet<int>();
                foreach (int key in toRefreshPartKeys)
                {
                    if (queryable.TryGetPartData(key, out string retrieved))
                    {
                        partTextResults.Add(key, retrieved);
                    }
                    else
                    {
                        qsRefreshPartKeys.Add(key);
                    }
                }
                foreach (var kvp in qsStat.GetPartData(connection.Resource, qsRefreshPartKeys, ct))
                    partTextResults.Add(kvp.Key, kvp.Value);
                foreach (var kvp in oldPartData.PartTextResults)
                    partTextResults[kvp.Key] = kvp.Value;

                var additionalResultsPart = new Dictionary<int, string>();
                var characteristicsDataLookup = new Dictionary<int, QsStatCharacteristicResultDTO>();
                var enabledCharacteristics = result.Item2.Select((item, i) => new { item, i }).Where(elem => elem.item.Enabled).Select(elem => elem.i + 1).ToList();
                foreach (var key in generalItemsToRetrieve.AdditionalPartKeys)
                    if (queryable.TryGetAdditionalPartData(key, enabledCharacteristics, out string value))
                        additionalResultsPart[key] = value;

                for (int i = 0; i < result.Item2.Count && !ct.IsCancellationRequested; i++)
                {
                    var characteristicResult = GetCharacteristicResult(qsStat, connection, queryable, generalItemsToRetrieve, panelSize, oldPartData, result.Item2[i], i, ct);
                    characteristicsDataLookup.Add(i, characteristicResult);
                }
                refreshedData = new QsStatPartResultDTO(partTextResults, characteristicsDataLookup, oldPartData.DatasetReferenceGuid, toRefreshPartKeys.Count > partTextResults.Count, queryable.LastDsChangeTime, additionalResultsPart);
            }
            return new Tuple<bool, QsStatPartResultDTO>(result.Item1, refreshedData);
        }

        internal static QsStatCharacteristicResultDTO GetCharacteristicResult(IQsStatDataRetrieval qsStat, PoolItem<int> connection, QueryableQsData queryable, QsStatRetrievalItems itemsToRetrieve, Size panelSize, QsStatPartResultDTO oldPartData, CharacteristicMetaData metaData, int characteristicIndex, CancellationToken ct)
        {
            bool incomplete = false;
            int characteristicNumber = characteristicIndex + 1;
            ISet<int> toRetrieveCharacteristicsKeys;
            Dictionary<int, string> characteristicTextResults = new Dictionary<int, string>();
            if (oldPartData.CharacteristicResults.TryGetValue(characteristicIndex, out QsStatCharacteristicResultDTO oldCharacteristicData))
            {
                foreach (var charKvp in oldCharacteristicData.CharacteristicTextResults)
                    characteristicTextResults[charKvp.Key] = charKvp.Value;
                toRetrieveCharacteristicsKeys = new HashSet<int>(itemsToRetrieve.CharacteristicTextKeys.Except(oldCharacteristicData.CharacteristicTextResults.Select(kvp => kvp.Key)));
            }
            else
            {
                toRetrieveCharacteristicsKeys = itemsToRetrieve.CharacteristicTextKeys;
            }
            var tempKeys = new HashSet<int>();
            foreach (var key in toRetrieveCharacteristicsKeys)
            {
                if (queryable.TryGetCharacteristicsData(key, characteristicNumber, out string value))
                {
                    characteristicTextResults.Add(key, value);
                }
                else
                {
                    tempKeys.Add(key);
                }
            }
            toRetrieveCharacteristicsKeys = tempKeys;
            var qsData = qsStat.GetCharacteristicData(connection.Resource, toRetrieveCharacteristicsKeys, characteristicNumber, ct);
            incomplete = qsData.Count < toRetrieveCharacteristicsKeys.Count;
            if (characteristicTextResults.Any())
            {
                foreach (var kvp in qsData)
                    characteristicTextResults.Add(kvp.Key, kvp.Value);
            }
            else
            {
                characteristicTextResults = qsData;
            }
            
            Dictionary<int, Tuple<string, double>> numericAndTextResults = qsStat.GetTextResult(connection.Resource, itemsToRetrieve.NumericAndTextKeys, characteristicNumber, ct);
            Dictionary<QsStatImageRequest, byte[]> imageResults = qsStat.GetGraphResult(connection.Resource, itemsToRetrieve.ImageKeys, characteristicNumber, panelSize, ct);
            incomplete = incomplete || itemsToRetrieve.NumericAndTextKeys.Count > numericAndTextResults.Count || itemsToRetrieve.ImageKeys.Count > imageResults.Count;
            var additional = new Dictionary<int, string>();
            foreach (int key in itemsToRetrieve.AdditionalCharacteristicKeys)
                if (queryable.TryGetAdditionalCharacteristicData(key, characteristicNumber, out string value))
                    additional[key] = value;

            return new QsStatCharacteristicResultDTO(metaData, characteristicTextResults, numericAndTextResults, imageResults, incomplete, additional);
        }

        internal static async Task<Tuple<bool,IReadOnlyList<CharacteristicMetaData>>> EvaluateDataAsync(QsStatWrapper qsStat, PoolItem<int> conWrapper, QueryableQsData queryable, GeneralSettings options, EvaluationAndColorSettings evalColorSettings, CancellationToken ct)
        {
            //first characteristic
            try
            {
                int numberOfCharacteristics = await qsStat.LoadAndEvaluateDataAsync(conWrapper.Resource, queryable.DfqData, ct).ConfigureAwait(false);
                //assuming that there is only one part
                if (numberOfCharacteristics < 1 || ct.IsCancellationRequested)
                {
                    return new Tuple<bool, IReadOnlyList<CharacteristicMetaData>>(false, new List<CharacteristicMetaData>());
                }
                var charaMetaEvaluation = EvaluateCharacteristicMetaData(qsStat, conWrapper, options, evalColorSettings, numberOfCharacteristics, queryable, ct);
                return new Tuple<bool, IReadOnlyList<CharacteristicMetaData>>(true,charaMetaEvaluation);
            }
            catch (COMException)
            {
                //TODO maybe just catch the exception and continue or attach some error code to the object?
                return new Tuple<bool, IReadOnlyList<CharacteristicMetaData>>(false, new List<CharacteristicMetaData>());
            }
        }

        private static IReadOnlyList<CharacteristicMetaData> EvaluateCharacteristicMetaData(QsStatWrapper qsHandle, PoolItem<int> conWrapper, GeneralSettings options, EvaluationAndColorSettings evalColorSettings, int numberOfCharacteristics, QueryableQsData data, CancellationToken ct)
        {
            //@see modqsSTAT.bas line 235ff. for original code (GetMerkFarbe)
            ProcessState state;
            bool enabled;
            TS_MERKMALSDATEN currentChara;

            List<CharacteristicMetaData> charaMetaData = new List<CharacteristicMetaData>();
            for (int i = 1; i <= numberOfCharacteristics; i++)
            {
                ct.ThrowIfCancellationRequested();
                currentChara = data.CharacteristicsData[i - 1];
                if (!currentChara.iMerkmal_Art.HasValue || !currentChara.iMerkmal_Klasse.HasValue)
                {
                    charaMetaData.Add(new CharacteristicMetaData(false, ProcessState.FailedEvaluation));
                    continue;
                }     
                
                if (!Enum.IsDefined(typeof(CharacteristicType), (int)currentChara.iMerkmal_Art.Value))
                {
                    charaMetaData.Add(new CharacteristicMetaData(false, ProcessState.TooFewValues));
                    continue;
                }
                else
                {
                    switch ((CharacteristicType)currentChara.iMerkmal_Art.Value)
                    {
                        case CharacteristicType.Variable:
                            state = EvaluateStateForCharacteristicVariable(qsHandle, conWrapper, data, options, evalColorSettings, i);
                            break;
                        case CharacteristicType.Attributive:
                            state = EvaluateStateForCharacteristicAttributive(qsHandle, conWrapper, data, options, evalColorSettings, i);
                            break;
                        default:
                            // or return ProcessState.FailedEvaluation
                            throw new NotImplementedException();
                    }
                    enabled = QsUtility.CheckIfCharacteristicClassEnabled(options, currentChara.iMerkmal_Klasse.Value);
                    charaMetaData.Add(new CharacteristicMetaData(enabled, state));
                }
            }
            return charaMetaData;
        }

        private static ProcessState EvaluateStateForCharacteristicVariable(QsStatWrapper qsHandle, PoolItem<int> conWrapper, QueryableQsData data, GeneralSettings options, EvaluationAndColorSettings evalColorSettings, int characteristicNumber)
        {
            Tuple<string, double> result2 = null;
            Tuple<string, double> result3 = null;
            int characteristicIndex = characteristicNumber - 1;
            TS_MERKMALSDATEN header = data.CharacteristicsData[characteristicIndex];
            
            if (!qsHandle.GetTextResult(conWrapper.Resource, 6302, characteristicNumber, out Tuple<string, double> result))
            {
                return ProcessState.FailedEvaluation;
            }
            int numberOfValues = (int)result.Item2;
            if (evalColorSettings.NoOrTooFewValues.IsEnabled)
            {
                if (options.CalcCpkS)
                {
                    if (numberOfValues < options.CalcCpkSMin)
                        return ProcessState.TooFewValues;
                }
                else
                {
                    if (numberOfValues < MIN_NUMBER_VALUES)
                        return ProcessState.TooFewValues;
                }
            }
            if (evalColorSettings.DeficientPart.IsEnabled)
            {
                if (options.RejectEvaluationMultipleParts)
                {
                    if (GetVarRejections(header, data.CharacteristicsValues, characteristicIndex, options.RejectEvaluationNoParts))
                        return ProcessState.DeficientPart;
                }
                else
                {
                    if (!qsHandle.GetTextResult(conWrapper.Resource, 5510, characteristicNumber, out result) || !qsHandle.GetTextResult(conWrapper.Resource, 5500, characteristicNumber, out result2))
                        return ProcessState.FailedEvaluation;

                    double numRejected = result.Item2 + result2.Item2;
                    if (numRejected > 0)
                        return ProcessState.DeficientPart;             
                }
            }

            Tuple<bool, bool> xqSViolated = null; 

            if (evalColorSettings.ActionLimitViolationXq.IsEnabled)
            {
                if (options.LimitActionControlLimitSamples)
                {
                    xqSViolated = GetVarInterventionThresholdViolated(header, data.CharacteristicsValues, characteristicIndex, options.ActionControlLimitNoSamples).ToTuple();
                    if (xqSViolated.Item1)
                        return ProcessState.ActionLimitViolationXq;
                }
                else
                {
                    if (!qsHandle.GetTextResult(conWrapper.Resource, 8020, characteristicNumber, out result))
                        return ProcessState.FailedEvaluation;

                    if (result.Item2 > 0)
                    {
                        return ProcessState.ActionLimitViolationXq;
                    }
                }
            }
            if (evalColorSettings.ActionLimitViolationS.IsEnabled)
            {
                if (evalColorSettings.ActionLimitViolationS.IsEnabled)
                {
                    if (options.LimitActionControlLimitSamples)
                    {
                        if (xqSViolated==null)
                            xqSViolated = GetVarInterventionThresholdViolated(header, data.CharacteristicsValues, characteristicIndex, options.ActionControlLimitNoSamples).ToTuple();
                        if (xqSViolated.Item2)
                            return ProcessState.ActionLimitViolationS;
                    }
                }
                else
                {
                    if (!qsHandle.GetTextResult(conWrapper.Resource, 8120, characteristicNumber, out result))
                        return ProcessState.FailedEvaluation;

                    if (result.Item2 > 0)
                    {
                        return ProcessState.ActionLimitViolationS;
                    }
                }
            }
            if (evalColorSettings.StabilityViolation.IsEnabled)
            {
                if (!qsHandle.GetTextResult(conWrapper.Resource, 8910, characteristicNumber, out result))
                    return ProcessState.FailedEvaluation;

                if (result.Item2 > 0)
                {
                    return ProcessState.StabilityViolation;
                }
            }
            if (evalColorSettings.ProcessOoC.IsEnabled || evalColorSettings.ProcessEndangered.IsEnabled)
            {
                double sCp, sCpk, kCp, kCpk;
                if (options.CpCpkFromPlanning)
                {
                    sCp = header.dCp_Wert_gefordert??options.EndangeredCp;
                    sCpk = header.dCpk_Wert_gefordert??options.EndangeredCpk;
                    kCp = header.dCp_Wert_gefixt??Math.Max(sCp-0.33, 0.01);
                    kCpk = header.dCpk_Wert_gefixt??Math.Max(sCpk-0.33, 0.01);
                }
                else
                {
                    sCp = options.EndangeredCp;
                    sCpk = options.EndangeredCpk;
                    kCp = options.OutOfControlCp;
                    kCpk = options.OutOfControlCpk;
                }

                if (options.CalcCpkS)
                {
                    if (!qsHandle.GetTextResult(conWrapper.Resource, 6302, characteristicNumber, out result)||!qsHandle.GetTextResult(conWrapper.Resource, 1000, characteristicNumber, out result2)|| !qsHandle.GetTextResult(conWrapper.Resource, 2100, characteristicNumber, out result3))
                        return ProcessState.FailedEvaluation;
                    if (!header.dNennmass.HasValue || !header.LowerSpecificationLimit.HasValue || !header.UpperSpecificationLimit.HasValue)
                        return ProcessState.FailedEvaluation;
                    
                    int numValues = (int)result.Item2;
                    double xq = result2.Item2;
                    double s = result3.Item2;
                    double cp;
                    double cpk;
                    if (numberOfValues >= options.CalcCpkSMin && numberOfValues <= options.CalcCpkSMax && s > 0.00001)
                    {
                        cp = CalcCp(header.LowerSpecificationLimit.Value, header.UpperSpecificationLimit.Value, s);
                        cpk = CalcCpk(header.LowerSpecificationLimit.Value, header.UpperSpecificationLimit.Value, header.dNennmass.Value, xq, s);
                    }
                    else
                    {
                        if (!qsHandle.GetTextResult(conWrapper.Resource, 5210, characteristicNumber, out result) || !qsHandle.GetTextResult(conWrapper.Resource, 5220, characteristicNumber, out result2))
                            return ProcessState.FailedEvaluation;

                        cp = result.Item2;
                        cpk = result2.Item2;
                        if (cp == 0 && cpk == 0)
                            return ProcessState.TooFewValues;
                    }
                    if (evalColorSettings.ProcessOoC.IsEnabled)
                    {
                        if (header.dNennmass.Value + header.LowerSpecificationLimit != 0 && cp < kCp)
                        {
                            return ProcessState.OutOfControl;
                        }
                        if (cpk < kCpk)
                        {
                            return ProcessState.OutOfControl;
                        }
                    }
                    if (evalColorSettings.ProcessEndangered.IsEnabled)
                    {
                        if (header.dNennmass.Value + header.LowerSpecificationLimit != 0 && cp < sCp)
                        {
                            return ProcessState.Endangered;
                        }
                        if (cpk < sCpk)
                        {
                            return ProcessState.Endangered;
                        }
                    }
                }
                else
                {
                    if (!qsHandle.GetTextResult(conWrapper.Resource, 5210, characteristicNumber, out result) || !qsHandle.GetTextResult(conWrapper.Resource, 5220, characteristicNumber, out result2))
                        return ProcessState.FailedEvaluation;

                    var cp = result.Item2;
                    var cpk = result2.Item2;
                    if (cp == 0 && cpk == 0)
                        return ProcessState.TooFewValues;

                    if (evalColorSettings.ProcessOoC.IsEnabled)
                    {
                        if (cp < options.OutOfControlCp || cpk < options.OutOfControlCpk)
                            return ProcessState.OutOfControl;
                    }
                    if (evalColorSettings.ProcessEndangered.IsEnabled)
                    {
                        if (cp < options.EndangeredCp || cpk < options.EndangeredCpk)
                            return ProcessState.Endangered;
                    }
                }
            }

            return ProcessState.Ok;
        }

        private static double CalcCpk(double lowerSpecificationLimit, double upperSpecificationLimit, double nominalValue, double xq, double s)
        {
            if (nominalValue + lowerSpecificationLimit == 0)
            {
                return (upperSpecificationLimit - xq) / (3 * s);
            }
            else
            {
                double cpkLL = (xq- lowerSpecificationLimit) /(3*s);
                double cpkUL = (upperSpecificationLimit - xq)/(3*s);
                return Math.Min(cpkLL, cpkUL);               
            }
        }

        private static double CalcCp(double upperSpecificationLimit, double lowerSpecificationLimit, double s)
        {
            double tolerance = upperSpecificationLimit - lowerSpecificationLimit;
            return tolerance / (6 * s);
        }

        /// <summary>
        /// Checks for position and variance violation if the values are present. FIXME sample variance function is most likely wrong
        /// </summary>
        /// <param name="header">header data providing the position and variance limits</param>
        /// <param name="characteristicsValues">measurement data</param>
        /// <param name="characteristicIndex">index for the characteristic to evaluate</param>
        /// <param name="actionControlLimitNoSamples">number of samples</param>
        /// <returns>(position, variance) violated</returns>
        private static (bool, bool) GetVarInterventionThresholdViolated(TS_MERKMALSDATEN header, IReadOnlyList<TS_MERKMAL_IST[]> characteristicsValues, int characteristicIndex, int actionControlLimitNoSamples)
        {
            bool sViolated = false;
            bool xqViolated = false;
            bool canEvaluateS = header.dStreuung_UEG.HasValue && header.dStreuung_OEG.HasValue && header.dStreuung_UEG != header.dStreuung_OEG;
            bool canEvaluateXq = header.dLage_UEG.HasValue && header.dLage_OEG.HasValue && header.dLage_UEG != header.dLage_OEG;
            short sampleSize = header.iStichprobe_Umfang_gesamt ?? 0;
            if (sampleSize < 3 || sampleSize < characteristicsValues.Count || !canEvaluateS && !canEvaluateXq)
                return (false, false);
            int numberSamples = characteristicsValues.Count / sampleSize;
            long lastSampleIndex = numberSamples * sampleSize;
            IEnumerable<TS_MERKMAL_IST[]> sampleSource = characteristicsValues;
            IEnumerable<TS_MERKMAL_IST[]> currentSample;
            for (int sample = 0; sample < numberSamples; sample++)
            {
                currentSample = sampleSource.Take(sampleSize);
                sampleSource = sampleSource.Skip(sampleSize);
                double merkS = 0;
                double merkSummX = 0;
                double merkSummXX = 0;
                double merkXq = 0;
                if (currentSample.Any(item => item[characteristicIndex].iAttribut != 0 || !item[characteristicIndex].dMesswert.HasValue))
                    continue;
                
                foreach (var item in currentSample.Select(elem=>elem[characteristicIndex]))
                {
                    merkSummX += item.dMesswert.Value;
                    merkSummXX += Math.Pow(item.dMesswert.Value, 2);
                }
                merkXq = merkSummX / sampleSize;
                merkS = merkSummXX - Math.Pow(merkSummX, 2) / sampleSize;

                merkS = Math.Sqrt(merkS / (sampleSize - 1));
                if (canEvaluateXq && !xqViolated)
                {
                    xqViolated = merkXq < header.dLage_UEG || merkXq > header.dLage_OEG;
                }
                if (canEvaluateS && !sViolated)
                {
                    sViolated = merkS < header.dStreuung_UEG || merkS > header.dStreuung_OEG;
                }
                if ((!canEvaluateXq || xqViolated) && (!canEvaluateS || sViolated))
                    break;
            }
            return (xqViolated, sViolated);
        }

        /// <summary>
        /// Geets the #rejected measurements for a variable characteristics. Adapted from modqsstat.bas->getVarAusschuss ~line 710
        /// </summary>
        /// <param name="header">to evaluate characteristics header data</param>
        /// <returns></returns>
        private static bool GetVarRejections(TS_MERKMALSDATEN header, IReadOnlyList<TS_MERKMAL_IST[]> characteristicsValues, int characteristicIndex, int rejectEvaluationNoParts)
        {
            double nominalSize = header.dNennmass??0;
            double lLimit = header.LowerSpecificationLimit??0;
            double uLimit = header.UpperSpecificationLimit??0;

            if (lLimit == 0 &&  uLimit == 0)
            {
                lLimit = nominalSize - (header.LowerAllowance ?? 0);
                uLimit = nominalSize + (header.UpperAllowance ?? 0);
            }
            for (int i = Math.Max(0, characteristicsValues.Count- rejectEvaluationNoParts); i<characteristicsValues.Count;i++){
                double measurement = characteristicsValues[i][characteristicIndex].dMesswert??0;
                //original code had an 'and' here, which could never be satisfied 
                if (measurement <= lLimit || measurement >= uLimit)
                    return true;
            }
            return false;
        }

        private static ProcessState EvaluateStateForCharacteristicAttributive(QsStatWrapper qsHandle, PoolItem<int> conWrapper, QueryableQsData data, GeneralSettings options, EvaluationAndColorSettings evalColorSettings, int characteristicsNumber)
        {
            Tuple<string, double> result = null;
            if (evalColorSettings.DeficientPart.IsEnabled)
            {
                if (options.RejectEvaluationMultipleParts)
                {
                    if (GetAttAusschuss(data.CharacteristicsData[characteristicsNumber-1], data.CharacteristicsValues, characteristicsNumber, options.ActionControlLimitNoSamples))
                        return ProcessState.DeficientPart;
                }
                else
                {
                    if (!qsHandle.GetTextResult(conWrapper.Resource, 3600, characteristicsNumber, out result))
                        return ProcessState.FailedEvaluation;

                    if (result.Item2 > 0)
                        return ProcessState.DeficientPart;
                }
            }
            return ProcessState.Ok;
        }

        private static bool GetAttAusschuss(TS_MERKMALSDATEN header, IReadOnlyList<TS_MERKMAL_IST[]> characteristicsValues, int characteristicsNumber, int actionControlLimitNoSamples)
        {
            int index = characteristicsNumber - 1;
            int toSkip = characteristicsValues.Count - actionControlLimitNoSamples;
            var toCheck = toSkip > 0 ? characteristicsValues.Skip(toSkip) : characteristicsValues;
            return toCheck.Any(items => items[index].iAnzahl_Fehler > 0 || items[index].iAttribut > 0);
        }

        private static int GetFirstEnabledCharacteristicIndex(IReadOnlyList<CharacteristicMetaData> characteristicsMetaData)
        {
            for(int i = 0; i < characteristicsMetaData.Count; i++)
            {
                if (characteristicsMetaData[i].Enabled)
                    return i;
            }
            return -1;
        }

        internal static int GetNextEnabledCharacteristicIndex(IReadOnlyDictionary<int, QsStatCharacteristicResultDTO> characteristicResults, int oldIndex)
        {
            var sortedIndices = characteristicResults.Keys.Where(index=>index > oldIndex).OrderBy(index=>index);
            foreach (var index in sortedIndices)
            {
                if (characteristicResults[index].CharacteristicMetaData.Enabled)
                {
                    return index;
                }
            }
            return -1;
        }

        internal static int GetPreviousEnabledCharacteristicIndex(IReadOnlyDictionary<int, QsStatCharacteristicResultDTO> characteristicResults, int oldIndex)
        {
            var sortedIndices = characteristicResults.Keys.Where(index => index < oldIndex).OrderByDescending(index => index);
            foreach (var index in sortedIndices)
            {
                if (characteristicResults[index].CharacteristicMetaData.Enabled)
                {
                    return index;
                }
            }
            return -1;
        }

        /// <inheritdoc/>
        public Task ManualRequest(SpcPath spcPath, ManualRequestTypes manualRequest)
        {
            lock (_lock)
            {
                if (!_spcChangeRequests.ContainsKey(spcPath))
                {
                    this._spcChangeRequests[spcPath] = manualRequest;
                    this._completeWait.TrySetResult(true);
                    return this._spcChangeRequestReset.Task;
                }
                return Task.CompletedTask;
            }
        }

        public void Dispose()
        {
            if (_cts != null)
            {
                _cts.Dispose();
                _cts = null;
            }
        }

        #region helper functions
        private QsStatRetrievalItems CombineRetrievalItems(QsStatRetrievalItems[] qsStatRetrievalItems)
        {
            var result = new QsStatRetrievalItems();
            foreach (var item in qsStatRetrievalItems)
            {
                result.PartTextKeys.UnionWith(item.PartTextKeys);
                result.CharacteristicTextKeys.UnionWith(item.CharacteristicTextKeys);
                result.NumericAndTextKeys.UnionWith(item.NumericAndTextKeys);
                result.ImageKeys.UnionWith(item.ImageKeys);
                result.AdditionalPartKeys.UnionWith(item.AdditionalPartKeys);
                result.AdditionalCharacteristicKeys.UnionWith(item.AdditionalCharacteristicKeys);
            }
            return result;
        }
        #endregion
    }
}
