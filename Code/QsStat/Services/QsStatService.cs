﻿using Promess.Common.Data;
using Promess.Common.Util.Async;
using Promess.Pbld.Data;
using Promess.Pbld.Data.Editor;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using Promess.QsStat.Data;
using Promess.QsStat.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Waf.Foundation;

namespace Promess.QsStat.Services
{
    /// <summary>
    /// Provides access to data from qsStat. When subscribing to Events (implemented and PropertyChanged) BeginInvoke must be used on UI Thread
    /// </summary>
    [Export(typeof(IQsStatService)), PartCreationPolicy(CreationPolicy.Shared), Export]
    public sealed class QsStatService : ModelAsync, IQsStatService, IDisposable
    {
        private readonly Task<byte[]> _cancelledImageTask;
        private readonly Task _cancelledTask;

        private SemaphoreSlim _semaphore;
        private QsStatState _qsStatState;
        private COMException _exception;

        private Task _startupTask;
        private CancellationTokenSource _startupCts;

        private Area _area;
        private IReadOnlyDictionary<SpcPath, FolderDatasets> _spcPathFolders;

        private GeneralSettings _generalSettings;
        private EvaluationAndColorSettings _evalColorSettings;
        private ListViewSettings _listViewSettings;
        private IReadOnlyCollection<PanelItemModel> _panelItemModels;

        private IReadOnlyDictionary<int, string> _moduleList;
        private IReadOnlyDictionary<int, QsLanguageEntry> _languageList;
        private IReadOnlyDictionary<int, string> _partKFields;
        private IReadOnlyDictionary<int, string> _characteristicKFields;
        private IReadOnlyList<QsReportEntry> _reports;
        private IReadOnlyDictionary<int, string> _printers;
        private IReadOnlyDictionary<int, string> _graphics;
        private IReadOnlyDictionary<int, string> _statResult;
        private IReadOnlyDictionary<int, string> _strategies;

        private int? _selectedStrategy;
        private int? _selectedLanguage;
        private int? _selectedModule;
        private string _selectedReport;
        private int? _selectedPrinter;

        private ResourcePool<int> _qsStatConnectionPool;
        //limits access to qsStat
        private ResourcePool<QsStatWrapper> _qsStatPool;
        private ResourceService<QsStatWrapper> _exclusiveQsStatAccess;

        private QsConnectionParameters _qsConnectionParameters;
        private IQsPeriodicalCalculation _periodicalCalculation;
        private IQsDedicatedCalculation _dedicatedCalculation;

        private SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> _evaluatedData;
        private Encoding _qCOMEncoding;

        #region events
        //note: events will most likely not be on the main thread
        public event EventHandler<EventArgs> QsSettingsChangedEvent;
        public event EventHandler<EventArgs> ModuleInvalidEvent;
        public event EventHandler<EventArgs> UserPasswordInvalidEvent;
        #endregion

        [ImportingConstructor]
        public QsStatService()
        {
            _semaphore = new SemaphoreSlim(1);
            _startupCts = new CancellationTokenSource();
            _qsStatState = QsStatState.Starting;
            _periodicalCalculation = new QsPeriodicalCalculationDummy();
            _dedicatedCalculation = new QsDedicatedCalculationDummy(); 
            _qCOMEncoding = Encoding.UTF8;
            _cancelledImageTask = GenerateCancelledTask<byte[]>();
            _cancelledTask = GenerateCancelledTask();
            SetupCollections();
        }

        private void SetupCollections()
        {
            _partKFields = new Dictionary<int, string>();
            _reports = new List<QsReportEntry>();
            _printers = new Dictionary<int, string>();
            _characteristicKFields = new Dictionary<int, string>();
            _statResult = new Dictionary<int, string>();
            _graphics = new Dictionary<int, string>();
            _strategies = new Dictionary<int, string>();
            _languageList = new Dictionary<int, QsLanguageEntry>();
            _moduleList = new Dictionary<int, string>();
            _spcPathFolders = new Dictionary<SpcPath, FolderDatasets>();
            _evaluatedData = new SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO>();
        }

        //TODO maybe the dictionaries need to use the lock, because they need to be replaced at the same time? Or a container
        public IReadOnlyDictionary<int, string> PartKFields
        {
            get
            {
                return _partKFields;
            }
            private set
            {
                SetProperty(ref _partKFields, value);
            }
        }

        public IReadOnlyList<QsReportEntry> Reports
        {
            get
            {
                return _reports;
            }
            private set
            {
                SetProperty(ref _reports, value);
            }
        }

        public IReadOnlyDictionary<int, string> Printers
        {
            get
            {
                return _printers;
            }
            private set
            {
                SetProperty(ref _printers, value);
            }
        }

        public IReadOnlyDictionary<int, string> CharacteristicKFields
        {
            get
            {
                return _characteristicKFields;
            }
            private set
            {
                SetProperty(ref _characteristicKFields, value);
            }
        }

        public IReadOnlyDictionary<int, string> StatResult
        {
            get
            {
                return _statResult;
            }
            private set
            {
                SetProperty(ref _statResult, value);
            }
        }

        public IReadOnlyDictionary<int, string> Graphics
        {
            get
            {
                return _graphics;
            }
            private set
            {
                SetProperty(ref _graphics, value);
            }
        }

        public IReadOnlyDictionary<int, string> Strategies
        {
            get
            {
                return _strategies;
            }
            private set
            {
                SetProperty(ref _strategies, value);
            }
        }

        public IReadOnlyDictionary<int, QsLanguageEntry> Languages
        {
            get
            {
                return _languageList;
            }
            private set
            {
                SetProperty(ref _languageList, value);
            }
        }

        public IReadOnlyDictionary<int, string> AvailableModules
        {
            get { return _moduleList; }
            private set
            {
                SetProperty(ref _moduleList, value);
            }
        }

        public int? SelectedModule
        {
            get { return _selectedModule; }
            private set { SetProperty(ref _selectedModule, value); }
        }

        public IReadOnlyDictionary<SpcPath, FolderDatasets> SpcPathFolderDatasets
        {
            get { return _spcPathFolders; }
            private set { SetProperty(ref _spcPathFolders, value); }
        }

        public IQsDedicatedCalculation DedicatedCalculation
        {
            get { return _dedicatedCalculation; }
            private set { SetProperty(ref _dedicatedCalculation, value); }
        }

        public QsStatState QsStatState
        {
            get { return _qsStatState; }
            private set
            {
                SetProperty(ref _qsStatState, value);        
            }
        }

        public COMException Exception
        {
            get { return _exception; }
            private set
            {
                SetProperty(ref _exception, value);
            }
        }

        /// <summary>
        /// Evaluation results for all spc paths
        /// </summary>
        public SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO> SpcPathResults
        {
            get { return _evaluatedData; }
            private set
            {
                SetProperty(ref _evaluatedData, value);           
            }
        }

        public QsStatVersion QCOMVersion { get; private set; }

        public bool Initialize(Area area, IReadOnlyCollection<PanelItemModel> panelItemModels, GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings, ListViewSettings listViewSettings, QsStatSettings qsStatSettings)
        {
            this._evalColorSettings = evalColorSettings;
            this._generalSettings = generalSettings;
            this._listViewSettings = listViewSettings;
            this._panelItemModels = panelItemModels;
            this._area = area;
            this._selectedModule = qsStatSettings.Module;
            this._selectedLanguage = qsStatSettings.Language;
            this._selectedStrategy = qsStatSettings.Strategy;
            this._selectedReport = qsStatSettings.ReportFile;
            this._selectedPrinter = qsStatSettings.Printer?.Item1;
            bool settingsChanged;
            (settingsChanged, this._qsConnectionParameters) = GenerateQsConnectionParametersFromQsStatSettings(qsStatSettings);
            this._qsConnectionParameters = new QsConnectionParameters(qsStatSettings.Module ?? 10, qsStatSettings.Language ?? 49, qsStatSettings.User, qsStatSettings.Password);
            return settingsChanged;
        }

        private static (bool settingsChanged, QsConnectionParameters conParams) GenerateQsConnectionParametersFromQsStatSettings(QsStatSettings newSettings, int? oldModule=null, int? oldLanguage=null)
        {
            bool changes = false;
            if (!newSettings.Module.HasValue)
            {
                changes = true;
                newSettings.Module = oldModule ?? 10;
            }
            if (!Nullable.Equals(newSettings.Module, oldModule) && oldModule.HasValue)
            {
                changes = true;
                newSettings.Strategy = null;
            }
            if (!newSettings.Language.HasValue)
            {
                changes = true;
                newSettings.Language = oldLanguage ?? 49;
            }
            return (changes, new QsConnectionParameters(newSettings.Module.Value, newSettings.Language.Value, newSettings.User, newSettings.Password));
        }

        /// <summary>
        /// Start the service. Service can only be used once returned Task is completed.
        /// </summary>
        /// <returns>Start Task</returns>
        public async Task RunAsync()
        {
            await InitializePoolAsync().ConfigureAwait(false);
            this._startupTask = RunInternalAsync(this._startupCts.Token);
        }

        private async Task InitializePoolAsync()
        {
            var qsStat = await QsStatWrapper.InitializeAsync().ConfigureAwait(false);
            this._qsStatPool = ResourcePool<QsStatWrapper>.Create(new[] { qsStat }, (item) => item.Dispose());
        }

        /// <summary>
        /// Main loop of the service for period data retrieval.
        /// </summary>
        /// <param name="ct">Cancellation for the loop</param>
        /// <returns>Task for the ongoing execution</returns>
        private async Task RunInternalAsync(CancellationToken ct)
        {
            int conHandle = -1;
            QsStatWrapper qsStat=null;
            bool settingsUpdated = false;
            QsConnectionParameters qsConnectionParameters = null;
            await this._semaphore.WaitAsync(ct).ConfigureAwait(false);
            try
            {
                //TODO maybe split pool creation out
                this.QsStatState = QsStatState.RetrievingConnectionParameters;
                using (var qsStatItem = await this._qsStatPool.GetResourceAsync(ct).ConfigureAwait(false))
                {
                    this._moduleList = qsStatItem.Resource.RetrieveModuleList();
                    this._languageList = qsStatItem.Resource.RetrieveLanguageList();
                    try
                    {
                        this.QsStatState = QsStatState.Connecting;
                        var parameterTestResult = TryConnecting(qsStatItem.Resource, this._qsConnectionParameters);
                        settingsUpdated = parameterTestResult.Item1;
                        conHandle = parameterTestResult.Item2;
                        qsConnectionParameters = parameterTestResult.Item3;
                    }
                    catch(COMException e)
                    {
                        switch (e.ErrorCode)
                        {
                            case WinErrorCodes.E_ACCESSDENIED:
                                this.QsStatState = QsStatState.MissingSettings;
                                RaiseModuleInvalidEvent();
                                return;
                            case WinErrorCodes.E_UNEXPECTED:
                                this.QsStatState = QsStatState.MissingSettings;
                                RaiseUserPasswordInvalidEvent();
                                return;
                            default:
                                this.QsStatState = QsStatState.Faulted;
                                this.Exception = e;
                                return;
                        }
                    }
                    ct.ThrowIfCancellationRequested();
                    this.QsStatState = QsStatState.RetrievingMetadata;
                    if (QCOMVersion is null)
                    {
                        QCOMVersion = QsStatVersion.Create(qsStatItem.Resource.GetVersion(conHandle));
                        if (QCOMVersion.Version < 11)
                        {
                            this._qCOMEncoding = Encoding.ASCII;
                        }
                        else
                        {
                            this._qCOMEncoding = Encoding.UTF8;
                        }
                        foreach (var folder in SpcPathFolderDatasets.Values)
                            await folder.UpdateEncodingForCQOM(this._qCOMEncoding).ConfigureAwait(false);
                    }                        
                    GetQsDictionaries(qsStatItem.Resource, conHandle, qsConnectionParameters);
                }
                this._qsConnectionParameters = qsConnectionParameters;

                if (!this._selectedStrategy.HasValue || this.Strategies.ContainsKey(this._selectedStrategy.Value))
                {
                    int? replacement = TryGetDefaultStrategy(Strategies);
                    if (!Nullable.Equals(this._selectedStrategy, replacement))
                    {
                        settingsUpdated = true;
                        this._selectedStrategy = replacement;
                    }
                }

                if (settingsUpdated)
                {
                    RaiseQsSettingsChangedEvent();
                }

                ct.ThrowIfCancellationRequested();
                await InitializeQsStatElementsAsync(new[] { conHandle } , this._panelItemModels, this._qsConnectionParameters, this._selectedStrategy).ConfigureAwait(false);
            }
            catch (COMException e)
            {
                this.QsStatState = QsStatState.Faulted;
                Exception = e;
                if (conHandle > 0)
                {
                    try
                    {
                        qsStat?.Disconnect(conHandle);
                    }
                    catch { }
                }          
            }
            finally
            {
                this._semaphore.Release();
            }
        }

        private static int? TryGetDefaultStrategy(IReadOnlyDictionary<int, string> strategies)
        {
            if (!strategies.Any())
                return null;
            var candidates = strategies.Where(kvp => kvp.Value.Contains("Q-DAS", StringComparison.OrdinalIgnoreCase)).Select(kvp=>kvp.Key);
            if (candidates.Any())
                return candidates.Min();
            return strategies.Keys.Min();
        }

        /// <summary>
        /// Reinitializes the running service when parameters change.
        /// </summary>
        /// <param name="toHandleOpenConnections">Connections to keep on reinitialization, must already be configures</param>
        /// <param name="panelItemModels">Models defining which data to retrieve</param>
        /// <param name="qsConnectionParameters">Configuration for new qsStat connection</param>
        /// <param name="maybeStrategy">QsStat strategy, if any</param>
        /// <returns>A task representing the reinitialization.</returns>
        private async Task InitializeQsStatElementsAsync(IReadOnlyCollection<int> toHandleOpenConnections, IReadOnlyCollection<PanelItemModel> panelItemModels, QsConnectionParameters qsConnectionParameters, int? maybeStrategy)
        {
            QsDedicatedLoaded dedicatedLoadedPart = null;

            ResetAreaAndFolders();
            var evaluationResults = new SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO>();
            foreach (var spcPath in this._spcPathFolders.Keys)
            {
                evaluationResults[spcPath] = new SpcPathEvaluationDTO();
            }
            SpcPathResults = evaluationResults;

            if (!maybeStrategy.HasValue)
            {
                this.QsStatState = QsStatState.MissingSettings;
                using (var qsStatItem = await this._qsStatPool.GetResourceAsync(CancellationToken.None).ConfigureAwait(false))
                {
                    foreach(var conHandle in toHandleOpenConnections)
                        qsStatItem.Resource.Disconnect(conHandle);
                }
                return;
            }

            dedicatedLoadedPart = this.DedicatedCalculation.LoadedDataset;
            int strategy = maybeStrategy.Value;
            async Task<int> conFactory()
            {
                return await this._exclusiveQsStatAccess.Enqueue((qsStat) =>
                {
                    var conId = qsStat.CreateConnection(this._qsConnectionParameters);
                    Debug.WriteLine($"[con]\t{conId}");
                    return conId;
                }).ConfigureAwait(false);
            }
            void conOnDispose(int conId)
            {
                Debug.WriteLine($"[~con]\t{conId}");
                using (var qsStat = this._qsStatPool.GetResourceAsync(CancellationToken.None).Result)
                    qsStat.Resource.Disconnect(conId);
            }
            //note: mbe: due to problems with parallel execution of qsstat just create two connections. one for exclusive use of dedicated calculation and one for periodical calculation. parallel execution limited by the execution resource pool anyway
            int totalConnections = 2;
            this._exclusiveQsStatAccess = new ResourceService<QsStatWrapper>(this._qsStatPool);
            this._exclusiveQsStatAccess.Start(1);
            this._qsStatConnectionPool = await ResourcePool<int>.CreateAsync(totalConnections, toHandleOpenConnections, conFactory, conOnDispose).ConfigureAwait(false);
            this._periodicalCalculation = new QsPeriodicalCalculation(this._exclusiveQsStatAccess, this._qsStatConnectionPool);
            this.DedicatedCalculation = await QsDedicatedCalculation.CreateAsync(this.SpcPathFolderDatasets, this._exclusiveQsStatAccess, this._qsStatConnectionPool, this._generalSettings, dedicatedLoadedPart).ConfigureAwait(false);
            this._periodicalCalculation.Start(this.SpcPathFolderDatasets, panelItemModels, this._generalSettings, this._evalColorSettings, this._listViewSettings, SpcPathResults);
            RaisePropertyChanged(nameof(SpcPathResults));
            this.QsStatState = QsStatState.Started;
        }

        /// <summary>
        /// Tries connecting to the com server with the given parameters
        /// </summary>
        /// <param name="qsStat">qsStat wrapper</param>
        /// <param name="desiredConParams">Parameters for creating the connection</param>
        /// <returns>boolean for parameter update required, generated connection handle, desired or updated connection parameters </returns>
        private Tuple<bool, int, QsConnectionParameters> TryConnecting(QsStatWrapper qsStat, QsConnectionParameters desiredConParams)
        {
            QsConnectionParameters toUseParams;
            bool needToUpdateSettings = false;
            string user;
            string password;
            int language;
            int module;
            if (String.IsNullOrWhiteSpace(desiredConParams.User) || String.IsNullOrWhiteSpace(desiredConParams.Password))
            {
                needToUpdateSettings = true;
                user = "Superuser";
                password = "Superuser";
            }else
            {
                user = desiredConParams.User;
                password = desiredConParams.Password;
            }

            if (!Languages.ContainsKey(desiredConParams.Language))
            {
                needToUpdateSettings = true;
                if (!Languages.Any())
                    throw new ArgumentOutOfRangeException("The Qdas COM-Server does not support any languages.");
                if (Languages.ContainsKey(49))
                {
                    language = 49;
                }
                else if (Languages.ContainsKey(44))
                {
                    language = 44;
                }
                else
                {
                    language = Languages.First().Key;
                }
            }
            else{
                language = desiredConParams.Language;
            }
            if (!AvailableModules.ContainsKey(desiredConParams.Module))
            {
                needToUpdateSettings = true;
                if (!AvailableModules.Any())
                    throw new ArgumentOutOfRangeException("The Qdas COM-Server does not support any modules.");
                module = AvailableModules.First().Key;
            }else
            {
                module = desiredConParams.Module;
            }
            if (needToUpdateSettings)
            {
                toUseParams = new QsConnectionParameters(module, language, user, password);
            }else
            {
                toUseParams = desiredConParams;
            }
            int connectionHandle = qsStat.CreateConnection(toUseParams);
            return new Tuple<bool, int, QsConnectionParameters>(needToUpdateSettings, connectionHandle, toUseParams);
        }

        private void GetQsDictionaries(QsStatWrapper qsStat, int connectionHandle, QsConnectionParameters connectionParameters)
        {
            AvailableModules = qsStat.RetrieveModuleList(connectionHandle);
            PartKFields = qsStat.RetrievePartKFieldList(connectionHandle);
            CharacteristicKFields = qsStat.RetrieveCharacteristicKFieldList(connectionHandle);
            Reports = qsStat.RetrieveReportList(connectionHandle);
            Printers = qsStat.RetrievePrinterList(connectionHandle);
            StatResult = qsStat.RetrieveStatList(connectionHandle, connectionParameters.Module);
            Strategies = qsStat.RetrieveStrategyList(connectionHandle, connectionParameters.Module);
            Graphics = qsStat.RetrieveGraphicsList(connectionHandle, connectionParameters.Module);
        }

        private void ClearQsDictionariesExceptModules()
        {
            PartKFields = new Dictionary<int, string>();
            CharacteristicKFields = new Dictionary<int, string>();
            Reports = new List<QsReportEntry>();
            Printers = new Dictionary<int, string>();
            StatResult = new Dictionary<int, string>();
            Strategies = new Dictionary<int, string>();
            Graphics = new Dictionary<int, string>();
        }

        /// <summary>
        /// Updates the settings with the set fields of the service. Currently strategy, module, report file, printer, language, module, user and password
        /// </summary>
        /// <param name="toUpdateSettings">settings which will be modified</param>
        public async Task UpdateQsSettings(QsStatSettings toUpdateSettings)
        {
            await this._semaphore.WaitAsync().ConfigureAwait(false);
            toUpdateSettings.Strategy = this._selectedStrategy;
            toUpdateSettings.Module = this.SelectedModule;
            toUpdateSettings.ReportFile = this._selectedReport;
            if (this._selectedPrinter.HasValue)
            {
                if(Printers.TryGetValue(this._selectedPrinter.Value, out string name)){
                    toUpdateSettings.Printer = Tuple.Create(this._selectedPrinter.Value, name);
                }
            }
            else
            {
                toUpdateSettings.Printer = null;
            }
            toUpdateSettings.Language = this._qsConnectionParameters.Language;
            toUpdateSettings.Module = this._qsConnectionParameters.Module;
            toUpdateSettings.User = this._qsConnectionParameters.User;
            toUpdateSettings.Password = this._qsConnectionParameters.Password;
            this._semaphore.Release();
        }

        private bool CheckAndUpdateSettingsQsDictionaries(QsStatSettings qsStatSettings)
        {
            bool changed = false;
            if (qsStatSettings.Strategy.HasValue && !Strategies.ContainsKey(qsStatSettings.Strategy.Value))
            {
                changed = true;
                qsStatSettings.Strategy = null;
            }
            if (!String.IsNullOrWhiteSpace(qsStatSettings.ReportFile) && !Reports.Any(item=>String.Equals(item.FileName, qsStatSettings.ReportFile, StringComparison.Ordinal)))
            {
                changed = true;
                qsStatSettings.ReportFile = "";
            }
            if (qsStatSettings.Printer != null && !Printers.Any(item=>item.Key == qsStatSettings.Printer.Item1 && String.Equals(item.Value, qsStatSettings.Printer.Item2)))
            {
                changed = true;
                qsStatSettings.Printer = null;
            }
            return changed;
        }

        private void ResetAreaAndFolders()
        {
            var newSpcDictionary = new Dictionary<SpcPath, FolderDatasets>();
            if (this._area != null)
            {
                GeneralSettings settings = this._generalSettings;
                foreach (var spcPath in this._area.ReadonlyPaths)
                {
                    newSpcDictionary[spcPath] = new FolderDatasets(spcPath.DeleteOldData, spcPath.ShowDataNumberDays, settings, this._qCOMEncoding);
                }
            }
            this.SpcPathFolderDatasets = newSpcDictionary;
        }

        public void UpdateGeneralRetrievalKeys(ListViewSettings listViewSettings)
        {
            this._periodicalCalculation.UpdateGeneralRetrievalKeys(listViewSettings);
        }

        /// <summary>
        /// Update for the settings influencing the data evaluation
        /// </summary>
        /// <param name="generalSettings">Settings concerning evalution</param>
        /// <param name="evalColorSettings">Define to observe violations</param>
        public void SetSettings(GeneralSettings generalSettings, EvaluationAndColorSettings evalColorSettings)
        {
            _semaphore.Wait();
            this._generalSettings = generalSettings;
            this._evalColorSettings = evalColorSettings;
            var evaluationResults = new SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO>();
            foreach (var spcPath in this._spcPathFolders.Keys)
            {
                evaluationResults[spcPath] = new SpcPathEvaluationDTO();
            }
            SpcPathResults = evaluationResults;
            var updateTasks = new List<Task>(this._spcPathFolders.Count);
            foreach (var fd in this._spcPathFolders.Values)
                updateTasks.Add(fd.SetGeneralSettingsAsync(generalSettings));
            Task.WhenAll(updateTasks).Wait();
            this._periodicalCalculation.UpdateSettings(this._generalSettings, this._evalColorSettings, evaluationResults);
            this._dedicatedCalculation.UpdateSettings(this._generalSettings);
            //TODO propertychanged
            //RaisePropertyChanged(nameof(SpcPathPanelItemModels));
            _semaphore.Release();
        }

        /// <summary>
        /// Set the new data retrieval targets for evaluation and reset current data./>
        /// </summary>
        /// <param name="area">New data retrieval targets</param>
        public void SetArea(Area area)
        {
            //TODO make async task?
            _semaphore.Wait();
            this._area = area;
            ResetAreaAndFolders();
            var evaluationResults = new SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO>();
            foreach (var spcPath in this._spcPathFolders.Keys)
            {
                evaluationResults[spcPath] = new SpcPathEvaluationDTO();
            }
            SpcPathResults = evaluationResults;
            this._periodicalCalculation.UpdateSpc(this._spcPathFolders, SpcPathResults);
            this.DedicatedCalculation.UpdateSpc(this._spcPathFolders);
            _semaphore.Release();
        }

        /// <summary>
        /// Updates service to use new settings. Raises events in case not all settings entries were valid.
        /// </summary>
        /// <param name="newSettings">New QsStatSettings to apply</param>
        /// <returns>true, if the settings were changed and the appropriate event raised, otherwise false.</returns>
        /// <event cref="QsSettingsChangedEvent">Raised when the applied settings differ from the supplied settings</event>
        /// <event cref="ModuleInvalidEvent">Raised when the supplied module was invalid</event>
        /// <event cref="UserPasswordInvalidEvent">Raised when the supplied username and password was invalid</event>
        public async Task<bool> SetQsParametersAsync(QsStatSettings newSettings)
        {
            await _semaphore.WaitAsync().ConfigureAwait(false);
            int conHandle = -1;
            bool settingsChanged = false;
            try
            {
                
                if (!CheckQsSettingChangesRequireRestart(newSettings))
                {
                    this._selectedReport = newSettings.ReportFile;
                    this._selectedPrinter = newSettings.Printer?.Item1??null;
                    return false;
                }
                var evaluationResults = new SimpleObservableConcurrentDictionary<SpcPath, SpcPathEvaluationDTO>();
                foreach (var spcPath in this._spcPathFolders.Keys)
                {
                    evaluationResults[spcPath] = new SpcPathEvaluationDTO();
                }
                SpcPathResults = evaluationResults;
                ClearQsDictionariesExceptModules();
                await ShutdownCalculationServicesAsync().ConfigureAwait(false);
                this._periodicalCalculation = new QsPeriodicalCalculationDummy();
                this.DedicatedCalculation = new QsDedicatedCalculationDummy();
                if (this._exclusiveQsStatAccess is object)
                    await this._exclusiveQsStatAccess.CancelAsync().ConfigureAwait(false);
                this._exclusiveQsStatAccess = null;
                this._qsStatConnectionPool?.Dispose();
                this._qsStatConnectionPool = null;
           
                QsConnectionParameters qsConnectionParameters;
                (settingsChanged, qsConnectionParameters) = GenerateQsConnectionParametersFromQsStatSettings(newSettings, this._selectedModule, this._selectedLanguage);
                this.QsStatState = QsStatState.Connecting;
                using (var qsStat = await this._qsStatPool.GetResourceAsync(CancellationToken.None).ConfigureAwait(false))
                {
                    try
                    {
                        conHandle = qsStat.Resource.CreateConnection(qsConnectionParameters);
                        this.SelectedModule = qsConnectionParameters.Module;
                    }catch(COMException e)
                    {
                        switch (e.ErrorCode)
                        {
                            case WinErrorCodes.E_ACCESSDENIED:
                                RaiseModuleInvalidEvent();
                                return false;
                            case WinErrorCodes.E_UNEXPECTED:
                                RaiseUserPasswordInvalidEvent();
                                return false;
                            default:
                                this.Exception = e;
                                return false;
                        }
                    }
                    try
                    {
                        this.QsStatState = QsStatState.RetrievingMetadata;
                        GetQsDictionaries(qsStat.Resource, conHandle, qsConnectionParameters);
                    }catch(COMException e)
                    {
                        this.Exception = e;
                        try
                        {
                            qsStat.Resource.Disconnect(conHandle);
                        }
                        catch { }
                        return false;
                    }        
                }                       
                if (this.Reports.Any(elem=> String.Equals(elem.FileName, newSettings.ReportFile, StringComparison.OrdinalIgnoreCase)))
                {
                    this._selectedReport = newSettings.ReportFile;
                }
                else 
                {
                    settingsChanged = true;
                    this._selectedReport = newSettings.ReportFile = null;
                }

                if (newSettings.Printer == null || this.Printers.ContainsKey(newSettings.Printer.Item1))
                {
                    this._selectedPrinter = newSettings.Printer?.Item1??null;
                }
                else
                {
                    settingsChanged = true;
                    this._selectedPrinter = null;
                    newSettings.Printer = null;
                }
                
                if (newSettings.Strategy.HasValue && this.Strategies.ContainsKey(newSettings.Strategy.Value))
                {
                    this._selectedStrategy = newSettings.Strategy;
                }
                else
                {
                    settingsChanged = true;
                    this._selectedStrategy = newSettings.Strategy = TryGetDefaultStrategy(this.Strategies);
                }

                this._selectedLanguage = qsConnectionParameters.Language;
                this._qsConnectionParameters = qsConnectionParameters;

                if (settingsChanged)
                    RaiseQsSettingsChangedEvent();

                if (this._selectedLanguage.HasValue && this._selectedStrategy.HasValue)
                {
                    await InitializeQsStatElementsAsync(new[] { conHandle }, this._panelItemModels, this._qsConnectionParameters, this._selectedStrategy.Value).ConfigureAwait(false);
                }
                else
                {
                    this.QsStatState = QsStatState.MissingSettings;
                }
                Exception = null;
                return settingsChanged;
            }
            catch (COMException e)
            {
                this.QsStatState = QsStatState.Faulted;
                Exception = e;
                if (conHandle > 0)
                {
                    try
                    {
                        using (var qsStatItem = await this._qsStatPool.GetResourceAsync(CancellationToken.None).ConfigureAwait(false))
                        {
                            qsStatItem.Resource.Disconnect(conHandle);
                        }
                    }
                    catch { }
                }
                return settingsChanged;
            }
            finally
            {
                _semaphore.Release();
            }
        }

        private bool CheckQsSettingChangesRequireRestart(QsStatSettings newSettings)
        {
            return !Nullable.Equals(newSettings.Language, this._selectedLanguage) || !Nullable.Equals(newSettings.Strategy, this._selectedStrategy) ||
                !String.Equals(newSettings.User, this._qsConnectionParameters.User) || !String.Equals(newSettings.Password, this._qsConnectionParameters.Password) ||
                !Nullable.Equals(newSettings.Module, this._qsConnectionParameters.Module);
        }

        private async Task ShutdownCalculationServicesAsync()
        {
            this.QsStatState = QsStatState.ShuttingDown;
            this.DedicatedCalculation?.Shutdown();
            this.DedicatedCalculation?.Dispose();
            await (this._periodicalCalculation?.ShutdownAsync()??Task.CompletedTask).ConfigureAwait(false);
            this.QsStatState = QsStatState.Shutdown;
        }

        /// <summary>
        /// Set items defining which data to retrieve.
        /// </summary>
        /// <param name="panelItemModels">The items</param>
        public void SetPanelItemModels(IReadOnlyCollection<PanelItemModel> panelItemModels)
        {
            this._panelItemModels = panelItemModels;
            this._periodicalCalculation.UpdatePanelItemModels(panelItemModels);
        }

        /// <summary>
        /// Set width and height of a panel for generating images
        /// </summary>
        /// <param name="panelName">Name of the panel as identifier</param>
        /// <param name="height">Height for this panel</param>
        /// <param name="width">Width for this panel</param>
        public void SetPanelHeightAndWidth(string panelName, double height, double width)
        {
            Debug.Assert(!Double.IsNaN(height));
            Debug.Assert(!Double.IsNaN(width));
            height = Math.Max(height, 50);
            width = Math.Max(width, 50);
            this._periodicalCalculation.SetPanelHeightAndWidth(panelName, height, width);
        }

        /// <summary>
        /// Request next part evaluation of a <see cref="SpcPath"/>. All data is checked for evaluation.
        /// </summary>
        /// <param name="path">Item for which the next part should be retrieved</param>
        /// <returns>Task representing state of request. Task is completed if the SpcPath is not valid anymore.</returns>
        public Task RequestNextPart(SpcPath path)
        {
            if (!_spcPathFolders.ContainsKey(path))
                return Task.CompletedTask;
            return this._periodicalCalculation.ManualRequest(path, ManualRequestTypes.NextPart);
        }

        /// <summary>
        /// Request previous part evaluation of a <see cref="SpcPath"/>. All data is checked for evaluation.
        /// </summary>
        /// <param name="path">Item for which the previous part should be retrieved</param>
        /// <returns>Task representing state of request. Task is completed if the SpcPath is not valid anymore.</returns>
        public Task RequestPreviousPart(SpcPath path)
        {
            if (!this._spcPathFolders.ContainsKey(path))
                return Task.CompletedTask;
            return this._periodicalCalculation.ManualRequest(path, ManualRequestTypes.PreviousPart);
        }

        /// <summary>
        /// Request next characteristic evaluation for the current part of a <see cref="SpcPath"/>. All data is checked for evaluation.
        /// </summary>
        /// <param name="path">Item for which the next characteristic should be retrieved</param>
        /// <returns>Task representing state of request. Task is completed if the SpcPath is not valid anymore.</returns>
        public Task RequestNextCharacteristic(SpcPath path)
        {
            if (!this._spcPathFolders.ContainsKey(path))
                return Task.CompletedTask;
            return this._periodicalCalculation.ManualRequest(path, ManualRequestTypes.NextCharacteristic);
        }

        /// <summary>
        /// Request previous characteristic evaluation for the current part of a <see cref="SpcPath"/>. All data is checked for evaluation.
        /// </summary>
        /// <param name="path">Item for which the previous characteristic part should be evaluated</param>
        /// <returns>Task representing state of request. Task is completed if the SpcPath is not valid anymore.</returns>
        public Task RequestPreviousCharacteristic(SpcPath path)
        {
                if (!this._spcPathFolders.ContainsKey(path))
                    return Task.CompletedTask;
                return this._periodicalCalculation.ManualRequest(path, ManualRequestTypes.PreviousCharacteristic);
        }

        /// <summary>
        /// Request a refresh for a <see cref="SpcPath"/>.
        /// </summary>
        /// <param name="path">Item for which to refresh</param>
        /// <returns>Task representing state of request. Task is completed if the SpcPath is not valid anymore.</returns>
        public Task RequestRefresh(SpcPath path)
        {
            if (!this._spcPathFolders.ContainsKey(path))
                return Task.CompletedTask;
            return this._periodicalCalculation.ManualRequest(path, ManualRequestTypes.Refresh);
        }
        
        /// <summary>
        /// Get graphic of a report for specific data
        /// </summary>
        /// <param name="spcPath">General item the data belongs to</param>
        /// <param name="fileGuid">Id of the data</param>
        /// <param name="pageNumber">Desired page number of the report</param>
        /// <param name="characteristicNumber">Specific characteristic number for the report, most likely will not matter</param>
        /// <param name="reportFileName">.def file name of the report</param>
        /// <param name="ct">Cancellation of the request</param>
        /// <returns>Result task. Will be cancelled if request does not fit with current loaded data.</returns>
        public Task<byte[]> RequestGraphicEvaluation(SpcPath spcPath, Guid fileGuid, int pageNumber, int characteristicNumber, string reportFileName, CancellationToken ct)
        {
            if (String.IsNullOrWhiteSpace(reportFileName) || !_spcPathFolders.TryGetValue(spcPath, out FolderDatasets fd))
            {
                return this._cancelledImageTask;
            }
            DataSet ds = fd.DataSets.FirstOrDefault(elem => elem.Guid.Equals(fileGuid));  
            if (ds == null)
            {
                return this._cancelledImageTask;
            }
            var data = ds.GetQueryableQsDataAsync();
            
            var task = this._exclusiveQsStatAccess?.Enqueue(
                              async (qsStat) =>
                              {
                                  var retrievedData = await data.ConfigureAwait(false);
                                  using (var con = await _qsStatConnectionPool.GetResourceAsync(ct).ConfigureAwait(false))
                                    return await RequestReportEvaluationHelperAsync(qsStat, con, retrievedData, characteristicNumber, pageNumber, reportFileName, ct).ConfigureAwait(false);
                              }, PriorityType.High)??this._cancelledImageTask;
            return task;
        }



        private async Task<byte[]> RequestReportEvaluationHelperAsync(QsStatWrapper qsHandle, PoolItem<int> connection, QueryableQsData data, int characteristicNumber, int pageNumber, string reportIdentifier, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();
            await qsHandle.LoadAndEvaluateDataAsync(connection.Resource, data.DfqData, ct).ConfigureAwait(false);
            ct.ThrowIfCancellationRequested();
            int width = 1000;//1400
            int height = (int)(width*Math.Sqrt(2)); //900
            int partNr = 1;
            if (qsHandle.GetReportResult(connection.Resource, reportIdentifier, partNr, characteristicNumber, pageNumber, width, height, out byte[] retrieved))
                return retrieved;
            return null;
        }

        /// <summary>
        /// Requests printing of a specific report. If no page number is given all pages are requested
        /// </summary>
        /// <param name="spcPath">Spc path the to print data belongs to</param>
        /// <param name="fileGuid">Id of the file to be printed</param>
        /// <param name="pageNumber">When a page number is given print the specific page, otherwise print all pages.</param>
        /// <param name="reportFileName">.def filename of the report to be printed</param>
        /// <param name="ct">Cancellation</param>
        /// <returns>Printing task. Will be cancelled if request does not fit with current loaded data.</returns>
        public Task RequestPrintReport(SpcPath spcPath, Guid fileGuid, int? pageNumber, string reportFileName, CancellationToken ct)
        {
            int? printerId = this._selectedPrinter;
            if (String.IsNullOrWhiteSpace(reportFileName) || !printerId.HasValue || !_spcPathFolders.TryGetValue(spcPath, out FolderDatasets fd))
            {
                return this._cancelledTask;
            }
            DataSet ds = fd.DataSets.FirstOrDefault(elem => elem.Guid.Equals(fileGuid));
            if (ds == null)
            {
                return this._cancelledTask;
            }
            var data = ds.GetQueryableQsDataAsync();

            var task = this._exclusiveQsStatAccess?.Enqueue(
                              async (qsStat) =>
                              {
                                  var retrievedData = await data.ConfigureAwait(false);
                                  using (var con = await _qsStatConnectionPool.GetResourceAsync(ct).ConfigureAwait(false))
                                  {
                                      await RequestPrintReportHelperAsync(qsStat, con, retrievedData, pageNumber, reportFileName, printerId.Value, ct).ConfigureAwait(false);
                                  }
                                      
                              }, PriorityType.High) ?? this._cancelledTask;
            return task;
        }

        private async Task RequestPrintReportHelperAsync(QsStatWrapper qsHandle, PoolItem<int> connection, QueryableQsData data, int? pageNumber, string reportFileName, int printerId, CancellationToken ct)
        {
            ct.ThrowIfCancellationRequested();
            await qsHandle.LoadAndEvaluateDataAsync(connection.Resource, data.DfqData, ct).ConfigureAwait(false);
            ct.ThrowIfCancellationRequested();
            if (pageNumber.HasValue)
                qsHandle.PrintReport(connection.Resource, reportFileName, printerId, pageNumber.Value);
            else
                qsHandle.PrintAllReport(connection.Resource, reportFileName, printerId);
        }

        private Task<T> GenerateCancelledTask<T>()
        {
            var ct = new CancellationToken(true);
            return Task.FromCanceled<T>(ct);
        }

        private Task GenerateCancelledTask()
        {
            var ct = new CancellationToken(true);
            return Task.FromCanceled(ct);
        }

        private void RaiseModuleInvalidEvent()
        {
            foreach (EventHandler<EventArgs> handler in this.ModuleInvalidEvent?.GetInvocationList() ?? new EventHandler<EventArgs>[0])
            {
                Task.Factory.FromAsync((asyncCallback, @object) => handler.BeginInvoke(this, EventArgs.Empty, asyncCallback, @object), handler.EndInvoke, null);
            }
        }

        private void RaiseUserPasswordInvalidEvent()
        {
            foreach (EventHandler<EventArgs> handler in this.UserPasswordInvalidEvent?.GetInvocationList() ?? new EventHandler<EventArgs>[0])
            {
                Task.Factory.FromAsync((asyncCallback, @object) => handler.BeginInvoke(this, EventArgs.Empty, asyncCallback, @object), handler.EndInvoke, null);
            }
        }

        private void RaiseQsSettingsChangedEvent()
        {
            foreach (EventHandler<EventArgs> handler in this.QsSettingsChangedEvent?.GetInvocationList() ?? new EventHandler<EventArgs>[0])
            {
                Task.Factory.FromAsync((asyncCallback, @object) => handler.BeginInvoke(this, EventArgs.Empty, asyncCallback, @object), handler.EndInvoke, null);
            }
        }

        #region IDisposable
        private bool _disposed = false;

        private void Dispose(bool disposing)
        {
            if (this._disposed)
                return;

            if (disposing)
            {
                this._startupCts?.Cancel();
                this._startupCts?.Dispose();
                this._startupCts = null;
                try { this._startupTask.Wait(); } catch { }
                ShutdownCalculationServicesAsync().Wait();
                if (this._exclusiveQsStatAccess is object)
                    this._exclusiveQsStatAccess.CancelAsync().Wait();
                this._dedicatedCalculation?.Dispose();
                this._dedicatedCalculation = null;
                this._qsStatConnectionPool?.Dispose();
                this._qsStatConnectionPool = null;
                this._qsStatPool?.Dispose();
                this._qsStatPool = null;
                this._semaphore?.Dispose();
                this._semaphore = null;
            }

            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~QsStatService()
        {
            Dispose(false);
        }
        #endregion
    }
}
