﻿using Promess.Pbld.Data;
using Promess.Pbld.Data.Measurements;
using System;
using System.Collections.Generic;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// Loaded data for querying independent of QCOM
    /// </summary>
    public class QsDedicatedLoaded
    {
        public QsDedicatedLoaded(SpcPath spcPath, Guid datasetGuid, QueryableQsData parsedData, int totalCharacteristics, IReadOnlyList<int> enabledCharacteristics)
        {
            this.SpcPath = spcPath;
            this.DatasetGuid = datasetGuid;
            this.TotalCharacteristics = totalCharacteristics;
            this.EnabledCharacteristics = enabledCharacteristics;
            this.ParsedData = parsedData;
        }

        public SpcPath SpcPath { get; private set; }
        public Guid DatasetGuid { get; private set; }
        public int TotalCharacteristics { get; private set; }
        public IReadOnlyList<int> EnabledCharacteristics { get; private set; }
        public QueryableQsData ParsedData { get; private set; }
    }
}
