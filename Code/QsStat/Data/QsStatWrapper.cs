﻿using Promess.Common.Data;
using Qs_stat;
using Promess.QsStat.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// Wrapper for qsStat COM component
    /// Class should be thread safe, but there seems to be a bug in the qsstat component when accessing multiple times at once, so access is restricted internally where possible.
    /// Retrieving data should be exclusive for a connection, otherwise there will be internal errors and degraded performance.
    /// </summary>
    public class QsStatWrapper: IQsStatDataRetrieval, IDisposable
    {
        private static TimeSpan STD_DELAY = TimeSpan.FromMilliseconds(200); 
        //FIXME handle infinite hang later. currently restart app..
        private const int TIMEOUT = int.MaxValue;
        private const int MAXTRIES = 3;
        private const int QSUNCONNECTEDHANDLE = 0;
        private const int RESET_PREVIOUSLY_LOADED = 0;
        private const int RESERVED = 0;
        private const int THREAD_MODE_ASYNC = 0;
        //note mbe: async eval access seems to be a qsstat bug
        private const int THREAD_MODE_SYNC = 1;
        private const string ERR_TEXT = "---";
        private TQsstatRemoteControl _qsstat;

        private QsStatWrapper(TQsstatRemoteControl qsStat)
        {
            this._qsstat = qsStat;
        }

        #region Initialization


        public static async Task<QsStatWrapper> InitializeAsync()
        {
            SemaphoreSlim semaphore = new SemaphoreSlim(0, 1);
            IQsEvents_OnQSSTATInitializedEventHandler initializedHandler = new IQsEvents_OnQSSTATInitializedEventHandler(
                () => {
                    semaphore.Release();
                });
            TQsstatRemoteControl qsstat = new TQsstatRemoteControl();
            qsstat.OnQSSTATInitialized += initializedHandler;
            if (!await semaphore.WaitAsync(TIMEOUT).ConfigureAwait(false))
            {
                qsstat.OnQSSTATInitialized -= initializedHandler;
                //throw exception or something, could not connect
                throw new TimeoutException("qdas connect timeout");

            }
            qsstat.OnQSSTATInitialized -= initializedHandler;
            var wrapper = new QsStatWrapper(qsstat);
            qsstat.OnGetAliveState += new IQsEvents_OnGetAliveStateEventHandler(wrapper.KeepAliveHandler);
            return wrapper;
        }

        /// <summary>
        /// For OnKeepAlive for TQsstatRemoteControl. Seems to be unneeded?
        /// </summary>
        /// <param name="state"></param>
        private void KeepAliveHandler(out int state)
        {
            state = 1;
        }
        #endregion

        #region connect and disconnect
        internal int CreateConnection(QsConnectionParameters qsParams)
        {
            return CreateConnection(qsParams.Module, qsParams.Language, qsParams.User, qsParams.Password);
        }

        internal void SetStrategy(int connectionHandle, int strategy)
        {
            this._qsstat.SetEvaluationStrategy(connectionHandle, 0, strategy);
        }

        /// <summary>
        /// Tries to create a connection with the diven parameters
        /// </summary>
        /// <param name="module">qs stat module</param>
        /// <param name="language">language id</param>
        /// <param name="user">username</param>
        /// <param name="password">password</param>
        /// <returns>connection handle >0 on success</returns>
        /// <exception cref="COMException">On an error. E_AccessDenied most likely for incorrect module and E_Unexpected for invalid username and password.</exception>
        internal int CreateConnection(int module, int language, string user, string password)
        {
            this._qsstat.ClientConnect(out int connectionHandle, module, language, user, password);
            //string version = "";
            //_qsstat.QsstatVersion(connectionHandle, out version);
            return connectionHandle;
        }

        internal void Disconnect(int connectionHandle)
        {
            _qsstat.ClientDisconnect(connectionHandle);
        }
        #endregion

        #region connection independent
        [DebuggerNonUserCode]
        internal Dictionary<int, string> RetrieveModuleList(int? maybeConnectionHandle = null)
        {
            IEnumModule modules = (IEnumModule)_qsstat;
            Dictionary<int, string> moduleLookup = new Dictionary<int, string>();
            int connectionHandle = maybeConnectionHandle ?? QSUNCONNECTEDHANDLE;
            int moduleId = 0;
            string moduleName = string.Empty;
            try
            {
                modules.GetFirstModule(connectionHandle, out moduleId, out moduleName);
                moduleLookup[moduleId] = moduleName?? string.Empty;
                while (true)
                {
                    modules.GetNextModule(connectionHandle, out moduleId, out moduleName);
                    moduleLookup[moduleId] = moduleName?? string.Empty;
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return moduleLookup;
        }

        [DebuggerNonUserCode]
        internal Dictionary<int, QsLanguageEntry> RetrieveLanguageList()
        {
            IEnumLanguage languages = (IEnumLanguage)_qsstat;
            Dictionary<int, QsLanguageEntry> languageLookup = new Dictionary<int, QsLanguageEntry>();
            int languageId = 0;
            string languageName = "";
            string languageShortName = "";
            try
            {
                languages.GetFirstLanguage(QSUNCONNECTEDHANDLE, out languageName, out languageShortName, out languageId);
                languageLookup[languageId] = new QsLanguageEntry(languageId, languageShortName, languageName);
                while (true)
                {
                    languages.GetNextLanguage(QSUNCONNECTEDHANDLE, out languageName, out languageShortName, out languageId);
                    languageLookup[languageId] = new QsLanguageEntry(languageId, languageShortName, languageName);
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return languageLookup;
        }
        #endregion

        #region connection dependent
        internal string GetVersion(int handle)
        {
            this._qsstat.QsstatVersion(handle, out string version);
            return version;
        }

        [DebuggerNonUserCode]
        internal Dictionary<int, string> RetrievePartKFieldList(int connectionHandle)
        {
            IEnumFieldKeyName qsKeyFields = (IEnumFieldKeyName)_qsstat;
            const int SUBKEY = 0;
            int fieldId = 0;
            string fieldName = string.Empty;
            var partKeyFields = new Dictionary<int, string>();
            try
            {
                qsKeyFields.GetFirstPartField(connectionHandle, ref fieldId, SUBKEY, ref fieldName);
                partKeyFields[fieldId] = fieldName?? string.Empty;
                while (true)
                {
                    qsKeyFields.GetNextPartField(connectionHandle, ref fieldId, SUBKEY, ref fieldName);
                    partKeyFields[fieldId] = fieldName?? string.Empty;
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return partKeyFields;
            //note: mbe: old code did not retrieve value fields
        }

        [DebuggerNonUserCode]
        internal Dictionary<int, string> RetrieveCharacteristicKFieldList(int connectionHandle)
        {
            IEnumFieldKeyName qsKeyFields = (IEnumFieldKeyName)_qsstat;
            const int SUBKEY = 0;
            int fieldId = 0;
            string fieldName = string.Empty;
            var characteristicKeyFields = new Dictionary<int, string>();
            try
            {
                qsKeyFields.GetFirstCharacteristicField(connectionHandle, ref fieldId, SUBKEY, ref fieldName);
                characteristicKeyFields[fieldId] = fieldName?? string.Empty;
                while (true)
                {
                    qsKeyFields.GetNextCharacteristicField(connectionHandle, ref fieldId, SUBKEY, ref fieldName);
                    characteristicKeyFields[fieldId] = fieldName?? string.Empty;
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return characteristicKeyFields;
            //note: mbe: old code did not retrieve value fields
        }

        [DebuggerNonUserCode]
        internal List<QsReportEntry> RetrieveReportList(int connectionHandle)
        {
            //TODO old pbld actually copies some reports before this call and filters reports. only the ones in reports.ini are accepted
            IEnumReport qsStrategy = (IEnumReport)_qsstat;

            int reportId = 0;
            string reportName = string.Empty;
            string reportFileName = string.Empty;
            var reports = new List<QsReportEntry>();
            try
            {
                qsStrategy.GetFirstReport(connectionHandle, out reportId, out reportName, out reportFileName);
                reports.Add(new QsReportEntry(reportId, reportName, reportFileName));
                while (true)
                {
                    qsStrategy.GetNextReport(connectionHandle, out reportId, out reportName, out reportFileName);
                    reports.Add(new QsReportEntry(reportId, reportName, reportFileName));
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return reports;
        }

        [DebuggerNonUserCode]
        internal Dictionary<int, string> RetrievePrinterList(int connectionHandle)
        {
            IEnumPrinter qsStrategy = (IEnumPrinter)_qsstat;

            int printer = 0;
            string printerName = String.Empty;
            var printers = new Dictionary<int, string>();
            try
            {
                qsStrategy.GetFirstPrinter(connectionHandle, out printer, out printerName);
                printers[printer] = printerName??string.Empty;
                while (true)
                {
                    qsStrategy.GetNextPrinter(connectionHandle, out printer, out printerName);
                    printers[printer] = printerName??string.Empty;
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return printers;
        }

        [DebuggerNonUserCode]
        internal Dictionary<int, string> RetrieveStrategyList(int connectionHandle, int module)
        {
            IEnumEvaluationStrategy qsStrategy = (IEnumEvaluationStrategy)_qsstat;
            const int STUDY = 0;
            int strategy = 0;
            string strategyName = string.Empty;
            var strategies = new Dictionary<int, string>();
            try
            {
                qsStrategy.GetFirstEvaluationStrategy(connectionHandle, module, STUDY, out strategy, out strategyName);
                strategies[strategy] = strategyName??string.Empty;
                while (true)
                {
                    qsStrategy.GetNextEvaluationStrategy(connectionHandle, module, STUDY, out strategy, out strategyName);
                    strategies[strategy] = strategyName??string.Empty;
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return strategies;
        }

        [DebuggerNonUserCode]
        internal Dictionary<int, string> RetrieveStatList(int connectionHandle, int module)
        {
            IEnumStatResults statResults = (IEnumStatResults)_qsstat;
            Dictionary<int, string> statLookup = new Dictionary<int, string>();
            int resultId = 0;
            string resultName = string.Empty;
            try
            {
                statResults.GetFirstStatResult(connectionHandle, module, out resultId, out resultName);
                statLookup[resultId] = resultName?? string.Empty;
                while (true)
                {
                    statResults.GetNextStatResult(connectionHandle, module, out resultId, out resultName);
                    statLookup[resultId] = resultName?? string.Empty;
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return statLookup;
        }

        [DebuggerNonUserCode]
        internal Dictionary<int, string> RetrieveGraphicsList(int connectionHandle, int module)
        {
            IEnumGraphic qsGraphic = (IEnumGraphic)_qsstat;
            Dictionary<int, string> graphicLookup = new Dictionary<int, string>();
            int resultId = 0;
            string resultName = string.Empty;
            try
            {
                qsGraphic.GetFirstGraphic(connectionHandle, module, out resultId, out resultName);
                graphicLookup[resultId] = resultName?? string.Empty;
                while (true)
                {
                    qsGraphic.GetNextGraphic(connectionHandle, module, out resultId, out resultName);
                    graphicLookup[resultId] = resultName?? string.Empty;
                }
            }
            catch (COMException e)
            {
                if (e.HResult != WinErrorCodes.E_LAST)
                    throw;
            }
            return graphicLookup;
        }
        #endregion
        #region general processing
        //TODO: keep here or move to service?
        //TODO: use reader writer lock once concurrent access is mostly solved to switch from sync execution?
        internal async Task<int> LoadAndEvaluateDataAsync(int connectionId, byte[] dataWithBOM, CancellationToken ct)
        {
            int chars = await LoadDataAsync(connectionId, dataWithBOM, ct).ConfigureAwait(false);
            ct.ThrowIfCancellationRequested();

            if (chars > 0)
            {
                try
                {
                    Debug.WriteLine($"eval: {connectionId}\t{Thread.CurrentThread.ManagedThreadId}");
                    await COMRetryHelper.DoAndRetryAsync(MAXTRIES,
                        () => _qsstat.EvaluateAllCharsTM(connectionId, THREAD_MODE_SYNC),
                        STD_DELAY, WinErrorCodes.E_PENDING, ct).ConfigureAwait(false);
                }
                finally
                {
                    Debug.WriteLine($"~eval: {connectionId}\t{Thread.CurrentThread.ManagedThreadId}");
                }
            }
            return chars;
        }

        internal async Task LoadAndEvaluateDataSingleCharacteristicAsync(int connectionId, byte[] dataWithBOM, int characteristicNumber, int strategy, CancellationToken ct)
        {
            int chars = await LoadDataAsync(connectionId, dataWithBOM, ct).ConfigureAwait(false);
            ct.ThrowIfCancellationRequested();
            this._qsstat.SetEvaluationStrategy(connectionId, 0, strategy);
            if (chars > 0)
            {
                try
                {
                    await COMRetryHelper.DoAndRetryAsync(MAXTRIES,
                        () => _qsstat.EvaluateCharTM(connectionId, 1, characteristicNumber, THREAD_MODE_SYNC),
                        STD_DELAY, WinErrorCodes.E_PENDING, ct).ConfigureAwait(false);
                }
                finally
                {
                }
            }
        }

        private async Task<int> LoadDataAsync(int connectionId, byte[] dataWithBOM, CancellationToken ct)
        {
            int chars = 0;
            SemaphoreSlim semaphore = new SemaphoreSlim(0, 1);
            var dataAvailHandler = new IQsEvents_OnDataAvailExtEventHandler(
                (int handle, int numOfChars) =>
                {
                    if (handle != connectionId)
                        return;
                    chars = numOfChars;
                    semaphore.Release();
                });
            try
            {
                _qsstat.OnDataAvailExt += dataAvailHandler;
                Debug.WriteLine($"LoadData: {connectionId}");
                await COMRetryHelper.DoAndRetryAsync(MAXTRIES,
                    () => _qsstat.TransmitFileExtTM(connectionId, dataWithBOM, RESET_PREVIOUSLY_LOADED, RESERVED, THREAD_MODE_ASYNC),
                    STD_DELAY, WinErrorCodes.E_PENDING, ct).ConfigureAwait(false);
                if (!await semaphore.WaitAsync(TIMEOUT).ConfigureAwait(false))
                {
                    //maybe better throw COMException with HRESULT E_PENDING 0x8000000A 
                    chars = 0;
                }
            }
            finally
            {
                Debug.WriteLine($"~LoadData: {connectionId}");
                _qsstat.OnDataAvailExt -= dataAvailHandler;
            }
            return chars;
        }

        protected int GetNumberOfCharacteristics(int connectionId)
        {
            _qsstat.GetGlobalInfo(connectionId, 1, 0, 2, out int chars);
            return chars;
        }

        private bool GetPartData(int connectionId, int key, out string result)
        {
            //part number is always one, only one part should be loaded
            try
            {
                _qsstat.GetPartInfo(connectionId, key, 1, 0, out result);
                return true;
            }
            catch (COMException e)
            {
                if (e.ErrorCode == WinErrorCodes.E_PENDING)
                {
                    result = String.Empty;
                    return false;
                }
                else
                {
                    result = ERR_TEXT;
                    return true;
                }
            }
        }

        /// <inheritdoc/>
        public Dictionary<int, string> GetPartData(int connectionId, IEnumerable<int> partTextKeys, CancellationToken ct)
        {
            string retrieved = null;
            Dictionary<int, string> result = new Dictionary<int, string>();
            Debug.WriteLine($"part:{connectionId}");
            foreach (var key in partTextKeys)
            {
                if (ct.IsCancellationRequested)
                    break;
                if (GetPartData(connectionId, key, out retrieved))
                {
                    result[key] = retrieved;
                }
            }
            Debug.WriteLine($"~part:{connectionId}");
            return result;
        }

        internal bool GetCharacteristicData(int connectionId, int key, int characteristicNumber, out string result)
        {
            //part number is always one, only one part loaded
            try
            {
                _qsstat.GetCharInfo(connectionId, key, 1, characteristicNumber, out result);
                return true;
            }
            catch (COMException e)
            {
                if (e.ErrorCode == WinErrorCodes.E_PENDING)
                {
                    result = String.Empty;
                    return false;
                }
                else
                {
                    result = ERR_TEXT;
                    return true;
                }
            }
        }

        /// <inheritdoc/>
        public Dictionary<int, string> GetCharacteristicData(int connectionId, IEnumerable<int> characteristicTextKeys, int characteristicNumber, CancellationToken ct)
        {
            string retrieved = null;
            Dictionary<int, string> result = new Dictionary<int, string>();
            foreach (var key in characteristicTextKeys)
            {
                if (ct.IsCancellationRequested)
                    break;
                if (GetCharacteristicData(connectionId, key, characteristicNumber, out retrieved))
                {
                    result[key] = retrieved;
                }
            }
            return result;
        }

        internal bool GetTextResult(int connectionId, int key, int characteristicNumber, out Tuple<string, double> result)
        {
            string textResult = "";
            double numberResult = 0;
            try
            {
                //part number is always one, only one part should be loaded
                _qsstat.GetStatResult(connectionId, key, 1, characteristicNumber, 0, out textResult, out numberResult);
                result = new Tuple<string, double>(textResult, numberResult);
                return true;
            }
            catch (COMException e)
            {
                numberResult = 0;
                if (e.ErrorCode == WinErrorCodes.E_PENDING)
                {
                    result = new Tuple<string, double>(String.Empty, numberResult);
                    return false;
                }
                else
                {
                    result = new Tuple<string, double>(ERR_TEXT, numberResult);
                    return true;
                }
            }
        }

        /// <inheritdoc/>
        public Dictionary<int, Tuple<string, double>> GetTextResult(int connectionId, IEnumerable<int> numericAndTextKeys, int characteristicNumber, CancellationToken ct)
        {
            Tuple<string, double> result = null;
            Dictionary<int, Tuple<string, double>> results = new Dictionary<int, Tuple<string, double>>();
            foreach (var key in numericAndTextKeys)
            {
                if (ct.IsCancellationRequested)
                    break;
                if (this.GetTextResult(connectionId, key, characteristicNumber, out result))
                {
                    results[key] = result;
                }
            }
            return results;
        }

        internal bool GetGraphResult(int connectionId, int key, int partNumber, int characteristicNumber, int width, int height, out byte[] result)
        {
            object requestResult = null;
            try
            {
                //note: old pbld actually calls GetGraphicExt
                _qsstat.GetGraphic(connectionId, key, partNumber, characteristicNumber, width, height, out requestResult);
                result = (byte[])requestResult;
                return true;
            }
            catch (COMException e)
            {
                if (e.ErrorCode == WinErrorCodes.E_FAIL || e.ErrorCode == WinErrorCodes.E_PENDING)
                {
                    result = null;
                    return false;
                }
                else
                {
                    //TODO result should be some error image
                    result = null;
                    return true;
                }
            }
        }

        /// <inheritdoc/>
        public Dictionary<QsStatImageRequest, byte[]> GetGraphResult(int connectionId, IEnumerable<QsStatImageRequest> imageKeys, int characteristic, Size panelSize, CancellationToken ct)
        {
            Dictionary<QsStatImageRequest, byte[]> results = new Dictionary<QsStatImageRequest, byte[]>();
            if (!imageKeys.Any())
                return results;
            byte[] result = null;
            foreach (var key in imageKeys)
            {
                if (ct.IsCancellationRequested)
                    break;
                int actualWidth = (int)(key.PercentageWidth * panelSize.Width);
                int actualHeight = (int)(key.PercentageHeight * panelSize.Height);
                if (actualHeight < 20 || actualWidth < 20)
                {
                    results[key] = null;
                    continue;
                }
                else
                {
                    if (GetGraphResult(connectionId, key.Key, 1, characteristic, actualWidth, actualHeight, out result))
                    {
                        results[key] = result;
                    }
                }
            }
            return results;
        }

        /// <summary>
        /// Try to get the value number for a given point in a previously generated image. May throw a COMException
        /// </summary>
        /// <param name="connectionId">id of an existing connection where the data was loaded</param>
        /// <param name="request">request id, previously retrieved from qsStat</param>
        /// <param name="characteristicNumber">characteristic number</param>
        /// <param name="width">Width of the original image that was clicked</param>
        /// <param name="height">Height of the original image that was clicked</param>
        /// <param name="atX">Click X position</param>
        /// <param name="atY">Click Y position</param>
        /// <exception cref="COMException">When parameters were invalid or something went wrong with the call to qsStat</exception>
        /// <returns></returns>
        public int GetGraphValueAtPosition(int connectionId, int request, int characteristicNumber, int width, int height, int atX, int atY)
        {
            byte onlyValue = 1;
            int jpegOutput = 1;
            this._qsstat.GetDataPositionByCoord(connectionId, request, 0, 1, characteristicNumber, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, onlyValue, ref atX, ref atY, width, height, 0, jpegOutput,
                out int partNrX, out int groupNrX, out int charNrX, out int valNrX, out int partNrY, out int groupNrY, out int charNrY, out int valueNrY, out object image);
            return valNrX;
        }

        internal bool GetReportResult(int connectionId, string reportIdentifier, int partNumber, int characteristicNumber, int pageNr, int width, int height, out byte[] result)
        {
            object requestResult = null;
            try
            {
                //note: old pbld actually uses part number 0 and characteristic number as page number
                _qsstat.GetReportExt(connectionId, reportIdentifier, partNumber, characteristicNumber, 0, 0, 0, 0, 0, 0, pageNr, width, height, 0, 0, out requestResult);
                result = (byte[])requestResult;
                return true;
            }
            catch (COMException e)
            {
                if (e.ErrorCode == WinErrorCodes.E_FAIL || e.ErrorCode == WinErrorCodes.E_PENDING)
                {
                    result = null;
                    return false;
                }
                else
                {
                    //TODO result should be some error image
                    result = null;
                    return true;
                }
            }
        }

        public bool PrintReport(int connectionId, string reportFilename, int printerId, int pageNr)
        {
            return PrintReport(connectionId, reportFilename, printerId, pageNr.ToString());
        }

        public bool PrintAllReport(int connectionId, string reportFilename, int printerId)
        {
            return PrintReport(connectionId, reportFilename, printerId, "*");
        }

        private bool PrintReport(int connectionId, string reportFilename, int printerId, string pageSelection)
        {
            try
            {
                int reportNr = 0;
                //direct printing
                int printMode = 33;
                //color from qs print settings
                int colorMode = 0;
                //orientation from report default
                int orientation = 0;
                this._qsstat.PrintReportExt(connectionId, reportNr, reportFilename, printerId, pageSelection, printMode, colorMode, orientation);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get the number of pages for the given report
        /// </summary>
        /// <param name="connectionId">id of an existing connection where the data was loaded</param>
        /// <param name="reportFileName">Filename of the .def file</param>
        /// <returns>Number of pages on success, otherwise null</returns>
        public int? GetReportNumberPages(int connectionId, string reportFileName)
        {
            try
            {
                this._qsstat.GetReportPages(connectionId, reportFileName, out int numberPages);
                return numberPages;
            }
            catch
            {
                return null;
            }
        }
        #endregion
        #region IDisposable
        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool calledFromDisposeNotFinalize)
        {
            if (this._disposed)
                return;

            if (calledFromDisposeNotFinalize)
            {
                if (this._qsstat != null)
                {
                    this._qsstat.OnGetAliveState -= this.KeepAliveHandler;
                    Marshal.ReleaseComObject(this._qsstat);
                    this._qsstat = null;
                }
            }         
            this._disposed = true;
        }

        ~QsStatWrapper()
        {
            Dispose(false);
        }
        #endregion
    }
}
