﻿namespace Promess.QsStat.Data
{
    /// <summary>
    /// Connection parameters for QCOM
    /// </summary>
    internal class QsConnectionParameters
    {
        //TODO password as secure string?
        public QsConnectionParameters(int module, int language, string user, string pwd)
        {
            this.Module = module;
            this.Language = language;
            this.User = user;
            this.Password = pwd;
        }

        public int Language { get; private set; }
        public int Module { get; private set; }
        internal string Password { get; private set; }
        public string User { get; private set; }
    }
}
