﻿using System;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// QCOM language entry
    /// </summary>
    public class QsLanguageEntry
    {
        public QsLanguageEntry(int id, string shortName, string name)
        {
            Id = id;
            ShortName = shortName;
            Name = name;
        }

        public int Id { get; private set; }
        public String ShortName { get; private set; }
        public String Name { get; private set; }

    }
}
