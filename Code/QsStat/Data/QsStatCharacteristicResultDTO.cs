﻿using Promess.Pbld.Data.Measurements;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// Data from a single characteristic
    /// </summary>
    public class QsStatCharacteristicResultDTO
    {
        public QsStatCharacteristicResultDTO(CharacteristicMetaData characteristicMetaData):this(characteristicMetaData, new Dictionary<int, string>(), new Dictionary<int, Tuple<string, double>>(), new Dictionary<QsStatImageRequest, byte[]>(), true)
        { }

        public QsStatCharacteristicResultDTO(CharacteristicMetaData characteristicMetaData, IReadOnlyDictionary<int, string> characteristicTextResults, IReadOnlyDictionary<int, Tuple<string, double>> numericAndTextResults, IReadOnlyDictionary<QsStatImageRequest, byte[]> imageResults, bool incomplete, IReadOnlyDictionary<int, string> measuringResults = null)
        {
            Debug.Assert(characteristicMetaData != null && characteristicTextResults != null && characteristicTextResults != null && numericAndTextResults != null);
            this.CharacteristicMetaData = characteristicMetaData;
            this.CharacteristicTextResults = characteristicTextResults;
            this.NumericAndTextResults = numericAndTextResults;
            this.ImageResults = imageResults;
            this.Incomplete = incomplete;
            this.MeasuringResults = measuringResults ?? new Dictionary<int, string>();
        }

        public CharacteristicMetaData CharacteristicMetaData { get; private set; }
        public IReadOnlyDictionary<int, string> CharacteristicTextResults { get; private set; }
        public IReadOnlyDictionary<int, Tuple<string, double>> NumericAndTextResults { get; private set; }
        public IReadOnlyDictionary<QsStatImageRequest, byte[]> ImageResults { get; private set; }
        public IReadOnlyDictionary<int, string> MeasuringResults { get; private set; }
        public bool Incomplete { get; private set; }
    }
}
