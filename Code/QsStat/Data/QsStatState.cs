﻿namespace Promess.QsStat.Data
{
    public enum QsStatState
    {
        Starting,
        RetrievingConnectionParameters,
        Connecting,
        RetrievingMetadata,
        Started,
        MissingSettings,
        ShuttingDown,
        Shutdown,
        Faulted
    }
}
