﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// Abstraction for data retrieval
    /// </summary>
    internal interface IQsStatDataRetrieval
    {
        /// <summary>
        /// Get part data
        /// </summary>
        /// <param name="connectionId">QCOM connection id</param>
        /// <param name="partTextKeys">QCOM part keys</param>
        /// <param name="ct">Cancellation</param>
        /// <returns>Lookup containing the keys with the results</returns>
        Dictionary<int, string> GetPartData(int connectionId, IEnumerable<int> partTextKeys, CancellationToken ct);
        /// <summary>
        /// Get characteristic data
        /// </summary>
        /// <param name="connectionId">QCOM connection id</param>
        /// <param name="characteristicTextKeys">QCOM characteristic keys</param>
        /// <param name="characteristicNumber">Target characteristic number</param>
        /// <param name="ct">Cancellation</param>
        /// <returns>Lookup containing the keys with the results</returns>
        Dictionary<int, string> GetCharacteristicData(int connectionId, IEnumerable<int> characteristicTextKeys, int characteristicNumber, CancellationToken ct);
        /// <summary>
        /// Get text results 
        /// </summary>
        /// <param name="connectionId">QCOM connection id</param>
        /// <param name="numericAndTextKeys">QCOM numeric and text keys</param>
        /// <param name="characteristicNumber">Target characteristic number</param>
        /// <param name="ct">Cancellation</param>
        /// <returns>Lookup containing the keys with the results as text and number</returns>
        Dictionary<int, Tuple<string, double>> GetTextResult(int connectionId, IEnumerable<int> numericAndTextKeys, int characteristicNumber, CancellationToken ct);
        /// <summary>
        /// Get graphic results
        /// </summary>
        /// <param name="connectionId">COM connection id</param>
        /// <param name="imageKeys">Image requests</param>
        /// <param name="characteristicNumber">Target characteristic number</param>
        /// <param name="panelSize">Size of the parent panel for the image requests</param>
        /// <param name="ct">Cancellation</param>
        /// <returns>Lookup with the keys and the results as byte arrays</returns>
        Dictionary<QsStatImageRequest, byte[]> GetGraphResult(int connectionId, IEnumerable<QsStatImageRequest> imageKeys, int characteristicNumber, Size panelSize, CancellationToken ct);
    }
}
