﻿namespace Promess.QsStat.Data
{
    public enum ManualRequestTypes
    {
        Refresh,
        NextPart,
        PreviousPart,
        NextCharacteristic,
        PreviousCharacteristic
    }
}
