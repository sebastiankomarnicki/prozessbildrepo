﻿using System.Collections.Generic;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// Data retrieval request for a part and specific characteristics.
    /// </summary>
    internal class QsStatRetrievalItems
    {
        public QsStatRetrievalItems()
        {
            PartTextKeys = new HashSet<int>();
            CharacteristicTextKeys = new HashSet<int>();
            NumericAndTextKeys = new HashSet<int>();
            ImageKeys = new HashSet<QsStatImageRequest>();
            AdditionalPartKeys = new HashSet<int>();
            AdditionalCharacteristicKeys = new HashSet<int>();
        }

        public ISet<int> PartTextKeys { get; private set; }
        public ISet<int> CharacteristicTextKeys { get; private set; }
        public ISet<int> NumericAndTextKeys { get; private set; }
        public ISet<QsStatImageRequest> ImageKeys { get; private set; }
        public ISet<int> AdditionalPartKeys { get; private set; }
        public ISet<int> AdditionalCharacteristicKeys { get; private set; }
    }
}
