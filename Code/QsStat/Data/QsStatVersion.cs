﻿using System;
using System.Text;

namespace Promess.QsStat.Data
{
    public class QsStatVersion
    {
        private static char[] VERSION_SPLIT = new char[] { '/' };
        private readonly string _versionString;
        private readonly int _version;
        private readonly int _revision;
        private readonly Encoding _fileEncoding;

        protected QsStatVersion(string versionString, int version, int revision, Encoding fileEncoding)
        {
            this._versionString = versionString;
            this._version = version;
            this._revision = revision;
            this._fileEncoding = fileEncoding;
        }

        /// <summary>
        /// Create version information from string. Assumed format is "VersionNumber / Revision". Version less than 11 only supports ASCII file encoding.
        /// </summary>
        /// <param name="versionString">Version text from QCOM</param>
        /// <returns>Processed string into <see cref="QsStatVersion"/></returns>
        public static QsStatVersion Create(string versionString)
        {
            versionString = versionString ?? string.Empty;
            string[] split = versionString.Split(QsStatVersion.VERSION_SPLIT, StringSplitOptions.RemoveEmptyEntries);
            int version = -1;
            int revision = -1;
            if (split.Length>=1)
            {
                int.TryParse(split[0], out version);
            }
            if (split.Length >= 2)
            {
                int.TryParse(split[1], out revision);
            }
            Encoding encoding;
            if (version > 10)
            {
                encoding = Encoding.UTF8;
            }
            else
            {
                encoding = Encoding.ASCII;
            }
            return new QsStatVersion(versionString, version, revision, encoding);
        }

        public int Version { get { return _version; } }
        public int Revision { get { return _revision; } }
        public string VersionString { get { return _versionString; } }
    }
}
