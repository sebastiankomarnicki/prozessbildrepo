﻿using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// Container for the evaluation of a <see cref="SpcPath"/>
    /// </summary>
    public class SpcPathEvaluationDTO
    {
        /// <summary>
        /// Generate an empty evaluation result
        /// </summary>
        public SpcPathEvaluationDTO():this(new List<QsStatPartResultDTO>(), Guid.Empty, -1) { }

        /// <summary>
        /// Generate an evaluation result from evaluated data
        /// </summary>
        /// <param name="evaluatedData">The evaluated data</param>
        /// <param name="selectedPartGuid">Id of current data set</param>
        /// <param name="selectedCharacteristicIndex">Index of current characteristic</param>
        public SpcPathEvaluationDTO(IReadOnlyCollection<QsStatPartResultDTO> evaluatedData, Guid selectedPartGuid, int selectedCharacteristicIndex)
        {
            this.EvaluatedData = evaluatedData.ToDictionary(item => item.DatasetReferenceGuid, item => item);
            this.ProcessState = MeasurementHelper.EvaluateState(this.EvaluatedData.Select(ed => ed.Value.ProcessState));
            this.SelectedPartGuid = selectedPartGuid;
            this.SelectedCharacteristicIndex = selectedCharacteristicIndex;
        }

        public Guid SelectedPartGuid { get; private set; }
        public int SelectedCharacteristicIndex { get; private set; }

        public ProcessState ProcessState { get; private set; }
        public IReadOnlyDictionary<Guid, QsStatPartResultDTO> EvaluatedData { get; private set; }
        /// <summary>
        /// Exception for the evaluation, otherwise null
        /// </summary>
        public Exception Exception { get; set; }
    }
}
