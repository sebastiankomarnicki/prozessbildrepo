﻿namespace Promess.QsStat.Data
{
    /// <summary>
    /// Request for an image from QCOM
    /// </summary>
    public sealed class QsStatImageRequest
    {
        private readonly int _key;
        private readonly double _percentageWidth;
        private readonly double _percentageHeight;

        /// <summary>
        /// Create image request
        /// </summary>
        /// <param name="key">QCOM key for the image</param>
        /// <param name="percentageWidth">Width of the image as percentage of the containers width</param>
        /// <param name="percentageHeight">Height of the image as percentage of the containers height</param>
        public QsStatImageRequest(int key, double percentageWidth, double percentageHeight)
        {
            this._key = key;
            this._percentageWidth = percentageWidth;
            this._percentageHeight = percentageHeight;
        }

        public int Key { get { return _key; } }
        public double PercentageWidth { get { return _percentageWidth; } }
        public double PercentageHeight { get { return _percentageHeight; } }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((QsStatImageRequest)obj);
        }

        private bool Equals(QsStatImageRequest other)
        {
            return _key == other.Key && _percentageWidth == other.PercentageWidth && _percentageHeight == other._percentageHeight;
        }

        public override int GetHashCode()
        {
            return (_key, _percentageWidth, _percentageHeight).GetHashCode();
        }
    }
}
