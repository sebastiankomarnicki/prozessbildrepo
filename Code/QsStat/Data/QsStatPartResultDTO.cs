﻿using Promess.Pbld.Data.Measurements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// Result of retrieved part data
    /// </summary>
    public class QsStatPartResultDTO
    {
        /// <summary>
        /// Create empty data set
        /// </summary>
        /// <param name="datasetReferenceGuid"></param>
        public QsStatPartResultDTO(Guid datasetReferenceGuid) : this(new Dictionary<int, string>(), new Dictionary<int, QsStatCharacteristicResultDTO>(), datasetReferenceGuid, true, DateTime.MinValue)
        { }

        public QsStatPartResultDTO(IReadOnlyDictionary<int, string> partTextResults, IReadOnlyDictionary<int, QsStatCharacteristicResultDTO> characteristicResults, Guid datasetReferenceGuid, bool incomplete, DateTime lastDsChangeTime, IReadOnlyDictionary<int, string> measuringResults = null)
        { 
            this.PartTextResults = partTextResults;
            this.CharacteristicResults = characteristicResults;
            this.ProcessState = MeasurementHelper.EvaluateState(this.CharacteristicResults.Values
                .Where(cr=>cr.CharacteristicMetaData.Enabled)
                .Select(cr=> cr.CharacteristicMetaData));
            this.DatasetReferenceGuid = datasetReferenceGuid;
            this.LastDsChangeTime = lastDsChangeTime;
            this.MeasuringResults = measuringResults??new Dictionary<int, string>();
            this.Incomplete = incomplete || CharacteristicResults.Values.Any(cr => cr.Incomplete);
        }

        public Guid DatasetReferenceGuid { get; private set; }

        public ProcessState ProcessState { get; private set; }
        public IReadOnlyDictionary<int, string> PartTextResults { get; private set; }
        public IReadOnlyDictionary<int, QsStatCharacteristicResultDTO> CharacteristicResults { get; private set; }
        public IReadOnlyDictionary<int, string> MeasuringResults { get; private set; }

        public DateTime LastDsChangeTime { get; private set; }

        public bool Incomplete { get; private set; }
    }
}
