﻿namespace Promess.QsStat.Data
{
    /// <summary>
    /// QCOM report entry
    /// </summary>
    public class QsReportEntry
    {
        public QsReportEntry(int id, string name, string fileName)
        {
            this.Id = id;
            this.Name = name?? string.Empty;
            this.FileName = fileName?? string.Empty;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public string FileName { get; private set; }
    }
}
