﻿using System.Collections.Generic;

namespace Promess.QsStat.Data
{
    /// <summary>
    /// Data retrieval request for a part and specific characteristics. Requests for a characteristic are accessed using their number.
    /// </summary>
    public class QsStatRetrievalItemsMultipleCharacteristics
    {
        public QsStatRetrievalItemsMultipleCharacteristics()
        {
            PartTextKeys = new HashSet<int>();
            CharacteristicTextKeys = new Dictionary<int,IReadOnlyCollection<int>>();
            NumericAndTextKeys = new Dictionary<int, IReadOnlyCollection<int>>();
            ImageKeys = new Dictionary<int, IReadOnlyCollection<QsStatImageRequest>>();
            AdditionalPartKeys = new HashSet<int>();
            AdditionalCharacteristicKeys = new Dictionary<int, IReadOnlyCollection<int>>();
        }

        public IReadOnlyCollection<int> PartTextKeys { get; set; }
        public IReadOnlyDictionary<int, IReadOnlyCollection<int>> CharacteristicTextKeys { get; set; }
        public IReadOnlyDictionary<int, IReadOnlyCollection<int>> NumericAndTextKeys { get; set; }
        public IReadOnlyDictionary<int, IReadOnlyCollection<QsStatImageRequest>> ImageKeys { get; set; }
        public IReadOnlyCollection<int> AdditionalPartKeys { get; set; }
        public IReadOnlyDictionary<int, IReadOnlyCollection<int>> AdditionalCharacteristicKeys { get; set; }

    }
}
