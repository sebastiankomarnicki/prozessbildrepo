﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Promess.QsStat.Utility
{
    internal static class COMRetryHelper
    {
        /// <summary>
        /// Retry an action several times until success, cancellation, try count exceeded or unexpected exception
        /// </summary>
        /// <param name="maxTries">Maximum number of tries to execute the action</param>
        /// <param name="action">Action to execute</param>
        /// <param name="delay">Delay between retries</param>
        /// <param name="allowedComErrorCode">To catch <see cref="COMException"/> error code for a retry </param>
        /// <param name="ct">Cancellation</param>
        /// <returns>Task for the execution</returns>
        public static async Task DoAndRetryAsync(int maxTries, Action action, TimeSpan delay, int allowedComErrorCode, CancellationToken ct)
        {
            Debug.Assert(maxTries > 0);
            for (int i=1;i<=maxTries;i++)
            {
                try
                {
                    action();
                    return;
                }
                catch (COMException e)
                {
                    if (e.ErrorCode != allowedComErrorCode || i==maxTries)
                        throw;
                    await Task.Delay(delay, ct).ConfigureAwait(false);
                }
            }
        }

        public static async Task DoAndRetry(int maxTries, Action action, TimeSpan delay, int allowedComErrorCode)
        {
            Debug.Assert(maxTries > 0);
            for (int i = 1; i <= maxTries; i++)
            {
                try
                {
                    action();
                    return;
                }
                catch (COMException e)
                {
                    if (e.ErrorCode != allowedComErrorCode || i == maxTries)
                        throw;
                    await Task.Delay(delay).ConfigureAwait(false);
                }
            }
        }

        public static async Task<T> DoAndRetry<T>(int maxTries, Func<T> func, TimeSpan delay, int allowedComErrorCode)
        {
            Debug.Assert(maxTries > 0);
            for (int i = 1; i <= maxTries; i++)
            {
                try
                {
                    return func();
                }
                catch (COMException e)
                {
                    if (e.ErrorCode != allowedComErrorCode || i == maxTries)
                        throw;
                    await Task.Delay(delay).ConfigureAwait(false);
                }
            }
            //cannot be reached, to satisfy parser
            return default(T);
        }
    }
}
