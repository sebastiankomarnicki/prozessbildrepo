﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Promess.QsStat.Utility
{
    /// <summary>
    /// Implementation of a resource pool. Users are required to properly return acquired resources (dispose).
    /// </summary>
    /// <typeparam name="T">Type of the wrapped resource</typeparam>
    public sealed class ResourcePool<T> : IDisposable
    {
        private BlockingCollection<PoolItem<T>> _resourcesInPool;
        private ConcurrentBag<T> _resourcesOwned;
        private Action<T> _onDisposeResource;
        private SemaphoreSlim _semaphore;
        private CancellationTokenSource _cts;

        private ResourcePool(int capacity, Action<T> onDisposeResource)
        {
            this._resourcesInPool = new BlockingCollection<PoolItem<T>>();
            this._resourcesOwned = new ConcurrentBag<T>();
            this._semaphore = new SemaphoreSlim(0, capacity);
            this._onDisposeResource = onDisposeResource;
            this._cts = new CancellationTokenSource();
        }

        private void Initialize(int capacity, Func<T> resourceFactory, IReadOnlyCollection<T> inheritedResources)
        {
            T resource;
            foreach (T item in inheritedResources)
            {
                this._resourcesInPool.Add(new PoolItem<T>(this, item));
                this._resourcesOwned.Add(item);
            }
            for (int i = 0; i < capacity - inheritedResources.Count; i++)
            {
                resource = resourceFactory();
                this._resourcesInPool.Add(new PoolItem<T>(this, resource));
                this._resourcesOwned.Add(resource);
            }
            this._semaphore.Release(capacity);
        }

        private void Initialize(IEnumerable<T> items)
        {
            if (!items.Any())
                throw new ArgumentException($"{nameof(items)} must at least contain one item");
            foreach (T item in items)
            {
                this._resourcesInPool.Add(new PoolItem<T>(this, item));
                this._resourcesOwned.Add(item);
            }
            this._semaphore.Release(this._resourcesOwned.Count);
        }

        private async Task InitializeAsync(int capacity, Func<Task<T>> resourceFactory, IReadOnlyCollection<T> inheritedResources)
        {
            int toCreate = capacity - inheritedResources.Count;
            Task<T>[] factoryTasks = new Task<T>[toCreate];            
            for (int i=0; i<toCreate; i++)
            {
                factoryTasks[i] = resourceFactory();
            }
            await Task.WhenAll(factoryTasks).ConfigureAwait(false);
            foreach (var resource in factoryTasks.Select(task=>task.Result))
            {
                this._resourcesInPool.Add(new PoolItem<T>(this, resource));
                this._resourcesOwned.Add(resource);
            }
            foreach (var resource in inheritedResources)
            {
                this._resourcesInPool.Add(new PoolItem<T>(this, resource));
                this._resourcesOwned.Add(resource);
            }
            this._semaphore.Release(capacity);
        }

        /// <summary>
        /// Factory for the pool, provides all pooled resources.
        /// </summary>
        /// <param name="inheritedResources">All pool resources</param>
        /// <param name="onDisposeResource">Action for all resources when pool is disposed.</param>
        /// <returns></returns>
        public static ResourcePool<T> Create(ICollection<T> inheritedResources, Action<T> onDisposeResource = null)
        {
            Debug.Assert(inheritedResources.Any());
            var ret = new ResourcePool<T>(inheritedResources.Count, onDisposeResource);
            ret.Initialize(inheritedResources);
            return ret;
        }

        /// <summary>
        /// Factory for the pool, some pool resources inherited.
        /// </summary>
        /// <param name="capacity">Total pool capacity. Must be larger or equal to the number of inherited resources.</param>
        /// <param name="inheritedResources">Inherited pool resources.</param>
        /// <param name="resourceFactory">Factory to create a pool resource.</param>
        /// <param name="onDisposeResource">Action for all resources when pool is disposed.</param>
        /// <returns></returns>
        public static async Task<ResourcePool<T>> CreateAsync(int capacity, IReadOnlyCollection<T> inheritedResources, Func<Task<T>> resourceFactory, Action<T> onDisposeResource = null)
        {
            Debug.Assert(capacity > 0);
            Debug.Assert(capacity >= inheritedResources.Count);
            Debug.Assert(inheritedResources is object);
            var ret = new ResourcePool<T>(capacity, onDisposeResource);
            await ret.InitializeAsync(capacity, resourceFactory, inheritedResources).ConfigureAwait(false);
            return ret;
        }

        /// <summary>
        /// Async factory for the pool, some pool items inherited.
        /// </summary>
        /// <param name="capacity">Total pool capacity. Must be larger or equal to the number of inherited resources.</param>
        /// <param name="inherited">Inherited pool resources.</param>
        /// <param name="resourceFactory">Factory to create a pool resource.</param>
        /// <param name="onDisposeResource">Action for all resources when pool is disposed.</param>
        /// <returns></returns>
        public static async Task<ResourcePool<T>> CreateAsync(int capacity, Func<Task<T>> resourceFactory, Action<T> onDisposeResource = null)
        {
            Debug.Assert(capacity > 0);
            var ret = new ResourcePool<T>(capacity, onDisposeResource);
            await ret.InitializeAsync(capacity, resourceFactory, new T[0]).ConfigureAwait(false);
            return ret;
        }

        /// <summary>
        /// Retrieve a resource from the pool. Must be properly disposed after use.
        /// </summary>
        /// <param name="ct">Cancellation for the retrieval</param>
        /// <returns>Retrieval task for a wrapped pool resource</returns>
        public async Task<PoolItem<T>> GetResourceAsync(CancellationToken ct)
        {
            using (var linkedCts = CancellationTokenSource.CreateLinkedTokenSource(this._cts.Token, ct))
                await _semaphore.WaitAsync(ct).ConfigureAwait(false);
            return _resourcesInPool.Take();
        }

        internal void SendBackToPool(T resource)
        {
            if (!_resourcesOwned.Contains(resource))
                throw new ArgumentOutOfRangeException("Released item must belong to the pool");
            this._resourcesInPool.Add(new PoolItem<T>(this,resource));
            this._semaphore.Release();
        }

        #region IDisposable
        private bool _disposed = false;

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (_resourcesInPool != null)
                {
                    if (_onDisposeResource != null)
                    {
                        this._cts.Cancel();
                        this._cts.Dispose();
                        while (this._resourcesInPool.Count < this._resourcesOwned.Count)
                        {
                            //FIXME is there a better way here? can I do something so this never happens? maybe call take directly? test!
                            Debug.WriteLine("Hanging in ResourcePool.Dispose..");
                            Thread.Sleep(100);
                        }
                        _resourcesInPool.CompleteAdding();
                        foreach (var resource in _resourcesOwned)
                        {
                            try
                            {
                                _onDisposeResource(resource);
                            }
                            catch { }                         
                        }
                    }
                    _resourcesInPool.Dispose();
                    _resourcesInPool = null;
                    _semaphore.Dispose();
                }
            }
            _disposed = true;
        }
        //FIXME implement IDisposable(Async?)
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ResourcePool()
        {
            Dispose(false);
        }
        #endregion

    }
}
