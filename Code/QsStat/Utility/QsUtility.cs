﻿using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using System;

namespace Promess.QsStat.Utility
{
    internal class QsUtility
    {
        internal static bool CheckIfCharacteristicClassEnabled(GeneralSettings options, int characteristicClassNumber)
        {
            if (!Enum.IsDefined(typeof(CharacteristicClass), characteristicClassNumber))
            {
                return false;
            }
            switch ((CharacteristicClass)characteristicClassNumber)
            {
                case CharacteristicClass.Unimportant:
                    return options.EvaluateUnimportant;
                case CharacteristicClass.LittleImportant:
                    return options.EvaluateLittleImportant;
                case CharacteristicClass.Important:
                    return options.EvaluateImportant;
                case CharacteristicClass.Significant:
                    return options.EvaluateSignificant;
                case CharacteristicClass.Critical:
                    return options.EvaluateCritical;
                default:
                    return false;
            }
        }
    }
}
