﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Promess.QsStat.Utility
{
    //TODO can use channels once upgraded to core/.net 5+
    /// <summary>
    /// Service for enqueueing operations on a wrapped resource
    /// </summary>
    /// <typeparam name="T">The resource</typeparam>
    public class ResourceService<T> where T : class
    {
        private readonly CancellationTokenSource _cts;
        private readonly BlockingCollection<Func<T, Task>> _priorityRequest;
        private readonly BlockingCollection<Func<T, Task>> _requests;
        private readonly ResourcePool<T> _resources;
        private readonly AutoResetEvent _autoResetEvent;
        private Task[] _processingTasks;

        public ResourceService(ResourcePool<T> resources)
        {
            this._cts = new CancellationTokenSource();
            this._requests = new BlockingCollection<Func<T, Task>>();
            this._priorityRequest = new BlockingCollection<Func<T, Task>>();
            this._resources = resources;
            this._autoResetEvent = new AutoResetEvent(false);
        }

        /// <summary>
        /// Start the service
        /// </summary>
        /// <param name="numberOfThreads">Number of threads to use for processing requests</param>
        public void Start(int numberOfThreads)
        {
            if (_processingTasks != null)
                throw new InvalidOperationException("cannot start multiple times");

            _processingTasks = new Task[numberOfThreads];
            for (int i = 0; i < numberOfThreads; i++)
            {
                _processingTasks[i] = Task.Factory.StartNew(async () =>
                {
                    var token = _cts.Token;
                    Func<T, Task> request = null;
                    while (!token.IsCancellationRequested)
                    {
                        try
                        {
                            if (_priorityRequest.TryTake(out request) || _requests.TryTake(out request))
                            {
                                long now = DateTime.UtcNow.Ticks;
                                Debug.WriteLine($"RS: {now}");
                                using (var resource = await _resources.GetResourceAsync(token).ConfigureAwait(false))
                                {
                                    await request(resource.Resource).ConfigureAwait(false);
                                }
                                Debug.WriteLine($"~RS: {now}");
                            }
                            else
                            {
                                WaitHandle.WaitAny(new[] { token.WaitHandle, _autoResetEvent });
                            }
                        }
                        catch (InvalidCastException)
                        {
                            Debug.WriteLine($"{nameof(ResourceService<T>)} on thread {Thread.CurrentThread.ManagedThreadId} has been cleared.");
                            break;
                        }
                        catch (OperationCanceledException)
                        {
                            Debug.WriteLine($"{nameof(ResourceService<T>)} on thread {Thread.CurrentThread.ManagedThreadId} has been canceled.");
                            break;
                        }
                    }
                }, _cts.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
            }            
        }

        /// <summary>
        /// Ends the service, aborting running and outstanding operations. Service cannot be restarted.
        /// </summary>
        /// <returns>Task for the operation. Completed once all running tasks are finished/aborted.</returns>
        public async Task CancelAsync()
        {
            _cts.Cancel();
            _priorityRequest.CompleteAdding();
            _requests.CompleteAdding();
            _autoResetEvent.Close();
            if (_processingTasks!=null)
                await Task.WhenAll(_processingTasks).ConfigureAwait(false);
        }

        /// <summary>
        /// Enqueue a possible async function which requires the wrapped resource as input.
        /// </summary>
        /// <typeparam name="R">Result type</typeparam>
        /// <param name="work">Operation to execute</param>
        /// <param name="priority">Execution priority for scheduling</param>
        /// <returns>Result task</returns>
        public Task<R> Enqueue<R>(Func<T, Task<R>> work, PriorityType priority = PriorityType.Normal)
        {
            var tcs = new TaskCompletionSource<R>();
            
            Func<T, Task> toEnqueue = (async (t) =>
            {     
                Task<R> completedTask;
                try
                {
                    var workTask = work(t);
                    using (var registration = _cts.Token.Register(() => tcs.TrySetCanceled()))
                    {
                        completedTask = await Task.WhenAny(workTask, tcs.Task).ConfigureAwait(false);
                    }
                    if (completedTask.Status == TaskStatus.RanToCompletion)
                    {
                        tcs.TrySetResult(completedTask.Result);
                    }
                    else
                    {
                        tcs.TrySetCanceled();
                    }
                }
                catch(Exception e)
                {
                    tcs.TrySetException(e);
                }        
            });
            switch (priority)
            {
                case PriorityType.Normal:
                    _requests.Add(toEnqueue);
                    break;
                case PriorityType.High:
                    _priorityRequest.Add(toEnqueue);
                    break;
                default:
                    throw new NotImplementedException($"No code defined for {nameof(PriorityType)} value: {priority}");
            }
            _autoResetEvent.Set();
            return tcs.Task;
        }

        /// <summary>
        /// Enqueue a function which requires the wrapped resource as input.
        /// </summary>
        /// <typeparam name="R">Result type</typeparam>
        /// <param name="work">Operation to execute</param>
        /// <param name="priority">Execution priority for scheduling</param>
        /// <returns>Result task</returns>
        public Task<R> Enqueue<R>(Func<T, R> work, PriorityType priority = PriorityType.Normal)
        {
            var tcs = new TaskCompletionSource<R>();

            Func<T, Task> toEnqueue = ((t) =>
            {
                R result;
                try
                {
                    result = work(t);
                    if (_cts.IsCancellationRequested)
                    {
                        tcs.TrySetCanceled();
                    }
                    else
                    {
                        tcs.SetResult(result);
                    }
                }
                catch(Exception e)
                {
                    tcs.TrySetException(e);
                }
                //this is only the internal task to determin if the operation is finished. must not reflect error states of wrapped operation, that is the responsibility for the TaskCompletionSource
                return Task.CompletedTask;
            });
            switch (priority)
            {
                case PriorityType.Normal:
                    _requests.Add(toEnqueue);
                    break;
                case PriorityType.High:
                    _priorityRequest.Add(toEnqueue);
                    break;
                default:
                    throw new NotImplementedException($"No code defined for {nameof(PriorityType)} value: {priority}");
            }
            _autoResetEvent.Set();
            return tcs.Task;
        }
    }
}
