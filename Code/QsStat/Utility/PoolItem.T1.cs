﻿using System;

namespace Promess.QsStat.Utility
{
    /// <summary>
    /// Wrapper for an item which is part of a <see cref="ResourcePool{T1}"/> to properly return to pool after use.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PoolItem<T> : IDisposable
    {
        private T _resource;
        private ResourcePool<T> _pool;

        internal PoolItem(ResourcePool<T> pool, T resource)
        {
            this._pool = pool;
            this._resource = resource;
        }

        /// <summary>
        /// Access the resource of this PoolItem. Should not be cached.
        /// <exception cref="ObjectDisposedException">
        /// If the pool this item came from was shut down.
        /// </exception>
        /// </summary>
        public T Resource {
            get {
                if (_disposed)
                    throw new ObjectDisposedException(this.ToString());
                return _resource; } }

        #region IDisposable
        private bool _disposed = false;

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                _pool.SendBackToPool(_resource);
            }

            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~PoolItem()
        {
            Dispose(false);
        }
        #endregion

    }
}
