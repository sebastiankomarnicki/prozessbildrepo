# Prozessbild / process picture architecture
The contained articles provide an overview of the architecture of the Prozessbild / process picture (Pbld). Following an overview of the architecture of the program.
![Overview](images/Components.png)

## General information
The Pbld GUI uses Windows Presentation Foundation (WPF) for visualization and Model-View-ViewModel (MVVM) as architecturial pattern. In addition there are some controllers for general setup and navigation, if not handled by the respective view models.
The program relies on the lightweight [Win Application Framework (WAF)](https://github.com/jbe2277/waf) for MVVM base classes and file system abstractions. Services, resources, and views are injected using the Managed Extensibility Framework (MEF). Entry point for the application is the partial class <xref:Pbld.App>. The `.xaml` part merges the resource dictionaries and the `.cs` part starts the application. The statistical evaluation of measurement data by this application relies on `QCOM/qsStat` by [Hexagon/Q-DAS](https://www.q-das.com/). Currently the target platform for all projects is `.NET Framework 4.8`.

> [!NOTE]
> When moving to .NET 5+ the shutdown logic for some classes can be adjusted by replacing <xref:System.IDisposable> with the new <xref:System.IAsyncDisposable>

## Library project Language
This is a general project for providing translations using text keys. The singleton <xref:Promess.Language.LanguageManager> manages the loaded languages and exposes the current language.
The XML Schema `LanguageSettings.xsd` specifies how all supported languages, their translations and defaults are to be saved. `Language.xsd` defines the format for the translations for one language. 

## Library project Common
This projects contains generally useful code in the context of working with WAF, WPF and processing data in the Advanced Quality Data Exchange Format (AQDEF). 
The subfolders/-namespaces Behaviros, Controls, Converters, Extensions and Resources contain code/definitions for a GUI. Validation contains some attributes useful for validation.

### <xref:Promess.Common.QDas>
This namespace contains the code for reading AQDEF files (.dfq/.dfd+.dfx) based on the VB6 code of the `Datenmanager` and extended for attribute access and mapping using k numbers and reflection.
The entry point for reading files is <xref:Promess.Common.QDas.QdasLesen>. For accessing and mapping k numbers see one of the classes implementing <xref:Promess.Common.QDas.ISupportKNumberDataManipulation> and <xref:Promess.Common.QDas.QdasValueConversions>.

> [!NOTE]
> With C#10 the interface methods for accessing the code can be defined as static, reducing code duplication.

### <xref:Promess.Common.Settings>
Read `.config` files from a custom path, extensions for configuration elements to support errors, wrapper for accepting/discarding changes for parts of a configuration. 

### <xref:Promess.Common.Settings>
Helper code mostly for issuing/handling async operations on/for the UI layer, retrying operations, serialization.

## Library project PbldData
Contains data classes for the settings, editors, `qsStat` (not AQDEF) independent logic for retrieving and manipulating measurement data, and attributes for validation.
Entry points for data processing are <xref:Promess.Pbld.Data.Measurements.FolderDatasets> and <xref:Promess.Pbld.Data.Measurements.QueryableQsData>. The former abstracts all data sets of an SPC parameter. The latter is a snapshot in time of a parsed data set. It can be queried for AQDEF K fields and custom data, as well as provide the raw data for an evaluation using `qsStat`. Query results are aggregated into <xref:Promess.Pbld.Data.Measurements.QueriedData> independent of their source.

## Authentication
Authentication is split into two projects, one containing the logic and defining interfaces and one for presentation. More presentation projects would need to be added to provide different look and feel (more than what is possible with themes). 

### Library project Authentication
Library to provide an authentication mechanism for a program by setting the <xref:System.Threading.Thread.CurrentPrincipal>. Views are decorated with <xref:System.Security.Permissions.PrincipalPermissionAttribute> to check for authentication. Currently only authenticated or not is supported. Information about the authentication state and commands for this libary are set in <xref:Promess.Pbld.Services.IMainService>. This library is an example for extending Pbld with modules by dynamically loading dlls (except the view annotation).

> [!NOTE]
> For a more general use of the library dependencies on <xref:Promess.Pbld.ViewModels.MainViewModel> and <xref:Promess.Pbld.Services.PathsAndResourceService> need to be abstracted.

> [!WARNING]
> Since .NET 5 <xref:System.Security.Permissions.PrincipalPermissionAttribute> is obsolete

### Library project Authentication.Presentation
Contains the implementation of the views defined by the `Authentication` project.

## Library project QsStat
Entry point for this project is <xref:Promess.QsStat.Services.QsStatService>. This implementation and its interface provide Pbld with information about the processed data, available options for `qsStat`, as well as data navigation. The data processing is executed asynchronously and not on the GUI thread. <xref:System.ComponentModel.INotifyPropertyChanged> is used to notify subscribers.
The accessed `qsStat` functionality is wrapped by <xref:Promess.QsStat.Data.QsStatWrapper>. <xref:Promess.QsStat.Services.QsPeriodicalCalculation> is responsible for the periodic evaluation of all data files of an area. Current evaluation results are exposed by the properties <xref:Promess.QsStat.Services.QsStatService.SpcPathResults> and found data sets by <xref:Promess.QsStat.Services.QsStatService.SpcPathFolderDatasets> of the service. <xref:Promess.QsStat.Services.QsDedicatedCalculation> provides access to one data set permanently loaded by `qsStat` for exploration of the data. Permanently loaded data does not require reloading and reevaluation when exploring the data, e.g. changing the characteristic for which K fields are retrieved, increasing responsiveness.

## Windows application PbldClient
The main application project for Pbld. Contains code for user interaction (GUI), application settings, editors, and presets using the defined services/dependencies. The startup process in <xref:Pbld.App.OnStartup*> serves as an entry point and shows the dependencies. 

### GUI
The subnamespaces/-folders Adorners, Behaviors, Controls, Converters, Decorators, DesignData, Resources, Selectors and Views all contain code to define the visual representation of this program. The naming scheme corresponds to what is expected for a WPF application, e.g. Adorners contains elements which adorn visual elements on the adorner layer.
The source for most of the image elements is the [Visual Studio Image Library](https://www.microsoft.com/en-us/download/details.aspx?id=35825), currently only from `VS2015`. Other sources are the old process picture or denoted in the `XAML` as a comment.
The Views and ViewModels are linked by the interface defined for a View in IViews.

> [!NOTE]
> If there is ever a requirement for a different presentation (more than themes) most of this code should be moved to a Presentation project (see Authentication) and other Presentation projects added.

### Language data
Available languages for Pbld are german and english. Lookup keys only retrieve text, `IsRtl` is not in use. Files must not be formatted automatically because new lines etc. are taken as is when retrieving and later displaying the text. For `XAML` code there is the markup extension <xref:Promess.Common.Extensions.LocalisedTextExtension> to use localized text in the UI and for code <xref:Promess.Language.LanguageManager>.

### Editor(s)
Pbld provides a user with several visual editors to generate their own views (see picture below). All editors share the same abstract base class <xref:Promess.Pbld.ViewModels.BaseEditorViewModel`1>, which defines a common base for moving items and snapping to a grid, size of the canvas, selection, and z-ordering. The implementations of this class wrap the models containing the displayed items, like an image item. The display items implement <xref:Promess.Pbld.Data.Editor.EditorItemBase> and are wrapped by implementations of <xref:Promess.Pbld.Editor.EditorItemModelBase> for the editors to support selection and movement. The base editor class is extended by <xref:Promess.Pbld.ViewModels.BaseEditorViewModelExtended`1> to provide the common functionality of copy, paste and delete.

![Editor overview](images/EditorOverview.png)

### Data retrieval
Following an example for data retrieval for the panel view (see image below). A <xref:Promess.Pbld.ViewModels.MetaPanelViewModel> manages all to display <xref:Promess.Pbld.ViewModels.PanelViewModel>s, which are associated with their data source by a <xref:Promess.Pbld.Data.SpcPath> and display items defined in a <xref:Promess.Pbld.Data.Editor.PanelItemModel>. The display items are registered with  data retrieval by calling <xref:Promess.QsStat.Services.QsStatService.SetPanelItemModels*> and reference height is set by calling <xref:Promess.QsStat.Services.QsStatService.SetPanelHeightAndWidth*>. The items define data retrieval targets. The panel dimension is required when images are generated by`qsStat`. Additional data navigation is provided by the `Request*` methods.
Available data is exposed by the service by the <xref:Promess.QsStat.Services.QsStatService.SpcPathResults> property. For each <xref:Promess.Pbld.Data.SpcPath> a <xref:Promess.QsStat.Data.SpcPathEvaluationDTO> is exposed with meta information, like the state of the process. Part data, which wraps data on the characteristic level, is exposed by the <xref:Promess.QsStat.Data.SpcPathEvaluationDTO.EvaluatedData> property. This allows the <xref:Promess.Pbld.ViewModels.PanelViewModel>s to update the data in their display items.
![Panel qsStat data flow](images/PanelQsStatDataOverview.png)
The items required for displaying the list view grids on the part and characteristic levels are also registered with the service and retrieved using the same property. The overview has no dynamic item requests and the required information is always retrieved. For displaying the information of graphic pages a dedicated data loading mechanism is used and exposed by the <xref:Promess.QsStat.Services.QsStatService.DedicatedCalculation> property.

### Persistence
Pbld saves its settings for the current machine, independent of the user. Pbld tries to use the `SPCDATA` folder structure by retrieving the `SpcSystemDataPath` registry key from `HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Promess GmbH` (32 Bit Registry). The fallback for when this fails is <xref:System.Environment.SpecialFolder.CommonApplicationData>.
Two types of persistence mechanisms are employed. First are the application settings, stored in the applications `.config` file. An example are the general settings of the application. Storage and retrieval is handled by <xref:Promess.Pbld.Utility.ModifiedConfigurationManagerPbld> and configuration elements extend <xref:System.Configuration.ConfigurationSection>. Data, which is persisted by the user, or can be transferred, e.g. graphic projects, use serialization and are marked with the <xref:System.Runtime.Serialization.DataContractAttribute>. The overall configuration is accessed using the <xref:Promess.Pbld.Services.ConfigurationService>. In addition serialization is used internally to generate copies, e.g. when the user can change properties and accept or discard these changes.