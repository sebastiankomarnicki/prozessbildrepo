﻿Project excluded when building solution because of the time it takes to generate the API. Building manually in Visual Studio creates the html.
Check encoding of files if non ASCII symbols are not displayed properly in generated output.
See log.txt for documentation errors
For generating pdfs wkhtmltopdf needs to be installed, see https://dotnet.github.io/docfx/tutorial/walkthrough/walkthrough_generate_pdf.html
docfx.exe must be on the %PATH% if commands targeting the .json files are executed outside visual studio
	- create pdf without title page: docfx pdf docfxOverviewPdf.json --excludeDefaultToc true
	- create api documentation and start webserver: docfx docfx.json --serve
