﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Promess.QsStat.Utility;
using System.Threading;

namespace Promess.QsStat.Tests.UnitTests
{
    [TestClass]
    public class ResourcePoolTest
    {
        

        class TestResource
        {
            public bool IsFree { get; set; }
        }

        const int MaxPoolSize = 3;
        private static Func<Task<TestResource>> _testResourcefactory;

        [ClassInitialize()]
        public static void ClassInit(TestContext context) {
            _testResourcefactory = async () =>
            {
                await Task.Delay(0);
                return new TestResource();
            };
        }

        [TestMethod]
        public async Task GetItem_SingleThread_ReturnsItem()
        {
            using (var pool = await ResourcePool<TestResource>.CreateAsync(MaxPoolSize, _testResourcefactory))
            {
                using (var item = await pool.GetResourceAsync(CancellationToken.None))
                {
                    Assert.IsNotNull(item);
                }
            }        
        }

        [TestMethod]
        public async Task GetItem_Resource_Returned_After_Waiting()
        {
            using (var pool = await ResourcePool<TestResource>.CreateAsync(MaxPoolSize, _testResourcefactory))
            {
                List<PoolItem<TestResource>> poolItems = new List<PoolItem<TestResource>>();
                for (int i = 0; i < MaxPoolSize; i++)
                {
                    poolItems.Add(await pool.GetResourceAsync(CancellationToken.None));
                }

                AutoResetEvent outerWaitLock = new AutoResetEvent(false);

                WaitCallback asyncCall = (o) => {
                    outerWaitLock.Set();
                    PoolItem<TestResource> item = pool.GetResourceAsync(CancellationToken.None).Result;
                    Assert.IsNotNull(item);
                    Assert.IsNotNull(item.Resource);
                    outerWaitLock.Set();
                };
                ThreadPool.QueueUserWorkItem(asyncCall);

                outerWaitLock.WaitOne();
                outerWaitLock.Reset();
                poolItems.ForEach(item => item.Dispose());

                outerWaitLock.WaitOne();
            }        
        }

        [TestMethod]
        public async Task GetItem_MultiThreaded_ReturnsItem()
        {
            Func<Task<TestResource>> factory = async () =>
            {
                await Task.Delay(0);
                return new TestResource() { IsFree = true };
            };
            using (ResourcePool<TestResource> pool = await ResourcePool<TestResource>.CreateAsync(MaxPoolSize, factory))
            {
                List<Task> tasks = new List<Task>();

                const int loopCount = 50;
                const int itemCount = 5;

                for (int i = 0; i < loopCount; i++)
                {
                    for (int j = 0; j < itemCount; j++)
                    {
                        Task t = Task.Run(
                            async () =>
                            {
                                using (var item = await pool.GetResourceAsync(CancellationToken.None))
                                {
                                    TestResource resource = item.Resource;
                                    Assert.IsTrue(resource.IsFree);
                                    resource.IsFree = false;
                                    long ticks = DateTime.Now.Millisecond;
                                    int sleepTime = (int)(ticks % 200);
                                    await Task.Delay(sleepTime);
                                    resource.IsFree = true;
                                }
                            }
                            );
                        tasks.Add(t);
                    }
                }
                await Task.WhenAll(tasks);
            }             
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public async Task Dispose_Cancels_Retrieval()
        {
            ResourcePool<TestResource> pool = await ResourcePool<TestResource>.CreateAsync(MaxPoolSize, _testResourcefactory, (resource) => { });
            using (var waitingForDispose = new SemaphoreSlim(0, 1))
            {
                var itemAccess = Task.Run(async () =>
                {
                    await waitingForDispose.WaitAsync();
                    PoolItem<TestResource> item = await pool.GetResourceAsync(CancellationToken.None);
                });
                
                pool.Dispose();
                waitingForDispose.Release();
                await itemAccess;
            }
        }
    }
}
