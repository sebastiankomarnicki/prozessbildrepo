﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promess.Pbld.Data.Measurements;
using Promess.QsStat.Data;
using Promess.QsStat.Services;
using Promess.QsStat.Utility;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Promess.QsStat.Tests.UnitTests
{
    [TestClass]
    public class QsDataRetrievalTest
    {
        private static QsStatDataRetrievalStub _testdata;
        private static QueryableQsData _emptyQueryable;
        private static ResourcePool<int> _fakeConnections;

        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            var cdata = new CharacteristicDataHolder();
            cdata.TextData = new Dictionary<int, string>() { { 5, "blub" }, { 7, "bla" } };
            cdata.ImageData = new Dictionary<QsStatImageRequest, byte[]>()
            {
                {new QsStatImageRequest(10,20,20), new byte[20] },
                {new QsStatImageRequest(20,10,10), new byte[10] }
            };
            cdata.NumericData = new Dictionary<int, Tuple<string, double>>()
            {
                { 33, new Tuple<string, double>("3.3", 3.3)},
                { 44, new Tuple<string, double>("44", 44) }
            };
            _testdata = new QsStatDataRetrievalStub() {
                Data = new Dictionary<int, CharacteristicDataHolder>() { { 1, cdata } },
                PartData = new Dictionary<int, string>() { { 1001, "teil"} }
            };
            _fakeConnections = ResourcePool<int>.Create(new[]{ 1 });
            _emptyQueryable = new QueryableQsData();
        }

        [TestMethod]
        public async Task NoDataRequiredIsCompleteTest()
        {
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.GetCharacteristicResult(_testdata, con, _emptyQueryable, new QsStatRetrievalItems(), System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), new CharacteristicMetaData(true, ProcessState.Ok), 1, CancellationToken.None);
                Assert.IsFalse(result.Incomplete);
            }
        }

        [TestMethod]
        public async Task CharacteristicTextRequiredIsCompleteTest()
        {
            var required = new QsStatRetrievalItems();
            required.CharacteristicTextKeys.Add(5);
            required.CharacteristicTextKeys.Add(7);
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.GetCharacteristicResult(_testdata, con, _emptyQueryable, required, System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), new CharacteristicMetaData(true, ProcessState.Ok), 0, CancellationToken.None);
                Assert.IsFalse(result.Incomplete);
            }            
        }

        [TestMethod]
        public async Task CharacteristicTextRequiredIsIncompleteTest()
        {
            var required = new QsStatRetrievalItems();
            required.CharacteristicTextKeys.Add(6);
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.GetCharacteristicResult(_testdata, con, _emptyQueryable, required, System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), new CharacteristicMetaData(true, ProcessState.Ok), 0, CancellationToken.None);
                Assert.IsTrue(result.Incomplete);
            }           
        }

        [TestMethod]
        public async Task ImageKeysRequiredIsCompleteTest()
        {
            var required = new QsStatRetrievalItems();
            required.ImageKeys.Add(new QsStatImageRequest(10, 20, 20));
            required.ImageKeys.Add(new QsStatImageRequest(20, 10, 10));
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.GetCharacteristicResult(_testdata, con, _emptyQueryable, required, System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), new CharacteristicMetaData(true, ProcessState.Ok), 0, CancellationToken.None);
                Assert.IsFalse(result.Incomplete);
            }            
        }

        [TestMethod]
        public async Task ImageKeysRequiredIsIncompleteTest()
        {
            var required = new QsStatRetrievalItems();
            required.ImageKeys.Add(new QsStatImageRequest(10, 20, 20));
            required.ImageKeys.Add(new QsStatImageRequest(22, 10, 10));
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.GetCharacteristicResult(_testdata, con, _emptyQueryable, required, System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), new CharacteristicMetaData(true, ProcessState.Ok), 0, CancellationToken.None);
                Assert.IsTrue(result.Incomplete);
            }              
        }

        [TestMethod]
        public async Task NumericAndTextKeysRequiredIsCompleteTest()
        {
            var required = new QsStatRetrievalItems();
            required.NumericAndTextKeys.Add(33);
            required.NumericAndTextKeys.Add(44);
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.GetCharacteristicResult(_testdata, con, _emptyQueryable, required, System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), new CharacteristicMetaData(true, ProcessState.Ok), 0, CancellationToken.None);
                Assert.IsFalse(result.Incomplete);
            }              
        }

        [TestMethod]
        public async Task NumericAndTextKeysRequiredIsIncompleteTest()
        {
            var required = new QsStatRetrievalItems();
            required.NumericAndTextKeys.Add(34);
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.GetCharacteristicResult(_testdata, con, _emptyQueryable, required, System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), new CharacteristicMetaData(true, ProcessState.Ok), 0, CancellationToken.None);
                Assert.IsTrue(result.Incomplete);
            }
        }

        [TestMethod]
        public async Task PartDataIsCompleteTest()
        {
            var required = new QsStatRetrievalItems();
            required.PartTextKeys.Add(1001);
            var charaMetaData = new Tuple<bool, IReadOnlyList<CharacteristicMetaData>>(true, new List<CharacteristicMetaData>() { new CharacteristicMetaData(true, ProcessState.Ok) });
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.RetrieveData(_testdata, con, _emptyQueryable, required, System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), charaMetaData, CancellationToken.None);
                Assert.IsFalse(result.Item2.Incomplete);
            }
        }

        [TestMethod]
        public async Task PartDataIsIncompleteTest()
        {
            var required = new QsStatRetrievalItems();
            //adding fake part key for test, otherwise will not be incomplete
            required.PartTextKeys.Add(9999);
            var charaMetaData = new Tuple<bool, IReadOnlyList<CharacteristicMetaData>>(true, new List<CharacteristicMetaData>() { new CharacteristicMetaData(true, ProcessState.Ok) });
            using (var con = await _fakeConnections.GetResourceAsync(CancellationToken.None))
            {
                var result = QsPeriodicalCalculation.RetrieveData(_testdata, con, _emptyQueryable, required, System.Windows.Size.Empty, new QsStatPartResultDTO(Guid.NewGuid()), charaMetaData, CancellationToken.None);
                Assert.IsTrue(result.Item2.Incomplete);
            }
        }

        private class QsStatDataRetrievalStub : IQsStatDataRetrieval
        {
            public Dictionary<int, CharacteristicDataHolder> Data { get; set; }
            public Dictionary<int, string> PartData { get; set; }
            public Dictionary<int, string> GetCharacteristicData(int connectionId, IEnumerable<int> characteristicTextKeys, int characteristic, CancellationToken ct)
            { 
                var result = new Dictionary<int, string>();
                if (Data.TryGetValue(characteristic, out var retrieved))
                {
                    foreach (var key in characteristicTextKeys)
                    {
                        if (retrieved.TextData.TryGetValue(key, out var value))
                            result[key] = value;
                    }
                }
                return result;
            }

            public Dictionary<QsStatImageRequest, byte[]> GetGraphResult(int connectionId, IEnumerable<QsStatImageRequest> imageKeys, int characteristic, System.Windows.Size panelSize, CancellationToken ct)
            {
                var result = new Dictionary<QsStatImageRequest, byte[]>();
                if (Data.TryGetValue(characteristic, out var retrieved))
                {
                    foreach (var key in imageKeys)
                    {
                        if (retrieved.ImageData.TryGetValue(key, out var value))
                            result[key] = value;
                    }
                }
                return result;
            }

            public Dictionary<int, string> GetPartData(int connectionId, IEnumerable<int> partTextKeys, CancellationToken ct)
            {
                var result = new Dictionary<int, string>();
                foreach(var key in partTextKeys)
                {
                    if (PartData.TryGetValue(key, out var retrieved))
                        result[key] = retrieved;
                }
                return result;
            }

            public Dictionary<int, Tuple<string, double>> GetTextResult(int connectionId, IEnumerable<int> numericAndTextKeys, int characteristic, CancellationToken ct)
            {
                var result = new Dictionary<int, Tuple<string, double>>();
                if (Data.TryGetValue(characteristic, out var retrieved))
                {
                    foreach (var key in numericAndTextKeys)
                    {
                        if (retrieved.NumericData.TryGetValue(key, out var value))
                            result[key] = value;
                    }
                }
                return result;
            }
        }
        internal class CharacteristicDataHolder
        {
            public Dictionary<int, string> TextData { get; set; }
            public Dictionary<QsStatImageRequest, byte[]> ImageData { get; set; }
            public Dictionary<int, Tuple<string, double>> NumericData { get; set; }

        }
    }
}
