﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promess.Common.Settings.Extensions;
using System.Configuration;
using System.IO;
using Promess.Common.Settings;

namespace Common.Tests.UnitTests
{
    /// <summary>
    /// Summary description for ConfigurationSectionExtensionTest
    /// </summary>
    [TestClass]
    public class ConfigurationSectionExtensionTest
    {
        private TestContext testContextInstance;
        private static string tmpConfigFile;
        private static ModifiedConfigurationManager cm;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [ClassInitialize()]
        public static void ConfigurationSectionExtensionTestInitialize(TestContext tc)
        {
            tmpConfigFile = Path.GetTempPath()+nameof(ConfigurationSectionExtensionTest)+".xml";
            if (File.Exists(tmpConfigFile))
                File.Delete(tmpConfigFile);

            cm = new ModifiedConfigurationManager();
            cm.Configuration = ModifiedConfigurationManager.GetConfigurationFromFile(tmpConfigFile);
            ConfigurationSectionExtensionWData data = cm.GetSectionAndAddIfMissing<ConfigurationSectionExtensionWData>("TestSection");
            data.Background = ConsoleColor.Blue;
            cm.Save(ConfigurationSaveMode.Full);
        }

        [ClassCleanup()]
        public static void ConfigurationSectionExtensionTestCleanup()
        {
            cm.Configuration = null;
            if (File.Exists(tmpConfigFile))
                File.Delete(tmpConfigFile);
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Copy_DoesNotImpactSave()
        {
            ConfigurationSectionExtensionWData originalData = cm.GetSectionAndAddIfMissing<ConfigurationSectionExtensionWData>("TestSection");
            ConfigurationSectionExtensionWData copiedData = originalData.Copy() as ConfigurationSectionExtensionWData;
            copiedData.Background = ConsoleColor.White;
            cm.Save(ConfigurationSaveMode.Full);
            ConfigurationSectionExtensionWData shouldBeOriginalData = cm.GetSectionAndAddIfMissing<ConfigurationSectionExtensionWData>("TestSection");
            Assert.AreEqual(originalData.Background, shouldBeOriginalData.Background);
            Assert.AreNotEqual(originalData.Background, copiedData.Background);
        }

        [TestMethod]
        public void CopyDataOnly_ImpactsSave()
        {
            ConfigurationSectionExtensionWData originalData = cm.GetSectionAndAddIfMissing<ConfigurationSectionExtensionWData>("TestSection");
            ConfigurationSectionExtensionWData newData = originalData.Copy() as ConfigurationSectionExtensionWData;
            newData.Background = ConsoleColor.Cyan;
            newData.CopyDataOnly(originalData);
            cm.Save(ConfigurationSaveMode.Full);
            ConfigurationSectionExtensionWData shouldBeNewData = cm.GetSectionAndAddIfMissing<ConfigurationSectionExtensionWData>("TestSection");
            Assert.AreEqual(newData.Background, originalData.Background);
            Assert.AreEqual(newData.Background, shouldBeNewData.Background);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CopyDataOnly_SourceDestinationMustHaveSameType()
        {
           new ConfigurationSectionExtensionWData().CopyDataOnly(new ConfigurationSectionExtensionDifferentData());
        }
    }

    internal class ConfigurationSectionExtensionWData:ConfigurationSectionExtension
    {
        [ConfigurationProperty("Background", DefaultValue = ConsoleColor.Black)]
        public ConsoleColor Background
        {
            get
            {
                return GetValue<ConsoleColor>("Background");
            }
            set
            {
                this["Background"] = value;
            }
        }

    }

    internal class ConfigurationSectionExtensionDifferentData:ConfigurationSectionExtension
    {
        [ConfigurationProperty("Foreground", DefaultValue = ConsoleColor.Black)]
        public ConsoleColor Foreground
        {
            get
            {
                return GetValue<ConsoleColor>("Foreground");
            }
            set
            {
                this["Foreground"] = value;
            }
        }

    }
}
