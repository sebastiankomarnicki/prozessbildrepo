﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Promess.Common.QDas;

namespace Common.Tests.UnitTests.QDas
{
    /// <summary>
    /// Summary description for QDasValueConversionTest
    /// </summary>
    [TestClass]
    public class QDasValueConversionTest
    {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void QdasDateStringToDateTimeSucceedsOnValidInput()
        {
            string[] qdasDateParts = {
                "17.06.96", "17.06.1996", "6/17/96", "6/17/1996",
                "96-6-17", "1996-6-17"
            };
            string[] qdasAMTimes =
            {
                "11:20:5", "11:20:5am","11:20:5a"
            };
            string[] qdasPMTimes =
            {
                "17:3:6", "5:3:6pm","5:3:6p"
            };
            const string onlyHour = "5";
            const string hourMinute = "5:6";
            string[] dateTimeSeparators = new string[] { " ", "/" };
            DateTime amDT = new DateTime(1996, 6, 17, 11, 20, 5);
            DateTime pmDT = new DateTime(1996, 6, 17, 17, 3, 6);
            DateTime onlyHourDT = new DateTime(1996, 6, 17, 5, 0, 0);
            DateTime hourMinuteDT = new DateTime(1996, 6, 17, 5, 6, 0);
            DateTime parsedDT;
            string input;
            Dictionary<String, Tuple<bool, DateTime>> results = new Dictionary<string, Tuple<bool, DateTime>>();
            foreach (string separator in dateTimeSeparators)
            {
                foreach (string date in qdasDateParts)
                {
                    foreach (string time in qdasAMTimes)
                    {
                            input = date + separator + time;
                            parsedDT = QdasValueConversions.QdasDateStringToDateTime(input);
                            results.Add(input, new Tuple<bool, DateTime>(amDT.Equals(parsedDT), parsedDT));
                    }
                    foreach (string time in qdasPMTimes)
                    {
                            input = date + separator + time;
                            parsedDT = QdasValueConversions.QdasDateStringToDateTime(input);
                            results.Add(input, new Tuple<bool, DateTime>(pmDT.Equals(parsedDT), parsedDT));
                    }

                    input = date + separator + onlyHour;
                    parsedDT = QdasValueConversions.QdasDateStringToDateTime(input);
                    results.Add(input, new Tuple<bool, DateTime>(onlyHourDT.Equals(parsedDT), parsedDT));
                    input = date + separator + hourMinute;
                    parsedDT = QdasValueConversions.QdasDateStringToDateTime(input);
                    results.Add(input, new Tuple<bool, DateTime>(hourMinuteDT.Equals(parsedDT), parsedDT));
                }
            }
            Assert.IsTrue(results.Values.All(triple => triple.Item1));
        }

        [TestMethod]
        public void QdasDateStringToDateTimeReturnsDefaultDateTimeOnInvalidFormat()
        {
            DateTime defaultDT = new DateTime(1970, 1, 1, 0, 0, 0);
            string[] invalidInputs = new string[] { "", "1/1/5:4", "17.06.1996","5:4:3" };
            foreach (string input in invalidInputs)
            {
                Assert.AreEqual(defaultDT,QdasValueConversions.QdasDateStringToDateTime(input));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void QdasStringDateTimeThrowsExceptionOnNonExistingDate()
        {
            string nonExistantDT = "2/31/1999/5";
            QdasValueConversions.QdasDateStringToDateTime(nonExistantDT);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void QdasStringDateTimeThrowsExceptionOnNonNumber()
        {
            string nonParseableDateTime = "2/a/1999/5";
            QdasValueConversions.QdasDateStringToDateTime(nonParseableDateTime);
        }
    }
}
