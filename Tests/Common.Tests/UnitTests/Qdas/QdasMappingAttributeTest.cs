﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promess.Common.QDas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Tests.UnitTests.QDas
{
    [TestClass]
    public class QdasMappingAttributeTest
    {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestMethod]
        public void TS_TeiledatenSetUsingAttributeTest()
        {
            var partData = new TS_TEILEDATEN();
            partData.TrySetField(1001, "test");
            Assert.AreEqual("test", partData.sTeil_Nummer);
        }

        [TestMethod]
        public void TS_MerkmalsdatenSetSameFieldDifferentKNumbersTest()
        {
            var characteristicsData = new TS_MERKMALSDATEN();
            characteristicsData.TrySetField(8500, 5);
            Assert.AreEqual(5, characteristicsData.iStichprobe_Umfang_gesamt.Value);
            characteristicsData.TrySetField(8505, 11);
            Assert.AreEqual(11, characteristicsData.iStichprobe_Umfang_gesamt.Value);
        }
    }
}
