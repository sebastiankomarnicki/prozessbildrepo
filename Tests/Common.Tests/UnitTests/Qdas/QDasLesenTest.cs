﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Reflection;
using System.Globalization;
using System.Linq;
using Promess.Common.QDas;
using Promess.Common.QDas.Settings;

namespace Common.Tests.UnitTests.QDas
{
    /// <summary>
    /// Summary description for QDasLesenTest
    /// </summary>
    [TestClass]
    public class QDasLesenTest
    {

        public QDasLesenTest()
        {
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void QdasZeileSplitEmptyLineTest()
        {
            QdasLineSplitResult qlsr = QdasLesen.QdasZeile_Split(String.Empty);
            Assert.AreEqual(String.Empty, qlsr.Key);
            Assert.IsFalse(qlsr.HasData);
            Assert.AreEqual(QdasLineSplitResult.InvalidOrMissingKeyIndex, qlsr.KeyIndex);
        }

        [TestMethod]
        public void QdasZeileSplitInvalidKeyTest()
        {
            QdasLineSplitResult qlsr = QdasLesen.QdasZeile_Split("setrser");
            Assert.AreEqual(String.Empty, qlsr.Key);
            Assert.IsTrue(qlsr.HasData,"In the legacy code this evaluates to true when an invalid key is given. In case this test fails due to refactoring (removing that behavior), change this line.");
            Assert.AreEqual(QdasLineSplitResult.InvalidOrMissingKeyIndex,qlsr.KeyIndex);
        }

        [TestMethod]
        public void QdasZeileSplitReadOnlyKeyTest()
        {
            const string line = "K1042";
            QdasLineSplitResult qlsr = QdasLesen.QdasZeile_Split(line);
            Assert.AreEqual(line, qlsr.Key);
            Assert.IsFalse(qlsr.HasData);
            Assert.AreEqual(QdasLineSplitResult.InvalidOrMissingKeyIndex,qlsr.KeyIndex);
        }

        [TestMethod]
        public void QdasZeileSplitKeyAndDataTest()
        {
            const string line = "K2407/0 Daten1";
            QdasLineSplitResult qlsr = QdasLesen.QdasZeile_Split(line);
            Assert.AreEqual("K2407", qlsr.Key);
            Assert.IsTrue(qlsr.HasData);
            Assert.AreEqual(0, qlsr.KeyIndex);
            Assert.AreEqual(1, qlsr.Remainder.Length);
            Assert.AreEqual("Daten1", qlsr.Remainder[0]);
        }

        [TestMethod]
        public void QdasZeileSplitSlashSpaceMissingKeyIndexTest()
        {
            const string line = "K2407/ 0 Daten1";
            QdasLineSplitResult qlsr = QdasLesen.QdasZeile_Split(line);
            Assert.AreEqual("K2407", qlsr.Key);
            Assert.IsTrue(qlsr.HasData);
            Assert.AreEqual(QdasLineSplitResult.InvalidOrMissingKeyIndex, qlsr.KeyIndex);
            Assert.AreEqual(1, qlsr.Remainder.Length);
            Assert.AreEqual("0 Daten1", qlsr.Remainder[0]);
        }

        [TestMethod]
        public void QdasZeileSplitMultipleRemainderEntriesTest()
        {
            string line = String.Format("0{0}1{0}2", ""+Convert.ToChar(15));
            QdasLineSplitResult qlsr = QdasLesen.QdasZeile_Split(line);
            Assert.IsTrue(qlsr.HasData);
            Assert.AreEqual(QdasLineSplitResult.InvalidOrMissingKeyIndex, qlsr.KeyIndex);
            Assert.AreEqual(3, qlsr.Remainder.Length);
            Assert.AreEqual("0", qlsr.Remainder[0]);
            Assert.AreEqual("1", qlsr.Remainder[1]);
            Assert.AreEqual("2", qlsr.Remainder[2]);
        }

        [TestMethod]
        public void QdasDFD_Datei_LesenSimpleFalkFile()
        {
            const string file = @"UnitTests\Qdas\P(16 Tesa 1-16 DRANGE auto)_OP(16 Tesa 1-16 DRANGE auto)_CS(16 Tesa 1-16 DRANGE auto [1])_TS(20080222091243569).dfd";
            TS_TEILEDATEN partData;
            TS_MERKMALSDATEN[] characteristicsData=null;
            QdasDfdReadResult qrs;
            using (FileStream fs = new FileStream(file, FileMode.Open))
            {
                bool valid=false;
                (valid, qrs) = QdasLesen.QdasDfdDateiLesen(fs, Encoding.UTF8);
                Assert.IsTrue(valid);
                characteristicsData = qrs.CharacteristicsData;
                partData = qrs.PartsData;
            }
            Assert.AreEqual(iErrCodes.iecno_error, qrs.ErrorCode);
            Assert.AreEqual(1, characteristicsData.Length);
            Assert.AreEqual("16 Tesa 1-16 DRANGE auto", partData.sTeil_Nummer);
            Assert.AreEqual("16 Tesa 1-16 DRANGE auto", partData.sTeil_Bezeichnung);
            Assert.AreEqual("16 Tesa 1-16 DRANGE", partData.sTeil_Bezeichnung_kurz);
            Assert.AreEqual("<Neues Modell>", partData.sErzeugnis);
            Assert.AreEqual("16 Tesa 1-16 DRANGE",partData.sTeil_Nummer_kurz);
            Assert.AreEqual(0, partData.iDoku_Pflicht.Value);
            //some empty fields
            Assert.AreEqual(null, partData.sZeichnung_Nummer_Text);
            Assert.AreEqual(null, partData.sZeichnung_Aenderung);
            Assert.AreEqual(null, partData.sZeichnung_Index);

            Assert.AreEqual("-", partData.sMaschine_Nummer_Text);
            Assert.AreEqual("16 Tesa 1-16 DRANGE auto", partData.sArbeitsgang);
            //Assert.AreEqual("", partData.sPruefeinrichtung_Nummer_Text);
            //Assert.AreEqual("", partData.sPruefeinrichtung_Bezeichnung);
            Assert.AreEqual(new DateTime(2008,2,22,11,11,50), partData.sPruefbeginn);
            //Assert.AreEqual("", partData.sPruefplatz);
            //Assert.AreEqual("", partData.sBemerkung);
            Assert.AreEqual("-", characteristicsData[0].sMaschine_Nummer_Text);
            Assert.AreEqual("16 Tesa 1-16 DRANGE auto", characteristicsData[0].sPruefplan_Name);
            Assert.AreEqual("Daten1", characteristicsData[0].sSPC_Geraet_Nummer_Text);
            Assert.AreEqual("Promess GmbH", characteristicsData[0].sSPC_Geraet_Bezeichnung);
            Assert.AreEqual("Daten1", characteristicsData[0].sSPC_Geraet_Typ);
            Assert.AreEqual("1", characteristicsData[0].sMerkmal_Nummer_Text);
            Assert.AreEqual("M1", characteristicsData[0].sMerkmal_Bezeichnung);
            Assert.AreEqual("M1", characteristicsData[0].sMerkmal_Bezeichnung_kurz);
            Assert.AreEqual(0, characteristicsData[0].iMerkmal_Art.Value);
            Assert.AreEqual(2, characteristicsData[0].iMerkmal_Klasse.Value);
            Assert.AreEqual(0, characteristicsData[0].iDoku_Pflicht.Value);
            Assert.AreEqual(0, characteristicsData[0].iRegelungsart.Value);
            Assert.AreEqual(1, characteristicsData[0].iVerteilungsart.Value);
            Assert.AreEqual(0.01,characteristicsData[0].dKlassenweite.Value,0.0001);
            Assert.AreEqual(4, characteristicsData[0].iNachkommastellen.Value);
            Assert.AreEqual(0.1, characteristicsData[0].UpperSpecificationLimit.Value, 0.0001);
            Assert.AreEqual(0.1, characteristicsData[0].UpperAllowance.Value, 0.0001);
            Assert.AreEqual(1, characteristicsData[0].iArt_der_Grenze_unten.Value);
            Assert.AreEqual(1, characteristicsData[0].iArt_der_Grenze_oben.Value);
            Assert.AreEqual(-3.0, characteristicsData[0].LowerPlausibilityLimit.Value, 0.0001);
            Assert.AreEqual(3.0, characteristicsData[0].UpperPlausibilityLimit.Value, 0.0001);
            Assert.AreEqual("mm", characteristicsData[0].sEinheit_Bezeichnung);
            Assert.AreEqual(0.01, characteristicsData[0].dProzessstreuung.Value, 0.0001);
            Assert.AreEqual("Test Tesa", characteristicsData[0].sFertigungsart_Bezeichnung);
            Assert.AreEqual(0.0001,characteristicsData[0].dPruefmittel_Aufloesung.Value, 0.000001);
            Assert.AreEqual(31, characteristicsData[0].ChartTypeLocation.iLage_Kartenart);
            Assert.AreEqual(2, characteristicsData[0].ChartTypeLocation.iLage_Streuungsschaetzer.Value);
            Assert.AreEqual(0.001, characteristicsData[0].dLage_Mittellage.Value, 0.00001);
            Assert.AreEqual(0.00071, characteristicsData[0].dLage_UEG.Value, 0.00000001);
            Assert.AreEqual(0.00129, characteristicsData[0].dLage_OEG.Value, 0.00000001);
            Assert.AreEqual(52, characteristicsData[0].ChartTypeVariation.iStreuung_Kartenart);
            Assert.AreEqual(2, characteristicsData[0].ChartTypeVariation.iStreuung_Streuungsschaetzer.Value);
            Assert.AreEqual(0.0003, characteristicsData[0].dStreuung_Mittellage.Value, 0.000001);
            Assert.AreEqual(0.00049, characteristicsData[0].dStreuung_OEG.Value, 0.00000001);
            Assert.AreEqual(5, characteristicsData[0].iStichprobe_Umfang_gesamt.Value);
            Assert.AreEqual(1.33, characteristicsData[0].dCp_Wert_gefordert.Value, 0.00001);
            Assert.AreEqual(1.33, characteristicsData[0].dCpk_Wert_gefordert.Value, 0.00001);
            Assert.AreEqual(1.33, characteristicsData[0].dCp_Wert_gefixt.Value, 0.00001);
            Assert.AreEqual(1.33, characteristicsData[0].dCpk_Wert_gefixt.Value, 0.00001);
            Assert.AreEqual(-0.288, characteristicsData[0].dKorrekturgrenze_unten.Value, 0.000001);
            Assert.AreEqual(0.288, characteristicsData[0].dKorrekturgrenze_oben.Value, 0.000001);

            var mashCatalog = partData.MainCatalogs.Where(elem => elem.MainCatalogKNumber == 4060).FirstOrDefault(); 
            Assert.IsNotNull(mashCatalog);
            Assert.AreEqual("Maschinenkatalog", mashCatalog.MainCatalogName);
            var mashEntryWhereList = mashCatalog.GetCatalogEntriesAndQdasKeys().Where(elem => elem.Item1 == 1).ToList();
            Assert.IsTrue(mashEntryWhereList.Count==1);
            var mashEntry = mashEntryWhereList.First().Item2;
            object retrieved = null;
            bool success = mashEntry.TryGetField(4062, out retrieved);
            Assert.IsTrue(success);
            Assert.AreEqual("1", retrieved);
            success = mashEntry.TryGetField(4063, out retrieved);
            Assert.IsTrue(success);
            Assert.AreEqual("-", retrieved);

            var eventCatalog = partData.K4220Catalog.EventCatalog;
            Assert.AreEqual("dthzhed", eventCatalog.MainCatalogName);
            var eventEntryWhereList = eventCatalog.GetCatalogEntriesAndQdasKeys().Where(elem => elem.Item1 == 1).ToList();
            Assert.IsTrue(eventEntryWhereList.Count == 1);
            var eventEntry = eventEntryWhereList.First().Item2;
            success = eventEntry.TryGetField(4222, out retrieved);
            Assert.IsTrue(success);
            Assert.AreEqual("1", retrieved);
            success = eventEntry.TryGetField(4223, out retrieved);
            Assert.IsTrue(success);
            Assert.AreEqual("sre", retrieved);
        }

        [TestMethod]
        public void QdasDFXDateiLesenVariable()
        {
            TS_MERKMALSDATEN tmpData = new TS_MERKMALSDATEN();
            tmpData.iMerkmal_Art = (short)iMerkmal_Art.imaVARIABEL;
            QdasValueLineSettings qs = new QdasValueLineSettings();
            double measuringValue = 5.2;
            short iAttribut = 1;
            List<short> aiEreignis = new List<short>() { 11, 14 };
            DateTime dt = new DateTime(2017, 11, 12, 13, 14, 15);
            StringBuilder sb = new StringBuilder();
            sb.Append(measuringValue.ToString(CultureInfo.InvariantCulture));
            sb.Append((char)20);
            sb.Append(iAttribut.ToString());
            sb.Append((char)20);
            sb.Append(dt.ToString("dd.MM.yyyy/HH:mm:ss", CultureInfo.InvariantCulture));
            sb.Append((char)20);
            sb.Append(String.Join(",", aiEreignis));

            sb.AppendLine();
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString())))
            {
                var res = QdasLesen.QdasDFXDateiLesen(stream, new TS_MERKMALSDATEN[] { tmpData }, qs, Encoding.UTF8);
                Assert.AreEqual(1, res.Data.Count);
                var enumerator = res.Data.GetEnumerator();
                enumerator.MoveNext();
                var parsed = enumerator.Current[0];
                Assert.AreEqual(measuringValue, parsed.dMesswert);
                Assert.AreEqual(iAttribut, parsed.iAttribut);
                Assert.IsTrue(parsed.sDatumZeit.HasValue);
                Assert.AreEqual(dt, parsed.sDatumZeit.Value);
                foreach (var ereig in aiEreignis)
                {
                    Assert.IsTrue(parsed.aiEreignis.Contains(ereig));
                }
            }
        }

        [TestMethod]
        public void QdasDFXDateiLesenAttribute()
        {
            TS_MERKMALSDATEN tmpData = new TS_MERKMALSDATEN();
            tmpData.iMerkmal_Art = (short)iMerkmal_Art.imaATTRIBUTIV;
            QdasValueLineSettings qs = new QdasValueLineSettings();
            int sampleSize = 25;
            int numberErrors = 2;
            short iAttribut = 1;
            List<short> aiEreignis = new List<short>() { 11, 14 };
            DateTime dt = new DateTime(2017, 11, 12, 13, 14, 15);
            StringBuilder sb = new StringBuilder();
            sb.Append($"{sampleSize}000");
            sb.Append((char)20);
            sb.Append(numberErrors.ToString());
            sb.Append((char)20);
            sb.Append("0");
            sb.Append((char)20);
            sb.Append(iAttribut.ToString());
            sb.Append((char)20);
            sb.Append(dt.ToString("dd.MM.yyyy/HH:mm:ss", CultureInfo.InvariantCulture));
            sb.Append((char)20);
            sb.Append(String.Join(",", aiEreignis));
            sb.AppendLine();

            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString())))
            {
                var res = QdasLesen.QdasDFXDateiLesen(stream, new TS_MERKMALSDATEN[] { tmpData }, qs, Encoding.UTF8);
                var enumerator = res.Data.GetEnumerator();
                enumerator.MoveNext();
                var parsed = enumerator.Current[0];
                Assert.AreEqual(sampleSize, parsed.iStichprobenUmfang.Value);
                Assert.AreEqual(numberErrors, parsed.iAnzahl_Fehler.Value);
                Assert.AreEqual(iAttribut, parsed.iAttribut.Value);
                Assert.IsTrue(parsed.sDatumZeit.HasValue);
                Assert.AreEqual(dt, parsed.sDatumZeit.Value);
                foreach (var ereig in aiEreignis)
                {
                    Assert.IsTrue(parsed.aiEreignis.Contains(ereig));
                }
            }
        }

        [TestMethod]
        public void QdasDFXDateiLesenQdasSettingsVariable()
        {
            TS_MERKMALSDATEN tmpData = new TS_MERKMALSDATEN();
            tmpData.iMerkmal_Art = (short) iMerkmal_Art.imaVARIABEL;
            QdasValueLineSettings qs = new QdasValueLineSettings();
            qs.ValueLineKNumbers = new List<short>(){ 4, 2, 5, 1 };
            double measuringValue = 5.2;
            short iAttribut = 1;
            List<short> aiEreignis = new List<short>() { 11, 14 };
            DateTime dt = new DateTime(2017, 11, 12, 13, 14, 15);
            StringBuilder sb = new StringBuilder();
            sb.Append(dt.ToString("dd.MM.yyyy/HH:mm:ss",CultureInfo.InvariantCulture));
            sb.Append((char)20);
            sb.Append(iAttribut.ToString());
            sb.Append((char)20);
            sb.Append(String.Join(",", aiEreignis));
            sb.Append((char)20);
            sb.Append(measuringValue.ToString(CultureInfo.InvariantCulture));
            sb.AppendLine();
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString())))
            {
                var res = QdasLesen.QdasDFXDateiLesen(stream,new TS_MERKMALSDATEN[]{ tmpData},qs, Encoding.UTF8);
                Assert.AreEqual(1, res.Data.Count);
                var enumerator = res.Data.GetEnumerator();
                enumerator.MoveNext();
                var parsed = enumerator.Current[0];
                Assert.AreEqual(measuringValue, parsed.dMesswert);
                Assert.AreEqual(iAttribut, parsed.iAttribut);
                Assert.IsTrue(parsed.sDatumZeit.HasValue);
                Assert.AreEqual(dt, parsed.sDatumZeit.Value);
                foreach (var ereig in aiEreignis)
                {
                    Assert.IsTrue(parsed.aiEreignis.Contains(ereig));
                }
            }
        }

        [TestMethod]
        public void QdasDFXDateiLesenQdasSettingsAttribute()
        {
            TS_MERKMALSDATEN tmpData = new TS_MERKMALSDATEN();
            tmpData.iMerkmal_Art = (short)iMerkmal_Art.imaATTRIBUTIV;
            QdasValueLineSettings qs = new QdasValueLineSettings();
            qs.ValueLineKNumbers = new List<short>() { 4, 2, 5, 1 };
            int sampleSize = 25;
            int numberErrors = 2;
            short iAttribut = 1;
            List<short> aiEreignis = new List<short>() { 11, 14 };
            DateTime dt = new DateTime(2017, 11, 12, 13, 14, 15);
            StringBuilder sb = new StringBuilder();
            sb.Append(dt.ToString("dd.MM.yyyy/HH:mm:ss", CultureInfo.InvariantCulture));
            sb.Append((char)20);
            sb.Append(iAttribut.ToString());
            sb.Append((char)20);
            sb.Append(String.Join(",", aiEreignis));
            sb.Append((char)20);
            sb.Append($"{sampleSize}000");
            sb.Append((char)20);
            sb.Append(numberErrors.ToString());
            sb.Append((char)20);
            sb.Append("0");
            sb.AppendLine();
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString())))
            {
                var res = QdasLesen.QdasDFXDateiLesen(stream, new TS_MERKMALSDATEN[] { tmpData }, qs, Encoding.UTF8);
                Assert.AreEqual(1, res.Data.Count);
                var enumerator = res.Data.GetEnumerator();
                enumerator.MoveNext();
                var parsed = enumerator.Current[0];
                Assert.AreEqual(sampleSize, parsed.iStichprobenUmfang.Value);
                Assert.AreEqual(numberErrors, parsed.iAnzahl_Fehler.Value);
                Assert.AreEqual(iAttribut, parsed.iAttribut.Value);
                Assert.IsTrue(parsed.sDatumZeit.HasValue);
                Assert.AreEqual(dt, parsed.sDatumZeit.Value);
                foreach (var ereig in aiEreignis)
                {
                    Assert.IsTrue(parsed.aiEreignis.Contains(ereig));
                }
            }
        }
    }
}
