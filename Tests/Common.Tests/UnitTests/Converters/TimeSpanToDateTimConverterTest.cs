﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promess.Common.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Common.Tests.UnitTests.Converters
{
    [TestClass]
    public class TimeSpanToDateTimConverterTest
    {
        private readonly TimeSpanToDateTimeConverter _converter;

        public TimeSpanToDateTimConverterTest()
        {
            _converter = new TimeSpanToDateTimeConverter();
        }

        [TestMethod]
        public void Convert()
        {
            TimeSpan original = new TimeSpan(1, 1, 1);
            Assert.AreEqual(original,
                _converter.ConvertBack(_converter.Convert(original,null,null,null),
                null,null,null));
        }

        [TestMethod]
        public void ConvertInvalidInput()
        {
            Assert.AreEqual(Binding.DoNothing,_converter.Convert(null,null,null,null));
            Assert.AreEqual(Binding.DoNothing,_converter.Convert("noneTimeSpan", null, null, null));
        }

        [TestMethod]
        public void ConvertBackInvalidInput()
        {
            Assert.AreEqual(Binding.DoNothing, _converter.Convert(null, null, null, null));
            Assert.AreEqual(Binding.DoNothing, _converter.Convert("noneTimeSpan", null, null, null));
        }
    }
}
