﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promess.Common.QDas;
using Promess.Pbld.Data.Measurements;
using Promess.Pbld.Data.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promess.Data.Tests.IntegrationTests
{
    [TestClass]
    public class DataSetTest
    {
        static string _testFileCopy;
        static string _content;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            DataSetTest._content = File.ReadAllText(@"IntegrationTests\TestData.dfq");
            DataSetTest._testFileCopy = Path.GetTempPath() + Guid.NewGuid().ToString() + ".dfq";
            File.WriteAllText(DataSetTest._testFileCopy, _content);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            if (File.Exists(_testFileCopy))
                File.Delete(_testFileCopy);
        }

        [TestMethod]
        public void SplitDfqDataTest()
        {
            (var dfd, var dfx) = DataSet.SplitDfqData(DataSetTest._content);
            Assert.IsTrue(dfd.StartsWith("K0100 2"));
            Assert.IsTrue(dfd.TrimEnd().EndsWith("K4721/5 0"));
            Assert.IsTrue(dfx.StartsWith("0.008650018.08.2017/13:07:28-0.025978018.08.2017/13:07:28"));
            Assert.IsTrue(dfx.TrimEnd().EndsWith("K0010/0 1"));
        }

        [TestMethod]
        public async Task MaxDfxLinesTest()
        {
            var settings = new GeneralSettings() { MaxNumberOfValues = 5};
            var ds = new DataSet(DataSetTest._testFileCopy, settings, Encoding.UTF8);
            await ds.LoadAsync(DateTime.UtcNow, false);
            Assert.IsTrue(ds.ParsedDfxData.LineData.Count == settings.MaxNumberOfValues * DataSet.DELETEDFXMult);
        }

        [TestMethod]
        public async Task DeleteOldFileTest()
        {
            string fn = Path.GetTempPath() + Guid.NewGuid().ToString() + ".dfq";
            File.WriteAllText(fn, DataSetTest._content);
            try
            {
                File.SetLastWriteTimeUtc(fn, new DateTime(1970, 12, 1));

                var settings = new GeneralSettings();
                var ds = new DataSet(fn, settings, Encoding.UTF8);
                Assert.IsFalse(await ds.LoadAsync(DateTime.UtcNow, true));
                Assert.IsFalse(File.Exists(fn));
            }
            finally
            {
                if (File.Exists(fn))
                    File.Delete(fn);
            }
        }

        [TestMethod]
        public async Task ReloadOldFileDoesNotChangeLastChangeTimeTest()
        {
            var settings = new GeneralSettings() { MaxNumberOfValues = int.MaxValue };
            var ds = new DataSet(DataSetTest._testFileCopy, settings, Encoding.UTF8);
            await ds.LoadAsync(DateTime.UtcNow, false);
            var ts = ds.LastChangeTime;
            await ds.ReloadAsync(DateTime.UtcNow, false);
            Assert.IsTrue(ts.Equals(ds.LastChangeTime));
        }

        [TestMethod]
        public async Task ReloadNewFileChangesLastChangeTimeTest()
        {
            string fn = Path.GetTempPath() + Guid.NewGuid().ToString() + ".dfq";
            File.WriteAllText(fn, DataSetTest._content);
            try
            {
                var settings = new GeneralSettings() { MaxNumberOfValues = int.MaxValue };
                var ds = new DataSet(fn, settings, Encoding.UTF8);
                await ds.LoadAsync(DateTime.UtcNow, false);
                var firstData = await ds.GetQueryableQsDataAsync();
                var ts = ds.LastChangeTime;
                //string contains zero spacing characters char(15) and char(20). Test will break if improperly modified.
                File.AppendAllText(fn, "-0.020982021.08.2017/08:21:160.004980021.08.2017/08:21:16\r\nK0010/0 1");
                await ds.ReloadAsync(DateTime.UtcNow, false);
                var secondData = await ds.GetQueryableQsDataAsync();
                Assert.IsFalse(ts.Equals(ds.LastChangeTime));
                Assert.IsTrue(firstData.CharacteristicsValues.Count < secondData.CharacteristicsValues.Count);
            }
            finally
            {
                if (File.Exists(fn))
                    File.Delete(fn);
            }
        }
    }
}
