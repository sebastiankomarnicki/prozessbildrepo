﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promess.Common.QDas;
using Promess.Pbld.Data.Measurements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Promess.Data.Tests.UnitTests
{
    [TestClass]
    public class DataSetTest
    {
        //[TestMethod]
        //public void DeletedAttributeReplacementTest()
        //{
        //    var metaData = new TS_MERKMALSDATEN[2];
        //    metaData[0] = new TS_MERKMALSDATEN() { iMerkmal_Art = (short) iMerkmal_Art.imaATTRIBUTIV };
        //    metaData[1] = new TS_MERKMALSDATEN() { iMerkmal_Art = (short)iMerkmal_Art.imaVARIABEL };
        //    var parsedData = new List<TS_MERKMAL_IST[]>();
        //    var lineData = new List<DfxLine>();
        //    var currentData = new TS_MERKMAL_IST[2];
        //    string currentLine = "256000\u00142\u0014256\u0014\u0014\u0014\u000f-0.0256\u00140\u0014\u0014\u0014";
        //    currentData[0] = new TS_MERKMAL_IST() { iMerkmal_Art = (short)iMerkmal_Art.imaATTRIBUTIV, iAnzahl_Fehler = 2, iStichprobenUmfang = 256 };
        //    currentData[0] = new TS_MERKMAL_IST() { iMerkmal_Art = (short)iMerkmal_Art.imaVARIABEL, dMesswert = -0.0256 };
        //    parsedData.Append(currentData);
        //    lineData.Append(new DfxLine(currentLine, new List<Tuple<short, string>>))
        //}

        [TestMethod]
        public void DeletedAttributeReplacementTest()
        {
            var metaData = new TS_MERKMALSDATEN[2];
            string testData = "256000\u00142\u0014256\u0014\u0014\u0014\u000f-0.0256\u00140\u0014\u0014\u0014\r\n"+
                "25000\u001422\u00140\u0014\u0014\u0014\u000f-0.02\u0014256\u0014\u0014\u0014\r\n" +
                "24000\u001421\u0014\u0014\u0014\u0014\u000f-0.02\u00140\u0014\u0014\u0014\r\n" +
                "K0002\0 256";
            metaData[0] = new TS_MERKMALSDATEN() { iMerkmal_Art = (short)iMerkmal_Art.imaATTRIBUTIV };
            metaData[1] = new TS_MERKMALSDATEN() { iMerkmal_Art = (short)iMerkmal_Art.imaVARIABEL };
            var parsed = QdasLesen.ParseQdasDFXData(testData, metaData, new Common.QDas.Settings.QdasValueLineSettings());
            var replacedData = DataSet.GenerateDfxData(parsed, true, int.MaxValue, null);
            var parsedReplaced = QdasLesen.ParseQdasDFXData(replacedData, metaData, new Common.QDas.Settings.QdasValueLineSettings());
            TS_MERKMAL_IST originalParsed;
            TS_MERKMAL_IST replacedParsed;
            for (int i=0; i < parsed.Data.Count; i++)
            {
                for (int j=0; j < parsed.Data[i].Length; j++)
                {
                    originalParsed = parsed.Data[i][j];
                    replacedParsed = parsedReplaced.Data[i][j];
                    Assert.IsTrue(originalParsed.iAttribut!=256 && originalParsed.iAttribut == replacedParsed.iAttribut || originalParsed.iAttribut == 256 && replacedParsed.iAttribut == 255, "attribut not replaced or improperly replaced");
                    if (originalParsed.iMerkmal_Art == (short)iMerkmal_Art.imaATTRIBUTIV)
                    {
                        Assert.IsTrue(originalParsed.iStichprobenUmfang == replacedParsed.iStichprobenUmfang, "sample size mismatch");
                        Assert.IsTrue(originalParsed.iAnzahl_Fehler == replacedParsed.iAnzahl_Fehler, "#error mismatch");
                    }
                    else
                    {
                        Assert.IsTrue(originalParsed.dMesswert == replacedParsed.dMesswert, "measurement value mismatch");
                    }
                }
            }
        }
    }
}
