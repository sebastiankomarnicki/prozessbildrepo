﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promess.Pbld.Data.Editor;
using System.Windows.Media;
using Promess.Common.Util;
using System.Windows;
using Promess.Pbld.Utility;
using System.IO;

namespace Promess.Data.Tests.UnitTests
{
    [TestClass]
    public class EditorItemTest
    {
        #region serialization
        [TestMethod]
        public void RectangleEditorItemSerializationTest()
        {
            var origItem = new RectangleEditorItem() {
                BackColor = Colors.Black,
                BorderColor = Colors.Blue,
                BorderThickness = 3,
                PercentageHeight = 0.200,
                RoundedCorners = true,
                ShowBorder = true,
                PercentageWidth = 0.100,
                PercentageX = 0.50,
                PercentageY = 0.75,
                ZIndex = 1
            };
            var copy = SerializeHelper.CreateDataContractCopy(origItem);
            Assert.AreEqual(origItem.BackColor, copy.BackColor);
            Assert.AreEqual(origItem.BorderColor, copy.BorderColor);
            Assert.AreEqual(origItem.BorderThickness, copy.BorderThickness);
            Assert.AreEqual(origItem.PercentageHeight, copy.PercentageHeight);
            Assert.AreEqual(origItem.RoundedCorners, copy.RoundedCorners);
            Assert.AreEqual(origItem.ShowBorder, copy.ShowBorder);
            Assert.AreEqual(origItem.PercentageWidth, copy.PercentageWidth);
            Assert.AreEqual(origItem.PercentageX, copy.PercentageX);
            Assert.AreEqual(origItem.PercentageY, copy.PercentageY);
            Assert.AreEqual(origItem.ZIndex, copy.ZIndex);
            var origSerialized = SerializeHelper.Serialize(origItem);
            var copySerialized = SerializeHelper.Serialize(copy);
            Assert.AreEqual(origSerialized.GetHashCode(), copySerialized.GetHashCode());
        }

        [TestMethod]
        public void EllipseEditorItemSerializationTest()
        {
            var origItem = new EllipseEditorItem()
            {
                BackColor = Colors.Black,
                BorderColor = Colors.Blue,
                BorderThickness = 3,
                PercentageHeight = 0.200,
                RoundedCorners = true,
                ShowBorder = true,
                PercentageWidth = 0.100,
                PercentageX = 0.50,
                PercentageY = 0.75,
                ZIndex = 1
            };
            var copy = SerializeHelper.CreateDataContractCopy(origItem);
            Assert.AreEqual(origItem.BackColor, copy.BackColor);
            Assert.AreEqual(origItem.BorderColor, copy.BorderColor);
            Assert.AreEqual(origItem.BorderThickness, copy.BorderThickness);
            Assert.AreEqual(origItem.PercentageHeight, copy.PercentageHeight);
            Assert.AreEqual(origItem.RoundedCorners, copy.RoundedCorners);
            Assert.AreEqual(origItem.ShowBorder, copy.ShowBorder);
            Assert.AreEqual(origItem.PercentageWidth, copy.PercentageWidth);
            Assert.AreEqual(origItem.PercentageX, copy.PercentageX);
            Assert.AreEqual(origItem.PercentageY, copy.PercentageY);
            Assert.AreEqual(origItem.ZIndex, copy.ZIndex);
            var origSerialized = SerializeHelper.Serialize(origItem);
            var copySerialized = SerializeHelper.Serialize(copy);
            Assert.AreEqual(origSerialized.GetHashCode(), copySerialized.GetHashCode());
        }

        [TestMethod]
        public void TextEditorItemSerializationTest()
        {
            var origItem = new TextEditorItem()
            {
                BackColor = Colors.Black,
                BorderColor = Colors.Blue,
                BorderThickness = 3,
                PercentageHeight = 0.2,
                RoundedCorners = true,
                ShowBorder = true,
                PercentageWidth = 0.1,
                PercentageX = 0.15,
                PercentageY = 0.25,
                ZIndex = 1,
                HorizontalAlignment = HorizontalAlignment.Left,
                Border = BorderType.Raised,
                Font = new FontFamily("Verdana"),
                FontColor = Colors.BlanchedAlmond,
                FontSize = 25,
                QdasNumber = new QdasNumber(TextEvaluationType.Part, 1001),
                OverwriteBackground = true,
                ScaleFont = true,
                BoldFont = true,
                ItalicFont = true,
                Text = "ABC",
                TextType = TextType.Qdas
            };
            var copy = SerializeHelper.CreateDataContractCopy(origItem);
            Assert.AreEqual(origItem.BackColor, copy.BackColor);
            Assert.AreEqual(origItem.BorderColor, copy.BorderColor);
            Assert.AreEqual(origItem.BorderThickness, copy.BorderThickness);
            Assert.AreEqual(origItem.PercentageHeight, copy.PercentageHeight);
            Assert.AreEqual(origItem.RoundedCorners, copy.RoundedCorners);
            Assert.AreEqual(origItem.ShowBorder, copy.ShowBorder);
            Assert.AreEqual(origItem.PercentageWidth, copy.PercentageWidth);
            Assert.AreEqual(origItem.PercentageX, copy.PercentageX);
            Assert.AreEqual(origItem.PercentageY, copy.PercentageY);
            Assert.AreEqual(origItem.ZIndex, copy.ZIndex);
            Assert.AreEqual(origItem.HorizontalAlignment, copy.HorizontalAlignment);
            Assert.AreEqual(origItem.Border, copy.Border);
            Assert.AreEqual(origItem.Font, copy.Font);
            Assert.AreEqual(origItem.FontColor, copy.FontColor);
            Assert.AreEqual(origItem.FontSize, copy.FontSize);
            Assert.AreEqual(origItem.QdasNumber.EvaluationNumber, copy.QdasNumber.EvaluationNumber);
            Assert.AreEqual(origItem.QdasNumber.EvaluationType, copy.QdasNumber.EvaluationType);
            Assert.AreEqual(origItem.OverwriteBackground, copy.OverwriteBackground);
            Assert.AreEqual(origItem.ScaleFont, copy.ScaleFont);
            Assert.AreEqual(origItem.BoldFont, copy.BoldFont);
            Assert.AreEqual(origItem.ItalicFont, copy.ItalicFont);
            Assert.AreEqual(origItem.Text, copy.Text);
            Assert.AreEqual(origItem.TextType, copy.TextType);
        }

        [TestMethod]
        public void ButtonEditorItemSerializationTest()
        {
            var origItem = new ButtonEditorItem()
            {
                BackColor = Colors.Black,
                BorderColor = Colors.Blue,
                BorderThickness = 3,
                PercentageHeight = 0.200,
                RoundedCorners = true,
                ShowBorder = true,
                PercentageWidth = 0.100,
                PercentageX = 0.50,
                PercentageY = 0.75,
                ZIndex = 1,
                HorizontalAlignment = HorizontalAlignment.Left,
                Font = new FontFamily("Verdana"),
                FontColor = Colors.BlanchedAlmond,
                FontSize = 25,
                ScaleFont = true,
                Text = "ABC",
                Function = ButtonFunction.NextDataset
            };
            var copy = SerializeHelper.CreateDataContractCopy(origItem);
            Assert.AreEqual(origItem.BackColor, copy.BackColor);
            Assert.AreEqual(origItem.BorderColor, copy.BorderColor);
            Assert.AreEqual(origItem.BorderThickness, copy.BorderThickness);
            Assert.AreEqual(origItem.PercentageHeight, copy.PercentageHeight);
            Assert.AreEqual(origItem.RoundedCorners, copy.RoundedCorners);
            Assert.AreEqual(origItem.ShowBorder, copy.ShowBorder);
            Assert.AreEqual(origItem.PercentageWidth, copy.PercentageWidth);
            Assert.AreEqual(origItem.PercentageX, copy.PercentageX);
            Assert.AreEqual(origItem.PercentageY, copy.PercentageY);
            Assert.AreEqual(origItem.ZIndex, copy.ZIndex);
            Assert.AreEqual(origItem.HorizontalAlignment, copy.HorizontalAlignment);
            Assert.AreEqual(origItem.Font, copy.Font);
            Assert.AreEqual(origItem.FontColor, copy.FontColor);
            Assert.AreEqual(origItem.FontSize, copy.FontSize);
            Assert.AreEqual(origItem.ScaleFont, copy.ScaleFont);
            Assert.AreEqual(origItem.Text, copy.Text);
            Assert.AreEqual(origItem.Function, copy.Function);
        }

        [TestMethod]
        public void ImageEditorItemSerializationTest()
        {
            var origItem = new ImageEditorItem()
            {
                BackColor = Colors.Black,
                BorderColor = Colors.Blue,
                BorderThickness = 3,
                PercentageHeight = 0.200,
                RoundedCorners = true,
                ShowBorder = true,
                PercentageWidth = 0.100,
                PercentageX = 0.50,
                PercentageY = 0.75,
                ZIndex = 1,
                Border = BorderType.Lowered,
                ImageSourceType = ImageSourceType.Qdas,
                EvaluationCode = 3400
            };
            var copy = SerializeHelper.CreateDataContractCopy(origItem);
            Assert.AreEqual(origItem.BackColor, copy.BackColor);
            Assert.AreEqual(origItem.BorderColor, copy.BorderColor);
            Assert.AreEqual(origItem.BorderThickness, copy.BorderThickness);
            Assert.AreEqual(origItem.PercentageHeight, copy.PercentageHeight);
            Assert.AreEqual(origItem.RoundedCorners, copy.RoundedCorners);
            Assert.AreEqual(origItem.ShowBorder, copy.ShowBorder);
            Assert.AreEqual(origItem.PercentageWidth, copy.PercentageWidth);
            Assert.AreEqual(origItem.PercentageX, copy.PercentageX);
            Assert.AreEqual(origItem.PercentageY, copy.PercentageY);
            Assert.AreEqual(origItem.ZIndex, copy.ZIndex);
            Assert.AreEqual(origItem.Border, copy.Border);
            Assert.AreEqual(origItem.EvaluationCode, copy.EvaluationCode);
            Assert.AreEqual(origItem.ImagePath, copy.ImagePath);
            Assert.AreEqual(origItem.ImageSourceType, copy.ImageSourceType);
            var origSerialized = SerializeHelper.Serialize(origItem);
            var copySerialized = SerializeHelper.Serialize(copy);
            Assert.AreEqual(origSerialized.GetHashCode(), copySerialized.GetHashCode());
            string imagePath = Path.GetFullPath(@".\UnitTests\PromessLogoWBg.png");
            byte[] image = ImageHelper.GetImageFromPathAsByteArray(imagePath);
            origItem.ImageSourceType = ImageSourceType.File;
            origItem.ImagePath = imagePath;
            origItem.Image = image;
            copy = SerializeHelper.CreateDataContractCopy(origItem);
            Assert.AreEqual(origItem.ImagePath, copy.ImagePath);
            Assert.AreEqual(origItem.ImageSourceType, copy.ImageSourceType);
            CollectionAssert.AreEqual(origItem.Image, copy.Image);
        }

        [TestMethod]
        public void CircleEditorItemSerializationTest()
        {
            var origItem = new CircleEditorItem()
            {
                BackColor = Colors.Black,
                BorderColor = Colors.Blue,
                BorderThickness = 3,
                ShowBorder = true,
                PercentageX = 0.50,
                PercentageY = 0.75,
                ZIndex = 1,
                PercentageDiameter = 0.200
            };
            var copy = SerializeHelper.CreateDataContractCopy(origItem);
            Assert.AreEqual(origItem.BackColor, copy.BackColor);
            Assert.AreEqual(origItem.BorderColor, copy.BorderColor);
            Assert.AreEqual(origItem.BorderThickness, copy.BorderThickness);
            Assert.AreEqual(origItem.ShowBorder, copy.ShowBorder);
            Assert.AreEqual(origItem.PercentageX, copy.PercentageX);
            Assert.AreEqual(origItem.PercentageY, copy.PercentageY);
            Assert.AreEqual(origItem.ZIndex, copy.ZIndex);
            Assert.AreEqual(origItem.PercentageDiameter, copy.PercentageDiameter);
            var origSerialized = SerializeHelper.Serialize(origItem);
            var copySerialized = SerializeHelper.Serialize(copy);
            Assert.AreEqual(origSerialized.GetHashCode(), copySerialized.GetHashCode());
        }
        #endregion

        [TestMethod]
        public void EditorItemIsDirtyTest()
        {
            var item = new TestEditorItem();
            item.ZIndex += 1;
            Assert.IsTrue(item.IsDirty);
        }

        [TestMethod]
        public void EditorItemsNotDirtyOnCreation()
        {
            var button = new ButtonEditorItem();
            Assert.IsFalse(button.IsDirty);
            var circle = new CircleEditorItem();
            Assert.IsFalse(circle.IsDirty);
            var ellipse = new EllipseEditorItem();
            Assert.IsFalse(ellipse.IsDirty);
            var image = new ImageEditorItem();
            Assert.IsFalse(image.IsDirty);
            var line = new LineEditorItem();
            Assert.IsFalse(line.IsDirty);
            var rectangle = new RectangleEditorItem();
            Assert.IsFalse(rectangle.IsDirty);
            var text = new TextEditorItem();
            Assert.IsFalse(text.IsDirty);
        }

        private class TestEditorItem : EditorItemBase
        {

            public override Rect GetBoundingRect()
            {
                throw new NotImplementedException();
            }

            public override void ShiftPosition(Vector delta)
            {
                throw new NotImplementedException();
            }
        }
    }
}
