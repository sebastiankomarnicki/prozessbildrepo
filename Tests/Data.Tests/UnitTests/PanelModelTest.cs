﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Promess.Pbld.Data.Editor;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using Promess.Common.Util;
using System.Windows.Media;
using System.Runtime.Serialization;
using System.Xml;
using System.Text;
using System.IO;
using Promess.Pbld.Utility;

namespace Promess.Data.Tests.UnitTests
{
    [TestClass]
    public class PanelModelTest
    {
        private static List<EditorItemBase> _testElements;

        [ClassInitialize()]
        public static void PanelModelTestInitialize(TestContext tc)
        {
            ImageEditorItem imageItem;
            _testElements = new List<EditorItemBase>();
            _testElements.Add(new LineEditorItem() { X1 = 150, X2 = 100, Y1 = 50, Y2 = 0 });
            _testElements.Add(new LineEditorItem() { X1 = 50, X2 = 0, Y1 = 0, Y2 = 50 });
            _testElements.Add(new CircleEditorItem() { X = 100, Y = 50, Diameter = 20 });
            _testElements.Add(new CircleEditorItem() { X = 300, Y = 150, Diameter = 50 });
            _testElements.Add(new EllipseEditorItem() { X = 200, Y = 200, Width = 50, Height = 100 });
            _testElements.Add(new RectangleEditorItem() { X = 200, Y = 100, Width = 50, Height = 50 });
            _testElements.Add(new RectangleEditorItem() { X = 150, Y = 150, Width = 50, Height = 50, RoundedCorners = true });
            _testElements.Add(new TextEditorItem()
            {
                X = 250,
                Y = 250,
                Width = 50,
                Height = 50,
                Text = "bfgggtfzzgla",
                BackColor = Colors.AliceBlue,
                FontColor = Colors.Salmon,
                Border = BorderType.Raised,
                ScaleFont = true
            });
            _testElements.Add(new ButtonEditorItem() { X = 350, Y = 350, Width = 50, Height = 50, Text = "retrtrttretertrtgggggggggggggggggggggggggggggggla" });
            imageItem = new ImageEditorItem() { X = 500, Y = 400, Width = 50, Height = 50, ImageSourceType=ImageSourceType.File, ImagePath= Path.GetFullPath(@".\UnitTests\PromessLogoWBg.png") };           
            byte[] image = ImageHelper.GetImageFromPathAsByteArray(imageItem.ImagePath);           
            imageItem.Image = image;
            _testElements.Add(imageItem);
            imageItem = new ImageEditorItem() { X = 550, Y = 550, Width = 50, Height = 50, ImageSourceType = ImageSourceType.Qdas, EvaluationCode = 3000 };
            _testElements.Add(imageItem);
        }

        [TestMethod]
        public void PanelModelSerializationManualTest()
        {
            var panel = new PanelItemModel("Test");
            foreach (var elem in _testElements)
            {
                panel.Items.Add(elem);
            }
            var writerSettings = new XmlWriterSettings()
            {
                Indent = true,
                IndentChars = "  ",
                OmitXmlDeclaration = true,
                Encoding = new UTF8Encoding(false),
                CloseOutput = false
            };
            var serializer = new DataContractSerializer(typeof(PanelItemModel));
            string data;
            using (MemoryStream ms = new MemoryStream())
            {
                serializer.WriteObject(ms, panel);
                data = Encoding.UTF8.GetString(ms.ToArray());
            }
            PanelItemModel copy;
            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data)))
            {
                copy = (PanelItemModel)serializer.ReadObject(ms);
            }
        }

        [TestMethod]
        public void PanelModelSerializationTest()
        {
            var panel = new PanelItemModel("Test");
            foreach (var elem in _testElements)
            {
                panel.Items.Add(elem);
            }
            var copy = SerializeHelper.CreateDataContractCopy(panel);
        }

        [TestMethod]
        public void PanelModelIsDirtyResetTest()
        {
            var panel = new PanelItemModel("tmp");
            Assert.IsTrue(panel.GetIsDirty());
            panel.ResetDirty();
            Assert.IsFalse(panel.GetIsDirty());
        }

        [TestMethod]
        public void AddingRemovingElementSetsIsDirtyTest()
        {
            var panel = new PanelItemModel("");
            var item = new LineEditorItem();
            panel.Items.Add(item);
            Assert.IsTrue(panel.GetIsDirty());
            panel.ResetDirty();
            panel.Items.Remove(item);
            Assert.IsTrue(panel.GetIsDirty());
        }

        [TestMethod]
        public void ModifyItemSetsIsDirtyTest()
        {
            var panel = new PanelItemModel("");
            var item = new LineEditorItem();
            //need to set X so it is not NaN, because logic was changed to percentage based
            item.X = 0;
            panel.Items.Add(item);
            panel.ResetDirty();
            item.X += 1;
            Assert.IsTrue(panel.GetIsDirty());
        }

        [TestMethod]
        public void ResetResetsModifiedItemIsDirtyTest()
        {
            var panel = new PanelItemModel("");
            var item = new LineEditorItem();
            panel.Items.Add(item);
            panel.ResetDirty();
            item.X += 1;
            panel.ResetDirty();
            Assert.IsFalse(panel.GetIsDirty());
        }
    }
}
