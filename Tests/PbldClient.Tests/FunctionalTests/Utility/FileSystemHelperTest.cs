﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Promess.Pbld.Utility;
using System.Linq;

namespace PbldClient.Tests.IntegrationTests.Utility
{
    [TestClass]
    public class FileSystemHelperTest
    {
        const string sub1 = "sub1";
        const string leaf1 = "leaf1";
        const string leaf2 = "leaf2";
        const string leaf3 = "leaf3";

        static string _root;
        static string _subdir;
        static string _leaf1dir;
        static string _leaf2dir;
        static string _leaf3dir;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            FileSystemHelperTest._root = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            FileSystemHelperTest._subdir = Path.Combine(FileSystemHelperTest._root, sub1);
            FileSystemHelperTest._leaf1dir = Path.Combine(FileSystemHelperTest._root, leaf1);
            FileSystemHelperTest._leaf2dir = Path.Combine(FileSystemHelperTest._subdir, leaf2);
            FileSystemHelperTest._leaf3dir = Path.Combine(FileSystemHelperTest._subdir, leaf3);
            Directory.CreateDirectory(FileSystemHelperTest._root);
            Directory.CreateDirectory(FileSystemHelperTest._subdir);
            Directory.CreateDirectory(FileSystemHelperTest._leaf1dir);
            Directory.CreateDirectory(FileSystemHelperTest._leaf2dir);
            Directory.CreateDirectory(FileSystemHelperTest._leaf3dir);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            Directory.Delete(FileSystemHelperTest._root);
        }

        [TestMethod]
        public void IsLeafDirectoryTest()
        {
            Assert.IsFalse(FileSystemHelper.IsLeafDirectory(FileSystemHelperTest._root));
            Assert.IsFalse(FileSystemHelper.IsLeafDirectory(FileSystemHelperTest._subdir));
            Assert.IsTrue(FileSystemHelper.IsLeafDirectory(FileSystemHelperTest._leaf1dir));
            Assert.IsTrue(FileSystemHelper.IsLeafDirectory(FileSystemHelperTest._leaf2dir));
            Assert.IsTrue(FileSystemHelper.IsLeafDirectory(FileSystemHelperTest._leaf3dir));
        }

        [TestMethod]
        public void GetLeafDirectoriesTest()
        {
            bool success;
            List<string> leaves;
            (success, leaves) = FileSystemHelper.GetLeafDirectories(FileSystemHelperTest._root);
            Assert.IsTrue(success);
            Assert.AreEqual(3, leaves.Count);
            Assert.IsTrue(leaves.Any(leaf => String.Equals(leaf, FileSystemHelperTest._leaf1dir, StringComparison.OrdinalIgnoreCase)));
            Assert.IsTrue(leaves.Any(leaf => String.Equals(leaf, FileSystemHelperTest._leaf2dir, StringComparison.OrdinalIgnoreCase)));
            Assert.IsTrue(leaves.Any(leaf => String.Equals(leaf, FileSystemHelperTest._leaf3dir, StringComparison.OrdinalIgnoreCase)));
            (success, leaves) = FileSystemHelper.GetLeafDirectories(FileSystemHelperTest._subdir);
            Assert.IsTrue(success);
            Assert.AreEqual(2, leaves.Count);
            Assert.IsTrue(leaves.Any(leaf => String.Equals(leaf, FileSystemHelperTest._leaf2dir, StringComparison.OrdinalIgnoreCase)));
            Assert.IsTrue(leaves.Any(leaf => String.Equals(leaf, FileSystemHelperTest._leaf3dir, StringComparison.OrdinalIgnoreCase)));

        }
    }
}
